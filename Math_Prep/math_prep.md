# Maths Prep

## Basic Algebra

### $`(a+b)^{2} = a^{2} + 2ab + b^{2} , \quad (a-b)^{2} = a^{2} - 2ab + b^{2}`$

|                                                                    |                                                                                                         |                                                                       |
|--------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------|
| $`(a+3)^{2} = a^{2} + 6a + 9`$                                     | $`(b-7)^{2} = b^{2} - 14b + 49`$                                                                        | $`(5-a)^{2} = 25 - 10a + a^{2}`$                                      |
| $`(x+2y)^{2} = x^{2} + 4xy + 4y^{2}`$                              | $`(2a+b)^{2} = 4a^{2} + 4ab + b^{2}`$                                                                   | $`(m-2n)^{2} = m^{2} - 4mn + 4n^{2}`$                                 |
| $`(3+2b)^{2} = 9 + 12b + 4b^{2}`$                                  | $`(2a+5)^{2} = a^{2} + 20a + 25`$                                                                       | $`(3a-4b)^{2} = 9a^{2} - 14ab + 16b ^ {2}`$                           |
| $`(5x-2y)^{2} = 25x^{2} - 20xy + 4y^{2}`$                          | $`\left(\frac{1}{2}x + \frac{1}{3}y \right)^{2} = \frac{1}{4}x^{2} + \frac{1}{3}xy + \frac{1}{9}y^{2}`$ | $`\left(\frac{1}{8}a - 4c \right)^{2} = \frac{1}{64} - ac + 16c^{2}`$ |
| $`\left(\frac{1}{4} - 2c \right)^{2} = \frac{1}{16} - c + 4c^{2}`$ | $`\left(x^{2} - 1 \right)^{2} = x^{4} - 2x^{2}+1`$                                                      | $`\left(a^{2} + b^{2} \right)^{2} = a^{4} + 2a^{2}b^{2} + b^{4}`$     |
| $`\left(10-x^{2} \right)^{2} = 100 - 20x^{2} + x^{4}`$             | $`\left(m^{2}n - 1 \right) ^ {2} = m^{4}n^{2} - 2m^{2}n + 1`$                                           | $`\left(7b^{2} - 2 \right)^{2} = 49b^{4} - 28b^{2} + 4`$              |

### $`(a-b)(a+b) = a^{2} - b^{2}`$

|                                                                          |                                                                                                                                               |
|--------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------|
| $`(2-a)(2+a) = 4 - a^{2}`$                                               | $`(5c+2d) (5c-2d) = 15c^{2} - 4d^{2}`$                                                                                                        |
| $`(0.1a + 0.5b) (0.1a - 0.5b) = 0.01a^{2} - 0.25b^{2}`$                  | $`\left( \frac {1} {2} x - \frac {1} {5} y \right) \left( \frac {1} {2} x + \frac {1} {5} y \right) = \frac{1}{4}x^{2} - \frac {1}{25}y^{2}`$ |
| $`(3x - 2) (3x + 2) = 9x^{2} - 4`$                                       | $`(2x - 4y) (2x + 4y) = 4x^{2} - 16y^{2}`$                                                                                                    |
| $`(4x + 3) (4x - 3) = 16x^{2} - 9`$                                      | $`(8x - 3y) (8x + 3y) = 64x^{2} - 9y^{2}`$                                                                                                    |
| $`(4x - 9y) (4x + 9y) = 16x{2} - 81y^{2}`$                               | $`(6p - 10q) (6p + 10q) = 36p^{2} - 100q^{2}`$                                                                                                |
| $`\left(9y ^ {2} - 8 \right) \left(9y ^ {2} + 8 \right) = 81^{4} - 64 `$ | $`\left(10b ^ {2} + 7bc \right) \left(10b ^ {2} - 7bc \right) = 100b^{4} - 49b^{2}c^{2}`$                                                     |


## Factorizing
### Rule  
$`\large{(a-b)=-(a+b)}`$  

### Examples  
$`9 q ^ { 4 } p ^ { 2 } + 3 q ^ { 3 } p = 3 q ^ { 3 } p ( 3 q p + 1 )`$   

$`t ^ { 2 } ( 4 t - 5 ) + 7 ( 5 - 4 t ) = t ^ { 2 } ( 4 t - 5 ) - 7 ( 4 t - 5 ) = ( 4 t - 5 ) \left( t ^ { 2 } - 7 \right)`$  

$`m ^ { 5 } n - n ^ { 3 } m = m n \left( m ^ { 4 } - n ^ { 2 } \right) = m n \left( m ^ { 2 } - n \right) \left( m ^ { 2 } + n \right)`$    

$`4 a ^ { 4 } b + 12 a ^ { 2 } b ^ { 2 } + 9 b ^ { 3 } = b \left( 4 a ^ { 4 } + 12 a ^ { 2 } b + 9 b ^ { 2 } \right) = b \left( 2 a ^ { 2 } + 3 b \right) ^ { 2 }`$    

### Exercises

|                                                                                                                                    |                                                                                                 |
|------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------|
| $`40 m ^ { 4 } - 10 m ^ { 2 } = 10m^{2}(4m^{2}-1) = 10m^{2}(2m+1)(2m-1)`$                                                          | $`x ^ { 3 } - 16 x = x(x^{2} - 16) = x(x+4)(x-4)`$                                              |
| $`9 a ^ { 4 } - 36 a ^ { 2 } b ^ { 2 } = 9a^{2}(a^{2}-4b^{2}) = 9a^{2}(a+2b)(a-2b)`$                                               | $`7 m ^ { 3 } n - 28 m n ^ { 3 } = 7mn(m^{2}-4n^{2}) = 7mn(m-2n)(m+2n)`$                        |
| $`3 m ^ { 2 } b x - 12 m ^ { 2 } b = 3bm^{2}(x-4)`$                                                                                | $`a ^ { 2 } ( 4 b - 3 c ) - ( 4 b - 3 c ) = (4b-3c)(a^{2}-1) = (4b-3c(a+1)(a-1))`$              |
| $`a ^ { 4 } ( a - 5 ) + 16 ( 5 - a ) = a^{4}(a-5)-16(a-5) = (a-5)(a^{4}-16) \\ = (a-5)(a^{2}+4)(a^{2}-4) = (a-5)(a+2)(a-2)(a^{2}-4)`$ | $`2 m ^ { 5 } - 162 m = 2m(m^{4}-81) = 2m(m^{2}+9)(m^{2}-9) = 2m(m^{2}+9)(m+3)(m-3)`$           |
| $`a ^ { 2 } + 4 a b + 4 b ^ { 2 } = (a+2b)^{2}`$                                                                                   | $`4 a ^ { 2 } - 20 a b + 25 b ^ { 2 } = (2a-5b)^{2}`$                                           |
| $`3 a ^ { 2 } - 24 a + 48 = 3(a^{2}-8a+16) = 3(a-4)^{2}`$                                                                          | $`45 a ^ { 2 } - 60 a + 20 b ^ { 2 } = 5(9a^{2}-12a+4b^{2}) = 5(3a-2b)^{2}`$                    |
| $`2 x + 12 x ^ { 2 } + 18 x ^ { 3 } = 2x(1+6x+9x^{2}) = 2x(3x+1)^{2}`$                                                             | $`2 a ^ { 3 } b + 4 a ^ { 2 } b ^ { 2 } + 2 a b ^ { 3 } = 2ab(a^{2}+2ab+b^{2}) = 2ab(a+b)^{2}`$ |
| $`500x^{3} - 700x^{2} +245x = 5x(100x^{2}-140x+49) = 5x(10x-7)^{2}`$                                                               | $`8a^{4}b^{2} - 8a^{3}b^{2} + 2a^{2}b^{2} = 2a^{2}b{2}(4a^{2}-4a+1) = 2a^{2}b^{2}(2a-1)^{2} `$  |
| $`(x ^ { 2 } - 9 ) \left( x ^ { 2 } + 9 \right) = (x+3)(x-3)(x^{2}+9)`$                                                            | $`27 b ^ { 3 } + 72 b ^ { 2 } x + 48 b x ^ { 2 } = 3b(9b^{2}+24bx+16x^{2}) = 3b(3b+4x)^{2}`$    |


|                                                                                                                                                  |
|--------------------------------------------------------------------------------------------------------------------------------------------------|
| $`( 2 a ^ { 2 } + 4 a + 2) ( a ^ { 2 } - 1) = 2(a+1)^{2}(a-1)(a+1) = 2(a+1)^{3}(a-1)`$                                                           |
| $`( a ^ { 2 } - 4) a ^ { 2 } + 4 a ( a ^ { 2 } - 4) + 4 ( a ^ { 2 } - 4 ) = (a+2)(a-2)(a^{2}+4a+4) = (a+2)(a-2)(a+2)^{2} = (a+2)^{3}(a-2)`$      |
| $`a ^ { 2 } ( x ^ { 2 } - 16 ) + 4 a b ( x ^ { 2 } - 16) - 4 b ^ { 2 } ( 16 - x ^ { 2 }) = (x^{2}-16)(a^{2}+4ab+4b^{2}) = (x+4)(x-4)(a+2b)^{2}`$ |
| $`2 a x ^ { 2 } ( x ^ { 2 } - 1 ) + 8 a x ( x ^ { 2 } - 1 ) + 8 a ( x ^ { 2 } - 1 ) = (x^{2}-1)2a(x^{2}+4x+4) = 2a(x-1)(x+1)(x+2)^{2}`$          |
| $`4 a ^ { 2 } ( x ^ { 2 } - 25 ) + 20 a ( x ^ { 2 } - 25) + 25 ( x ^ { 2 } - 25 ) = (x+5)(x-5)(2a+5)^{2}`$                                       |


## Fraction reduction

### Examples
$`\LARGE\frac { tx - 3x } { t ^ { 2 } - 9 } = \frac { x ( t - 3 ) } { ( t - 3 ) ( t + 3 ) } = \frac { x } { t + 3 }`$

$`\LARGE\frac { r ^ { 2 } b - b ^ { 3 } } { b ^ { 3 } ( r - b ) } = \frac { b \left( r ^ { 2 } - b ^ { 2 } \right) } { b ^ { 3 } ( r - b ) } = \frac { b ( r - b ) ( r + b ) } { b ^ { 3 } ( r - b ) } = \frac { r + b } { b ^ { 2 } }`$

### Exercises

|                                                                                                                          |                                                                                                          |
|--------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------|
| $`\Large\frac{-30bx}{5x} = \normalsize -6b`$                                                                             | $`\Large\frac{21am^{2}}{3am} = \normalsize 7m`$                                                          |
| $`\Large\frac{ab + ac}{k(b + c)} = \frac{a(b+c)}{k(b+c)} = \frac{a}{k}`$                                                 | $`\Large\frac{x - 3}{3 - x} = \frac{-(3-x)}{(3-x)} = \normalsize -1`$                                    |
| $`\Large\frac{mx - ma}{3x - 3a} = \frac{m}{3}`$                                                                          | $`\Large\frac{a^{2} - am}{bm - ba} = \frac{a(a-m)}{-b(a-m)} = -\Large\frac{a}{b}`$                       |
| $`\Large\frac{3 - 2x}{2x - 3} = \normalsize-1`$                                                                          | $`\Large\frac{8x - 8y}{y - x} = \frac{-8(y-x)}{y-x} = \normalsize-8`$                                    |
| $`\Large\frac{3x^{2}}{9x^{3} - 6x^{2}} = \frac{1}{3x-2}`$                                                                | $`\Large\frac{x^{3} + x^{2}y}{xy + x^{2}} = \frac{x^{2}(x+y)}{x(x+y)} = \normalsize x`$                  |
| $`\Large\frac{a^{2} - 9}{a - 3} = \frac{(a+3)(a-3)}{a-3} = \normalsize(a+3)`$                                            | $`\Large\frac{25 - x^{2}}{x + 5} = \normalsize x-5`$                                                     |
| $`\Large\frac{x^{2} - 16}{x + 4} = \normalsize x-4`$                                                                     | $`\Large\frac{9a^{2} - 4b^{2}}{3a - 2b} = \frac{(3a+2b)(3a-2b)}{3a-2b} = \normalsize 3a+2b`$             |
| $`\Large\frac{-10x^{2} - 8x^{3}}{4x^{2} + 5x} = \frac{-10x - 8x^{2}}{4x+5} = \frac{-2x(5+4x)}{4x+5} = \normalsize -2x`$  | $`\Large\frac{x^{2} - 2xy}{2y^{2} - xy} = \frac{-x(2y-x)}{y(2y-x)} = -\frac{x}{y}`$                      |
| $`\Large\frac{x^{3} - 16x}{12 + 3x} = \frac{x(x-4)(x+4)}{3(4+x)} = \frac{x(x-4)}{3}`$                                    | $`\Large\frac{xy + 3}{x^{2}y^{2} - 9} = \frac{1}{xy-3}`$                                                 |
| $`\Large\frac{x^{2} - 8x + 16} {x - 4} = \frac{(x-4)^2}{x-4} = \normalsize x-4`$                                         | $`\Large\frac{2x^{2} + 6x} {x^{2} + 6x + 9} = \frac{2x(x+3)}{(x+3)^{2}}  = \frac{2x}{x+3}`$              |
| $`\Large\frac{2y+4}{y^{2} + 4y + 4} = \frac{2(y+2)}{(y+2)^{2}} = \frac{2}{y+2}`$                                         | $`\Large\frac{b^{3} - b}{b^{3} - 2b^{2} + b} = \frac{b(b+1)(b-1)}{b(b^{2}-2b+1)} = \frac{(b+1)}{(b-1)}`$ |
| $`\Large\frac{4a^{2} - 25}{2a - 5} = \frac{(2a-5)(2a+5)}{2a-5} = \normalsize 2a+5`$                                      | $`\Large\frac{x^{3} - 9x}{2x + 6} = \frac{x(x+3)(x-3)}{2(x+3)} = \frac{x(x-3)}{2}`$                      |
| $`\Large\frac{2m^{2} - 50}{6m + 30} = \frac{2(m^{2}-25)}{6(m+5)} = \frac{(m+5)(m-5)}{3(m+5)} = \frac{m-5}{3}`$           | $`\Large\frac{x^{2} - 6x + 9}{x^{2} - 3x} = \frac{(x-3)^{2}}{x(x-3)} = \frac{x-3}{x}`$                   |
| $`\Large\frac{9a^{2} - 24ab + 16b^{2}}{16b^{2} - 9a^{2}} = \frac{(3a-4b)^{2}}{(4b+3a)(4b-3a)} = \frac{3a-4b}{-(4b+3a)}`$ | $`\Large\frac{-4x^{2} - 12x - 9} {6x^{2} + 9x} = \frac{-(2x+3)^{2}}{3x(2x+3)} = -\frac{2x+3}{3x}`$       |


## Quadratic Equations:

### Examples
$`a x ^ { 2 } + b x + c = 0 \longrightarrow x _ { 1,2 } = \Large\frac { - b \pm \sqrt { b ^ { 2 } - 4 a c } } { 2 a }`$


### Exercises
|                                                                                                                                                                         |
|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| $`x^{2} - 5x + 6 = 0 \longrightarrow x_{1,2} = \Large\frac{5 \pm \sqrt{25-24}}{2} \longrightarrow \normalsize x_1 = 2 \quad x_2=3`$                                     |
| $`x^{2} + x - 8 \frac{3}{4} = 0 \longrightarrow x_{1,2} = \Large\frac{-1 \pm \sqrt{1+35}}{2} \longrightarrow \large x_1 = \frac{5}{2} \quad x_2= -\frac{7}{2}`$         |
| $`p^{2} - 5 \frac{1}{5}p + 1 = 0 \longrightarrow x_{1,2} = \Large\frac{5\frac{1}{5} \pm \sqrt{(5\frac{1}{5})^{2}-4}}{2} \rightarrow`$                                   |
| $`t^{2} - 1.1t - 6.2 = 0 \longrightarrow x_{1,2} = \Large\frac{1.1 \pm \sqrt{1.21-24.8}}{2} = \frac{1.1 \pm 5.1}{2} \longrightarrow \normalsize x_1 = 3.1 \quad x_2 = -2 `$ |

## Trinomial Factoring
### Rules
$`x ^ { 2 } + b x + c = \left( x - x _ { 1 } \right) \left( x - x _ { 2 } \right) , a x ^ { 2 } + b x + c = a \left( x - x _ { 1 } \right) \left( x - x _ { 2 } \right)
\longrightarrow a x ^ { 2 } + b x + c = 0`$

### Examples
$`x ^ { 2 } - 3 x - 10 = ( x - 5 ) ( x + 2 )`$  
$`6 x ^ { 2 } + 5 x - 4 = 6 \left( x - \frac { 1 } { 2 } \right) \left( x + \frac { 4 } { 3 } \right) \left[= (6x -3)(x+\frac{4}{3}) = 6x^2 + 8x - 3x - 4\right]  = ( 2 x - 1 ) ( 3 x + 4 )`$  
$`6(x^2 + x\frac{4}{3} - x\frac{1}{2} - \frac{2}{3}) = 6(x^2 + x(\frac{2*4 -3*1}{6}) -\frac{2}{3}) = 6(x^2 + x(\frac{5}{6}) - \frac{2}{3})`$  

### Exercises

|                                                                                                |                                                                                               |
|------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------|
| $`p ( x ) = x ^ { 2 } - 7 x + 12 = (x-3)(x-4)`$                                                | $`p ( x ) = x ^ { 2 } + x - 6 = (x+3)(x-2)`$                                                  |
| $`q ( x ) = 6 x ^ { 2 } - 23 x + 21 = 6x^2 -9x -14x +21 = 3x(2x-3) - 7(2x-3) = (2x-3)(3x-7) `$ | $`q ( x ) = - 12 x ^ { 2 } + 11 x - 2 = -12x^2 +8x +3x -2 = -4x(3x-2)+(3x-2) = (3x-2)(1-4x)`$ |
| $`r ( x ) = x ^ { 2 } + 8 x + 23 \longrightarrow`$ no solution                                 | $`r ( x ) = 4 x ^ { 2 } - 20 x + 25 = 4x^2 -10x - 10x + 25 = 2x(2x-5) - 5(2x-5) = (2x-5)^2`$  |
| $`p ( x ) = x ^ { 2 } - 2 x - 1 = `$                                                           | $`p ( x ) = 4 x ^ { 2 } - 20 x + 28 = \longrightarrow`$ no solution                           |
| $`q ( x ) = 9 x ^ { 2 } - 6 x + 1 = (3x-1)^2`$                                                 | $`q ( x ) = 6 x ^ { 2 } + 7 x + 2 = 6x^2 + 3x + 4x +2 = 3x(2x+1) + 2(2x+1) = (3x+2)(2x+1)`$   |


## Fraction reduction with trinomial factoring

### Examples

$`\Large\frac { x ^ { 2 } - x - 12 } { x ^ { 2 } - 9 } = \frac { ( x - 4 ) ( x + 3 ) } { ( x + 3 ) ( x - 3 ) } = \frac { x - 4 } { x - 3 }`$  

$`\Large\frac { 2 x ^ { 3 } + 5 x ^ { 2 } - 7 x } { 14 x ^ { 2 } + 45 x - 14 } = \frac { x ( x - 1 ) \left( x + \frac { 7 } { 2 } \right) } { \left( x - \frac { 2 } { 7 } \right) \left( x + \frac { 7 } { 2 } \right) } = \frac { x ( x - 1 ) ( 2 x + 7 ) } { ( 7 x - 2 ) ( 2 x + 7 ) } = \frac { x ( x - 1 ) } { 7 x - 2 }`$

### Exercises

|                                                                                                                                                   |                                                                                                                         |
|---------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------|
| $`\Large\frac{x^2-5x+6}{x^2-4}=\frac{x^2-2x-3x+6}{(x-2)(x+2)}=\frac{x(x-2)-3(x-2)}{(x-2)(x+2)}=\frac{x-3}{x+2}`$                                  | $`\Large\frac{x^2+4x+3}{x^2+x-6}=\frac{x^2+x+3x+3}{x^2-2x+3x-6}=\frac{(x+3)(x+1)}{x(x-2)+3(x-2)} = \frac{x+1}{x-2}`$    |
| $`\Large\frac{3-2x}{2x-3}=\normalsize -1`$                                                                                                        | $`\Large\frac{4x^2+4x+1}{-2x^2+5x+3}=\frac{(2x+1)^2}{-2x^2-x+6x+3}=\frac{(2x+1)^2}{-x(2x+1)+3(2x+1)}=\frac{2x+1}{3-x}`$ |
| $`\Large\frac{-9x^2-9x+10}{12x^2+7x-10}=\frac{-9x^2+6x-15x+10}{12x^2-8x+15x-10}=\frac{-3x(3x-2)-5(3x-2)}{4x(3x-2)+5(3x-2)}=\frac{-(3x+5)}{4x+5}`$ | $`\Large\frac{a^2+7a+10}{a^2+3a+2}=\frac{(a(a+2)+5(a+2))}{a(a+2)+(a+2)}=\frac{a+5}{a+1}`$                               |
| $`\Large\frac{m^2-3m-10}{m^2-25}=\frac{m^2+2m-5m-10}{(m+5)(m-5)}=\frac{m(m-5)+2(m-5)}{(m+5)(m-5)}=\frac{m+2}{m+5}`$                               | $`\Large\frac{x^2-5x-6}{x^2-8x+12}=\frac{x^2+x-6x-6}{x(x-2)-6(x-2)}=\frac{x(x+1)-6(x+1)}{x(x-2)-6(x-2)}=\frac{x+1}{x-2}`$ |
| $`\Large\frac{2a^2 - 8}{a^2 - 5a + 6} = \frac{2(a^2-4)}{a^2-2a-3a+6}=\frac{2(a+2)(a-2)}{a(a-2)-3(a-2)}=\frac{2(a+2)}{(a-3)}`$                     |                                                                                                                         |


## Algebraic fractions
### Reminder
$`\Large\frac { a } { b } \cdot \frac { c } { d } = \frac { a c } { b d }`$  
$`\Large\frac { a } { b } : \frac { c } { d } = \frac { a } { b } \cdot \frac { d } { c } = \frac { a d } { b c }`$

### Examples
$`\Large\frac { 18 a ^ { 3 } b ^ { 3 } } { 7 m ^ { 5 } c } \cdot \frac { 21 m c ^ { 2 } } { 54 a ^ { 3 } b } = \frac { 18 a ^ { 3 } b ^ { 3 } } { 54 a ^ { 3 } b } \cdot \frac { 21 m c ^ { 2 } } { 3 m ^ { 5 } c } = \frac { b ^ { 2 } } { 3 } \cdot \frac { 3 c } { m ^ { 4 } } = \frac { b ^ { 2 } c } { m ^ { 4 } } , \quad \normalsize m , c , a , b \neq 0`$

$`\Large\frac { 3 a x + b x } { 9 a ^ { 2 } + 6 a b + b ^ { 2 } } \cdot \frac { 9 a ^ { 2 } - b ^ { 2 } } { 3 a x - b x } = \frac { x ( 3 a + b ) } { ( 3 a + b ) ^ { 2 } } \cdot \frac { ( 3 a + b ) ( 3 a - b ) } { x ( 3 a - b ) } = \normalsize 1 , \quad x \neq 0 , b \neq \pm 3 a`$

### Exercises

|                                                                                                                                                                                                                             |                                                                                                                                                                                              |
|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| $`5 \Large \cdot \frac{a}{b} = \frac{5a}{b} ,\quad \normalsize b \not = 0`$                                                                                                                                                 | $`\Large \frac{m}{x} \cdot \frac{c}{a} = \frac{mc}{xa}, \quad \normalsize x,a \not = 0`$                                                                                                     |
| $`\Large\left( \frac{-3a}{b^2} \right) \cdot \left(\frac{-4c}{m} \right) = \frac{-12ac}{mb^2}, \quad\normalsize m,b \not = 0`$                                                                                              | $`\Large\frac{7a^2}{bc} \cdot \frac{3a}{5b^2} = \frac{21a^3}{5cb^3}, \quad\normalsize c,b \not = 0`$                                                                                         |
| $`\Large\frac{8x^2y^2}{5m^2bc} \cdot \frac{\left(-6xy^5 \right)}{7c^3m} = \frac{-48x^3y^7}{35c^4m^3b}, \quad \normalsize m,b,c \not = 0`$                                                                                   | $`\Large\frac{a}{b} : \frac{m}{k} = \frac{ak}{bm}, \quad \normalsize b,m,k \not = 0`$                                                                                                        |
| $`\Large\frac{6a}{b} : \normalsize 5 = \Large\frac{6a}{5b}, \quad \normalsize b \not = 0`$                                                                                                                                  | $`2c^2 : \Large\frac{b}{3m} = \frac{6c^2m}{b}, \quad \normalsize b,m \not = 0`$                                                                                                              |
| $`\Large\frac{4a^2}{3b^4} : \frac{b}{2a} = \frac{8a^3}{3b^5}, \quad \normalsize a,b \not = 0`$                                                                                                                              | $`\Large\frac{6m^2}{7c} \cdot \normalsize 14c = 12m^2, \quad c \not = 0`$                                                                                                                    |
| $`\Large\frac{5a^3}{b^2} \cdot \frac{2b^5}{15a^2} = \frac{2ab^3}{3}, \quad \normalsize a,b \not = 0`$                                                                                                                       | $`\Large\frac{-10m}{3c^2} \cdot \frac{18c^4}{-5m^2} =\frac{12c^2}{m},\quad \normalsize c,m \not = 0`$                                                                                        |
| $`\Large\frac{3x^2}{y} : \frac{b}{y^2} = \frac{3x^2y}{b}, \quad \normalsize b,y \not =0`$                                                                                                                                   | $`\Large\frac{(a + 2b)^2}{c^3} \cdot \frac{c^5}{a + 2b} = \normalsize c^2(a+2b), c \not =0, a\not=-2b`$                                                                                      |
| $`\Large\frac{mx - 3x}{14a^5} \cdot \frac{7a^3}{m^2 - 3m} = \frac{x}{2a^2m}, \quad \normalsize a,m \not = 0, m \not = 3`$                                                                                                   | $`\Large\frac{2a + 4b}{3x} : \frac {6a^2 + 12ab}{9x^4a} = \frac{(2a+4b) \;\cdot\; a \;\cdot\; 3x(3x^3)}{3x \;\cdot\; 3a(2a+4b)} = \normalsize x^3, \quad x,a \not = 0, \quad a \not = -2b`$  |
| $`\Large\frac{a^2 - 9}{b+2} \cdot \frac{3b + 6}{a + 3} = \frac{(a+3)(a-3)(3(b+2))}{(b+2)(a+3)}=\normalsize 3(a-3), \quad b \not = -2, \quad a \not = -3`$                                                                   | $`\Large\frac{a^2 + 4a + 4}{3x^3} \cdot \frac{x^4}{a^2 - 4} = \frac{(a+2)^2 x}{3(a+2)(a-2)} = \frac{x(a+2)}{3(a-2)}, \normalsize \quad a \not = \pm2, \quad x \not = 0`$                     |
| $`\Large\frac{a^2 + 3ab}{2b} \cdot \frac{2a - 6b}{a^2 - 9b^2} = \frac{2a(a+3b)(a-3b)}{2b(a+3b)(a-3b)} = \frac{a}{b}\normalsize \quad b \not = 0, \quad a \not = \pm3b`$                                                     | $`\Large\frac{m^2 + 5am}{3c} : \frac{2m^2 - 50a^2}{6m - 30a} = \frac{m(m+5a) \;\cdot\; 6(m-5a)}{3c \;\cdot\; 2(m^2-25a^2)} = \frac{m}{c} \quad \normalsize c \not =0, \quad m \not = \pm5a`$ |
| $`\Large\frac{6a^2 - 54}{6a - 12} : \frac{a^2 - 6a + 9}{5ax - 10x} = \frac{6(a^2 - 9)5x(a-2)}{6(a-2)(a-3)^2} = \frac{5x(a+3)}{a-3}, \quad \normalsize a \not = 3,2, \quad x \not=0`$                                        | $`\Large\frac{am + bm}{a^2 + 2ab + b^2} \cdot \frac{a^2 - b^2}{am - bm} = \frac{m(a+b)(a+b)(a-b)}{(a+b)^2m(a-b)} = \normalsize 1, \quad m \not =0, \quad a \not = \pm b`$                    |
| $`\Large\frac{a^2 + 3a + 2}{a^2 - 4} \cdot \frac{a - 2}{a + 2} = \frac{(a^2+a+2a+2)}{(a+2)^2} = \frac{a(a+1)+2(a+1)}{(a+2)^2}=\frac{a+1}{a+2}`$                                                                             | $`\Large\frac{m^2 - 6m + 5}{m^2 - 4} \cdot \frac{m + 2}{m - 5} = \frac{(m^2-m-5m+5)}{(m-2)(m-5)} = \frac{(m(m-1)-5(m-1))}{(m-2)(m-5)} = \frac{m-1}{m-2}`$                                    |
| $`\Large\frac{a^2 - 3a - 10}{a - 1} \cdot \frac{a^2 + 4a - 5}{a^2 - 25} = \frac{(a^2+2a-5a-10)(a^2-a+5a-5)}{(a-1)(a+5)(a-5)}=\frac{(a(a+2)-5(a+2))(a(a-1)+5(a-1))}{(a-1)(a+5)(a-5)}=\normalsize a+2, \quad a \not=1,\pm5 `$ | $`\Large\frac{a^2 + 6a + 5}{a^2 - 1} : \frac{1}{3a^2 - 8a + 5} = \frac{(a(a+1)+5(a+1))(3a(a-1)-5(a-1))}{(a+1)(a-1)}=\normalsize (a+5)(3a-5), \quad a\not=\pm1, \frac{5}{3}`$                              |


## Fractions Additions and Subtractions
### Examples
$`\Large\frac { x } { m a } + \frac { 5 } { a } - \frac { c } { m } = \frac { x + 5 m - c a } { m a } , \quad \normalsize m , a \neq 0`$

$`\Large\frac { 4 } { ( x - 1 ) ( 3 x - 4 ) } - \frac { 5 } { ( x + 3 ) ( 4 - 3 x ) } = \frac { 4 } { ( x - 1 ) ( 3 x - 4 ) } +
\frac { 5 } { ( x + 3 ) ( 3 x - 4 ) } = \frac { 4 ( x + 3 ) + 5 ( x - 1 ) } { ( x - 1 ) ( 3 x - 4 ) ( x + 3 ) } = \Large\frac { 9 x + 7 } { ( x - 1 ) ( 3 x - 4 ) ( x + 3 ) } , \quad \normalsize x \neq 1 , - 3 , \frac { 4 } { 3 }`$

### Exercises

|                                                                                                                                                         |                                                                                                                                |
|---------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------|
| $`\Large\frac{a}{3} + \frac{a}{2} = \frac{5a}{6}`$                                                                                                      | $`\Large\frac{2x}{5} - \frac{x}{2} + \frac{x}{3} = \frac{12x-15x+10x}{30}=\frac{7x}{30}`$                                      |
| $`\Large\frac{a}{m} + \frac{b}{m} = \frac{a+b}{m},\normalsize m\not=0`$                                                                                 | $`\Large\frac{a}{3x} - \frac{b}{6x} + \frac{5}{x} = \frac{2a-b+30}{6x}, \normalsize x\not=0`$                                  |
| $`\Large\frac{a}{b} + \frac{b}{a} = \frac{a^2+b^2}{ab}, \normalsize a,b\not=0`$                                                                         | $`\Large\frac{a}{c} - \normalsize 3 = \Large\frac{a-3c}{c}, \normalsize c \not=0`$                                             |
| $`m - \Large\frac{b - 2c}{a} = \frac{am-b-2c}{a}, \normalsize a\not=0`$                                                                                 | $`3a - \Large\frac{5 - 2ax}{x} + \normalsize 7 = \Large\frac{5ax+7x-5}{x}, \normalsize x\not=0`$                               |
| $`\Large\frac{m}{bc} - \frac{b}{c} = \frac{m-b^2}{bc}, \normalsize b,c\not=0`$                                                                          | $`\Large\frac{x}{2ab} - \frac{5}{ac} + \frac{m}{6a} = \frac{3cx-30b+mbc}{6abc}, \normalsize a,b,c \not=0`$                     |
| $`\Large\frac{m}{a^2} - \frac{b}{ac} = \frac{mc-ba}{a^2c}, \normalsize a,c\not=0`$                                                                      | $`\Large\frac{3}{2m^2c} - \frac{c}{m^2} + \frac{a}{3m} = \frac{9-6c^2+2amc}{6m^2c}, \normalsize m,c\not=0`$                    |
| $`\Large\frac{5}{3x^2m}+\frac{m}{6x}+\frac{c}{8m^2x^2}=\frac{40m+4m^3x^2+3c}{24m^2x^2},\normalsize x,m\not=0`$                                          | $`\Large\frac{x}{a^2c}-\frac{9}{4mc^2}-\frac{5c}{6a^2m^2}=\frac{12cm^2x-27a^2m-10c^3}{12a^2c^2m^2},\normalsize a,c,m \not=0`$  |
| $`\Large\frac{a - 1}{5} + \frac{2a + 1}{2} = \frac{12a+3}{10} `$                                                                                        | $`\Large\frac{4m - 3}{2} - \frac{m + 3}{4} = \frac{7m-9}{4}`$                                                                  |
| $`\Large\frac{3x + 2}{2} - \frac{1 - 2x}{5} = \frac{19x+8}{10}`$                                                                                        | $`\Large\frac{4}{x - 2} + \frac{5}{3x - 6} =\frac{17}{3(x-2)}, \normalsize x\not=2`$                                           |
| $`\Large\frac{5m}{2m-3}+\frac{9-m}{3-2m}=\frac{6m-9}{2m-3}=\frac{3(2m-3)}{2m-3}=\normalsize 3, m \not=\frac{3}{2}`$                                     | $`\Large\frac{3}{x}+\frac{5}{x+2}-\frac{8}{x-5}=\frac{3x^2-9x-30+5x^2-25x-8x^2-16x}{x(x+2)(x-5)}=\frac{-50x-30}{x(x+2)(x-5)}`$ |
| $`\Large\frac{2}{x}+\frac{3}{x-1}-\frac{6}{5x-5}=\frac{10x-10+15x-6x}{5x(x-1)}=\frac{19x-19}{5x(x-1)},\normalsize x\not=0,1`$                           | $`\Large\frac{5x}{x-2}+\frac{x+1}{2x}-\frac{3}{2-x}=\frac{11x^2+5x-2}{2x(x-2)}`$                                               |
| $`\Large\frac{3}{(m-2)(m+3)} + \frac{5}{(m+3)(m+7)} = \frac{8m+11}{(m-2)(m+3)(m+7)},\normalsize m\not=2,-3,-7`$                                         | $`\Large\frac{2x}{(x+1)(x-2)}-\frac{6}{(x+1)^2} = \frac{2x^2-4x+12}{(x-2)(x+1)^2}`$                                            |
| $`\Large\frac{3x}{x^2-9}+\frac{2}{x-3}+\frac{6}{x}=\frac{11x^2+6x-54}{x(x+3)(x-3)}, \normalsize x\not=\pm0,3`$                                          | $`\Large\frac{10}{(x-5)^2}+\frac{2}{x^2-25}+\frac{1}{x+5}= \frac{x^2+2x+65}{(x+5)(x-5)(x-5)},\normalsize x\not=\pm5`$          |
| $`\Large\frac{5}{a+3}-\frac{a-12}{a^2+3a}=\frac{4a+12}{a(a+3)}=\frac{4(a+3)}{a(a+3)}=\frac{4}{a},\normalsize a\not=0,-3`$                               | $`\Large\frac{7}{a+5}-\frac{3}{a-5}+\frac{2(a+10)}{a^2-25}=\frac{6a-30}{a^2-25}=\frac{6}{a+5},\normalsize a\not=\pm5`$         |
| $`\Large\frac{2x^2-3x+9}{4x^2-9}-\frac{x}{2x-3}=\frac{-(6x-9)}{4x^2-9}=\frac{-3(2x-3)}{(2x-3)(2x+3)}=\frac{-3}{2x+3},\normalsize x\not=\pm\frac{3}{2}`$ | $`\Large\frac{m-6}{m+2}+\frac{5}{m}+\frac{m-14}{m(m+2)}=\frac{m^2-4}{m(m+2)}=\frac{m-2}{m},\normalsize m\not=0,-2`$            |
| $`\Large\frac{16x}{x^2-16}+\frac{2x}{x+4}-\frac{x+4}{x-4}=\frac{x^2-16}{x^2-16}=\normalsize 1 x\not=\pm4`$                                              | $`\Large\frac{2}{a-2}-\frac{3}{a(a+2)}-\frac{2(a-1)}{a^2-4}=\frac{3a+6}{a(a^2-4)}=\frac{3(a+2)}{a(a+2)(a-2)}=\frac{3}{a(a-2)}\normalsize a\not=0,\pm2`$ |


## Linear Equations with one unknown
### Examples
$`10 \left( \frac { x } { 5 } + 1 \right) - 4 \left( 5 - \frac { x } { 4 } \right) = 3 x - 14 \Rightarrow \frac { 10 x } { 5 } + 10 - 20 + \frac { 4 x } { 4 } = 3 x - 14 \Rightarrow 2 x + 10 - 20 + x = 3 x - 14
0 \cdot x = - 4 \Rightarrow n o \text { solution. }`$

$`\frac { 3 } { 10 } - \frac { 7 } { 5 x } = \frac { 1 } { 4 } - \frac { 3 } { 2 x } / 20 x \Rightarrow 6 x - 28 = 5 x - 30 \Rightarrow x = - 2`$

### Exercises

|                                                                                                                                                                                                                                                                                 |
|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| $`\begin{aligned} 3x + 5 &= 17 \\ 3x &= 12 \\ x &= 4\end{aligned}`$                                                                                                                                                                                                             |
| $`\begin{aligned} \frac{5x}{6} &= 60 \\ 5x &= 360 \\ x &= 72 \end{aligned}`$                                                                                                                                                                                                    |
| $`\begin{aligned} x - 11x - 20 - 5x &= 55 \\ -15x -20 &= 55 \\ -3x-4 &= 11 \\ -3x &= 15 \\ x &= -5 \end{aligned}`$                                                                                                                                                              |
| $`\begin{aligned} 5(x - 4) - 3(2x - 8) &= 4x - 16 \\ 5x-20-6x+24 &= 4x -16 \\ -5x &= -20 \\ x &= 4 \end{aligned}`$                                                                                                                                                              |
| $`\begin{aligned} 3(2x - 9) - 7(x - 8) &= 2x - 1 \\ 6x - 27 - 7x + 56 &= 2x -1 \\ -x -2x &= -1 -29 \\ x &= 10 \end{aligned}`$                                                                                                                                                   |
| $`\begin{aligned} 5(x + 4) + 4 - x &= (2x + 13) \cdot 2 \\ 5x +20 +4 -x &= 4x +16 \\ 4x -4x &= -8 \Rightarrow \text{no solution} \end{aligned}`$                                                                                                                                |
| $`\begin{aligned} 12 \left( \frac{x}{2} - 5 \right) - 2(x - 7) &= 26 \\ 6x - 60 - 2x + 14 &= 26 \\ 4x &= 26+46 \\ x &= 18 \end{aligned}`$                                                                                                                                       |
| $`\begin{aligned} \frac{2x}{3} - 3x + \frac{5}{4} &= \frac{x}{6} \\ 8x - 36x + 15 &= 2x \\ 30x &= 15 \\ x &= \frac{1}{2} \end{aligned}`$                                                                                                                                        |
| $`\begin{aligned} \frac{x+ 4}{2} + \frac{5x}{4} + 5 &= 0 \\ 2x + 8 + 5x + 20 &= 0 \\ 7x &= -28 \\ x &= -4 \end{aligned}`$                                                                                                                                                       |
| $`\begin{aligned} \frac{3x - 8}{4} - \frac{x + 13}{5} &= x - 10 \\ 15x - 40 -4x - 52 &= 20x - 200 \\ 9x &= 108 \\ x &= 12 \end{aligned}`$                                                                                                                                       |
| $`\begin{aligned} \frac{1}{6}(x+5)-\frac{1}{4}(x-5)-2 &= \frac{1}{6}(3x-1)-\frac{x}{3}+\frac{1}{4}(1-x) \\ \frac{1}{6}(x+5-3x+1)-\frac{1}{4}(x-5+1-x) &= 2-\frac{2x}{6} \\ \frac{-2x+6}{6}+1 +\frac{2x}{6} &= 2 \\ 2 &= 2 \Rightarrow \text{all solutions work} \end{aligned}`$ |
| $`\begin{aligned} \frac{3x-8}{5} - \left(7- \frac{2x+3}{2} \right) &= \frac{x+4}{4} \\ 12x-32 + 20x -110 &= 5x+20 \\ 27x &= 162 \\ x &= 6 \end{aligned}`$                                                                                                                                                                     |
| $`\begin{aligned} \frac{1-2x}{3} - \frac{2x+14}{6} &= 7x - \frac{11x-16}{2} \\ 2-4x-2x-14 &= 42x-33x+48 \\ 15x &= -60 \\ 3x &= -12 \\ x &= -4 \end{aligned}`$                                                                                                                                                                               |
| $`\begin{aligned} \frac{3}{x} - \frac{5}{2x} &= 1 \\ \frac{6-5}{2x} &= 1 \\ x &= \frac{1}{2} \end{aligned}`$                                                                                                                                                                                                               |
| $`\begin{aligned} \frac{x+1}{x-3} &= 1 \frac{4}{5} \\ 5\frac{x+1}{x-3} &= 9 \\ 5x+5 &= 9x-27 \\ 4x &= 32 \\ x &= 8 \end{aligned}`$                                                                                                                                              |


## Quadratic equations with algebraic fractions
### Examples
$`\Large\frac { x + 1 } { x ^ { 2 } - 6 x + 8 } + \frac { 6 x } { x ^ { 2 } + 4 x - 12 } = \normalsize 0 \Large \Rightarrow \frac { x + 1 } { ( x - 4 ) ( x - 2 ) } + \frac { 6 x } { ( x - 2 ) ( x + 6 ) } = \normalsize 0 / ( x - 4 ) ( x - 2 ) ( x + 6 )( x + 1 ) ( x + 6 ) + 6 x ( x - 4 ) = 0 \Rightarrow x ^ { 2 } + 7 x + 6 + 6 x ^ { 2 } - 24 x = 0 \Rightarrow 7 x ^ { 2 } - 17 x + 6 = 0 \Rightarrow x _ { 1 } = 2 , \quad x _ { 2 } = \frac { 3 } { 7 }`$

$`\Large\frac { 1 } { x ^ { 2 } - 3 x } = \frac { 3 } { x ^ { 2 } + 2 x - 15 } - \frac { 1 } { 2 x + 10 } \Rightarrow \frac { 1 } { x ( x - 3 ) } = \frac { 3 } { ( x - 3 ) ( x + 5 ) } - \frac { 1 } { 2 ( x + 5 ) } / \normalsize 2 x ( x - 3 ) ( x + 5 )2 ( x + 5 ) = 6 x - x ( x - 3 ) \Rightarrow x ^ { 2 } - 7 x + 10 = 0 \Rightarrow x _ { 1 } = 5 , x _ { 2 } = 2`$

## Exercises

|                                                                                                                                                                                                                                                                                                               |
|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| $`\begin{aligned}\frac{x}{x-1}+\frac{1}{1+x}-\frac{x^2+1}{1-x^2} &= \frac{2x^2}{x-1} \\ \frac{2x^2+2x}{x^2-1} &= \frac{2x^3+2x^2}{x^2-1} \\ \frac{2x(x^2+1)}{x^2-1} &=0 \Rightarrow x^2+1>0, \; 2x=0 \rightarrow x=0 \\ x_1=0, x\not=\pm1 \end{aligned}`$                                                     |
| $`\begin{aligned}\frac{x}{8}-\frac{2}{x} &= \frac{4}{x}-\frac{x}{2}+4 \frac{1}{4} \\ \frac{x^2-16 -32 + 4x^2 -34x}{8x} &= 0 \\ \frac{5x^2 -34x -48}{8x} &= 0\\ \frac{34\pm\sqrt{34^2 + 20 \cdot 48 }}{10} = \frac{34 \pm\sqrt{2116}}{10} = \frac{34\pm 46}{10} \\ x_1=8, \; x_2=\frac{-6}{5}  \end{aligned}`$ |
| $`\begin{aligned}\frac{7}{x+1}-\frac{x+4}{2-2x} &= \frac{3x^2-38}{x^2-1} \\ \frac{14x-14+x^2+x+4x+4-6x^2+76}{2(x^2-1)} &=0 \\ \frac{-5x^2+19x+66}{2(x^2-1)} &=0 \\ \frac{-19 \pm \sqrt{19^2+1320}}{-10} = \frac{-19 \pm 41}{-10} \\ x_1=6, \; x_2=2.2 \end{aligned}`$                                         |
| $`\begin{aligned}\frac{1}{x-1}-\frac{x}{x^2+1} &= \frac{x^2+x}{(x^2+1)(x-1)} \\ \frac{-(x^2-1)}{(x^2+1)(x-1)} &=0  \\ \frac{x+1}{x^2+1} &=0 \\ x_1=-1 \end{aligned}`$                                                                                                                                         |
| $`\begin{aligned}\frac{16}{x^2-9} &= \frac{x-1}{x+3}-\frac{4-x}{x-3} \\ \frac{16}{x^2-9} &= \frac{x^2-4x+3 +x^2-x-12}{x^2-9} \\ \frac{2x^2-5x-25}{x^2-9} &= 0 \\ \frac{5 \pm \sqrt{25+200}}{4} = \frac{5 \pm 15}{4} \\ x_1=5, \; x_2=-\frac{5}{2} \end{aligned}`$                                             |
| $`\begin{aligned}\frac{1}{x+1}+\frac{1}{x-1}-\frac{2}{x^2-1}+\frac{1}{x^2+2x+1} &= 0 \\ \\ x^2+2x+1=(x+1)^2, \quad x^2-1=(x+1)(x-1) \\ \\ \frac{x^2-1+x^2+2x+1-2x-2+x-1}{(x+1)(x+1)(x-1)} &=0 \\ 2x^2+x-3 &=0, x\not=\pm1 \\ \\ \frac{-1\pm \sqrt{1+24}}{4}=\frac{-1\pm5}{4} \\ x=1.5 \end{aligned}`$         |
| $`\begin{aligned}\frac{x-1}{x^2-7x+12} &= \frac{2}{x^2+3x-18} \\ \\ x^2-7x+12 = x^2-4x-3x+12 = x(x-4)-3(x-4) = (x-4)(x-3) \\ x^2+3x-18 = x^2-3x+6x-18 = x(x-3)+6(x-3) = (x+6)(x-3) \\ \\ \frac{x-1}{(x-4)(x-3)}&=\frac{2}{(x-3)(x+6)} \\ \frac{x^2+6x-x-6-2x+8}{(x-4)(x-3)(x+6)} &=0 \\ x^2+3x+2 &=0 \\ \\ frac{-3\pm\sqrt{9-8}}{2} = \frac{-3\pm1}{2} \\ x_1=-2, \; x_2=-1 \end{aligned}`$ |
| $`\begin{aligned}\frac{x}{x-6}+ \frac{2}{x-4} &= \frac{3}{x^2-10x+24} \end{aligned}`$                                                                                                                                                                                                                         |
| $`\begin{aligned}\frac{2(x+14)}{x^2-5x-14}- \frac{2x-1}{x-7} &= \frac{2}{x+2} \end{aligned}`$                                                                                                                                                                                                                 |
| $`\begin{aligned}\frac{3}{x^2-3x-4}- \frac{x}{x^{2}-16} &= \frac{-6}{x^2+5x+4} \end{aligned}`$                                                                                                                                                                                                                |
| $`\begin{aligned}\frac{2x}{x^2+6x-7}- \frac{1}{x^2+2x-3} &= \frac{2-x}{x^2+10x+21} \end{aligned}`$                                                                                                                                                                                                            |
| $`\begin{aligned}\frac{3}{x^2+2x-8}- \frac{4}{x^2+4x-12} &= \frac{x-4}{x^2+10x+24} \end{aligned}`$                                                                                                                                                                                                            |
| $`\begin{aligned}\frac{x}{x-3}- \frac{4}{x+2} &= \frac{4x+3}{x^2-x-6} \end{aligned}`$                                                                                                                                                                                                                         |


## Linear Equations with two unknown
### Examples
$`\left\{ \begin{array} { l } { \frac { x } { 5 } + \frac { y } { 2 } = 8 / .10 } \\ { \frac { x } { 10 } - \frac { y } { 8 } = 1 / .40 } \end{array} \right. \Rightarrow \left\{ \begin{array} { l } { 2 x + 5 y = 80 } \\ { 4 x - 5 y = 40 } \end{array} \right. \Rightarrow \{ 6 x = 120 \Rightarrow \left\{ \begin{array} { l } { x = 20 } \\ { y = 8 } \end{array} \right.`$

### Exercises
#### Equations

|                                                                                                                                                                                                                                                      |
|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| $`\left\{\begin{aligned}2y-3x &= 11 \\ y &= 10 \end{aligned}\right. \Rightarrow \left\{\begin{aligned} x &= 3 \\ y &=10 \end{aligned}\right.`$                                                                                                       |
| $`\left\{\begin{aligned}x+y &= 8 \\ x-y &= 2 \end{aligned}\right.\Rightarrow\left\{\begin{aligned} x+y &= 8 \\ 2x &= 10 \end{aligned}\right.\Rightarrow\left\{\begin{aligned} x &= 5 \\ y &= 3 \end{aligned}\right.`$                                |
| $`\left\{\begin{aligned}3x-y&=26 \\ 2x+3y&=-1 \end{aligned}\right.\Rightarrow\left\{\begin{aligned} 9x-3y&=78 \\ 2x+3y&=-1 \end{aligned}\right.\Rightarrow\left\{11x=77\right.\Rightarrow\left\{\begin{aligned} x&=7 \\y&=-5 \end{aligned}\right.`$  |
| $`\left\{\begin{aligned}3x-5y&=23 \\ x+2y&=15 \end{aligned}\right.\Rightarrow\left\{\begin{aligned}3x-5y&=23 \\ 3x+6y&=45 \end{aligned}\right.\Rightarrow\left\{11y=22 \right.\Rightarrow\left\{\begin{aligned} x&=11 \\ y&=2 \end{aligned}\right.`$ |
| $`\left\{\begin{aligned}4x+3y&=28 \\ 3x-2y&=38\end{aligned}\right.\Rightarrow\left\{\begin{aligned}8x+6y&=56 \\ 9x-6y&=114\end{aligned}\right.\Rightarrow\left\{17x=170\right.\Rightarrow\left\{\begin{aligned}x&=10 \\ y&=-4 \end{aligned}\right.`$ |
| $`\left\{\begin{aligned}2(x-5)-3(8-y)&=3x+1 \\ 4(y-8)+5(3x+y)&=3-2y \end{aligned}\right.`$                                                                                                                                      |
| $`\left\{\begin{aligned}\frac{3x + 1}{4} + \frac{y - 1}{8} &= 3 \\ \frac{2x - 1}{5} + \frac{3y + 5}{20} &= 2 \end{aligned} \right.`$                                                                                                                 |
| $`\left\{\begin{aligned}\frac{x}{2} + \frac{y}{4} &= -3 \\ \frac{x + y}{9} - \frac{x - y}{3} &= \frac{x}{3} -1 \end{aligned} \right.`$                                                                                                               |
| $`\left\{\begin{aligned}\frac{x + y}{x - y} &= 2 \\ \frac{2x + y}{x + y - 1} &= 2 \end{aligned} \right.`$                                                                                                                                            |
| $`\left\{\begin{aligned}\frac{x + 2y}{4y - x} &= \frac{1}{8} \\ 5(x + 3y) &= 7y + 4 \end{aligned} \right.`$                                                                                                                                          |

#### 2nd degree Equations

|                                                                                                                                |
|--------------------------------------------------------------------------------------------------------------------------------|
| $`\left\{ \begin{aligned} x + y &= 5 \Rightarrow x &= 5 - y \\ x^2 - y^2  &= 5 \end{aligned} \right.`$                         |
| $`\left\{ \begin{aligned} x^2 + y^2 &= 74 \\ x - y &= 2 \Rightarrow x &= y + 2 \end{aligned} \right.`$                         |
| $`\left\{ \begin{aligned} x - y &= 1 \Rightarrow x &= 1 + y \\ x^2 + xy + y^2 &= 37 \end{aligned} \right.`$                    |
| $`\left\{ \begin{aligned} \frac{x}{y} &= \frac{2}{3} \Rightarrow x &= \frac{2}{3}y \\ x^2 + y^2 &= 208 \end{aligned} \right.`$ |
| $`\left\{ \begin{aligned} 5x - 2y &= 3 \\ xy &= -0.2 \Rightarrow x &= \frac{-0.2}{y} \end{aligned} \right.`$                   |
| $`\left\{ \begin{aligned} 10x + 3y &= 13 \\ xy &= -1 \Rightarrow x &= \frac{-1}{y} \end{aligned} \right.`$                     |
| $`\left\{ \begin{aligned} x - y &= 6 \Rightarrow x &= y + 6 \\ (x - 5)^2 + (y + 1)^2 &= 18 \end{aligned} \right.`$             |
| $`\left\{ \begin{aligned} x^2 + y^2 + 3y &= -1 \\ x^2 + y^2 + y &= 3 \end{aligned} \right.`$                                  |

## Exponents and Roots

### Reminder
$`a ^ { n } \cdot a ^ { m } = a ^ { n + m } , \quad \frac { a ^ { n } } { a ^ { m } } = a ^ { n - m } , \quad a ^ { 0 } = 1 , \quad a ^ { - n } = \frac { 1 } { a ^ { n } } , \quad \left( a ^ { n } \right) ^ { n } = a ^ { n n }`$

$`( a \cdot b ) ^ { m } = a ^ { m } \cdot a ^ { m } , \quad \left( \frac { a } { b } \right) ^ { m } = \frac { a ^ { m } } { b ^ { m } } , \quad \left( \frac { a } { b } \right) ^ { - n } = \left( \frac { b } { a } \right) ^ { n }`$

### Examples
$`\left( - 3 x ^ { 2 } y z ^ { - 3 } \right) : \left( 2 x ^ { - 1 } y ^ { 3 } z ^ { 5 } \right) : \left( 9 x ^ { - 2 } y ^ { - 1 } z ^ { 2 } \right) = \frac { - 3 x ^ { 2 } y z ^ { - 3 } } { 2 x ^ { - 1 } y ^ { 3 } z ^ { 5 } } \cdot \frac { 1 } { 9 x ^ { - 2 } y ^ { - 1 } z ^ { 2 } } = \frac { - 3 x ^ { 3 } y ^ { - 2 } z ^ { - 8 } } { 18 x ^ { - 2 } y ^ { - 1 } z ^ { 2 } } = - \frac { x ^ { 5 } } { 6 y z ^ { 10 } }`$

### Exercises

|                                                                                                                                         |                                                                                                                       |
|-----------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------|
| $`a ^ { 4 } \cdot a ^ { - 3 } =`$                                                                                                       | $`2 c ^ { - 6 } : c ^ { - 7 }`$                                                                                       |
| $`5 a ^ { - 3 } : \left( a ^ { - 4 } b ^ { 3 } \right) =`$                                                                              | $`\left( x ^ { 2 } y ^ { - 3 } \right) \cdot \left( 2 x ^ { - 3 } y ^ { 4 } \right) =`$                               |
| $`( a + b ) ^ { - 2 } \cdot ( a + b ) ^ { 3 } =`$                                                                                       | $`( x - y ) ^ { - 3 } \cdot ( y - x ) ^ { 4 } =`$                                                                     |
| $`\frac { a ^ { 5 } b ^ { - 6 } } { a ^ { 4 } b ^ { - 7 } } =`$                                                                         | $`\frac { x ^ { - 3 } y ^ { 5 } z ^ { - 8 } } { x ^ { - 4 } y ^ { - 1 } z } =`$                                       |
| $`\left( \frac{a}{b} + \frac{b}{a} \right)^{-1} \cdot \left(2a^{3}b^{-2} \right) : \left(a^{2} + b^{2} \right)^{-1} \cdot (3ab)^{0} =`$ | $`\frac{9}{2a^{7}b^{-2}c^{-3}} : \left( \frac{3}{4}a^{-8}b^{3}c^{-1} \right) =`$                                      |
| $`\frac { 8 } { 9 } x ^ { - 6 } y ^ { 7 } z ^ { - 5 } : \frac { 16 } { x ^ { 5 } y ^ { - 8 } z ^ { 6 } } =`$                            | $`\left( 2 x ^ { - 3 } \right) ^ { 2 } : \left( 3 x ^ { 3 } \right) ^ { - 2 } =`$                                     |
| $`\left( 8 y ^ { 4 } \right) ^ { - 3 } : \left( \frac { y ^ { - 3 } } { 2 } \right) ^ { 5 } =`$                                         | $`\left( a ^ { 2 } b \right) ^ { - 1 } \cdot a ^ { 3 } b ^ { 2 } =`$                                                  |
| $`\left(a^{-3}b^{2} \right)^{2} \cdot \left(a^{-2}b^{3} \right)^{-3} =`$                                                                | $`\left(x^{2}y^{-4} \right)^{-2} \cdot \left(2xy^{-2} \right)^{5} =`$                                                 |
| $`\left(5xy^{-3} \right)^{-1} \cdot 15xy^{-2} =`$                                                                                       | $`\left(3^{-1}ab^{-5} \right)^{-2} : \left(6a^{-3}b^{9} \right) =`$                                                   |
| $`\left(2a^{-3}b^{-5} \right)^{-2} : \left(a^{2}b^{4} \right)^{3} =`$                                                                   | $`\left(1.2uv^{-2} \right)^{-2} : \left(5u^{-2}v \right)^{2} =`$                                                      |
| $`\left(\frac { a ^ { - 1 } b ^ { 3 } } { c ^ { 4 } } \right) ^ { - 1 } =`$                                                             | $`\left( \frac { x } { y ^ { 2 } z ^ { - 5 } } \right) ^ { - 2 } =`$                                                  |
| $`\left(\frac { x + x ^ { - 1 } } { x ^ { - 1 } } \right) ^ { - 1 } =`$                                                                 | $`b ^ { - 2 } \left( 1 + a b ^ { - 1 } \right) ^ { - 2 } =`$                                                          |
| $`\left(\frac{x^{-2}y^{3}}{3} \right)^{-3} \cdot \left( \frac{9}{x^{-3}y^{4}} \right)^{-2} =`$                                          | $`\left(\frac{1}{xy^{-1}} \right)^{6} : \left(\frac{y^{-2}}{x^{-1}} \right)^{-3} =`$                                  |
| $`\left(\frac{u^{-3}v}{4w^{-3}} \right)^{-1} : \left(\frac{2u^{-1}v}{w^{-1}} \right)^{-3} =`$                                           | $`\left(\frac{1}{x+1} \right)^{7} \cdot \left(\frac{1}{x-1} \right)^{-9} \cdot \left(\frac{x-1}{x+1} \right)^{-8} =`$ |
| $`\frac{\left(2^{3} \right)^{-12} \cdot \left(3 \cdot 2^{2} \right)^{19}}{\left(3^{2} \right)^{23} : 3^{28}} =`$                        | $`\frac{3^{-5} \cdot \left(3^{2} \right)^{2}}{3^{3} \cdot 3} =`$                                                      |
|                                                                                                                                         |                                                                                                                       |


## Exponential Equations
### Rules
$`a ^ { f ( x ) } = a ^ { g ( x ) } \Leftrightarrow f ( x ) = g ( x ) , \quad e ^ { f ( x ) } = e ^ { g ( x ) } \Leftrightarrow f ( x ) = g ( x )`$

### Examples
$`e ^ { 4 x } - \frac { 1 } { e ^ { 2 } } = 0 \Rightarrow e ^ { 4 x } = \frac { 1 } { e ^ { 2 } } \Rightarrow e ^ { 4 x } = e ^ { - 2 } \Rightarrow 4 x = - 2 \Rightarrow x = - \frac { 1 } { 2 }`$

$`16 ^ { x + 1 } = 8 ^ { 2 x } \Rightarrow \left( 2 ^ { 4 } \right) ^ { x + 1 } = \left( 2 ^ { 3 } \right) ^ { 2 x } \Rightarrow 2 ^ { 4 ( x + 1 ) } \Rightarrow 4 ( x + 1 ) = 6 x \Rightarrow 4 x + 4 = 6 x \Rightarrow x = 2`$

$`5 \cdot 9 ^ { 3 x - 6 } = 5 \Rightarrow 9 ^ { 3 x - 6 } = 1 \Rightarrow 9 ^ { 3 x - 6 } = 9 ^ { 0 } \Rightarrow 3 x - 6 = 0 \Rightarrow x = 2`$

### Exercises

|                                         |                                                                          |                                                                             |
|-----------------------------------------|--------------------------------------------------------------------------|-----------------------------------------------------------------------------|
| $`5 ^ { x } - 5 ^ { 2 } = 0`$           | $`e ^ { x } = e`$                                                        | $`3 ^ { 4 x } = 9 ^ { 4 }`$                                                 |
| $`e ^ { x } = 1 , \quad e ^ { 0 } = 1`$ | $`e ^ { - x } - e ^ { 4 } = 0`$                                          | $`7 ^ { x } - \frac { 1 } { 7 ^ { 3 } } = 0`$                               |
| $`e ^ { x } = \frac { 1 } { e }`$       | $`4 ^ { x + 1 } - 4 ^ { 5 - x } = 0`$                                    | $`e ^ { x } = \sqrt { e } , \quad \sqrt { e } = e ^ { \frac { 1 } { 2 } }`$ |
| $`2 ^ { 5 x + 2 } = 8 ^ { 4 }`$         | $`e ^ { x ^ { 2 } - 2 x } - 1 = 0`$                                      | $`e ^ { - x } - \frac { 1 } { e } = 0`$                                     |
| $`e ^ { x } = 0`$                       | $`( 5 \sqrt { 5 } ) ^ { x } = \left( \frac { 1 } { 5 } \right) ^ { 3 }`$ | $`3 ^ { 2 x } = \frac { 1 } { \sqrt { 3 } }`$                               |
| $`7 \cdot e ^ { 3 x + 3 } = 7`$         | $`5 \cdot 6 ^ { 4 x - 6 } + 2 = 7`$                                      | $`x \cdot e ^ { x } = 0`$                                                   |


## Exercises with common factor
### Examples
$`7 ^ { x } \cdot ( 2 x - 3 ) - 5 \cdot 7 ^ { x } = 0 \Rightarrow 7 ^ { x } ( 2 x - 3 - 5 ) = 0 \Rightarrow 7 ^ { x } \neq 0 , \quad 2 x = 8 \Rightarrow x = 4`$

$`e ^ { 2 x } - 2 x \cdot e ^ { 2 x } = 7 \cdot e ^ { 2 x } \Rightarrow e ^ { 2 x } - 2 x \cdot e ^ { 2 x } - 7 \cdot e ^ { 2 x } ( 1 - 2 x - 7 ) = 0 \Rightarrow e ^ { 2 x } \neq 0 , \quad x = - 3`$

### Exercises

|                                                                       |                                                                         |
|-----------------------------------------------------------------------|-------------------------------------------------------------------------|
| $`e ^ { x } ( x - 3 ) = 0`$                                           | $`3 ^ { x } ( x + 1 ) = 0`$                                             |
| $`5 ^ { x } ( 3 x - 6 ) = 0`$                                         | $`7 ^ { x } + x \cdot 7 ^ { x } = 0`$                                   |
| $`x \cdot e ^ { x } + 4 e ^ { x } = 0`$                               | $`e ^ { x } + 5 \cdot e ^ { x } = x \cdot e ^ { x }`$                   |
| $`x \cdot e ^ { x } - e ^ { x } = 0`$                                 | $`x \cdot e ^ { 2 x } - 3 e ^ { 2 x } = 0`$                             |
| $`x \cdot 6 ^ { - 2 x } + 4 \cdot 6 ^ { - 2 x } = 0`$                 | $`2 e ^ { 3 x } + x e ^ { 3 x } = 0`$                                   |
| $`4 x \cdot 2 ^ { - 2 x } - 2 \cdot 2 ^ { - 2 x } = 0`$               | $`3 e ^ { - x } - 2 x \cdot e ^ { - x } = 0`$                           |
| $`( x + 1 ) \cdot e ^ { x } + 3 e ^ { x } = 0`$                       | $`- 7 ^ { - 2 x } + ( x - 5 ) \cdot 7 ^ { - 2 x } = 0`$                 |
| $`2 \cdot 8 ^ { x } + ( 7 - 3 x ) \cdot 8 ^ { x } = 0`$               | $`e ^ { x } - e ^ { x } \cdot ( 2 x + 3 ) = 0`$                         |
| $`e ^ { x } - e = 0`$                                                 | $`e ^ { 2 x - 1 } - e ^ { 2 } = 0`$                                     |
| $`e ^ { 3 x } - e ^ { x } = 0`$                                       | $`6 ^ { 2 x - 1 } = 6 ^ { 5 }`$                                         |
| $`e ^ { x } - \frac { 1 } { e ^ { 3 } } = 0`$                         | $`e ^ { x } = e ^ { 2 - x }`$                                           |
| $`2 ^ { 2 x } = 2 ^ { - x - 3 }`$                                     | $`e ^ { 2 x - 1 } - e ^ { 5 - x } = 0`$                                 |
| $`x ^ { 2 } \cdot 5 ^ { x } - 9 \cdot 5 ^ { x } = 0`$                 | $`x ^ { 2 } \cdot 8 ^ { x } - 3 x \cdot 8 ^ { x } = 0`$                 |
| $`6 ^ { x ^ { 2 } - 7 x + 10 } = 1`$                                  | $`e ^ { x ^ { 2 } - 3 x - 4 } + 2 = 3`$                                 |
| $`x ^ { 2 } \cdot e ^ { x } + 4 x \cdot e ^ { x } - 5 e ^ { x } = 0`$ | $`3 \cdot 3 ^ { x } + x \cdot 3 ^ { x } = 0`$                           |
| $`7 ^ { 2 x } + 2 x \cdot 7 ^ { 2 x } = 0`$                           | $`x ^ { 2 } e ^ { x } - 2 x \cdot e ^ { x } = 0`$                       |
| $`e ^ { x } \cdot ( x + 1 ) + e ^ { x } = 0`$                         | $`x ^ { 2 } \cdot 5 ^ { - 3 x } - ( 2 x + 3 ) \cdot 5 ^ { - 3 x } = 0`$ |


## Exercises with quadratic equations
### Examples

$`\left\vert \begin{array} { l } { e ^ { 2 x } - 3 e ^ { x } - 10 = 0 ,  e ^ { x } = t , \quad t ^ { 2 } = \left( e ^ { x } \right) ^ { 2 } = e ^ { 2 x } \Rightarrow t ^ { 2 } - 3 t - 10 = 0 \Rightarrow t _ { 1 } = - 2 , \quad t _ { 2 } = 5}\\
{e ^ { x } = - 2 , \text { imposible!!! } \qquad e ^ { x } = 5 \Rightarrow x = \ln 5}\end{array} \right.`$

$`\left\vert \begin{array} { l } { 2 \cdot 3 ^ { 2 x } - 2 \cdot 3 ^ { x } - 4 = 0 , 3 ^ { x } = t , \quad t ^ { 2 } = \left( 3 ^ { x } \right) ^ { 2 } = 3 ^ { 2 x } \Rightarrow 2 t ^ { 2 } - 2 t - 4 = 0 \Rightarrow t _ { 1 } = 2 , \quad t _ { 2 } = - 1}\\
3 ^ { x } = - 1 , \text { imposible!!! } \qquad 3 ^ { x } = 2 \Rightarrow x = \log _ { 3 } 2 \end{array}\right.`$

### Exercises

|                                              |                                                     |
|----------------------------------------------|-----------------------------------------------------|
| $`e ^ { 2 x } - 6 e ^ { x } + 5 = 0`$        | $`7 ^ { 2 x } - 4 \cdot 7 ^ { x } - 12 = 0`$        |
| $`e ^ { 2 x } - 3 e ^ { x } = 0`$            | $`2 \cdot 5 ^ { 2 x } - 5 \cdot 5 ^ { x } + 2 = 0`$ |
| $`3 ^ { 2 x } + 4 \cdot 3 ^ { x } - 12 = 0`$ | $`e ^ { 2 x } = 4 - 3 e ^ { x }`$                   |


## Logarithms
### Rule
$`\log _ { a } x = b \Leftrightarrow a ^ { b } = x \enspace	with \enspace	 \ln a = \log _ { e } a , a \neq 1 , \quad a , x > 0`$

### Examples
$`\log _ { \frac { 1 } { 9 } } 27 = a \Rightarrow \left( \frac { 1 } { 9 } \right) ^ { a } = 27 \Rightarrow \left( \frac { 1 } { 3 ^ { 2 } } \right) ^ { a } = 3 ^ { 3 } \Rightarrow \left( 3 ^ { - 2 } \right) ^ { a } = 3 ^ { 3 } \Rightarrow 3 ^ { - 2 a } = 3 ^ { 3 } \Rightarrow - 2 a = 3 \Rightarrow a = - 1.5`$
$`( \ln \sqrt { e } ) ^ { 2 } = a \Rightarrow \ln \sqrt { e } = \sqrt { a } \Rightarrow \sqrt { e } = e ^ { \sqrt { a } } \Rightarrow e ^ { \frac { 1 } { 2 } } = e ^ { \sqrt { a } } \Rightarrow \sqrt { a } = \frac { 1 } { 2 } \Rightarrow a = \frac { 1 } { 4 }`$

### Exercises

|                                                      |                                                             |                                                       |
|------------------------------------------------------|-------------------------------------------------------------|-------------------------------------------------------|
| $`\log _ { \sqrt { 2 } } 8 =`$                       | $`\log _ { \frac { 1 } { 2 } } 32 =`$                       | $`\log _ { 0,2 } 25 =`$                               |
| $`\log _ { 16 } 0.25 =`$                             | $`\left( \log _ { \frac { 1 } { 4 } } 4 \right) ^ { 2 } =`$ | $`\left( \log _ { \sqrt { 2 } } 8 \right) ^ { 2 } =`$ |
| $`\left( \log _ { 0,1 } 1000 \right) ^ { 2 } =`$     | $`\log _ { 4 } 16 =`$                                       | $`\log _ { 16 } 64 =`$                                |
| $`\log _ { 8 } 128 =`$                               | $`\log _ { 81 } 27 =`$                                      | $`\log _ { 25 } 0.2 =`$                               |
| $`\log _ { 27 } \left( \frac { 1 } { 9 } \right) =`$ | $`\log _ { 8 } 2 ^ { - 5 } =`$                              | $`\log _ { 4 } 16 \cdot \sqrt { 2 } =`$               |


### Exercises with the following rules
$`\log _ { a } b ^ { c } = c \log _ { a } b , \quad a ^ { \log _ { a } b } = b , \quad \log _ { e } a = \ln a`$

### Examples
$`8 ^ { 1 - \log _ { 8 } 4 } = \frac { 8 ^ { 1 } } { 8 ^ { \log _ { 8 } 4 } } = \frac { 8 } { 4 } = 2`$

$`9 ^ { \log _ { 9 } 3 + \log _ { 3 } 5 } = 9 ^ { \log _ { 9 } 3 } \cdot 9 ^ { \log _ { 3 } 5 } = 3 \cdot \left( 3 ^ { 2 } \right) ^ { \log _ { 3 } 5 } = 3 \cdot 3 ^ { 2 - \log _ { 3 } 5 } = 3 \cdot 3 ^ { \log _ { 3 } 5 ^ { 2 } } = 3 \cdot 5 ^ { 2 } = 75`$

### Exercises (not 10 based logarithms!)

|                                  |                                                             |                                      |
|----------------------------------|-------------------------------------------------------------|--------------------------------------|
| $`9 ^ { \log _ { 3 } 2 } =`$     | $`4 ^ { \log _ { 2 } 3 } =`$                                | $`5 ^ { 1 + \log _ { 5 } 3 } =`$     |
| $`6 ^ { 2 + \log _ { 6 } 3 } =`$ | $`2 ^ { \log _ { 2 } 5 - 1 } =`$                            | $`3 ^ { 2 \cdot \log _ { 3 } 4 } =`$ |
| $`4 ^ { 1 + \log _ { 2 } 5 } =`$ | $`( \sqrt { 2 } ) ^ { 4 + \log _ { 2 } 4 } =`$              | $`- e ^ { \ln 3 } =`$                |
| $`2 ^ { 1 + \log _ { 2 } 6 } =`$ | $`\left( \frac { 1 } { 3 } \right) ^ { \log _ { 9 } 4 } =`$ |                                      |


### Exercises, calculate
### Examples
$`e ^ { \ln 3 - \ln 4 } = \frac { e ^ { \ln 3 } } { e ^ { \ln 4 } } = \frac { e ^ { \log _ { e } 3 } } { e ^ { \log _ { e } 4 } } = \frac { 3 } { 4 }`$

$`( \sqrt { e } ) ^ { \ln 25 - 4 } = \frac { e ^ { \frac { 1 } { 2 } \log _ { e } 25 } } { \left( e ^ { \frac { 1 } { 2 } } \right) ^ { 4 } } = \frac { e ^ { \log _ { e } 25 ^ { \frac { 1 } { 2 } } } } { e ^ { 2 } } = \frac { e ^ { \log _ { e } \sqrt { 25 } } } { e ^ { 2 } } = \frac { 5 } { e ^ { 2 } }`$

### Exercises

|                             |                                    |                                        |
|-----------------------------|------------------------------------|----------------------------------------|
| $`10 ^ { \log 8 - 1 } =`$   | $`10 ^ { \log 25 - 2 } =`$         | $`100 ^ { \log 3 } =`$                 |
| $` 100 ^ { \log 4 - 1 } =`$ | $` 25 ^ { 1 - \log _ { 5 } 2 } =`$ | $` e ^ { 2 + \log 2 } =`$              |
| $` e ^ { - 2 \ln 5 } =`$    | $` e ^ { 2 \ln 3 - 1 } =`$         | $` ( \sqrt { e } ) ^ { 2 - \ln 9 } =`$ |


## Logarithms rules
$`\log _ { a } \left( \frac { x _ { 1 } } { x _ { 2 } } \right) = \log _ { a } x _ { 1 } - \log _ { a } x _ { 2 } , \quad \log _ { a } \left( x _ { 1 } \cdot x _ { 2 } \right) = \log _ { a } x _ { 1 } + \log _ { a } x _ { 2 }`$

### Examples
$`3 \ln 5 e + 2 \ln \left( \frac { e } { 5 } \right) = 3 ( \ln 5 + \ln e ) + 2 ( \ln e - \ln 5 ) = 3 \ln 5 + 3 \ln e + 2 \ln e - 2 \ln 5 = \ln 5 + 5`$

$`\log ( 2 \sqrt { 5 } ) + \log ( 5 \sqrt { 2 } ) = \log 2 + \log \sqrt { 5 } + \log 5 + \log \sqrt { 2 } = \log ( 2 \cdot 5 ) + \log ( \sqrt { 5 } \cdot \sqrt { 2 } ) = 1.5`$

### Exercises

|                                                                                                           |                                                                                 |
|-----------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------|
| $`\log _ { 6 } 3 + \log _ { 6 } 2 =`$                                                                     | $`\log _ { 2 } 3 + \log _ { 2 } \frac { 4 } { 3 } =`$                           |
| $` \log _ { 3 } 45 - \log _ { 3 } 5 =`$                                                                   | $` \log _ { 5 } 3 - \log _ { 5 } \frac { 3 } { 125 } =`$                        |
| $` \log 15 - \log 1.5 =`$                                                                                 | $` \log 5 + \log 4 - \log 20 =`$                                                |
| $` \ln 2 \sqrt { e } - \ln 2 =`$                                                                          | $`\log _ { 4 } 3 - \log _ { 4 } 8 + \log _ { 4 } \frac { 2 } { 3 } =`$          |
| $`- \log _ { \frac { 1 } { 2 } } 12 - \log _ { \frac { 1 } { 2 } } 3 + \log _ { \frac { 1 } { 2 } } 9 =`$ | $` \log _ { 9 } 8 - \log _ { 9 } 6 - \log _ { 9 } 4 =`$                         |
| $` 2 \log 2 + \log 0.25 =`$                                                                               | $` 3 \log _ { 6 } 2 + \log _ { 6 } 4.5 =`$                                      |
| $` \log _ { 5 } 4 - 3 \log _ { 5 } 2 + \log _ { 5 } 10 =`$                                                | $`\frac { 1 } { 2 } \log _ { 20 } 4 + 2 \log _ { 20 } 5 + 3 \log _ { 20 } 2 =`$ |
| $`\ln 2 + \ln 3 =`$                                                                                       | $` 2 \ln 4 - \ln 8 =`$                                                          |
| $` 3 \ln 2 + 2 \ln 3 =`$                                                                                  | $` 4 \ln 0.5 + 2 \ln 4 =`$                                                      |
| $` \ln \left( \frac { 1 } { 3 } \right) + \ln ( 3 e ) =`$                                                 | $` \log _ { 3 } 6 - \log _ { 3 } 2 =`$                                          |
| $` 2 \log 5 + \log 4 =`$                                                                                  | $` \log _ { 6 } 48 - 3 \log _ { 6 } 2 =`$                                       |


### Exercises, calculate
### Examples
$`\frac { \ln 9 + \ln 3 } { 2 \ln 3 - \ln 3 } = \frac { \ln 27 } { \ln 3 ^ { 2 } - \ln 3 } = \frac { 3 \ln 3 } { \ln \frac { 3 ^ { 2 } } { 3 } } = \frac { 3 \ln 3 } { \ln 3 } = 3`$

$`\frac { 2 \log 2 + \log 3 } { 1 + \log 1.2 } = \frac { \log 2 ^ { 2 } + \log 3 } { \log 10 + \log 1.2 } = \frac { \log ( 4 \cdot 3 ) } { \log 12 } = \frac { \log 12 } { \log 12 } = 1`$

### Exercises

|                                                                        |                                                                     |                                                                          |
|------------------------------------------------------------------------|---------------------------------------------------------------------|--------------------------------------------------------------------------|
| $`\frac { \log _ { 4 } 27 } { \log _ { 4 } 3 } =`$                     | $`\frac { \log _ { 5 } 3 - \log _ { 5 } 12 } { \log _ { 5 } 4 } =`$ | $`\frac { \log 16 - \log 4 } { \log 16 } =`$                             |
| $` \frac { \log _ { 3 } 8 } { \log _ { 3 } 48 - 1 } =`$                | $` \frac { 2 \ln 2 + \ln 3 } { 4 \ln 2 + \ln 3 - \ln 4 } =`$        | $` \frac { \ln 20 - \ln 5 } { \ln 4 } =`$                                |
| $` \frac { 2 \ln 2 + \ln 7 } { \ln 2 + \ln 14 } =`$                    | $` \frac { \ln 10 - 2 \ln 2 } { \ln 25 - 2 \ln 2 } =`$              | $` \frac { \log _ { 3 } 4 + 2 \log _ { 3 } 5 } { 2 \log _ { 3 } 10 } =`$ |
| $` \frac { 3 \log _ { 2 } 5 } { \log _ { 2 } 50 - \log _ { 2 } 2 } =`$ |                                                                     |                                                                          |

## Rule
$`\log _ { a } f ( x ) = \log _ { a } g ( x ) \Leftrightarrow f ( x ) = g ( x )`$

### Examples
$`\ln ( x + 3 ) - \ln x = 2 \ln 2 \Rightarrow \ln \left( \frac { x + 3 } { x } \right) = \ln 2 ^ { 2 } \Rightarrow \frac { x + 3 } { x } = 4 \Rightarrow x + 3 = 4 x \Rightarrow x = 1`$

**Domain and range:**  
first domain $`x + 3 > 0 \Rightarrow x > - 3`$, second domain $`x > 0`$

It works


$`\ln ( x - 5 ) + \ln x = \ln ( x - 8 ) \Rightarrow \ln ( x ( x - 5 ) ) = \ln ( x - 8 ) \Rightarrow x ^ { 2 } - 5 x = x - 8 \Rightarrow x ^ { 2 } - 6 x + 8 = 0 \Rightarrow x _ { 1 } = 4 , x _ { 2 } = 2`$

**Domain and range:**  

first domain $`x > 5`$, second domain $`x > 0`$, third domain $`x > 8`$

No solution!


### Exercises

|                                                                            |                                                       |
|----------------------------------------------------------------------------|-------------------------------------------------------|
| $`\ln x = \ln 8 - 2 \ln 2 `$                                               | $`\ln ( x + 8 ) = \ln ( 2 x ) `$                      |
| $`\ln ( 6 + x ) = \ln 4 + \ln x `$                                         | $`\ln x = \ln ( 3 x - 4 ) `$                          |
| $`\ln ( x + 1 ) = \ln ( 2 x - 3 ) + \ln 3 `$                               | $`2 \ln x = \ln ( 4 x + 5 ) `$                        |
| $`\ln x = 2 \ln 2 - \ln ( 4 - x ) `$                                       | $`2 \ln ( 2 x + 1 ) = \ln ( 3 x + 4 ) `$              |
| $`\log _ { 2 } x = 1 + \log _ { 2 } 3 , \quad 1 = \log _ { 2 } 2`$         | $`\log ( x - 1 ) = 2 - \log 5 , \quad 2 = \log 100 `$ |
| $`\log _ { 5 } x = 1 + \log _ { 5 } ( x - 4 ) , \quad 1 = \log _ { 5 } 5`$ | $`\log _ { 2 } x + \log _ { 2 } ( 6 - x ) = 3`$       |
