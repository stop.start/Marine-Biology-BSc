# Chemical Bonds

![atom](./img/02_Chemical_Bonds/chemical_bonds.png)

## Hebrew/English Lexicon
| Hebrew         | English           | Definitions |
|----------------|-------------------|-------------|
| זיקה אלקטרונית | Electron affinity |             |


## Periodic Table

### The different groups

![groups](./img/02_Chemical_Bonds/periodic_groups.png)

- Metals (מתכות):
  - **Alkali Metals** (מתכות אלקליות) - column 1.
  - **Alkaline Earths** (מתכות אלקליות עפרוריות) - column 2.
  - **Transition Metals** (מתכות מעבר) - columns 3-12.
- **Nonmetal** (אל-מתכות) - columns 13-17 or 3A-7A (above the "stairs").
- **Halogens** - column 17 or 7A.
- **Noble Gases** - column 18 or 8A.

**The main group contains the s and p groups or in others words columns 1,2,13-17.**

### Specifications for the groups

- All atoms "want" have a full external layer (like the noble gases).
- Metals (columns 1A-3A) will give more easily their external (valence) electrons since it is easier to get rid of 1 to 3 electrons rather to get 5 to 7 electrons.
- Nonmetals tend to take electrons in order to acheive the 8 electrons rule.
- Nobles gases don't do chemical reactions because their external layer is already full.

### The octet rule

- All atoms aspire to have a full external layer - 8 valence electrons -  by either giving, taking or sharing electrons.
- Periods 3 and above have d orbitals which can be empty (for example period 3 don't use the d orbitals at all). This causes some atoms to have **more than 8 electrons in their valence shell**:  
Because of the empty orbitals more electrons are able to enter the valence shell (see [Lewis structures](./03_Lewis_Structures.md) for more details).


## Ions

- **Ion** is an atom that gave away or took in one or more electrons.
  - **Cation**: an atom that gave electrons thus has a **positive charge**.
  - **Anion**: an atom that took in electrons thus has a **negative charge**.
- Atoms will naturally lose or gain at most 3 electrons.

### Ionazation energy

- **Ionazation energy is the energy required to pull out an electron**.
- **First ionazation energy** refers to the energy required to **pull out the first electron** (in the external layer) of an atom.
- **Second ionazation energy** refers to the energy required to **pull out the second electron** (in the current external layer) of an atom that already lost an electron.
- It is harder to pull out the next electron since the number of protons stays the same for less electrons.
- When looking at the periodic table, for each period the ionazation energy increases from left to right.

![ionazation energy](./img/02_Chemical_Bonds/ionization_energy.png)

- After removing all electrons of the last layer, removing electrons from the layer underneath which is full requires a lot lot more energy (even more than from a noble gas).

![ionization energy next layer](./img/02_Chemical_Bonds/ionization_energy_layers.png)

### Electron affinity

- **Electron affinity (זיקה אלקטרונית) is the energy released or spent to take in an electron**.
- Some atom will take in electrons easily and some will require energy to take in electrons.

## Chemical Bonds

Bonds are charactized by:
- **The length of the bond**: 
  - The distance between the nucleuses. 
  - **The shorter the stronger**. 
  - Depends on the radiuses of the atoms in the bond.
- **The energy of the bond**: How much energy is needed to break the bond.
- **The geometry (angle of the bond)**: The **a/symmetry** sets the **molecule characteristics**.

### Metallic bond

- Bond between metals.
- Metals have a low ionization energy and low electron affinity. That's why in ion form they always are cation.
- Metals are cations in a sea of electrons: every atom pushes as much as possible the electrons that "too much" to go by the octet rule.
- Because ions have a positive charge and electrons have a negative charge, the electrons are what allows the metal ions to stick together.
- The more excited are the electrons, the farther are the ions from one another: it is what's make the metal go from solid to liquid to gas. 
- The sea of moving electrons is what makes the metals conductive.
- The sea of electrons is also what makes metals bendable and malleable.
- The strength of a metal block depends on the size and charge of the ions. For example, an atom with a charge of +2 will have a stronger charge and more electrons to share, thus being stronger. As well the smaller the radius the stronger the ion.
- Metal alloy is a mix with a metal and another element. For example: bronze.

### Ionic bond

- Ionic compounds are called **salt**.
- Bond between cation and anion.
- **Ionic bonds** form due to the transfer of an electron from one atom to another.
- The transfer of electron is between **metals and nonmetals** since one wants to give electrons and the other wants to receive electrons.
- Due to their opposite charges the ions are attracted to one another which is a strong bond.
- Unlike metallic bond, this bond is not malleable since there is no sea of electrons to act as a buffer when applying force and the ions with the same charge are close.
- For the same reason they are not conductive.
- Ionic compound have high boiling point. 
- Example:
> Sodium (Na) has 1 valence electron so it wants to give it away (easier than taking in 7 electrons).  
> Chlorine (Cl) has 7 valence electrons and wants to get one.  
> Sodium will give its 1 valence electron to Chlorine which means that both will now be stable electron configuration wise but:  
> Sodium has now a positive charge while Chlorine has a negative charge thus attracting each other and forming and ionic bond: NaCl (salt!)  
> Note: they don't share electrons but stick together because of their charge  

### Covalent bond

- **Covalent bonds** involve the sharing of electrons between two atoms. 
- Metals **don't** form covalent bonds (only ionic bonds).
- There can be up to 3 covalent bonds between 2 atoms.
- A triple bond is stronger than a double bond which is strong than a single bond but the strength between bonds in double and triple bonds is not equal (see geometry lesson).
- Example:
> Oxygen (O) has 6 valence electrons, which means it would like to gain 2 more electrons.  
> 2 Oxygen atoms can share 2 electrons and pretend they each have 8.  
> None "gives away" its electrons but each one shares two electrons thus forming a covalent bond.  

## Electronegativity

- It is the **power of an atom to attract electrons to itself**. 

- The closer the electrons are to the nucleus the more energy it requires to take them away from the atom

- For example Fluorine (F) or Oxygen (O) are a lot more **electronegative** than, for example, Sodium (Na) and even more so than, for example, Cesium (Cs), because Cesium has one elecron in its outter shell and it's really far from the nucleus. The valence shell of Fluorine and Oxygen is the 2nd shell which is a lot closer to the nucleus, and they both want to add electrons in order to be more stable.

- Noble gases (group 18) are stable, they don't need to gain or give electrons thus electronegativity doesn't really apply to them.

- **Linus Pauling** calculated atoms electronegativity and created the below scale:

 ![pauling_scale](./img/02_Chemical_Bonds/electronegativity_table.png)

- When atoms form a molecule, the higher electronegativity value will determine where the electrons are most of the time. _For example, in a molecule composed of Carbon (C) and Oxygen(O), Oxygen has a higher electronegativity value than Carbon, thus the electrons will be more around Oxygen.

### Polarity

- In the above example, the Carbon has a **partial positive charge** because the electrons will be more around the Oxygen, and for that reason the Oxygen will have a **partial negative charge**. This is a **polar covalent bond**.

- Molecules composed of atoms with the same electronegativity will form a **non-polar covalent bond**.

- If the difference between the electronegativity of 2 atoms in a molecule is small enough the **covalent bond** can be considered **non-polar**. _For expample, Carbon (C) has an electronegativity of 2.5 and Hydrogen (H) 2.1 (or in this table 2.2) so the difference is quite small_.

- **The limit between polar and non-polar covalent bond is around 0.5**.
- **The limit between covalent bond and ionic bond is around 1.7**.

## TODO
- Add שריג יוני
