# Lewis Structures or Lewis Dot Structures

## Intro

Lewis structures are molecules diagrams showing the bonds between atoms and the atoms lone pairs of electrons.  
The lone pairs of electrons are represented by dots and the bonds by a line between the atoms.  

## Ionic Bonds in Lewis Structures

- The **cation** won't have dots (electrons) around since it "gave" them away.
- The **anion** gets the dots from the cation (released electrons).
- Both show the charge they have.
- There are no lines between the atoms since those are not covalent bonds meaning the atoms do not share their electrons.

![lewis ionic bonds](./img/03_Lewis_Structures/lewis_ionic_bonds.png)

_Note: during the course we won't talk too much about the ionic bonds_

## Covalent bonds in Lewis Structures

Example of single, double and triple bonds in Lewis structure:  

![lewis covalent bonds](./img/03_Lewis_Structures/lewis_covalent_bonds.png)

Example of molecules in Lewis structure:  

![lewis molecules](./img/03_Lewis_Structures/lewis_molecules.png)


### Steps for drawing Lewis structures

---

1. Count the number of valence electrons for the entire molecule.
2. Choose the atom that will be at the center.
3. Draw in single bonds between the atoms.
4. Complete the octet rule for the atoms on the outside (not the central one!).
5. Count the number of electrons in the drawing (a line = 2 electrons).
6. Complete the octet rule for the central atom: either by adding electrons or by adding double/triple bonds to get the correct number of electrons.
7. Recount the number of electrons in the drawing (a line = 2 electrons).

---

#### Example

$`\bold{CO_2}`$  

1. Number of valence electrons:
   - C: 4 electrons
   - O: 6 electrons x 2
   - **Total = 4 + 6 x 2 = 16**
2. Atom at the center: 
   - C makes more bonds so it will be at the center
   
   ![lewis step2](./img/03_Lewis_Structures/lewis_step2.png)

3. Draw single bonds:

   ![lewis step3](./img/03_Lewis_Structures/lewis_step3.png)
   
4. Complete the octet rule for the atoms on the outside:

  ![lewis step4](./img/03_Lewis_Structures/lewis_step4.png)

5. Count the number of electrons in the drawing:
   - 6 lone pairs for each O = 2 x 6 = 12
   - 2 single covalent bonds (1 bond = 2 electrons) = 2 x 2 = 4
   - **Total = 12 + 4 = 16**
6. Complete the octet rule for the central atom:
   - C has only 4 valence electrons in the last drawing. 
   - If we add 4 more then the **total count will be 16 + 4 = 20 > 16**. It's too much
   - Let's add some double bonds:
   
  ![lewis step6](./img/03_Lewis_Structures/lewis_step6.png)
  
7. Recount:
   - 4 lone pairs = 2 x 4 = 8
   - 4 bonds = 2 x 4 = 8
   - **Total = 8 + 8 = 16**
   - All atom completed the octet rule: it's ok!

#### Example with an ion

$`\bold{OH^-}`$  

1. Number of valence electrons:
   - O: 6 electrons
   - H: 1 elecron
   - It is an anion (-1) so +1 elecron (for a cation we remove an elecron)
   - **Total = 6 + 1 + 1 = 8**
2. There's only two atoms so there's only one bond.

   ![lewis anion step2](./img/03_Lewis_Structures/lewis_anion_step2.png)

3. Draw single bond:

   ![lewis anion_step3](./img/03_Lewis_Structures/lewis_anion_step3.png)

4. Complete the octet rule:

   ![lewis anion step4](./img/03_Lewis_Structures/lewis_anion_step4.png)

5. Count the number of electrons in the drawing:
   - O: 6 lone pairs = 6 x 2 = 12
   - 1 covalent bond = 2
   - **Total = 8**
   - All atoms complete the octet rule: it's ok!

   ![lewis anion final](./img/03_Lewis_Structures/lewis_anion_final.png)

_Note: because it is an ion the drawing is put in brackets [] and the charge is written at the top right of it_

### Octet rule violations

- Atoms from period 3 and above have empty orbitals (d and f). For example, period 3 has empty d orbitals for all its atoms.
- Those orbitals still exist and can receive atoms which causes some atoms to have for than 8 electrons in their valence shell.

#### Example of "More than an octet"

- In $`PCl_5`$ P has 5 bonds which means 10 electrons instead of 8.
- The formal charge (see below) for P is still 0.

![more than octet](./img/03_Lewis_Structures/pcl5.png)

## Formal charge

- In a molecule the overall charge can be 0 but each atom can have a different charge.
- The **formal charge** is the charge of a **specific atom** in a molecule.
- **The sum of the formal charges in the molecule equals the charge of the molecule**. If the molecule is neutral then the sum of the formal charges is 0.
- In order to calculate the **formal charge** of an atom:
  
  ```math
  \text{Formal Charge} = (\text{Group number of the atom}) - (\text{Number of the covalent bonds}) - (\text{Number of electrons in lone pairs})
  ```
#### Example

$`CN^-`$

![formal charge](./img/03_Lewis_Structures/formal_charge.png)

- Formal charge for N: 5 (group) - 3 (covalent bonds) - 2 (lone pairs) = 0
- Formal charge for C: 4 - 3 - 2 = -1
- $`CN^-`$ is an anion thus the sum of the formal charges is -1.


## Resonance

Some molecules can be drawn in several ways: they are symetric drawing.  

### Examples

$`SO_2`$

![resonance](./img/03_Lewis_Structures/resonance1.png)

In order to draw all options in one drawing we can do the following

![resonance](./img/03_Lewis_Structures/resonance2.png)  


$`C_6H_6`$

![resonance](./img/03_Lewis_Structures/resonance3.png)  
