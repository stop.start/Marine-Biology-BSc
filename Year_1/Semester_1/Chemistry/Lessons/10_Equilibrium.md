# Equilibrium

## Intro

- Stoichiometrically balanced equations that are bi-directional don't react entirely because the reaction goes both ways.
- The reaction will go both ways and at some point the rate of reactants to products will stabalize as well as the rate of products to reactants. _That doesn't mean that the quantity of reactants = the quantity of products nor that there are no more reactions!_ This is called **equilibrium**.
- The reactions still happen but at a macroscopic level it looks like it stays the same.
- A **dynamic equilibrium** happens in a **closed system** where the concentration of reactants and products remains the same.
- At first the reaction happens more in one way or another until it stabalizes and reach equilibrium. In order to acheive this the **temperature needs to be constant**.
- In dynamic equilibrium there is no additional energy, everything happens **spontaniously**.

## Equilibrium constant $`K_{eq}`$

- Every reaction in equilibrium has an **equilibrium constant $`\bold{K_{eq}}`$**.
- The equilibrium constant relates only to **balanced equations**.
- The equilibrium constant depends on the **temperature** and the **concentration of each reactants and products**:
  - A,B: reactants - C,D: products.
  - a,b,c,d: stoichiometric coefficients.
  - [] : concentration in molar.

```math
\begin{aligned}
&aA + bB \leftrightharpoons cC + dD \\
&K_{eq} = \frac{[C]^c \cdot [D]^d}{[A]^a \cdot [B]^b}
\end{aligned}
```
- The more matter the higher will be $`K_{eq}`$. That's why the stoichiometric coefficients are important and we can't just write any balanced equation:

```math
\begin{aligned}
&2A_{(g)} \leftrightharpoons B_{(g)}\\
&K_{eq1} = \frac{[B]}{[A]^2}
\end{aligned}

\qquad\quad

\begin{aligned}
&4A_{(g)} \leftrightharpoons 2B_{(g)}\\
&K_{eq2} = \frac{[B]^2}{[A]^4} \\
&K_{eq2} = (K_{eq1})^2
\end{aligned}

\qquad\quad

\begin{aligned}
&B_{(g)} \leftrightharpoons 2A_{(g)}\\
&K_{eq3} = \frac{[A]^2}{[B]} \\
&K_{eq3} = \frac{1}{K_{eq1}}
\end{aligned}
```
- Solid matter isn't taken into consideration when calculating the constant K. It doesn't react enough to take into account. **If there's solid matter the concentration is written as 1**.

## Gases in equilibrium

- We know that: $`PV = nRT`$ which can be written as $`P = \large{\frac{n}{V}}\normalsize RT`$
- $`\large\frac{n}{V} = \normalsize C_{(M)}`$ which means that **at constant temperature the pressure P is proportional to the concentration C**: $`P = C_{(M)}RT`$.
- This means that with gases we can also get a constant according to the pressures in atm.
  - **The constant based on concentration is called  $`\bold{K_c = K_{eq}}`$**.
  - **The constant based on pressure is called $`\bold{K_p}`$** and is calculated (**if all gases**):
  
```math
\begin{aligned}
&aA + bB \leftrightharpoons cC + dD \\
&K_p = \frac{P(C)^c \cdot P(D)^d}{P(A)^a \cdot P(B)^b}
\end{aligned}
```
  - Or calculated from $`K_c`$
   
```math
  K_p = K_c \times (RT)^{\Delta n}
```

- $`\bold{\Delta n}`$ is the **sum of stoichiometric coefficients of the products minus the stoichiometric coefficients of the reactants**.
  - $`\Delta n = 0`$ then $`K_p = K_c`$
  
## Size of $`K_{eq}`$

- $`K_{eq}`$ is **big** means that there's **a lot of products and little reactants**.
- $`K_{eq}`$ is **less than 1** means that there's **a lot of reactants and little products**.

## Reaction Quotient Q

```math
\begin{aligned}
&aA + bB \leftrightharpoons cC + dD \\
&Q = K_{eq} = \frac{[C]^c \cdot [D]^d}{[A]^a \cdot [B]^b}
\end{aligned}
```

- Given K and the concentration of each reactants and products we can know if the mixture is in equilibrium.
- If Q = K then it is in equilibrium.
- If Q > K then there's too much products thus the reaction will be more a **reverse reaction**.
- If Q < K then there's too much reactions thus the forward reaction will happen more.

## Energy

- **Activation energy** is the energy required to start a reaction.
- **Exothermic reactions** need less activation energy than they release - they release heat - $`\bold{\Delta H < 0}`$.
- **Endothermic reactions** need more activation energy than they release - they need heat to happen - $`\bold{\Delta H > 0}`$.

 ![activation energy](./img/10_Equilibrium/activation_energy.png)

- Breaking bonds requires energy.
- Creating bonds releases energy.

## Disturbance in the equilibrium

**_What happens when we change the conditions of a reaction in equilibrium?_**  

### Le Châtelier's principle

- This principle helps predict how a reaction in equilibrium will react to a change of conditions (concentration/temperature/pressure).

#### Changing the concentration

- At constant temperature, changing the concentration of one of the reactants/products will result in a reaction to lower the disturbance and return to equilibrium.
- Despite the change of concentration **K doesn't change!**
- In the example below, adding CO will make $`CO`$ react with $`Cl_2`$ in order to return to equilibrium.

 ![concentration change](./img/10_Equilibrium/concentration.png)
 
#### Changing the volume

- At constant temperature, in a system with **gases**, changing the volume will result in change of concentration **for all the components**.
- **Decreasing the volume will increase the concentration**.
- If the number of moles for the components is not equal, then the reaction will go in the direction: more moles $`\rightarrow`$ less moles. 
- **Reaction with the same number of moles won't be disturbed by a change of volume**. For example: $`A_{2(g)} + B_{2(g)} \leftrightarrow 2AB_{(g)}`$
- Despite the change of volume **K doesn't change!**

 ![volume change](./img/10_Equilibrium/volume.png)

#### Changing the temperature

- **K is defined for a specific temperature**.
- By Le Châtelier's principle:
  - With heat, the system will decrease the heat by using it.
  - With cold, the system will warm up by creating heat.
- Changing the temperature will change the constant K:
  - With **exothermic reaction** (releases heat), increasing the temperature is "like" having more products (since heat is produced) thus the reaction will go in the **direction of the reactants**. This means that **K will decrease** (reactants are in the denominator).
  - With **endothermic reaction** (absorbs heat), increasing the temperature is "like" having more reactants (since heat is used) thus the reaction will go in the **direction of the products**. This means that **K will increase** (products are in the numerator).

 ![temperature change](./img/10_Equilibrium/temperature.png)
 
