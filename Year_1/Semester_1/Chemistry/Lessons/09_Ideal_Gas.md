# Ideal Gas

## States of matter

- Substances can exist under different **temperature** and **pressure** conditions.
- The difference between the states is the distance between the molecules and their mobility.

### Solid

- There's a pulling force that keeps the molecules close to each other.
- The molecules are sill in movement altough the distance between doesn't change because of it.
- The molecules are arranged in an organized way.
- The movement of the molecules depends on the temperature, **the higher the temperature the higher the kinetic energy** (and at some point will change the substance state). 
- There is no visible change as a result of changes in temperature or pressure.

### Liquid

- The molecules move but are still linked to each other.
- There is no visible change in volume as a result of changes in temperature or pressure.

### Gas

- Molecules move freely.
- There is no arrangement between the molecules.
- A gas has no fix volume or shape but takes the shape/volume of the its container.
- Unlike liquids and solids, **gases spreads or is compressed depending on the temperature and pressure**.

## Pressure

- Pressure is a force per unit area: $`P = \frac{F}{A}`$
- There are 3 units of measure for pressure:
  - Atmospheric - **atm**: 1 atm is the pressure at sea level.
  - Milliliter Mercury (מיליטר כספית)- **mm Hg**
  - **Torr**
- **1 atm = 760 mm Hg = 760 Torr**

  ![atm](./img/09_Ideal_Gas/atm.png)

- Pressure measurement:
  - P - pressure
  - $`\rho`$ - density 
  - h - height of the column
  - g - gravitational force
  
```math
P = \rho \times h \times g
```

## Ideal Gas

- All gases have common characteristics that are unrelated to their composition.
- When talking about ideal gas:
  1. The volume of atoms and molecules is null compared to the volume of the container.
  2. The overall kinetic energy stays the same (despite collisions between atoms/molecules).
  
### Boyle law

- **At constant temperature, the volume of a fixed amount of gas is inversibly proportional to its pressure** $`\Longrightarrow`$ the more pressure the less volume.

 ![volume pressure](./img/09_Ideal_Gas/volume_pressure.png)

- The relation between the volume and the pressure is proportional at the same temperature: 

```math
\begin{aligned}
&PV = \text{constant} \\
&P_1 \times V_1 = P_2 \times V_2
\end{aligned}
```

 ![volume pressure proportional](./img/09_Ideal_Gas/volume_pressure_proportional.png)

### Charles law

- **At constant pressure, the volume of a fixed amount of gas is directly proportional to its absolute temperature** $`\Longrightarrow`$ the higher the temperature the bigger the volume.

 ![volume temperature](./img/09_Ideal_Gas/volume_temperature.png)

- The relation between the volume and the temperature is proportional with the same pressure: 

```math
\frac{V}{T} = \text{constant}
```

#### Absolute zero

```math
T_K = T_C + 273.15
```

- When looking at the volume in function of temperature the results are linear.
- Although the temperature cannot be dropped too much since it would change the state of the gas to liquid/solid, if we contiue the line for all gases until the volume is 0, we get to $`-273.15\degree C`$: the absolute zero.

 ![absolute zero](./img/09_Ideal_Gas/absolute_zero.png)

### Avogadro's law

- **At constant temperature and pressure, the volume of a gas is proportional to the number of moles**.
- In those conditions, the volume is a measure of the amount of molecules.
- **Equal volumes of different gases in the same conditions contain the same amount of molecules**.

```math
\begin{aligned}
&V = \text{constant} \times n \\
&\frac{V}{n} = \text{constant}
\end{aligned}
```

### Ideal gas law

- **Ideal gas** that observe Boyle, Charles and Avogadro's laws.
- When combining all laws (V proportional to 1/P, V proportional to T, V proportional to n) we get the following formula:
  - R is the **universal gas constant** which depends on the **units** of P, V, n and T (see table below). Because they are all related - if P changes V, n and T changes etc - R was calculated for different units.
  - T is temperature in Kelvin.
  
```math
\begin{aligned}
&V = R \cdot \frac{nT}{P} \\
&\bold{PV = nRT}
\end{aligned}
```
![r constant](./img/09_Ideal_Gas/r_constant.png)

### Standard Temperature and Pressure S.T.P

- **S.T.P: 1atm, $`0\degree C`$ ($`273\degree K`$)**
- **The volume of 1mol of ideal gas in S.T.P conditions equals to 22.414L**.

### Dalton's law

- **The pressure of a mixture of gases is equal to the sum of the pressures for each gas**.
- Each gas put a pressure depending on the number of moles and isn't dependent on the other gas.

  ![dalton's law](./img/09_Ideal_Gas/daltons_law.png)
  
### Diving in the sea

- Every **10 meters** adds 1 atm pressure.
