# Geometry

## VSEPR Theory

- VSPER: Valence Shell Electron Pair 
- The theory of the repulsion between elecrons.
- The repulsion between elecrons is what set the angles between the atoms bonds.
- A **bond pair - BP - (זוג קושר)** is a pair of electrons engaged in a bond.
- A **lone pair - LP - (זוג לא קושר)** is a pair of electrons that are not in a bond.
- A **lone pair** takes **more space** than a bond pair. This is because a lone pair is held on by one nucleus whereas a bond is shared between two nucleuses thus sometimes around one and sometimes around the other.

## Geometries

- **2 atoms will always have a linear structure**: $`180\degree`$ as far as they can be.
- **2 atoms bonded to one central atom which doesn't have any lone pairs will have a linear structure**.
- A molecule with 4 bonding pairs and one lone pair will be shaped like a trigonal bipyramid and the **lone pair will be on the triangle**. That's because the angles are $`120\degree`$ whereas the angles on top and bottom are $`90\degree`$. This called **seesaw (טטרא-הדר מעובת)**.
- 3 bonding  pairs and 2 lone pairs is called **T shaped**. It's the same principe as above and when 2 atoms are absent from the triangle we're left with a T.
- 2 bonding pairs and 3 lone pairs is **linear**.
- When there's more than one central atom, go over each of them.

![summary](./img/04_Geometry/summary.png)
