# Molecular Bonds

## Intro

- Molecules can be solid, liquid or gas.
- Their state depends on the strength of the covalent bonds as well as the temperature and pressure.
- **Every substance has its boiling point and evaporation point**.
- The stronger the bonds between the molecules the higher the temperature of the boiling/evaporation points will be.
- On the periodic table, for each atom is indicated the boiling point and melting point.
- 3 forces exist that link between molecules:
  - **Vander Waals forces**: which are divided into two forces:
    - **Dipole-dipole** - polar bonds.
    - **London** forces.
  - **Hydrogen bonds**.
  
## Van der Waals forces

### Dipole-dipole

- Polar molecules (due to diffences in electronegativity) are attracted to one another because of the charge.

![dipole-dipole](./img/06_Molecular_Bonds/dipole_dipole.png)

- They arrange themselves more organised when in solid state

![dipole-dipole solid](./img/06_Molecular_Bonds/dipole_states.png)

### London

- Molecules that are not polar still bond. 
- They manage to bond due to **temporary dipole moment** (דיפול דיפול מושרה).
- **Temporary dipole moment** is due to the movement of electrons around the atoms. At some point there can be more electrons on on side creating a temporary charge that can be used to bond with another molecule.
- When atoms or molecules with a temporary dipole get close to other atoms/molecules will push the elctrons in a way that will cause a temporary dipole moment in the other atoms/molecules.
- Those bonds are the weakest of all types of bonds.
- The strength of those bonds depend on the number of protons and electrons: the more there are the stronger the bond is.
- Bonds between atoms/molecules with a lot of electrons will last longer since the temporary dipole is created more easily.
- The more atoms and the less branches in a molecule, the more electrons there are and the more possibilities there are to create van der waals bonds.
- Big atoms/molecules can get to the point where the substance is solid.

### States of molecules with van der waals bonds: halogenes

The following table shows the states of halogenes molecules at room temperature.  

| Iodine $`I_2`$ | Bromine $`Br_2`$ | Chlorine $`Cl_2`$ |
|----------------|------------------|-------------------|
| Solid          | Liquid           | Gas               |

- [I]odine is solid because it has bigger atoms with more electrons and protons making the London force stronger.
- [C]hlorine is in gas state because its atoms are smaller thus the bonds created were a lot weaker and the room temperature is enough to break those bonds.

## Hydrogen bonds

- Hydrogen bonds are stronger than Van der Waals bonds (but a lot weaker than covalent bond). 
- Hydrogen bonds are dipole-dipole bonds between **"protonated" Hydrogen** and one of the **NOF** atoms: **Nitrogen, Oxygen, Fluorine**.
- NOF atoms are small and electronegative, the bonded hydrogen is left will only its proton.
- The positive hydrogen is so small that it can get really close to other molecule on their negative side.
- Hydrogen bonds are the reason why molecules $`HF`$, $`NH_3`$ and $`H_2O`$ have a boiling point a lot higher than expected.
- $`H_2O`$ has the highest boiling temperature, although the hydrogen bonds between $`HF`$ is stronger, because the geometry of the molecules allows for more hydrogen bonds on average than the other two.

## Bonds scale

The following shows a comparision of the different bonds strength

![bonds strength](./img/06_Molecular_Bonds/bonds_scale.png)

## Disolving/Mixing 

- A **substance will disolve** in an other substance if:
  - **They can form bonds between them**.
  - **Those bonds are stronger or at least equal in strength**.
- Molecules with Van der Waals bonds will interact (mix) with other molecules with Van der Waals bonds.
- Molecules with hydrogen bonds will interact (mix) with other molecules with hydrogen bonds.
- Non polar molecules with Van der Waals bonds --- 
- Water will disolve substances that either are polar or have van der waals or hydrogen bonds. 
- Water is a small molecule from small atoms so it is strong.
- **Fats** are mainly composed of carbon and hydrogen. They're not polar nor have NOF atoms thus they are **hydrophobic**.
- Molecules that have hydrophobic tails and hydrophilic heads can be disolved in water: the longer the tail the more water is required to disolve the molecule.

### Soap

- **Soap has and hydrophilic head and hydrophobic tail**.
- Because of that it can bond with water and oil.
- Red wine has the same characteristics as soap in this matter and that's why it is good to drink it with meat: meat has fat and the wine help clean the tongue when eating it.
