# Stoichiometry

## Chemical equation

### Reactants $`\longrightarrow`$ Products

![equation example](./img/07_Stoichiometry/equation_example.png)

### Law of conservation

- The number of atoms before and after the reaction stays the same.
- The equation is balanced by adding **stoichiometric coefficients**.

#### Example

```math
\bold{3}IBr + \bold{4}NH_3 \longrightarrow NI_3 + \bold{3}NH_4Br
```

### Heating and Disolving of ionic compounds

- Water and heat are not written has part of the reaction but above the arrow.

```math
NaCl_{(s)} \xrightarrow{H_2O_{(l)}} Na^{+}_{(aq)} + Cl^{-}_{(aq)} 
```

```math
CuCl_{2(s)} \xrightarrow{heat} Cu^{2+}_{(l)} + 2Cl^{-}_{(l)}
```

## Mass

- **AMU** is the **Atomic Mass Unit**. Proton = Neutron = 1amu. Electron ~= 1/1800 amu
- In the periodic table the mass for each atom is the average mass for all isotopes (according to their frequency in nature).
- The mass of a molecule or ionic matter is the total mass of all atoms in the molecule/ion formula. 

> _Example:_  
> $`H_2O: 1\;amu (H) \times 2 + 16amu (O) = 18\;amu`$  

## Empirical formula

### Molecules

- Formula gotten from the **ratio of atoms in the substance**. 
- An empirical formula can define multiple substances. 
- Different substances with the same empirical formula have different masses.

 > _Examples:_  
 > $`CH_2O`$ (1:2:1 ratio) is the empirical formula for $`CH_2O`$, $`C_2H_4O_2`$, $`C_3H_6O_3`$ and $`C_6H_{12}O_6`$  
 > $`C_2H_6O`$ (2:6:1 ratio) is the empirical formula for $`CH_3OCH_3`$ and $`C_2H_5OH`$  

### Ions compounds

- The empirical formula for ion compounds has:
  - On the left the positive ion
  - On the right the negative ion
  - Overall the formula needs to be neutral
  
 > _Examples:_  
 > $`Na^{+} + Cl^{-} \longrightarrow NaCl`$  
 > $`Na^{+} + O^{-2} \longrightarrow \text{needs 2 Na in order to be neutral} \longrightarrow \bold{2}Na^{+} + O^{-2} \longrightarrow Na_2O`$  
 > $`\bold{3}Mg^{+2} + \bold{2}N^{-3} \longrightarrow Mg_3N_2`$  

## Mole

- Also called **Avogadro's number ($`N_A`$)**.
- Usually written "mol".
- The mole is a number not weight or length.
- **1 mol = 6.022x10<sup>23</sup>**
- **Molar mass**: mass of an atom in grams and multiply it by the mole number (6.022x10<sup>23</sup>).
- The molar mass of an element is the atomic mass number for this element. In other words the mass of one atom in amu is the same number as the mass of 1 mole of atoms in grams. This means that there are 6.022x10<sup>23</sup> atoms in 1mole of a substance.

 > _Example: hydrogen_  
 > Mass for one H is 1.674<sup>-24</sup>gr.  
 > 6.022x10<sup>23</sup> * 1.674<sup>-24</sup>gr = **1.0079gr**. In other words: 1.0079 gr/mol  
 > Atomic mass number for one H is **1.0079amu**.  

- The molar mass is written **$`\bold{M_W}`$** and is measured in gram per mole : $`M_W = \frac{gr}{mol}`$.

 > _Example: 
 > Sodium (Na)_  
 > Atomic mass = 23amu  
 > Molar mass $`M_W`$ = 23gr/mol  
 >  
 > Water ($`H_2O`$)  
 > Atomic mass = 18amu  
 > Molar mass $`M_W`$ = 18gr/mol

- For every substance a mole is different:

![moles comparision](./img/07_Stoichiometry/moles_comparision.png)

### Calculating the molar mass for an element

#### Example

- Chlorine Cl
- Two isotopes:
  - $`_{17}^{35}Cl`$: frequency = 75.53%
  - $`_{17}^{37}Cl`$: frequency = 24.47%
- Molar mass:

```math
\frac{35 \times 75.53 + 37 \times 24.47}{100} = 35.49 gr/mol
```

### Calculating the number of moles

The number **n** of moles for X grams of a substance is calculated as follow:  

```math
\bold{n} \;\text{in mol} = \frac{\bold{m} \;\text{in gr}}{\bold{M_W} \;\text{in gr/mol}}
```

From this we have:  

```math
n = \frac{m}{M_W} \Longleftrightarrow M_W = \frac{m}{n}
```

#### Examples

1. 1 mole of hydrogen: n = 1, m = 1.0079gr, $`M_W`$ = 1.0079gr/mol
2. If we have 12 grams of [He]lium and knowing that its molar mass is 4gr/mol then we have $`\frac{12}{4} = 3mol`$ of He.

_Note: If we multiply by 6.022x10<sup>23</sup> we get the number of molecules/atoms in the substance_

### Finding the empirical formula

- Let's say that we have 100gr of Chrome Salt which contains the following atoms: O, Cr and Na.
- We know that we have 42.8% of O, 39.7% of Cr and 17.5% of Na.
- Because we have 100gr of salt the above means that we have 42.8gr of O, 39.7gr of Cr and 17.5gr of Na.

|                    | O                         | Cr                        | Na                        |
|--------------------|---------------------------|---------------------------|---------------------------|
| % in 100gr         | 42.8%                     | 39.7%                     | 17.5%                     |
| Mass in the 100gr  | 42.8gr                    | 39.7gr                    | 17.5gr                    |
| Molar mass $`M_W`$ | 16gr/mol                  | 52gr/mol                  | 23gr/mol                  |
| Number of moles n  | $`\frac{42.8}{16}=2.680`$ | $`\frac{39.7}{52}=0.763`$ | $`\frac{17.5}{23}=0.761`$ |

- Currently the empirical formula is $`Na_{0.761}Cr_{0.763}O_{2.680}`$
- We need integers:

|                    | O                            | Cr                                 | Na                           |
|--------------------|------------------------------|------------------------------------|------------------------------|
| Number of moles n  | 2.680                        | 0.763                              | 0.761                        |
|                    | $`\frac{2.680}{0.761}=3.52`$ | $`\frac{0.763}{0.761}\approx 1.0`$ | $`\frac{0.761}{0.761}=1.00`$ |

- We don't want half atoms so:

|                   | O                            | Cr                                 | Na                           |
|-------------------|------------------------------|------------------------------------|------------------------------|
| Number of moles n | 2.680                        | 0.763                              | 0.761                        |
|                   | $`\frac{2.680}{0.761}=3.52`$ | $`\frac{0.763}{0.761}\approx 1.0`$ | $`\frac{0.761}{0.761}=1.00`$ |
| Multiply by 2     | 3.5x2=7                      | 1x2=2                              | 1x2=2                        |

- **Empirical formula**: $`Na_2Cr_2O_7`$


### How many moles after a reaction

- If we have two reactants as follow: 2 moles of a substance and 1 mole of another substance, it doesn't mean that we'll have 3 moles in the product.
- Since a mole is 6.022x10<sup>23</sup> particules of a substance, if we take for instance 2 moles of $`H_2`$ and 1 mole of $`O_2`$ we won't have 3 moles of $`H_2O`$ but 2 moles because it keeps the ratio which is deduced from stoichiometric coefficients.

### Example of calculations with moles and reactions

Water reaction from hydrogen and oxygen is as follow:  

```math
2H_{2(g)} + O_{2(g)} \longrightarrow 2H_2O_{(l)}
```
- This means that we have a ratio of 2 moles of $`H_2`$ for 1 mole of $`O_2`$ and get 2 moles of $`H_2O`$.
- If we have **100gr** of $`H_2`$ **how many grams of $`O_2`$ do we need?**
  1. $`M_W`$ of H is 1gr/mol so $`M_W`$ of $`H_2`$ is 2gr/mol.
  2. We have 100gr of $`H_2`$ which means that we have $`\frac{100gr}{2gr/mol}=50mol`$ of $`H_2`$.
  3. Since the ratio of the reaction is 2:1:2 we know that if we have 50mol of $`H_2`$ we have 25mol of $`O_2`$ and we'll get 50mol of $`H_2O`$.
  4. $`M_W`$ of O is 16gr/mol so $`M_W`$ of $`O_2`$ is 32gr/mol.
  5. We know we have 25mol of $`O_2`$ which means that we have $`32gr/mol \cdot 25mol= 800gr`$ of $`O_2`$ for 100gr of $`H_2`$.

## Limiting Reactants

- **Limiting reactants - מגיב מגביל**: reaction that is not balanced will leave part of the reactants that haven't reacting
- The **amount of product** depends on the reactant that is lacking or limiting.

> _Example:_  
> $`2H_2 + 4O_2 \Longrightarrow 2H_2O + 3O_2`$  
> Hydrogen is the limiting reactant.  
> Because there is too much oxygen 1 mole reacted and 3 moles didn't.  
