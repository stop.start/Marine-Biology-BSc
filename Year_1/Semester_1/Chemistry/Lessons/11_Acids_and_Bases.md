# Acids and Bases

- **Autoionization** is when molecules in a substance produce ions.  
- For water it happens but not a lot.

For example:
> In water an H<sub>2</sub>O molecule will pull an H from another H<sub>2</sub>O molecule (reminder: the oxygen in water molecule has a partial negative charge and the hydrogen has a partial postitive charge thus attracting each other).  
> When pulling the _H_ from the H<sub>2</sub>O molecule, the electron stays with the now _HO_ mocule making it an **anion** since it has more electrons than protons. **HO<sup>-</sup>** is called **hydroxide**  
> The molecule that pulled the _H_ atom without its electron is now _H<sub>3</sub>O_ making it a **cation** since it has more protons than electrons. **H<sub>3</sub>O<sup>+</sup>** is called **hydronium**  
> The concentration of hydronium and hydroxide in regular water is quite small.  

 ![hydronium_hydroxide](./img/11_Acids_and_Bases/hydronium_hydroxide.png)

## Acids and bases by Arrhenius

- **Acids are molecules which will increase the number of protons of H<sup>+</sup> when dissolved in water.**  
For example:
> Hydrochloric Acid HCl, when put in water will disolve and its H<sup>+</sup> ion will bond with the H<sub>2</sub>O molecule to form H<sub>3</sub>O<sup>+</sup>:  
> HCl + H<sub>2</sub>O --> H<sub>3</sub>O<sup>+</sup> + Cl<sup>-</sup>  

- **Bases are moleculea which will increase the number of OH<sup>-</sup> when dissolved in water**.  
For example:
> Sodium Hydroxide NaOH, when put in water will disolve (as seen [here](#water-as-a-solvent)) and release OH<sup>-</sup>  
> NaOH + aqueous sol. --> Na<sup>+</sup> + OH<sup>-</sup>  

## Acids and bases by Bronsted-Lowry

- **Acids are proton/H<sup>+</sup> donors** _(not only in water)_.
- **Bases are proton/H<sup>+</sup> receivers** _(not only in water)_.

- **Conjugate Bases lost or donated a proton**
- **Conjugate Acids gained or acquired a proton**  

For example:
> HCl + H<sub>2</sub>O --> H<sub>3</sub>O<sup>+</sup> + Cl<sup>-</sup>  
> Cl<sup>-</sup> is a **conjugate base** of HCl since it lost the H<sup>+</sup> proton  
> H<sub>3</sub>O<sup>+</sup> is a **conjugate acid** of H<sub>2</sub>O since it gained a H<sup>+</sup> proton  

**_Note: in the parts above we consider proton==H<sup>+</sup> ion since the most common isotope of hydrogen has no neutron and as an ion has released its only electron_**  

**_Note2: water can act as an acid or base because  H<sub>2</sub>O--> H<sup>+</sup> & OH<sup>-</sup>_**

 ![acids and bases](./img/11_Acids_and_Bases/acid_base.png)
 
## Acid/Base pairs

![acids/bases pairs](./img/11_Acids_and_Bases/acid_base_pairs.png)

## Acid/Base in water

- Water is **amphoteric**: it is both an acid and a base
- **Acid in water** will make the **water the base** transformning it into its conjugate base $`\bold{H_3O^+}`$.
- **Base in water** will make the **water the acid** transformning it into its conjugate acid $`\bold{OH^-}`$.

|Acid in Water| Base in Water|
|--|--|
|![acids and bases](./img/11_Acids_and_Bases/acid_water.png)|![acids and bases](./img/11_Acids_and_Bases/base_water.png)|

## Strength of and acid/base

- The strength of an acid/base depends on the substance they react with.
- A strong base is a base that will get more easily protons.
- A strong acid breaks down entirely into ions (Arrhenius), gives protons easily (Bronsted-Lowry).
- **The stronger the acid, the weaker the base**.
- Strong acids to remember: **HCl, HBr, HI**. 
- HF isn't a strong acid because the bond is too strong to break.


 ![acids/bases strength](./img/11_Acids_and_Bases/acid_base_strength.png)

- In equilibrium, we can see the strength of an acid.

 ![equilibrium](./img/11_Acids_and_Bases/equilibrium.png)
 

## Acid dissociation constant $`K_a`$

- Measure the strength of an acid in a water solution.
- It is a private case of $`K_{eq}`$ since it applies the same logic but it works with acids.
- For strong acid, $`K_a`$ can't be found because they completely dissolve in water.
- **The concentration of water is so high that we don't use it in the constant calculation**.
- Its concentration is 55.555M.

```math
\begin{aligned}
&HA + H_2O \leftrightharpoons H_3O^+ + A^- \\
&K_c = \frac{[H_3O^+] \cdot [A^-]}{[HA] \cdot [H_2O]} \\
&K_a = K_c \times 55.555 = \frac{[H_3O^+] \cdot [A^-]}{[HA]}
\end{aligned}
```

## Dissociation of water $`K_w`$

- In neutral solutions (for example: pure water), the number of $`H^+ = OH^-`$ (where $`H^+ = H_3O^+`$).
- $`K_w`$ is the constant of the water and is the product of $`[H_3O^+]`$ and $`[OH^-]`$.

```math
\begin{aligned}
&\bold{H_2O_{(l)} + H_2O_{(l)} \leftrightharpoons H_3O^+_{(aq)} + OH^-_{(aq)}} \\
&K_c = \frac{[H_3O^+] \cdot [OH^-]}{[H_2O]^2} \\
&K_c = \frac{[H_3O^+] \cdot [OH^-]}{[55.555]^2} \\
&K_c \times [55.555]^2 = \text{constant} = K_w \\
&\bold{K_w = [H_3O^+] \cdot [OH^-]}
\end{aligned}
```
- When putting an acid in a solution like this, the concentration of $`H_3O^+`$ will be higher.
- When putting a base in a solution like this, the concentration of $`OH^-`$ will be higher.

 ![dissociation of water](./img/11_Acids_and_Bases/water.png)

### At $`25\degree C`$

- At $`25\degree C`$, $`\bold{[H_3O^+][OH^-] = K_w = 10^{-14}}`$
- In pure water $`\bold{[H_3O^+] = [OH^-] = 10^{-7}}`$

```math
\begin{aligned}
&[H_3O^+] = \frac{10^{-14}}{[OH^-]} \\
&[OH^-] = \frac{10^{-14}}{[H_3O^+]}
\end{aligned}
```

 ![dissociation of water molar](./img/11_Acids_and_Bases/water_molar.png)
 

## pH

- pH is a more comfortable scale than negative power of 10.
- pH measures the acidity of a solution on a scale of 0-14 (for 1M of solution).
- On the same principle we can measure the how basic a solution is.
- The p scale: **p = -log()** $`\Rightarrow`$ pX = -log([X])
- $`\bold{pH = -log[H_3O^+]}`$
- $`pOH = -log[OH^-]`$
- In **neutral solution** at $`\bold{25 \degree C}`$:

```math
\begin{aligned}
&K_w = [H_3O^+][OH^-] = 10^{-14} \Rightarrow [H_3O^+] = [OH^-] = 10^{-7}\\
&\hookrightarrow pH = -log[H_3O^+] = pOH = -log[OH^-] = -log(10^{-7}) = 7 \\
&\bold{pH = pOH = 7} \Rightarrow pH + pOH = 14
\end{aligned}
```

  ![pH](./img/11_Acids_and_Bases/pH.png)

## Weak Acids and Bases

- Strong acids have no Ka because they completely disolve in water (reaction goes only one way).
- Ka is only for weak acid.
- Same principle for bases where Kb is their constant.

### Weak Acid

- The higher the $`K_a`$ the stronger the acid.
- The lower the $`pK_a`$ the stronger the acid.
- In reaction between two acids, the strong acid (lower $`pK_a`$) will give its proton to the other acid (which becomes the base of the solution).

```math
\begin{aligned}
&HA_{(aq)} + H_2O_{(l)} \leftrightharpoons H_3O^+_{(aq)} + A^-_{(aq)} \\
&K_a = \frac{[H_3O^+][A^-]}{[HA]} \\
&\bold{pK_a = -log{K_a}}
\end{aligned}
```

### Weak Base

```math
\begin{aligned}
&B_{(aq)} + H_2O_{(l)} \leftrightharpoons BH^+_{(aq)} + OH^-_{(aq)} \\
&K_b = \frac{[BH^+][OH^-]}{[B]} \\
&\bold{pK_b = -log{K_b}}
\end{aligned}
```

_Note:**When the concentration of acid/base is lower than $`\bold{10^{-6}}`$ the concentration of water ions ($`\bold{H_3O^+}`$ and $`\bold{OH^-}`$) cannot be neglected!!!**_  
