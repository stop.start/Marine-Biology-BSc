# sp Hybridization

![hybridizations](./img/05_SP_Hybridization/hybridizations.png)

## Polar bonds

- A **polar bond** is when one atom is more electronegative than the other.
- A **polar molecule** is a molecule with a difference of charge on each end.
- A **dipole** is the separation of charge in a polar molecule (for example $`H_2O`$ has a dipole).
- A polar molecule in an electric field will turn to have its negative side facing the positive and vice versa. This is called **moment dipole**.
- A molecule with polar bonds but is **symetric** will not be polar as a whole thus won't have moment dipole.
- In order for a **molecule** to be **polar** it needs to have **polar bonds** and be **asymetric**.

|||
|--|--|
|![polar molecule](./img/05_SP_Hybridization/polar_molecule.jpg)|![dipole](./img/05_SP_Hybridization/dipole_h2o.png)|

## Hybridization

### How do atoms create the bonds between them?

Looking at an atom, for example C, its **electron configuartion** is $`1s^{2}2s^{2}2p^{2}`$.  
It looks like it's 2s orbital is full, so how does it bond with up to 4 other atoms?  
The atom moves its valence electrons so they can bond. In order to do that the **s and p orbitals will merge** in a process that is called **hybridization**.  
The energy of the new sp orbitals higher than the orbital s and less than orbitals p.  
Those hybridizations explains the geometry of the molecules since it gives the directions of the new orbitals. 
- **Hybridizing between 2 orbitals (one s and one p) will result in 2 new orbitals.**
- **Hybridizing between 3 orbitals (one s and two p) will result in 3 new orbitals.**
- **Hybridizing between 4 orbitals (one s and three p) will result in 4 new orbitals.**

![sp sp2 sp3](./img/05_SP_Hybridization/sp_sp2_sp3_orbitals.jpg)

### sp hybridization

- Hybridization between s and 1 p.
- Can bond with 2 other atoms.
- With this hybridization the **geometry** will be **linear**.

![sp](./img/05_SP_Hybridization/be_sp_hybridization.png)

- Example of $`BeH_2`$ geometry:

![BeH2](./img/05_SP_Hybridization/BeH2.png)

### $`\text{sp}^2`$ hybridization

- Hybridization between s and 2 p.
- Can bond with 3 other atoms.
- With this hybridization the **geometry** will be **triangular planar**.

![sp2](./img/05_SP_Hybridization/b_sp2_hybridization.png)

- Example of $`BF_3`$ geometry:

![BF3](./img/05_SP_Hybridization/bf3.png)

### $`\text{sp}^3`$ hybridization

- Hybridization between s and 3 p.
- Can bond with 4 other atoms.
- With this hybridization the **geometry** will be **tetrahedral**.

![sp3](./img/05_SP_Hybridization/c_sp3_hybridization.png)

- Example of $`CH_4`$ geometry:

![CH4](./img/05_SP_Hybridization/CH4.jpg)


### All hybridization for [C]arbon

- Atoms can have more than 2 valence electrons and still make an sp hybridization. In this case they'll make **double bonds** with the p orbitals that remained unhybridized.
- This applies for $`\text{sp}^2`$ as well.
- Below are all the hybridizations possible for carbon:

![carbon hybridizations](./img/05_SP_Hybridization/c_hybridizations.png)


### What about more than 4 bonds (more than an octet)?

- In order for an atom to make more than 4 bonds it needs orbital d which will create the remaining bonds after the sp hybridization.

## Types of bonds

- A **single covalent bond** is called **sigma** $`\sigma`$.
- A **sigma** bond is strong.
- In double and triple bonds there's always one sigma bond.
- The other bonds in double and triple bonds are called **pi** $`\pi`$.
- **Pi** bonds are weaker.

![sigma and pi bonds](./img/05_SP_Hybridization/sigma_pi_bonds.png)

### Sigma bonds

- Sigma bonds are bonds between either two s orbitals or s and p orbitals. 
- It can also be between p and p as long as it on the same imagenary "line" that goes trough the 2 nuclueses of the atoms.

### Pi bonds

- Pi bonds are bonds betwwen p orbitals.
- Those pi bonds aren't in straight line and are weaker.
- Those p orbitals aren't hybridized.
- One Pi bond is **above and below** a sigma bond.

> Example: Carbon  
> - When carbon bond with 3 other atoms it has two single bonds and one double bond.
> - Carbon will make and $`sp^2`$ hybridization which leaves one orbital p non hybridized.
> - The hybridized orbitals will make the 3 sigma bonds.
> - The last p orbital will create the pi bond.


## Covalent structures of Carbon

![carbon](./img/05_SP_Hybridization/carbon.png)

### Diamond

- Structure composed of carbons.
- All carbons have 4 bonds to other carbons (tetrahedrals bonded to other tetrahedrals).
- $`\text{sp}^3`$ hybridization.

![diamond](./img/05_SP_Hybridization/diamond.png)

### Graphite
 
- In graphite, each carbon atom is bonded to three others, forming layers. 
- The bonds between the layers are much weaker than covalent bonds. 
- Graphite can also conduct electricity, between the layers of carbon atoms.
- $`\text{sp}^2`$ hybridization.

![graphite](./img/05_SP_Hybridization/graphite.png)

### Buckminsterfullerene

- Formula $`C_{60}`$
- Looks like a football.

![fulren](./img/05_SP_Hybridization/fulren.png)

