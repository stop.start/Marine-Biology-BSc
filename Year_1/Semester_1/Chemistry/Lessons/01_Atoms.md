# Atoms

![atom](./img/01_Atoms/atom_icon.png)

## Hebrew/English Lexicon
| Hebrew | English                                | Definitions                                                              |
|--------|----------------------------------------|--------------------------------------------------------------------------|
| יסודות | **Elements**                           |                                                                          |
| מטען   | **Charge**                             |                                                                          |
|        | **Proton**                             | Subatomic particle with a positive charge                                |
|        | **Neutron**                            | Subatomic particle with a neutral charge (uncharge)                      |
|        | **Electron**                           | Subatomic particle with negative charge                                  |
|        | **Atomic number**                      | The number of protons in the atom                                        |
|        | **Nucleus**                            | The part at the center of the atom. It contains the protons and neutrons |
|        | **Neutral atom**                       | Atom with an equal number of protons and electrons                       |
|        | **Isotope**                            | Atoms with the same number of protons but different number of neutrons   |
|        | **AMU**                                | Atomic mass unit. Proton = Neutron = 1amu. Electron ~= 1/1800 amu        |
|        | **Mass number**                        | Number of protons + number of neutrons                                   |
|        | **Atomic mass**                        | ~= mass number                                                           |
|        | **Atomic weight\Relative atomic mass** | Average mass of the isotopes considering their natural abundance         |

## Basics
|   Atom diagram      |             Periodic table                  |          Atom representation           |
|---------------------|---------------------------------------------|----------------------------------------|
|![atom](./img/01_Atoms/atom.png)|![periodic_table](./img/01_Atoms/periodic_table.png)|![atom_rpr](./img/01_Atoms/atom_representation.jpg)|

- Atoms are defined by their number of protons. _For instance an atom with 6 protons is a Carbon atom (C), an atom with 2 protons is a Helium atom (He)_.
- The mass of the atom is mostly in its centre - **the nucleus** (protons + neutrons). The electrons mass is considered negligible.
- Charges in the atom:
  - **Protons are positive**
  - **Neutrons are neutral**
  - **Electrons are negative**
- **Nuclear force (כוח גרעיני)** is what makes protons, which are positive, stick together (to a certain point). Neutrons help with this as well, mainly when there are a lot of protons.
- The charge (מטען) isn't connected to the mass of a particle: electrons have a negligible mass compared to the protons but their charge are equally opposite.
- Periodic table shows the atoms ordered by **atomic number**, meaning it is ordered by the number of protons the atom has.
- Atoms have all protons,neutrons and electrons particles except for Hydrogen (H) which has 1 proton and 1 electron.
- An atom with a **different number a neutrons** than protons are **isotopes**.
- Some **isotopes** are stables and some are not. Those unstable isotopes are called **radioisotope**.They release particles in order to get to a stable form (decay). _For example Carbon14 will become Nitrogene14 (part of the decay process, which is less described for now)_.
- **Angstrom** is the unit of measure used at the atom scale. **$`1\text{AA} = 10^{-10}m = 10pm`$**. 
- **Electron Volt** is the unit used to measure energy regarding electrons.

## Electrons
- The electrons are "buzzing" around the nucleus. Their exact position isn't known. Because of their negative charge they are attracted to the protons, but they also repel each other.
- When electrons are exited (given energy) they enter a higher **energy level** (or energy  state). Basically they go farther away from the nucleus.
- Electrons can be attracted to other atoms and, when an electron is swiped away from the atom, then the atom has a **positive charge**. The atom that pulled the electron has a **negative charged**.
- **Ionazation energy** is the energy required to pull an electron from the atom making it an ion (cation).

### Orbitals

- The state of an electron is determined by 4 quantic numbers:
  - **n**: Main quantic number. It is the layer (רמה) where the electron is. **From 1 to infinite**.
  - **l**: Quantic number of the angle. Shape of the orbital. **From 0 to n-1** which are given names s,p,d,f (the main ones from 1 to 4).
  - **$`m_l`$**: Magnetic quantic number. Which orbital the electron might be. **From -l to +l**.
  - **$`m_s`$**: Quantic number of the spin. **Either 1\2 or -1\2**.
  
  _Examples:_
  
  ```math
  \begin{aligned}
  n&=1 \implies l=\quad 0 \implies s \implies m_l=0
  \\
  n&=2 \implies l=\begin{cases}
  0 \implies s \implies m_l=0\\
  1 \implies p \implies m_l=-1,0,1
  \end{cases}
  \\
  n&=3 \implies l=\begin{cases}
  0 \implies s \implies m_l=0\\
  1 \implies p \implies m_l=-1,0,1\\
  2 \implies d \implies m_l=-2,-1,0,1,2
  \end{cases}
  \\
  n&=4 \implies l=\begin{cases}
  0 \implies s \implies m_l=0\\
  1 \implies p \implies m_l=-1,0,1\\
  2 \implies d \implies m_l=-2,-1,0,1,2\\
  3 \implies f \implies m_l=-3,-2,-1,0,1,2,3
  \end{cases}
  \end{aligned}
  ```

**Table of electrons orbitals configurations:**

![atomic_orbitals](./img/01_Atoms/atomic_orbitals.png)

- On the above table, each **row** represents an energy **shell** which correspond also to the periodic table **periods**. **Columns** represent **orbitals**.
- **Subshells** contains one or more **orbitals**.
- The different **subshells** are named **s,p,d,f** (there are more but those are the main ones).
- Orbitals p,d,f have directions (for example $`p_x, p_y, p_z`$). Orbital s is spheric thus doesn't have a direction.
- Each **orbitals** can contain **at most 2 electrons**.
- The orbitals can have weird shapes! Here's an example of an atom with p orbitals:  

![s_p_orbitals](./img/01_Atoms/s_p_orbitals.png)

- When looking at the p,d or f orbitals the shapes are the same but in a different inclination. For example the d orbitals are in an 3 dimensional plan (x,y,z).
- The **d subshell backfills**. It means that when s subshell will start to get fill in the `n` subshell, the `n-1` d subshell will start to get filled (for `n>=4`). _For example, Iron (Fe) configuration is [Ar]4s<sup>2</sup>3d<sup>6</sup> ([Ar] means Argon configuration which is 1s<sup>2</sup>2s<sup>2</sup>2p<sup>6</sup></sup>3s<sup>2</sup>3p<sup>6</sup>)_
- The d subshell backfills because this orbital is considerably higher in energy than the 3s and 3p orbitals and does not begin to fill until the fourth row of the periodic table.

> _Explaination for d subshell:_  
>  
> | Subshell | n (energy level) | l (orbital type: s,p,d,f) | n+l (relative energy) |
> |----------|------------------|---------------------------|-----------------------|
> | 1s       | 1                | 0                         | 1                     |
> | 2s       | 2                | 0                         | 2                     |
> | 2p       | 2                | 1                         | 3                     |
> | 3s       | 3                | 0                         | 3                     | 
> | 3p       | 3                | 1                         | 4                     |
> | 3d       | 3                | 2                         | 5                     |
> | 4s       | 4                | 0                         | 4                     |
> | 4p       | 4                | 1                         | 5                     |
> | 4d       | 4                | 2                         | 6                     |
> | 4f       | 4                | 3                         | 7                     |
>  
> - Orbitals 2p and 3s have the same energy (3) but 2p is in lower energy level thus the electron will first enter it.  
> - Orbital 3d has a higher energy than 4s despite being in a lower energy level. That's the reason it backfills: electrons will enter first 4s then 3d.  
> - After an electron enters 3d orbital, the energy of 3d decreases a bit and the energy of 4s increases a bit.  

### Electron configuration

#### How the electrons are configured in the different orbitals?

- Electrons are set from the lower energy level to the highest needed for the number of electrons in the atom.
- **Hund's rule** (or the bus seat rule): electrons are set first alone in the orbitals before being paired up.

> _Examples (where $`\uparrow`$ and $`\downarrow`$ are the spin (+/- 1/2)):_  

 > | Atom | Atomic Number | 1s                     | 2s                     | $`2p_x`$               | $`2p_y`$               | $`2p_z`$               | 3s           |
 > |------|---------------|------------------------|------------------------|------------------------|------------------------|------------------------|--------------|
 > | He   | 2             | $`\uparrow\downarrow`$ |                        |                        |                        |                        |              |
 > | C    | 6             | $`\uparrow\downarrow`$ | $`\uparrow\downarrow`$ | $`\uparrow`$           | $`\uparrow`$           |                        |              |
 > | N    | 7             | $`\uparrow\downarrow`$ | $`\uparrow\downarrow`$ | $`\uparrow`$           | $`\uparrow`$           | $`\uparrow`$           |              |
 > | Na   | 11            | $`\uparrow\downarrow`$ | $`\uparrow\downarrow`$ | $`\uparrow\downarrow`$ | $`\uparrow\downarrow`$ | $`\uparrow\downarrow`$ | $`\uparrow`$ |

_Examples of electron configuration written without arrows:_

> Hydrogen (H): **1s<sup>1</sup>** - meaning that its electron is in the **s subshell** of the **first shell**  
> Lithium (Li): **1s<sup>2</sup>2s<sup>1</sup>** - meaning that its electrons fill the **first shell** (2 electrons) and the third one is in the **s subshell** of the **second shell**  
> Carbon (C):   **1s<sup>2</sup>2s<sup>2</sup>2p<sup>2</sup>** - meaning that its electrons fill the **first shell** (2 electrons), fill the **s subshell** of the **second shell** (2 electrons) and fill the **p<sub>z</sub> subshell** of the **second shell** (2 electrons)  
> Nitrogen (N):   **1s<sup>2</sup>2s<sup>2</sup>2p<sup>3</sup>** - meaning that its electrons fill the **first shell** (2 electrons), fill the **s subshell** of the **second shell** (2 electrons) and 3 electrons in the **p subshell** of the **second shell** (2 in p<sub>z</sub> 1 in p<sub>x</sub>)  

- Because the 3d orbitals have a higher energy than 4s, but the energy changes as the electrons enter the orbitals, it gives some unexpected results: Chromium and Copper have a special configuration:


 | Atom | Atomic Number | Electrons Configuration                             | $`3d_x`$               | $`3d_y`$               | $`3d_z`$               | $`3d_w`$               | $`3d_t`$               | 4s           |
 |------|---------------|-----------------------------------------------------|------------------------|------------------------|------------------------|------------------------|------------------------|--------------|
 | Cr   | 24            | $`[Ar]3d^{5}4s^1`$ (expected $`[Ar]3d^{4}4s^2`$)    | $`\uparrow`$           | $`\uparrow`$           | $`\uparrow`$           | $`\uparrow`$           | $`\uparrow`$           | $`\uparrow`$ |
 | Cu   | 29            | $`[Ar]3d^{10}4s^1`$   (expected $`[Ar]3d^{9}4s^2`$) | $`\uparrow\downarrow`$ | $`\uparrow\downarrow`$ | $`\uparrow\downarrow`$ | $`\uparrow\downarrow`$ | $`\uparrow\downarrow`$ | $`\uparrow`$ |

- Those configuration are more stable than the expected ones.

### Radius

- The **atomic radius** is the distance between the nucleus and the last electron layer.
- The **atomic radius** depends on the number of protons and the number of electrons present in the atom.
- The more protons there are, the stronger the hold on the electrons.
- The more electrons layer there are, the farther the electrons are from the nucleus where the protons are.
- For each period (line) in the periodic table, the **atomic radius** get **smaller from left to right** since the electrons are all on the same layers but the number of protons get higher thus the atoms on the right hold the electrons closer.
- For each column in the periodic table, the **atomic radius** get **bigger from top to bottom** since there are more layers each period (line) thus despite having more protons its harder to hold the electrons closer.

![atomic radius](./img/01_Atoms/radius.png)

