# Stoichiometry of Solutions

## Intro

- A **solution** is an **homogenous mixture** of two or more substances.
- The **solvent - הממס** is the substance which is in the biggest amount.
- The **solutes - המומסים** are the substances in lower quantity.
- Usually the solvent is **liquid**, the solutes can be in all state (solid, liquid, gaz) and the solution is **liquid**.
- **Polar solvents will dissolve polar solutes**.
- **Non polar solvents will dissolve non polar solutes**.

| Polar solvents | Non polar solvents |
|----------------|--------------------|
| Water          | Oil                |
| Alcohol        | Benzin             |
|                | Gasoline           |

- **Water** is considered a **universal solvent** because it can dissolve:
  - **Polar substances** by creating hydrogen bonds. Those solutions aren't conductive.
  - **Ionic compounds** (salts). Those solutions are conductive.

## Concentration

- **Molarity**, also called **molar concentration C(M)** is the **quantity of solute in moles** for **1 Liter of solution**.

```math
C(M) = \frac{n (\text{in moles})}{V (\text{in liter})}
```

### Dilution

- **Dilution** change the concentration of a solute by added solvent.
- **The number of moles of the solute doesn't change in the solution just its concentration**.
- **Dilution equation**:

```math
C_1(M) \cdot V_1 = c_2(M) \cdot V_2
```
