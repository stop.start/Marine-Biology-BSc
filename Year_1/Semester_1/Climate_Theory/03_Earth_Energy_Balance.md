# Earth Energy Balance

## Hebrew/English Lexicon

| Hebrew    | English    | Definition                                                           |
|-----------|------------|----------------------------------------------------------------------|
| אפיק      | Trough     | Low pressure system                                                  |
| זרם הסילן | Jet stream | Strong air currents flowing at a distance around 10km from sea level |


## A bit of info

- A **trough (אפיק)** is a low pressure system (like depression) which allows clouds to form.

> Red sea trough - **אפיק ים סוף** is a low pressure system in Israel.  

- Small pressure systems are less predictable than bigger ones. 

## Energy balance

- Some areas get more radiations than others. Some have a **surplus (עודף)** of radiations and some have a **deficit  (חוסר)** in radiations.
- The above takes into consideration the radiations emited by the earth.
- **51%** of the radiations (either direct or diffused) are **absorbed by the ground**.
- **19%** of the radiations are **absorbed by the atmosphere**. Those warm the atmosphere but not the Earth itself.
- The average life span of a cloud is 30 minutes:
  - Water vapor condensed into a cloud
  - If the water drops are big enough the cloud "empty" as rain
  - If the water drops aren't big enough the water will evaporate back to the atmosphere
- **ITCZ**: intertropical convergence zone. Band of big clouds in the middle of the oceans around ($`10\degree`$ North and South) the **equator**. It causes a lot of rain and is mainly responsible for the **planetary albedo**.
- **No matter the temperatures, if there's water vapor and the air is warmer than its surrounding it will go up and create clouds.**
- **Earth releases energy**. 62% stays in the atmosphere (greenhouse effect) and the rest is released to space.
- Earth releases mainly infrared radiations.
- **Our atmosphere reacts well with longwaves length radiations**. That's why it reacts well with the energy released by Earth.
- **The radiations released by Earth stays pretty much the same during the year and all around the globe**.
- Because of the above, some areas have a **surplus** of radiations (emit more radiations than receive) and some have a **deficit** of radiations (receive more than emit).
- The **energy balance** on Earth is kept via **air streams** (winds) and **water currents**. 
