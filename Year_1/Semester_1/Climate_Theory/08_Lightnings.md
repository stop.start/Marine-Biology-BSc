# Lightnings

## Where does it happen?

- Most lightnings happen **between clouds** that are next to each other rather than to the ground.
- Some lightnings happen from the cloud upward (we don't know why).

## What is lightning?

- Lightning is an **electrical discharge (התפרקות חשמלית)** inside a cloud (cumulunimbus).
- Within a cumulunimbus with strong vertical winds there's **dipole structure** that forms: **positive charge on the top** and **negative charge on the base** of the cloud (we don't know why).
- At some point, a spark happens (we don't know why) and the lightning happen.
- **Lightnings bring down the electric charge**.
- It is a fast process: 0.2s.
- At any time there are around 1800 lightning storms.
- After the lightning happens the cloud isn't in a dipole structure anymore but it will **go back to be in a dipole structure** again and again.

## How does it happen?

- The negative charge accumulates at the bottom of the cumulunimbus.
- A spark (ניצוץ) happens and it starts going down (or other direction) creating a **channel of negative charge**.
- In the **channel of negative charge** two things happen:
  - **Thunder**: Negative charges meet positive make a "boom" sound (מכת הלם). The temperature at this point is $`\bold{2700\degree C}`$ and warms the channel.
  - **Lightning**: The negative charges **ionize** particles around the channel (מכת ההחזר).
  
