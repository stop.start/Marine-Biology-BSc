# Climate Theory

1. [Solar System](./01_Solar_System.md)
2. [Earth and Seasons](./02_Earth_and_Seasons.md)
3. [Energy and Balance](./03_Earth_Energy_Balance.md)
4. [Atmospheric Pressure](./04_Atmospheric_Pressure.md)
5. [Atmosphere](./05_Atmosphere.md)
6. [Atmospheric Stability](./06_Atmospheric_Stability.md)
7. [Clouds](./07_Clouds.md)
8. [Lightnings](./08_Lightnings.md)
9. [Fog](./09_Fog.md)
10. [Atmospheric Dynamics](./10_Atmospheric_Dynamics.md)
11. [Global Circulation](./11_Global_Circulation.md)
12. [Deserts](./12_Deserts.md)
13. [Effect of the Sea on Land](./13_Effect_of_the_Sea_on_Land.md)
14. Extreme Weather: 
     - [Sandstorms](./14_Sandstorms.md)
     - [Tornados & Hurricanes](./15_Extreme_Weather.md)
