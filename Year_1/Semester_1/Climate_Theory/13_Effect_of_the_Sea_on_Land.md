# The Effect of the Sea on Land

## Intro

### What causes currents?

1. Strong winds (trade and westerlies).
2. Water density

---

- The radiations from the Sun are not equally distributed on Earth.
- The **atmospheric circulations** (60%) and **oceanic circulations** (40%) move energy from zone with a surplus to zone with a deficit.
- The **trade winds (רוחות הפסת)** and the **westerlies (מערביות)** are constant winds around the globe that drag along the superior layer of sea water create **currents**.
- The currents are up to **100m deep** and the **speed at the surface** is between **10cm-1m/sec**.
- There are 5 main **circular ocean currents**:
  - 2 in the Atlantic ocean.
  - 2 in the Pacific ocean.
  - 1 in the Indian ocean.
- The circular currents in the **northen hemisphere** move **clockwise** and in the **south hemisphere counterclockwise**.

  ![circular currents](./img/13_Effect_Sea_Land/circular_currents.png)
- **Denser water** will stream close to the ground. **Less dense water** will stream closer to the surface.
- The density of the water is set by:
  - Its temperature: colder water is more dense.
  - Salt concentration: the more salt the more dense.

 
### Ekman spiral

- The currents are affected by the Coriolis effect.
- The water at the surface stream at $`45\degree`$ from the wind direction.
- The deeper under the surface the currents direction changes due the the weakened coriolis effect.
- The spiral can reach up to 100m/200m.

 ![ekman spiral](./img/13_Effect_Sea_Land/ekman.png)

## Big currents
- 3 of the circular currents are:
  - Gulf stream
  - Peru current
  - El Nino

![currents](./img/13_Effect_Sea_Land/currents.png)

### Gulf stream

- The **Gulf stream** is a current of **warm water** ($`27\degree-28\degree`$).
- It flows **from Florida** and cross the Atlantic up to **Europe**.
- It flows between 30-80 million mm/sec.
- Because it's a warm current, it **increases the temperature in Europe**: without it the temperature in England would be the same as the temperature in Canada (cost).
- The current being warm also **increases the precipitations in Europe**: because of it Norvegia has more rain than Canada (cost), but Sweden isn't affected by the gulf stream it has a lot less rain than Norvegia (around the same amount as the cost of Canada at the same latitude).

 > _Note:_  
 > Because of global warming, ice from Greenland melts and the forecast is that all this cold water will push down the gulf stream making England colder and Portugal warmer.

  ![gulf stream](./img/13_Effect_Sea_Land/gulfstream.png)

### Peru current

- The **Peru current** - also called **Humbolt** - is in the Pacific on the South America costs.
- It's a **cold current**.
- It's a northen extension of the Antartic current.
- How the stream is form:
  - In Peru the winds are **southern winds** (which then become eastern winds close to the equator).
  - Those southern winds are affected by the coriolis effect making them go **left**.
  - This make the current going **from the beach zone in Peru to the ocean**.
  - In order to replace the water removed by the stream, **cold water comes from the deep (upwelling) from the antartic**.
- This current causes the **temperature in Peru to be colder** than they would be without it.
- It also causes **anticyclones which decreases the amount of precipitations**

  ![peru current](./img/13_Effect_Sea_Land/peru.png)
  
### El Nino

- Happens once a year around Christmas and last about a month.
- **El nino is the event of the water at the surface of the ocean.**
- All the different systems (like ITCZ) are below the equator which weakens the south-eastern winds thus the upwelling in south america isn't happening.
- Without the upwelling the anticyclone above Peru is weakened allowing for some rain (not a lot).
- The warmer zone that is usually around Australia moves in the middle of the Pacific moving with it the zone where it rains.
- It also causes an increase in temperature due to the warmer weather in south america.
- **Once in a few years, El Nino last longer than a month (up to a year). This is called "year El Nino"**.

  ![el nino](./img/13_Effect_Sea_Land/el_nino.png)
  
  ![el nino](./img/13_Effect_Sea_Land/el_nino2.png)
  
#### Phenomenons caused by El Nino

- Increase in pressure in the Indian ocean, Indonesia and Australia.
- Decrease in pressure in Tahiti, center and east of the Pacific ocean.
- Weaker winds in the Pacific ocean or change of direction to the East.
- Warm air rises above Peru causing rain in the Peru deserts.
- Warm currents from West Pacific ocean to East Pacific ocean.

#### Implications of El Nino

- Expansion of the warm water to the east and along with it rain.
- Mansoon rain in the middle of the Pacific instead of west of the Pacific causing drought in the west.
- Rainstorms in the Pacific islands, typhons struck Tahiti and Hawai.
- Increase of around $`5\degree C`$ around Peru beaches.
- Rain in Peru.
- In Australia and Borneo, big fire and drought.
- In year El Nino, there are less hurricanes in the Atlantic ocean.

### La Nina

- After year El Nino, when returning to its normal state, the trade winds are stronger, and pushes the warm water more to the west.

  ![la nina](./img/13_Effect_Sea_Land/la_nina.png)

