# Clouds

## Intro

- The average cloud is 30K tons.
- Clouds don't fall because the hot air is always going up and support the drops.
- Inside clouds there are always air streams going up and down (that can be felt when flying: turbulence).
- Most of clouds don't rain.

## What makes the air rise?

- **Convection streams**: air is hotter than its surroundings so it rises.
- **Orographic lift (עליה אורוגרפית)**: cold air that run into an obstacle like a mountain thus cannot continue to move horizontally, will be pushed upward despite being colder than its surroundings. At some point it will get to its dew point and a cloud can be formed (if there's water in the air block).
- **Mesometeorologic circulation (סירקולציה מזומטאורולוגית)**: (synoptic circulation scale = thousands of km - ex: hurricane, meso meteorologic scale = hundreds of km, ex: breeze). Cycle of hot air that goes up which enables cold air to come from cold source (like oceans).
- **Front (חזית)**: Two blocks of air (one hot, one cold) that collide. The hot air come first, then the cold air: cold front. Cold air comes first, hot air replaces it: warm front. The clouds formed are different for the hot air and the cold air.

## Clouds types

### Types by formation
- There is 3 types of clouds:
  - **Cumulus (ערימתיים)**: 
    - Big clouds (can be several km high), like cotton. 
    - Formed from fast convection streams.
    - Rain for the most part.
    - Name when raining: **cumulunimbus**.
  - **Stratus (שכבתיים)**:
    - Very long (several km long) and thin.
    - Formed from slow rise of air.
    - Don't rain much.
    - Name when raining: **nimbus stratus**.
  - **Cirrus (צירוס)**:
    - Made from ice.
    - Exist in the higher troposphere.
    - Don't rain (so no name for rain).

- There can be clouds mixed: cirrus-stratus

### Types by height in the atmosphere
Clouds are defined low, medium, high by their lowest point.

- **Low**: under 2km. Ex: low cumulus, low stratus.
- **Medium**: between 2km-5km.
- **High**: above 5km.

We know what a cloud contain by its height.

### Types by temperature
- Warm clouds: contain only water dropplets.
- Mixed clouds: contain water dropplets and some ice blocks.
- Cold clouds: from 40km and above, only ice.

## Water drops in clouds

- **Water drops in clouds are big enough ($`10\mu m`$) to be stable: the number of molecules that vaporize == the number molecules that condense**.
- In order to rain, the drops need to get bigger and bigger (at least 1mm) until they fall.
- The drops can get bigger by colliding into one another.
- **טיפה עוברית**: drop that can keep its size (doesn't vaporize).
- How **טיפה עוברית** are formed?
  - **Homogeneous nucleation**: condensation without any particles or surface (doesn't happen naturally because it needs too many molecules).
  - **Heterogeneous nucleation**: condensation with help of particles called **aerosols**: the water molecules "find" a particle to attach on. 
- **Aerosols**: 
  - Origins from volcans erruption, fire smoke, salt from the sea, dust from meteors etc.
  - There is 5 times more aerosols above land than above the sea. 
  - The most common aerosol above the sea is sodium.
  - If there is too much aerosol in the air then water drops won't form because there won't be enough water vapors per aerosol particle thus they will evaporate quickly.
  
## Ice in clouds

- Happens in the top part of the cloud when the temperature is below $`0\degree`$.
- There are two ways for ice to form in a cloud: **heterognous freezing** and **homogenouse freezing**.

| Homogeneous freezing                                                                 | Heterogeneous freezing                                                               |
|--------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------|
| temperature<-$`-40\degree`$                                                          | $`0\degree`$>temperature>$`-40\degree`$                                              |
| No need for particles to attach on in order to freeze - water vapors freeze directly | Need particles **גרעין קפאון** (same as for drops to form) for the water vapors to attach on in order to freeze - **deposition** |

- Not all dropplets will freeze to ice.
- There are no two identical snow crystals.
- Ice in clouds can fall as **ice or rain** depending on the **temperature at the base of the cloud**: if the temperature is under $`0\degree`$ then it will fall as snow, above $`0\degree`$ snow and hail will fall as rain.
- The structure of the crystals depends on the temperature and the water saturation.

| Temperature | Structure         |
|-------------|-------------------|
| 0 to -4     | thin plates       |
| -4 to -6    | needles           |
| -6 to -10   | columns           |
| -10 to -12  | plates            |
| -12 to -16  | dentrites, plates |
| -16 to -22  | plates            |
| -22 to -40  | hollow needles    |

![snow crystals](./img/07_Clouds/snow_crystals.png)

- Water molecule will prefer to freeze on a ice crystal rather than on the water drop: the vapor pression is higher on ice than on the drop.
- How do the crystal grow?
  - **Deposition**: More and more water vapor freeze on an existing crystal.
  - **Aggregation**: Existing crystals moving within the cloud bump into each other and stuck together. The crystal won't look pretty.
- **Riming** is the process of a **super cooled drop (טיפה בקירור יתר)** freezing on an ice crystal. This produce a **Graupel particle** which is like soft hail.
- **Hail (ברד)** is an intensified riming process in a **cumulunimbus** cloud with developed **vertical wind streams**: the graupel particule freeze and defreeze multiple times creating hail.
 
## Summary

### Types of drops per cloud

| Type of cloud | Vertical wind streams | Type of drops                                  |
|---------------|-----------------------|------------------------------------------------|
| Stratus       | Weak                  | Small drops will fall                          |
| Cumulus       | Strong                | Big drops will fall (the small ones evaporate) |

### Type of precipitations

| Precipitation name | Description                                                                                                  |
|--------------------|--------------------------------------------------------------------------------------------------------------|
| Drizzle (רסס)      | 1mm/hour - From stratus - it still rains because the temperature is low enough for the drop to not evaporate |
| Rain (גשם)         | From cumulunimbus - Average drop size                                                                        |
| (מטר)              | 10mm/hour - From cumulunimbus with strong winds - Big drops                                                  |
| Hail (ברד)         | מטר that went up and freezed a number of times to ice balls                                                  |
| Snow (שלג)         | Snow crystals created from growing ice crystal by deposition or aggregation (התלכדות)                        |
| Virga (וירגה)      | Precipitations that don't reach the ground                                                                   |

_Note: In Israel we mainly have מטר and the drops and usually drops that started as ice and melted_

## TODO
- Picture for each type of cloud
