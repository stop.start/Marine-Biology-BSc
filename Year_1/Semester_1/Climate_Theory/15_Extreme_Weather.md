# Extreme Weather 

## Microburst - פרץ רוח

- **Microburst - פרץ רוח** happens from air streams coming from **cumulonimbus**.
- In cumulus clouds there always are **vertical air streams** that go **up and down**. Those vertical streams help create the rain drops in the cloud.
- **Microburst are strong vetical air stream going downward under the cumulonimbus cloud base**.
- The vertical stream from the cloud comes down and break on the ground creating winds all around.
- This air stream can be:
  - **Humid**: it brings down water drops with it.
  - **Dry**: the water dropplets evaporate before reaching the ground.
- The **wind** coming from the microburst is **up to 70 knots** (around 40 knots on average).
- Its size can be from **a few meters and up to 1km**.
- Can last from **a few seconds and up to a few minutes** depending on the strength of the stream.
- In **dry microbursts** the fact that water dropplets evaporate requires **latent heat - חום כמוס** which **cools down the air**. The **cooling down causes the air stream to be faster**.

   ![dry microburst](./img/15_Extreme_Weather/microburst.png)

## Tornado

- **Tornado is a land storm** in specific areas and is very violent.
- The **wind** in the tornado is **over 300km/h** (up to 560km/h) and sucks everything on its path.
- It starts in a split second and can last from a few seconds up to half an hour (**10min on average**).
- It travels around 7km at 35-70km/h.
- It is the most common between **March-June**.
- It is formed from clouds called **supercell**.
- **Supercell** clouds are **very big cumulonimbus**. They can be **up to 10km high**.
- Supercell clouds have inside **vertical winds** that are separated: **upward winds on one side and downward winds on the other**. This means that there's no (or a lot less) friction to **slow down the upward winds**.
- Because of the strong air streams, the **precipitations** from it are **frozen (hail)**: big cloud that gets to high altitude which causes the water dropplets to freeze and the strong winds transform them into big hail.
- **15% from supercell clouds form tornados**.
- Tornados are measured on the **Fujita's scale** from EFO to EF5.
- There are more and more tornados

### How are supercell clouds formed?

- It needs a certain topography which exist in the central US, although there have been tornados in Asia and Australia.
- Collision between **hot & humid air** and **cold & dry air** (frontal depression - **שקע חזיתי**).
- **Unstability**: heat in low altitude and cold in high altitude (thus air goes up).
- Winds cut - **גזירת רוח**: sudden change in the main wind stream where smaller streams start to form with **different speed and direction**.
- Supercell clouds can last from **up to 12 hours** and can move **hundreds of kilometers**.
- One supercell clouds can create more than one tornado.

 ![supercell formation](./img/15_Extreme_Weather/supercell_formation.png)

#### Anatomy of a supercell cloud

- On top the supercell cloud has an **anvil - סדן** shape.
- From the anvil there's an **overshooting top** caused by the tornado.
- The precipitations from the cloud are usually hail.

|||
|--|--|
|![supercell anatomy](./img/15_Extreme_Weather/supercell_anatomy.png)|![supercell](./img/15_Extreme_Weather/supercell.png)|

### How are the tornados formed?

- The winds cut - **גזירת רוח** cut the **upward winds** in the supercell cloud (at high altitude).
- This causes a **low pressure area** to form which "needs" to be filled.
- The filling of this low pressure area starts from the top of the low pressure area down to the ground.
- The air streams from the ground up to the anvil of the cloud are strong and push upward the vertical stream of the clouds thus forming the overshooting top.
- At the center air going downward.
- The diameter of the tornado (low part) from a few meters up to a few hundreds meters.
- The low part of the tornado is dark because of all dirt is sucks with the winds.
 
  ![tornado](./img/15_Extreme_Weather/tornado.png)
  
## Mini Tornados - נד מים

- We can see those in Israel.
- It's the same physical results but the winds are a lot weaker.
- Happens above water.

## Hurricanes

- **Hurricanes are sea storms**.
- Water vapors above the sea fuel the hurricane.
- When the hurricane comes above ground it is weakened from lack of humidity and from friction. It can take between a few hours up to 2 days to weaken the hurricane.
- It is a **tropic depression** that get stronger into **tropical storm** which get stronger into a **hurricane**.
- In order for a hurricane to form it needs:
  - **Large area to develop**.
  - **Water above $`\bold{26.5\degree C}`$**.
  - **A lot of humidity at low altitude**.
- The area around the ITCZ in the oceans fits those criterias.
- In those areas there are the **trade winds - רוחות הפסת** which take the hurricanes to the West.
- Hurricanes are called:
  - Hurricanes in America
  - Cyclones in Australia and Indian oceans.
  - Typhoons in Asia
- Hurricanes are measured on the **Saffir-Simpson scale**.
- Hurricanes develop around the **tropics** from the **low thermal pressue**.
- The hurricanes winds are around **100 knots**.
- **Up to 2000mm/day precipitations**.
- The strong winds create big waves (6-8m high).
- Weak to no winds cut at high altitude.
- **Latent heat** increases the thermal effect thus fueling the hurricane.
- The water is the warmest at the **end of the summer** thus it is the season where hurricanes form.
- The **eye of the storm - עין הסערה**  around **20-50km** is an **anticyclone**: the air goes down preventing the formation of clouds and rain thus being the calmest part of the hurricane.
- **The closest to the eye to stronger the winds and precipitations**.

  ![hurricane winds](./img/15_Extreme_Weather/hurricane_winds.png)
