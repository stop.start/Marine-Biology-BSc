# Global Circulation

- Reminder: Energy (Sun's radiations) is not distributed equally on Earth.
- Because of that fact, Earth moves energy from areas which more energy to areas with not less energy.
- There are two ways for Earth to move the energy: **oceans circulation** (next lesson) and **air circulation** (this lesson).


## Pressure systems

- Pressure systems are highs and lows (רמה ושקע).
- There are two ways for them to be created: **thermal systems** and **dynamic systems**.

### Thermal systems (מערכות תרמליות)

Caused by changes in temperatures:  
- **Depression** (שקע):
  - Hot area will heat the air above it which will make it rise.
  - Hot air rising will create a depression.
  - This depression is a **hot thermal depression**.
- **Anticyclone** (רמה):
  - In cold areas, the air converges and descend.
  - This is a **cold thermal anticyclone**.
  
### Dynamic systems (מערכות דינאמיות)

Caused by convergence of streams or by collision of air blocks with different temperatures:  
- Convergence of air streams causes anticyclones (increases the pressure at the certain point)
- Collision of air blocks with different temperatures causes the air to go up which creates a depression.

## ITCZ - Inter Tropical Convergence Zone

- The equator receives the most radations thus it is the hottest area on Earth.
- The heat released there causes a thermal depression which creates clouds.
- The clouds are created when the temperatures are the highest (in the afternoon) thus the precipitations there are mostly in the afternoon.

## Air streaming cells

There are 3 cells of air streams in the northen hemispehere:
- **Hadley Cell** between $`0\degree`$ and $`30\degree`$
- **Ferrel Cell** between $`30\degree`$ and $`60\degree`$
- **Polar Cell** between $`60\degree`$ and the pole

![summary](./img/11_Global_Circulation/summary.png)

### Hadley Cell $`0\degree - 30\degree`$

- The air in the ITCZ rises due to the high temperatures creating a **hot thermal depression**.
- Creates clouds and precipitations.
- When the air reaches the atmosphere (10km), it starts to move to the north $`30\degree`$ latitude (קו רוחב) and $`30\degree`$ south.
- In the northen hemispehere at latitude $`30\degree`$, the air **converges** because this latitude is a lot smaller than the equator thus creating a **dynamic anitcyclone**.
- This a a hot dynamic anticyclone because latitude $`30\degree`$ is still hot although less than the equator.
- This anticyclone is called **subtropical anticyclone** (הרמה הסאבטרופית).
- On the ground of latitude $`30\degree`$ this anticyclone causes the deserts. This is called **רצועות המדבריות עולמית**.
- Part of the air on the ground goes back to the equator and part of it continues to the latitude $`60\degree`$.

 ![hadley cell](./img/11_Global_Circulation/hadley_cell.jpg)

#### Passat winds

- On the ground around the equator there are the **trade winds (רוחות הסחר)** - **passat** (הפסטים).
- Passat winds are north eastern winds in the northen hemispehere and south eastern winds in the south hemispehere.
- The passat winds comes from the north (latitude $`30\degree`$) in the direction of the equator and due to the **coriolis effect** they go to the right (west) thus being north-eastern winds (same for the sourth but mirrored).

 ![passat winds](./img/11_Global_Circulation/passat.png)


### Polar Cell $`60\degree - 90\degree`$

- The temperatures on the poles are really low thus the air converges and descend to the ground.
- This creates a **cold thermal anticyclone** called **polar anticyclone - רמה פולרית**. That's the reason there is almost no rain/snow in the poles (those are considered deserts).
- The air descending and goes all around in direction of latitude $`60\degree`$ on the ground
- At latitude $`60\degree`$ the air goes up. Part of it goes back to the poles and part to the latitude $`30\degree`$

 ![polar winds](./img/11_Global_Circulation/polar.png)


### Ferrel Cell $`30\degree - 60\degree`$

- On latitude $`60\degree`$, cold air comes from the pole and hot air comes from the equator and they **collide**.
- The collision make the air go up creating a **dynamic depression** causing precipitations.
- Some of the air goes back to the pole and some of it goes back to the latitude $`30\degree`$.
- There a more precipitations in the winter because the difference of temperatures between the air blocks colliding is greater than in the summer.
- Unlike in the equator which has a thermal depression, a dynamic depression can happen anytime during the day.

 ![ferrel](./img/11_Global_Circulation/ferrel.png)

#### Westerlies winds

- On the ground, winds between $`30\degree`$ and $`60\degree`$ are south-west winds.
- In altitude the winds are north-east 


### Summary of the winds

- $`0\degree-30\degree`$: trade winds - רוחות הסחר/פסט
- $`30\degree-60\degree`$: westerlies - מערביות
- $`60\degree-90\degree`$: צפון מזרחיות

![winds](./img/11_Global_Circulation/winds.png)


### Effect of the different cells on the atmosphere

- Above the equator the air is hot and so the tropopause is higher whereas in the poles the air is cold and converges resulting in the tropopause being lower.

![tropopause](./img/11_Global_Circulation/tropopause.png)

### What influence the pressure systems?

1. The systems moves along with the **seasons** and the Sun's radation: 
  - In July the zenith is at $`23.5\degree N`$ - cancer tropic - and all the systems (ITCZ, thermal and dynamic depression/anticyclone) move north around $`+10\degree`$/$`+15\degree`$. 
  
   > _For example_:  
   > ITCZ, thermal depression, instead of being at $`0\degree`$ it will be at around $`15\degree`$  
   > The dynamic anticyclone instead of being at $`30\degree`$ will be at $`40\degree`$  
   > The dynamic depression won't be at $`60\degree`$ will be at $`70\degree`$  
 
  - In January, the zenith is at $`23.5\degree S`$ - capricorn tropic - and all the systems move south around $`-10\degree`$/$`-15\degree`$

   > _For example_:  
   > ITCZ, thermal depression, instead of being at $`0\degree`$ it will be at around $`-15\degree`$  
   > The dynamic anticyclone instead of being at $`30\degree`$ will be at $`20\degree`$  
   > The dynamic depression won't be at $`60\degree`$ will be at $`50\degree`$  
   
   ![itcz](./img/11_Global_Circulation/itcz.png)

2. The **difference of temperatures between the land and the sea**:
   - In a **thermal system**, in the summer there is more rain above land than sea because the land is hotter.
   - In the winter, the sea is warmer than land and the depressions expand.
   - Influence of the topography of Asia: in summer the land in Asia get warmer fast, especially comparing to the Indian ocean just below it (making the ITCZ higher than expected). This causes a thermal depression: mansoon depression. In winter this land is a lot colder comparing to the Indian ocean creating an anticyclone instead of the expected depression.
  
## Jet streams - זרם הסילון
![jet streams](./img/11_Global_Circulation/jet_streams.png)

![jet streams](./img/11_Global_Circulation/jet_streams2.png)

- There are 2 jet streams: **Subtropical** jet stream and **Polar** jet stream
- Jet streams are western winds.
- The jet streams are **between** the cells and is caused by the difference in air temperatures which causes pressure gradient.
- They are found mainly around the **tropopause (11km)**.
- The polar jet stream is stronger and shorter than the subtropical.
- They move during the year like the rest of the systems.
- The jet streams is faster in its center and the further from the center the less fast is the wind (see pic below). That's what cause the turbulences when flying.

  ![jet stream speed](./img/11_Global_Circulation/jet_streams_speed.png)


