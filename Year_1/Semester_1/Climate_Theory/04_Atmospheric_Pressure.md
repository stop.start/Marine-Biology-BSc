# Atmospheric pressure

![atmospheric pressure](./img/04_Atmospheric_Pressure/atmospheric_pressure.jpg)

## Hebrew/English Lexicon

| Hebrew      | English       | Definition                                          |
|-------------|---------------|-----------------------------------------------------|
| חום מוחשי   | Sensible heat | The heat we feel                                    |
| חום כמוס    | Latent heat   | Heat released by change of state                    |
| קונבקציה    | Convection    | heated air that travels and transfers sensible heat |
| מרכיבי עקבה | Trace gases   | Gases that account for 0.01% of the atmosphere      |
| מפלס            | Level              |                                                     |

## Basics

- **Sensible heat (חום מוחשי)** is the heat we feel around us and part of it is the heat emitted from the Earth.
- **Latent heat (חום כמוס)** is the heat released during change of state of water (?). From liquid to vapor from example releases energy.
- **Convection (קונבקציה)** is heated air that travels away and carries the thermal energy along. This energy carried is sensible heat.
- **Residence time** is the average time that an individual molecule of a given substance remains in the atmosphere.
_Examples:_

> Some area is heated and this area is warmer than its surroundings, sensible heat is released which goes up via convection streams.  
> If in the air that goes up there's some water it will create clouds thus releasing latent heat.  
> The latent heat increases the air temperature and causes it to go up more.  


## Atmosphere composition
- Two main composants (99%):
  - Oxygen (חמצן)
  - Nitrogen (חנקן)
- Trace gases (מרכיבי עקבה) that can either be from natural source or human source.

![atmospheric composition](./img/04_Atmospheric_Pressure/atmosphere_composition.png)

## Atmospheric pressure

**Reminder**  

> Air which is warmer than its surroundings will go up. If there's enough water in it, it will create clouds.
> Clouds can rain only if the water drops are big enough.

- **Atmospheric pressure is the pressure a column of air exerted above a specific area**
- Planets and other astronomical objects need to be big enough to keep an atmosphere around them. Smaller objects like the moon don't have the gravitational pull to keep an atmosphere around them.
- Planets with atmosphere don't have the same mix of gas in it.
- Our atmosphere is around **550km**. Most of its mass is close to the ground.
- The **composition of the atmosphere** doesn't change no matter the height. Only the quantity.
- **Dry air** is the air with its regular composition.
- **Humid air** is the air with its regular composition and with some **water vapors** in addition.
- The **humidity** depends on the area (oceans tends to have more) and the time of the year.
- **Water vapors** are mostly up to 15km in the atmosphere. Above that there are some but a small percentage.
- Above 15km the air is dry.
- 550km of atmosphere weighs the same as 10m of water.
- The higher above ground the smaller the atmospheric pressure (shorter column above = smaller pressure). 
- **Millibars** is the unit to **mesure atmospheric pressure**.
- When measuring the atmospheric pressure the lowest measure is a depression (שקע) and the highest a high (רמה).
- An **isobar** is a line of equal or constant pressure on a graph, plot, or map.
- A **barometric pressure map** shows the atmospheric pressure using isobars.
- The **average pressure at sea level** is 1013 millibars.

![atmospheric composition](./img/04_Atmospheric_Pressure/isobars.png)

- **Daily pressure changes are caused by the fact that the Earth isn't heated uniformly**:
  - When the ground is warmer than its surroundings, the air above it goes up
  - In this area the pressure will be low
- Winds are the result of those changes in pressure.
- **Air will always go from high pressure areas to low pressure areas** in order to balance the pressures.
- The higher the pressure gradient, the stronger the winds (we can also see the direction of the winds).
- In the depression the air goes up, in a high the air goes down.
- A high (or anticyclone) isn't bound to the temperature.
- **Pressure gradient** is the change of pressure per unit distance. 

## Questions
- tough, depression
- Why the earth ground release the same amount of radiation everywhere
- why hot air goes up = low pressure (water?)

