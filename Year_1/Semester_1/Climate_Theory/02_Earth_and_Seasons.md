# Earth and Seasons

![earth](./img/02_Earth_and_Seasons/earth.jpg)  ![seasons](./img/02_Earth_and_Seasons/earth_seasons.jpg)

## Hebrew/English lexicon


| Hebrew        | English             | Definition                                                  |
|---------------|---------------------|-------------------------------------------------------------|
| מישור המילקה  | Ecliptic            | Imaginery plane around the Sun on which the planets revolve |
| דמדומים       | Twilight            |                                                             |
| שחר           | Dawn                |                                                             |
| חוג הסרטן     | Tropic of Cancer    | $`23.5\degree`$ north                                       |
| חוג הגדי      | Tropic of Capricorn | $`23.5\degree`$ south                                       |
| נקודת השוויון | Equinox             | When the sun is closest to the equator                      |
| נקודת היפוך   | Solstice            | when the sun is farthest from the equator                   |
| קטבים              | Poles                    |                                                             |

**Different areas of the Earth receive different quantity of radiations**

## The Earth

- The Earth is a **terrestrial planet (כוכב לכת ארצי)**.
- The Earth is a **geoid**.

> One theory for how the Moon was created, called the giant-impact hypothesis or Big Splash, is that when the Earth was still all lava there was a collision between the Earth and a big asteroid which caused some parts of the Earth to detach and from those debris the Moon was formed.

- Some planets have more than one moon.
- The Earth rotates around itself and revolves around the Sun.
- It takes almost 24h for the Earth to complete a rotation at a speed of 500m/s.
- It takes $`365\frac{1}{4}`$ for the Earth to complete a revolution around the Sun. This is why we add Feb 29 every 4 years.
- **The Earth is tilted ($`23.5\degree`$) compared to the plane on which it revolves**.
- The plane on which the Earth revolves around the Sun is called **ecliptic (מישור המלקה)**.
- The Earth (and other planets) are pulled to the Sun and the **solar wind (רוח השמש)** pushes back.
- At all time, half the Earth is illuminated from the Sun. In hebrew the area where there is light but no sun is called **קו ההארה**. It happens during **twilight (דמדומים)** and **dawn (שחר)**.
- Earth's orbit around the Sun is elliptic.
- The point where the Earth is closest to the Sun is called **perihelion (פריהליון)** and is in January.
- The point where the Earth is the furthest from the Sun is called **aphelion (אפהליון)** and is in July.
- Because of the tilt of the Earth, summer and winter are inversed between the northern hemisphere and the southern hemisphere.
- The fact that the Earth's orbit is elliptic and that the Earth is not always at the distance from the Sun does not influence the temperatures because those differences in distance are negligible.
- The reason the summer is more extreme in the northern hemisphere than in the southern hemisphere is because of their composition: there are more continents in the northern hemisphere than in the southern.

_More explanation:_
> Land gets warmer and colder faster than water.  
> Deserts are warmer and colder than other types of lands because of the lack of water.  
> The northern hemisphere is mostly composed of land and the southern hemisphere is mostly composed of water. Thus the northern hemisphere has hotter summers and the southern hemisphere has more moderate climate (as a whole).  
> 
> Why the antartic is colder than the artic then?  
> The artic is a small portion of land surrounded by water while the antartic land is wide and elevated.  
> _Side note: Australia is not that hot! (except in the desert)_

- **Zenith: when the sunlight reaches the Earth at $`90\degree`$**.
- Zenith exists between the tropics (tropic of cancer - חוג הסרטן and tropic of capricorn - חוג הגדי).
- Each day the zenith is in another place.
- The zenith happens twice a year for every place except for the tropics themselves since these are the turning points.
- Sunlight at zenith has stronger radiations since it passes through less atmosphere.
- The **equinox (יום השיוויון)** happens twice a year: 22.9 and 22.3
- The **solstice (יום המפנה)** happens twice a year: 22.12 where the zenith is on the tropic of capricorn and 21.6 where the zenith is on the tropic of cancer.
- **3 factors of radiation quantity from the sun**:
  - Time with direct sunlight
  - Angle at which the sunlight hits the ground. At $`90\degree`$ it covers less ground thus stronger. At a wider angle it covers more ground thus weaker
  - The path the sunlight takes in the atmosphere. The more the sunlight will be in the atmosphere the weaker it will be when it hits the ground
  
  ![earth sunlight](./img/02_Earth_and_Seasons/sunlight_earth.png)
