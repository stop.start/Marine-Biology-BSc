# Deserts

## Climate areas

- 6 factors that design the climates all around the globe:
  - **Latitude**: 
    - Latitudes get different amount of Sun's radiation and precipitations.
  - **Distance from the sea**: 
    - The sea moderates the climate: the further from the sea the more extreme the climate is.
    - The sea increases the humidy thus lands close to the sea will have more precipitations.
  - **Oceans streams**:
    - _Hot ocean streams_ increase the temperature around the beach area as well as precipitations.
    - _Cold ocean streams_ decrease the temperature around the beach area as well as precipitations.
  - **Topography**:
    - The _higher_ the colder and the more precipitations because the air is blocked by the moutain making it go up.
  - **Winds**: takes along the characteristics of the desert/sea it blows above.
  - **Stationary anticyclones and depressions**: thermal and dynamic anticyclones and depressions.

- The **Köppen climate classification** divides climates into **five main climate groups**, with each group being divided based on seasonal precipitation and temperature patterns. The five main groups are A (tropical), B (dry), C (temperate), D (continental), and E (polar).

 > _Note:_  
 > In Australia there's a mediterranean climate.  
 
 
## Desert
 
- **Desert is a zone where there is less than 200mm precipitations a year**.
- Desert can be cold or hot.
- A hot desert has a high temperature because:
  - No humidity to create clouds.
  - No clouds no protections against Sun's radiations.
- The **evaporation rate** is higher than the **condensation rate**.
- There's a constant lack of water. 
- It is called an **arid area - איזור צחיח**.
- The **tundra** around the poles and **steppe - ערבה** around the deserts (hot or cold) are zones in between climates.
- In the **steppe - ערבה** (hot or cold) the **precipitations** are between **200mm and 400mm**.

### Factors for the formation of deserts

- **Subtropical anticyclone** prevents the air from rising on lalitude $`30\degree`$.
  - The fluctuation of the subtropical anticyclone sets the deserts boundaries (גבולות).
- **Rain shadows - צל גשם**: dry area on the leeward side of a mountainous area (away from the wind). 
  - The mountains block the passage of the air thus the air rises. 
  - If there's humidity it will rain (on the blocking side of the mountains). 
  - Then air gets colder and starts the descend on the other side of the ridge (רכס) it is already dry.
  ![rain shadows](./img/12_Deserts/rain_shadows.png)
- **Cold sea/ocean streams** 
  - Gets to the beach zone and decrease the temperature of the sea.
  - The cold temperature of the sea makes the air above it colder than its surroundings creating an anticyclone making impossible for clouds to form.
  
### Climate characteristics of arid areas

- Almost no precipitations.
- Almost no clouds (anticyclone).
- Strong radiations from the Sun.
- Very high temperatures in the summer.
- Big difference between day/night temperatures and summer/winter temperatures.
- Strong winds due to the air descending from the anticyclone.
- Sands storms due to the strong winds.

### Deserts in the world

![deserts](./img/12_Deserts/deserts.png)

### Climate in Israel

- There are 3 types of climates in Israel:
  - **Arid climate - אקלים מדברי** (desert) due to the subtropical anticyclone.
  - **Semi-arid climate - אקלים ערבתי** surrounding the desert.
  - Mediterranean climate.
  
![israel](./img/12_Deserts/israel.png)
