# Fog

- Fog is a cloud that touches the ground.

## Types of fog

### Regular
- It forms when the air just above the ground is over saturated (even a little) which allows condensation thus creating a layer of cloud on the surface.
- This happens when the **air touches a colder surface** (the ground), making it colder than the dew point.

### Steam fog - ערפל קיטור

- **Steam fog - ערפל קיטור** which happens above hot surfaces (like lakes, rivers, swamps) during the night before dawn.
- Steam fog happens because the cold air gets more water vapors from the water on the surface allowing for the condensation of the water vapor.

### Radiation fog - ערפל קרינה

- Forms during the night.
- Can be a few meters high (up to 10m).
- It is a dense and immobile fog.
- It forms because of the cooling down of the ground from the Sun's radiations.
- The cooling down of the ground also cools down the air above it up to its dew point.
- **Optimal conditions** for radiation fog to form:
  - **Clear night**: allows the ground to cool down fast.
  - **Stability**: inversion prevents air turbulence.
  - **Humid air**
  - **Weak to no wind**: so the fog won't disperse (under 10knots).
- If the air above the fog is dry the cooling down happens faster.
- The fog forms from **heterogeneous nucleation** (= with aerosols).
- It tends to falls into lower places (heavy fog).
- Strong winds or Sun's warmth will disperse the fog.

## TODO
Last paragraph

### Advection fog - ערפל הסעה

- From hot air that is closed to be saturated.
- The air gets saturated while traveling above cold surfaces.
- This fog can be a few dozen of meters high.
- **Conditions** to form:
  - **Weak wind** (5 to 10 knots) which brings the air above cold grounds.
  - **Atmospheric stability**.
- It usually happend during the winter because the land is usually colder than the sea: the air travels from the sea to the land fast and gets saturated above the beach.
- **Clear sky** can help cooling down the ground and create the fog when the ground isn't cold enough to form the fog: this an advection/radiation but denser than a regular radiation fog.
- The advection fog can also be formed above the sea when the land is warmer than the sea.

### Hill fog - ערפל ההרים

- **Orographic cloud** (clouds that develop in response to the forced lifting of air by the earth's topography).
- The fog covers the lower ground and part of the slope up to a certain height: the height of over saturation (can be above the hill, in this case the fog will cover the top of the hill as well).
- This fog can last long. 
- During harsh weather, when the base of cumulus clouds reach the ground it can create fog (happend in the Hermon).

### Travel fog - ערפל מוסע

- Fog that comes from somewhere else with weak winds (around 10 knots).

### Smog - ערפיח

- Fog in urban or industrial zones which mixes with pollution.
- It is dangerous for the health.
- Los angeles and London suffered from big smog.

### Acid fog - ערפל חומצי

- Gases and airosols from power stations and vehicles can transform into acid fogs.
- Not healthy.


