# Sandstorms and Dust Storms

## Differences

- Sandstorms and dust storms come from the same deserts thus have the same mineralogy.
- Because sandstorms have bigger grains, it lasts less and on shorter distances.

| Sandstorm                   | Dust storm                                                       |
|-----------------------------|------------------------------------------------------------------|
| Grains bigger than 2$`\mu`$ | Grains smaller than 2$`\mu`$                                     |
| Fall speed 50cm/s           | Fall speed 1cm/s                                                 |
| Small distance              | Big distances, stays in the atmosphere until the dust falls back |
| Shorther                    | Last longer                                                      |

## Conditions of formation

- Need a big area of sand like deserts.
- The dryer the atmosphere the higher the chance of sand/dust storm formation. Humidity makes the grains heavier thus less likely to be dragged in the air.
- In order to start it needs winds of at least 20knots.
- It needs unstability in the first 2km of atmosphere (from the ground and up).

## Formation

Two steps:  

1. Plucking the sand from the ground.
   - Tearing the grains of sand from the mounds (תלוליות).
   - The winds create a **low pressure area above the sand** and the grains "jump" making other grain jump.
2. Elevation of the sand in altitude.
   - **Mechanical turbulence - ערבול מכני**: chaotic air movement due to the fact that the air on the ground moves slower (due to friction with the sand) than the air above it.
   - Upward **convection streams** (during the day).
   - External constraint like a depression.
   
> _Note:_  
> זרמי קונבציה:  
> Vertical air streams created from a difference in temperature  
> In hebrew: זרמים אנכיים שהם תוצר של הפרש טמפרטורה  

## Dust storms

- Dust storms are a **source of aerosols**.
- They can **increase or decrease the Earth temperature** depending of their size and chemical structure.
- Affect the cloudiness and precipitations.
- They can affect our health.

  ![dust storm](./img/14_Sandstorms/dust_storms.png)
  
## In Israel

### Heat wave depression - שקע השרבי

- Phenomenon in mediteranean sea.
- Mainly in spring.
- Starts in the Atlas moutains in Morocco as a topographic depression.
- Deepens because of the difference of temperature between the sea and land: end of winter thus the sea is the coldest it can be and the land already started to get warmer.
- Finish with muddy rain.

## Extensive sandstorms

- Started from a synoptic system with **strong winds in a wind sand area**.
- If there's no rain there will be extensive sandstorms with clouds of dust in wide distance.

## Haze - אובך

- Limited vision due to dust in the air.
- Can last a few days and travel to far areas.
- Particles of dust falls back at a slow speed.

## Dust devil - עלעול

- Convection stream that is visible because it elevates dust with it.
- Air that rises with a rotation movement.
- **Needs winds of 30knots**.
- Diameter of a few meters and height up to 100m.
- Forms in an **unstable and dry atmosphere due to the ground being heated not equally**.
- **Happens in the afternoons when it's the hotest** and lasts up to a few minutes.
