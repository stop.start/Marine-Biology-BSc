# Solar System

![solar sytem](./img/01_Solar_System/solar_system.jpg) 

## Hebrew/English lexicon

| Hebrew          | English            | Definition                                                                                                                     |
|-----------------|--------------------|--------------------------------------------------------------------------------------------------------------------------------|
| שקע             | Depression (Low pressure area) | Region where the atmospheric pressure is lower than that of surrounding locations                                              |
| רמה             | High or Anticyclone (High pressure area) | Region where the atmospheric pressure is higher than that of surrounding locations                                             |
| משקעים          | Precipitations     | Product of the condensation of atmospheric water vapor that falls under gravity (drizzle, rain, sleet, snow, graupel and hail) |
| כוכב לכת        | Planet             |                                                                                                                                |
| כוכב לכת ארצי   | Terrestrial planet | Planet that is composed primarily of silicate rocks or metals                                                                  |
| ענק גז          | Giant gaz          | Giant planet composed mainly of hydrogen and helium                                                                            |
| שביט            | Comet              | Icy asteroid that, when passing close to the Sun, warms and begins to release gases                                            |
| ננס צהוב        | Yellow dwarf       | Type of star (like the Sun)                                                                                                    |
| היתוך גרעיני    | Nuclear fusion     | Process where 4 Hydrogen atoms creates one Helium atom and releases energy                                                     |
| מימן            | Hydrogen           |                                                                                                                                |
| ערפילית פלנטרית | Planetary nebula   | Gazeous shell around the core of the Sun at the end of its life                                                                |
| חלקיקים         | Particles          |                                                                                                                                |
| חממה            | Greenhouse         |                                                                                                                                |


## Notes from "Actuality time"

- Hurricanes are classified by the **Saffir-Simpson scale** up to Category 5.
- The scale is based on the **winds strength** and the **depression** (שקע)


## Intro

- The **climate system** is caused by the **balance of the difference of energy between different areas**.
- Some areas gets more radiation than others.
- Energy moves from areas with more radiation to areas with less radiation.
- The energy is moved with **winds** and **sea currents**.

## Basics

- **Weather** is what we see and feel when going outside: hot, cold, raining, cloudy, etc.
- **Climate** is the **statistical summary** of all weather events (temperature, rain volume, etc) for a specific area and time frame. Based on those statistics we can define the climate for specific areas.
- **Meteorology** is the **atmospheric** science including the interactions between the atmospheric chemistry and physics and what's going on on the ground.

_Interesting facts:_
> **Radiosonde** (רדיוסונדה) is a tool hanged on a helium balloon and is sent to the atmosphere on specific times and measures each atmospheric level the pressure, temperature, humidity, wind strength and direction etc. The results are transmitted to a receiver on the ground. In the end the balloon explode and the radiosonde falls somewhere on the ground (not used again).

- **Synoptic charts** are maps that summarise atmospheric conditions (temperature, precipitation, wind speed and direction, atmospheric pressure and cloud coverage) over a wide area at a given time. 
- **Synoptic systems** are very large. Smaller scale systems are called **meso meteorology**.
- **High pressure area or Anticyclone (רמה)** is a region where the atmospheric pressure is higher than that of surrounding locations.
- **Low pressure area or Depression (שקע)** is a region where the atmospheric pressure is lower than that of surrounding locations.
- In high pressure areas, the air cannot go upward thus clouds cannot be formed. 
- In low pressure areas, the air can go upward thus clouds and precipitations can be formed. 
- The pressure doesn't influence the temperature.

_Interesting facts:_
> **Syberian high (הרמה הסיבירית)** brings cold dry air.  
> **Mansoon trough (השקע המנסוני)** or mansoon low 

## Solar System

### Basic facts

- Created 4.6 milliard years ago.
- The universe exists for 13 milliard years.
- The solar system was created from a cloud of dust and matter. This is called the **nebular hypothesis**. Most of the mass at the center formed the **Sun**.
- The Sun contains 99% of the system's mass. 
- Shortly after the Sun was created, the **planets were formed from the rest of the dust and matter**.
- Our solar system is composed of planets, moons, asteroids, comets
- The solar system contains **9 planets** (from the Sun):
   - Mercury - חמה 
   - Venus - נוגה
   - Earth -  כדור הארץ
   - Mars - מאדים
   - Jupiter - צדק
   - Saturn - שבתאי
   - Uranus - אורנוס
   - Neptune - נפטון
   - Pluto - פלוטו
   
![planets](./img/01_Solar_System/planets.png)
- There are 4 **terrestrial planets**: Mercury, Venus, Earth and Mars.
- There are 4 **Giant gaz** planets: Jupiter, Saturn, Uranus and Neptune.
- **Pluto** came later and was not created when the solar system and all the planet were.
- There are more than a hundred of moons in our solar system. Some of the planets have no moons, some have several (64!).
- Asteroids revolve around the Sun whereas moons revolve around planets.
- There is an **asteroid belt** between Mars and Jupiter.
![asteroid belt](./img/01_Solar_System/asteroid_belt.jpg)
- The asteroid belt might be from a planet that exploded.
- Some of those asteroids break into our atmosphere.
- An **asteroid** is a rock from any size that is in space.
- A **meteor** is an asteroid that breaks into our atmosphere.
- A **meteorite** is a meteor that actually hits the ground.

### The Sun

- The difference between stars and planets is that stars **create energy**.
- The process by which the stars create energy is called **Nuclear fusion (היתוך גרעיני)**. During this process 4 hydrogen atoms fuse together to create one helium atom and release energy.
- The energy released by nuclear fusion is the reason for light and warmth.
- The energy released forms the **corona (עטרה)** which is an aura of plasma aournd the core.
- The bigger the stars the more energy it releases. 
- Life cycle of the Sun:
  - Cloud of dust and matter
  - Nuclear fusion $`\longrightarrow`$ it is officialy a star and it is in its **main sequence stage** (yellow dwarf is part of this stage)
  - Helium atoms in the core occupy less volume than the hydrogen atoms (the fusion release 2 protons) thus the core shrinks and the outter layers move closer to it.
  - **Red giant** where the Sun will be a lot larger and will consume Mercury and Venus and maybe Earth.
  - No more helium: the outter layers move away from the core as a gazeous shell which is called **Planetary Nebula (ערפילית פלנטרית)** 
  - **White dwarf** the remaining core slowy diming.
  - **Black dwarf** same core that stopped shining.
  - From the **planetary nebula** will come a new solar system.

![sun life cycle](./img/01_Solar_System/sun_life_cycle.jpg)

- The sun releases energy in wave form.
- This energy spreads as **radiation**.
- The Sun **rotation** takes around 25 days.
- The Sun revolves around the **milky way galaxy**. It takes between 225M-250M years for a full revolution.
- The energy released by the Sun is called **Solar wind (רוח השמש)**.
- **Solar wind** is a stream of charged particles like protons, electrons released from the **corona (עטרה)**.
- Those particles travels at 300k km/s (light speed).

_Notes:_
> In the Milkyway galaxy there are 100M stars  
> From one end to the other it would take 100K years  
> At the center of the galaxy there's a black hole  

### Radiation (קרינה)

- Radiation is the emission or transmission of energy in the form of waves or particles through space or through a material medium. 
- It is a stream of particles.
- The Sun's radiation comes through space to us.
- In space the warmth of the Sun is not felt because there is nothing to "trap" it.
- The Sun releases light also called **electromagnetic radiation** with a spectrum of **wavelength**.
- **Electromagnetic spectrum** is all of the different wavelength.
- Each wave from the sunlight has it's own length.
- Waves can be short of large.
- The shorter the wave more energetic it is (same amount of energy is less time)
- **The interactions between our atmosphere and the radiation is different based on the wavelength of the radiation. This influence the climate**.
- Some of those radiation wavelength are visible to the human eye: **visible light**.

_Interesting fact:_
> Near-infrared radiation represents almost half the Sun's raditions.  
> This type of radiation is mainly absorbed by water vapor and $`C0_2`$. That's the reason it is so hot in the desert.  

- **Wein's law** states that **all body (incl. objects) radiates in inversed relation to its temperature** $`\longrightarrow`$ **the higher the temperature the shorter the radiation wavelength**.

```math
\lambda_{max} = \frac{2897\mu m}{T(K)} \quad \text{where T(K) is the body's temperature in Kelvin}
```

- From the above, the Sun, despite radiating in all wavelengths, radiates mainly shorter wavelengths.
- The **Sun's maximum wavelenght is 0.48 $`\bold{\mu m}`$** and the **Earth's is 10.05 $`\bold{\mu m}`$**.
- The radiation entering our atmosphere goes through several processes:
  - Part of it is **sent back to space = refection**: by gazes in the atmosphere, clouds, ice on the ground, everything white
  - Part of it **scattered**: by gazes in the atmosphere
  - Part of it is **absorbed** in the atmosphere (gazes) as well as on the ground
- Types of radiaton:
  - **Direct radiaton (קרינה ישירה)**: direct from the Sun.
  - **Diffused radiation (קרינה מפוזרת)**: radiation that was dispersed by the atmosphere.
  - **Reflected radiation (קרינה חוזרת)**: radiation that was sent back to space as is and was not used to warm the planet: _example: radiation hit ice ground will return to space_.
- **Albedo**: Percentage of the radiation that was sent back to space without influencing the warmth on Earth.
- On Earth there's **planetary albedo** which is 30% (30% of radiation goes back to space), or in other words 70% of the radiations take part in warming the Earth.
- Of the 30% of the radiations that are **reflected** to space:
  - 4% are reflected from the ground (90% of that is from the ice and 10% from the water).
  - 20% are reflected from the clouds.
  - 6% are reflected from the atmosphere.
- Of the 70% absorbed:
  - 51% absorbed on the ground.
  - 19% absorbed by the atmosphere.
- Changes on the Earth's surface can change the **planetary albedo**.

_Notes:_
> Ice reflects 90% of the radiations, water 10%.  
> Surfaces once covered in ice that are now water cause more warming.  
> The warming causes more warming (משוב חיובי).  

- The **greenhouse effect (אפקט החממה)** is a natural effect caused by gases in the atmosphere (including water in its gas form). It interacts well with long wavelength radiations ($`10 \mu m`$) which are the radiations that the Earth releases and **only 62% goes to space**.
- Without the greenhouse effect the Earth's temperature would be $`18\degree`$. Because of the greenhouse effect it is $`30\degree`$
- The greenhouse effect gases:
  - Methane $`CH4`$
  - Carbon dioxide $`CO_2`$
  - Ozone $`O_3`$
  - Water vapor $`H_2O`$
  - Nitrous Oxide $`N_2O`$
- Humans adds to the greenhouse effect by adding for example $`CO_2`$.



-------------------------
### Questions
- Is energy really directed from areas with less radiation to areas with more radiation?
- Why the siberian high influence the temperature
