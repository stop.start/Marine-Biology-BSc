# Atmospheric Stability


## Hebrew/English Lexicon

| Hebrew                | English                     | Definition              |
|-----------------------|-----------------------------|-------------------------|
| להתעבות / עיבוי       | Condense / Condensation     |                         |
| להתאדות/ אידוי        | Evaporate / Evaporation     |                         |
| המסה                  | Melting (liquification)     |                         |
| המראה                 | Sublimation                 | From solid state to gas |
| ריבוץ / דפוזיציה      | Deposition                  | From gas to solid       |
| נגר עילי/ זרימת עילית | Upper flow / Surface runoff |                         |
| דיוט                  | Transpiration               |                         |
| רוויה                 | Saturation                  |                         |
| טל                    |   Dew                       |                         |

## Intro

**Clouds** are created when air goes up with enough **water vapors** that at some point can **condense** into drops.

## Humidity

- **Water vapors** can be found everywhere: above sea and land, all year long, day and night.
- We can't see/feel the water vapors since it's a gas.
- Water can be found in oceans and land (rivers, vegetation).
- **Water** goes from **land to oceans** via ***upper flows (נגר עילי)**.
- **Water** goes from the **oceans to the atmosphere** via **evaporation**.
- **Water** goes from the **atmosphere to the oceans** via **condensation**.
- **Water vapors** move from the **oceans atmosphere to the land atmosphere** via **advection streams (horizontal streams)**.
- In the **land atmosphere** there's some **evaporation** and **transpiration** and **condensation** back to the land.
- **78% of the precipitations above land are from the oceans**.

![water cycle](./img/06_Atmospheric_Stability/water_cycle.jpg)

## Different states of water

- $`H_2O`$ is the only molecule found in its 3 states - solid, liquid, gas - in nature and atmosphere. Its state depends on a lot of factors.
- In its **gas state** water is invisible and we cannot feel it.

_Fact:_

> When, for example, cooking the "vapor" released by the process is not actually vapor but **water droplets (טיפונות מים)**.  
> If we can "see the vapor" then the water is actually liquid.  

- **When changing state, water either use or release energy $`\rightarrow`$ latent heat (חום כמוס)**.
- **Breaking water molecules bonds (evaporation or liquification) requires energy**.
- The higher the temperature the faster the evaporation.
- **Condensation and solidification of water molecules releases the same amount of energy (latent heat) needed for the evaporation**.
- The process in which water goes from **solid to gas** without being liquid is called **sublimation (המראה)**. Requires energy.
- The process in which water goes from **gas to solid** without being liquid is called **deposition (דפוזיציה)**. Releases energy.

![water state](./img/06_Atmospheric_Stability/water_states.png)

## The effect of water vapors in the atmosphere

- The presence or absence of water vapors in the atmosphere will determine the possibility of precipitations. Despite the best conditions if there's no water in the atmoshphere there won't be any precipitations.
- Water vapors dictate the stability of air blocks by using/releasing latent heat through change of states.
- Water vapors are greenhouse gases $`\rightarrow`$ energy that is released from the earth that is trapped in the atmoshphere.
- Water vapors can become clouds which increase the **planetary albedo**.
- Humidity is part of the **heat load (עומס חום)** measure. **Heat load = temperature and relative humidity**.
- **The atmoshphere can contain a certain amount of water**. If there's 100% humidity it means the atmoshphere can not contain more water thus liquid water and droplets won't be able to evaporate. 
- Water vapors (humidity) will affect humans $`\rightarrow`$ the more water is in the atmoshphere the less humans can evaporate sweat (thus the sweat will stay on us).
- **3 factors of water evaporation**:
  - **Temperature**: it gives the water molecules the energy to break their bonds.
  - **Humidity level**: the air can contain up to a certain amound of water vapors, thus water drops will form.
  - **Wind speed**: wind will drag away air with its humidity.

## Vapor pressure

- **Vapor pressure** is
- **Atmospheric pressure** is the sum of all the partial pressures for every type of particules in the atmosphere.
- The amount of water vapors in the atmoshphere is not directly related to the possibility of the precipitations.
- The **saturation (רוייה)** is the point where the atmoshphere cannot contain any more water vapor thus water droplets will form.
- The **saturation (רוויה)** is defined by the **relation between the rate of evaporation and the rate of condensation**.
- The saturation is related to the temperature because **the hotter the air the more water vapors it can contain** and **the colder the air the faster it will get saturated**.
- If the **evaporation == condensation** $`\longrightarrow`$ **saturated state**.
- If the **evaporation > condensation** $`\longrightarrow`$ **unsaturation (תת רוויה)**.
- If the **evaporation < condensation** $`\longrightarrow`$ **supersaturation (על רוויה)**.

## Relative humidity

- The higher the temperature, the bigger the air block and can hold more water vapors. 
- **Relative humidity is the relation between the amount of water vapors in the atmoshphere and the maximum amount that the atmoshphere can hold (for the current temperature)**:
```math
RH = 100 \cdot \frac{P_{vap}}{P_{sat}(T)}
```
- Clausius Clapeyron relation:

![water vapor pressure](./img/06_Atmospheric_Stability/water_pressure.png)

- **Dew point temperature (טמפרטורה נקודת הטל)** is the temperature to which air must be cooled to become saturated with water vapor. 
- The Dew point temperature (טמפרטורה נקודת הטל) **decreases $`2\degree C`$ per kilometer**.
- If the air block is able to go upward enough to get to **its dew point** (by shrinking), a cloud will start to form (if there's enough water vapors). 
- The point where the cloud starts to form is called **Lifting Condensation Level (LCL) (גובה רום עיבוי)**.

## Stability states

### Stable atmoshphere
- A **stable atmosphere** is an atmoshphere that does not enable air to elevate. This happens in an **inversion layer (מפל אינברסיה)**.
- Sometimes in the **troposphere** there's an **inversion layer**. This caused by conditions that are not relevant to this course.
- Air that cannot go more upward will spread horizontally.

### Unstable atmoshphere
- An **unstable atmoshphere** is an atmoshphere that enables hot air to go upward. It has a positive lapse rate (מפל חיובי).

## Adiabatic lapse rate

- **Adiabatic process** is the process of **hot air expanding causing the pressure to drop and resulting in a decrease of temperature**.
- In the **adiabatic process** there is **no exchange of heat or mass** with the system's surroundings.
- The **cooling rate** of the air is constant: for every kilometer the air travels upward, the temperature drops $`9.8\degree C`$.
- If the air block is able to go upward enough to get to **its dew point** (by shrinking), a cloud will start to form (if there's enough water vapors). 
- When a cloud creates, the change of state of the water release **latent heat** which increases the temperature of the air block (but not outside of this air block!!) thus enabling it to go upward.
- Before the condensation, the air rises at the **dry adiabatic lapse rate**. After condensation (cloud started to form), the air rises at the **moist adiabatic lapse rate**.
- In a **moist adiabatic lapse rate**, the temperature "only" drops $`6\degree C`$ per kilometer.

![adiabatic process](./img/06_Atmospheric_Stability/adiabatic_process.png)


## Creation of cloud when air is colder than its surroundings

- Sometimes cold air is pushed upward (see clouds lesson) and can create clouds.
- When the cold air is pushed upward enough to be hotter than its surroundings it is called **free (or natural) convection (קונבקציה חופשית)** and it goes up until it reaches the **inversion layer**.

## TODO
- Check what vapor pressure is in the book (audio: 33.25)
