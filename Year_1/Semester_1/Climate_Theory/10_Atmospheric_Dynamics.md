# Atmospheric Dynamics

**What forces work on the wind**  

## Highs and Depression

- Atmospheric pressure is checked at 2km above ground.
- Atomspheric pressure that is higher than the pressure of its surroundings is a **high (רמה)**. Symbol: **H**.
- Atmospheric pressure that is lower than the pressure of its surroundings is a **depression (שקע)**. Symbol: **L**.
- The air will always move from a high pressure environment to a low pressure region.
- The force with which the air **starts to move** from a high pressure environment to a low pressure region is called **pressure gradient (גרדיאנט הלחץ)**.
- Differences in pressure is mainly a result difference in temperature.
- **Wind** is air streaming because of the differences in pressure. It aims to equalize the atmospheric pressure.
- The wind has a **direction and speed**.
- Speed is measured either in knots, m/s or km/h.
- **The wind is defined by the direction it comes from**. _For example, if the wind comes from the north and moves to the south, it is called northen wind._

> In Israel:
> - Western wind comes from the sea thus it is filled with humidity.
> - Eastern wind comes from the land thus it is dry with aerosols from the desert.

- Wind is measured on **Beaufort scale**.

![beaufort scale](./img/10_Atmospheric_Dynamics/beaufort.png)

## Forces that works on the wind

There are **3 main forces**:
- **Pressure gradient**
- **Coriolis effect**
- **Friction**

### Pressure gradient

- It is the first force that starts everything.
- The **wind starts** to move **from the center of the high** in **all around it** and **in direction of the depressions**.
- On a synoptic map the wind will move perpendicular to the isobars (because it comes from a high in direction to the depression).
- The **bigger the pressure gradient** (the bigger the difference in pressure) the **stronger the wind**.
- The more the **isobars are close** to each other the **stronger the wind**.

![pressure gradient](./img/10_Atmospheric_Dynamics/pressure_gradient.png)

### Coriolis effect

- The Coriolis force is a result the Earth rotation's high speed (30km/s).
- The Coriolis force **changes the course of the wind to the right from its origin** in the northen hemisphere and to the left in the southern hemisphere.
- The Coriolis force is 0 in the equator and the strongest in the poles.

|||
|--|--|
|![coriolis 1](./img/10_Atmospheric_Dynamics/coriolis1.gif)|![coriolis 2](./img/10_Atmospheric_Dynamics/coriolis2.png)|

### Friction

- Force that is active on the wind **up to 1km obove ground**.
- This force is due to everything in the ground: trees, buildings, moutains etc.
- **The friction force weakens the wind thus weakens the Coriolis force**.

![friction](./img/10_Atmospheric_Dynamics/friction.png)

## Geostrophic wind

- **Geostrophic winds** are wind that are **>1km above sea level** where there is no friction but still there's still pressure gradient and Coriolis.
- From a **high** the movement of the air stream is **rotating clockwise** in northen hemisphere.
- To the **depression**, the air stream is **rotating counterclockwise** - northen hemisphere.
- There's balance between pressure gradient force and the Coriolis effect.
- The **geostrophic wind moves in parallel of the isobars** as result of the balance between the pressure gradient force and the Coriolis force.

![geostrophic wind](./img/10_Atmospheric_Dynamics/geostrophic_wind.png)

![geostrophic wind2](./img/10_Atmospheric_Dynamics/geostrophic_wind2.png)

## On the ground

- On the ground, there's friction in addition to the pressure gradient force and the Coriolis force.
- **The friction force weakens the Coriolis force thus the pressure gradient force is stronger**.
- Because the Coriolis is weakened making the pressure gradient stronger, the direction of the wind **from a high goes outward** but still to the right. This is called **divergence (התבדרות)**.
- From the same effect the direction of the wind towards the low pressure area. This is called **convergence (התכנסות)**. 

![winds on the ground](./img/10_Atmospheric_Dynamics/forces_with_friction.png)

- As a result of the **convergence** - התכנסות - the **low pressure area fills up**.
- When the low pressure area fills up, the **air goes upward** (as it does in low pressure areas) thus making it possible for clouds to form.
- During a divergence, in the high pressure area the **air goes downward** to fill the void caused by loosing air from the ground. This called **subsidence (התמוככות)**.

![winds on the ground](./img/10_Atmospheric_Dynamics/high_low_winds.png)


## Associated Systems

- **Trough - אפיק**: same characteristics as a depression - air goes up, clouds and precipitations.
- **Ridge - רכס ברומטרי**: same characteristics as a high - air can't go up, no clouds and no rain.
- **Col - אוכף**: area between 2 depressions - not important.

![trough and ridge](./img/10_Atmospheric_Dynamics/trough_ridge.png)

## Synoptic systems vs Mesometeoroligic systems

- **Synoptic scale systems** are big (thousands of km) and slow (seasons long).
- **Mesometeoroligic scale systems** are local (up to a few hundreds of km)

> Example:  
> In Israel, the wind is a western wind --> synoptic scale.  
> However, in Eilat it is a northen wind because of the path the wind takes caused by the moutains --> mesometeorologic scale.  

## Local scale systems

### Influence of the topography on the wind

- Buildings, trees and so on influence the direction and strength of the wind.
- When things block the wind then "more" wind goes through the same path resulting in higher pressure (stronger wind). This is called **אפקט הוואדי**.
- The area that is smaller which causes higher pressure: high.


![topography and winds](./img/10_Atmospheric_Dynamics/topography_and_wind.png)

### Moutain wind

- Moutain winds are the result of the difference in temperature between the air just above the slope of the hill and the air in the valley on the same level.
- **Anabatic** wind happens during the day.
- **Katabatic** wind happens during the night.
- In order for those winds to happen:
  - No clouds in the sky
  - No strong winds 
  
#### Anabatic winds

- During **the day** the ground heats up from the Sun, which causes the **air just above the slope** to be **warmer** than the air in the valley at the same level.
- Since the ground is warmer (sun radiation are absorbed by the ground which reflect them as infrared radiation = heat), the air above it goes up.
- Air goes up == low pressure zone.
- Inside the valley the air in colder thus creating a high.
- Clouds will form above the hills.
- At the peek of the sun (around noon) the wind are the strongest.

||||
|--|--|--|
|![anabatic winds 1](./img/10_Atmospheric_Dynamics/anabatic_wind.png)|![anabatic winds 2](./img/10_Atmospheric_Dynamics/anabatic_winds.png)|![anabatic clouds](./img/10_Atmospheric_Dynamics/anabatic_clouds.png)|

#### Katabatic winds

- During the night, the sun stopped warming the ground which becomes cold faster thus the **air above the slope** is **colder** than the air in the valley.
- The depression is now inside the valley (warmer than the ground).
- The high is above the slope.
- Air moves from the high to the depression close to the ground. 
- This can cause fog.

||||
|--|--|--|
|![katabatic winds 1](./img/10_Atmospheric_Dynamics/katabatic_wind.png)|![katabatic winds 2](./img/10_Atmospheric_Dynamics/katabatic_winds.png)|![katabatic clouds](./img/10_Atmospheric_Dynamics/katabatic_clouds.png)|


### Breeze

- Breeze is a result of the difference of temperature between air above the sea and the air above the beach.
- All water systems with a diameter larger than 1km create breezes.
- The sea warms and cool slower than land because:
  - The land is not transparent like water thus absorbing more light.
  - The sea is in constant movement which disperse the warmth deeper.
  - The specific heat (חום סגולי) which is the amount of heat per unit mass required to raise the temperature by one degree Celsius. It is higher for water thus the temperature will rise less.

- During the day, the land warms more than the sea: 
  - Low pressure zone above the land
  - High pressure above the sea 
  - The air will move from the sea to the land
  - Because the air comes from the sea it is filled with humidity thus there're high chances of clouds building up.
  
![breeze day](./img/10_Atmospheric_Dynamics/breeze_day.png)
- During the night, the land cools faster than the sea:
  - High pressure zone and the sea
  - Low pressure above the land
  - The air will move from the land to the sea.
  - Because the air comes from the land, there's less humidity thus less chances of clouds building up.

![breeze night](./img/10_Atmospheric_Dynamics/breeze_night.png)

- Breezes are weak winds. Stronger winds coming against the breeze will destroy it.
