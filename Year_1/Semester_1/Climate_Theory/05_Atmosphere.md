# Atmosphere

## Hebrew/English Lexicon

| Hebrew                   | English                               | Definition                                      |
|--------------------------|---------------------------------------|-------------------------------------------------|
| חמצן                     | Oxygen                                |                                                 |
| חנקן                     | Nitrogen                              |                                                 |
| מפל אתמוספרי             | Lapse rate                            | Change in temperature according to height       |
| מפל חיובי                | Positive lapse rate                   | The higher the colder                           |
| מפל שלילי                | Negative lapse rate                   | The higher the warmer                           |
| מפל איזוטרמי             | Isothermic lapse rate                 | No change in temperature with increasing height |
| מפל שלילי = מפל אינברסיה | Inversion layer = negative lapse rate |                                                 |
|      מוליך               |  Conduct(ive)                          |   For example electrons are conductive              |

## Atmosphere composition

- Nitrogen (חנקן) - concentration: 78%
- Oxygen (חמצן) - concentration: 21%
- Argon - concentration: 1%
- Trace gases (מרכיבי עקבה) - concentration: 0.01%

![atmosphere composition](./img/05_Atmosphere/atmosphere_composition.png)

## Intro

We can divide the atmoshpere by:
- Gas composition
- Temperature
- Electric charge

## Gas composition

There are 2 layers: **Homosphere** and **Heterosphere**

![gases division](./img/05_Atmosphere/gases.jpg)

### Homosphere
- From the ground to 90km
- The concentration of the gases stays the same in all this layer. This is because of the winds which "mix" the air.

### Heterosphere
- From 90km and higher
- There are molecules layers: **oxygen layer and above it nitrogen layer**.
- A lot less air molecules
- Pretty much no air movement because it is far from the ground and there is no much air to move.

### Exosphere
- Layer where the molecules goes to space

## Temperature

![temperatures division](./img/05_Atmosphere/temperatures.jpg)

- Lapse rate positive מפל חיובי: the higher the colder
- Lapse rate negative מפל שלילי: the higher the warmer
- Some layers have a lapse rate positive and some have a lapse rate negative.
- There are 4 layers: **Troposphere**, **Stratosphere**, **Mesosphere** and **Thermosphere**
- **Pause**: tropopause, stratopause and mesopause are boundaries between the layers where the temperature doesn't change. 

### Troposphere
- **From the ground to around 12km**
- Its temperature decrease the higher
- It is lapse rate positive (מפל חיובי).
- Most of the mass (gases) are in this layer (around 80%).
- Contains most of the **humidity** thus clouds form mainly in the troposphere.
- The end (highest) of the layer has no change in temperature. It is an **isothermic lapse rate - מפל איזוטרמי**. This sublayer is called **tropopause** and is actually part of the stratosphere. It is around 10km but depends on the latitude (highest above the equator).

### Stratosphere
- **From 10/12km to 50km**.
- Starts with a temperature around $`-60\degree`$ 
- **Isothermic** lapse rate up to around 20km (the temperature stays $`-60\degree`$).
- From 20km to 50km it is a **negative lapse rate (מפל שלילי)** or **inversion layer (מפל אינברסיה)** (temperature rises).
- Ends with the **stratopause** which is an isothermic lapse rate (part of the mesosphere).
- Because the temperature rises the air cannot go up.
- There's no air movement in this layer.

_Note:_  

> If some air (really hot) manages to enter the stratosphere, it will stay in the stratosphere (emprisonned) until its weight bring it down to the ground.  
> This process can take a long time (months).

- The **ozone layer** resides in this layer between 20km and 25km.
- **The ozone layer filters UV lights from the Sun**:
  - $`O_2 + UV \longrightarrow O + O + heat`$
  - $`O_2 + O \longrightarrow O_3`$
  - $`O_3 + UV \longrightarrow O_2 + O + heat`$
- This process is what makes the temperature rise in the stratosphere.

![ozone process](./img/05_Atmosphere/ozone.png)

### Mesosphere
- **From 50km to 80/90km**.
- Positive lapse rate - מפל חיובי.
- If water vapors managed to enter the mesosphere, **ice clouds** are formed (noctilucent clouds).
- Ends with the **mesopause**.

### Thermosphere
- **From 85km to 550km**.
- Starts with an **isothermic** lapse rate.
- Has a **negative lapse rate (inversion layer)**.
- Around 500km the temperature is $`1200\degree K`$ on the side turned to the Sun and on the other side $`0\degree K`$ or $`-273 \degree C`$.
- Ends with the **Exosphere**.

## Electrical layers

![ionosphere](./img/05_Atmosphere/ionosphere.jpg)

- One layer: **Ionophere** from  **550km to 60km** (start from the top).
- Radiations from the sun removes electrons from oxygen and nitrogen atoms transforming them into ions (cations).
- Because of the **cations and free electrons**, this layers affects **radio waves** emitted from Earth by returning them to Earth.
- The ionosphere changes according to the Sun radiations. During the day it is thicker and at night thinner.
- At night the electrons and ions don't "reassemble" because there's no much air thus the molecules are farther apart.
- Nitrogen, when ionised shows red lights. Oxygen, when ionised shows green lights. Those can be seen in aurora.

_What is an aurora?_  
> Aurora is an overload of particles from the Sun which get to the Van allen belts and are channeled to the poles. When reaching the poles they hit nitrogen and oxygen atoms and ionise them.  
> The lights we see are the result from the energy released.  
> It happens close to the poles because the magnetic field is closer to Earth.
> Van allen belts are energetic charged particles from solar winds and surround the earth, trapped in the Earth's magnetic field.  

