# Membrane Transport Mechanisms

## Tansporting molecules accross membranes

| Transport Mechanism   | External Energy Required                              | Membrane Protein Required | Specific molecules |
|-----------------------|-------------------------------------------------------|---------------------------|--------------------|
| (Simple) Diffusion    | No - [Concetration gradient]                          | No                        | No                 |
| Facilitated Diffusion | No - [Concetration gradient]                          | Yes                       | Yes                |
| Active Transport      | Yes - [ATP hydrolysis against concentration gradient] | Yes                       | Yes                |

### Passive transport

- Passive transport doesn't require energy in order for the molecules to pass through the cellular membrane.
- Passive transport is basically the process of a substance going down its **concentration gradient**.

#### Simple Diffusion
-  **Diffusion**: Uncharged non-polar small molecules can pass directly through the phospholipid bilayer membrane.
 
#### Facilitated diffusion

- **Channels proteins**: 
  - Proteins shaped like a tube that cross the membrane. 
  - Some are always opened and some can open and close (**gated**). 
  - They have an affinity for only **one type of molecules** (sometimes also for other closely related ones). 
  - Examplea:
    - **Ion channels**: gated channels that open when the protein is stimulated to change its shape. Stimulus can be a molecule (**ligand-gated**) or electrical charge resulting from many ions (**voltage-gated**).​
    - Aquaporins are channel proteins that allow water to cross the membrane very quickly.
- **Carrier proteins**: 
  - They can **change their shape to move a target molecule** (usually polar molecules like glucose) from one side of the membrane to the other. 
  - They (like channels) let through only **one or few types of molecules**.
    
| Channels                        | Carrier proteins                                |
|---------------------------------|-------------------------------------------------|
| ![channels](./img/07_Membrane_Transport/channels.png) | ![carrier proteins](./img/07_Membrane_Transport/carrier_proteins.png) |

_**Note:**_
> Water is partially charged and the phosphate heads are hydrophilic thus they are attracted to each other.  
> That being said, the charge of the water molecules is not that strong and some water molecules can pass through the phospholipid bilayer membrane of the cell.


### Active transport

- Sometimes, the cell needs the molecules to move against their concentration gradient. For example, there is more sugar inside the cell than outside but the cell still needs to import it. Passive transport can't help in this case.
- Active transport requires energy (like ATP) in order to move molecules against their concentration gradient.

#### Primary active transport

- Uses **ATP as an energy source**. 
- For example the **sodium-potassium pump** which moves Na<sup>+</sup> out of the cell and K<sup>+</sup> inside. This specific pump, in addition to regulating the concentrations of Na<sup>+</sup> and K<sup>+</sup>, helps generating the voltage accross the plasma membrane. Those types of pump are called **electrogenic pump**.
- This pump is an **intergrale membrane glycoprotein**.
- It is found in all animal cells.

  _**How does the pump work?**_
  > 1. To begin, the pump is open to the inside of the cell. In this form, the pump really likes to bind (has a high affinity for) sodium ions, and will take up three of them.
  > 2. When the sodium ions bind, they trigger the pump to hydrolyze (break down) ATP. One phosphate group from ATP is attached to the pump, which is then said to be phosphorylated. ADP is released as a by-product.
  > 3. Phosphorylation makes the pump change shape, re-orienting itself so it opens towards the extracellular space. In this conformation, the pump no longer likes to bind to sodium ions (has a low affinity for them), so the three sodium ions are released outside the cell.
  > 4. In its outward-facing form, the pump switches allegiances and now really likes to bind to (has a high affinity for) potassium ions. It will bind two of them, and this triggers removal of the phosphate group attached to the pump in step 2.
  > 5. With the phosphate group gone, the pump will change back to its original form, opening towards the interior of the cell.
  > 6. In its inward-facing shape, the pump loses its interest in (has a low affinity for) potassium ions, so the two potassium ions will be released into the cytoplasm. The pump is now back to where it was in step 1, and the cycle can begin again.
  
![primary active transport](./img/07_Membrane_Transport/primary_active_transport.png)

#### Secondary active transport

- Uses **energy stored by primary active transport** to move other substances against their own gradients:
  - Energy comes from an **ion concentration gradient** that is established by primary active transport.​
  - The movement of particles down their gradient is coupled to the uphill transport of amino acids and sugars by a shared carrier protein (a **cotransporter**). 
  - The two molecules being transported may move either in the same direction (i.e., both into the cell), or in opposite directions (i.e., one into and one out of the cell). When they move in the **same direction**, the protein that transports them is called a **symporter**, while if they move in **opposite directions**, the protein is called an **antiporter**.
  
  ![secondary active transport](./img/07_Membrane_Transport/secondary_active_transport.png)
  
  ![porters](./img/07_Membrane_Transport/porters.png)


_**Passive and active transports work for small particles but how the cell can import/export bigger or larger quantities of molecules?**_
