# Cellular Respiration

## Intro: Fuels

- **Fuels**: molecules whose stored energy can be released for use.​
- The most common fuel in organisms is glucose. Other molecules are first converted into glucose or other intermediate compounds.​

### Burning or metabolism of glucose

```math
\bold{C_6H_{12}O_6 + 6O_2 \longrightarrow 6CO_2 + 6H_2O + \textbf{free energy}}
```

### Glucose metabolism pathway traps the free energy in ATP:​

```math
ADP + P_i + \text{free energy} \longrightarrow ATP
```

## Intro: Cellular respiration 

### Energy of life

![energy of life](./img/08_Cellular_Respiration/energy_of_life.png)

### Steps for cellular respiration

![steps of cellular respiration](./img/08_Cellular_Respiration/steps.png)

- After the glycolysis: 
  - In presence of oxygen: krebs cylce and so on.
  - In abscence of oxygen: fermentation.
- **Glycolysis and fermentation** are done in the **cytosol**.

### NAD is an energy carrier in redox reactions​

#### Redox Reactions

**In chemistry:**

- **Oxidation** is like losing electrons. Usually caused by oxygen (but not always) which is very electronegative.
- **Reduction** is like gaining electrons. Hydrogen likes to give its electrons which makes it mostly a reducing agent.

> **Formation of water as example of a redox reaction:**  
> Water (H<sub>2</sub>O) is formed from oxygen and hydrogen.  
> Oxygen being a lot more electronegative than hydrogen, it will hog so much more their shared electrons that it can be seen as taking those electrons from the hydrogen. Therefore we can say that oxygen has been reduced (it gained electrons) and the hydrogen have been oxidized (they lost their electron).  

**In biology:**

- **Oxidation** is like losing hydrogen. 
- **Reduction** is like gaining hydrogen. 

_Note: It also works with gaining/losing oxygen, where oxidation is gaining O atoms and reduction is losing O atoms._

> **Some explanation:**  
> In biology, we see that hydrogen tends to bond more with certain elements: Carbon (C), Oxygen (O), Nitrogen (N) and Phosphorus (P).  
> Those are a lot more electronegative than hydrogen and when the hydrogen is "passed** from one atom to another it comes with its electron.  

**Notation:**

To denote the oxidation state, the number of electrons followed by + or - in written in superscript next to the atom.  
_For example: H<sub>2</sub>O can be written **H<sub>2</sub><sup>1+</sup>O<sup>2+</sup>**_

#### NAD/NADH

```math
NAD^+ + 2H \longrightarrow NADH + H^+
```

## Steps for cellular respiration (aerobic)


### Steps Overview

| Step                                        | What it does                        | Where it does it               | What it produces                                     |
|---------------------------------------------|-------------------------------------|--------------------------------|------------------------------------------------------|
| **Glycolysis**                              | Breaks Glucose into 2 **Pyruvate**  | **Cytosol** of the cell        | 2 ATP & 2 NADH                                       |
| **Pyruvate oxidation**                      | Breaks Pyruvate into **Acetyl-CoA** | **Matrix of the mitochondria** | 2 NADH                                               |
| **Citric Acid cycle or Krebs cycle**        |                                     | **Matrix of the mitochondria** | 2 ATP + 6 NADH + 2 FADH<sub>2</sub> + CO<sub>2</sub> |
| **Electron Transport Chain + Chemiosmosis** |                                     | **Membrane of the crista**     | ~32-38 ATP                                           |

![steps of cellular respiration](./img/08_Cellular_Respiration/cellular_respiration_steps.png)


### Glycolysis (aerobic)

Formula:  

```math
\bold{
Glucose + 2 NAD^+ + 2 ADP + 2 P_i \longrightarrow 2 \; Pyruvate + 2 \; NADH + 2 \; ATP + 2 H_2O + 2 H^+
}
```

- **Glycolisys is the process of transforming Glucose (C<sub>6</sub>H<sub>12</sub>O<sub>6</sub>) in 2 Pyruvate (C<sub>3</sub>H<sub>4</sub>O<sub>3</sub>) and, in addition, making 2 ATP (net) and reducing 2 NAD<sup>+</sup> to NADH**
- There are 2 part in glycolysis:
  - **Break glucose** into two molecules called PGAL (C<sub>3</sub>H<sub>7</sub>O<sub>6</sub>P). It **requires 2 ATP**.
  - **Make pyruvate** from the PGAL molecules. It **produces 2 ATP + NADH** for each PGAL so **4 ATP + 2 NADH total**.
  
_Note: Glycolysis makes 4 ATP but uses 2 ATP to start so it makes a net of 2 ATP._

#### Steps

1.  **Glucose** to **Glucose-6-phosphate** via **Hexokinase enzyme** + Mg<sup>++</sup> cofactor + ATP
2.  **Glucose-6-phosphate** to **Fructose-6-phosphate** (can be in reverse also) via **Phosphoglucose isomerase**
3.  **Fructose-6-phosphate** to **Fructose-1,6-biphosphate** via **Phosphofrutokinase** + Mg<sup>++</sup> cofactor + ATP
4.  Fructose-1,6-biphosphate to 1 Glyceraldehyde-3-phosphate and 1 Dihydroxyacetone phosphate via Fructose biphosphate aldolase
5.  Dihydroxyacetone phosphate to Glyceraldehyde-3-phosphate via Triosephosphate isomerase (now there are 2 Glyceraldehyde-3-phosphate)
6.  (x2) Glyceraldehyde-3-phosphate to 1,3-Bisphosphoglycerate via Glyceraldehyde-3-phosphate dehydrogenase + Mg<sup>++</sup> cofactor + reduction to NAD<sup>+</sup> to NADH
7.  (x2) 1,3-Bisphosphoglycerate + ATP to 3-Phosphoglycerate via Phosphoglycerate kinase + ADP 
8.  (x2) 3-Phosphoglycerate to 2-Phosphoglycerate via Phosphoglycerate mutase
9.  (x2) 2-Phosphoglycerate to Phosphoenolpyruvate via Phosphopyruvate hydratase (Enolase)
10. (x2) Phosphoenolpyruvate + ATP to Pyruvate via Pyruvate kinase + Mg<sup>++</sup> cofactor + ADP

_Note: ATP is used to attach the phosphate groups to the glucose then fructose-6-phosphate_

_**Summary of "energy" produced: 2 ATP + 1 NADH per pyruvate (=4 ATP + 2 NADH)**_

![glycolysis steps](./img/08_Cellular_Respiration/glycolysis_steps.jpg)

![glycolysis steps](./img/08_Cellular_Respiration/glycolysis1.png)

![glycolysis steps](./img/08_Cellular_Respiration/glycolysis2.png)

#### Change of energy during glycolysis

![glycolysis energy](./img/08_Cellular_Respiration/glycolysis_energy.png)


### Pyruvate oxidation

- Pyruvate oxidation is a step **between glycolysis and krebs cycle** and takes place in the **matrix of the mitochondria**.
- It is not always show as a step but included in the Krebs cycle maybe because it is really short.
- Pyruvate molecules are composed of a 3 carbon chain, after pyruvate oxidation they will be 1 carbon down (so now 2 carbon chain): 
 - This is done by removing the **carboxyl group** which release CO<sub>2</sub>.
 - This new molecule is called **Acetyl CoA**.
 
![coenzymeA](./img/08_Cellular_Respiration/coa.png)
 
- Pyruvate oxidation also reduces NAD<sup>+</sup> to NADH.
- **Acetyl CoA will react (catalyzed by enzymes) with Oxaloacetic Acid (4 carbon chain molecule) and together they form Citric Acid (6 carbon chain molecule).**

_Note: In the krebs cycle, the citric acid will be oxidized and will return to be Oxaloacetic Acid (2 carbon will be removed and those will become CO<sub>2</sub>)_

_**Summary of "energy" produced: 1 NADH per pyruvate (=2 NADH)**_ 

|||
|--|--|
| ![pyruvate cycle](./img/08_Cellular_Respiration/pyruvate_cycle.png)| ![pyruvate cycle](./img/08_Cellular_Respiration/pyruvate_oxidation.jpg)|
 
### Krebs cycle 

Simple steps of the cycle per pyruvate (without mentioning the enzymes participating in the process):

- Acetyl CoA reacts with Oxaloacetic Acid and form Citrate (Citric Acid).
- NAD<sup>+</sup> is reduced to NADH.
- One carbon is removed from the the citrate and produces CO<sub>2</sub>.
- NAD<sup>+</sup> is reduced to NADH (yes again).
- One carbon is removed from the the citrate and produces CO<sub>2</sub> (yes again).
- GTP is produced from GDP. This will produce ATP.
- Q is reduced to QH<sub>2</sub>. This will produce FADH<sub>2</sub>.
- NAD<sup>+</sup> is reduced to NADH.

_Note: Proteins and fat can be catabolized (broken down) to Acetyl CoA and go into the Krebs cycle._

_**Summary of "energy" produced: 1 ATP, 3 NADH and 1 FADH<sub>2</sub> per pyruvate (=2 ATP, 6 NADH and 2 FADH<sub>2</sub>)**_

 ![Krebs cycle](./img/08_Cellular_Respiration/krebs_cycle.png)

#### Change of energy during the citric acid/Krebs cycle

 ![krebs energy](./img/08_Cellular_Respiration/krebs_energy.png)

### Electron Transport Chain

- Happens **on the inner membrane of the mitochondria**.
- The electron transport chain is a collection of membrane-embedded proteins and organic molecules, most of them organized into four large complexes labeled I to IV.
- As the electrons travel through the chain, they go from a higher to a lower energy level, moving from less electron-hungry to more electron-hungry molecules.
- The electron transport chain results in the active transport of protons (H+) across the inner mitochondrial membrane: **the transmembrane complexes (I,II & IV) act as proton pumps**.​
- The proton pump results in a **proton concentration gradient (called proton-motive force)** and **an electric charge difference** across the inner membrane: potential energy!
​

![electron transport chain](./img/08_Cellular_Respiration/etc.png)
 
### Chemiosmosis

- The **proton-motive force** is like stored energy.
- On the inner membrane of the mitochondria, there's **one protein that allow the H+ to go down their concentration gradient into the mitochondrial matrix**. It is called **ATP Synthase**.
- This protein uses the H+ going down their concentration gradient as **energy to make ATP**. This process is called **Chemiosmosis**.

 ![chemiosmosis](./img/08_Cellular_Respiration/chemiosmosis.png)
 

## Steps for cellular respiration (anaerobic)

- After glycolysis is perfomed, in the abscence of oxygen **fermentation** occurs.
- It happens in the **cytosol**.
- After glycolysis, there's a need to break down the pyruvates.
- It **yields 2 ATP** (which is nothing compared to the 32-38 of cellular respiration).
- Fermentation by-products have a lot of energy remaining.​
- It is either:
  - **Lactic acid fermentation** (microorganisms, muscle cells):
    - Part of the energy created (NADH) is transfered to the pyruvate in order to created **lactate**.
  
    ![lactic fermentation](./img/08_Cellular_Respiration/lactate.png)
  
  - **Alcohol fermentation** (yeast and some plant cells)
    1. Carboxyl group is removed from pyruvate and released in as carbon dioxide.
    2. NADH passes its electrons to acetaldehyde, regenerating NAD+ and forming ethanol

   ![alcohol fermentation](./img/08_Cellular_Respiration/alcohol.png)

## Photosynthesis

- Plants take in CO2 and release glucose and oxygen.

```math
Light + 6CO_2 + 6H_2O \longrightarrow C_6H_{12}O_6 + 6O_2
```

- Photosynthesis is done in the chloroplasts more specifically **on the thylakoid membrane**.
- Two pathways:​
  - **Light reactions**: light energy converted to chemical energy (in ATP and NADPH + H+).​
  - **Light-independent reactions (Calvin cycle)**: use the ATP and NADPH + H+  plus CO2 to produce sugars. ​
  
  ![photosynthesis](./img/08_Cellular_Respiration/photosynthesis.png)
