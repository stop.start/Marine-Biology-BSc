# Cellular Division

## Intro

- Unicellular organisms use cell division primarily for reproduction.​
- In multicellular organisms, cell division is also important in growth, repair of tissues and differentiation.
- Four events must occur for cell division:​
  - **Reproductive Signal**: to initiate cell division. It is important so the cell won't divide if the conditions are not optimal (like if there's no nutrients to feed the cells or for eukarytic organisms creating to many skin cells for example would be detrimental).​
  - **Replication**: of DNA​
  - **Segregation**: distribution of the DNA into the two new cells​
  - **Cytokinesis**: separation of the two new cells​

## Prokaryotes

- **Binary fission** results in two new cells.
- The **reproductive signals** are external factors (nutrient concentration, environmental conditions).
- For many bacterias, the more food the faster the division.
- Usually, prokaryotes have **one circular chromosome**.
- Cytokinesis begins by a pinching in of the plasma membrane; protein fibers form a ring.​
- As the membrane pinches in, new cell wall materials are synthesized, resulting in separation of the two cells.​
- The cells are completely identical.

## Eukaryotes

- There are 2 types of cell divisions: sexual and asexual cell division.

### Mitosis

- **Asexual** cell division.
- The **DNA** after the division **is the same** in both of the cells.
- The signals for reproduction, unlike prokaryotes, isn't based on external factors but on the needs of the whole organisms.
- **Asexual reproduction** is based on mitotic division and produce **clones** of the parent. It is used by:
  - Unicellular organisms.
  - Multicellular organisms that break off to form a new individual.
- **Cell cycle**: from one eukaryotic cell to two cells. It is comprised of the Interphase and Mitosis phases which are themselves composed of several steps.

#### Interphase 

- Phases between divisions where the cell spends most of its life.
  - **G1**: get external signals and decides if the cell needs to divide, the cells grows and synthesize all the needed proteins to replicate, copies organelles etc.
  - **S**: DNA synthesis: the DNA is replicated. The centrosome (microtubule-organizing structure) is also duplicated in this phase.
  - **G2**: check that all is ready for division.
  - **G0**: special phase (usually non represented in diagrams) that is between mitosis (leaving the cell cycle) and G1 phase (entering the cell cycle).
  
  ![cell cycle](./img/10_Cellular_Division/cell_cycle.png)
 
#### Mitosis - M phase
 
- Mitosis phases:
  - **Prophase**: 
    - Chromosomes start to condense. They are chromatids (2 copies of the same chromosome attached at their center by a centromere).
    - The centrosomes move to be one on each side of the cell.
    - Mitotic spindle (microtubules) begins to form. They grow between the centrosomes as they move apart.
  - **Prometaphase**
    - The nuclear envelop disappears.
    - The mitotic spindle attach to the chromosomes from each centrosome.
  - **Metaphase**:
    - - The mitotic spindle attached to all the chromosomes.
    - The chromosomes are lined up in the middle of the cell, ready to divide.
  - **Anaphase**:
    - The sister chromatids separate from each other as they're pulled out by the microtubules to the centrosomes.
    - Microtubules that are not attached to the chromosomes, elongates in order to push and make the cell longer.
  - **Telophase**: 
    - The cells start to return to their original structure.
    - Nuclei form in each cell.
    - **Cytokinesis** takes places (dividing the cytoplasm and its content).

  ![mitosis](./img/10_Cellular_Division/mitosis.png)

### Meiosis

- **Sexual** cell division.
- Unlike asexual reproduction, in **sexual reproduction** the offsprings are not indentical to the parents.
- Meiosis produces **gametes** that differ genetically from the parents, and also from each other.
- Gametes contain one set of chromosomes. They are **haploid** which means that they contain **n chromosome**.
​- **Fertilization**: two haploid gametes (female egg and male sperm) fuse to form a diploid zygote; chromosome number = 2n​.

  ![meiosis vs mitosis](./img/10_Cellular_Division/mitosis_meiosis.png)

#### Interphase 

- Phases between divisions where the cell spends most of its life.
  - **G1**: get external signals and decides if the cell needs to divide, the cells grows and synthesize all the needed proteins to replicate, copies organelles etc.
  - **S**: DNA synthesis: the DNA is replicated. The centrosome (microtubule-organizing structure) is also duplicated in this phase.
  - **G2**: check that all is ready for division.
  
#### Meiosis I

- Stages:
  - **Early prophase I**: 
    - Chromosomes start to condense. They are chromatids (2 copies of the same chromosome attached at their center by a centromere).
    - The centrosomes move to be one on each side of the cell.
  - **Mid-prophase I**:
    - Homologous pairs start getting close to each other.
  - **Late prosphase I - prometaphase**
    - Homologous pairs of chromosomes get close to each other looking like there's only one pair. Thats the way they can exchange genetic material.
    - The nuclear envelop start to disappear.
  - **Metaphase I**:
    - The nuclear envelop disappeared.
    - The chromosomes are lined up in the middle but, unlike mitosis, they are lined up with homologous pairs next to each other.
    - Microtubules spread from the centrosomes to the chromosomes, each side attaching to one of the homologous pairs.
  - **Anaphase I**:
    - Microtubules pull the chromosomes (still with their sister chromatids)
  - **Telophase I**:
    - All chromosomes are on each side just before separation.
    - **Cytokenis**: 2 haploid cells: 23 chromosomes with their sister chromatids in each new cell.

  ![meiosisI](./img/10_Cellular_Division/meiosis1.png)
  
##### Crossover

- During the late prosphase, homologous pairs of chromosomes can exchange genetic material.
- This allow for more diversity.
- The point where the 2 chromosomes attach is called **chiasmata**.

  ![crossover](./img/10_Cellular_Division/crossover.png)

#### Meiosis II

More like mitosis but with a haploid number of chromosomes.  

- Stages:
  - **Prophase II**:
    - Chromosomes with sister chromatids are condensed in the 2 cells just created.
  - **Metaphase II**:
    - Chromosomes are lined up in the middle of the cells.
    - Microtubules spread from centromeres on each side and attach to each of the chromatids 
  - **Anaphase II**
    - The microtubules pull back the chromosomes.
  - **Telophase II**:
    - All chromosomes are on each side just before separation.
    - **Cytokenis**: 4 haploid cells (**gametes***: 23 chromosomes with one chromatid in each new cell.
  
  ![meiosisII](./img/10_Cellular_Division/meiosis2.png)
  
### Life cycles

- In sexual reproduction diploid organisms don't produce diploid organisms (gametes are considered organisms):
  - Diploid organism will create halpoid organism (gametes) via meiosis.
  - Haploid organisms will create diploid organisms via fertilization.

#### Haplontic life cycle

- In protists and many **fungi**.
- Mature organisms in haploid form.
- Mature organisms produce gametes by mitosis.
- Zygote in the only diploid stage (between fertilization and meiosis).

  ![haplontic](./img/10_Cellular_Division/haplontic.png)

#### Alternate life cycle

- Most **plants** and some protists have alternate generations: one generation is haploid the next is diploid and so on.
- Spores divide by mitosis to form the haploid generation (gametophyte).
- Gametophyte forms gametes by mitosis. Gametes fuse to form diploid zygote (sporophyte).​

  ![alternate](./img/10_Cellular_Division/alternate.png)
  
#### Diplontic life cycle

- In **animals** and some plants
- Gametes are the only haploid stage (between meiosis and fertilization).
- Mature organisms are in diploid form.
- Mature organisms produce gametes by meiosis.

### Dying cells

#### Necrosis

- Cell is damaged or starved for oxygen or nutrients. The cell swells and bursts.​
- Cell contents are released to the extracellular environment. Can cause inflammation.​
- Doesn't require ATP

#### Apoptosis

- Genetically programmed cell death. Two possible reasons:​
 - Cell is no longer needed, e.g., the connective tissue between the fingers of a fetus.​
 - Old cells may be prone to genetic damage that can lead to cancer; blood cells, epithelial cells die after days or weeks.
- Requires ATP​ 
 