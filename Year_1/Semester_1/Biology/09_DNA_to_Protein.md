# DNA to Protein - Genotype to Phenotype

## Shapes of the DNA

- Human DNA would be 2 meters long if extended.
- Thickness of the **double helix is 2nm**.

 ![dna thickness](./img/09_DNA_to_Protein/dna_thick.png)

- Prokaryotes have a single circular chromosome.
- Eukaryotes have a specific number of chromosomes and each species have a different number of chromosomes. _For example: humans have 46 (23 pairs), fruit fly have 8 (4 pairs), monkeys have 48 (24 pairs)._
- **Chromatin** is DNA + RNA + proteins.
- **Chromatin** can be found in 2 states: **condensed** and **extended**

### Histones and Nucleosomes

- The DNA is wrapped around proteins called **histones**.
- The **histones** proteins play a role in which genes are active or inactive.
- A **nucleosome** is a basic unit of DNA packaging in eukaryotes, consisting of a segment of DNA wound in sequence around **eight histone protein cores (octamer)**.
- The octamer is 4 types of histones (each is present twice).
- On the outside the octamer has a positive charge. The DNA has a negative charge thus can be wrapped around the octamer.
- The DNA is wrapped 3 times around the octamer of histone and another **histone called H1** "seals" the DNA around the octamer. All of this is called **nucleosome**.
- The DNA between the nucleosomes is called **linker DNA**.

 ![nucleosome](./img/09_DNA_to_Protein/nucleosome.png)

- The histones have **tails** on which other proteins can attached to **open or close the nucleosome**.

 ![histones](./img/09_DNA_to_Protein/histones.png)
 
### Extended chromatin

- Most of the time the chromatin is in its extended form.
- In its extended form the chromatin **looks like beads on a string** because of the nucleosomes.

 ![extended chromatin](./img/09_DNA_to_Protein/extended_chromatin.png)
 
### Condensed chromatin

- The **condensed** state happens when the cell is about to **divide**.
- Chromosomes are visible only when the cell is ready to divide. They unwind, and take they recognizable form which makes them accessible to the **enzymes that transcribe it into RNA**.

#### Process of condensation

1. When **condensing** the chromatin, the nucleosomes are put next to each other resulting in a **filament of 30nm diameter**

![condensed chromatin](./img/09_DNA_to_Protein/condensed_chromatin.png)

2. Then the filament is curled around a proteins filament resulting in a **diameter of 300nm**.
3. This whole complex is also curled up and has a **diameter of 700m**. This is the condensed form of the chromosome.

![chromosome](./img/09_DNA_to_Protein/chromosome.png)
 
### Karytotype

- **Karyotype** is a complete set of chromosomes in a species or in an individual organism. 
- **Autosomal chromosomes** are the chromosomes that **are not** sex chromosomes.
- **Y chromosome** is small compared to the X chromosome.

## Central dogma

- From the data in DNA, RNA can be created. From the data in RNA. polypeptides can be created.

![central dogma](./img/09_DNA_to_Protein/central_dogma.png)

- How does genetic information get from the nucleus to the cytoplasm?​
- What is the relationship between a DNA sequence and an amino acid sequence?​


## Gene expression

- Gene expression is the synthesis of polypeptides from data in the genes.
- Most of the DNA is not genes.
- There is 2 steps in order to get a **polypeptide from a gene**:
  - **Transcription** - copies information from gene to a sequence of RNA.​
  - **Translation** - converts RNA sequence to amino acid sequence.​
- Prokaryotes do the transcription and translation in the cytosol (they don't have a nucleus).
- Eukaryotes do the transcription in the nucleus and the translation in the cytosol.

 ![gene to protein](./img/09_DNA_to_Protein/gene_protein.png)

#### Viruses - the exception

- Viruses are the exception to the central dogma.
- Viruses reproduce inside cells.
- A lot viruses have RNA instead of DNA.
- There are 2 types:
  - Viruses which, from RNA build RNA, from this RNA they build RNA, from this they build proteins.
  - Viruses which build DNA from RNA (**reverse transcription**), from the DNA they'll build RNA and from this RNA they'll build proteins. Those are called **retroviruses** (most known; HIV).

### Transcription - שיעתוק

- **Transcription** is the process in which a gene's DNA sequence is copied (transcribed) to make an RNA molecule.
- It happens in the **nucleus**.
- **RNA polymerase** is the enzyme doing the transcription.
- There are 3 phases to the transcription:
  - **Initiation**
  - **Elongation**
  - **Termination**

 ![transcription](./img/09_DNA_to_Protein/transcription.png)
 
#### Initiation

- **Promoter** is a special sequence of DNA which is used by the **RNA polymerase to know where to start transcribing and in which direction** .​
- Part of each promoter is the **initiation site**.​

#### Elongation

- RNA polymerase binds to the promoter and start synthesizing the RNA from the **template strand (3'-5')** 
- The RNA synthesized is a **reverse complement** and is the copy of the non-template DNA strand (antiparallel to the DNA template strand).​
- RNA polymerase **unwinds DNA about 10 base pairs** at a time; reads template in 3'-5' direction.​
- The zone that is being transcribed forms the **transcription bubble** in order to have the template strand as a single strand.
- Other proteins are in charge of the bubble: its advancement by opening and closing.
- RNA polymerases **do not proofread** and correct mistakes.​

#### Termination

- The termination is specified by a **specific DNA base sequence**.​
- Mechanisms of termination are complex and varied.​
- In **eukaryotes**, the transcribed RNA strand is called **pre-mRNA**.
- The pre-mRNA still need processing in the nucleus to become mRNA.


### From pre-mRNA to mRNA

- In prokaryotes, when the transcription is done, and even before it is finished, a ribosome can attach to the mRNA and start translating.
- In eukaryotes, the pre-mRNA has a **3 stages process** to undergo until it is ready to exit the nucleus:
  - **Capping**: addition of 5' cap.
  - **Polyadenylation**: poly A tail (AAUAAA)​.​
  - **Splicing**: parts not needed are cut out.

 ![pre-mRNA to mRNA](./img/09_DNA_to_Protein/mRNA.png)

#### Capping

- Adding a cap at the **5' end** (start of the strand).
- The cap is a **G nucleotide linked in reverse**: **linked from 5' to 5'** (instead of 3' to 5').

#### Polyadenylation

- At the end of the strand (3' end), a small part is cut.
- The part that is cut is signaled by AAUAAA string of nucleotides.
- Then a long **string of A nucleotides** is added.

#### Splicing

- The pre-mRNA is a copy (except for the T-U nucleotides) of the non-template DNA strand (5'-3').
- Some parts that were synthesized from the DNA to the pre-mRNA are not needed:
  - The needed parts are called **Exon**.
  - The non needed part are called **Intron**.
- The introns are cut out resulting in a shorter mRNA (can even be 80% shorter).
- In this form the mRNA can go through the **nuclear pores** to the **cytosol** to be translated.

### Translation

- **Proteins are build from the mRNA** that went trough the nucleic pores of the nucleus into the cytosol.
- The mRNA contains the codes for **amino acids** that allow the protein to be built. This is the **genetic code**.

#### Codons

- A **codon** is a sequence of 3 RNA bases (A,U,C,G).
- There are 64 codons (4^3).
- Each codon can be translated into an amino acid. 
- Most amino acids can be translated from different codons.
- The first two nucleotides (bases - A,U,C,G) are the most important, the third not always.
- There is one **start codon: AUG**. It also codes for **methionine**. This means that **all proteins start with a methionine amino acid**.
- When AUG if found after the start codon and before the stop codon, then it is translated only to methionine.
- The start codon is usually (but not always) the first AUG. The real start codon is identified a sequence of nucleotides around it called **Shine-Dalgarno** (Kozak in prokaryotes).
- The parts before start and stop are called UTR (UnTranlated Region): 3'UTR 5'UTR.
- There are **3 stop codons**.
- Pretty much all organisms have the same nucleic acids to amino acid dictionary (exceptions: mitonchondria, chloroplasts, one group of protists).

 ![dictionary](./img/09_DNA_to_Protein/dictionary.png)

#### Transfer RNA - tRNA

- **Short RNA molecule that serves as the physical link between the mRNA and the amino acid sequence of proteins**.
- In the cell there are **dozens** of types of tRNA molecules: **one for each amino acid (specific)**.
- Their **plane/2D shape** is called **clover leaf** (see picture).
- The tRNA is composed of **more than the 4 known nucleotides** despite being at first **synthesized with 4 nucleotides**.
- All tRNA molecules have more or less the same **folding/3D shape** which results from **base pairing (H bonds) within the molecule** (see picture).
- The 2 important part of the tRNA are in yellow and red: 
  - Yellow: amino acid attachement site at the **3' end of the tRNA and is always CCA**.
  - Red: **anticodon** (reverse complement of codons): site of **base pairing with the mRNA** and **is unique for each type of tRNA**.

 > _Example for anticodon/codon:_  
 > DNA codon for arginine: 3'-GCC-5'  
 > Complementary mRNA: 5′-CGG-3′​  
 > Anticodon on the tRNA: 3′-GCC-5′  
 > This tRNA is charged with arginine.  ​
 
 ![tRNA](./img/09_DNA_to_Protein/tRNA.png)

##### Charged tRNA

- **Amino-acyl-tRNA synthetases - AARS**: enzymes-proteins that attach an amino acid to its specific tRNA (the tRNA with the corresponding **anticodon**).
- For each type of tRNA there's a specific AARS. 
- The amino acid is attached to the 3' end of the tRNA by an energy rich bond.
- This energy will be used for the synthesis of the peptide bond to join amino acids.
- Steps:
  - At first the the amino-acid is attached the AARS enzyme at a specific site and with the help of a molecule of ATP.
  - Once the amino-acid is attached, two phosphate from the ATP are removed leaving an AMP (adenosine monophosphate) which still has a bit of energy.
  - The amino-specific tRNA then attaches to the enzyme which attaches it to the amino acid with the help of the AMP.
  - Once they're bonded the **charged tRNA** (tRNA + amino acid) leaves the site (enzyme).

  ![charged tRNA](./img/09_DNA_to_Protein/charged_tRNA.png)

#### Ribosomes

![ribosome](./img/09_DNA_to_Protein/ribosome.png)

- There are **2 subunits: one large and one small**.
- Between the subunits sits an mRNA molecule. 
- Multiple ribosomes can translate the same mRNA. The number of ribosome on one mRNA depends on how many proteins (polypeptides) of this type is needed. This is called **polyribosome** or **polysome**.
- In the large subunit there are **3 tRNA binding sites**:
  - **A site**: binds with the anticodon of the charged tRNA.
  - **P site**: where the tRNA adds its amino acid to the **growing chain**.
  - **E site**: "exit", where the tRNA sits before being released.
- How does it start:
  - The small subunit, looks for the **start codon (AUG) in the mRNA**. 
  - Then a tRNA with the methionine (AUG=start codon) amino acid comes and binds (all proteins start with methionine).
  - Then the large subunit attaches.
- Only the correct charged tRNA can bond for each codon:
  - Hydrogen bonds form between the anticodon of tRNA and the codon of mRNA.​
  - Small subunit rRNA validates the match, if the hydrogen bonds have not formed between all three base pairs, it must be an incorrect match and the tRNA is rejected.​
- Like transcription, **translation happens in 3 steps: initiation, elongation, termination**.

 ![protein synthesis](./img/09_DNA_to_Protein/protein_synthesis.png)
 
#### Initiation

- An initiation complex forms—charged tRNA and small ribosomal subunit, both bound to mRNA.​
- rRNA binds to recognition site on mRNA, the Shine-Dalgarno (Kozak) sequence, “upstream” from the start codon.​
- Start codon is AUG; first amino acid is always methionine, which may be removed after translation.​
- The large subunit joins the complex, the charged tRNA is now in the P site of the large subunit.​

 ![initiation](./img/09_DNA_to_Protein/trans_init.png)

#### Elongation
- The second charged tRNA enters the A site.​
- Large subunit catalyzes two reactions:​
  - Breaking the bond between tRNA in P site and its amino acid.​
  - Peptide bond forms between that amino acid and the amino acid on tRNA in the A site.​
- The large subunit has peptidyl transferase activity.​
- RNA acts as the catalyst; normally proteins are catalysts. ​
- Supports the idea that catalytic RNA evolved before DNA.​
- When the first tRNA has released its methionine, it moves to the E site and dissociates from the ribosome—can then become charged again.​

|||
|--|--|
|![elongation](./img/09_DNA_to_Protein/trans_elongation1.png)|![elongation](./img/09_DNA_to_Protein/trans_elongation2.png)

#### Termination

- Translation ends when a stop codon enters the A site.​
- Stop codon binds a protein release factor: allows hydrolysis of bond between polypeptide chain and tRNA on the P site.​
- Polypeptide chain—C terminus is the last amino acid added.
​- Once the chain is done being formed and the stop codon received, the subunits disassemble and the polypeptide goes free.

||||
|--|--|--|
|![termination](./img/09_DNA_to_Protein/trans_termination1.png)|![termination](./img/09_DNA_to_Protein/trans_termination2.png)|![termination](./img/09_DNA_to_Protein/trans_termination3.png)|

### Folding and Sending the Polypeptides to their Destination
 
- After translation, the amino acid chain still needs to be folded and sent to its destination.
- The polypeptide folds as soon as it goes free from ribosome.
- Its destination can be inside the cell, in one of the organelles or outside the cell.
- Each polypeptide has an "address" which is a sequence within the polypeptide called **signal sequence**.
- The signal sequences can give a set of instructions. Examples:
  - Finish translation and send to an organelle.
  - Stop translation, go to the ER, finish synthesis there.
- Conformation of signal sequences allow them to bind specific receptor proteins - docking proteins - on outer membranes of organelles. ​
- Receptor forms a channel that the protein passes through.​
- Polypeptides which will be used by **organelles** finish being synthesized in the **cytosol** and then are sent to their respective organelle.
- Polypeptides which are meant to go **outside of the cell** or **on the plama membrane** of the cell finish being synthesized in ribosomes on the RER:
  - The signal sequence binds to a receptor on the RER while the ribosome is still synthesizing the amino acids chain.
  - The signal sequence is cut off the polypeptide, it has served its purpose.
  - Once the polypeptides chain is done being synthesized it is released in the RER membrane.
  - Then they are sent to the Golgi from where they will be shipped off in vesicle.

![protein routes](./img/09_DNA_to_Protein/proteins_routes.png)

#### Posttranslational Modifications

- **Proteolysis**: cutting the polypeptide chain, by proteases.​
- **Glycosylation**: addition of sugars to form glycoproteins.​
- **Phosphorylation**: addition of phosphate groups by kinases. Charged phosphate groups change the conformation.​
