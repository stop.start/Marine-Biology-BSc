# Basic Chemistry

## Periodic Table for Biologists

![periodic table for biologist](./img/02_Basic_Chemistry/biologists_pt.png)

- Elements for biologists (the yellow ones in the above table): **H, C, N, O, P, S**.
- Elements in orange in the above table are found under certain conditions like in ions but there are not part of the molecules.

## Basic Chemistry

- **Reactants** are the molecules participating in the reaction.
- **Products** are the result of the reaction.
- Some bonds contain energy that is released when breaking during a reaction.

![reaction](./img/02_Basic_Chemistry/reaction.png)

### Number of covalent bonds per "biologists" atoms

| Atom         | Number of covalent bonds |
|--------------|--------------------------|
| [H]ydrogen   | 1                        |
| [O]xygen     | 2                        |
| [S]ulfur     | 2                        |
| [N]itrogen   | 3                        |
| [C]arbon     | 4                        |
| [P]hosphorus | 5                        |

### Geometry

- Carbon is the base that build us.
- The basic geometric form is **tetrahedon** because carbon makes 4 bonds.
- Example - methane:

![methane](./img/02_Basic_Chemistry/methane.png)

### Polarity

- **Polar covalent bond** is when an atom is more electronegative than the other thus hogging more the shared electrons.
- **Non polar covalent bond** is when 2 atoms have around the same electronegativity thus sharing the electrons pretty much equally.
- Polar bonds are marked with $`\delta +`$ and $`\delta -`$
- Example - water:

![water molecule](./img/02_Basic_Chemistry/water_molecule.png)

- In biology, **polar** molecules are called **hydrophilic** (love water).
- In biology, **non polar** molecules are called **hydrophobic** (hate water).

### Hydrogen bond

- Polar molecules usually contain hydrogens.
- **Hydrogen bond**: weak bond between polar molecules.

![hydrogen bond](./img/02_Basic_Chemistry/hydrogen_bond.png)

### Ions

- **Ions and ionic compounds are hydrophilic**.

