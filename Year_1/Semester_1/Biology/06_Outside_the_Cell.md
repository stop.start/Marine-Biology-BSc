# Outside the cell


## Cell junctions
**How do cells communicate?**  

### Gap junctions

- **Gap junctions** allow the exchange of substance between cells (cytosplasmic connection).
- **Connexins** (in vertebrates, **innexins** in invertebrates) are membrane proteins that can form by group of 6 a structure called **connexon**.
- When those connexons align between cells they act as channels.
  
  ![gap junctions](./img/06_Outside_the_Cell/gap_junctions.png)

### Tight junctions

- They create a **watertight seal** between two adjacent animal cells.
- Groups of proteins called **claudins**, interact with a partner group on the opposite cell membrane and allow to hold the cells tightly agains each other.
- The purpose is to keep liquids from escaping between the cells.
  
  |                                                                   |   |
  |--|--|
  | ![tight junctions](./img/06_Outside_the_Cell/tight_junctions.png) | ![tight junctions](./img/06_Outside_the_Cell/tight_junctions2.png)  |


### Desmosomes

- They act like **spot welds**.
- They connect **cytoskeleton of adjacent cells**.
- Desmosomes pin adjacent cells together, ensuring that cells in organs and tissues that stretch, such as skin and cardiac muscle, remain connected in an unbroken sheet.
  
  |                                                                   |   |
  |--|--|
  | ![desmosome](./img/06_Outside_the_Cell/desmosome.png) | ![desmosome](./img/06_Outside_the_Cell/desmosome2.png)|


## Cell wall (plant cells)

- Plants don't have collagen.
- Plants have a **cell wall** which is their type of extracellular matrix.
- The **cell wall** is around the plasma membrane and gives the cell a cubical shape.
- The **cell wall** is rigid, which gives the plant the ability to "stand".
- It made of **polysaccharide** like **pectin** or **hemicellulose**, the main component of the cell wall being **cellulose**.
- The **middle lamella** is the space between the cells, and it is sticky which allow it to keep the cells next to each other.

> _Note:  
> As humans, we cannot digest fibers but they are still good for digestion._  

![cell wall](./img/06_Outside_the_Cell/cell_wall.png)

**How do cells communicate?**  
- **Plasmodesmata** are holes in the cell wall that allow plant cells to communicate or exchange between one another.
- This is a cytosplasmic connection.

![plasmodesmata](./img/06_Outside_the_Cell/plasmodesmata.png) 

## Extracellular matrix - ECM (animal cells)

- The **extracellular matrix** helps coordinates cells together.
- It is built by the cells.
- It is made of different type of proteins and carbohydrates secreted by the cell, like **collagen** proteins.
- The extracellular matrix contains **proteoglycans** attached to a long **polysaccharide backbone**.
- The extracellular matrix is directly connected to the cells mainly by proteins called **integrins** which are integreted into the plasma membrane and are linked to the cytoskeleton.

![extracellular matrix](./img/06_Outside_the_Cell/extracellular_matrix.png)

## Cell signaling

- Membrane proteins process information. ​
- Binding of a specific ligand can initiate, stop, or change cell functions.​

  ![signaling](./img/06_Outside_the_Cell/signals.png)
