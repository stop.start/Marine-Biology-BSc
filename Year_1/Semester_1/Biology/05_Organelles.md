# Organelles

## Nucleus

- Usually the largest organelle.
- Contains the DNA. It does not get out of the nucleus.
- Site of **DNA replication** (semiconservative replication).
- Contains the **nucleolus** is where the ribosomes are made.
- The **nuclear envelope** is made of a **double membrane** with water in between.
- There’s a thin space between the two layers of the nuclear envelope, and this space is directly connected to the interior of another membranous organelle, the **endoplasmic reticulum** which makes proteins.
- **Nuclear pores** are small channels that span the nuclear envelope. They let substances enter and exit the nucleus.
- Each pore is lined by a set of proteins, called the **nuclear pore complex**, that control what molecules can go in or out.
- The **nucleolus** is the site where new **ribosomes are assembled**.

![nucleus](./img/05_Organelles/nucleus.png)

### DNA
- DNA is organized in **chromosomes**.
- The DNA would be 2 meters long if extended.
- The DNA is wrapped around proteins called **histones**. 
- DNA and all the proteins that know how to use it form a complex called **chromatin**.
- The **chromatin** fills up the nucleus.
- Prokaryotes have a single circular chromosome.
- Eukaryotes have a specific number of chromosomes and each species have a different number of chromosomes. _For example: humans have 46 (23 pairs), fruit fly have 8 (4 pairs), monkeys have 48 (24 pairs)._
- Chromosomes are visible only when the cell is ready to divide. They unwind, and take they recognizable form which makes them accessible to the enzymes that transcribe it into RNA.
- The **histones** proteins play a role in which genes are active or inactive.

#### Cell division

1. When a cell wants to divide, it first **replicates the DNA**. 
   - This means that the cell contains twice as much DNA.
   - Replicated chromosomes are bonded together.
2. Replicated chromosomes are split.
   - Each is put on a different side of the cell so that on either side of the cell there are 23 pairs of chromosomes.
   - During this process the chromosomes are **condensed**.
   - Condensed chromosomes are unreadable that's why they're in this form only during this part of the cell division.
   - Once the chromosomes are split they are expanded again.
   
## Ribosomes

- **Ribosomes are the site of proteins synthesis**.
- They only create **chains** of proteins, they don't fold them.
- Ribosomes are both in prokaryotic and eukaryotic cells.
- In **eukaryotic cells** they can be found:
  - Free in the cytoplasm.
  - Attached to the ER.
  - Inside the mitochondria and chloroplasts.
- Ribosomes are mainly composed of **ribosomal RNA** as well as some proteins.
- Ribosomes are a type of **ribozymes**.

![ribosome](./img/05_Organelles/ribosome.png)

## Endomembrane system

### Endoplasmic reticulum (ER)

- The **nuclear envelope** is directly connected to an **organelle** called **endoplasmic reticulum (ER)**.

#### Rough ER (RER)

- On the ER, **ribosomes are attached** and those parts of the ER are called **rough ER**.
- The ribosomes attached on the external side of the RER (side facing the plasma membrane).
- When a **protein** is synthesized by a ribosome on the ER, it won't be inside the cystosol, but in the inside of the ER called the **lumen**.
- Some of the proteins stay in the ER but others need to be secreted outside of the cell.
- Then the protein buds out of the ER. It has a "mini-membrane" and this complex is called a **vesicle**.
- Those vesicles go to the **Golgi apparatus** and attach to it.

#### Smooth ER (SER)

- It is more **tubular** and there are **no ribosomes attached**.
- It chemically modifies small molecules such as drugs and pesticides.
- Hydrolysis of glycogen in animal cells.
- **Synthesis of lipids and steroids**.

![endoplasmic reticulum](./img/05_Organelles/endoplasmic_reticulum.png)
 
### Golgi apparatus or Golgi body

- There can be several Golgi apparatus in the cell.
- The side of the Golgi apparatus that **receives the vesicles** is called the **cis face**
- The side of the Golgi apparatus where the **vesicles bud out** is called the **trans face**.
- When a vesicle comes from the ER to the cis face of the Golgi body, it merge with it and the content (protein or lipid) is transfered to the lumen of the Golgi body.
- The lipids or proteins undergo some modifications - **the proteins are folded** -  in the Golgi body that allow to know what to do with them and where to send them.

 ![er and golgi](./img/05_Organelles/er_golgi.png)
 
### Lysosomes (animal cells)

- It is an **organelle** that contains digestive enzymes.
- It is created by the ER and Golgi apparatus.
- It acts as the **recycling facility** of the cell.
- It breaks down old and unnecessary structures so their molecules can be reused.
- Some of the vesicles that leaves the Golgi apparatus are meant for the lysosome.

### Vacuols (plant cells and protists)

- Store waste and toxic compounds.
- Provide structure for plant cells - **turgor**.


## Mitochondria

_**Mitochondria are the powerhouses of the cell**_

- The **mitochondria** make ATP (Adenosine TriPhosphate) from the energy stored in chloroplasts (in animal cells, after said animal has ingested the plant with the chloroplasts)
- The process of making ATP using chemical energy is called **cellular respiration**.
- Mitochondria are in the cytosol of the cell.
- They are oval shaped with **two membranes** (inner and outter).
- The **inner membrane** has many inward protrusions called **cristae** that increase surface area.
- The **inner membrane** was thought to be one "fluffy part" but it turns out it is not,so most of representations found in books and internet isn't acccurate.
- The space inside the inner membrane is called the **mitochondrial matrix**.
- It contains mitochondrial DNA and ribosomes.
- Depending on the type of cell, there can be from none to a lot on mitochondria. _For example, muscle cells need to create a lot of energy so they have a lot of mitochondria, but blood cells have no such needs thus don't have any_.

![mitochondria](./img/05_Organelles/mitochondria.jpg)

## Chloroplasts

- Chloroplasts are organelles only found in plants and algae. 
- They're responsible for capturing light energy to make sugars from carbon dioxide. This is called **photosynthesis**.
- The sugars produced in photosynthesis may be used by the plant cell, or may be consumed by animals that eat the plant, such as humans. 
- The energy contained in these sugars is harvested through a process called **cellular respiration**, which happens in the **mitochondria** of both plant and animal cells.
- They are disc-shaped organelles in the **cytosol** of the cell, with **two membranes** (inner and outter).
- They contain chloroplast DNA, ribosomes and **thylakoids** surrounded by a fluid called **stroma**
- **Thylakoids** are membrane discs that are interconnected and arranged in stacks called **grana** (singular: granum).
- The **thylakoids** contain what is needed to harvest light (not learned yet). It includes **chlorophyll** which gives the green color.
- The **thylakoids** discs are hollow and their inside is called **thylakoid space** or **lumen**.

![chloroplast](./img/05_Organelles/chloroplast.png)


>_Note:  
> Chloroplasts and mitochondria are thought to have been bacteria which at some point were ingested by a larger cell and not broken done._ 

## Chromoplasts

- In plants only.
- Contain red, orange and yellow pigments.
- Give flowers their color.

## Leucoplasts

- In plants only.
- Store starch and fats.

## Peroxisomes

- Collect and break down toxic waste created from metabolism.
- Especially for molecules containing oxygen which will bond easily with any hydrophilic molecules.

## Cytoskeleton

- Cytoskeleton is a **network of filaments** which :
   - Gives cells their shapes.
   - Supports the plasma membrane.
   - Help keep the organelles in place.
   - Provides tracks for the transport of vesicles.
   - Allows the cell to move (in many cells) by dissamemblimg and reasembling the cytoskeleton.
- In eukaryotes, there are three types of **protein fibers** in the cytoskeleton: 
  - **Microfilaments** 
  - **Intermediate filaments** 
  - **Microtubules**

![cytoskeleton](./img/05_Organelles/cytoskeleton.png)

### Microfilaments

- They are the narrowest of the protein fibers (diameter 7nm).
- They are made of **actin**, a monomer and are structured in a double **helix shape**.
- They are also known as **actin filaments**.
- They have two different structural ends: **plus and minus**.
- They can grow and dissable quickly.
- Their **roles**:
  - Serve as tracks for the movement of a motor protein called **myosin**, which can also form filaments. Because of its relationship to myosin, actin is involved in many cellular events requiring motion.
  - Serve as "highways" for the **passage of vesicles and organelles**. They are carried by myosin motors.
  - **Help with motility**.
  - Give the cell its shape and structure: a network of actin filaments is found in the region of cytoplasm at the very edge of the cell. This network, which is linked to the plasma membrane by special connector proteins.
  
![actin filaments](./img/05_Organelles/actin_filament.jpg)


### Intermediate filaments

- They have a diameter of 8 to 10nm.
- They are made of made of multiple strands of fibrous proteins wound together.
- There is a number of different intermediate filaments each made of different type of proteins: 50 different kind in 6 molecular classes.
- Their **role** is mainly to keep the structure of the cell: they are specialized to **bear tension - עמיד בפני מתיחה**, and their jobs include maintaining the shape of the cell and anchoring the nucleus and other organelles in place.

![intermediate filament](./img/05_Organelles/intermediate_filament.png)

### Microtubules

- They are the largest of the protein fibers with a diameter of 25nm.
- They are made of **tubulin proteins** and form a hollow tube.
- Each tubulin protein consists of two subunits, α-tubulin and β-tubulin.
- They can **grow and shrink quickly** by the addition or removal of tubulin proteins.
- They have two different structural ends: **plus and minus**.

 ![microtubule](./img/05_Organelles/microtubule.png) 

- Their **roles** are: 
  - Help the cell **resist compression forces**.
  - During cell division they assemble into a structure called **spindle** which pulls the chromosomes apart.
  - They are also **used as tracks to motor proteins** called **kinesins** and **dyneins** which, given energy, transport vesicles.
    - **Dyneins**: binds to microtubule doublets and allows them to slide past each other by detaching from one doublet and reattaching on another place on the that same doublet.
    - **Kinesins**: binds to a vesicle and “walks” it along by changing shape.​ It only knows to go **minus to plus**.

   |                                             |                                               |
   |---------------------------------------------|-----------------------------------------------|
   | ![dyneins](./img/05_Organelles/dyneins.png) | ![kinesins](./img/05_Organelles/kinesins.png) |
   |                                             |                                               |

- Cilia and eukaryotic flagella are made of microtubules (9+2 array).
- The microtubules within the **basal body** in the cytoplasm as **9 triplets**.
- Outside of the basal body it composed of **9 doublets**.

![cilia](./img/05_Organelles/cilia.png)

