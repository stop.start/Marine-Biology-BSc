# Intro

## Classification method

- Invented by Carolus Linnaeus.
- The practice of classifying is called taxonomy. 

### Criterions

- **Morphology**: external appearance.
- **Development**: from foetus to adult.
- **Paleontology**: 
- **Behavior**: for example frogs voices (this criterion is not much used).
- **Molecular Data**: DNA. Today's main criterion.

## Tree of life

### Common ancestor

- Cells are either **prokaryotes** or **eukaryotes**.
- From the common ancestor to today, **life** can be separated in **3 main groups**:

  ```math
  \begin{array}{cc}
   \begin{array}{cc}
   \bold{Bacteria} \Rightarrow prokaryote
   \end{array} \Bigg\} \\
   \bold{Archaea} \Rightarrow prokaryote \\
   \bold{Eukarya} \Rightarrow eukaryote
  \end{array} \Bigg\}
  ```
- **Eukaryotes** separated in **3 main groups**:

  ```math
  \begin{array}{cc}
   \begin{array}{cc}
   \bold{Plants}
   \end{array} \Bigg\} \\
   \bold{Fungi}\\
   \bold{Animal}
  \end{array} \Bigg\}
  ```
- Eukaryotes that are not from the 3 main groups and are unicellular and those are all **protists**.
- Depending on the type of classification, **protists** can be in one big group or several groups.
- The below graph is important

![classification](./img/01_Intro/classification.png)

### Evolution

- Every species name is composed of 2 words and is written in *italic*:
 - **genus** (גוף) starts with a *capital*
 - **species name**
> Example:  
> *Home sapiens**  

![evo2](./img/01_Intro/evolution2.png)

![evo1](./img/01_Intro/evolution1.png)
