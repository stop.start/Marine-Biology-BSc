# Macromolecules

## Intro

- There are 4 types of **macromolecules**:
  - **Carbohydrates** (סוכרים)
  - **Lipids** (שומנים)
  - **Proteins** (חלבונים)
  - **Nucleic acids** (חומצות גרעין)
- Proteins can be called **polypeptides**.
- Carbohydrates can be called **[poly]saccharides**
- Most of the macromolecules are proteins and nucleic acids (but mostly proteins).
- Molecules with a weight greater than 1000 daltons are considered macromolecules.

### Some terms to know

- **Monomer**: a molecule that can bond to other molecules of the same kind.
- **Dimer**: two bonded molecules of the same kind.
- **Trimer**: chain of three molecules of the same kind.
- **Oligomer**: long chain of molecules of the same kind.
- **Polymer**: really long chain of molecules of the same kind.

| Macromolecule | Monomer        | Oligomer        | Polymer        |
|---------------|----------------|-----------------|----------------|
| Proteins      | Amino acid     | Peptide         | Polypeptide    |
| Nucleic acids | Nucleotide     | Oligonucleotide | Nucleic acid   |
| Carbohydrates | Monosaccharide | Oligosaccharide | Polysaccharide |

__*Lipids don't usually form polymers__

## Dehydration synthesis (condensation) and Hydrolysis

- Dehydration synthesis is also called **condensation reaction**.
- **Dehydration synthesis** is a reaction where **monomers** form bonds between them by releasing water: one monomers release an H and one release a hdyroxyl group OH thus forming a **water molecule and a covalent bond**.
- Monomers are joined by covalent bonds so **energy must be added**.

![dehydration synthesis](./img/03_Macromolecules/dehydration_synthesis.png)

- **Hydrolysis** is the reverse reaction where water added to a polymer will split the polymer back to monomers.
- Hydrolysis: _hydro_=water; _lysis_=break 
- **Hydrolysis releases energy**.

![hydrolysis](./img/03_Macromolecules/hydrolysis.png)

- **Enzymes** catalyze (speed up) those reactions. 

## Functional groups

- Functional groups are groups of molecules that, when added to hydrocarbons, give the same chemical properties.
- On the molecule representation, **R** means a non-specified hydrocarbon.

### Groups
- **Hydroxyl**: **OH** molecule attached to the hydrocarbon. 
  - It turns a hydrocarbon into an **alcohol**. 
  - **OH is polar**. 
  - Because it is polar it **can be dissolve into water**. 
  - _Note hydroxide refers the the **ion OH<sup>-</sup>**, hydroxyl refers to the OH group as part of a molecule for example CH<sub>3</sub>OH_.
- **Carboxyl**: if one of the carbon from the hydrocarbon has a bond to a hydroxyl group as well as a bond to a carbonyl group then the whole is considered a **carboxyl group**. 
  - **Polar**.
  - Since the oxygen can easily lose the H<sup>+</sup> it is considered and **acid**.
- **Amino**: **NH<sub>2</sub>**. 
  - Since Nitrogen has 5 valence electrons, NH<sub>2</sub> has a lone pair of electrons and can swipe some H<sup>+</sup> ions which makes it **basic**. 
  - _Note: hydrocarbon with amino group and carboxyl group is an **amino acid**_

![functional_groups](./img/03_Macromolecules/functional_groups.png)

## Carbohydrates

- Molecules that **contain energy** that can be transferred.
- Serve as structural component.
- Prefix **glyco** means that a sugar has been added to the molecules.
- Can be found as **[mono/di/oligo/poly]saccharide** (oligosaccharide is between 3 and 20 monosaccharides).
- Carbohydrates are molecules made of **carbon, oxygen and hydrogen**.
- **The general formula is $`\bold{CH_2O \Rightarrow}`$** ratio of one C to 2 H and 1 O.
- All carbon atoms in the molecules are numbered. 
- Carbohydrates can be **modified** by adding functional groups. Their function change entirely. _Example: sugar phosphate composed of fructose with phosphate group._


### Glucose

- **Formula: $`\bold{C_6H_{12}O_6}`$**.
- It is an **hexose**: six-carbon backbone.
- The same Carbon is also bonded to a Hydrogen which makes it part of the **Aldehyde group** thus it is an **aldose**.
- Can be in a cyclical form (ring).

![glucose](./img/03_Macromolecules/glucose.png)

- In water glucose tend to open and close.
- There a **2 forms** of the ring form:
  - $`\alpha\text{Glucose}`$
  - $`\beta\text{Glucose}`$
- The **difference** between the two forms is the **orientation of the hydroxyl group**.
- $`\alpha\text{Glucose}`$ cannot transform into $`\beta\text{Glucose}`$ and vice-versa. They have to open and close in order to transform from one form to another.

#### Other hexoses

- All hexoses have the same formula $`\bold{C_6H_{12}O_6}`$
- The difference is how the ring is closed and where the hydroxyl group is attached.
- Other hexoses besied glucose are: 
  - Fructose
  - $`\alpha`$-Mannose
  - $`\alpha`$-Galactose

![hexoses](./img/03_Macromolecules/hexoses.png)

### Disaccharides

- Linking between 2 sugar monomers is called **glycosidic linkage**.
- In a glycosidic linkage there always an atom of oxygen (see in the example below how the 2 molecules are linked by an oxygen).

> Example - Linking between glucose and fructose:  
> Glucose and Fructose link between the **glucose hydroxyl group and the hydrogen on carbon 2 of the fructose** releasing one molecule of water.  
> This linkage is called $`\bold{\alpha-1,2 \text{Glycosidic linkage}}`$
> The **disaccharide** formed is called **sucrose**.  
> ![glycosidic linkage](./img/03_Macromolecules/glycosidic_linkage.png)

- Other examples:

![maltose](./img/03_Macromolecules/maltose.png)
![cellobiose](./img/03_Macromolecules/cellobiose.png)

- Our bodies are able to break down maltose but not cellobiose although the molecules are really close.

### Polysaccharides

- Polysaccharides are giant polymers of monosaccharides connected by glycosidic linkages.
- **Cellulose** and **Starch** or **Glycogen** are such polymers.
- Both starch and glycogen are polymers of $`\alpha`$-glucose. 
- **Starch** is found in **plants**.
- **Glycogen** is found in **animals**.

| Cellulose                                          | Starch & Glycogen                                                          |
|----------------------------------------------------|----------------------------------------------------------------------------|
| Structural molecule - used to build strong tissues | Storage molecule - used to store available energy to use at any given time |
| Monomers linked on $`\beta-1,4`$                   | Monomers linked on $`\alpha-1,4`$                                          |
| Animals can't break down cellulose*                | Animals can break down starch/glycogen                                     |

_*Cows harbor bacterias in their stomach that know how to break down cellulose but are not break them down themselves._

## Lipids

- Lipids are **hydrophobic** because of the non-polar covalent bond between carbon and hydrogen.

| Types of lipids                        | What they do                      |
|----------------------------------------|-----------------------------------|
| Fats and oils                          | Store energy                      |
| Phospholipids                          | Structural role in cell membranes |
| Carotenoids and chlorophylls           | Capture light energy in plants    |
| Steroids and modified fatty acids      | Hormones and vitamins             |
| Animal fat                             | Thermal insulation                |
| Lipid coating around nerves            | Provides electrical insulation    |
| Oil and wax on skin, fur, and feathers | Repels water                      |

### Fats and Oils

- They store energy.
- They are called **triglyceride**.
- **Triglycerides** are composed of a **glycerol** molecule and **3 fatty acids**. The glycerol and fatty acids **link on the hydroxyl groups** and **release $`\bold{3H_2O}`$**.

![triglyceride](./img/03_Macromolecules/triglyceride.png)

#### Un/Saturated fat

- A **saturated fat** is a triglyceride that has fatty acids with only **single bonds between carbons**. Because of the single bonds it contains as many hydrogens as it can. It is **solid** at room temperature.
- An **unsaturated fat**  and **polyunsaturated fat** is a triglyceride that has fatty acids with **double bonds in the cis form**. Unsaturated fat refers to one double bonds whereas polyunsaturated fat refers to several double bonds. Those are usually **liquid**.
- The **longer and saturated** a fatty acid is the **more solid** it is.
- The **shorter and unsaturated** a fatty acid is the **more liquid** it is.

### Phospholipids

- **Hydrophilic head** and **hydrophobic tails**.
- It looks like a fatty acid but with a hydrophilic molecule attached to the phosphate group and only 2 fatty acid.
- The **head** is composed of a molecule of **choline** which is hydrophilic, a **phosphate** and **glycerol**.
- The **tails** are 2 fatty acids.

![phospholipid](./img/03_Macromolecules/phospholipid.png)

- Phospholipids can arrange in different structures:
- **Micelle**
- **Liposome**
- **Phospholipid bilayer** is the most important structure since it is what makes the **cells membranes**.

![phospholipid_structures](./img/03_Macromolecules/phospholipid_structures.png)

### Carotenoids

- Light absorbing pigment. _Example: carotene which split in 2 to get vitamin A._
- In plants $`\beta`$-carotene trap light in photosynthesis.

### Steroids

- Steroids are signagling molecules.
- Are built with fused rings.
- Found in cholesterol, vitamin D.
- Cholesterol is the base on which are built testosterone and estrogen. Testosterone has a double bond to $`O`$ and one more $`CH_3`$ while estrogen has a hydroxyl group.

![steroids](./img/03_Macromolecules/steroids.png)

## Proteins

Proteins are polymers made of **amino acids** and can be also **polypeptides**. There are several types of proteins like **enzymes** and **hormones**, each serving a different purpuose.  

| Types of proteins        | What they do                                             |
|--------------------------|----------------------------------------------------------|
| Enzymes                  | Catalyze reactions                                       |
| Structural Proteins      | Provide physical stability and movement                  |
| Defensive Proteins       | Recognize and respond to nonself substance (antibodies)  |
| Signaling Proteins       | Control physiological processes (hormones)               |
| Receptor Proteins        | Receive and respond to chemical signals                  |
| Membrane Transporters    | Regulate passage of substance accross cellular membranes |
| Storage Proteins         | Store amino acids for later use                          |
| Transport Proteins       | Bind and carry substances within the organism            |
| Gene Regulatory Proteins | Determine the rate of expression of a gene               |

- Proteins are first created as **long and unbranched polypeptide chains**.
- The chain is then folded into a **specific 3D shape**. 
- What sets the **shape and function** is the **sequence of the amino acids**.
- Proteins can constitute of one or more polypeptide chains.

![polypeptide](./img/03_Macromolecules/polypeptide.png)

### Amino acids

- There are **20 common amino acids groups** for all organisms.
- All **amino acids** have the same structure base with a **carbon** bonded to an **amino group**, a **carboxyl group** and an **hydrogen**.
- The geometric form is **tetrahedral**.
- The **carbon** is usually **noted α**.
- The R is the "remainder": the part that will differenciate between the amino acids. It can have many shapes and sizes.

![amino acids base](./img/03_Macromolecules/amino_acids_base.png)

- Usually, the **amino group protonated** and the **carboxyl group deprotonated** as shown above (although some representations can show them neutral). This means they have on one end a positive charge and on the other end a negative charge, being in all **neutral**. This is called a **zwitterion** (zwitter in german means hybrid).
- The amino group is **basic** and the carboxyl group is **acidic**. Together they are **neutral**.
- Because of the tetrahedral form, there are 2 isomers possible which are called **L-Alamine** and **D-Alamine**. The only **naturally found** isomer is **L-Alamine**
- Some amino acids are **hydrophilic** and will be on the **outside** of the protein. 
- Some amino acids are **hydrophobic** and will be on the **inside** of the protein.

**Amino acids can be divided into 4 groups which are based on the remainder R**:

#### Charged amino acids

- Those are **hydrophilic**.
- They attract ions of the opposite charge.

![charged amino acids](./img/03_Macromolecules/charged_amino_acids.png)


#### Polar amino acids

- Those are **hydrophilic**.

![polar amino acids](./img/03_Macromolecules/polar_amino_acids.png)

#### Hydrophobic (non-polar) amino acids

- Non polar thus **hydrophobic**.

![non polar amino acids](./img/03_Macromolecules/hydrophobic_amino_acids.png)

#### Special cases

- Glycine has a R[emainder] only composed of an H[ydrogen]. That's why it's the only amino acid with only one isomer (it is symetric).
- Proline is the only that forms a ring **with** the base (as opposed to having a ring in the remainder).
  - When chaining the amino acids, the chain is just an unshaped string but, where there is a proline there's a small "kink (פינה)".
- Cysteine has a [S]ulfure which is active due to the [H]ydrogen it is bonded to. It can chain itself to other Cysteines with a covalent bond (SS bond).

![specific amino acids](./img/03_Macromolecules/special_amino_acids.png)

### Formation of polypeptides

- Petpide bonds are formed by **dehydration synthesis**.
- The linkage between the amino acids is done between the bases. The R part is not included at all in the process.
- The dehydration synthesis process is done between **hydroxyl group of the last amino acid** on the chain and the **hydrogen of the amino acid that is being added** release a water molecule. 
- One extremity of the chain (on the left of the image) is called **N terminus** (from the $`H_3N^+`$ amino group).
- The other extremity of the chain (on the right of the image) is called **C terminus** (from the $`COO^-`$ carboxyl group).
- Amino acids are always added to the end of the chain (from N terminus to C terminus)!
- The NC bonds formed from the dehydration synthesis form the polypeptide **backbone** (N-C-N-C...)

![peptide bonds](./img/03_Macromolecules/peptide_bonds.jpg)


There are **4 levels of protein structure**: primary, secondary, tertiary and quarternay.  

- **Primary structure**:
The primary structure is the simplest since it is the **sequence of the amino acids** in a polypeptide chain. Each chain contains amino acids in a specific order.

- **Secondary structure**:
They are **local sub-structures** formed due to the **hydrogen bonds between the atoms of the backbone** (backbone refers to the polypeptide chain without the R groups). 
The mains structures includes:  
   - **α helices**: the carbonyl (C=O) of one amino acid is hydrogen bonded to the amino H (N-H) of an amino acid that is four down the chain
   - **β-pleated sheets**: two or more segments of a polypeptide chain line up next to each other, forming a sheet-like structure held together by hydrogen bonds. The hydrogen bonds form between carbonyl and amino groups of backbone, while the R groups extend above and below the plane of the sheet.  
       - The strands of a **β pleated sheet may be parallel**, pointing in the same direction (meaning that their N- and C-termini match up).
       - The strands of a **β pleated sheet may be antiparallel**, pointing in opposite directions (meaning that the N-terminus of one strand is positioned next to the C-terminus of the other). 
  - Some proteins have only one or the other or they can have both.
 
![alpha beta](./img/03_Macromolecules/alpha_beta.png)
 
- **Tertiary structure**:
- This is the overall 3D structure of a polypeptide and is primarily due to the interaction between the R groups of the amino acids that makes up the protein.
- It orders the α helices and β-pleated sheets in relation to one another.
- The **hydrophobic** amino acids are more likely to be found inside the protein while the **hydrophilic** ones are more likely to be found outside of the protein and will interact with the surrounding water molecules.
- **Disulfide bonds**, covalent linkages between the sulfur-containing side chains of cysteines, are much stronger than the other types of bonds that contribute to tertiary structure. They act like molecular **safety pins, keeping parts of the polypeptide firmly attached to one another**.

- **Quaternary structure**
- Proteins that are made up of only one polypeptide chain have only 3 levels of structure.
- Some proteins are made up of several polypeptide chains and when they comes together it makes the **quaternary structure**(for example: hemoglobin).
- The interactions in this levels are more or less like the tertiary structure.
- A protein composed of the same proteins is called **homodimer**.
- A protein composed of different proteins is called **heterodimer**. 

### Ligand

- Receptors and ligands are molecule that come in many forms. 
- A receptor recognizes just one (or a few) specific ligands, and a ligand binds to just one (or a few) target receptors.
- Binding of a ligand to a receptor changes its shape or activity, allowing it to transmit a signal or directly produce a change inside of the cell.

![ligand](./img/03_Macromolecules/ligand.png)

### Denaturation/Renaturation

- **Denaturation**: 
  - Protein that **lose its shape** due to **heating, change of pH, high concentration of polar molecules**.
  - A denaturated protein loses its function.
- Renaturation:
  - Reshape the proteins after denaturation.
  - Done in lab for simple proteins, but complex ones are more.. well.. complex.

### Chaperons

- **Chaperons** are proteins that prevent mistakes during proteins development.
- Proteins get into the chaperons proteins unshaped and get out with their correct shape.
- Chaperons know how to fold multiple proteins.

![chaperon](./img/03_Macromolecules/chaperon.png)


## Nucleic Acids

- There are 2 main groups of nucleic acids:
  - **DNA** - deoxyribonucleic acid
  - **RNA** - ribonucleic acid
- Nucleic acids (DNA ans RNA) are **polymers**.
- **Nucleotides** are the **monomers** composing the nucleic acids.

### How nucleotides are created?

- There are 3 componants:
  - A **base**
  - A **sugar** (either ribose or deoxyribose)
  - A **phosphate** (after the sugar and base are bonded)
- The base bonded to the sugar is called **nucleoside**.
- The phosphate bonded to the nucleoside is called nucleotide.

![nucleotide creation](./img/03_Macromolecules/nucleotide_creation.png)

#### Bases

- In nature, there are 5 different bases of two different kinds:
  - Pyrimidines: **1 ring**: Cytosine, Thymine, Uracil
  - Purines: **2 rings**: Adenine, Guanine
- Thymine and Uracil look alike. The only difference is the $`H_3C`$ on the Thymine.

![bases](./img/03_Macromolecules/nucleotide_bases.png)

#### Sugars

- Two types:
  - Deoxyribose (for DNA) (one less oxygen hence "deo" prefix).
  - Ribose (for RNA)
- They are **5 carbon** rings (important to know!!):
  - **Carbon 1**: Base will bond here (via the hydroxyl group).
  - **Carbon 2**: If OH --> ribose. If H --> deoxyribose.
  - **Carbon 3**: Bonding point (hydroxyl group).
  - **Carbon 4**: The only one that is not important.
  - **Carbon 5**: Phosphate bonding point.

![sugar](./img/03_Macromolecules/sugar.png)

#### Phosphate

- Phosphate atoms bond on the 5th carbon.
- There will 3 phosphate bonded on the carbon (in a chain).
- When one phosphate is bonded to a nucleoside for example Adenosine it is called **Adenosine monophosphate** or **AMP**.
- When two phosphate is bonded to a nucleoside for example Adenosine it is called **Adenosine diphosphate** or **ADP**.
- When three phosphate is bonded to a nucleoside for example Adenosine it is called **Adenosine triphosphate** or **ATP**.
- In order to **chain nucleotides 3 phosphate are needed**.

![phosphate](./img/03_Macromolecules/atp.png)

### Chaining nucleotides to form nucleic acids

| Base                                         | Nucleotide - RNA (ribose) | Nucleotide - DNA (deoxyribose) |
|----------------------------------------------|---------------------------|--------------------------------|
| Adenine                                      | ATP                       | dATP                           |
| Guanine                                      | GTP                       | dGTP                           |
| Cytosine                                     | CTP                       | dCTP                           |
| Uracil (only in RNA) / Thymine (only in DNA) | UTP                       | dTTP                           |

- The **chaining of nucleotides** is done by bonding a nucleoside on carbon 5 called **5'** (with the phosphate) to the end of the chain to a carbon 3 called **3'** (with hydroxyl group).
- Chaining can only be done from 3' (end of chain) to 5' (of nucleotide that is added).
- Chaining nucleotides **releases 2 phosphate and one water molecule**.
- The linkage between nucleotides is called **phosphodiester linkage**.

![chaining nucleotide](./img/03_Macromolecules/chaining_nucleotides.png)

- The chaining forms a phosphate/carbon (P-C-P-C) backbone.
- The phosphate/carbon backbone has a **negative charge**.

### DNA/RNA

- The differences between the two:
  1. Uracil in RNA and Thymine in DNA
  2. On Carbon 2' there's an oxygen in RNA which isn't in DNA.
  3. RNA is more flexible than DNA due to having an hydroxyl group instead of just an H.
- They are read 3 nucleotides at a time from C 3' to C 5'.
- RNA in single stranded
- DNA in double stranded (helix).

|RNA|DNA|
|--|--|
|![RNA](./img/03_Macromolecules/rna.png)|![DNA](./img/03_Macromolecules/dna.png)|


### DNA

#### DNA strands

- The two strands of DNA are in **opposite directions**.
- With one strand on DNA the other can be deduced. This is called **reverse complement**.
- The two strands of DNA are bonded with **hydrogen bonds**.

#### DNA hydrogen bonds

- Hydrogen bonds are a lot weaker than covalent bonds thus it's easy to divide the DNA into 2 single strands.
- The sizes of the bonds are the same so the strands are parallel.
- The hydrogen bonds are always **purine-pyrimidine** or **pyrimidine-purine**:
  - **Adenosine - Thymine**
  - **Guanine - Cytosine**
- There are **2 hydrogen bonds** between **Adenosine and Thymine**.
- There are **3 hydrogen bonds** between **Guanine and Cytosine**.
- In hydrogen bonds there's the **donnor** (gives the H) and the **acceptor** (receives the H). That's the reason the bonds A-T and G-C can form only that way (see picture below).

![DNA hydrogen bonds](./img/03_Macromolecules/dna_hydrogen_bonds.png)

#### Anatomy of a DNA helix

- DNA is a **right handed helix**. 
- Right handed helix is called **B-DNA**. This is the most common and the only one important in this course.
- It takes around 10 bonds to do complete turn which is **3.4nm high** (the space between two bonds is 0.34nm).
- **Major groove** is the side with more space whereas **minor groove** is the side between the strands.
- Usually DNA is read from the **major groove** side but sometimes it is read from the **minor groove** side.

![DNA anatomy](./img/03_Macromolecules/anatomy_dna.png)

### RNA

- Some strands of RNA fold like proteins.
- First **loops**, called **stem loop** or **hairpin**, are created by parts of the strands which sole purpose is to make those loops: hydrogen bonds are formed in order to create the loop.
- The nucleotides on the loop itself are not bonded and have other roles.
- Other parts are too far from each other to make loops but still bonds. That's what make the RNA fold into a **3D shape**.
- The strand will always fold in the same manner.
- A **folded RNA** can act as a **catalist** (enzyme). It is called **ribozyme** and not enzyme.

![RNA folding](./img/03_Macromolecules/rna_fold.png)


### DNA Replication

- **Replication** is creating a new double helix from an existing one.
- DNA is first **expanded** - separate the strands to single strands - by **denaturation**.
- Then enzymes called **DNA polymerase** use each strand as **template** to synthesize the **reverse complement**. 
- This type of replication is called **semi-conservative**.

![DNA replication](./img/03_Macromolecules/dna_replication.png)

#### Origin of replication - ORI

- **Origin of replication - ORI** refers to the sequence where to start the replication.
- In animals there are more than one origins of replication.
- Those sequence are a chain of nucleotide **specifying** that they are an origin of replication.
- They create the **first expansion** within the double stranded DNA. This requires energy.

![ori](./img/03_Macromolecules/ori.png)

#### DNA Polymerase

##### How it looks
- The DNA polymerase looks like a right hand.
- The part of the DNA that's being synthesized is in the **"palm"** of the enzyme.

![DNA polymerase](./img/03_Macromolecules/dna_polymerase.png)

- The DNA polymerase **needs 3 things** in order to start the replication:
  - A **template - תבנית**: this is the single dna strands.
  - **Nucleotides**: provided by another entity.
  - A **primer - תכל**: RNA primer that help the DNA polymerase know where to start synthesizing (see below).

##### How it works
- The DNA polymerase don't know where to start synthesizing and needs a **primer** to point where to start replicating.
- The DNA polymerase continues to expand the DNA creating a **replication bubble**. On each side of the bubble there is a **replication fork**.
- There are DNA polymerase working on both forks.
- **The DNA polymerase start synthesizing after the primer and continues until it overwrite the next primer sequence**.
- Each DNA polymerase **reads a strand from 3' to 5' and creates the reverse complement** (matching strand) from 5' to 3' one nucleotide at a time.
- Because the 2 single strands of DNA have **opposite directions**, one of the strands is easy to synthesize (the one that actually goes from 3' to 5') while the other one is more tricky.
- The **3' to 5'** strand that is easy to synthesize is called the **leading strand**.
- The **5' to 3'** strand that is tricky to synthesize is called the **lagging strand**.
- Each strand is leading and lagging depending on which fork we look.
- **The leading strand requires only one primer at the ORI point**. It can continue to be synthesized along with the fork movement (expansion).
- **The lagging strand requires primers every time the fork opens up more** (see picture). This causes the synthesis to be done by fragments called **Okazaki fragments**.
- DNA polymerase check what they synthesize and correct any error found.

![DNA synthesis](./img/03_Macromolecules/synthesize_dna.jpg)

![fork](./img/03_Macromolecules/fork.png)

#### RNA Primer

- The **primer** pointing to the start of replication is an RNA strand.
- The **RNA primer** is synthesized by **RNA polymerase**.
- DNA and RNA are similar enough for the RNA to by synthesized on the DNA.
- The **RNA polymerase** unlike the DNA polymerase knows how to start from scratch.
- The **RNA primer** will be overwritten by the DNA polymerase.

### Other roles of nucleotides

- Besides building DNA and RNA, nucleotides have other roles:
  - **ATP**: energy storage.
  - **GTP**: energy source in protein synthesis.​
  - **cAMP**: essential to the action of hormones and transmission of information in the nervous system.
