# Cells

## Important notions

### Concentration gradient

- The concentration gradient is the distribution of particles accross space from high to low concentration.

![concentration gradient](./img/04_Cells/concentration_gradient.jpg)

### Diffusion

- Diffusion is the process of particles moving down their concentration gradient. 
- Diffusion happens since statistically there is a higher chance that particles will move from the area with higher concentration to the area with a lower concentration than the other way around.
- At some point the particles will continue to move but with **no net change**. This is called **equilibrium**.

![diffusion](./img/04_Cells/diffusion.png)

### Osmosis

- Occurs in the presence of a semipermeable membrane to a certain a solvent (usually water) but not to a certain solute, with on one side a higher concentration of that solute (e.g salt). 
- Because of the higher concentration of the solute on one side, the probability of the water moving from this side to the other is lower than the other way around.
- **Osmosis means that the solvent (water) will move from a lower concentration of solute to a higher concentration of solute.** 
- **Osmolarity** is the total concentration of all solutes in the solution. Low osmolarity means less solute in a solution and high osmolarity means that the solution has more solute particles.

![osmosis](./img/04_Cells/osmosis.png)

### Tonicity

- Tonicity is the ability of an extracellular solution to make water move into or out of a cell by osmosis.
- The tonicity of a solution is linked to its osmolarity (higher osmolarity will get more water out of the cell, lower osmolarity will get more water in the cell).
- **Hypertonic, hypotonic and isotonic** are used to compare the osmolarity of a cell to the osmolarity of the extracellular fluid around it:
  - **Hypertonic** is when there is a **higher concentration of solute in the solution** surrounding the cell than in the cell.
  - **Hypotonic** is when there is a **lower concentration of solute in the solution** surrounding the cell than in the cell.
  - **Isotonic** is when there is an **equal concentration of solute inside and outside** of the cell.
![hypo-hyper-isotonic](./img/04_Cells/tonicity.png) 


- A cell in an hypertonic solution, will cause the water to leave the cell and it will shrink.
- A cell in an hypotonic solution, will case the water to go into the cell and it will swell and even explode.
- A cell in an isotonic solution, will keep its concentration of water and will stay healthy.

![results of tonicity](./img/04_Cells/tonicity_results.png)

- For **plant cells**, an hypotonic solution is good because plant cells have a wall thus cannot explode.

### Electrochemical gradient

- In addition to the concentration gradient, there can also be an electrochemical gradient or difference in charge, across a plasma membrane since atoms and molecules can form ions and carry positive or negative electrical charges. 
- Living cells typically have what’s called a **membrane potential**, an electrical potential difference (voltage) across their cell membrane.
- The membrane potential of a typical cell is -40 to -80 millivolts, with the minus sign meaning that inside of the cell is more negative than the outside.

## Cell theory

1. All living things are composed of one or more cells.
2. The cell is the basic unit of life.
3. New cells arise from per-existing cells.

## Basic cell structure

- Most prokaryotes cells are around $`1\mu m`$.
- Most eukaryote cells are around $`10\mu m`$.
- With bigger cells, one thing to think about is that, **cells interact with their surroundings**, which means that the **surface area is responsible for the volume inside**. In other words, the **ratio of volume to surface area matters**. So the cells can not be too big either.

 > _Note: Cell size_  
 > Cells need to stay rather small. The surface is responsible to transfer in and out of the cell things like nutrients or waste.  
 > As the cell gets bigger the volume gets bigger a lot more than the surface, meaning that as the cell gets bigger the surface gets responsible for more and more volume per unit (surface-area-to-volume-ratio).  
 > Cells cannot get too big in order to stay viable.  

### Similarity between prokaryotes and eukaryotes

- Conduct glycolysis.
- Replicate DNA semiconservatively.
- Have DNA that encodes peptides.
- Produce peptides by transcription and translation using the same genetic code.
- Have plasma membrane and ribosomes.

### Comparasion between prokaryotes and eukaryotes

![comparasion](./img/04_Cells/comparasion.png)

## Interaction with the surroundings

- In order to transfer particles inside or outside of the cells, the particles need to go through the **plasma membrane**.
- There are 2 ways:
  - **Active transport** which requires energy.
  - **Passive transport (diffusion)** which doesn't require energy. 

## Prokaryotes

- There are 2 families:
  - **Bacterias**
  - **Archaea**

### Structure

- Most of prokaryotes are **unicellular**.
- **Prokaryotes cells are not organised**: everything is inside the cell with no compartments (no membrane-enclosed organelles).
- The **cytoplasm** consist of **cytosol** fills up the cells and all the particles move in it.
- The DNA is in a defined area called **nucleoid** (there is no membrane around).
- They don't have a cytoskeleton (although it is being research that they do have one but not as developped as the eukaryotes one).
- The **cytoskeleton**, a network of fibers that supports the cell and gives it shape, is also part of the cytoplasm and helps to organize cellular components.
- Cell wall made out of **peptydoglycan** (polymer composed of carbohydrates and proteins).
- Many bacterias have an outer most layer called the **capsule** (made of polysaccharide) which helps attach to surfaces.
   
   ![prokaryote](./img/04_Cells/prokaryote_cell.png)

- There are 3 common shapes among Bacteria:
  - **Sphere** - **coccus** (pl. cocci) occur singly, or in plates, blocks or clusters.
  - **Rod** - **bacillus** (pl. bacilli).
  - **Helical**.
  
    ![bacteria shapes](./img/04_Cells/bacteria_shapes.png)

- Some bacterias have specialized structures like:
  - **Flagella**: acts as a rotary motors to help the bacteria move.
  - **Fimbriae**: hair like structures that help attach to host cells and other surfaces.
  - **Pili**: there are several types. Some are used to transfer DNA to other bacterias, some help move and more.

  ![flagella pili fimbriae](./img/04_Cells/flagella_pili.png)
  
### Life

- They can live in all types of habitats even harsh ones.
- Prokaryotes **live alone** but there are often found in **chains** or **clusters** and can be of different types even small eukaryotes.
- Chains of prokaryotes are called **filament**.
- Even in chains or cluster they are **independant**.
- They can **communicate** by sending **chemical signals**.
- Microbial communities form **biofilms** when the cells excrete when they are in contact with a solid surface.
- **Biofilms** is a gel-like polysaccharide that protect the microbial communities from being moved/killed by external factors even chemical factors.

  ![biofilm](./img/04_Cells/biofilm.png)
  
### Gram stain method

- The **gram stain** method helps identifying the complexity of the cell wall structure.
- The method uses 2 stains: 
    - **Violet dye** 
    - **Red dye** 
- **gram positive**:
  - Retain the violet dye.
  - Cell wall made of a thick layer of peptydoglycan and underneath a plasma membrane.
- **gram negative**:
  - Retain the red dye.
  - Cell wall made of a thin layer of peptydoglycan between two plasma membranes.
- **Steps**:
  - Associates with peptidoglycan so all bacterias are going to stain purple.
  - After using a decolorizing solution, the violet stain will strongly remain in bacterias that have a much thicker peptidoglycan layer (gram-positive cells).
  - After using the decolorizing solution, the red dye is used in order to identify easily the gram-negative cells.

 ![gram stain method](./img/04_Cells/gram_stain.png)

### Cyanobacteria

- **Cyanobacteria** are called **blue algae** which actually are small prokaryotes.
- They are the origin of the **photosynthesis**.
- They know have to **fix atmospheric nitrogen and carbon**.
- Colonies of cyanobacterias can be in the form of: flat sheets, filaments or spherical balls.
- Some colonies have cells which specialize in different roles:
  - **Vegetative cells**: do the photosynthesis.
  - **Spores**: cells specializing in reproduction.
  - **Heterocysts**: cells specializing in fixing the nitrogen.

![bacterias energy](./img/04_Cells/bacterias_energy.png)


### Bacterias and animals

- A lot of prokaryotes live in other organisms:
  - Some plants use them to fix nitrogen
  - In animals they are useful in the digestive tract.
- **Microbiomes** are bacterial communities that live in the human body.

### Phylogeny

The study of the evolutionary history and relationships among individuals or groups of organisms.

#### rRNA

- **rRNA- ribosomal RNA** is used to compare and organize organisms.
- All organisms have rRNA.
- Has the same role in translation in all organisms.

#### Lateral gene transfer

- Occurs when genes from one species become incorporated into the genome of another species.​
- Transfer by plasmids or virus.
- Make more difficult to catalog species.

![lateral gene transfer](./img/04_Cells/lateral_gene_transfer.png)


## Eukaryotes

- **Endosymbiosis theory**: eukaryotic cells that evolved by engulfing prokaryotes (like mitochondria and chloroplasts).
- **Eukaryotic** cells are around $`10\mu m`$ which is 10 times bigger that prokaryotic cells.
- They have **membrane-bound organelles** that allow them to have **compartments with specialized functions** that require certain pH or to create certain reactions that could be toxic for the rest of the cell:
  - **Lysosomes** are like recycling centers that dispose of waste and need an acidic pH.
  - **Peroxisomes** carry out reaction that could be toxic for the rest of the celled.

### Cytoplasm

- Different in prokaryotes and eukaryotes.
- In prokaryotes, the cytoplasm is everything inside the plasma membrane.
- In eukaryotes, the cytoplasm is everything inside the plasma membrane except the nucleus.
- The main component of the cytoplasm is **cytosol**. A gel-like solution.
- In eukaryotes, the **organelles** which are part of the cytoplasm are suspended in the cytosol.
- The **cytoskeleton**, a network of fibers that supports the cell and gives it shape, is also part of the cytoplasm and helps to organize cellular components.

### Animal cells

| | |
|--|--|
|![animal cell](./img/04_Cells/animal_cell1.png)|![animal cell](./img/04_Cells/animal_cell2.png)|


### Plant cells

| | |
|--|--|
|![plant cell](./img/04_Cells/plant_cell1.png)|![plant cell](./img/04_Cells/plant_cell2.png)|

- Unlike animal cells, plant cells have
  - **Chloroplasts** responsible for the photosynthesis.
  - **Vacuole** is like water baloon which apply pressure inside the cell in order to keep the form of the cell.
  - **Cell wall** made of cellulose which make the cells hard thus allowing the plant to stand.


### Membranes

![plasma membrane](./img/04_Cells/plasma_membrane.png)

- The plasma membrane controls the passage of various molecules in and out of the cell via its hydrophilic/hydrophobic and via proteins.

#### Phospholipid bilayer

- Made of lipids called **phospholipids** which contains phosphate heads.
- They are double layered and arrenged themselves so that the heads are on the outside of the double layer and the tails touch.
- The phospholipids have **hydrophilic** heads which are attracted to polar substance (like water) thus they face outward.
- They also have **hydrophobic** tails which face inward, thus in the bilayer they face each other.
- The tails are made of **saturated** and **unsaturated** fatty acids. 
- The saturated fatty acids are straight lines, and **at low temperatures will pack together** while unsaturated fatty acids wont. Thus a cell with more unsaturated fatty acids will stay fluid at lower temperatures.
- In order to prevent to much change in fluidity due to temperature, **cholesterol** (lipid) is present in the membrane as well.
- **Cholesterol** made up to 25% of the membrane and provides integrity to the membrane by regulating how fluid it is, such as by raising the melting point at high temperatures and preventing the phospholipids from sticking together at lower temperatures. It also gives the membrane structural integrity to ensure it doesn't become too fluid and allow unwanted molecules to permeate.
- This configuration will make the passing of some molecules easier than others. _For example, polar substances will tend to not pass since they're attracted to the polar heads._

 ![phospholipids](./img/04_Cells/phospholipids.png)

#### Proteins

- For each organelle/cell their membrane has different proteins with different role according to the organelle/cell job.
- The plasma membrane **contains proteins** which helps communication between outside and inside of the cell:
  - **Integral membrane proteins**: go through both ends of the membrane.
  - **Peripheral membrane proteins**: go through only one end of the membrane (the other end of the protein being inside the membrane). 
- Most of the proteins are **glycoproteins**: protein with a sugar. They have **hydrophilic** parts outside the membrane and **hydrophobic** parts inside the membrane.
- The proteins are not "fixed" in the membrane, they move around.
- Small non-polar molecules can pass easily through the membrane, but bigger and more polar ones need to go through proteins channels.

#### Carbohydrates
- Carbohydrates are found at the surface of the membrane, attached to either lipids (forming glycolipids) or proteins (forming glycoproteins).
- **Glycolipids** and **glycoproteins** are used by the cells to identify which cells are from the organism itself and which cells are from outside. This is important in order to protect the right cells and not attack them.

#### Microvilli

- Some cells needs to have a bigger surface in order to pass more waste or nutrients. To acheive that some cells have **microvilli** which is the membrane that is folded and looks like fingers. This form increases the surface area withou increasing too much the volume area. There are more proteins on the "fingertips" since those are the parts that do the work.
 
 ![microvilli](./img/04_Cells/microvilli.jpg)


#### Endocytosis

- Endocytosis is a way for the cell to "eat" or englobe larger molecules or larger quantities of molecules.
- The way is works is first, the plasma membrane of the cell invaginates (folds inward), forming a pocket around the target particle or particles. The pocket then pinches off with the help of specialized proteins, leaving the particle trapped in in a newly created **vesicle** or vacuole inside the cell.
- On the vesicle membrane, there are proteins that allow to identify the content of the vesicle.

**Types of endocytosis:**
- **Phagocytosis**: (means literally, “cell eating”) is a form of endocytosis in which large particles, such as cells or cellular debris, are transported into the cell.
- **Pinocytosis**: (means literally, “cell drinking”) is a form of endocytosis in which a cell takes in small amounts of extracellular fluid.
- **Receptor-mediated endocytosis**: form of endocytosis in which receptor proteins on the cell surface are used to capture a specific target molecule.

 ![endocytosis](./img/04_Cells/endocytosis.png)

#### Exocytosis

- Exocytosis is when materials are transported from the inside to the outside of the cell in membrane-bound vesicles that fuse with the plasma membrane. _For example, after the Golgi apparatus finishes with proteins, the proteins in vesicles exit the cell via exocytosis_.


![exocytosis](./img/04_Cells/exocytosis.jpg)
