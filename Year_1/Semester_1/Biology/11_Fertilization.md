# Fertilization

- Fertilization in most organisms is done externally.
- Two spermatozoids cannot fertilize the same egg.
- After penetration from one spermatozoid, the egg change its charge from negative to positive thus pushing away other spermatozoids.
- Once fertilization has occured, the cell is called **zygote**.

## Spermatozoid

  ![sperm](./img/11_Fertilization/sperm.png)

- Really small and compact.
- Composed of:
  - **Tail/flagella** built in 9+2 microtubule form.
  - **Midpiece** conatains a lot of mitonchondria.
  - **Centriol**
  - **Head**: 
    - Nucleus with the DNA
    - Acrosome allows the sperm to penetrate the egg and delivers the DNA by **exocytosis**.
- When fertilizing the egg **only the DNA** enter the egg, not the rest of the body.

## Egg

  ![egg](./img/11_Fertilization/egg.png)
  
- A lot bigger than spermatozoids.
- **All mitonchondria and organelles are passed from the mother**.

## Cleavage - חלוקה התלמה

- **Cleavage**: a rapid series of cell division, but no cell growth.​
- **Morula**: the embryo as a solid ball of small cells. ​
- **Blastocoel**: a central fluid-filled cavity that forms in the ball.​
- The embryo becomes a **blastula** and its cells are called blastomeres.​
- **Blastulation**: from morula to blastula.

## Gastrulation

- The blastula is transformed into an embryo in gastrulation, through movement of cells.​
- During gastrulation, a tube is formed within the cell which will become the **digestive system**.

 ![gastrulation](./img/11_Fertilization/gastrulation.png)


- During gastrulation **3 germ layers** form:​
  - The **endoderm** is the innermost layer and becomes the lining of the digestive and respiratory tracts, pancreas, and liver. ​
  - The **ectoderm** is the outer germ layer and becomes the nervous system, the eyes and ears, and the skin. ​
  - The **mesoderm** is the middle layer and contains cells that migrate between the other layers—it forms organs, blood vessels, muscle, and bones.​

## Organogenesis

- Development of organs

### Neurulation

- The mesoderm closest to the midline is called the chordamesoderm.
- It produces a rod of connective tissue called the notochord (מיתר), which provides support for the embryo. ​
- The chordamesoderm has organizer functions and induces the ectoderm to form the nervous system.​

- Steps in neurulation:​
  - The ectoderm over the notochord thickens and forms the neural plate.​
  - Edges of the neural plate fold and a deep groove forms.​
  - The folds fuse, forming the neural tube and a layer of ectoderm.​

- The anterior (front) end of the neural tube becomes the brain, the rest becomes the spinal cord.​

||||
|--|--|--|
|![neurulation1](./img/11_Fertilization/neurulation1.png)|![neurulation2](./img/11_Fertilization/neurulation2.png)|![neurulation3](./img/11_Fertilization/neurulation3.png)|

## Development of the body - Segmentation

- **Somites** are separate, segmented blocks of cells on either side of the neural tube. ​
- Muscle, cartilage, bone, and lower layer of the skin form from somites.​
- Neural crest cells are guided by somites to develop into peripheral nerves and other structures.​

 ![segmentation](./img/11_Fertilization/segmentation.png)
