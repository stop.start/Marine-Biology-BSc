# Competition

## Relationships

- Relationships (יחסי גומלין) are the base of any ecological society composed of several populations that interact either directly or indirectly.
- Mutualism (אדדיות) is a relationship where (everyone wins).

### Types of relationships and results (win/lose/none)

| Relationship | Definition/Example                                                          | Win[+]/Lose[-]/None[0] |
|--------------|-----------------------------------------------------------------------------|:----------------------:|
| Mutualism    | Everyone gets something out of it                                           | +,+                    |
| Predation    | One eats the other                                                          | +,-                    |
| Commensalism | Fishes that swin under sharks are protected, sharks don't lose anything     | +,0                    |
| Competition  | Want the same resources. The one that "wins" is the one that loses the less | -,-                    |
| Amemsalism   | Penicillium secretes penicillin which kills bacteria                        | -,0                    |
| Neutrality   | Two species with no interactions that live alonside                         | 0,0                    |

## Competition

- Competition is linked to the **carrying capacity** of the area: if there's enough of the resource 2 species compete on for both species then there's no competition.
- 2 types of competition: **intraspecific** (תוך מינית) and **interspecific** (בין מינית)
- **Intraspecific**: compete within the same species on resources, females, habitats. There 2 types:
  - **תחרות סבילה**: when 2 individuals compete for the same resource (without directly interacting with each other).
  - **תחרות אקטיבית - הפרעה אדדית**: reproduction, compete for the same female by physically trying to push back one another.
- **Interspecific**: compete between species on resources. There are 6 types:
  - **Consumptive - תחרות מכלה**: The more one competitor requires more of the limited resource it leaves less for the other competitor.
  - **Preemtpive - תחרות מקדימה**: One competitor takes the area/zone before the other competitor thus the other cannot use it.
  - **Overgroth - תחרות מאפילה**: One competitor grows on or next to another competitor and takes from them resources.
  - **תחרות כימית - אללופטיה**: One competitor disperse chemical matters that harmful or prevent growth of the other.
  - **Territorial - טריטוריאליים**: One competitor protect its area by different means.
  - **Encounter - תחרות מפגש**: Competitors meet and it results in energy/food loss or they're hurt.
- When talking about carrying capacity, the maximum potential of an area for a species is when there's no competition (when N is low):

```math
\frac{dN}{dt} = rN(\frac{K-N}{K}) \longrightarrow \text{the lower the N the higher the potential capacity}
```
- When there's competition the above equation needs adjusting because competition reduces even more the carrying capacity ($`K_1 - \text{something} - N_1`$).
- Usually it is another competitor: ($`\frac{K_1-N_1-N_2}{K_1}`$).

 ![competition examples](./img/09_Competition/competitions_examples.png)
 
- By the Lotka-Voltera model:
  - **N**: size of the population
  - **K**: carrying capacity of the area for the species
  - **r**: growth coefficient of the population
  - **$`\bold{\alpha{1,2}}`$**: competition coefficient (2 affects 1 - when written 2,1 then 1 affects 2). It is the ratio between 2 species (if there's 1 individual of pop1 for 9 of pop2 then $`\alpha{1,2}={1}{9}`$ and $`\alpha{2,1}=9`$).

```math
\begin{aligned}
&\frac{dN_1}{dt} = r_1N_1 \times (\frac{K_1-N_1-\alpha_{1,2}N_2}{K_1}) \\
&\frac{dN_2}{dt} = r_2N_2 \times (\frac{K_2-N_2-\alpha_{2,1}N_1}{K_2})
\end{aligned}
```

### 0 Isocline - איזוקלינת ה- 0

- When a population growth is 0:

```math
\begin{aligned}
&\frac{dN_1}{dt} = r_1N_1(\frac{K_1-N_1-\alpha{1,2}N_2}{K_1})\\
&\frac{dN_1}{dt} = 0 \Longrightarrow K_1-N_1-\alpha{1,2}N_2 = 0 \\
&\bold{N_1 = K_1 - \alpha{1,2}N_2}
\end{aligned}
```

 ![isocline](./img/09_Competition/isocline.png)
 
- When taking into account the competition between the species, the graphs look a bit different.

|||||
|--|--|--|--|
|![pop1](./img/09_Competition/pop1.png)|![pop2](./img/09_Competition/pop2.png)|![non stable](./img/09_Competition/non_stable.png)|![equilibrium](./img/09_Competition/equilibrium.png)|

- Those are just models. In real life, there are other factors to take into account like abiotic conditions, fire, diseases, extreme seasons, etc.

