# Limiting factors and their effect on Primary Production

## What is a limiting factor?

- A **limiting factor - גורם מגביל** is a factor which quantity or availabilty is limited thus sets a process flow in the system. In other words **bottleneck**.
- When there are multiple limiting factors, the factors which is the most limited sets the rate. When removing this factor, the next most limited factor sets the rate.
- **Threshold response - תגובת סף** is **adding the last bit that removes the limited factor** thus making the process flow independant to quantity.
- **Dose response - תגובת תלוית מינון/ריכוז** is **adding to the limited factor will improve the flow**. The more is added the better the flow.

### Steps of limiting factors

- Lack of limited factor - none or not enough of it.
- Removing the limitation - there is enough of the factor. A bit more of it won't affect the flow.
- Adding more of the (limiting) factor might degrade (עיכוב) the system.
- Adding even more of the factor can destroy completely the system's element.

![effect limiting factors](./img/03_Limiting_Factors/effect_limiting_factors.png)

### The limiting factors of primary production

There are 3 limiting factor of the primary production:
- **Light**
- **Geographic location**
- **Fertilizers**

## Light

- Light comes as a **spectrum of colors** (visible and non visible) which have each a **different wavelength**.
- Each colors is absorbed in the water in different depth: first red, last blue.
 
![effect limiting factors](./img/03_Limiting_Factors/colors_water.png)

- Light is an **abiotic** factor.
- It is import to the primary producers in order to perform **photosynthesis**.
- **On land light is usually not a limiting factor but in water it is**.
- Plants that have limited light **adapt** to those condition and might not survive under regular conditions with a lot of light.

### What affects the quantity of light?

- **On land:**
  - Geographic location
  - Clouds
  - Shadows
- **In the water:**
  - Inorganic opacity (ex: from sediment)
  - Biotic opacity (ex: algae, unicellular organisms)
  - Colors matters (from plants)
  
### Light in water

- **Photic Zone** is the zone at the surface where the light can penetrate.
- **Aphotic Zone** is the zone below the surface where the light cannot penetrate.
- The photic zone border is the depth where the **light quantity is 1% of the light at the surface**.
- **Photic degradation - עיכוב פוטי**: in the graph below we can see that the primary production reaches 100% at a certain depth (not the surface). That's because at the surface there's too much light (degrading factor - גורם מעכב). 

![effect limiting factors](./img/03_Limiting_Factors/light_water.png)

- Photosynthesis in water is **quantity dependant - תלוי מינון**.

### Effect of geographic location on light quantity

#### Light in the equator vs poles

- **Geographic location** affects the light quantity. Comparing the equator and the poles shows that well.
- There are more light radiations in the equator than in the poles: 
  - More direct thus less time in the atmosphere which absorbs part of radiations.
  - Rays of light come at $`90\degree`$ angle thus covers less surface and thus more concentrated.

![radiations on earth](./img/03_Limiting_Factors/light_earth.png)

- In the equator there is 12 hours of light and 12 hours of dark.
- On the **equinox** days (21.03, 21.09) there is 12 hours of light and 12 hours of dark all around the world.
- On the **solstice** on 21.06 in the norther hemisphere there is the most light of the year (24 hours in the north pole) and the less light in the south hemisphere (24 hours of no light in the south pole). On the solstice on the 21.12 it is the other way around.

#### How the geographic location affect the primary production?

![primary production equator vs n. pole](./img/03_Limiting_Factors/pp_equator_pole.png)

- In the equator the primary production is pretty much the same all year long.
- In the equation the concentration of radiation is the highest causing some photic degradation (עיכוב פוטי).
- In June (summer solstice in N. hemisphere) there is more primary production than in the equator although the light is less intense than in the equator. That's because there is light 24/7.
- In September (equinox month) there is 12 hours of light but it's too weak to produce primary production.
- In December (winter solstice in N.hemisphere) there is no primary production from $`60\degree`$ latitude (Moscow) and above.

> Interesting fact:  
> Plants need light and water in order to perform photosynthesis.  
> In winter there is not enough light to perform photosynthesis but if the process is still "up" then there is a danger of dehydration.  
> In order to prevent the dehydration the plants/trees shutdown the parts that do the photosynthesis (leaves).  
> Some plants/trees have really small surface in order to not lose water and dehydrate in cold areas.  

- When comparing the primary production in different habitats per g/m2/yr the most productive are the coral reefs and the least productive are the oceans.
- Because the oceans are so big (most of the Earth) they are the biggest primary producers.

## Nutrients and Fertilizers

- Most of nutrients recycle naturally.
- **Limiting nutrients** are mainly **nitrogen (N), phosphorus (P) and trace elements**.
- In **aquatic ecological systems**, in addition to the above, **carbon (C)** can also be a limiting factor.
- Atmospheric nitrogen isn't available to the primary producers.
- In **aquatic ecolgogical systems**, phosphor is usually the first limiting factor, then nitrogen and last carbon.

### Nitrogen

- **Nitrogen is used to build amino acid and proteins**.
- Although nitrogen is the most common gas ($`N_2`$) in the atmosphere, the organisms cannot use it.
- Available forms of nitrogen: ammonium [$`NH_{4}^{+}`$], nitrite [$`NO_2`$] and nitrate [$`NO_3`$].
- From ammonium we can get nitrite and nitrate when oxidized.

#### Nitrogen cycle

1. **Nitrogen fixation**: process of transforming atmospheric nitrogen into usable forms (see above). 
- There are 2 ways to do that:
  - **Certain bacterias and blue algae (cyanobacteria)** know how to take atmospheric nitrogen [$`N_2`$] and transform it into ammonium [$`NH_{4}^{+}`$].
  - Via **lightning**: creating amomnia [$`NH_3`$] from nitrogen and hydrogen. Less effective than from bacterias and algae.
2. Organisms use the nitrogen to build proteins and amino acids.
3. Organisms (especially fishes) excrete (in urine) ammonia. Bigger organisms have a reduced concentration of ammonia in urine because it is very toxic.
   - Urine that stays in water releases ammonia.
4. **Nitrification**: certain bacterias transform ammonia into nitrite (which will then be oxidized into nitrate).
5. **Denitrification** is the process of transforming nitrate into atmospheric nitrogen.

![nitrogen cycle](./img/03_Limiting_Factors/nitrogen_cycle.png)

### Phosphorus

- **Phosphorus is used build nucleic acid (חומצות גרעיו) ATP and ADP**.
- Some forms are usable by the primary producers and some are not.
- **Organic phosphorus**: Usable form of phosphorus are kinds of phosphoric acid [$`H_3PO_4`$].
- Phosphoric acid disolve in water and at some point becomes phosphate [$`PO_4^{3-}`$].
- **Inorganic phosphorus**: The other forms which don't dissolve in water are not usable and sink into the ground.
- Those other forms are phosphoric acid that bonded with other elements (ex: calcium) to form insoluble compound like chalk (גיר) and iron.
- Plants get the phosphorus either by the **water they drink** or with **fertilizers**.

#### Phosphorus cycle

- **Usable phosphorus cycle**:
  1. Phosphoric acid dissolve in water and becomes phosphate.
  2. Plants absorb and use it to build ATP, nucleic acid.
  3. When the plants die it returns to the evironment and is either:
     - Stuck in sediment.
     - bacterias return is to its orginal form.
- **Unusable: Flocculation (הפתתה)**:
  1. Particules attach with phosphoric acid molecules to form an inorganic compound.
  2. As a compound they cannot dissolve in water thus sinking to the ground in sediment.
  3. With the right conditions it can actually be dissolve (המסה) thus goin back to its original form.
- **Unusable: chemical compounds**:
  1. Some chemicals like detergents, fertilizers are released.
  2. Inorganic and sink into sediment.
- **Fumigation compounds with phosphorus**:
  1. Some chemicals like detergents, fertilizers are released.
  2. Inorganic and sink into sediment.

_Note: fumigation compounds and war biological compounds are the same just with certain element in sdifferent quantities._

![phosphorus cycle](./img/03_Limiting_Factors/phosphorus_cycle.jpg)

> Nitrogen and Phosphorus concentration in oceans:  
>  
> - In the photic zone the concentration of nitrogen and and phosphorus is low.  
> - In aphotic zone the concentration is high.  
> - We can deduce that the phytoplankton (primary producers) in the photic zone uses the nitrogen and phosphorus.  
> _ In the photic zone nitrogen and phosphorus are limiting factors (because it used) but it isn't in the aphotic zone (there's plenty and no one use them).  

![nitrogen and phosphorus concentration](./img/03_Limiting_Factors/n_p_concentrations.png)

### Carbon

1. **Inorganic carbon fixation** by photosynthesis and creation of organic carbon (glucose, cellulose).
2. Aerobic break down of organic carbon via **respiration** releasing inorganic carbon.
3. After plants die, certain specialized **decomposers** break down the organic carbon (glucose, cellulose) with the help of oxygen into $`H_2O`$ and $`CO_2`$.
4. Organic carbon that doesn't go through (if oxygen isn't available) aerobic break down can become fossil fuel (methane).

### Trace elements (יסודות קורט)
 
- Those are magnesium, potassium, zinc and copper.
- All of them (for ex. copper) have a **threshold response (תגובת סף)** unike nitrogen, phosphorus and carbon which have a **dose response (תגובת תלוית מינון)**.
- Iron is not really a trace element (it is more needed) but also not a main nutrients (not that much needed). It has a **Sigmoidal** dose response.

![copper vs the rest](./img/03_Limiting_Factors/copper.png)


### How limiting nutrients affect the ecology

- When fertilizing (once) a lake with no primary production, the phytoplankton biomass increases more and more over the years (except when the lake freezes in winter).
- Virus or extreme weather on a specific year can affect the primary production.
- Steps:
  - The more algae there are, the more nutrients they use thus becoming a limiting factor. 
  - Aglae start to die because they don't get enough nutrients. 
  - Dead algae release nutrients. 
  - Then the ones that haven't died have more than enough to reboost a bit the primary production unitl winter.

### Summary

- Phosphorus (זרחן) is the main limiting nutrients but when there's enough nitrogen becomes the main limiting factor. That's why the **N/P ratio** is an important index in ecology.
- **Redfield ratio**: in marine ecosystems, the ratio phosphorus/nitrogen is 1 unit of phosphorus for 17 units of nitrogen (and for 107 units of carbon)
- When there's not enough nitrogen, blue algae develop a lot more since they take the nitrogen from the atmosphere. This causes the water to not be clear.
- When there's enough nitrogen, blue algae disappear from the ecosystem. The water is clearer.
- In the middle of the oceans there is less primary production (less nutrients from the shores).
