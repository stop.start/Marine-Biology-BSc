# Mortality and Survival

## Mortality

- **Mortality** is one of the **4 factors** affecting the size of a population (natality, mortality, immigration, emigration).
- The basic model for population growth includes the mortality:
  - b is the natality rate
  - d is the death rate

```math
r - b-d
```

This allow to calculate:  

- N: size of the population.
- r: growth coefficient.
- t: time.

```math
\frac{dN}{dt} = rN = (b-d)N
```

- In all sort of fish species living in groups, espcially those with separate generations, all the group is the same age - **cohort**.
- When the group is the same age, it is easy to observe the diminution (התמעטות) of the population with the age growing.
- When looking at the **mortality rate** and the **natality rate** in those populations, the point where they meet is **k - carrying capacity - כושר הנשיאה**.

 ![k](./img/06_Mortality_and_Survival/k.png)

## Survival

- **Survival** is another way to look at the diminution of a population.
- Instead of looking at how many individuals die with age we look at how many survive.
- **Types of survival models**:
  - Some species have a fast mortality in young individuals and once the surviving individuals reach a certain age the mortality rate decreases.
  - Other species have a steady mortality rate.
  - Other species have a fast mortality rate in old individuals.

   ![survival models](./img/06_Mortality_and_Survival/survival_models.png)

- From the abobe, the **3 mortality models**:

   ![mortality models](./img/06_Mortality_and_Survival/mortality_models.png)
   

## Effect of Density on the Population Growth

- There are 3 non-exclusive reasons for population regulation (וויסות):
  - **Biotic** explanation - **density dependent**: sickness, competition, death by predators.
  - **Climate** explanation - **density dependent**: weather and other abiotic factors.
  - **Self regulation** - **density independent**: behavior, physiologic factors.
- **The higher the density, the lower the natality**.
- **The higher the density, the higher the mortality**.
- The principe of **density dependent mortality** is the base of the models for **sustainable fishing**.
- **MSY** - Maximum Sustainable Yield: the size of a natural population at which it produces a maximum rate of increase, typically at half the carrying capacity.
- Animals populations which are not density dependent will have the same natality and mortality rates no matter how dense the population is.
- Populations of animals that eat together and are density dependent:
  - Will have less to eat the more they are thus the natality will drop.
  - At first they will die less from predators (being more is being stronger) but with too many individuals the competition will be high thus increasing the mortality.
  
   ![density dependent](./img/06_Mortality_and_Survival/density_dependent.png)

## Reproduction strategies

- Each individual in populations has to **allocate resources** for every part of life including for strenghtening its body and for reproduction.
- The part of resources allocated for reproduction is called **reproductive allocation/effort - מאמץ הרבייה**
- Reproductive allocation can be:
  - Being fertile younger rather than older.
  - Reproduce once (for ex. salmons) instead of multiple times.
  - Smaller clutch size (number of eggs laid). Species without territories the clutch size is smaller.
- The **ideal organism** would have a high survival rate, reproduce just after being born, big clutch size, offsprings that are protected and taken care of by parents and with a lot of reproduction cycles. This does not exist and every organisms have to compromise somewhere.

### 2 main reproduction strategies

#### r Strategy

- A lot of offsprings
- Sexually mature young
- Big clutch size
- Not a lot of reproduction cycles
- Not a lot of investment in bringing the offsprings to sexual maturity: short gestation period, minimal parental care.


- Intensive population growth.
- Populations that are more oportunistic thus lack regulation.
- Habitats are not fixed thus high uncertainty.
- Species that don't specialized.

#### K Strategy

- Not a lot of offsprings
- Late sexual maturity
- Small clutch size
- Lots of reproduction cycles
- Lot of investment in bringing the offsprings to sexual maturity: long gestation period and parental care intensive.


- Limited population growth.
- Density dependent.
- Fix habitat and high certainty.
- Species that specialized (for example eat very specific food).

## Ages structure in the population

- Age distribution in a population can be indicatif of the population state.

| Percentage of youngs | Population state            | $`R_0`$ coefficient                      |
|----------------------|-----------------------------|------------------------------------------|
| 75% (a lot)          | healthy & developping       | $`R_0 >> 1`$ (high)                      |
| 65%                  | healthy                     | $`R_0 > 1`$ (normal)                     |
| 50%                  | problematic and growing old | $`R_0 = 1`$ (low reproduction potential) |
| 25% (low)            | old population              | $`R_0 < 1`$  (close to extinction)       |
