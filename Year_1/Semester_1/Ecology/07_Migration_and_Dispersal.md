# Migration and Distribution

- **Population - אוכלוסיה**: Group of organisms from the same species, living in a defined geographic area at a defined time.
- **Population growth**:
  - **r**: population growth constant
  - **b**: births (natality)
  - **i**: immigration
  - **d**: deaths (mortality)
  - **e**: emmigration
  
```math
r = (b+i) - (d+e)
```
- **Population growth rate**:
  - **r**: population growth constant
  - **b**: births (natality)
  - **i**: immigration
  - **d**: deaths (mortality)
  - **e**: emmigration
  - **N**: population size
  
```math
\frac{dN}{dt} = rN = ((b+i)(d+e)) \cdot N
```

## Immigration/Emmigration - הגירה

- **Immigration/Emigration - הגירה**: transfer of individuals from one population to another or to a place with no previous population.
- Source population - אוכלוסית מקור: births > deaths & immigrants < emigrants
- Sink population - אוכלוסית מבלע: births < deaths & immigrants > emigrants
- Critical factor for the size of a population: some populations don't reproduce a lot and immigration allows for them to grow.
- Factors defining the geographic domain of a species:
  - Natural barriers like water streams, cliffs, temperature, areas with little food, etc.
  - Relationships with other species and the competition for the same resources. 
- Despite the natural barriers, some individuals still manage to emigrate when the conditions change (change of seasons, human interaction, global warming).
- Individuals usually emigrate to close areas.
- Immigration allows to increase the living domain of a species.

### Human interaction

- Some species are introduced in a new territory by humans either knowingly or not.
- Those species can take control of their new territory and are called **invaders - פולשים**.
- This can cause some local species to disappear and others to flourish (if competitors or predators disappeared).

## Migration - נדידה

- **Migration - נדידה**: mass movement of an entire population in cycles at more or less specific times between more or less the same places.
- 3 types of migrations:
  - **Cyclic and multiple times**
  - **Cyclic once**
  - **One way change**
  
### Plankton: daily cycle

- **Plankton**: Organisms that can't swim easily against current but **swim easily vertically**.
- Plankton can be catagorised is several ways:
  - Type of organism:
    - Phytoplankton: algae and such
    - Zooplankton: animals
  - Size:
  - Microplankton
  - Mesoplankton
  - Macroplanton (ex: jellyfish)
- Some species are plankton all their life and other just during the larvae phase.
- Zooplankton during the day swim deeper and during the night swim closer to the surface. This is because during the day a lot of bigger species swim close to the surface to eat algae thus it is dangerous for them.

### Copepodite: seasonal cycle

- In the winter the adults are found deep in the sea and lay eggs.
- During the spring the eggs float upward and release larvae which swim close to the surface.
- During the summer the more developped larvae swim deeper in the sea.

## Dispersal - הפצה

- **Dispersal - הפצה**: active or passive movement of organisms (plant seeds).
- Good for:
  - Mixing the genes.
  - Less inbreeding.
  - If the carrying capacity of the habitat (בית גידול) is at maximum.
  - Better habitat.
  - Less competition for food.
- **3 hypothesis of a successful dispersal**:
  - **The escape hypothesis - היפותזת ההימלטות**: prevent mortality from density, predators, diseases.
  - **The settlements hypothesis - היפותזת ההתנחלות**: take advantage of new populating opportunities when habitats change.
  - **The guided distribution hypothesis - היפותזת ההפצה המונחית**: possibility to get to a micro habitat.
- **4 dispersal strategies**:
  - **Self dispersal**: example: exploding seed pods.
  - **Dispersal from the wind**: example: seeds of trees.
  - **Dispersal from water**: float and dragged by the current.
  - **Dispersal from other animals**: internal or external.
  
## Dispersal models

- From the different types of movement (immigration/emigration/migration/dispersal), we get different dispersal models for a population:
  - **Random distribution - דגם פיזור אקראי**: each individual can be everywhere with no dependency on other organisms/other factors.
  - **Regular/uniform distribution - דגם פיזור סדור**: individuals will keep away from each other.
  - **Clamped/aggregated distribution - דגם פיזור מקובץ**: most common, individuals will keep close the each other.
 
 ![population distribution](./img/07_Migration_and_Dispersal/population_distribution.png)
