# Relationships Predator-Prey

- When there's 2 populations - 1 predator, 1 prey - there's a need for balance so the 2 populations can live:
  - If the predators kill too many preys, the prey population will go extinct.
  - If the preys population goes extinct, the predators population will go extinct since it relies on the preys to survive.
- **Theoretic model of Volttera and Lotka** is the theoretic model allowing shared existance predator/prey without driving them to extinction.
- In this model there are a few suppositions (the model can be made more complex by changing some of those - outside of this course):
  1. **There are only 2 populations: predator and prey** with the predators relying on the prey to survive.
  2. In the absence of predators, **the prey population increases in a logistic manner**.
  3. In the absence of preys, **the predator population decreases in a logistic manner**.
  4. Distribution of the predators and preys is random.
  5. The ratio of successful preyings is constant for specific numbers of predators/preys.
  6. The ratio between the number of predators and preys is linear.
  7. All results from the preying is instaneous (no wait time from catching/digest/etc).
  8. The energy surplus the predator gets from the preying is instaneously translated into offsprings.
- The predators population will always be smaller than the prey population.
- There 2 factors that affect the results:
  - Opening conditions - תנאי פתיחה: **starting quantity ratio predators/preys**.
  - **Preying success**: capability of the predator to catch its prey resulting in **surplus of energy**. This has 2 components: 
    - Capabilty of the prey to escape the predator.
    - Capabilty of the predator to catch the prey and translate the energy from the preying into creating offsprings.
- **Optimal foraging - טריפה אופטימלית**: minimum effort (energy invested), maximum energy surplus (from catching prey).

### Growing rate of a prey population in presence of predators

- The growing rate of a prey population in presence of predators depends on its capability to escape the predators.
  - **N**: size of the prey population.
  - $`\bold{\varepsilon P}`$: capability of the prey to escape the predators (depends on the size of the predator population).
  - $`\bold{r_1}`$: growth coefficient of the prey population.
  
  ```math
  \frac{dN}{dt} = (r_1 - \varepsilon P)N = r_1N - \varepsilon PN
  ```
  
### Growing rate of a predator population in presence of preys

- The growing rate of a predator population in presence of preys depends on its capability to catch the preys.
  - **P**: size of the predator population.
  - $`\bold{\varepsilon N}`$: capability of the prey to catch the preys (successful preyings) - depends on the size of the size of the prey population.
  - $`\bold{r_2}`$: growth coefficient of the predator population.
  
  ```math
  \frac{dP}{dt} = (r_2 - \theta N)P = r_2P - \theta NP
  ```
 
### Comparasion of the 2 populations growth

- Both populations start to grow.
- The more preys there are the more the predator population can grow.
- The more predators there are, the more they catch preys resulting in a decrease in the prey population.
- Decrease in the prey population means less food for predators which means the population starts to decrease.
- Less predators results in a growth in the prey population.
- And so on and so on. 

 ![comparasion prey predators](./img/08_Predator_Prey/prey_predators.png)
 
### Balance between the population

- When there is balance between the 2 population, it means that there is no change in the sizes of the populations.
- When there's preying but both of the populations are stable (no growing or declining) it is called **zero net growth isocline - איזוקלינת האפס**.

#### Prey
- No change in the size of the prey population means that **preying death + natural death = reproduction**.
- If the population is stable then dN = 0 which means:

```math
\left.\begin{aligned}
&\frac{dN}{dt} = 0 \\ 
&\frac{dN}{dt} = r_1N - \varepsilon PN 
\end{aligned}\right\}
\Longrightarrow r_1N - \varepsilon PN = 0 \Longrightarrow
\left\{\begin{aligned}
&r_1 = \varepsilon PN \\
&\bold{P} = \frac{r_1}{\varepsilon}
\end{aligned}\right.
```
- Conclusion, when the size of the predator population is:
  - Equal to $`\frac{r_1}{\varepsilon}`$ the size of the prey population is stable.
  - Smaller than $`\frac{r_1}{\varepsilon}`$ the size of the prey population will increase. 
  - Bigger than $`\frac{r_1}{\varepsilon}`$ the size of the prey population will decrease. 

#### Predator
- If the population is stable then dP = 0 which means:

```math
\left.\begin{aligned}
&\frac{dP}{dt} = 0 \\ 
&\frac{dP}{dt} = r_2P - \theta NP 
\end{aligned}\right\}
\Longrightarrow r_2P - \theta NP = 0 \Longrightarrow
\left\{\begin{aligned}
&r_2 = \theta NP \\
&\bold{N} = \frac{r_2}{\theta}
\end{aligned}\right.
```
- Conclusion, when the size of the prey population is:
  - Equal to $`\frac{r_2}{\theta}`$ the size of the predator population is stable.
  - Smaller than $`\frac{r_2}{\theta}`$ the size of the predator population will decrease. 
  - Bigger than $`\frac{r_2}{\theta}`$ the size of the predator population will increase. 
  
|||
|--|--|
|![graph populations](./img/08_Predator_Prey/graph.png)|![circular graph populations](./img/08_Predator_Prey/circular_graph.png)|

#### Notes

- When doing experiences, the conditions can affect the results (like fish in a barrel).
- When looking at numbers from natural environment, the results can be explained also by external factors (like climate).

### Effect of preying on the predator population

#### With more preys available to the predators, the predators population grows until:

- **Saturation - תגובת רוויה**: the density of the population increases but its increase rate decreases and stablize due to other population regulations (וויסות אופטימליה). Food is only one of the population regulations.
- **Selective consumption - אכילה סלקטיבית**: the predator population still eats at the same rate and the population won't grow even if the prey population grows. For example, territorial populations like lions.
- **Decrease in effectivness of the preying**: with more preys it is easier to catch them, thus the effectivness of preying will decrease which causes the predator population to get smaller.

   ![effect preying on predators](./img/08_Predator_Prey/preying_predators.png)

#### Functional response (תגובת תפקודית) of the predator

- The predator will eat faster and will prey more frequently. 3 responses:
  - **Saturation or satiation (רוויה או שובע)**: preying is proportional to the preys availability up to the point of satiation where it stablizes.
  - **Effectiveness of the taking care of prey that was caught**: the effectivness for taking care of the prey will increase until it becomes a limiting factor (too much prey to hunt, not enough time to eat them).
  - **Sigmoidal response**: at first there's a learning curve for the predator to know the prey population. The more there are preys the better it learns. Then the preying effectiveness increases until it stablizes either because of availability of preys of the effectiveness of eating becomes a limiting factor.

   ![eating responses](./img/08_Predator_Prey/eating_responses.png)

#### The price of preying

- Preying can be dangerous (physically and energetically) and that's why it needs to be worth from an energy point of view.
- The preys availability needs to be enough for the preys to be worth hunting in order to grow in size and to bring offsprings.

#### Effectiveness of preying

- $`\bold{N_a}`$: preying effectiveness measurement = number of preys caught be the predator in a specific time lapse.
- **a**: constant of search effectiveness of the predator.
- $`\bold{N_0}`$: preys density.
- **b**: constant of time spent taking care off/eating the caught prey.
- $`\bold{T_s}`$: time freeing caught prey (?)

```math
N_a = \frac{a \times T_s \times N_0}{1 + a \times b \times N_0}
```

#### Prey preferences

- When there is a choice in prey, the predator will choose the most worth. 2 options:
  - The easiest prey: less energy to invest in catching it.
  - The biggest prey: biggest energy value.
  
   ![prey preference](./img/08_Predator_Prey/prey_preference.png)
   

### Effect of preying on the prey population

