# Intro to Ecology

**Ecology is the science of the interactions between organisms and the surroundings they live in.**

## Hebrew/English Lexicon
| Hebrew      | English     | Definition                       |
|-------------|-------------|----------------------------------|
| חומרי דישון | Fertilizers |                                  |
| אביוטי      | Abiotic     | Non dependant on other organisms |
|          טורף   | predator            |                                  |

## 3 Approches of Ecology

- **Descriptive Ecology** (גישה תיאורית): Describing what is seen in the world. Froms there deduce statistics formulas.
- **Theoretical Ecology** (גישה תיאורטי): Start with a rule without basing it on experiments or observations, then check if the rule actually works.
- **Experimental Ecology** (גישה ניסויית): Instead of deducing just for natural observations, deductions are made from experiments which means human involvement in the process. From the results the rule is created.

## What does Ecology

- **Effect of environmental factors on the organisms**.
  - Environmental factors are: temperature, humidity, light and so on (most of them are **abiotic factors**).
- **Effect of the organisms on the environment**.
- **Growth patterns (דגמי גידול) of plants and animal populations**.
- **Interactions between populations** like competition, sharing, preying).
- **Structure and functioning of classes of organisms**.
- **Organisms distribution and biology diversity**.

### Interactions between Ecology and Biology
- **Evolution**
- **Behavior**
- **Physiology**
- **Genetics**

### What is environmental quality (איכות הסביבה)

- Interactions between humans and the surroundings we live in.
  - Polution
  - Changes in ground use.
  - Environmental economy.
  - Environmental ethics and so on and so on.
  
### Interactions between Ecology and Environmental quality
- Earth Science
- Ethics and law
- Economy
- Medecine
- Environment planning

## History

- In the 18th century, Darwin was a doctor on the "Beagle" ship.
- First work on demography around the end of 18th century.
- Around mid 19th century, Lyell wrote a "biology" book that actually is an ecology book before ecology was invented.
- With the idea of god being strong, scientists always saw nature as perfect.
- In the beginning of the 20th century, 2 ideas came up:
  - **Some species are extinct (נכחדו)**.
  - **Resources in nature are limited thus competition is important**.
- From then on, **ecology** moved from being a science that describes processes without rules to a **quantitative science** that look for rules.

### A quantitative science

- Ecology takes into account a lot of variables, a lot more than in physics for example.
- Some of those variables can be complex.
- Types of variables
  - **Additive Effect - משתנים מצתברים **$`\longrightarrow 3 + 2 = 5`$
  - **Substitution Effect - משתנים מתחסרים** $`\longrightarrow 3 + 1 = 2`$
  - **Synergistic Effect - משתנים סינרגטיים** $`\longrightarrow 3 + 2 = 10`$
  - **Chaotic Effect - משתנים אלינאריים** $`\longrightarrow 3 + 2 = ?`$

| Additive Effect                 | Substitution Effect                     | Synergistic Effect                    | Chaotic Effect |
|---------------------------------|-----------------------------------------|---------------------------------------|----------------|
| ![additive](./img/01_Intro/additive.png) | ![substitution](./img/01_Intro/substitution.png) | ![synergistic](./img/01_Intro/synergistic.png) | ?              |


## Some terms to know
- **Ecosystem - מערכת אקולוגית**: 
  - Basic unit of ecology.
  - Fully working system even if it has some interactions with external factors.
  - Composed of several habitats (see below for definition).
  > Example:  
  > A lake  
- **Abiotic and Biotic componants -  מרכיבים אביוטיים וביוטיים**:
  - Every ecosystem contains some abiotoc and some biotic componants.
  - **Abiotic componants**: temperature, light, humidty, wind, oxygen, etc.
  - **Biotic componants**: competition, preying, sharing, etc.
- **Habitat - בית גידול**:
  - Also called **biotop**.
  - System with specific conditions (biotic and abiotic) that enable particular species to live.
  - Isn't an independant system (for example the water at the surface is an habitat but there is not surface without the lake).
  > Examples:  
  > - If we take a rock then there are several habitats: above the rock, under rock  
  > - In a lake: the water at the surface, the ground below the water, the beach, etc.  
- **Ecological niche - נישה אקולוגית**:
  - Ecological conditions surroundings a specific species that enable it to live.
  - This has nothing to do with geographic location!
  > Examples:  
  > - The temperatures can enable some species to live but not other  
  > - The nutritive niche of chimpazees is where there termites  
  
  > Note:  
  > Adaptation is when a species adapts to a different niche.
- **Landscape - יחידת נוף**:
  - Also called **biom**.
  - Multiple ecosystems.
- **Biosphere - ביוספרה**:
  - All the biotic componants on Earth or in other words all the ecological systems on Earth.
- **Population - אוכלוסיה**:
  - All the individuals of the same species within and ecological community.
- **Species - מין**: 
  - Two individuals are in the same species if they can reproduce naturally and the children are fertile (פוריים).
- **Society - חברה**: 
  - Multiple species living together
