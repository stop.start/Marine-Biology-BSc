# Limiting factors and their effect on Organisms

## Some definitions

- **Habitat - בית גידול**:
  - Also called **biotop**.
  - System with specific conditions (biotic and abiotic) that enable particular species to live.
  - Isn't an independant system (for example the water at the surface is an habitat but there is not surface without the lake).
  > Examples:  
  > - If we take a rock then there are several habitats: above the rock, under rock  
  > - In a lake: the water at the surface, the ground below the water, the beach, etc.  
- **Ecological niche - נישה אקולוגית**:
  - Ecological conditions surroundings a specific species that enable it to live.
  - Each condition has a gradient (range) that match a species needs.
  - This has nothing to do with geographic location!
  > Examples:  
  > - The temperatures can enable some species to live but not other  
  > - The nutritive niche of chimpazees is where there termites  
- **Continous distribution - תפוצה רציפה**:
  - When a species is found on a large geographic domain (e.g northen hemisphere).
- **Discontinued distribution - תפוצה מופסקת**.
  - Can be found on a large geographic domain but with some areas that it won't.
- **Fitness - כשירות**: 
  -  **מותאמות**
  - The ability of an organism to survive with the ability to reproduce.
  - **Success**: the survival and living of the offsprings.

## Range of conditions for species to live

- For a specific species X and a specific condition C that is part of its niche (like temperature), how individuals are distributed accross the niche.

> Example:  
> If a species can live between $`5\degree \text{C}`$ and $`35\degree \text{C}`$:  
> If it lives at either of the extremities it will be uncomfortable and it will require a lot of energy to live there.  
> It won't reproduce there but will be able to live.  
> If a tree is planted in it's survival domain it will grow but won't give fruits.  

- **Survival domain - תחום השרידות**: 
  - The extreme conditions that a species can live with in an uncomfortable way (e.g extreme temperatures). 
  - Those conditions are **unfavorable to reproduction** due to the energy being used to survive.
- **Tolerance domain - תחום הסבילות**: 
  - Better conditions where a species can live and reproduce.
- **Optimal domain  - תחום אפטימלי**:
  - Optimal conditions, comfortable to live in and reproduce.
  - All individuals in the species don't live in this domain because it's crowded (thus more competition).
- **Lethal domain - תחום תמותה**: 
  - No chance of survival.

| General graph | Peas |
|---------------|------|
|![living domains](./img/04_Limiting_Factors_on_Organisms/living_domains.png)|![living domains temperature](./img/04_Limiting_Factors_on_Organisms/living_domains_temp.png)|

- If an area has the right conditions most of the time for a species but at some point the conditions are lethal, then there's no chance of survival and reproduction in this area.
- On the graph below, on the left is the **survival temperature range** in purple and **optimal temperature range** in green.
- On the right is described the temperature range of 4 areas A,B,C and D:
  - A and B are within the survival domain thus the species will be able to live and reproduce.
  - C and D, although at some point the temperature is suitable for the species, the species cannot survive there (thus won't be found there) because the **temperature range overlaps the lethal domain**.

![living domain example](./img/04_Limiting_Factors_on_Organisms/living_domain_example.png)

## Adaptation

- **Adaptation - התאמה**: 
  - Solution of the organism to cope with problems that its environment put it through.
- There are 3 types of adaptations:
  - **Behavioral adaptation - אדפטציה התנהגותית**: examples: change of active hours, living in groups, going underground, etc..
  - **Physiological adaptation - אדפטציה פיזיולוגית**: examples: stream more red blood cells to survive with less oxygen.
  - **Structural adaptation - אדפטציה מבנית**: changes in the structure of the body - examples: development of big ears to get rid of too much heat, opposable thumd to hold things, degeneration of feet and eyes in order to live underground.

### Example of behavioral adaptation: fringe-toed lizard (שנונית)

- Changes its active hours according to the time in the year.
- In winter it is not active as long as the temperature is not high enough to function (lower temperature requires more energy).
- In summer, it is the opposite. It is active when the temperature is lower because when it's too hot it requires more energy.

![lizard active hours](./img/04_Limiting_Factors_on_Organisms/lizard_active_hours.png)
  
### Example of physiological adaptation: gerbil

- The gerbil changes decreases its water loss with the rising of temperature.
- Above $`45\degree\text{C}`$ this protective system stops working and it dies.

## Mutations

- Mutations are random. 
- Some are good, some are bad and some are neutral.
- Bad mutations usually won't continue because the individuals carying them won't be able to live long enough to pass them on.
- Good mutations will develop more and more as they are beneficial and will spread out with time.
- Some neutral mutations can suddenly be not neutral when there's a big change in the environment.

## Phenotypic plasticity 

- **Phenotypic plasticity - פלסטיות פנוטיפית** is the ability of one genotype to produce more than one phenotype when exposed to different environments (wikipedia).
- Phenotype is the external characteristics.
- Characteristics comes at least from 2 genes (mother, father) but some are in more than 2 genes.
- Characteristics in more than 2 genes are the result of a mix between those genes.
- There can be a gene for some characteristic and **another gene which is the trigger for the gene to be active**.
- **Phenotypic plasticity** is the abilty to activate a gene or another according to the environments.

### Example: salamander

- Salamanders are born in water but live on land. 
- When the season change and it starts to get warmer, the water level decreases and they start to live on land.
- Up until they start to live on land they live in water thus breathe with gills.
- Once they live on land they breath with lungs.
- But if the water level doesn't decrease because the temperature doesn't rise enough, they continue to develop in water and continue to breath with gills.
- Adults, once they live on land or water cannot go back.

### Example: pinus murrayana

- Tree which has different appearences depending how high the ground is.
- The higher level it on is the shorter it is in order to face the extreme concentrations (like stronger winds and snow).

![phenotype tree](./img/04_Limiting_Factors_on_Organisms/phenotypes_tree.png)

## Ecotypes

- **Ecotypes**: populations within a species that are alike for most characteristics but differ in specific characteristics.
- They often can mix and reproduce between them. The offsprings will have characteristics in between (like brown skin from a black parent and white parent).
- The more different the areas where species live the more different the species will evolve.
- Two populations of the same species can elvove and adapt so much to their respective areas that they won't be able the reproduce and are not the same species anymore. This will mainly happen when there's geographical barrier.

 > _Example where it is almost at the point where the populations are not the same species anymore:_  
 > The **mole**.  
 > Common is Israel, in different types of grounds.  
 >  
 > Their eyes and legs went adapted to the conditions of living in the ground (short legs, eyes just differenciate between light and dark).
 >  
 > One of the populations has **2n=60** chromosomes. Another has **2n=58** chromosomes  
 > The third has **2n=54** chromosomes and the fourth **2n=52** chromosomes  
 >  
 > The populations with 60 and 58 chromosomes can reproduce. The population with 54 chromosomes can reproduce with the population with 58 chromosomes but not with the population with 60 chromosomes and so on.  
 
 |                                                          |                                                               |
 |----------------------------------------------------------|---------------------------------------------------------------|
 | ![mole](./img/04_Limiting_Factors_on_Organisms/mole.png) | ![mole](./img/04_Limiting_Factors_on_Organisms/mole_pops.png) |
 |                                                          |                                                               |


## Zoo-geographic rules

- Rules that deals with morphological differences between populations of the same species living in different geographic areas.
- Those rules are about adaptations to deal with temperature.

### Bergmann's rule

- **Relations between body surface area and its volume**.
- In **cold areas** the populations of a specific species will be **bigger** whereas in **hot areas** the populations will be smaller.

![wolves](./img/04_Limiting_Factors_on_Organisms/wolves.png)

### Allen's rule

- **Big limbs help release heat excess**.
- Like big ears, long tail, long legs are use to get rid of heat excess.

![rabbits](./img/04_Limiting_Factors_on_Organisms/rabbits.png)

### Gloger's rule

- **Dark pigmentation increases the absorption of radation**.
- This rule is about the skin and not the fur of animals and doesn't apply to humans (who don't have fur).
- Under the white (actually transparent) fur of northen animals the skin is black and helps get warmer.

