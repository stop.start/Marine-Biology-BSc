# Populations Growth

- 4 factors affect the size of a population:  
  - **Natality - רבייה**: increases the population size.
  - **Mortality - תמותה**: descreases the population size.
  - **Immigration - הגירה פנימה**: increases the population size.
  - **Emmigration - הגירה החוצה**: descreases the population size.
  
## Populations with separate generations

- **Populations with separate generations***: reproduce once per generation and the offsprings replace the parents. _For example: salmons which reproduce and die._
- **Model** describing this type of population where:
  - t is the current generation and t+1 is the next.
  - $`N_t`$ is the size of current population.
  - $`N_{t+1}`$ is the size of the population for the next generation.
  - $`R_0`$: net natality (include only the ones that get to maturity and reproduce).
- When calculating the net natality, we need to take into consideration if it is asexual reproduction. If it isn't then the number of offsprings needs to be divided by 2 (2 parents for 1 offspring = 0.5 net natality).

 _Note: it gets more complicated when one male reproduces with many females._

```math
N_{t+1} = R_0 \times N_t
```
- The **net natality** can be calculated with:

```math
R_0 = \frac{\text{Number of offsprings born at t+1}}{\text{Number of offsprings born at t}}
```

![r0](./img/05_Growing_Populations/r0.png)

- Example for generations up to t+8:

![t8](./img/05_Growing_Populations/t8.png)

- The bigger $`R_0`$ the faster the population grows from generation to generation.

![r0 comparasion](./img/05_Growing_Populations/r0_comparasion.png)

## Populations with overlapping generations

- **Populations with overlapping generations**: reproduce more than once and the parents and offsprings can reproduce at the same time.
- We need to take into consideration the length of the generation.
- The **size of the population changes constantly** and the **model** is differential: $`\frac{dN}{dt} = rN`$
- In **integral**:
  - t is the time in days/years etc.
  - $`N_t`$ is the size of the population at time t.
  - $`N_0`$ is the size of the population at t=0.
  - r growth coefficient of population - growth potential. Varies depending on the conditions.
  - e is the natural logarithm base: 2.71828.
  
```math
N_t = N_0 \times e^{rt}
```
![overlapping generations](./img/05_Growing_Populations/overlap_gen.png)

- **Coefficient r** varies depending on the living conditions. The higher the value of r the higher the population growth.
- In the graph below, the r is shown in function of temperature and humidity (pink is 14% humidity, green 11% and red 10.5%). The r is optimal with temperature of 29 and humidity of 14%.

![example overlapping generations](./img/05_Growing_Populations/example_overlap.png)

## Calculations related to growth coefficient r

### Calculate r from $`R_0`$

- G being the length of the generation (known variable).
- $`R_0`$ is the reproduction coefficient for the first generation.

```math
r = \frac{ln(R_0)}{G}
```

### Calculate r from natality and mortality rates

- b is the natality rate
- d is the death rate

```math
r - b-d
```

This allow to calculate:  

- N being the size of the population.
- r the growth coefficient.
- t the time.

```math
\frac{dN}{dt} = rN = (b-d)N
```

### Calculate the time it takes for a population to double

```math
t = \frac{ln2}{r}
```
In order to know how much time for the population to tripple then ln3 instead of ln2 (and so on).  

### Why populations do not grow unlimited? (not considering the human populations)

- Limiting factors affect the coefficient r.
- The **main limiting factor** is how many organisms an area can hold - כושר הנשיאה של שטח. Symbol: K.
  - It is set by the **primary production** in the specific area.
  - The limiting factors for the primary production in the specific area will thus limit the carrying capacity of the area (כושר הנשיאה של שטח).
- When reaching the limit of the area the growth of a population will stabalize.
- In order to calculate the **growth potential** of a population for the area capacity K: 

```math
\frac{dN}{dt} = rN \times \frac{K-N}{K}
```
 
![area capacity](./img/05_Growing_Populations/area_capacity.png)

> _Note:_  
> When there's one individual and when there's 99 individuals the growth rate is the same but for different reasons:  
> One individual will reproduce thus grow the population more slowly than more than one individual.  
> With 99 individuals there are not enough resources thus the growth rate is slow.  

### The carrying capacity of an area isn't fixed

- Sometimes the carrying capacity of an area changes due to **changes in the environment** (change in climate, fire, etc).
  - When something like this happens, the growth of the population isn't affected right away but with time.
- **Population explosion - התפוצצות אוכלוסין** happens when a **population grows exponentially without stabalizing until it collapses**. This happens usually because of humans interaction.
