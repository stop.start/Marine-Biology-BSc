# Energy in Ecosystems

## Hebrew/English Lexicon

| Hebrew       | English             | Definition                                                                 |
|--------------|---------------------|----------------------------------------------------------------------------|
| היתוך גרעיני | Nuclear fusion      | Process where 4 Hydrogen atoms creates one Helium atom and releases energy |
| יצור ראשוני  | Primiary production |                                                                            |
| עמילן        | Starch              | Multiple glucose molecules weakly bonded                                   |
| תאית         | Cellulose           | Multiple glucose molecules strongly bonded                                 |
|              |                     |                                                                            |

## Intro

- The main origin of energy is the **nuclear fusion** from the Sun.
- **Nuclear fusion** is the process where 4 Hydrogen atoms creates one Helium atom and releases energy.
- The **energy** from the nuclear fusion is released in two ways: **light** and **heat**.
- **Only the light arrives to Earth**.
- The heat we feel is the heat released by the Earth's ground when **reflecting radations (light)**: it reflects them in different wavelength (infrared).
- The **greenhouse effect** is what allows to keep the heat close to the ground.

## Some terms to know

- **Biomass** is the total mass of organisms in a given area or volume.
- **Primary production - ייצור ראשוני** is the biomass (or energy) created from photosynthesis.
- **Secondary production - ייצור שניוני** is the added biomass (or energy) by the organisms that eats plants and predators.
- _The primary and secondary production are the energy that passes to the next level on the food chain_.
- **GPP or gross primary production - הייצור הראשוני הגולמי**: total amount of energy created during photosynthesis.
- **NPP or net primary production - הייצור הראשוני נטו**: amount of energy created during photosynthesis - amount of energy used in the metabolism. 
- **Productivity - ייצרנות**: it is the **rate** of generation of energy in a given time lapse.
- **Food web (מארג)** is replacing the expression food chain.

## Primary Producers

- **Chlorophyll a** is a compound that allows to transform non organic matter to organic matter (glucose) when absorbing light - **photosynthesis**:

```math
6CO_2 + 6H_2O \Longleftrightarrow C_6H_{12}O_6 + 6O_2
```
  
- This reaction produces as a side product oxygen which is released and not used.
- When living organisms break down **glucose** it releases the energy needed to live and exist.
- All green organisms (mainly plants) have the chlorophyll a.
- **Starch - עמילן** is when a lot of glucose molecules are bonded together and their bond is weak which allows for the glucose to be **readily available**.
- **Cellulose - תאית**is when a lot of glucose molecules are bonded together and their bond is strong and hard to break down which allows for the **glucose to be stored**. 
- **Primary producers** are organisms that don't need organic matter in order to live (by doing photosynthesis).
- **Primary consumers** are the organisms **eating** the primary producers.

### Efficiency of primary producers 

```math
\text{Efficiency} = \frac{\text{Energy created from primary producers}}{\text{Energy coming from the Sun}}
```

- Only a small fraction of the energy from the Sun is used by the primary producers during photosynthesis which means the efficiency is really low.
- Part of the energy created by the primary producers is used for **basic metabolism (respiration), reproduction (offsprings), growing tissues (like cellulose)**.
- The energy that can be **passed on** to others is from **tissues** and **offsprings**.

![energy usage](./img/02_Energy_in_Ecosystems/pp_energy_usage.png)

### How to measure net biomass production?

```math
\Delta B_G = Biomass_{t_2} - Biomass_{t_1}
```

### How to measure the net biomass productivity?

Productivity being a rate, we need to divide the previous net biomass calculated by the time that has passed between $`t_1`$ and $`t_2`$:

```math
P_G = \frac{\Delta B_G}{\Delta t}
```

### How to measure the gross biomass production?

We need to add to the previous the biomass that was lost during respiration, death and decomposition as well as the biomass eaten by herbivors:

```math
\Delta B_N = \Delta B_G + L_{t_2-t_1} + G_{t_2-t_1}
```
Where L is the biomass lost during respiration and death and decomposition and G the eaten biomass.

### The light/dark bottles method

One method to measure gross and net biomass production is to check the production of $`O_2`$ from primary producers.  
From the $`O_2`$ production, the production of glucose can be easily deduced (see photosynthesis equation).  
Using light and dark bottles can be used to achieve that:  

- Take transparent and dark bottles.
- Fill them with water from the same source.
- Check the concentration of $`O_2`$
- Put the bottles in the source where the water was taken from.
- Wait a few hours.
- Check back the concentration of $`O_2`$:
  - In the black bottle the concentration of $`O_2`$ decreased due to respiration.
  - Int the transparent bottle the concentration of $`O_2`$ increased minus what was used.

> Example:  
> The concentration of $`O_2`$ at the beginning of the experiment is 6mg/L.  
> After a few hours, in the dark bottle there is a concentration of 2mg/L $`\longrightarrow`$ 4mg/L were used for respiration.  
> In the transparent bottle there is a concentration of 12mg/L $`\longrightarrow`$ 6mg/L were produced **net**.  
> In order to know the gross production in the transparent bottle we add the known concentration used by respiration (4mg/L) $`\Rightarrow`$ 6mg\L + 4mg\L = 10mg\L gross production.  

## Secondary producers

- Secondary producers are primary consumers.
- The equivalent of readily available energy - starch (עמילן) in primary producers - in secondary producers is **glycogen**.
- The equivalent of stored energy - cellulose (תאית) in primary producers -  in secondary producers is **fats**.
- **Fats** is made from glucose but the molecule is changed.
- When comparing the energy usage there is not much difference between secondary producers and primary producers besides the source of the energy used: secondary producers eat primary producers.

![energy usage](./img/02_Energy_in_Ecosystems/sp_energy_usage.png)

- Some of the energy is used for activity and resting or **maintenance** (basic metabolism).
- Some of the energy is used for growth and reproduction or **production**.

### How to calculate the energy balance for animals?

- First way:

```math
\text{Energy} = \text{Energy invested in Maintenance} + \text{Energy invested in Production}
```
- Second way which will give a different result from the above:

```math
\text{Energy} = \text{Energy digested} + \text{Energy wasted (poop)}
```

### How to measure the secondary production?

```math
P_{II} = \text{Growth} + \text{Number of births}
```

Or

```math
P_{II} = \text{Net change in biomass} + \text{Decomposition}
```

### Next level on the food chain

- **First predators**: the ones that eat the secondary producers.
- **Superior predators**: the ones that eat others but are not eaten.
- **Every food level is small than the previous one**.
- As seen in the example below **only a small fraction of the energy passes from one level to another**.

### Measuring the efficiency of secondary producers

It is calculated based on the **food consumption rate**:

```math
\text{Efficiency} = \frac{\text{Energy in Production}}{\text{food consumption rate}}
```
> Example - Elephants:  
>  
> **Net primary productivity (=food available)**: 747 Kcal/m<sup>2</sup>/year  
>  
> Food consumption: 71.5 Kcal/m<sup>2</sup>/year  
> Feces (= energy not used): 40.2 Kcal/m<sup>2</sup>/year  
> **Net energy from food = food consumption - feces**: 71.5 - 40.2 = 31.3 Kcal/m<sup>2</sup>/year  
>  
> Basic metabolism: 24.2 Kcal/m<sup>2</sup>/year  
> **Growth and offsprings (=Production)**: 7.1 Kcal/m<sup>2</sup>/year  
> The **secondary production** is the above Production: 7.1 Kcal/m<sup>2</sup>/year  
>  
> **The efficiency is $`\bold{E = \frac{\text{Production}}{\text{Food consumption}}}`$**: 7.1 / 71.5 = 10%  

- Notes:
  1. Warm blooded animals need to keep their temperature which require energy. Invertebrates don't have this issue thus have a higher efficiency (around 25%).
  2. Surface area (unit<sup>2</sup>) is were animals lose heat while their inside (volume, unit<sup>3</sup>) is where they create heat. The bigger the surface area compared to the volume the higher the lost of heat. Because the volume will increase faster then surface (v<sup>3</sup> compared to s<sup>2</sup>) then the smaller the animal the fater it loses heat thus the more it has to eat in order to keep its temperature.

#### Efficiency per group
- Poultry: 1.29%
- Mammals:
  - Insects eaters: 0.86%
  - Rodents: 1.51%
  - Big herbivors: 3.14%
- Fishes: 9.8%
- Invertebrates: 25%

## Quantitive comparision between the food levels

- There are several ways to compare between food levels:
  - Comparing the number of individuals. Can be not significative when comparing single cell organisms versus mammals.
  - Comparing the biomass of species. Can be not significative because some animals have more bones that are not nutritive and some more fat.
  - Comparing the energy: the best option.
  
| Number of individuals                                                   | Biomass                                               | Energy                                              |
|-------------------------------------------------------------------------|-------------------------------------------------------|-----------------------------------------------------|
| ![number of individuals](./img/02_Energy_in_Ecosystems/individuals.png) | ![biomass](./img/02_Energy_in_Ecosystems/biomass.png) | ![energy](./img/02_Energy_in_Ecosystems/energy.png) |


## Summary of the energy consumption

![summary](./img/02_Energy_in_Ecosystems/summary.png)
