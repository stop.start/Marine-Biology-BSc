**Elinor Alpay Natanzon**  
300619368

# Assignment 6

## Exercise 6

### Question 1

- $`y_0 = 20m`$
- $`v_{0x} = 30m/s`$
- $`v_{0y} = 0`$

- Let's calculate when it needs to drop the children by looking at the y axis:

```math
\begin{aligned}
&y(t) = y_0 + v_{0x} \cdot t + \frac{1}{2}a\cdot t^2 \\
&0 = 20 + 0 + \frac{1}{2}(-9.8) \cdot t^2 \\
&t_1 < 0 \quad t_2 = 2.02s
\end{aligned}
```
- From then we can use t to know at which distance by looking at the x axis:

```math
\begin{aligned}
&x(t) = v_{0x} \cdot t \\
&x(t=2.02s) = 30 \cdot 2.02 \\
&\bold{x = 60.6m}
\end{aligned}
```
## Others

### Question 4

- $`y_0 = 40m`$
- $`v_{0y} = 23m/s`$
- $`v_{0x} = 0`$

#### What is the maximum height it will reach?

- At maximum height the speed will be 0m/s. From this can calculate:

```math
\begin{aligned}
&y = y_0 + \frac{v_y^2 - V_{0y}^2}{2a_g} \\
&y_{max} = 40 + \frac{0 - 23^2}{2 \cdot -9.8} \\
&\bold{y_{max} = 66.98m}
\end{aligned}
```

#### How long is the rock in the air until it reaches the ground?

```math
\begin{aligned}
&y(t) = y_0 + v_{0y} \cdot t + \frac{1}{2}a\cdot t^2 \\
&0 = 40 + 23 \cdot t + \frac{1}{2}(-9.8) \cdot t^2 \\
&t_1 < 0 \quad \bold{t_2 = 6.04s}
\end{aligned}
```

### Question 5

- $`y_0 = 0m`$
- $`y = 12m`$
- $`v_{0} = 30m/s`$
- $`\alpha = 40\degree C`$

#### How long is the ball in the air?

- Let's calculate when it lands by looking at the y axis. The ball will be at height 12m twice and we're interested in the longest time.

```math
\begin{aligned}
&y(t) = y_0 + v_{0y} \cdot t + \frac{1}{2}a\cdot t^2 \\
&12 = 0 + 30 \sin40\degree C \cdot t + \frac{1}{2}(-9.8) \cdot t^2 \\
&0 = -12 + 19.28 \cdot t + \frac{1}{2}(-9.8) \cdot t^2 \\
&t_1 = 0.77s \quad \bold{t_2 = 3.16s}
\end{aligned}
```

#### What's the horizontal distance traveled by the ball?

- Let's use the time from the previous question when looking at the x axis:


```math
\begin{aligned}
&x(t) = v_{0x} \cdot t \\
&x(t=3.16s) = 30 \cos40 \cdot 3.16 \\
&x(t=3.16s) = 22.98 \cdot 3.16 \\
&\bold{x = 72.62m}
\end{aligned}
```
