**Elinor Alpay Natanzon**  
300619368

# Assignment 7

## Exercise 6

### Question 2

- $`x = 69.11m`$
- $`v_0 = 30m/s`$

#### Throwing angle

```math
\begin{aligned}
&R = - \frac{V_0^2 \cdot \sin{2\alpha}}{a_g}\\
&69.11 = \frac{30^2 \cdot sin{2\alpha}}{9.8128}\\
&sin{2\alpha} = \frac{69.11\cdot 9.8128}{900}\\
&2\alpha = \sin^{-1} 0.7535 \\ 
&2\alpha = 48.894 \\
&\bold{\alpha_1 = 24.447\degree}
&\alpha_2 = \frac{180 - 48.894}{2} = 65.553\degree
\end{aligned}
```

#### Difference with $`a_g=9.7967`$

```math
\begin{aligned}
&\frac{R_2}{R_1} = \Large \frac{- \frac{V_0^2 \cdot \sin{2\alpha}}{a_{g2}}}{- \frac{V_0^2 \cdot \sin{2\alpha}}{a_{g1}}} \normalsize = - \frac{\cancel{V_0^2} \cdot \cancel{\sin{2\alpha}}}{a_{g2}} \cdot - \frac{a_{g1}}{\cancel{V_0^2} \cdot \cancel{\sin{2\alpha}}} = \frac{a_{g1}}{a_{g2}} = \frac{9.8128}{9.7967} = \bold{1.00164} \\ 
&R_2 = 69.11 \cdot 1.00164 = \bold{69.22m}
\end{aligned}
```

### Question 3

- Speed of the cannon: 10km/h = 2.7m/s
- Speed of the cannon ball: 800m/s
- Angle of the cannon ball trajectory: $`20\degree`$
- Distance cannon-hill: 10km=1000m

#### When does the cannon ball hit the hill?

- x axis:

```math
\begin{aligned}
&x = v_x \cdot t \Rightarrow t = \frac{x}{v_x} \\
&t = \frac{10000}{2.7 + 800 \cdot \cos{20}} = \bold{13.25s}
\end{aligned}
```

#### At which height does the bullet hit the hill?

- y axis:

```math
\begin{aligned}
&y = y_0 + v_{0y} \cdot t + \frac{1}{2} a_g \cdot t^2 \\
&y = 0 + 800 \cdot \sin{20} \cdot 13.25 + \frac{1}{2} (-9.8) \cdot 13.25^2 \\
&y = 3625.41 - 860.26 \\
&\bold{y = 2765.15m}
\end{aligned}
```

#### Speed when it hit?

```math
\begin{aligned}
&v_x = 754.5\frac{m}{s} \\
&v_y = v_{0y} + a_g\cdot t = \\ 
&v_y = 800 \sin{20} - 9.8 \cdot 13.25 \\
&v_y = 143.766\frac{m}{s} \\
&\bold{v = \sqrt{v_x^2 + v_y^2} = \sqrt{754.5^2+143.766^2} = 768.1\frac{m}{s}}
&\bold{\tan{\alpha}=\frac{v_y}{v_x} = 0.19 \Rightarrow \alpha = 10.8\degree}
\end{aligned}
```

### Question 4

- Angle: $`\alpha = 32\degree`$
- Speed: $`v_0 = 22\text{m/s}`$
- Fence distance: 38m
- Fence height: 3m

#### Does the ball go over the fence?

- x axis, how long did it take to reach the fence:

```math
\begin{aligned}
&x = v_x \cdot t \\
&38 = 22 \cos{32} \cdot t \\
&t = 2.036s
\end{aligned}
```
- y axis, height of the ball at this point:

```math
\begin{aligned}
&y = y_0 + v_{0y} \cdot t + \frac{1}{2}a_g\cdot t^2 \\
&y = 0 + 22 \sin{32} \cdot 2.036 - 4.9 \cdot 2.036^2 \\
&\bold{y = 3.42m \Rightarrow \text{The fence is 3m high and so the ball goes over it by 0.42m}}
\end{aligned}
```

## Others

### Question 1

- W = 130N
- $`\mu_k`$ = 0.3 

#### At what force does the box needs to be pull in order to move at constant speed?

- Constant speed means the sum of the forces equals 0. So:

```math
\left\{\begin{aligned}
&\Sigma F_y = 0 \Rightarrow F \sin{50} - W + N = 0 \Rightarrow 0.766F - 130 + N = 0 \Rightarrow 0.23F - 39 + 0.3N = 0\\
&\Sigma F_x = 0 \Rightarrow F\cos{50}-\mu_k \cdot N = 0 \qquad\quad\qquad \Longrightarrow \qquad\qquad\qquad 0.642F - 0.3N = 0\\\\
&\text{We add the 2 equations:}\\
&0.872F - 39 = 0\\
&\bold{F} = \frac{39}{0.872} = \bold{44.72N} \\
\end{aligned}\right.
```

### Question 2

- $`W_A = 240N`$
- $`W_B = 90N`$
- $`\mu_{kB} = 0.4`$

#### What is the weight of box C?

 ![fig1](./fig1.png)

- A moves down at constant speed so the sum of the forces is equal to 0.
- Forces on A:

```math
\Sigma F_y = 0 \Rightarrow T_A - W_A = 0 \Rightarrow \bold{T_A = 240N}
```

- Forces on B: 
  - y axis:
```math
\begin{aligned}
&\Sigma F_y = 0 \\
&N - W_B = 0 \\
&N = 90N
\end{aligned}
```
  - x axis: 
```math
\begin{aligned}
&\Sigma F_x = 0 \\
&T_A - T_C - f_k = 0 \\ 
&T_A - T_C - \mu_{kB} \cdot N = 0 \\ 
&240 - T_C - 0.4 \cdot 90 =0 \\
&T_C = 204N
\end{aligned}
```

- Forces on C:

```math
\begin{aligned}
&\Sigma F_y = 0 \\ 
&T_C - W_C = 0 \\
&\bold{\bold{W_C = 204N}}
\end{aligned}
```

### Question 6

```math
R = - \frac{V_0^2  \cdot sin{2\alpha}}{2a_g}
```

- On the moon R is 6 times the R of Earth:

```math
\bold{R_{moon}} = 6 \cdot R_{Earth} = 6.65 = \bold{390m}
```
