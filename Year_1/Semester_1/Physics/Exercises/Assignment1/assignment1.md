**Elinor Alpay**  
300619368

# Physics/Mecanics - Assignment 1

## Question 1

### a: Cartesian represention of $`\bold{\overrightarrow{a}}`$ and $`\bold{\overrightarrow{b}}`$
|                 |
|-----------------|
| ![](./fig1.png) |

```math
\begin{aligned}
&\left.\begin{aligned}
a_x &= 8\cdot cos60\degree = 4\\
a_y &= 8\cdot sin60\degree = 6.93
\end{aligned}\right\} 
\bold{\overrightarrow{a} = (4,6.93)}

\\\\

&\left.\begin{aligned}
b_x &= 5\cdot cos45\degree = -3.54\\
b_y &= 5\cdot sin45\degree = 3.54
\end{aligned}\right\} 
\bold{\overrightarrow{b} = (-3.54,3.54)}
\end{aligned}
```

### b: Cartesian represention of:

```math
\begin{aligned}
\overrightarrow{a} + \overrightarrow{b} &= 
\left\{\begin{aligned}
a_x + b_x &= 4 - 3.54 = 0.46\\
a_y + b_y &= 6.93 + 3.54 = 10.47
\end{aligned}\right.
\\
\overrightarrow{a} + \overrightarrow{b} &= \bold{(0.46,10.47)}
\\\\

\overrightarrow{a} - \overrightarrow{b} &= 
\left\{\begin{aligned}
a_x - b_x &= 4 + 3.54 = 7.54\\
a_y - b_y &= 6.93 - 3.54 = 3.39
\end{aligned}\right.
\\
\overrightarrow{a} - \overrightarrow{b} &= \bold{(7.54,3.39)}
\\\\

2\overrightarrow{a} - 3\overrightarrow{b} &= 
\left\{\begin{aligned}
2a_x - 3b_x &= 2\cdot4 + 3\cdot-3.54 = -2.62\\
2a_y - 3b_y &= 2\cdot6.93 + 3\cdot3.54 = 24.48
\end{aligned}\right.
\\
2\overrightarrow{a} - 3\overrightarrow{b} &= \bold{(-2.52, 24.48)}
\end{aligned}
```

### c: Polar represention of $`\bold{\overrightarrow{c} = \overrightarrow{a}+\overrightarrow{b}}`$

|                 |
|-----------------|
| ![](./fig2.png) |

```math
\begin{aligned}
c &= \sqrt{0.46^2+10.47^2} = \bold{10.48}\\
\alpha &= tan^{-1}\frac{10.47}{0.46} = \bold{87.48\degree}
\end{aligned}
```
## Question 2

```math
\begin{aligned}
F_1 &= 50N, 60\degree\\
F_2 &= 40N, 30\degree
\end{aligned}
```

- Cartesian represention of $`F_1`$ and $`F_2`$:
```math
\begin{aligned}
F_{1x} &= 50\cdot cos60\degree = 25N\\
F_{1y} &= 50\cdot sin60\degree = 43.3N
\\\\
F_{2x} &= 40\cdot cos30\degree = 34.64N\\
F_{2y} &= -40\cdot sin30\degree = -20N \quad\text{(undex x axis)}
\end{aligned}
```
- $`F_3`$, the force of the teen:
```math
\begin{aligned}
F_{1x} + F_{2x} + F_{3x} &= 0\\
F_{1y} + F_{2y} + F_{3y} &= 0\\
\\
F_{1x} + F_{2x} + F_{3x} &= 0\\
25 + 34.64 + F_{3x} &= 0\\
F_{3x} &= \bold{-59.64N}\\
\\
F_{1y} + F_{2y} + F_{3y} &= 0\\
43.3 - 20 + F_{3y} &=0\\
F_{3y} &= \bold{-23.3N}
\end{aligned}
```

- $`F_3`$ in polar represention:

|                 |
|-----------------|
| ![](./fig3.png) |

```math
\begin{aligned}
|F_3| &= \sqrt{(-59.64)^2 + (-23.3)^2} = \bold{64.02N}\\
\alpha &= tan{-1}\frac{-23.3}{-59.64} = \bold{21.33\degree}
\end{aligned}
```

## Question 3

|                 |
|-----------------|
| ![](./fig4.png) |

F1 and F2 apply forces on both x and y axis. If we want the box to move only on the x axis we need to cancel out the force on the y axis. We know from the previous question that the F1 and F2 apply *23.3N upward* on the y axis which means that the teen needs to apply **23.3N downward** on the y axis. Not adding any other forces will result in the minimal force for our purpose.



## Question 4
The plane and passenger are going in the same direction so the **speed of the passenger compared to the ground is 500 km/h + 8 km/h = 500 km/h**.

|                 |
|-----------------|
| ![](./fig5.png) |

Let's call $`\overrightarrow{z}`$ the vector representing the speed of the lent compared to the ground:

```math
\begin{aligned}
|\overrightarrow{z}| &= \sqrt{508^2+30^2} = \bold{508.89 \, km/h}\\
\alpha &= tan_{-1}\frac{30}{508} = \bold{3.38\degree}
\end{aligned}
```
