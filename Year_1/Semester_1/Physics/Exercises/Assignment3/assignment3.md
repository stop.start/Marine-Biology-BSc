**Elinor Alpay**  
300619368

# Physics/Mecanics - Assignment 3

## Exercise 2

## Question 3

$`W_A = 490N`$  
$`\mu_{s} = 0.30`$  
$`W_B = 98N`$  

![fig1](./fig1.png)

1. **Friction force on A**

- Forces on B:

```math
\Sigma F_y = 0 \Longrightarrow T_3 - W = 0 \Longrightarrow T_3 = W \Longrightarrow \bold{T_3 = 98N}
```

- Forces on the $`T_1-T_2-T_3`$ junction:

```math
\begin{aligned}
& \Sigma F_y = 0 \Longrightarrow T_{2y} - T_{3} + 0 = 0 \Longrightarrow T2\sin{45\degree} - 98 = 0 \Longrightarrow \bold{T_2} = \frac{98}{sin{45\degree}} = \bold{138.59N} \\
& \Sigma F_x = 0 \Longrightarrow T_{2x} - T_{1} + 0 = 0 \Longrightarrow T2\cos{45\degree} - T_1 = 0 \Longrightarrow \bold{T_1} = 138.59 \cdot \cos{45\degree} = \bold{98N} 
\end{aligned}
```

- Friction on A:

```math
\Sigma F_x = 0 \Longrightarrow T_1 - f_S = 0 \Longrightarrow \bold{f_s = 98N}
```

2. **Maximal weight for B so the system stays balanced**

- Maximal forces on A:

```math
\begin{aligned}
& \Sigma F_y = 0 \Longrightarrow N - W = 0 \Longrightarrow \bold{N = 490N} \\
& \text{f}_s = \mu{s} \cdot N \Longrightarrow \text{f}_s = 0.30 \cdot 490 \Longrightarrow \bold{\text{f}_s = 147N} \Longrightarrow \bold{T_1 = 147N}
\end{aligned}
```

- Maximal forces on the $`T_1-T_2-T_3`$ junction:

```math
\begin{aligned}
& \Sigma F_x = 0 \Longrightarrow T_{2x} - T_{1} + 0 = 0 \Longrightarrow T2\cos{45\degree} - T_1 = 0 \Longrightarrow \bold{T_2} = \frac{147}{\cos{45\degree}} = \bold{207.88N} \\
& \Sigma F_y = 0 \Longrightarrow T_{2y} - T_{3} + 0 = 0 \Longrightarrow T2\sin{45\degree} - T_3 = 0 \Longrightarrow \bold{T_3} = 207.88 \cdot sin{45\degree} = \bold{146.99N} 
\end{aligned}
```

- Maximal weight for B:

```math
\Sigma F_y = 0 \Longrightarrow T_3 - W = 0 \Longrightarrow \bold{W = 146.99N}
```

## Question 4

$`W_B = 294N`$   
$`\mu_{k} = 0.4`$  

![fig4](./fig4.png)

1. **Weight of A**

- Forces on B:

    The speed is constant so $`\Sigma F_y = 0 `$:

```math
\Sigma F_y = 0 \Longrightarrow T_1 - W = 0 \Longrightarrow \bold{T_1 = 294N}
```

- Forces on A:

    We'll take the surface A is on as the X axis.  
    The speed is constant so $`\Sigma F_y = \Sigma F_x = 0`$

```math
\begin{aligned}
&\Sigma F_y = 0 \Longrightarrow N - W_y \Longrightarrow N - W \cdot \cos{60\degree} \Longrightarrow \bold{N = W \cdot \cos{60\degree}} \\
\\
&\begin{aligned}&\Sigma F_x = 0 \Longrightarrow T_1 + f_k - W_x = 0 \\
&T_1 + \mu_{k} \cdot W \cos{60\degree} - W \cdot \sin{60\degree} = 0 \\
&294 + 0.4 \cdot W \cdot 0.5 - W \cdot 0.866 = 0 \\
&\bold{W} = \frac{-294}{0.2-0.866} = \bold{441.44N} \\
\end{aligned}
\end{aligned}
```

## Question 5

1. **Does the forces cancel out each other?**

    Wether the system is static or with a constant speed, the sum of the forces is 0 but they are active: the horse applies force on the cart and the cart applies force on the horse. 

2. **How does the cart move?**

    The cart will move when the horse applies force greater then the static friction force.

## Exercise 4

### Question 3

1. **Draw a graph with speed as a function of time**

| Location x(t)       | Speed v(t)        |
|---------------------|---------------------|
| ![fig2](./fig2.png) | ![fig3](./fig3.png) |


2. **At which times the worm moves left?**

    The worm moves left when speed is negative: between **2 and 5.5**.

3. **What is the average speed of the worm?**

```math
\bar{v} = \frac{-1}{5.5} = 0.18 m/s
```
