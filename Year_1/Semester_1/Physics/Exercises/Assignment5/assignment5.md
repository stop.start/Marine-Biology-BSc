**Elinor Alpay**  
300619368  

# Assignment 5

## Exercise 5

### Question 5

- Ball is going upward at 30m/s.

#### How long does it go up?

- The ball goes up and before it falls back down it has a speed of 0m/s:

```math
\begin{aligned}
&v_y = v_{0y} + a_g \cdot t \\
&0 = 30 - 9.8 \cdot t \\
&\bold{t = 3.06 \; seconds}
\end{aligned}
```

#### What is the maximum height the ball will reach?

```math
\begin{aligned}
&y = y_0 + v_{0y} \cdot t + \frac{1}{2} \cdot a_g \cdot t^2 \\
&y = 0 + 30 \cdot 3.06 + \frac{1}{2} \cdot (-9.8) \cdot 3.06^2 \\
&\bold{y = 45.92m}
\end{aligned}
```

#### What is the speed at half the maximum height?

```math
\begin{aligned}
&y = y_0 + \frac{v_y^2-v_{0y}^2}{2a_g} \\
&\frac{45.92}{2} = 0 + \frac{v_y^2 - 30^2}{2 \cdot (-9.8)} \\
&22.96 \cdot (-19.6) = v_y^2 - 900 \\
&v_y^2 = 900 - 450 \\
&v_y = \sqrt{450} \\
&\bold{v_y = 21.21m/s} \\
\end{aligned}
```

## Exercise 5*

### Question 2

- At half the maximum height ($`y_{max}`$) the body is going at 9.8m/s

#### What is the maximum height?

Using the equation $`y = y_0 + \frac{v_y^2 - v_{0y}^2}{2a_g}`$, we can set $`y_0`$ as half the maximum height thus $`v_{0y}`$ is 9.8m/s. And then:

```math
\begin{aligned}
&y_{max} = \frac{y_{max}}{2} + \frac{0^2 - 9.8^2}{2 \cdot (-9.8)} \\
&\frac{y_{max}}{2} = \frac{0^2 - 9.8^2}{2 \cdot (-9.8)} \\
&\frac{y_{max}}{2} \cdot 2 \cdot (-9.8)= - 9.8^2 \\
&\bold{y_{max} = 9.8m}
\end{aligned}
```

### Question 3

- The sand bag falls from a height of 60m with a starting speed of 5m/s

#### How long will it take for the sand bag to reach the ground?

```math
\begin{aligned}
&y = y_0 + v_{0y} \cdot t + \frac{1}{2} \cdot a_g \cdot t^2 \\
&0 = 60 + 5t + \frac{1}{2} \cdot (-9.8) \cdot t^2 \\
&-4.9t^2 + 5t + 60 = 0 \\
\end{aligned}\\

\Downarrow
```
```math
\begin{aligned}
&t_1, t_2 = \frac{-5\pm \sqrt{25 - 4 \cdot (-4.9) \cdot 60}}{2 \cdot (-4.9)} \\
&t_1 < 0\\
&\bold{t_2 = 4.04 \; seconds}
\end{aligned}
```

#### What will be the sand bag speed when it reaches the ground?

```math
\begin{aligned}
&v_y = v_{0y} + a_gt \\
&v_y = 5 + (-9.8) \cdot 4.04 \\
&\bold{v_y = 34.59m/s}
\end{aligned}
```
