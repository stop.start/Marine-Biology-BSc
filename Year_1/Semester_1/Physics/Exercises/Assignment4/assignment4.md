**Elinor Alpay**  
300619368  

# Assignment 4

## Exercise 4

## Question 4

![fig1](./fig1.png)

### Average speed of the worm

```math
\bar{v} = \frac{2-1}{6} = \bold{0.16m/s}
```

## Exercise 5

### Question 4

- Constant acceleration $`a = \frac{1m}{sec^2}`$

#### Speed after 5 seconds

```math
\begin{aligned}
&v(t) = v_0 + a \cdot t \\
&v(t=5sec) = 0 + 1 \cdot 5 = \bold{5m/sec}
\end{aligned}
```

#### Distance traveled by the car after 5 seconds

```math
\begin{aligned}
&x(t) = x_0 + v_0 \cdot t + \frac{1}{2} a \cdot t^2 \\
&x(t=5sec) = 0 + 0 + \frac{1}{2} 1 \cdot 5^2 = \bold{12.5m/s}
\end{aligned}
```

#### Average speed after 5 seconds

```math
\bar{v} = \frac{12.5}{5} = \bold{2.5m/s}
```

#### What is the distance traveled until the car reached the speed 10m/s

```math
\begin{aligned}
&x(t) = x_0 + \frac{v^2 - v_0^2}{2\cdot a} \\
&x(t=10sec) = 0 + \frac{10^2 - 0^2}{2 \cdot 1} = \bold{50m}
\end{aligned}
```

## Exercise 5*

### Question 1

- Car acceleration: $`a = \frac{2m}{sec^2}`$
- Truck's constant speed: $`v = \frac{9m}{sec}`$

#### How long does the car need to accelerate in order to catch up to the truck.

- Equation for the car's location:

```math
\begin{aligned}
&x(t) = x_0 + v_0 \cdot t + \frac{1}{2} a \cdot t^2 \\
&x(t)_{car} = 0 + 0 + \frac{1}{2} \cdot 2 \cdot t^2 \\
&x(t)_{car} = t^2
\end{aligned}
```

- Equation for the truck's location:

```math
\begin{aligned}
&x(t) =  v \cdot t \\
&x(t)_{truck} = 9t
\end{aligned}
```
- The car will reach the truck at:

```math
\begin{aligned}
&x(t)_{car} = x(t)_{truck} \\
&t^2 = 9t \\
&\bold{t = 9sec}
\end{aligned}
```

### Question 4

- Train1 speed: $`v_1 = 90km/h= 25m/s`$
- Train2 speed: $`v_2 = 126km/h= 35m/s`$
- Decelaration for the trains: $`a = -1m/sec^2`$

#### Will the trains collide?

- Train1 will stop at:

```math
\begin{aligned}
&x = x_0 + \frac{v^2 - v_0^2}{2a} \\
&x_{train1} = 0 + \frac{0^2 - 25^2}{-2} \\
&\bold{x_{train1} = 312.5m}
\end{aligned}
```

- Train2 will stop at:

```math
\begin{aligned}
&x = x_0 + \frac{v^2 - v_0^2}{2a} \\
&x_{train2} = 0 + \frac{0^2 - 35^2}{-2} \\
&\bold{x_{train2} = 612.5m}
\end{aligned}
```

- The trains have 1km=1000m to stop:

```math
312.5 + 612.5 = 925m \Longrightarrow \bold{925m<1000m} \Longrightarrow \text{No collision!}
```
