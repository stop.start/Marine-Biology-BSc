**Elinor Alpay**  
300619368

# Physics/Mecanics - Assignment 2

## Question 1

![fig1](./fig1.png)

1. Forces on the load:

```math
\Sigma F_y = 0 \Longrightarrow 
\left \{\begin{aligned}
&T_C - W = 0\\
&T_C - 98N = 0\\
&\bold{T_C = 98N}
\end{aligned}
\right .
```
2. Forces on the friction point (where A, B and C cords meet)

    From the drawings below we deduced the angles used to calculate $`T_A`$ and $`T_B`$:
  
|                     |                     |
|---------------------|---------------------|
| ![fig2](./fig2.png) | ![fig3](./fig3.png) |
  
```math
\left \{ 
\begin{aligned}
&\Sigma F_x = 0 \\
&\Sigma F_y = 0 
\end{aligned}
\right .

\Longrightarrow

\left \{
\begin{aligned}
& -T_A \cos{30\degree} + T_B \cos{45\degree} = 0 \\
& -T_A \sin{30\degree} + T_B \sin{45\degree} - T_C = 0
\end{aligned}
\right .

\Longrightarrow

\left \{
\begin{aligned}
&T_B = \frac{0.866 \cdot T_A}{0.707} = 1.225 \cdot T_A\\
& -0.5 \cdot T_A  + 0.707 \cdot T_B = 98N
\end{aligned}
\right .

\Longrightarrow

\left \{
\begin{aligned}
&T_B = 1.225 \cdot T_A\\
& -0.5 \cdot T_A  + 0.866 \cdot T_A = 98N
\end{aligned}
\right . 
```

```math
\left \{
\begin{aligned}
&\bold{T_A = \frac{98N}{0.366} = 267.760N}\\
&\bold{T_B = 1.225 \cdot 267.760N = 328.006N}
\end{aligned}
\right . 
```

## Question 2

![fig4](./fig4.png)

```math
\left \{ 
\begin{aligned}
&\Sigma F_{ALL x} = 0 \\
&\Sigma F_{ALL y} = 0 
\end{aligned}
\right .

\Longrightarrow

\left \{ 
\begin{aligned}
&F_x - f_k = 0 \\
&F_y + N - W = 0 
\end{aligned}
\right .

\Longrightarrow

\left \{ 
\begin{aligned}
&100 \cdot cos{30\degree} - \mu_{k} \cdot N = 0 \\
&100 \cdot sin{30\degree} + N - 490 = 0 
\end{aligned}
\right .

\Longrightarrow

\left \{ 
\begin{aligned}
&86.6 - \mu_{k} \cdot N = 0 \\
&N = 490 - 50 = 440N
\end{aligned}
\right .
```

```math
\bold{\mu_{k}} = \frac{86.6}{N} = \frac{86.6}{440} = \bold{0.197}
```
