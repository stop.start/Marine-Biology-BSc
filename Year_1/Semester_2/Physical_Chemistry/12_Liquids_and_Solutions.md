# Liquids and Solutions

## Liquids

### Cohesion and Adhesion

- **Cohesion** is the pulling forces between the molecules of a matter.
- **Adhesion** is the pulling forces between different molecules: between molecules of a matter and a surface/tool.
- If the adhesion forces are greater than the cohesion forces a **concave meniscus** will form.
- If the cohesion forces are greater than the adhesion forces a **conflex meniscus** will form.

![meniscus](./img/12_Liquids_and_Solutions/meniscus.png)
![meniscus](./img/12_Liquids_and_Solutions/meniscus2.png)

- The **cohesion** in the water (the force making it sticking to itself) comes frome the **hydrogen bonds**.
- **Cohesion** is what makes the water not overflowing immediately when pouring it in the glass up to the top and a bit more.
- When put in a glass recipient, we can see that the surface isn't flat but forms a **concave meniscus**. It is due to the **adhesion** which is the force that causes the water molecules to be attracted to the glass. 
- Because Glass is polar and its molecules contains Oxygen (O) as well as Silicon (Si) which is a lot less **electronegative** than Hydrogen. This causes the Hydrogen in the water to be attracted to the Oxygen in the glass which has a **stronger partial charge** since the Silicon is less electronegative than the Hydrogen. The Oxygen in the water is attraced to the Silicon in the glass.
- If we take a thin enough glass tube (or tube made of another polar material) then we can see the water "climbing" (despite gravity).  Between the cohesion within the water and the adhesive forces between the water and tube the water molecules are "jumping" up the tube and sticking to it. It's called **capillary action** (נימיות).
- Water has a high capillary action which allows it to rise high in glass tubes, but also in **plants**.
- The plants rely on this process to get water and nutrients from the root through the stem.

### Surface tension

- Molecules of a liquid, for example water, are always attracted to one another (**cohesion**) thus flowing between each other. But the molecules at the surface have no forces pulling them up (if only air is present). This causes the molecules at the surface to be more densed and closer to each other, and so their (Hydrogen) bond is stronger. This is called **surface tension**.
- The **surface tension** is what allows some denser "things" to be on the surface and not sink (unless applied pressure on them).
- It is also what makes, for example water, form small drops on some surfaces because the molecules at the surface are only pulled by the molecules inside.
- The molecules at the center of the liquid have a lower energy than the ones at the surface. That's why more molecules are "inside" and less at the surface.
- Mercury has the highest surface tension of all the known liquids and it is the only metal found in liquid state at room temperature.

_Note: **surface tension is based on cohesion**, so if we replace water with oil which is made of hydrocarbons which are non-polar, then it would have a lot less surface tension because the molecules are a lot less attracted to each other_  

### Vaporization and Condensation

- The molecules **kinetic energy** of a liquid changes when colliding with other molecules.
- The overall kinetic energy of the liquid's molecules behave according to the **Maxwell-Boltzmann distribution**

![maxwell-boltzmann distribution](./img/12_Liquids_and_Solutions/max_boltz.png)

#### Vaporization

- With enough energy, molecules will leave the liquid (vaporization). Their kinetic energy is converted to potential energy of breaking bonds.
- Because the molecules with higher kinetic energy leave the liquid, its overall kinetic energy decreases $`\rightarrow`$ its overall temperature decreases.
- Vaporization happens in all temperatures: in an open system in order to equalize the temperatures, heat will go from the surroundings to the liquid and some of its molecules will leave (evaporate).
- Increasing the temperature accelerate the vaporization process.
- A bigger surface area increases the vaporization process.
- **Enthalpy of vaporization ($`\Delta H_{vap}`$)** is the quantity of heat required in order to **evaporate 1 mol** of liquid at a given temperature.
- Gaz molecules that loses their energy change back to their liquid state (condensation). It is an **exothermic** process.

#### Vapor Pressure

- In a closed system, there's a dynamic equilibrium between the molecules evaporating and the ones condensing.
- The **vapor pressure** is the pressure from the the vapors that are in equilibrium with the liquid.
- The higher the temperature the higher the pressure.

#### Boiling Point

- The **boiling point** is the temperature where the vapor pressure of the liquid equals the external pressure.
- The **normal boiling point** is for an external pressure of 1atm.
- If the pressure is under 1atm the boiling point of water will be under $`100 \degree C`$ (vacum distillation).
- If the pressure is over 1atm the boiling point of water will be over $`100 \degree C`$ (pressure cooker).
- **Humidity** is the the vaporization of water at room temperature.

![boiling point](./img/12_Liquids_and_Solutions/bp.png)

#### Sublimation - המראה and Deposition - הנחתה

- **Sublimation**: change of state from solid to gaz.
- **Deposition**: change of state from gaz to solid.

```math
\Delta H_{sub} = \Delta H_{fusion} + \Delta H_{vap} = - \Delta H_{deposition}
```

- **Phase diagram**, for a specific matter, is the **pressure as a function of temperature** and shows the conditions for equilibrium between states (dots on the graph) and for each state when it the most stable.
- The diagram divides the graph surface into 3 for each state: solid, liquid, gaz.
- The dots are the equilibrium points between 2 or 3 states.
- For example: at $`-78.3 \degree C`$ and 1atm $`CO_2`$ will go through sublimation: from solid to liquid.
- We can see that at 1atm $`CO_2`$ cannot be liquid. That's why it is called **dry ice**.
- Water, at 5mmHg and $`0.0075 \degree C`$ will be in equilibrium for the 3 states.
- Point D in water: **supercritical fluid (SCF)** is any substance at a temperature and pressure where distinct liquid and gas phases do not exist because it is so hot that it "wants" to be a gaz but some much pressure that it "wants" to be a liquid.
- On the water diagram we can see the **anomaly**: more pressure will cause it to be liquid and not solid.

| $`H_2O`$                                       | $`CO_2`$                                       |
|------------------------------------------------|------------------------------------------------|
| ![h2o](./img/12_Liquids_and_Solutions/h2o.png) | ![co2](./img/12_Liquids_and_Solutions/co2.png) |


## Solutions

### How dissolving some non-volatile substance affects a volatile solution?

- We use idea solutions.
- **Ideal solutions** suppositions: 
  - Pulling forces between molecules in solutions are the same for all solutions (pulling force between water molecules = pulling force between ethanol molecules = etc..) thus the pulling force between molecules of 2 substances is the same as within those substances. 
  - Mixing 2 solutions A and B results in a solution of volume V = Va + Vb
  - $`\Delta H = 0`$ (real reactions are either exothermic or endothermic).
- **Colligative properties** are porperties such as freezing point or boiling point. Here we'll look at:
  - Vapor pressure.
  - Freezing point.
  - Boiling point.
- For the soluble substances we're using only non-volatile substances (usually solids).
- When dissolving there will be changes in vapor pressure, freezing and boiling points. 
- We look at the **quantity of particles (concentration) of the dissolved substance and not the type of the substance** since it doesn't matter what substance disturbs the state of the solvent, the fact it that its molecules do disturb.
- **Molarity concentration** is based on volume ($`\text{Molarity (M)} = \frac{\text{Amount of solute (mol)}}{\text{Volume of solution (L)}}`$). The issue is that when changing the temperature the volume changes thus we can't use molarity here.
- **Molality concentration** is based on the mass of the solvent and not volume which is practical when playing with the temperature: $`\text{Molality (m)} = \frac{\text{Amount of solute (mol)}}{\text{Mass of solvent (Kg)}}`$.
- **Vapor pressure** is basically how many molecules manage to leave the liquid. 
  - When dissolving a substance in water, the molecules of the dissolved substance will prevent some of the water molecules to leave thus the **vapor pressure will decrease**.
  
![vapor pressure](./img/12_Liquids_and_Solutions/vapor_pressure.png)

#### Effect of solute on the solvent

- **Raoult's law**: **decrease of vapor pressure**: the more solute there is the more the vapor pressure decreases.
   - $`n_i`$ = moles of solvent
   - $`n_{total}`$ = total number of moles (solvent + solute)
   - $`P_i`$ = vapor pressure of the solution.
   - $`P^0_i`$ = vapor pressure of the pure solvent.
   
```math
\begin{aligned}
& P_i = P^0_i \cdot x_i  \\
& x_i = \frac{n_i}{n_{total}}
\end{aligned}
```

- **Increase of the boiling point** because the vapor pressure increases.
  - C(m) is the molality of the solute (number of moles of solute in 1000gr of pure solvent)
  - $`K_b`$ is the **ebulliosscopic constant** of the solvent.
  
```math
\Delta T_b = K_b \times C(m)
```

- The **freezing point is lower**.
  - C(m) is the molality of the solute (number of moles of solute in 1000gr of pure solvent).
  - $`K_f`$ is the **cryoscopic constant** of the solvent.
  
```math
\Delta T_f = -K_f \times C(m)
```
