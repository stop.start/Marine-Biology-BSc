# Chemical Kinetics

**Chemical Kinetics tells how fast a reaction will take.**  

## Intro

### What is the speed or rate of a chemical reaction?

- It is the number of moles disappearing of reactants per unit of time or the number of moles created of product per unit of time.
- Measured by **mol/time**. _Example mol/sec_.
- When the volume of the reaction doesn't change, the speed can also be measured in concentration per unit of time: **M/time**. _Example: M/sec_.
- The unit of time can be any unit (seconds, hours) depending on how fast the reaction happens.

### How do we measure the speed of a chemical reaction?

- Take one reactant or product and follow its change over time. It can be done several ways:
  - Gases can be measured by volume or pressure created per unit of time.
  - Some compounds have a color and then use a spectrophotometer to measure the how much light it absorbs.
  - In solution the electricity conduction can be measured.
 
### What influences the speed of a chemical reactions?

- Nature of the compounds themselve (energy required to create or break bonds).
- The concentration of the reactants (if the concentration is really low then it will take more time for them to **meet** each other).
- Temperature (the higher the temperature the faster the reaction).
- External factor like catalysts or inhibitors. 
 
## Reaction Rate with Concentration

- Following the concentration of reactants or products over time:

```math
\text{Reaction Rate} = \frac{\Delta[\;\;]}{\Delta t} \quad \text{where [ ] is the concentration of reactants or products}
```

- Because the reaction rate **changes over time** (usually becomes slower due the concentration of the reactants decreasing), the rate used is the **average reaction rate**.
- The **average speed** is the speed calculated over time while the **momentary speed** is the speed at a given time.

```math
\begin{aligned}
& aA + bB \longrightarrow cC + dD \\
& \text{Reaction rate} = \\
& \text{rate of disappearance of reactants } = -\frac{1}{a}\;\frac{\Delta[A]}{\Delta t} = -\frac{1}{b}\;\frac{\Delta[B]}{\Delta t}\\
& = \\
& \text{rate of the appearance of products } = \frac{1}{c}\;\frac{\Delta[C]}{\Delta t} = \frac{1}{d}\;\frac{\Delta[D]}{\Delta t}
\end{aligned}
```
- The concentration of each compound will follow the stoichiometric ratios.
- In order to use only positive numbers even whent the concentration decreases, "-" (minus) is put in front of the reactants $`\Delta`$ (for example concentration starts at 20 and ends at 5 then: -(5-20)=15).
- Example: $`NO`$ with $`O_2`$ making $`NO_2`$. The concentration of each compound follows the stoichiometric ratios which are 2:1:2 (or 1:0.5:1).

```math
\begin{aligned}
& 2NO_{(g)} + O_{2(g)} \longrightarrow 2NO_{2(g)}\\
& \text{Reaction Rate } = -\frac{\Delta[O_2]}{\Delta t} = -\frac{1}{2}\frac{\Delta[NO]}{\Delta t} = \frac{1}{2}\frac{\Delta[NO_2]}{\Delta t}
\end{aligned}
```

- **Molecularity** is the number of molecules that come together to react in a reaction and is equal to the sum of stoichiometric coefficients of reactants in this elementary reaction.
- The molecularity doesn't indicate anything on the reaction rate.
- **Order of reaction - סדר התגובה** is how the concentration of the reactants influences the reaction rate.
- Each reaction has a **rate constant - k -** which depends on the temperature and the reactants.

```math
\text{Rate of Reaction} = k[A]^m[B]^n \quad \text{where m and n do not depend on the stoichiometric ratios}
```

- The order (סדר) for a particular reactant is its exponent (m,n).
- The **overall order of reaction** = m + n + ...

#### Zero order reaction

- No matter how much of reactants there is, the reaction keeps the same speed.

```math
A \longrightarrow \text{product} \Rightarrow \text{rate} = k[A]^0 = k
```

- From integrals, we get that the **A's concentration at time t** equals:

```math
\begin{aligned}
&-\frac{d[a]}{dt} = k\\
&\bold{[A]_t = [A]_0 - kt}
\end{aligned}
```

- This is a straight line equation:

  ![zero graph](./img/03_Chemical_Kinetics/zero_graph.png)

#### First order reaction

```math
\begin{aligned}
& A \longrightarrow B \Rightarrow \text{rate} = k[A]\\
& A + B \xrightarrow[S_N1]{} C + D \Rightarrow \text{rate} = k[A] \quad \text{B concentration doesn't influence the reaction}\\
\end{aligned}
```

- From integrals, we get that the **A's concentration at time t** equals:

```math
\begin{aligned}
&\frac{d[a]}{dt} = -k[A] \\
&\bold{\ln[A]_t = -kt + \ln[A]_0}
\end{aligned}
```

- Ln is used in order to get a straight line:

  ![first graph](./img/03_Chemical_Kinetics/first_graph.png)

#### Second order reaction

```math
A + B \xrightarrow[S_N2]{} C + D \Rightarrow \text{rate} = k[A][B]\\
```

- Usually the order of reaction is deduced from experiments by using the **method of initial rates**:
  - First experiment: check the rate of reaction for initial concentrations of reactants
  - Second experiment: increase the concentration for one of the reactant and check how it affects the reaction rate.
  - Third experiment: increase the concentration of the other reactant and check how it affects the reaction rate.

$`A + B \longrightarrow C`$  

| Experiment | Concentration of B | Concentration of A | Reaction Rate (M/s)  |
|------------|--------------------|--------------------|----------------------|
| 1          | 0.1                | 0.1                | $`1.5\times10^{-5}`$ |
| 2          | 0.2                | 0.1                | $`3.0\times10^{-5}`$ |
| 3          | 0.2                | 0.2                | $`1.2\times10^{-4}`$ |

- When **increasing B's concentration by 2** the reaction rate is **twice as fast**. It is a **first order reaction** for B (x2 [B] $`\rightarrow`$ x2 rate $`\rightarrow`$ ratio: 1:1).
- When **increasing A's concentration by 2** the reaction rate is **4 times or $`\bold{2^2}`$ faster**. It is a **second order reaction**.
- The experiment is usually done a bit differently, with more steps, for example, by increasing more than once the concentration of each reactant.
- From those results it can be deduced that:

 ```math
 \bold{Rate = k \times [A]^2 \times [B]^1}
 ```
- From this the **overall order** of the reaction is the **sum of all orders**: First order + Second order = **Third order**

 ![order of reaction](./img/03_Chemical_Kinetics/order_reaction.png)

- From integrals, we get that the **A's concentration at time t** equals:

```math
\begin{aligned}
&\frac{d[A]}{dt} = -k[A]^2 \\
&\bold{\frac{1}{[A]_t} = kt + \frac{1}{[A]_0}}
\end{aligned}
```

- It is a straight line as well:

  ![second graph](./img/03_Chemical_Kinetics/second_graph.png)

- The **reaction rate** has always the same **unit: $`\Large\frac{Concentration}{time}`$**:
  - **Zero order**: $`Rate = k[A]^0 \rightarrow \Large\frac{concentration}{time} = \frac{concentration}{time} \normalsize \Rightarrow \text{k units} = \Large\frac{M}{s} `$
  - **First order**: $`Rate = k[A]^1 \rightarrow \Large\frac{concentration}{time} = \frac{1}{time} \normalsize \times concentration \Rightarrow \text{k units} = s^{-1}`$
  - **Second order**: $`Rate = k[A]^2 \rightarrow \Large\frac{concentration}{time} = \frac{1}{time \times concentration} \normalsize \times concentration^2 \Rightarrow \text{k units} = M^{-1}s^{-1}`$
- This gives us an indication about the constant k.

- When testing, the **rate law** can be deduced from drawing plots (or seeing a **linear change**) of each equation:

  ![testing rate law](./img/03_Chemical_Kinetics/test_rate.png)

## Half-Life

- **Time taken for one half of a reactant to be consumed**.
- It can also be defined as $`[A]_t = \frac{1}{2}[A]_0`$.
- How to get the half time for a first order reaction:

```math
\begin{aligned}
&\ln{\frac{\frac{1}{2}[A]_0}{[A]_0}} = -kt_{\frac{1}{2}}\\
&-\ln{2} = -kt_{\frac{1}{2}}\\
&t_{\frac{1}{2}} = \frac{\ln{2}}{k}
\end{aligned}
```

- **Note: if we're intersted in a quarter of the time or any other time lapse, we just have to change $`\bold{\frac{1}{2}}`$ to whatever we need (like $`\frac{1}{4}`$ for a quarter of the time)**.

### Time equation for each order

|Zero Order|First Order|Second Order|
|--|--|--|
|$`\bold{t_{\frac{1}{2}} = \frac{A_0}{2k}}`$|$`\bold{t_\frac{1}{2} = \frac{\ln{2}}{k}}`$|$`\bold{t_\frac{1}{2} = \frac{1}{A_0k}}`$|

## Collision Theory

- In order for the reaction to take place, the molecules of the reactants need to collide with minimum energy and in the right direction.
- That's why the **temperature** is important: the higher the temperature the higher kinetic energy.
- The **kinetic energy** is translated into **potential energy** of breaking bonds which has to happen in order to create new bonds.

### Activation Energy $`E_a`$ - אנרגיית השפעול

- Activation energy is the average kinetic energy that molecules must bring to their collisions for a chemical reaction to occur.
- Without this activation energy, even if the molecules collide in the right way nothing will happen because the electrons in the bonds repulse each other.
- This true for **endothermic** reactions as well as **exothermic** reactions.
- The activation energy doesn't change with temperature.
- High temperature increases the kinetic energy thus will make the reaction go faster because more molecules will have the required activation energy to make the reaction happen.
- When more than one reaction can occur at the same temperature, the reaction with the **higher activation energy** will be **slower**.

|||
|--|--|
|![activation energy](./img/03_Chemical_Kinetics/activation_energy.png)|![activation energy](./img/03_Chemical_Kinetics/activation_energy2.png)|

- The **constant k** depends on the temperature: the higher the temperature the bigger k is.

![k temperature example](./img/03_Chemical_Kinetics/k_temp_example.png)

## Transition State Theory

- The **activated complex** is a hypothetical species lying between reactants and products at a point on the reaction profile called the transition state:
  - When two molecules collide, they form a complex containing all the atoms together.
  - This complex can either go back to be the two original molecules or form new molecules.
  - This complex is between the reactants and product and is called **transition state**.
  
   ![transition state](./img/03_Chemical_Kinetics/transition_state.jpg)

## Effect of Temperature on Reaction Rate - Arrhenius equation

```math
\begin{aligned}
&\bold{k = Ae^{-E_a/RT}} \\
&\ln{k} = \frac{-E_a}{R} \cdot \frac{1}{T} + \ln{A}
\end{aligned}
```
- **A** is constant called the **frequency factor**. It is related to the frequency of collision between the molecules.
- **R** is the gas constant.
- **k increases when T increases**.
- **k decreases when $`\bold{E_a} increases`$**.

### The relation between k and the temperature

- If we look at the k ($`k_1`$ and $`k_2`$) for 2 different temperatures ($`T_1`$ and $`T_2`$) we can know how much the rate of the reaction will increase when increasing the temperature. The equation is:

```math
\ln{\frac{k_2}{k_1}} = \frac{-E_a}{R}\left(\frac{1}{T_2}-\frac{1}{T_1}\right)
```
![arrhenius plot](./img/03_Chemical_Kinetics/arrhenius_plot.png)

## Mechanisms of reactions

- The mechanism of a reaction is how it happens, what are the steps between the start and the end of the reaction.
- The steps in between are intermediate reactions called **elementary reactions**.
- The molecules that are used as reactants in one step and as product in the other (and vice versa) can be reduced. The main equation will contain only reactants and products appearing once on each side. Those molecules are usually short lived and are called **reaction intermediates - חמרי בניים**.
- The **slower step** is the one that will determine the **rate of the reaction** and is the one with the **higher activation energy**.
- _Example:_

```math
\begin{aligned}
&NO_{2(g)} + CO_{(g)} \longrightarrow NO_{(g)} + CO_{2(g)}\\ 
\text{This reaction happens in 2 steps:} \\
&\bold{NO_{2(g)}} + \cancel{NO_{2(g)}} \longrightarrow \cancel{NO_{3(g)}} + \bold{NO_{(g)}}\\
&\cancel{NO_{3(g)}} + \bold{CO_{(g)}} \longrightarrow \cancel{NO_{2(g)}} + \bold{CO_{2(g)}}\\ 
\end{aligned}
```

### Rate for the basic reactions

- Basic reactions (תגובות יסוד) happen with only 1 or 2 molecules (really rare to see a basic reaction with more molecules).
- The **molecularity** is the number of reactant molecules at the basic step.
- With basic reactions (and **only with basic reactions**) the **rate equation** can be deduced from the stoichiometric ratios (if the ration isn't 1:1 then it might not be a basic reaction):

```math
\begin{aligned}
& A \xrightarrow{k} B + C \quad\quad Rate = k[a] \\
& A + B \xrightarrow{k'} C \quad\quad Rate = k'[A][B] \\
\end{aligned}
```

### Rate of the limiting step

- The slower step controls the rate of the reaction.
- Slow/fast reaction is an indication of the amount of activation energy required.
- Several mechanism seen in class:
  - Slow step followed by fast step.
  - Fast reversible step followed by slow step.

#### Example

- Slow step followed by fast step:

```math
\begin{aligned}
&\text{Reaction:}\\
&\bold{H_{2(g)} + 2ICl_{(g)} \longrightarrow I_{2(g)} + 2HCl_{(g)}}\\
&\text{From experiements it has been found that:}\\
&\text{Rate} = \frac{d[P]}{dt} = k[H_2][ICl]\\
\\
&\text{Elementary reactions for this reaction:}\\
&H_{2(g)} + ICl_{(g)} \xrightarrow{slow} \cancel{HI_{(g)}} + HCl_{(g)} \quad \Rightarrow \quad \text{Rate} = \frac{d[HI]}{dt} = k[H_2][ICl]\\
&\cancel{HI_{(g)}} + ICl_{(g)} \xrightarrow{fast} I_{2(g)} + HCl_{(g)} \quad \Rightarrow \quad \xcancel{\text{Rate} = \frac{d[I_2]}{dt} = k[HI][ICl]}\\
\\
&\text{This shows that the slow step indeed determines the rate of the reaction.}
\end{aligned}
```

![2 steps](./img/03_Chemical_Kinetics/slow_fast.png)

### Equilibrium

- At equilibrium the reaction goes both ways.
- There are 2 k constants for each direction of the reaction.

```math
\begin{aligned}
\bold{A \xrightleftharpoons[k_{-1}]{k_1} B} \\
&\text{Rate of forward reaction} = k_1[A]\\
&\text{Rate of reverse reaction} = k_{-1}[B]\\
\\
&\text{At equilibrium:}\\
&k_1[A] = k_{-1}[B]\\
&\frac{[B]}{[A]} = \bold{K_{eq}} = \frac{k_1}{k_{-1}}
\end{aligned}
```

- A reaction at equilibrium can be an intermediate step.

#### Example

```math
\begin{aligned}
&\text{Reaction:}\\
&\bold{2NO_{(g)} + O_{2(g)} \longrightarrow 2NO_{2(g)}}\\
&\text{From experiements it has been found that:}\\
&\text{Rate} = k[NO]^2[O_2]\\
\\
&\text{Elementary reactions for this reaction:}\\
&\text{fast: } 2NO_{(g)} \xrightleftharpoons[k_1]{k_{-1}} \cancel{N_2O_{2(g)}} \\
&\text{slow: } \cancel{N_2O_{2(g)}} + O_{2(g)} \xrightarrow{k_2} 2NO_{2(g)}\\
\\
&\text{From what we learn the slow step determines the rate so the rate should be:}\\
&\text{Rate} = k_2[N_2O_2][O_2]\\
\end{aligned}
```

- If the rate found contains intermediate compounds, they should be expressed with the help of an other equation:

```math
\begin{aligned}
&K_1 = \frac{k_1}{k_{-1}}=\frac{[N_2O_2]}{[NO]^2}\\
&[N_2O_2] = \frac{k_1}{k_{-1}}[NO]^2 = K_1[NO]^2\\
&\text{Take this and place it in the rate found earlier:}\\
&\textbf{Rate} = k_2[N_2O_2][O_2] = k_2\frac{k_1}{k_{-1}}[NO]^2[O_2] = \bold{k_2 \cdot K_{eq} \cdot [NO]^2[O_2]}\\
&k_2 \; \text{ and } \; K_{eq} \; \text{ being just constants we can say that: }\\
&k_2 \cdot K_{eq} \cdot [NO]^2[O_2] = \bold{k \cdot [NO]^2[O_2]}
\end{aligned}
```

## Catalyst - זרז

- Catalysts speed up chemical reactions:
  - Change the itinery of the reacion (for example: adds a step but with lower activation energy).
  - Decrease the required activation energy.
  - Increase the effectiveness of the collision between molecules.

![catalyst](./img/03_Chemical_Kinetics/catalyst.png)

![enzyme](./img/03_Chemical_Kinetics/enzyme.png)

- 2 types of catalysts:
  - **Homogenic catalyst**: compound which is in the same state as the compound reacting.
  - **Heterogenic catalyst**: compound which is not in the same state as the compound reacting.
