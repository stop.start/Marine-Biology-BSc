# Enthalpy

## Extensive and Intensive Properties

- **Extensive properties** depend on the size of the system, i.e. the amount of matter being measured (e.g. mass, volume, length). The bigger the system the bigger the sizes of those properties.
- **Intensive properties** are independant from the system: temperature, pressure, density. No matter the size of the system they stay the same.
- **Specific heat - קיבול חום סגולי** is the amount of heat per unit mass required to raise the temperature by one degree Celsius.

### Isochoric process

- An **isochoric process** is a process at **constant volume**.
- Because the volume is constant there's **no work** since work depends on the **path**.
- From the **first law of thermodynamics**:

```math
\begin{aligned}
& \Delta E = q + W\\
& \Delta E = q + 0\\
&\bold{\Delta E = q_v}
\end{aligned}
```
- If the reaction **releases heat** (exothermic), $`q_v < 0`$ thus $`\Delta E < 0`$.
- If the reaction **absorbs heat** (endothermic), $`q_v > 0`$ thus $`\Delta E > 0`$.

### Isobaric process

- Most processes are done at **constant pressure** which will create work (volume can increase or decrease).
- $`\Delta H`$ is the heat for processes at constant pressure.
- **H** is called **enthalpy** and is defined by **state functions** which makes it a state function:
  - E: energy of the system
  - P: pressure
  - V: volume
  
```math
H = E + PV
```

- Because H is a state function we can calculate:

```math
\begin{aligned}
& \Delta H = \Delta E + \Delta(PV)\\
& \text{Because P is constant:}\\
& \bold{\Delta H = \Delta E + P\Delta V}
\end{aligned}
```
- By using the **first law of thermodynamics**:

```math
\begin{aligned}
& \Delta H = \Delta E + P\Delta V\\
& \Delta E = q + W\\
& \Delta H = q_p + W + P\Delta V\\
& \text{At constant pressure: } W = -P\Delta V\\
& \Delta H = q_v -P\Delta V + P\Delta V\\
& \bold{\Delta H = q_p}
\end{aligned}
```
- If the reaction **releases heat** (exothermic), $`q_p < 0`$ thus $`\Delta H < 0`$.
- If the reaction **absorbs heat** (endothermic), $`q_p > 0`$ thus $`\Delta H > 0`$.

#### Difference between $`\Delta H`$ and $`\Delta E`$

- For **liquids and solids**, the change of volume in reactions is usually negligible thus:

```math
\begin{aligned}
& \Delta H = \Delta E + \Delta (PV) \quad \text{pressure is constant}\\
& \bold{\Delta H \text{\textasciitilde} \Delta E}
\end{aligned}
```

- Reactions with gas $`\Delta H`$ and $`\Delta E`$ are different because the volume will change.
- With **ideal gases** we know that **$`PV = nRT \Rightarrow \Delta (PV) = \Delta (nRT)`$**:

```math
\begin{aligned}
& \Delta H = \Delta E + \Delta (PV) \\
& \text{At constant temperature: }\\
& \Delta (PV) = \Delta n \cdot RT \Delta\text{ n is the change in mols of the ideal gas}\\
& \bold{\Delta H = \Delta E + \Delta nRT} \Longrightarrow \Delta H \text{ depends on the number of moles.}
\end{aligned}
```

- _Notes: 
1. The system will release heat thus has a $`\Delta H`$ but the temperature of the surroundings won't change because the amount of heat released is negligible compared to the size of the surroundings.
2. The temperature of the system doesn't change also because it goes back to its original temperature after releasing heat to the surroundings._ 


## More about $`\Delta H`$

- It is a **state function**: $`\Delta H \text{(reactants)} - \Delta H \text{(products)} `$
- It is the heat per mol of a matter.
- It is used when the pressure is constant.
- **Endothermic reaction: $`\bold{\Delta H > 0}`$**: the system gained heat.
- **Exothermic reaction: $`\bold{\Delta H < 0}`$**: the system lost heat.

![enthalpy](./img/06_Enthalpy/enthalpy.png)

- If the stoichiometric ratios changes (double, half, etc) then $`\Delta H`$ will change proportianally.

```math
\begin{aligned}
& N_2 + O_2 \longrightarrow 2NO \quad \Rightarrow \quad \Delta H = +180.50 kJ \\
& \frac{1}{2}N_2 + \frac{1}{2}O_2 \longrightarrow NO \quad \Rightarrow \quad \Delta H = +90.25 kJ
\end{aligned}
```

- If we reverse the reaction then $`\Delta H`$ will change sign.

```math
\begin{aligned}
& \frac{1}{2}N_2 + \frac{1}{2}O_2 \longrightarrow NO \quad \Rightarrow \quad \Delta H = +90.25 kJ \\
& NO \longrightarrow \frac{1}{2}N_2 + \frac{1}{2}O_2 \quad \Rightarrow \quad \Delta H = -90.25 kJ
\end{aligned}
```

### Four methods to calculate $`\Delta H`$

#### 1 - Experimentally

- By checking the surroundings temperature at the start and end of the experiment we can deduce the $`\Delta H`$.
- If the temperature of the surroundings (q) increase then the temperature of the system (H) decreases and vice versa.

```math
\bold{-q = \Delta H}
```

#### 2 - Hess's Law

- The change in enthalpy in a chemical reaction is independant of the pathway between the initial and final states.
- The total enthalpy change for the reaction si the sum of all changes of the intermediate reactions.

```math
\bold{\Delta H = \sum \Delta H_{products} - \sum \Delta H_{reactants}}
```

![hess's law](./img/06_Enthalpy/hess.png)

- Example of Hess's law:

![hess example](./img/06_Enthalpy/hess_example.png)

#### 3 - Bond enthalpy

- Can be written as $`\Delta H_d`$ where _D_ means **dissociation**. When talking about energy of ionization then **I (I1: first ionization, I2: second ionization)** will be written insead of D.
- The energy required in order to break 1 mol bonds.
- This energy is equal (but opposite in sign) to the energy released when a mol of bonds in created.
- **Defined for gases atoms**.
- The **length** of a bond is the minimum length after the bond is created and is measured in angstrom.
- The **energy** of a bond is the energy released/invested when forming/breaking a bond between 2 **gas** atoms. It is measured in kJ/mol or eV (electron-volt).
- **$`\bold{\Delta H}`$ is the difference between the energy of broken bonds and created bonds**.

```math
\begin{aligned}
& H_{2(g)} + Cl_{2(g)} \longrightarrow 2HCl_{(g)} \\
& H-H \; \text{and} Cl-Cl \; \text{broken} \\
& 2H-Cl \; \text{created}
& \bold{\Delta H = E_{H-H} + E_{Cl-Cl} - 2E_{H_Cl}} \\
& \Delta H = 436  + 242 - 2 \cdot 431 = -184kJ
\end{aligned}
```

#### 4 - $`\Delta H_f^0`$

- f for **formation**. 
- 0 means constant pressure of 1atm.
- Standard conditions (1atm 25$`\degree`$C).
- Reaction where we get **1 mole** (no more no less) of a mixture from elements in their **state at standard conditions**. _For example: formation of $`CO_2`$ from carbon at standard conditions  and oxygen at standard conditions.
- When writting the equation the product has to be 1mol so we need to adapt the stochiometric quotient to this.
- $`\Delta H_f^0`$ of elements at standard conditions (like $`O_{2(g)}`$) is 0 because nothing needs to be done in order to form them ($`O_2`$ at standard conditions is in a gas state).
- For reactions:

```math
\bold{\Delta H^0 = \sum \Delta H^0_{f(products)} - \sum \Delta H^0_{f(reactants)}}
```

- Example with the burning equation:

```math
\begin{aligned}
& CH_{4(g)} + 2O_{2(g)} \longrightarrow CO_{2(g)} + 2H_2O_{(l)}\\
& \textbf{ Because } \bold{H_2O} \textbf{ is liquid, the bond equation cannot be used, but } \bold{\Delta H} \textbf{ formation can.}\\
& \sum \Delta H^0_{f(products)} = -393.5 (CO_2) -285.9 \times 2 \text{ ( } 2H_2O \Rightarrow \times 2 \text{ because we get 2 moles of water)}\\
& \sum \Delta H^0_{f(reactants)} = -74.8 (CH_4) + 0 \times 2 (2O_2)\\
& \Delta H = \sum \Delta H^0_{f(products)} - \sum \Delta H^0_{f(reactants)}\\
& \Delta H = -393.5 - 285.9 \times 2 + 74.8 = -890.5kJ
\end{aligned}
```

### Summary

![summary](./img/06_Enthalpy/summary.png)
