# Electrochemistry

## Galvanic Cell

- Some metals will oxidize more and some will get oxidize more.
- This means that a metal at solid state in a solution of ions of another metal can cause a reaction but the other way round might not react.
- A metal in solid state at has an oxidation number of 0. As soon as it gets oxidized the oxidation number increases.
- A metal at solid state in a solution of ions from the same metal is called an **electrode**.
- Electrons aren't be free in the solution but on the electrode.
- If the metal has a tendency to get oxidized then it will dissolve and free ions in the solution and the electrons will be on the metal.
- Taking metal also in a solution of this same metal with a lower tendency to get oxidized, and connecting between them with an external conductive system (metal string). This will cause this metal to get "bigger".
- This causes a **difference of potentials**.
- An electrode in a solution of ions of the same metal is called **half cell**.
- Electrons will go from one half cell (the one with the most electrons) to the other through the conductive part: electric current.
- The ions of the solutions are from a dissolved salt. The negative ions aren't part of the electric current created.
- **Anode**: electrode where the oxidation takes place.
- **Catode**: electrode where the reduction takes place.
- When drawing usually the anode is on the left.

![electrodes](./img/10_Electrochemistry/electrodes.png)

- During the reaction in the anode the number of ions increases more and more and in the catode and they decrease more and more.
- At some point the electric current will stop. In the anode all the positive ions will react with the electrons and in the catode there won't be anymore ions (?).
- In order to keep the charges in both electrodes, a **salt bridge** is used which works by **diffusion**.

![electrodes example](./img/10_Electrochemistry/electrodes_example.png)

- This is called and **electrochemical cell** or **galvanic cell**. By definition: cell that uses a spontaneous reaction in order to create an electric current.
- **Electrolytic cell**: cell with an electric current from an external source causing a non-sponteneous reaction to happen.
- Writing of a galvanic cell from the reaction: $`Zn_{(s)} + Cu^{2+}_{(aq)} \rightarrow Zn^{2+}_{(aq)} + Cu_{(s)}`$
  - One the left the anode (where the oxidation takes place).
  - One the right the catode (where the reduction takes place).
  - One vertical line: differentiate between the states in the electrode (solid/aquaeous(ions)). Because it's obvious that ions are in aquaeous form the "(aq)" can be not written.
  - Double vertical line: differentiate between the electrodes (salt bridge).
  
```math
Zn_{(s)}|Zn^{(2+)} \; (1M)||Cu^{2+} \; (1M) | Cu_{(s)}
```

![writing](./img/10_Electrochemistry/writing.png)

- $`E^0`$ is the electric potential of a half cell which is the tendency of the matter to receive electrons.
- It cannot be measured directly thus it is measured by comparing to a half cell with a $`E^0=0`$ or $`\epsilon^0`$.
- Table of **reduction potentials** contains all the $`\epsilon^0`$ that were tested against this half cell of $`E^0=0`$/$`\epsilon^0`$ at $`25\degree C`$.
- **Standard conditions**:
  - Concentration: 1M
  - Pressure: 1atm
  - Temperature: $`25\degree C`$
- An **inerte electrode** is an electrode that doesn't do any reaction but allows for electrons to come (from the other electrode).
- This half cell contains **hydrogen**. Because it is not a metal, platina is used with it.

![measuring](./img/10_Electrochemistry/measuring.png)

- When looking at 2 values (metal against hydrogen base) from the reduction potentials table (or by testing), and connecting those 2 metals as electrodes, the one with the higher reduction potential will stay the same (in the example above Cu)
- The electric power of the cell is calculated by:

```math
\epsilon^0_{cell} = \epsilon^0_{cathode} - \epsilon^0_{anode}
```

- A spontaneous reaction: $`\epsilon_{cell} > 0`$
- The reverse reaction of a non-spontaneous reaction ($`\epsilon_{cell} < 0`$) is sponteneous and they have the same $`\epsilon^0`$ with inverse sign.
- Sometimes the half-reactions need to be balanced (multiply, divided) but the $`\epsilon^0`$ never changes.
