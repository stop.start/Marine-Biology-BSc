# Physical Chemistry


1. [Acids and Bases](./01_Acids_Bases.md)
2. [Insoluble Salts](./02_Insoluble_Salts.md)
3. [Chemical Kinetics](./03_Chemical_Kinetics.md)
4. [Thermodynamics](./04_Thermodynamics.md)
5. [Reversible and Irreversible Reactions](./05_IrReversible_Reactions.md)
6. [Enthalpy](./06_Enthalpy.md)
7. [Entropy](./07_Entropy.md)
8. [Gibbs Free Energy](./08_Gibbs_Free_Energy.md)
9. [Redox Reactions](./09_Redox.md)
10. [Electrochemistry](./10_Electrochemistry.md)
11. [Nernst Equation](./11_Nernst_Equation.md)
12. [Liquids and Solutions](./12_Liquids_and_Solutions.md)
