# Nernst Equation


## Intro

- In electrochemistry, until now all the equations were for 1M of everything and 1atm.
- With **different concentrations** (non standard conditions) the electric power will be different. The Nernst equations allows to calculate the difference of potential in the cell in those non-standard conditions.
- Calculating $`\Delta G`$ for a cell:
  - **n**: number of moles of electrons in **balanced** redox equation
  - **F**: Faraday constant = 96,485 coulombs/mol $`e^-`$
  
```math
\Delta G = -nF\epsilon
```

- Calculating $`\Delta G^0`$ for a cell:
  - **n**: moles of electrons in **balanced** redox equation
  - **F**: Faraday constant = 96,485 coulombs/mol $`e^-`$
  
```math
\Delta G^0 = -nF\epsilon^0
```

- If $`\Delta G^0 < 0`$ then it is a spontaneous reaction.

## Nernst Equation

- Getting to the Nernst equation:
  - **R** = 8.314 J/(mol*K)
  - **T** in K
  - **n**: moles of electrons in **balanced** redox equation
  - **F**: Faraday constant = 96,485 coulombs/mol $`e^-`$
  
```math
\begin{aligned}

& Q = \frac{[C]^c[D]^d}{[A]^a[B]^b} \\
& \Delta G = \Delta G^0 + RT\ln{(Q)} \\
& \Delta G = -nF\epsilon_{cell} \\
& -nF\epsilon_{cell} = -nF\epsilon^0_{cell} + RT\ln{(Q)} \\\\
& \text{From this we can the } \textbf{Nernst equation:} \\
& \boxed{\bold{\epsilon = \epsilon^0 - \frac{RT}{nF}\ln{(Q)}}} \\

\end{aligned}
```

- At $`25 \degree C = 298 \degree K`$, the equation is simplified:
  - **R** = 8.314 J/(mol*K)
  - **T** = $`298 \degree K`$
  - **F** = 96,485 coulombs/mol $`e^-`$
  - **Multiply by 2.303 to convert from ln to log**
  
```math
\boxed{\bold{\epsilon = \epsilon^0 - \frac{0.059}{n} \log{(Q)}}}
```

- Given a non-spontaneous reaction (at standard conditions: $`\epsilon^0 < 0`$), changing the the concentrations (thus the ratio Q) can cause the reaction to be spontaneous.

### Example

```math
Pt|Fe^{2+}(0.10M),Fe^{3+}(0.20M)\|Ag^+(1.0M)|Ag_{(s)}
```

![cell example](./img/11_Nernst_Equation/cell_example.png)

- From the **standard reduction potentials table**:

```math
\begin{aligned}

& Ag^+_{(aq)} + e^- \rightarrow Ag_{(s)} \quad \epsilon^0 = 0.799V \\
& Fe^{3+}_{(aq)} + e^- \rightarrow Fe^{2+}_{(aq)} \quad \epsilon^0 = 0.77V \\
& \text{In the anode there is an oxidation reaction and not reduction thus the above reaction needs to be inversed:} \\
& Fe^{2+}_{(aq)} \rightarrow Fe^{3+}_{(aq)} + e^- \quad \epsilon^0 = -0.77V \\
& \text{-------------------------------------------------------} \\
& Fe^{2+}_{(aq)} + Ag^+_{(aq)} \rightarrow Fe^{3+}_{(aq)} + Ag_{(aq)} \\
& \epsilon^0_{cell} = 0.799V - 0.77V = 0.029V \\

\end{aligned}
```

- Then, using the simplified Nernst equation:
  - In this specific reaction equation all the stochiometric quotient are equal to 1.
  - There is 1 mol of electrons being balanced.
  
```math
\begin{aligned}

& \epsilon_{cell} = \epsilon^0_{cell} - \frac{0.059}{n} \log{(Q)}\\
& \epsilon_{cell} = 0.029 - \frac{0.059}{1} \; \log{\frac{[Fe^{3+}]}{[Fe{2+}][Ag^+]}} \\
& \epsilon_{cell} = 0.029 - \frac{0.059}{1} \; \log{\frac{0.2}{0.1 \times 1}} \\
& \bold{\epsilon_{cell} = 0.029 - 0.018 = 0.011V } \\

\end{aligned}
```

- **If we change the stoichiometric quotients by multiplying or divising the cell potential stays the same!**

### Equilibrium

- At equilibrium $`Q = K_{eq}`$.
- At equilibrium _"the battery is dead"_.
- The cell potential, $`\epsilon`$, is 0V.

```math
\begin{aligned}
& \epsilon = \epsilon^0 - \frac{0.059}{n} \log{(K_{eq})}\\
& 0 = \epsilon^0 - \frac{0.059}{n} \log{(K_{eq})}\\
& \boxed{\bold{\epsilon^0 = \frac{0.059}{n} \log{(K_{eq})}}}
\end{aligned}
```


### Summary

![summary](./img/11_Nernst_Equation/summary.png)

## Concentration Cell

- Electric power can be created from concentration differences even if the difference of standard potentials of the cell is 0 ($`\Delta\epsilon^0=0`$).
- 2 half cells with difference in **ion concentrations** with produce **spontaneously** electric power in order to equilibrate the concentrations.
- This means that 2 half cells with the same ions can produce electric power if the concentrations are different.
- The half cell with the **lower concentration is the anode** and the one with the **higher concentration is the cathode**.
- The **salt bridge** function is to regulate the concentration of ions in the solutions and keep the solutions neutral (charge=0):
  - In the anode there are more and more ions because of the oxidation. Too many positive ions will stop the anode from functioning properly.
  - In the cathode there are less and less ions because of the reduction. Not enough ions will stop the cathode from functioning properly.
- [More explaination for the salt bridge](https://youtu.be/VKcK5qKKH_c)

![concentration cell](./img/11_Nernst_Equation/concentration_cell.png)


## Corrosion

- See the presentation (ppt 12 - slide 32 and above)

