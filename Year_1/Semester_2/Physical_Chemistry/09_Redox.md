# Redox Reactions

## Intro

- Reduction/Oxidation.
- Transfer of electrons.
- Oxidation - from oxygen that knows to take electrons (very electronegative) $`rightarrow`$ atom that **oxidizes** (**מחמצן**) will **receive** electrons from someone else that gives the electrons (**reduces** - in hebrew **מחזר**).
- Reduction - gives electrons.
- Oxidation and reduction always come together: one receives electrons (oxidation) and one gives electrons (reduction).
- **OIL RIG**:

![oil rig](./img/09_Redox/oil_rig.png)

- Example: $`Mg_{(s)} + 0.5 O_{2(g)} \rightarrow MgO_{(s)} \Longrightarrow Mg^{+2} + O^{-2}`$
- Magnesium gave 2 electrons to the oxygen, it formed an ionic compound.
- Redox reactions are not only for ionic compounds. When there's a **change in the distribution of the negative charge** in molecules, it is also considered redox reactions: $`H_{2(g)} + 0.5 O_{2(g)} \rightarrow H_2O_{(g)}`$
- In the above reaction there are no ions but the new covalent bonds are now polar.

## Oxidation Numbers

- Oxidation numbers are between -8/8 but usually between -7/7.
- The oxidation numbers for a specific atoms depends on the molecule it is in.
- It depends on the difference of electronegativity:
  - Fluor has always an oxidation number of -1 because it is extremlu electronegative. Oxygen will usually have an O.N of -2 since it is also very electronegative. Other atoms can have a + or - O.N depending on the atoms they are bonded to.
  
### Rules

1. Oxidation number of elements ($`H_2, O_2, S_8, Mg`$) is 0.
2. In a neutral molecule the sum of the oxidation numbers is 0.
3. In a composed ion the oxidation number is equal to the charge of the ion ($`SO_4^{-2}`$ has an O.N of -2).
4. Ion composed of only 1 atom its O.N is equal to its charge ($`Cl^-`$ has an O.N of -1).
5. Alkali metals (1 column) have always an O.N of +1.
6. Alkaline Earth metals have always an O.N of +2.
7. [F]luor always has an O.N of -1.
8. [H]ydrogen always has an O.N of either -1 or +1:
  - Bonded to a non-metal: +1
  - Bonded to a metal: -1
9. Oxygen has an O.N of -2 unless it is bonded to fluor, then it has an O.N of +2 ($`F_2O`$)

#### Elements with multiple oxidation numbers

- Elements that can have several oxidation numbers will be named with the oxidation number in it:
  - $`KMnO_4`$: potassium manganate (VII)
  - $`NaClO_3`$: sodium cholorate (V)
  - $`K_2Cr_2O_7`$: poatassium dichromate (VI)

### Example

- In green: oxidation of the carbon. 
  - The reductor gives electrons: either an oxygen comes in or 2 hydrogens leave.
  - The oxidation number of the carbon increases.
- In blue: reduction of the carbon.
  - The oxidator receives electrons: either an oxygen leaves or 2 hydrogens come in.
  - The oxidation number of the carbon decreases.
  
![example](./img/09_Redox/example.png)

## Balancing Redox Equations

1. Write the oxidation numbers for all the atoms in the equation.
2. Identify the elements that undergo oxidation (oxidators and reductors).
3. Write 2 equations: one for the oxidation and one reduction.
4. Balance the electrons in the 2 equations with quotients on the elements.
5. Join the 2 equations (there's no electrons in the final equation).
6. Check the equation with the law of conservation of matter.

## TODO

2/30:00 / last powerpoint.