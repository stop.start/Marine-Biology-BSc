# Gibbs Free Energy

## $`\Delta G`$

- **Surroundings** are where we make observations on the energy transferred into or out of the system.
- 

```math
\begin{aligned}
& \Delta S = \frac{q}{T} \Longrightarrow \Delta S_{surr} = \frac{- \Delta H_{sys}}{T}\\
& \Delta S_{univ} = \Delta S_{surr} + \Delta S_{sys} \\
& \text{From this we get:}\\
& \Delta S_{univ} = \frac{- \Delta H_{sys}}{T} + \Delta S_{sys}\\
& -T \Delta S_{univ} = \Delta H_{sys} - T \Delta S_{sys} = \Delta G_{sys} = \text{change in Gibbs free energy}\\
& \bold{\Delta G_{sys} = \Delta H_{sys} - T \Delta S_{sys}}\\
& \text{At standard conditions: }\\
& \bold{\Delta G^0_{sys} = \Delta H^0_{sys} - T \Delta S^0_{sys}}
\end{aligned}
```


- The second law of thermodynamics:

```math
\begin{aligned}

& \Delta G_{sys} < 0 \Rightarrow \text{the process is spontaneous} \\
& \Delta G_{sys} = 0 \Rightarrow \text{the process is at equilibrium}\\
& \Delta G_{sys} > 0 \Rightarrow \text{the process is not spontaneous}

\end{aligned}
```

- $`\Delta G_{sys} > 0`$ not only means that the reaction won't happen spontaneously but also that the reverse reaction will happen spontaneously.
- **All spontaneous processes produce an increase in the entropy of the universe** (thus $`\Delta G < 0 \Rightarrow`$ increase in entropy of the universe).
- This energy can be used to do **work**.
- $`\Delta G_{sys}`$ of elements ($`H_2, O_2,`$ etc) is equal to 0.

- $`\Delta G_{sys}`$ is a **state function**. This means that it can also be calculated with:
- $`\Delta G_{sys}`$ is used with constant pressure
- $`\Delta G^0`$ is for pressure at 1atm (standard conditions). In this course we use only $`\Delta G^0`$.

```math
\Delta G^0 = \sum{\Delta G^0_{f(products)}} - \sum{\Delta G^0_{f(reactants)}}
```

![summary signs](./img/08_Gibbs_Free_Energy/delta_g_signs.png)

- $`\Delta G`$ can be shown as a graph:

![graph](./img/08_Gibbs_Free_Energy/g_graphs.png)


## $`\Delta G`$ and $`K_{eq}`$

- $`\Delta G^0_{rxn}`$ iis the change in free energy when pure reactants convert to pure products (reactants transform to products).
- Reactions favoring the creation of products: $`K_{eq} > 1`$.
- At non-standard conditions (pressure not 1atm):
  - Where Q in the reaction quotient ($`= K_{eq}`$ at equilibrium)

```math
\Delta G = \Delta G^0 + RT \ln{Q}
```

- Because at equilibrium $`\Delta G = 0`$ and $`Q = K_{eq}`$ we get:

```math
\begin{aligned}
& \Delta G^0 =  0 - RT \ln{K_{eq}} \\
& \bold{\Delta G^0 = - RT \ln{K_{eq}}} \\
& \bold{K_{eq} = e^{-(\frac{\Delta G^0}{RT})}}
\end{aligned}
```

- The higher $`\Delta S^0`$ the higher K.
- The lower $`\Delta H^0`$ the higher K.

- Having $`\Delta G^0`$ for a specific temperature allow to get $`K_{eq}`$.

### Activities

- _**Not for the exam.**_
- **Activity**: the real pressure or the real concentration. With ideal gases it equals the pressure and with ideal solutes it equals the concentration. In this course we don't work with real pressure/concentration (only ideal).
- Activities:
  - **Solid: a = 1**
  - **Ideal gases: a = P**
  - **Ideal solutes in aqueous solution: a = c (in $`mol L^{-1}`$)**

## $`\Delta G^0`$ and $`K_{eq}`$ as functions of temperature

```math
\begin{aligned}
& \Delta G^0 = \Delta H^0 - T\Delta S^0 \\
& \Delta G^0 = -RT\ln{K_{eq}} \\
& \ln{K_{eq}} = \frac{-\Delta G^0}{RT} = \frac{-\Delta H^0}{RT} + \frac{\cancel{T} \Delta S^0}{R\cancel{T}} \\
& \ln{K_{eq}} =  \frac{-\Delta H^0}{RT} + \frac{\Delta S^0}{R} \\
& \bold{\ln{\frac{K_{eq2}}{K_{eq1}}} = ... = \frac{-\Delta H^0}{R} \left( \frac{1}{T_2} - \frac{1}{T_1} \right)}
\end{aligned}
```

## Coupled Reactions

- Some reaction have a $`\Delta G^0 > 0`$.
- In order to make them happen is to use another reaction with a $`\Delta G^0_2 < - \Delta G^0_1`$ thus overall the $`\Delta G^0 < 0`$. This makes a non-spontaneous reaction happen spontaneously.
- This is called **coupled reactions**.
- The spontaneous reaction has a reactant which is a product of the non-spontaneous reaction. The spontaneous reaction uses this reactant (making it disappear) and because of Le Chatelier, the non-spontaneous reaction will happen.

![coupled reaction](./img/08_Gibbs_Free_Energy/coupled_reaction.png)

## Types of processes

- **Adiabatic**: There's no heat (q) transfer.
- **Isothermal**: At constant temperature (T).
- **Isobaric**: At constant pressure (P).
- **Isochoric**: At constant volume (V).

## Summary

```math
\Delta G^0 = \Delta H^0 -T\Delta S^0
```

|                |                                             |                                             |                                             |                                             |
|----------------|---------------------------------------------|---------------------------------------------|:-------------------------------------------:|:-------------------------------------------:|
| $`\Delta H^0`$ | < 0 - Exothermic                            | > 0 - Endothermic                           | < 0                                         | > 0                                         |
| $`\Delta S^0`$ | > 0 - Entropy increases                     | < 0 - Entropy decreases                     | < 0                                         | > 0                                         |
| $`\Delta G^0`$ | < 0 - Spontaneous                           | > 0 - Non spontaneous                       | < 0 at low temperature T                    | < 0 at high temperature T                   |
| Spontaneous?   | Yes                                         | No                                          | At low temperature (enthalpy driven)        | At high temperature (entropy driven)        |
|                | ![1](./img/08_Gibbs_Free_Energy/graph1.png) | ![2](./img/08_Gibbs_Free_Energy/graph2.png) | ![3](./img/08_Gibbs_Free_Energy/graph3.png) | ![4](./img/08_Gibbs_Free_Energy/graph4.png) |
