# Reversible and Irreversible Reactions

- Work is the transfer of energy between the system and its surroundings using some force.
- Work isn't a state function.
- Work can be either **reversible** or **irreversible**.
- For example if the spreading of a gas molecules go the exact same way in reverse of their compression it is a reversible process. If not, then it is an irreversible process meaning that the work when compressing and spreading is not equal.
- Usually reversible process are done with infinitisimal state functions changes.
- **The work in a reversible process is always bigger than in an irreversible process.**
- A reversible process requires an infinite number of steps thus it requires infinite time. Basically it is purely theoretical.
- Reversible processes, although theoretical, give us the upper limit of work that can be produced from a specific process and the minimum work quantity to invest.
- Reversible are **not** spontaneous.
- **Irreversible are spontaneous**.

## Irreversible process

```math
\boxed{\bold{ W = -P_{ex} \cdot \Delta V }}
```

## Reversible process

```math
\boxed{\bold{ W_{rev} = -nRT \ln{\frac{V_2}{V_1}} = -nRT \ln{\frac{P_1}{P_2}} }}
```

```math
\boxed{\bold{ \Delta E = 0 \Rightarrow q = -w }}
```
