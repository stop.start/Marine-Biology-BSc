# Acids and Bases

## Acid Base Neutralization Reaction

- In hebrew **תגובת סתירה**.
- An acid can be neutralized by adding a base to the solution (and vice-versa).
- The **result** of a neutralization reaction is **salt and water**.

```math
\text{Acid} + \text{Base} \longrightarrow \text{Salt} + \text{Water}
```

- Example: NaOH with HCl, the $`H^+`$ and $`OH^-`$ released from the molecules will form water leaving a salt - NaCl.

```math
\begin{aligned}
&HCl_{(aq)} + NaOH_{(aq)} \longrightarrow NaCl_{(aq)} + H_2O \\\\
&H^+_{(aq)} + Cl^-_{(aq)} + Na^+_{(aq)} + OH^-_{(aq)} \longrightarrow Na^+_{(aq)} + Cl^-_{(aq)} + H_2O \\\\
&\text{The actual reaction:} \quad\quad H^+_{(aq)} + OH^-_{(aq)} \longrightarrow H_2O 
\end{aligned}
```

### Titration

- **Laboratory method for determining the concentration of an unknown acid or base using a neutralization reaction**.

- In the reaction NaOH with HCl, one ion $`H^+`$ will react with one ion $`OH^-`$ so the molar ratio is 1:1. 
- With **polyprotic acid - מומצה רב פרוטית**, for example $`H_2SO_4`$ which releases 2 protons, the ratio for the reaction with $`NaOH`$ is 1:2 (1 mol of $`H_2SO_4`$ for 1 mol of $`NaOH`$).
- The **equivalence point (E.P)** is the point where the **titration stops** (the right amount of acid and base) because the concentration of $`H^+`$ and $`OH^-`$ are equals and **pH = 7**.

#### Normal Concentration

- **Normal concentration C(N)** is the molar concentration of the $`H^+`$ and $`OH^-`$.
- With _v_ (valance) the number of protons that can be released:

```math
C(N) = v \times C(M)
```
- For example, 1 mol of $`H_2SO_4`$ will have a normal concentration $`\bold{C(N) = 2 \times 1 = 2N}`$ because each molecule releases 2 protons.
- When the reaction finishes (**endpoint**) the number of moles $`H^+`$ and $`OH^-`$:

- _Note:_

> An equivalence point is reached when the moles of titrant and moles of solution are equal, while an endpoint is reached when the indicator changes color .
> So when the indicator pH is the same as the pH at equivalence, the endpoint and equivalence point should be the same in an ideal titration. 


```math
C_{\text{acid}}(N) \times V_{\text{acid}} =  C_{\text{base}}(N) \times V_{\text{base}}
```
- **Indicators (סמן)** help identifying when a reaction finished by changing colors according to the pH of the solution (like litmus paper, phenolphthalein, purple cabbage).
- Indicators are acidic or basic an indicate a specific range of pH.
- **pH meter** is a tool used to measure the pH.

#### Titration curve

- A titration curve is a plot of **pH** vs. the amount of **titrant** added.
- It is useful for determining **endpoints**.
- When looking at the curve it is easy to predict the equivalence point unless both initial acid/base and the titrant are weak. So adding a **strong titrant** is useful.
- The more titrant we add the less we need to add in order to increase the pH (because the concentration of the initial acid/base decrease). That's why around the equivalence point the pH changes quickly.

 ![titration curve](./img/01_Acids_Bases/titration_curve.png)

## Polyprotic acids

- Polyprotic acids release more than one proton.
- They release the protons in steps (not at the same time): the first one is easier the remove, the second harder, etc.
- Each $`H^+`$ ionization has it own $`K_a`$.
- The more oxygen atoms there are in a molecule the higher the $`K_a`$ because they keep the electron and the proton can be removed easily.

 ![polyprotic acids](./img/01_Acids_Bases/polyprotic_acids.png)
 
- The titration curve for an acid that releases 2 protons will have 2 equivalence points.

```math
\begin{aligned}
&H_2CO_{3(aq)} + OH^-_{aq} \longleftrightarrow H_2O_{(l)} + HCO^-_{3(aq)}\\
&HCO^-_{3(aq)} + OH^-_{(aq)} \longleftrightarrow H_2O_{(l)} + CO^{2-}_{(aq)}\\
\end{aligned}
```

 ![polyprotic titration curve](./img/01_Acids_Bases/polyprotic_titration.png)

## Buffer

- A buffer system is a solution that resists a change in pH when acids or bases are added by using either a weak acid and its conjugate strong base or vice-versa. 
- The conjugate acid/base will usually come from a salt and will be added in high concentration (maximum 1Molar). The salt in the water will release the acid/base which will be able to react with its conjugate already present in the solution.
- Important in biology because a lot of biologic reactions need to happen in a specific pH range: buffers help decrease the impact of acids/bases and keep a constant pH in the body in the blood and the digestive system.
- Two important buffer systems that help maintain the pH in the blood at 7.4:
  - **Carbonic acid [$`H_2CO_3`$] - bicarbonate ions buffer [$`HCO_3^-`$]**
  - **Phosphoric acid [$`H_3PO_4`$] - sodium dihydrogen phosphate [$`NaH_2PO_4`$]**
- A lot of buffers are created in labs in order to work on research.

### How does it work?

- The body needs to keep a certain amount of $`H^+`$.
- When there are too many $`H^+`$ ions, a buffer will absorb some of them, bringing pH back up; and when there are too few, a buffer will donate some of its own $`H^+`$ ions to reduce the pH.
- It works by **Le Chatelier**'s principle: adding on one side of the equation will cause the reaction to go in the other side's direction.

![buffer](./img/01_Acids_Bases/buffer.png)

#### Example: $`CO_2`$ in the blood

- $`CO_2`$ in the blood will travel in the form of carbonic aicd $`\bold{H_2CO_3}`$ which is a **weak acid**.
- Its **conjugate base** is bicarbonate $`\bold{HCO_3^-}`$ which is a **strong base**.

```math
CO_{2(aq)} + H_2O \Longleftrightarrow H_2CO_{3(aq)} \Longleftrightarrow HCO^-_{3(aq)} + H^+_{(aq)}
```

- By **Le Chatelier**'s principle if protons are added the reaction will go towards the reactants (the carbonic acid), and if protons are removed the reaction will go towards making more protons, i.e the products.

![blood buffer](./img/01_Acids_Bases/blood_buffer.png)


## Henderson Hasselbalch Equation

### Weak acid dissolution equations

- Reminder: in order to work with easier numbers (bigger numbers):

```math
\begin{aligned}
&pH = - \log [H_3O^+]\\
&pK_a = - \log [K_a]
\end{aligned}
```

- A little maths:

```math
\begin{aligned}
&HA_{(aq)} + H_2O_{(l)} \leftrightharpoons H_3O^+_{(aq)} + A^-_{(aq)} \\
&K_a = \frac{[H_3O^+][A^-]}{[HA]} \\
&\bold{[H_3O^+] = K_a \frac{[HA]}{[A^-]}}\\
&\log[H_3O^+] = \log K_a + log \frac{[HA]}{[A^-]} \longrightarrow \text{when multplying by -1:}\\
&\textbf{Henderson-Hasselbalch equation:}\\
& \bold{pH = pK_a + \log \frac{[A^-]}{[HA]}}
\end{aligned}
```

- One the same principe we can get the equation for bases.

- **Handerson-Hasselbalch equations:**

| Weak Acid | Weak Base |
|--|--|
|$`HA + H_2O \leftrightharpoons H_3O^+ + A^-`$|$`B + H_2O \leftrightharpoons BH^+ + OH^-`$|
|$`pH = pK_a + \log \frac{[A^-]}{[HA]}`$ |$`pOH = pK_b + \log \frac{[BH^+]}{[B]}`$ |

- Useful buffer range: $`pH = pK_a \pm 1`$ - This means the ratio $`\frac{[A^-]}{[HA]}`$ is either 10:1 or 1:10.
- When the **concentrations** of acid and base are **equals**:

```math
\begin{aligned}
&[HA] = [A^-]\\
&pH = pK_a
\end{aligned}
```
