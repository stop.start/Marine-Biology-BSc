# Thermodynamics

- Thermodynamics deals with the change of energy between the start of the reaction and the end.
- It doesn't deal with the speed of the reaction, just if it is possible from an energy point of view.
- **Laws of thermodynamics**:
  1. The amount of energy in the universe is constant. It cannot be created or destroyed only converted from one form to another (law of conservation of energy).
  2. The entropy in the universe constantly increases.
  3. The entropy of elements at absolute zero temperature is 0.
- The universe is separated in 2 categories:
  - **System**: the part of the universe where the experiment takes place. 
  - **Surroundings**: the rest of the universe surrounding the system that can affect the system.
- A system can (but not has to) exchange heat and matter with the surroundings.
- Types of systems:
  - **Isolated system**: there is no exchange of heat, matter or energy with the surroundings. It has "no surroundings".
  - **Closed system**: can exchange heat and energy with its surroundings but **not** matter.
  - **Open system**: can exchange heat, matter and energy with its surroundings.
- **State functions**:
  - Property of the system in a specified state (ex: current state) with a defined value (for example: temperature, concentration, volume)
  - The values don't depend of on how the system got to be in the current state.
  - A state function can determine another state function (example: $`C=\frac{n}{v}`$).
  
## Energy

- There are different types of energy: thermal, kinetic, radiant, chemical, potential.
  - **Kinectic energy** is the energy of movement.
  - **Thermal energy** is kinetic energy since it is produced by the high movement of molecules, hence kinetic energy.
  - **Potential energy** is the energy that has not been used yet but can be released.
- Kinetic energy:

```math
\begin{aligned}
&e_k = \frac{1}{2}mv^2\\
&[e_k] = \frac{kg \cdot m^2}{s^2} = J\\
\end{aligned}
```
- The change of energy of a system is defined by $`\bold{\Delta E}`$.
- E[energy] is a function state which is not easily measured but the change between two states has a unique value that can be easily measured.
- The **internal energy** is the energy of the system.

![state function](./img/04_Thermodynamics/state_function.png)

### Ideal gases

- Ideal gases don't have pulling force between the molecules and so the **potential energy between the molecules is 0**.
- The **internal energy** is only the latent energy of the molecules themselves.
- The **kinetic energy** depends only on the temperature of the gas.
- The temperature is the indicator of the energy of the molecules.
- **If a system has the same temperature at the start and in the end then $`\bold{\Delta E = 0}`$**.

## Laws of Thermodynamics

### First Law: conservation of energy

- **Energy cannot be created or destroyed only converted from one form to another.**
- Energy can be transfered by **heat** or **work**.
- Energy can change from one to another (work to heat and vice versa).

### [W]ork 

- Transfer of energy from and to a **mechanic system**.
- Work is a process and not a state function.
- Work is **force * distance** and requires both of them: $`w = F \cdot \Delta x`$
- Taking a recipient like a piston with gas, we can say that:
  - $`w = -\frac{F}{A} \cdot \Delta x \cdot A`$ where A is the surface of the recipient/piston.
  - $`-\frac{F}{A} = P`$[ressure] (where P is the external pressure) and $`\Delta x \cdot A = \Delta V`$[olume] thus:
  
  ```math
  \bold{W = -P_{ex} \cdot \Delta V}
  ```
- In general, where the external pressure isn't constant:

```math
W = -\int{P_{ex}} \cdot dV
```

- The amount of work **depends on the distance** even though the states at the beggining and end are the same.

### [Q] Heat

- _Note:_
> Q is used because Lavoisier and Laplace measured heat as [q]uantity and not as heat itself.  

- Heat (Q) is the **energy transfered between systems as a result of the difference of temperature**.
- Heat (Q) isn't a state function.
- Heat (Q) can be transfered through:
  - **Conductivity**
  - **Transport**
  - **Radiation**
- The amount of heat is measured in [J]oule which is an energy unit.
- It can also be measured in [Cal]ories which is the amount of heat required in order to _increase the temperature of 1gr of **water** $`1\degree`$_.
- The [q]uantity of heat can be measured as follow where:
  - q = quanity
  - m = mass
  - $`\Delta T`$ = difference in temperatures
  - C = heat capacity:
    - Amount of heat required in order to _increase the temperature of 1gr of **matter** $`1\degree`$_.
    - Depends on the type of the matter
    - Depends on the process affecting the system: Cv = process with constant volume, Cp = process with constant pressure.
    
```math
\bold{q = mC\Delta T}
```
- Heat is measured with a tool called Calorimeter which uses water (a known mass) by measuring the change in temperature and from this calculates $`q = mC\Delta T`$ .
- Heat can be released or absorbed.
- Heat is **alway transfered from a warm body to a cold body** until the temperatures are equal.

### First law of thermodynamics

- A system contains **only** internal energy.
- The energy of an isolated system is constant (law of conservation of energy).
- The change in internal energy of a system is work + heat:

```math
\Delta E = q + w
```

- An **endothermic** reaction absorbs heat thus **q>0**.
- An **exothermic** reaction releases heat thus **q<0**.
- Work that is done **on the system from the surroundings**: W>0.
- Work that the **system does on the surroundings**: W<0.

![first law of thermodynamics](./img/04_Thermodynamics/first_law.png)
