# Insoluble Salts

- Ionic compound that don't dissolve well in water.
- Water can dissolve two types of compounds:
  - **Polar** compounds which will make **hydrogen bonds**.
  - **Ionic** compounds.
- Water solutions can be divided in two groups: 
  - **Electrolyte solutions** which conduct electricity. The solute breaks up into ions in the solution.
  - **Non electrolyte solutions** which doesn't conduct electricity. The solute won't break up and will stay solid.
- The **solubility** of a compound depends on 2 factors:
  - **Entropy** is the spreading out of energy and the possibilites (states) of the spreading out (simplified explaination). In a solution the ionic solute will spread out in the solution (and won't stay at the same place).
  - **Energy** that is required in order to break the ions, the stronger the ionic bond the more energy is required.
- In chemical equations, ion with (aq) next to it means that is was dissolved in water.
- **Solubility - מסיסות** is the **maximal quantity** of material **dissolving in a given quantity of solvent** at a given temperature. It is measured in **gr/L** or **mol/L** (molar).
- A **saturated solution - תמיסה רוויה** is a solution which contains the maximal quantity of solute at the given conditions. For example $`NaCl`$ can be dissolve up to 5.4M and $`AgCl`$ is less soluble since it can be dissolved only up to $`10^{-5}`$M.
- The **molar solubility (S)** is the number of moles of the solute that dissolve to form a liter of saturated solution.
- In a saturated solution there's a **dynamic equilibrium** between the solid and the dissolved particles: particles reattach to the solid while others break from it.
- **Insoluble salts are salts with a solubility under 0.1 - 0.01 mol/L**.
- Example of equation for an insoluble salt:

```math
\begin{aligned}
&AgCl{(s)} \Longleftrightarrow Ag^+_{(aq)} + Cl^-_{(aq)}\\
&[Ag^+_{(aq)}] = [Cl^-_{(aq)}] = S
\end{aligned}
```

### Equilibrium

- The equilibrium constant is used only for insoluble salts while for soluble salt only molar concentrations are used since they dissolve completely.
- The equilibrium constant (as learned before) can be written as: $`K_{eq} = \frac{[Ag^+_{(aq)}][Cl^-_{(aq)}]}{[AgCl_{(s)}]}`$
- Because solids are not taken into consideration in the equilibrium constant, we can remove it.
- The constant for solubility is called **solubility-product constant - קבוע מכפלת מסיסות** or $`K_{sp}`$:

```math
K_{sp} = [Ag^+_{(aq)}][Cl^-_{(aq)}]
```

- In this example the molar solubility is S for $`Ag^+`$ and $`Cl^-`$ thus $`K_{sp} = S \times S = S^2`$ 
- All the rules of the equilibrium constants from previous lectures work here as well: stoichiometric quotients and stoichiometric ratios:

```math
\begin{aligned}
&\bold{A_aB_{b(s)} \Longleftrightarrow aA^{b+}_{(aq)} + bB^{a-}_{(aq)}}\\
&\bold{K_{sp} = [A^{b+}]^a [B^{a-}]^b}
\end{aligned}
```

- **The higher the $`\bold{K_{sp}}`$** the higher the solubility of a compound (there are more product).
- $`K_{sp}`$ for each salt in constant for a specific temperature.
- In a **saturated system** the system contains the maximum soluble ions.
- **Common-ion effect**: if, in the solution, there's already some of the ions that are in the salt, the **salt will dissolve less** (Le Chatelier!).
- When the number of ions present in the solution is a lot higher than $`K_{sp}`$, the number of identical ions that will dissolve from the salt can be dropped:

|                                    |                      |                 |
|------------------------------------|----------------------|-----------------|
| $`MgF_{2(s)} \Longleftrightarrow`$ | $`Mg^{2+}_{(aq)} +`$ | $`2F^-_{(aq)}`$ |
|                                    | ---                  | 0.080M          |
|                                    | +s                   | +2s             |
|                                    | s                    | 0.080 + 2S      |

```math
\begin{aligned}
&K_{sp} = 7.4 \times 10^{-11} = [Mg^{2+}][F^-]^2 = (s)(0.080 + 2s)^2\\
&K_{sp} \; \text{is really small compared to 0.080 and so it is safe to assume that 2s << 0.080 and drop it from the equation}\\
&K_{sp} = 7.4 \times 10^{-11} = (s)(0.080)^2\\
&\bold{s = 1.16 \times 10^{-8}} mol/L
\end{aligned}
```

### Precipitation reactions - תגובות שיקוע

- **Mixing 2 solutions of soluble salt can create an insoluble salt**. _For example, mixing $`AgNO_3`$ with $`NaCl`$ which are both soluble salt creates $`AgCl`$ which is an insoluble salt_.
- Be aware that with those reaction there is not stoichiometric relations between the ions forming the new salt.
- Still, at equilibrium, the product of the maximum concentrations exponent their stoichiometric coefficients equals to $`K_{sp}`$.

### Reaction Quotient Q - מנת תגובה

- $`Q_{sp}`$ is the product of the concentration (with matching exponents) when the system isn't in equilibrium:
  - At equilibrium $`Q_{sp} = K_{sp}`$
  - $`Q_{sp} < K_{sp}`$ there won't be any solid salt (לא ישקע משקע).
  - $`Q_{sp} > K_{sp}`$ there will be solid salt in the solution.
   
### Fractional Precipitation

- Technique in which two or more ions in solution are separated by the proper use of one reagent that causes precipitation of both ions.
- Practical if there's a bad/toxic ion that needs to be removed although it isn't possible to remove compounds completely (0 ions).
- Significant differences in solubilities are necessary.
- The fractional precipitation is done by adding a **precipitating ion** which precipitates a specific ion before the precipitation with the second ion.
- The first ion to precipitate is the one which requires less of the precipitating ion.
- Because the ions are is a solution and not solid, the stoichiometric ratios are not the ones of the salt (that is made form those ions). The precipitation is done in accordance to the stoichiometric ratios of the new salts.
- Knowing the $`K_{sp}`$ of each salt allows to know which concentration of the precipitating salt is needed for each salt.
- The $`K_{sp}`$ alone isn't enough to know which ion will precipitate first, but also the stoichiometric ratios. _For example: $`AgBr`$ has a lower $`K_{sp}`$ than $`Ag_2CrO_4`$ but for each $`CrO_4^{2-}`$ 2 $`Ag^+`$ are required._

### Hard water - מים קשים

- Water that contains ions (usually cation with a 2+ charge) like $`Ca^{2+}`$.
- They can form limescale (אבנית) like $`CaCO_3`$ and $`MgCO_3`$.
