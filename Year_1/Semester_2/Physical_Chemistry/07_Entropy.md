# Entropy

- A **spontaneous reaction** happens naturally and without any external help (no work required). They can be exothermic, endothermic or athermic.  
- A non spontaneous reaction will happen only if work is invested.
- Transfer of energy happens from a body at high temperature to a body at lower temperature (and not vice versa).
- Entropy is a measure of **disorder** or the possible microscopic states of a given macroscopic state.
- At $`0 \degree K`$ the atoms don't move at all and the entropy is null (only one state is possible).
- With higher temperature the entropy increases.
- There are 2 types of disorder:
  - Thermal disorder
  - Position disorder
- **Entropy is represented by the letter S**.
- The units are **joule/K**.
- **Entropy is a function state**.
- All elements were given a certain entropy $`S^0`$ at $`25 \degree C`$ and 1atm.
- $`\bold{S^0_{(g)} > S^0_{(l)} > S^0_{(s)}}`$
- In **gas state** the entropy depends on:
  - The **size** of the molecules (electron cloud).
  - Their **complexity** (number of atoms, types of bonds).
- In **liquid state** the entropy depends on:
  - The **size** of the molecules.
  - The complexity of the molecules.
  - The type of bonds **between** molecules. **Hydrogen bonds cause a lower entropy**.
- $`\bold{\Delta S}`$ is the change in entropy. There are 3 types:
  - The entropy in the **system**.
  - The entropy in the **surroundings**.
  - The entropy in the **universe** ($`\Delta St`$).
- The change is entropy is calculated with:
  - n is the number of moles.
  - $`\Delta S^0`$ is in J/K/mol.
  
```math
\Delta S^0_{system} = \Sigma {nS^0_{products}} - \Sigma {nS^0_{reactants}}
```

- With **gases**, it is possible to know if a reaction causes an increase or decrease in entropy by looking at the stoichiometric quotients. Solids are not taken into consideration since their entropy is small.

```math
\begin{aligned}
& 2C_{(s)} + O_{2(g)} \longrightarrow 2CO_{(g)} \Longrightarrow \bold{\Delta S^0_{system} > 0} \\
& N_{2(g)} + 3H_{2(g)} \longrightarrow 2NH_{3(g)} \Longrightarrow \bold{\Delta S^0_{system} < 0} \\
& I_{2(g)} + H_{2(g)} \longrightarrow 2HI_{(g)} \Longrightarrow \textbf{Cannot be determined} \\
\end{aligned}
```

- **Second law of thermodynamics**: 
  - **Each spontaneous reaction increases the entropy of the universe**. There's a constant increase in entropy of the universe.
- At equilibrium there is no change in entropy of the universe.

```math
\Delta S_{universe} = \Delta S_{system} + \Delta S_{surroundings} \leqslant 0

```

## Entropy in classic thermodynamics

- This is only for **reversible processes**.
- Entropy is defined by the following:

```math
dS_{sys} = \frac{dq_{rev}}{T}
```

- Giving heat (q > 0) will increase the entropy.
- Extracting heat (q <0 ) will decrease the entropy.
- Heat depends on the way the process is done (not a function state).
- At **constant temperature**:

```math
\Delta S_{sys} = \frac{q_{rev}}{T}
```

- Change in entropy depends on the amount of heat as well as the initial temperature of the system:
  - If the amount heat asborbed is small compared to the initial then it won't affect much the entropy.

### Change in entropy in isothermic in propagation/compression processes of an ideal gas

- When the volume increases ($`V_f > V_i`$) heat gets into the system and $`\Delta S > 0`$.
- When the volume decreases ($`V_f < V_i`$) heat gets out of the system and $`\Delta S < 0`$.

### With non constant temperature

```math
\begin{aligned}
& \Delta S = \intop{\frac{q_{rev}}{T}} \\
& \displaystyle \intop_{T_{1}}^{T_{2}}{\frac{nCdT}{T}} = nC\ln{\frac{T_2}{T_1}}\\
& \bold{\Delta S = nC\ln{\frac{T_2}{T_1}}}
\end{aligned}
```

- If T2 > T1 the entropy will be positive.

## Change in state

- Solid, liquid, gas

### Latent heat

- Example with water:
  - When heating water in solid state, its temperature increases until it reaches the melting point ($`0 \degree C`$ for water).
  - At this point the temperature stops increasing. That's because the energy invested is used to transform the ice into liquid. Water and ice are found both at $`0 \degree C`$.
  - When continuing to heat the water (which is now liquid), its temperature will increase until it reaches the boiling point ($`100 \degree C`$) .
  - At this point the temperature stops increasing. That's because the energy invested is used to break the hydrogen bonds and transform the liquid water into gas.
  
- The heat for melting and boiling is called latent heat.
- **Melting and boiling processes are isothermic**.
- $`\bold{\Delta H^0_m}`$ is the amount of heat required to **melt a solid**.
- $`\bold{\Delta H^0_v}`$ is the amount of heat required to **vaporate a liquid**.
- **The amount of heat required to transform from solid to liquid is lower than from liquid to gas**. That's because from solid to liquid only part of the bonds are broken, from liquid to gas all of the bonds (between molecules) are broken.

#### Change of entropy in melting/boiling processes

```math
\begin{aligned}
& \Delta S^0_m = \frac{\Delta H^0_m}{T_m} \\
& \Delta S^0_v = \frac{\Delta H^0_v}{T_b}
\end{aligned}
```
- The change in entropy in processes of vaporization of liquids with van der Walls bonds is pretty much the same.
- The change in entropy in processes of vaporization of liquids with hydrogen bonds is high than with thoses with van der Walls bonds.
