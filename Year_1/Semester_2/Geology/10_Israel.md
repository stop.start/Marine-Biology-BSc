# Israel

![geo map](./img/10_Israel/geo_map.png)

- In orange: magmatic metamorphic rocks (the mix is called massive(?)) over 550M old.
- In purple: basaltic rocks.
- In Israel the colors are green and yellow which are from 120M-50M old (Cretaceous to Eocene epochs).
- The rocks in Israel are mostly chalk (גיר) and a some dolomites.
- Nubian desert between the Nile and the Red Sea.
- Magmatic and metamorphic rocks indicates that it is a convergent zone.
- From the tectonic point of view the Suez canal isn't active anymore. Eilat canal is.