# Sedimentary Rocks

**Sedimentary rocks are rocks that are formed at "room temperature" and atmospheric pressure from precipitations**  

- Sediments form mainly underwater but can happen on land.

### 3 types of sedimetary rocks

- **Clastic rocks**: 
  - Parts of other rocks that were broken down.
  - Come from **moutains and stream down the rivers**. They accumulate in deltas and part of it slide down the deep oceans.
  - Classified according to the size of the grain: 
    - $`>2mm`$ - the rock is called **conglomerate**. Conglomerates are formed from several sediments into one (bigger) conglomerate of sediments.
    - $`2mm - 60\mu m`$ - the rock is called **sandstone - אבן חול**.
    - $`60\mu m - 4\mu m`$ - the rock is called **silt - טין**.
    - $`<4\mu m`$ - the rock is called **פצלים**, which is mud which became rock.
- **Organogenic rocks**: from shells (see first chapter)
  - אבן גיר
  - קירטון
  - דולימית
  - צור
- **Geochemical rocks**: 
  - Some are halogenic.
  - **Evaporites**: formed from evaporation: flint (צור) will be formed with phosphate, then גבס, then halite (salt), then silvit.
- **Marl - חוואר** is part פצלים and part קירטון (organogenic)

![sedimetary rocks types](./img/05_Sedimentary_Rocks/sedimentary_rocks_types.png)

### Continental margin

- Divided in 3 parts:
  - Continental shelf (מדף היבשת) - sendiments accumulate there.
  - Continental slope (מדרון היבשת) - sendiments stream down.
  - Continental rise (התרוממות היבשת) - sendiments accumulate there.
  
![continental margin](./img/05_Sedimentary_Rocks/continental_margin.png)

- Around **deltas**, sediments mix up with water and form mud. This mud when streaming can cause more damage than water since it's heavier.

### Weathering and erosion

- Weathering breaks down rocks physically and chemically:
  - Physically means that rocks are fragmented by mechanical process like freezing and thawing.
  - Chemically means the minerals in the rocks are chemically altered or dissolved.
- Erosion carries away the fragments created by the weathering (physical and chemical).
- Abbrasion (erosion in the course?) when the rocks bump into each other and get smaller, more rounded and polished.
- Sediments from ice stream don't get eroded because they get stuck in the ice and don't bump into each other.
- The minerals found in the sediments depends on the **intensity of the weathering**:

  ![weathering](./img/05_Sedimentary_Rocks/weathering.png)

- Quartz more or less stays quartz even in intense conditions.

### Some other stuff

- **Coquina**: rock built of shell fragments.
- **Ripples**: sediments do not spread equally under water and form sand ripple perpendicular to the current. Depending on their formation, they show the direction of the currents (symmetrical: back and forth, asymmetrical: one way).
- **Sorting**: Strong currents stream down sediments with them. As they get weaker, the size of the sediments they can carry decrease. This causes the sediments to be _sorted_ by size.
- **Turbidity currents - זרמי עירבו**: sediments get mixed up with water causing the density of the water to increase: **mud**. This mud slides down far in the water.
  - The mud brings with it sand into the deep water forming sandstone (אבן חוך) which has pores and can contain hydrocarbons.
- After sendiments are buried they undergo changes due to the increase in pressure and temperature (over long period of time). This is called **diagensis**.
  - **Cementation**: sendiments have pores with water in it. With the increase in pressure and temperature, the pores are filled, for example carbonate is precipitate as calcited but other minerals can also cement sediments into sandstone, mudstone or conglomerate.
  - **Compaction**: sendiments are squeezed close together (due to the pressure and temperature increase). Mainly true for mud.
- **Lithification** is the result of the cementation and compaction transforming the soft sediments into hard rock.
- Most sendiment rocks are siltstone, mudstone and shale (from clay mud)

  ![sediments occurence](./img/05_Sedimentary_Rocks/sediments_occurence.png)
  
  ![sediments sizes](./img/05_Sedimentary_Rocks/sediments_sizes.png)

### Summary of sediments

- Notes:
  - Calcium carbonate sediments are formed mainly from **calcite** minerals (for this course no need to know aragonite).
  
![sediments summary](./img/05_Sedimentary_Rocks/sediments_summary.png)


### Reefs

- Built on exctint volcanoes.
- Built from organisms.
- Form lagoons.
  
  
