# Mineralogy

**A mineral is a solid, natural and non-organic material.**  

- Some minerals are by the definition and some are formed from an organic (biologic) process like the shells of foraminifers (organogenic).

 ![foraminifers](./img/03_Mineralogy/foraminifers.png)

- Common minerals are from carbon: diamond, graphite, etc (same atoms, different arrangement).

 ![carbon](./img/03_Mineralogy/carbon.png)

- The size of the ions matter for the mineral to be more or less stable (big anions with small cation is more stable, for example quartz is made of silicon and oxygen and is very stable).

|||
|--|--|
|![atoms](./img/03_Mineralogy/atoms.png)|![atoms](./img/03_Mineralogy/ions.png)|
 
- Quartz is made like 2 prisms joined by their base.
- Minerals can be divided into groups:

| Class                  | Note                | Defining Anions                     | Example                                  |
|------------------------|---------------------|-------------------------------------|------------------------------------------|
| **Elements**           | With no other atoms | None                                | Diamond, graphite (C), copper metal (Cu) |
| **Oxides - תחמוצות**   | Have oxygen ions    | $`O^{2-}`$                          | Hermatite $`Fe_2O_3`$                    |
| **Halides - הלידים**   | Contain either:     | Chloride, Fluoride, Bromide, Iodide | Halite NaCl                              |
| **Carbonates - פחמתי** |                     | $`CO_3^{2-}`$                       | Calcite $`CaCO_3`$                       |
| **Sulfates**           |                     | $`SO_4^{2-}`$                       | Anhydrite $`CaSO_4`$                     |
| **Silicates**          |                     | $`SiO_4^{4-}`$                      | Olivine $`(MgFe)SiO_4`$                  |
| **Sulfides**           |                     | $`S^{2-}`$                          | Pyrite $`FeS^2`$                         |
  
## Silicates

- Silicate forms a tetrahedral with 4 oxygen, and those oxygen can bond with other atoms including other silicates and forming layers.
- When silicate is bonded only to other silicate ions it create **quartz**.
- Quartz is harder than glass.

 ![silicate](./img/03_Mineralogy/silicate_ion.png)
 
### Olivine

![olivine](./img/03_Mineralogy/olivine.png)

- Light green color.
- Silicate with Magnesium and Iron - $`(MgFe)_2SiO_4`$.
- Isolated tetrahedral bonded magnesium and iron.
- Need high temperature: top layer between the liquidus and solidus.

### Pyroxene

![pyroxene](./img/03_Mineralogy/pyroxene.png)
  
- Silicate with Magnesium and Iron with one less oxygen allowing for bonding between the tetrahedrals- $`(MgFe)_2SiO_3`$.
- Single chain of tetrahedral bonded to each other by one of their corners.
- Can create toxic compounds (asbestos).
- At lower temperature.

### Amphibole

![amphibole](./img/03_Mineralogy/amphibole.png)
  
- $`Ca_2(Mg,Fe)_5Si_8O_{22}(OH)_2`$
- Double chain, chain built like pyroxene, two chains bonded by their top corner (see picture).
- In between the tetrahedrals other molecules can be found.
- Can also create asbestos.
- Lower temperature
  
#### Pyroxene vs amphibole

![pyroxene vs amphibole](./img/03_Mineralogy/pyroxene_amphibole.png)

### Mica

![mica](./img/03_Mineralogy/mica.png)

- Muscovite: $`KAI_2(AlSi_3O_{10})(OH)_2`$
- Plane where other molecules can be found in between the mica molecules.

#### Feldspar

![feldspar](./img/03_Mineralogy/feldspar.png)

- With Aluminium, there's a new familly called **alumosilicate** that contains aluminium and silicon which are created in parallel to the silicate minerals. _For example, in parallel to the olivine feldspar is created_.
- Contains sodium and calcium where at high temperature only calcium and at low temperature only sodium and a mix in between.

## TODO:
- check what I wrote about silicate.

## Carbonates

- Trigonal planer (?) of on carbon with 3 oxygen
- Carbonate ions with calcium ions make **chalk - גיר**.
- Sometimes the carbonate bonds with magnesium instead of calcium.
- Mineral **calcite** is made (can be non organic, organogenic).

![carbonate](./img/03_Mineralogy/carbonate_ions.png)

## Oxides

- Metals with oxygen.
- A same metal can sometimes give 1 or more electron. _For example, iron gives 2 in olivine but 3 in hematite_.

||||
|--|--|--|
|![hematite](./img/03_Mineralogy/hematite.png)|![spinel](./img/03_Mineralogy/spinel.png)|![pyrite](./img/03_Mineralogy/pyrite.png)

## Sulfates

- Contain either $`SO_2`$,$`SO_3`$.

![sulfate](./img/03_Mineralogy/sulfate.png)

## Differentiate Minerals

![differentiations](./img/03_Mineralogy/differentiations.png)

### Mohs scale

- Scales the hardness of the minerals.
- Calcite can be dissolve with acids (like coca cola).

![mohs scale](./img/03_Mineralogy/mohs.png)

### Luster

- Differentiate by characteristics like how shiny a mineral is.
- Minerals can have several of those characteristics.

![luster](./img/03_Mineralogy/luster.png)


## Rocks

- Rocks are a mix of minerals (starting at 1 mineral).
- Granite is one example:

![granite](./img/03_Mineralogy/granite.png)

## Origin of minerals

- There are **2 areas where minerals are created** that are related to heat:
  1. Oceans
  2. Convergence areas
  
### Convergence areas

- Convergence areas get close to the solidus and part of the components of the ocean crust start to dissolve.
- When water is part of rock, the liquidus temperature of the minerals that compose it decreases because of this water (?).
- Water reduces the density of the rocks 
- Part of the crust is released from its water and go down and part of it remains/absorbs water and becomes lighter thus goes back up.
- Pyroxene in the convergence zones can become two different minerals depending if there's water present.

#### 3 types of rocks in convergence zones
- **Igneous rocks** (magma rocks) - rocks formed when magma (rock melted in hot, deep crust and upper mantle) cools and hardens.
- **Sedimentary rocks** - rocks formed from particles of sand, shells, etc.
- **Metamorphic rocks** - rocks that change due to temperature and pressure.

![types of rocks](./img/03_Mineralogy/rocks_convergence.png)
  
#### Common minerals in the 3 types of rocks

![minerals in rocks](./img/03_Mineralogy/rocks_minerals.png)

### Metals in rocks

- Some of them are found as metals in the rocks and can extracted from them (ex: gold).
- Some areas are richer in metals than others usually from magmatic or metamorphic processes in convergence areas where there is high temperature ($`300\degree-400\degree`$) and high pressure.
- **Magma chambers** are areas with magma that don't get to the surface, groundwater (מי קרום) can get close to those chambers, get hotter and go back to the surface with warm water and steam: **geysers**.

![magma chambers](./img/03_Mineralogy/magma_chambers.png)
