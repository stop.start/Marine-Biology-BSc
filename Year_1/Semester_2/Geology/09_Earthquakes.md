# Earthquakes

- There is some stress between two plates. When the stress is too high for the rocks to bear a rupture begins sending out seismic waves in all directions.

|                                            |                                                       |
|--------------------------------------------|-------------------------------------------------------|
| ![stress](./img/09_Earthquakes/stress.png) | ![earthquakes](./img/09_Earthquakes/segmentation.png) |

- Measuring an earthquake is done by measuring vertically and measuring horizontally.
- Usually there are 3 instruments that measures: one that measures the movements horizontally on the East-West line, one horizontally on the North-South line and one vertically.
- Instruments used springs with mass attached to them in order to measure earthquakes (see picture below).
- Then the instruments use light in glass fiber where the earthquake causes changes in the light strength.
- Today another instrument is used.

![instruments](./img/09_Earthquakes/instruments.png)

- Earthquakes cannot be predicted. We can only know where they can happen but not when.
- Earthquakes cause **3 types of waves**:
  - Surface waves that go only on the surface.
  - Pressure waves (גלי לחץ) in the Earth's interior which, while going forward overall, go forward and backward.
  - Shear waves (גלי גזירה) in the Earth's interior go upward and downward while going forward.
- Pressure waves are the fastest, shear waves second fastest and surface the slowest.
- Pressure waves are twice as fast as the shear waves.
- The surface waves are the cause of the earthquake strength and the destructions.
- There are 2 types of surface waves: one that moves up and down and one that moves back and forth on the sides. The latter (moves left and right is the one that causes destructions).

![waves types](./img/09_Earthquakes/waves_types.png)

- Shear waves can only move through **solid matter** while pressure waves can go through liquids and gas as well.
- Waves depend on the **density** of the matter they're going through: the more dense the faster they are.
- We can know when an earthquakes happened by looking at the time difference of arrival of the P and S waves (since p waves are twice as fast).

![speed](./img/09_Earthquakes/speed.png)

- By looking at the arrival time of those waves at 3 different locations we can have an idea of the location of the epicenter.

![epicenter](./img/09_Earthquakes/epicenter.png)

- Once the earthquakes were placed on a scale of "damages" caused by the earthquake (certain damages were caused the closer to the epicenter), then came the **Richter scale**. Today we use **energy measurement** (amplitude scale).
- The first waves can help know which type of segmentation it is (convergent, divergent, transform).

![first motion](./img/09_Earthquakes/first_motion.png)

- Most earthquakes are up to 50km depth.

![map](./img/09_Earthquakes/map.png)
