# Plate Tectonics

## Intro 

- It is the **movement of plates** and the force acting on them.
- It **explains the distribution of volcanoes, earthquakes, mountains chains, rock assemblage and seafloor structures**.
- The forces that drive plate motions arise from the **mantle convection system**.
- When looking at the geologic structures of continent (mainly South America and Africa) we can see that they "fit" together. This point towards the theory that all continents formed one big continent at some point.

## Plate Tectonics

- Earth is composed of **6 big plates** and a few smaller ones.
- Those plates are **always in movement**.
- They **explains the locations of volcanoes, earthquakes, mountains chains, oceans**.
- They help understand the past.
- The movement of the plates is caused by the **movement of the matter in the mantle (מעתפת)**.
- One supposition is that in the mantle there are hotter areas and less hotter areas.

 ![continental drift](./img/02_Plate_Tectonics/continental_drift.png)
 
### Seafloor Spreading 

- When looking at the volcanic and seismic activity in the Pacific ocean we can see it matches the edges the plate it is on.
- In the Pacific ocean, during earthquakes, part of the plate is going back into the mantle.
- The parts going back to the mantle **melt**, that's why the oceanic floor is younger than the land.
- Some oceans are created/get wider causing on the other side the ocean to close until lands collide and create mountains chains like the Alps: for example the Atlantic ocean gets wider while the Pacific ocean is closing.
- The earthquakes in the middle of the Atlantic ocean are higher and less strong (20-30km depth - level 5-6) while on the edge of the Pacific they are deeper and stronger (700km - level 6-9).

| Pacific                                          | Atlantic                                           |
|--------------------------------------------------|----------------------------------------------------|
| ![pacific](./img/02_Plate_Tectonics/pacific.png) | ![atlantic](./img/02_Plate_Tectonics/atlantic.png) |


### Plate Boundaries

 ![plates](./img/02_Plate_Tectonics/plates.png)

- There are 3 types of boundaries:
  - **Divergent**: basalt comes up in the middle of the ocean and pushes the sides.
  - **Convergent**: ocean against land collide, part goes downward, volcanoes are created and earthquakes happen.
  - **Transform**: plates slide one against another.
- There is an exception: **Hawaii**. There's a volcano like a pipe and not from a plate which creates a ridge. This is an isolated volcano. The fact that matter comes up from the mantle create mountains that are the island chain of Hawaii.

#### Divergent

- Some part of the mantle are around $`1300\degree C`$ (around 100km under the continents and less under the oceans). But some part of the mantle are more than $`1300\degree C`$ thus the materials go upward because they are lighter (in yellow with red arrows on the pictures below).
- Part of the materials going upward goes to the side and part of it gets to the **surface and this make volcanoes chains**.
- This all take part in pushing to the side the plates.

|||
|--|--|
|![divergent](./img/02_Plate_Tectonics/divergent1.png)| ![divergent](./img/02_Plate_Tectonics/divergent2.png) |

#### Convergent

- Divergent plates get farther apart causing other plates to get closer.
- Most of the convergence is on the **edges of the oceans** (ocean meets continent).
- Those parts are the **deeper** areas in the ocean.
- The **ocean crust** is around 100-200M years old, when going up it cooled down and became heavier. **Ocean crust is denser than continental crust** and thus it goes back into the mantle (under the continental crust) when colliding with a continental edge. 
- The **basalt that makes the ocean crust** is made of several materials, as a rock (סלע) they are solid, but when going back down to the mantle and the rock heat, each material reacts differently since they have different melting point. At some point everything will be melted, but between the crust and this point there's a region where things can be unexpected.
- The parts of the melted basalt that become lighter from absorbing water go back up and the rest continue to sink.
- In the convergent areas there can be **2 types of volcanoes**:
  - Volcanoes from material that go back up after being melted. **Not basalt**.
  - Volcanoes from material that was already there but was pushed by the ocean crust going down. **Basalt**.

||||
|--|--|--|
|![convergent](./img/02_Plate_Tectonics/convergent1.png)|![convergent](./img/02_Plate_Tectonics/convergent2.png)|![convergent](./img/02_Plate_Tectonics/convergent3.png)|

#### Transform

- There are several types of transform boundaries.
- One type is between two sections in the middle of the ocean:
  - Happens when two plates diverge with some movement to the sides then it creates a lot of small sections (see picture "tranform-fault bounderies (b)").
  - **Fracture zones** are sections from two different plates that are next to each other and go in the same direction.
  - **Transform fault** is in between fracture zones where the plates go in different direction
  
|||
|--|--|
|![transform](./img/02_Plate_Tectonics/transform2.png)|![fracture zones](./img/02_Plate_Tectonics/fracture_zones.png)|
|![transform](./img/02_Plate_Tectonics/transform1.png)||

### Magnetic anomalies

- The magnetic anomalies are "recorded" is the rocks from the ocean ground.

![magnetic anomalies](./img/02_Plate_Tectonics/magnetic_anomalies.png)

## The supercontinent Pangea

- By using the magnetic anomalies to retrace back the divergence of plates, it was possible to know how the geography looked millions years ago.
- All the continent formed one big continent with an ocean on each side (Tethys, Panthalassic).
- **Formation**: 237M years ago.
- **Break up**: 197M years ago.
- **Formation of the Atlantic and some continents**: 152M years ago.
- **Continents almost like today**: 66M years ago.

 ![pangea](./img/02_Plate_Tectonics/pangea.png)


## Flow in the Mantle

- 3000km thick, from the outer core up to the surface.
- 2 types of volcanic activities.
- Some volcanoes are like pipe 1000km. They are stable because the "hot origin" is stable. The plate moves on this spot creating each time a new volcanoe while the others go extinct from moving away form the "hot origin". They are called **hot spot**. Example Hawaii.

 ![hot spot](./img/02_Plate_Tectonics/hot_spot.png)

## More to know

- **Isochron** area identified by magnetic anomalies?


## Terms

| Terms                   | Definition |
|-------------------------|------------|
| Continental drift       |            |
| Convergent boundary     |            |
| Divergent boundary      |            |
| Geodesy                 |            |
| Island arc              |            |
| Isochron                |            |
| Magnetic anomaly        |            |
| Magnetic time scale     |            |
| Mantle plume            |            |
| Mid-ocean ridge         |            |
| Pangaea                 |            |
| Plate tectonics         |            |
| Relative plate velocity |            |
| Rodinia                 |            |
| Seafloor spreading      |            |
| Spreading center        |            |
| Subduction              |            |
| Transform fault         |            |
