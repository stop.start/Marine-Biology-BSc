# Structural Geology

- Mainly about structure of sedimentary rocks.
- Rocks that were:
  - Compressed/folded: **Ductile**
  - Broken: **Brittle**
- Folds happens in deep under, breaks happen either below or above the surface.
- **Dip angle - זווית נטייה** is the angle of the slope (see below). (dip = צניחה).

![dip angle](./img/07_Structural_Geology/dip_angle.png)

- **Syncline** - קימוט קעור fold with a bowl form, the inverse: **anticline** קימוט קמור
- In an anticline older layers are at the center, in a syncline the younger layers are at the center

![syncline anticline](./img/07_Structural_Geology/clines.png)

- San Andreas fault: horizontal fracture.
- Types of fault:
  - **Normal** (pulling)
  - **Reverse** (compression)
  - **Thrust reverse** + shallow dipping fault (oblique)
  
![fractures](./img/07_Structural_Geology/fractures.png)

Also gave up...
  
