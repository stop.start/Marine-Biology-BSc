# Geology

1. [Intro](./01_Intro.md)
2. [Plates Tectonics](./02_Plate_Tectonics.md)
3. [Mineralogy/Rocks](./03_Mineralogy.md)
4. [Igneous Rocks](./04_Igneous_Rocks.md)
5. [Sedimentary Rocks](./05_Sedimentary_Rocks.md)
6. [Metamorphic Rocks](./06_Metamorphic_Rocks.md)
7. [Structural Geology](./07_Structural_Geology.md)
8. [Stratigraphy](./08_Stratigraphy.md)
9. [Earthquakes](./09_Earthquakes.md)
10. [Israel](./10_Israel.md)
