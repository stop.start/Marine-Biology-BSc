# Metamorphic Rocks

**Will be maximum one question (if any) in the exam**.  

- Some rocks underwent processes in different pressures and temperatures in half-solid/half-liquid state.
- A **metamorphic rock** is a rock that was on the Earth's surface, that went deep underground and underwent partial melting and then came back to the Earth's surface.
- The conditions of pressure and temperature can vary a lot, for example, when volcanic matter goes up, the rocks that it comes in contact with are subjected to high temperatures but low pressure.
- **Two types of metamorphism**:
  - **Contact metamorphism**: matter at high temperature touches and warms rocks.
  - **Regional metamorphism**: a whole region goes down dozens of kilometers and comes back up.
  
![types of metamorphism](./img/06_Metamorphic_Rocks/metamorphism_types.png)

- Types of metamorphic rocks:

![types of metamorphic rocks](./img/06_Metamorphic_Rocks/metamorphic_rocks.png)

- Migmatic rocks: part metamorphic and part magmatic.
- Garnet the higher the pressure the higher its density.
- **Eclogite** high pressure, middle temperature. Contains garnet.
- Eclogite is heavy and as such is the component taking the plate down.
- **Diagenese**: minerals that changes up to $`200\degree C`$.
- Metamorphic rocks contain more sodium than calcium.

![texture](./img/06_Metamorphic_Rocks/texture.png)

## To know for the exam

![conditions](./img/06_Metamorphic_Rocks/rocks_conditions.png)
![pressure temp graph](./img/06_Metamorphic_Rocks/pressure_temp_graph.png)

I GIVE UP

