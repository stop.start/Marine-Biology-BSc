# Igneous Rocks or Magma Rocks

## Intro

- Rocks created from melting of silica material.
- They come from the **mantle (מעטפת) and the crust**.
- From the mantle:
  - The rocks are **darker**.
  - **Lighter in silicate** and **richer in iron and magnesium**. 
  - They are called **mafic** rocks (dark in greek). _Example: basalt_.
  - Example of source: Hawaii (volcano).
- From the crust:
  - Origin: convergence zones.
  - Rocks that completely melted when going back into the mantle. 
  - Have a lighter color.
  - Those are called **felsic rocks**. _Example: granite_.
- **Mafic rocks are "original" rocks from the mantle while felsic rocks come from existing rocks from the surface that melted**.

![basalt/granite](./img/04_Igneous_Rocks/granite_basalt.png)

- **Porphyritic**: when magma gets into some cracks in existing rocks and can cause differences in the size of the crystals of the rock: 
  - The crystals that were created before getting into the cracks - those are bigger due to having more time to cool down.
  - The ones that were created while in the cracks and cooled down faster - those are smaller.
- Porphyritic texture indicates two periods of crystallization: fast and slow.
 
  ![porphyritic](./img/04_Igneous_Rocks/porphyritic.png)

## Felsic and Mafic rocks

![felsic mafic](./img/04_Igneous_Rocks/felsic_mafic.png)

- Rocks to know:
  - **Ultramafic** rocks: 
    - Peridotite
    - Troctolite: forms fast from volcanic eruption (newly defined).
  - **Mafic**:
    - Gabbro
    - Basalt
  - **Intermediate**
    - Diorite
    - Andesite
  - **Felsic**:
    - Granite
    - Rhyolite
    
  ![rocks](./img/04_Igneous_Rocks/rocks.png)
  
- The minerals that can be created under the same temperature and pressure can be found in the same rocks.

### Melting temperature

- **The higher the temperature, the more pressure the less water, the more mafic a rock will be**.
- **The lower the temperature, the less pressre the more water, the more felsic a rock will be**.

- Minerals from cooling magma from high temperature ($`1200\degree`$) to low temperature ($`600\degree`$):
  - From the deeps up to the surface:
    - Olivine
    - Pyroxene
  - From ocean crust into the mantle in convergence zones (recycled):
    - Amphibole
    - Mica
    - Quartz
- Plagioclase feldspar:
  - High temperature: rich in calcium.
  - Low temperature: rich in sodium.
- Magma composition from high temperature (low silica) to low temperature (high silica):
  - Utramafic (low silica) - those are composed of olivine and pyroxene (see graph above)
  - Mafic, basaltic
  - Intermediate, andesitic
  - Felsic, rhyolitic (high silica) - those are composed of orthoclase feldspar, quartz and sodium rich plagioclase feldspar.

![temperatures](./img/04_Igneous_Rocks/temperatures.png)

## Dikes and sills

- **Dikes** are is the vertical penetration of magma. A dike going through the surface is a volcano.
- **Sills** are like dikes but horizontal.

|||
|--|--|
|![dikes sill](./img/04_Igneous_Rocks/dikes_sills.png)|![dikes sill](./img/04_Igneous_Rocks/dikes_sills2.png)|


## Intermediate rocks

- Temperature of the ocean ground: $`0\degree - 1.5\degree`$.
- Intermediate rocks - between solidus and liquidus.
- Serpentine mineral: 5-10% water + olivine = serpentine.
- When serpentine go up to the surface it can mix in the way with pyroxene and olivine. This mix is called ophiolite.
- On those rocks: sediments from the oceans.

## Magma chambers

- A magma chamber is a large pool of liquid rock beneath the surface of the Earth.
- Each magma chamber has a different composition.
- Some chambers can connect and mix.

|||
|--|--|
|![magma chamber](./img/04_Igneous_Rocks/magma_chamber1.png)|![magma chamber](./img/04_Igneous_Rocks/magma_chamber2.png)|

- We suppose that they are some magma chambers under the ocean.
- Those magma chambers **provide basalt** to the **oceanic ridges**.
- Those magma chambers contain **material different from the mantle**.
- The mantle has a **peridotite layer - ultramafic** but what comes out to the surface is mafic.
- In between the peridotite layer and the basalt layer, there's a **separation layer** where **gabbro** can be found. 
- **Gabbro** is part of what basalt is made of but in this layer it slowly becomes rocks.
- **Moho** (from the name Mohorovich): about ~20km below oceans, ~40km below continents, there's a **clear separation** of the **sound wave speed** (seismic wave and sound wave are alike), from 7km/s to 8km/s.
- **Earth's crust - קרום כד"א** is defined **between the surface down to the Moho line**. 
- Underneath Moho, the mantle starts and above the mantle can be found magma chambers (under the oceans ridges).

![moho](./img/04_Igneous_Rocks/moho.png)

### Convergence areas

![convergence magma](./img/04_Igneous_Rocks/magma_convergence.png)

- Part of the crust is released from its water and go down and part of it remains/absorbs water and becomes lighter thus goes back up.
- The material that goes back up is **felsic mainly diorite an andesite but sometimes granite and rhyolite**.
- In the magma chambers, $`1200\degree`$ where olivine precipitates, the temperature gets lower $`900\degree`$ where pyroxene and calcium plagioclase precipitate and in the end amphibole and plagioclase.

![convergence magma](./img/04_Igneous_Rocks/magma_composition.png)

