# Intro

## Earth

- 2/3 water - 1/3 land.
- The **continents** can reach highs up to **9 km** while the **sea** can reach depth up to **11 km**.
- Earth **radius** is close to 6000km.
- The Earth is quite unified but there are differences between the sea ground and the land.

### Earth composition

- Layers:

|                 | **Inner core**    | **Outer core**    | **Mantle - מעטפת**     | **Crust - כרום** |
|-----------------|-------------------|-------------------|------------------------|------------------|
| **Composition** | Solid iron        | Liquid iron       | Mainly oxygen, silicon |                  |
| **Radius**      | 1220 km           | 2260 km           | 2850 km                | 40 km            |
| **Range**       | 5150 km - 6370 km | 2890 km - 5150 km | 40 km - 2890 km        | 0 - 40 km        |

- The liquid iron in the outer core moves around 10cm/year.
- The **lands can be as old as 450M years**.
- The **seas are not older than 200M years old**.
- The seas are younger because the ground is always moving and renewing itself while the lands just build up.
- Part of the mantle (מעטפת) is crystilized (מגובש) and part of it isn't. That is due to the **speed of cooling** of rocks when getting close to the ground - it can be slow or fast: fast speed means that not all componants of the rocks have time to crystilized.

| Mantle rock                          | Core rock (iron)                 |
|--------------------------------------|----------------------------------|
| ![mantle](./img/01_Intro/mantle.png) | ![core](./img/01_Intro/core.png) |

- Earth's crust **floats** on the mantle.
- The **continental crust (כרום) is thicker than the oceanic crust**.
- The **density of the oceanic crust is higher than the density of the continental crust**. That why when cooling the oceanic crust drown and the continental crust floats.

 ![density](./img/01_Intro/density.png)
 
## Earth's Energy

- Most of the energy on Earth is from the Sun's radiations.
- Part of the energy from the Sun is returned (not used).
- The Sun's radiations are converted into energy through the **photosynthesis** of plants.
- Some minerals contain more energy (like uranium).
- The Earth emits heat but at a lesser level (volcanic eruptions).

### Some terms

- **Atmosphere**: envelope around Earth's surface.
- **Hydrosphere**: all water surfaces (seas, oceans, lakes, etc).
- **Biosphere**: all organic matter on Earth's surface.

## Magnetic Field

- Earth has a magnetic field.
- We think that it comes from the liquid iron moving causing an **electrical field** and electrical fields have magnetic field.
- The magnetic is counterclockwise which makes the north pole the positive pole.
- The magnetic field sometimes flips $`180\degree`$ and becomes clockwise making the south pole the positive pole. We don't know why but the theory is that there are hotter areas and colder areas in the outer core (which is liquid).

- _Experiment that has been done to test this theory:_
> Researchers followed earthquakes and their speed.  
> They found that earthquakes that went from east to west were slower than from north to south.  
> That goes towards the fact that some areas are hotter than others because hotter means less dense.  
> Less dense means that it moves slower.  

- The magnetic field flip is completely **random**. Because **each flip is unique** and has its own characteristics, it is used to **date magnetic field events**.
- **4500M** is the age of **the first solid rock** of Earth but not the age of the Earth.
- **Isotopes** are used to date rocks, in geology $`{^14}C`$ is not used because it can date only up to around 50K years.
- **Hadean** period: Earth formation where it started to cool down and the atmosphere started formed. The water vapors at some point formed rain drops and rain started. The temperature was around $`70\degree C-80\degree C`$. There was no life.
- The first traces of **life** dates back to **3700M years** ago: photosynthesis.
- There was ice period 2500M years ago. After this period the first animals (unicellular).
- **Proterozoic period**: animals developed more and more between 2500M - 550M but had no hard tissues (no bones or shells).
- **Phanerozoic period**: includes the **Cambrian period**: from around 550M years ago start more complex life (with bones and shells) after a big ice period.

 ![timeline](./img/01_Intro/timeline.png)

 ![timeline](./img/01_Intro/timeline2.png)
 
 
