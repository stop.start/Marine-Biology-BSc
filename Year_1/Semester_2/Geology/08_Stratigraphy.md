# Stratigraphy

- Fossils found in sedimentary rocks can be used to identify the layers in sediments.
- When finding certain fossils somewhere the layers beneath can be guessed.

![fossils](./img/08_Stratigraphy/fossils.png)

- **Hadean eon - עידן השהול** - from 4500Ma - 3700Ma: no life on earth.
- **Archean eon - עידן ארכאי** - up to 2500Ma.
- At the end of every geological era there's an **ice period** (not the same as an ice era which is more extreme).
- Tilit is a rock which contain elements from ice. A lot of tilit indicates an ice period.
- The first animals appeared after the archean eon, during the **proterozoic eon**. They were breathing air, releasing carbon dioxide and eating from primary production.
- The proterozoic eon ended with the hardest ice period known where most of the earth was ice during tens of millions years.
- From 550Ma until today: **Phanerozoic eon**. During this era the first animals with shells appeared.
- Those animals fossils help a lot to know Earth's history.
- **Trilobites** are the oldest (550Ma).
- **Amonite** lived in the water. They were really sensitive to climate change thus a lot of species went extinct or came to life.

| Trilobites                                    | Amonite                                    |
|-----------------------------------------------|--------------------------------------------|
| ![eras](./img/08_Stratigraphy/trilobites.png) | ![eras](./img/08_Stratigraphy/amonite.png) |

- Phanerozoic is divided into 4 eras:
  - Paleozoic
  - Mesozoic
  - Cenozoic
  
![eras](./img/08_Stratigraphy/eras.png)

- Sedimentary layers are mainly on the side of the lands and they are horizontals.
- Sedimentary layers that aren't horizontals indicate that something happened like a tectonic event.
- Unconformity in the sedimentary layers indicates erosion and/or tectonic events.

![unconformity](./img/08_Stratigraphy/unconformity1.png)
![unconformity](./img/08_Stratigraphy/unconformity2.png)

- In the mediterranean, during the dry period, there was some erosion.
- Rubidium which transform into strontium is used to date.

![dating](./img/08_Stratigraphy/dating.png)

- Artificial earthquakes in the sea are used to map what's going on underneath.
- Sound waves go through matter as well as are reflected.

![artificial earthquake](./img/08_Stratigraphy/artificial_earthquake.png)

