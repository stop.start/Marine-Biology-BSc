# Buoys

- There are 2 types of buoys:
  - I.A.L.A:
    - A
    - B
  - CARDINAL
- Every lights has its own flashing 

## I.A.L.A

- A and B are the same but inversed.
- Some places use the I.A.L.A A signalization and some the I.A.L.A B (see map to see where is what)

 ![iala](./img/01_Buoys/iala.png)
 
 ### I.A.L.A A
 
 - The green lights/buoys are on the **starboard** side (right).
 - The **topmark** for green buoys is a **triangle**.
 - The red lights are ont the **port** side (left).
 - The **topmark** for red buoys is a **square**.
 - Entry to a port: green on the right - red on the left
 - In areas which are not ports but have buoys signalization, the **direction of buoyage** in indicated on the map by an arrow (see below picture)
 
 ![red/green](./img/01_Buoys/red_green.png)

#### Preferred Channels

 - When there is more than one paths a **preferred channel** is indicated with 2 colored buoys:
   - Preferred channel to starboard RGR (red-green-red).
   - Preferred channel to port GRG (green-red-green).
- The preferred channel buoys have flashing lights at nights (see picture below)

|   |   |
|--|--|
| ![preferred channels](./img/01_Buoys/preferred_channels.png)|![preferred channels map](./img/01_Buoys/preferred_channels_map.png)|
 
- Each buoy has its own flashing signal (at least in the same area).
- Some buoys don't have lights at all.

## TODO
- slide 5

#### BRB buoys

- **Black-Red-Black** buoys are for an **isolated danger**.
- There is no indication of how much to keep away or from where to go around them.
- **Topmark**: 2 black circle.
- Lights at night: **white flashing lights (2 flashes)**.

|||
|--|--|
|![brb](./img/01_Buoys/brb.png)|![brb map](./img/01_Buoys/brb_map.png)|

#### Special mark

- Like a private beach.
- Yellow buoys.

|||
|--|--|
|![special mark](./img/01_Buoys/special_mark.png)|![special mark map](./img/01_Buoys/special_mark_map.png)|


#### Lightship - ספינת מגדלאור

- Is used by pilots to rest in between piloting ships.
- On the map the **small circle** is the **actual position** (coordinates).

|||
|--|--|
|![lightship](./img/01_Buoys/lightship.png)|![lightship map](./img/01_Buoys/lightship_map.png)|

## TODO
slide 6 - safe water buoy (red-white)

## CARDINAL

- Buoys for **isolated dangers** that need to be passed from a **specific side**.
- Yellow-Black buoy.
- **Topmark**: depends on which direction to go:
  - 2 triangles upward: North
  - 2 triangles downward: South
  - 2 triangles joined by their base: East
  - 2 triangles joing bu the "pointy side": West
- The lights flash differently for each direction

![cardinal](./img/01_Buoys/cardinal.png)


## Lighthouse flashing lightship

- Lighthouses have different flashing in order to tell them apart from afar.
- A few flashing code to remember:
  - **F**: fixed $`\rightarrow`$ fixed light (not flashing).
  - **Iso**: isophase $`rightarrow`$ same duration of light and dark.
  - **Fl**: flashing $`rightarrow`$ more dark than light.
  - **Q**/**VQ**/**UQ**: Quick/Very Quick/Ultra Quick $`rightarrow`$ flashing fast, very fast, ultra fast.
  
 ![all flashing](./img/01_Buoys/flashing.png)
 
- The flashing type (Fl) can flash differently: flash X times then stop then flash X times and so on.
- It can also have different colors (white/red/green).
- The interval between flashes is measured between the *first flash* of each group
- On the map the number of flashes, the time between the flashes and color is indacted as follow: Fl(3 R 10s) $`\longrightarrow`$ flashing 3 times in red at 10s interval.

 ![flashing](./img/01_Buoys/flashing_group.png)
 

## Leading & Sector lights

- When getting closer to the port, in order for ships to avoid danger, 2 types of lights can be used to **direct** the ships on a dangerless path:
  - **Leading light**: one light showing a straing light.
  - **Sector light**: two lights showing a sector (larger than the path of the leading light). On one side of the sector there is a green light and on the other side red light.

|||
|--|--|
|![leading](./img/01_Buoys/leading.png)|![sector](./img/01_Buoys/sector.png)|
  

## Map

- Regarding lights, if no color is written then the light is white.

## TODO
- audio 1/45

- A **green island** on the map means that at **low tide the island is visible** and **at high tide the island is under the water**.
- **Curly lines** shows communication cables and pipes.
- Some letters in a diamond can be found on the map, they are used to know the **stream strength - עוצמת הזרם**. In the pilot book look for the **letter and time**.

  ![letter](./img/01_Buoys/f_letter.png)
  
## Racoon

- On some buoys there's a device which broadcasts to RADAR (מכ"ם) frequency.
- This is useful in places with a lot of traffic in order to differentiate boats and buoys.

## Buoys to attach

- Nothing special

  ![attach](./img/01_Buoys/attach_buoys.png)
  
