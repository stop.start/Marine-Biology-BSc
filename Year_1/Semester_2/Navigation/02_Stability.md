# Stability of Boats

## Boat parts

![boat parts](./img/02_Stability/boat_parts.png)

- **Draft** is the part of the boat that is submerged.
- **Freeboard** is the part above the water.

## Loading

- The part of the boat under water can be more or less high depending on their load. For a same load it varies depending on the type of water (it will sink more in fresh water than salt water). The state of the sea needs also to be taken into consideration, since navigating in calm waters and in a storm will affect the boat differently.
- There are regulations on how deep a boat can "sink" in the water. It depends on the **season and type of water**.
- **Loadlines - קווי טעינה** chart shows how deep the boat can sink where the "summer loadline" is the comparison point.
- Above the summer are the conditions which allow for sinking deeper and below for sinking less.
- _Note: in fresh water the boat will sink more anyway than in salt water._
- _Note2: in yachts there is no such issues because the freeboard in very big which means that a few persons on it with their belongings won't make a big difference regarding the weight._

 ![loadlines](./img/02_Stability/loadlines.png)

- **Maximum capicity - תו קיבלת** show the maximum capicity of a boat and which type of water it is allowed to navigate in.

 ![maximum capicity](./img/02_Stability/max_cap.png)


## Stability and Balance

- **Center of [G]ravity** is a **point on the vessel through which all forces of gravity act vertical downward**.
- **Center of [B]uoyancy** is a **point on the center-line of the vessel through which all the forces of buoyancy pass when the vessel is heeled**.
- As long as the center of gravity and buoyancy are on the same line the boat is balanced.

 ![notions](./img/02_Stability/notions.png)
 
- **Trim by the stair**: the end of the boat (ירכתיים) is lower than the nose (חרטום).
- **Trim by the head**: the end of the boat (ירכתיים) is higher than the nose (חרטום).
- Trim by the stair is prefferable since the propeller is at the end of the boat.

![definitions](./img/02_Stability/definitions.png)

- The center of buoyancy moves when the boat is tilted.
- KG is the height of the center of gravity above the keel.
- KB is the height of the center of buoyancy above the keel.
- **[M]etacenter**: When the boat is tilted: 
  - The center of gravity G stays at the same place (if everything stays in place on the boat).
  - The part of the boat that is submerged changes (see picture) which causes the center of buoyancy B to move.
  - The **metacenter** is the meeting point of the line when the boat is straight with the new line when the boat is tilted. 
- The metacenter isn't relevant when the boat is straight because the boat doesn't need to be straighten up.
- The force that "wants" to straighten up the boat goes through the metacenter.

### 3 states of stability

- If the boat is tilted it needs to get back straight.
- When the boat is tilted:
  - A new vertical line is "created".
  - G stays at the same place on the original line.
  - B moves to $`B_1`$ on the new line.
  - On the new line a new point Z is the meeting point with an horizontal line from G.
- GZ, called **righting lever**, is the ability of the boat to return to the upright position.
- GM has a minimum length for the boat to be stable. The bigger the GM the more stable the boat is.
- When M is between G and B, it unstable. G "pushes" downward, $`B_1`$ upward thus rolling the the boat further.
- When G and M are on the same point, at first it won't move much, but might sink from water entering the ship.