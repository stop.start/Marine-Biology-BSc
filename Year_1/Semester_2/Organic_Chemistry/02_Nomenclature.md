# Nomenclature

## IUPAC

- International Union of Pure and Applied Chemistry.
- Sets rules of how to name molecules and compounds.
- There can be more than one name for a compound, the IUPAC one being one of them.
- Other names can be **common name** and some of them are still used because of the extensive use of their name.

## Alakanes

- **General formula of alkanes:**

```math
C_2H{2n_+2}
```
|||
|--|--|
|![alkanes](./img/02_Nomenclature/alkanes_examples.png)|![alkanes](./img/02_Nomenclature/alkanes_examples2.png)|

- Alkanes are also called **paraffins** or **saturated**.
- Alkanes names **finish** with **ane**: methane, ethane...
- Alakanes are molecules that contains only carbons and hydrogens (hydrocarbons).
- They're called saturated because they have **only single bonds ($`\bold{\sigma}`$)** meaning that each carbon bonds with 4 other atoms. The atoms on the side will bond with 1 carbon and 3 hydrogens and the ones in the middle bond with 2 carbons and 2 hydrogens.
  - Each carbon has 4 bonds which means they all are in the **tetrahedral** form.

### 10 First Alkanes

![alkanes](./img/02_Nomenclature/alkanes.png)

### Conformations

- Because of the single bonds, part of the molecule can **rotate** on those single bonds.
- The different appearences of the same molecule due to rotating is called **conformations**.

|||
|--|--|
|![conformations](./img/02_Nomenclature/conformations.png)|![conformations](./img/02_Nomenclature/conformations2.png)|


### Isomers

- **Isomers** have the same formula but have a different structure: the arrangement of the atoms in the molecule is not the same.
- They more precisely called **constitutional isomers - איזומרים מבניים**.
- Isomers are different molecules with different characteristics.
- In hebrew the isomers (not the simple, straight chain) are called **מסועף** or **מותמר**. This comes from the fact that the molecule isn't a simple chain but one of the carbon's hydrogen has been replaced with another carbon.

 ![butane](./img/02_Nomenclature/butane.png)
 
- **Iso unit**: carbon bonded to an hydrogen and to 2 carbons (methyl groups - $`CH_3`$).
  - Iso units are always on the "side" of the molecule (where the $`CH_3`$) usually are.
- Molecules with an **iso unit** are called with and **iso prefix**: for example, isobutane.
- **Neo unit**: carbon bonded to 4 carbons (methyle groups): for example, neopentane.
- Iso and Neo names are common names and not IUPAC but are still commonly used.

|||
|--|--|
|![isobutane](./img/02_Nomenclature/isobutane.png) | ![isopentane](./img/02_Nomenclature/isopentane.png)|

### Alkyl

- Small branches/substituents (ענפים/מתמירים) in and alkane chain.
- Those are **alkanes minus one H**. For example: methyl $`CH_3`$ is methane ($`CH_4`$) minus one H.
- They are written in the general form of **RH** where R is the alkyl group. RH alkane because it has one more H than the alkyl.

 ![alkyl](./img/02_Nomenclature/alkyl.png)

- The "missing" H can be replaced by all types of other structure like alcohol (OH), amine ($`NH_2`$), halogen (7th column), etc.

 ![alkyl with other groups](./img/02_Nomenclature/alkyl_mix.png)

#### Naming the Carbons

- **Primary carbon**: carbon bonded to **1 carbon**.
- **Secondary carbon**: carbon bonded to **2 carbons**.
- **Tertiary carbon**: carbon bonded to **3 carbons**.
- For example propyl is propane with one less H on the primary carbon while isopropyl is isopropane with one less H on a secondary carbon.

 ![carbon naming](./img/02_Nomenclature/carbon_names.png)
 
- Hydrogens that are bonded to primary carbon are also called primary and so on.

 ![hydrogen naming](./img/02_Nomenclature/hydrogen_names.png)
 
##### Example for naming molecules based on butyl (alkyl of butane)

- **Butyl** and **Isobutyl** have on less H on a primary carbon. Isobutyl has also an iso unit while butyl is a straight chain.
- **Sec-butyl** has one less H on the secondary carbon.
- **Tert-butyl** has one less H on the tertiary carbon.

 ![butyl](./img/02_Nomenclature/butyl.png)

### Name Molecules by IUPAC standards

1. Choose the **longest carbon chain**. On this chain the substituents (מתמירים). If there are two or more longest chains, **take the one with the most substituents**.
2. Number the carbons so that **carbon 1 is the closest to the substituents**. If there are more than 1 possibility, go in **alphabetical order**.
3. The substituents are on the lowest possible carbons.
4. For each substituent the name of the molecule will include the carbon to which each substituent is bonded, the name of the substituent (the name of the long chain is at the end of the name of the molecule). _For example 3-methylhexane_.
5. If there are **several substituents**, there are **written in alphabetical order**. _For example: 3-ethyl-1-methylhexane_
6. If there are **several identical substituents**, their **numbers are written together separated by commas and prefixes like "di/tri*** are added*. _For example 2,2,4-trimethylpentane or 6-ethyl-3,4-dimethyloctane_.
7. **Prefixes** (like di,tri,sec,tert, etc) are **not included in the alphabetical order**, meaning di**m**ethyl comes after **e**thyl.

||||
|--|--|--|
|![step 1](./img/02_Nomenclature/step1.png)|![step 2](./img/02_Nomenclature/step2.png)|

||
|--|
|![di tri](./img/02_Nomenclature/di_tri.png)|

#### Names with Halogens

- Halogens like hydrogen want to do 1 bond thus can replace them.
- Naming when they are substituent:
  - [F]luor: **fluoro**
  - [Cl] Chlore: **chloro**
  - [Br]ome: **bromo**
  - [I]iod: **iodo**
- For example **2-bromo-3-chlorobutane** (bromo first because b comes before c).

 ![halogen](./img/02_Nomenclature/halogens.png)

### Boiling Point of Alkanes

- The **longest the alkane chain** is (the more there are carbons), the stronger the Van der Walls bonds (no hydrogen bond).
- At room temperature:
  - **Up to 4 carbons: gaz**
  - **5 to 19 carbons: liquid**
  - **Above 19 carbons: solid** (19-20 soft solid like wax)
- The **more branches** there are (הסתעפויות) the farther the molecules between them thus the **boiling point decreases**.

 ![boiling point](./img/02_Nomenclature/boiling_point.png)
 
- **Distillation - זיקוק**: process of boiling mixture to evaporate the shorter carbon chain and get the more solid ones. Those are the ones use as flammable.
- Flammable reaction of alkanes releases energy and causes the emmission of toxic gas CO. 
- The release of energy is because the new bonds formed from the reaction release more energy that it took to break the original bonds.

```math
\begin{aligned}
&CH_4 + 2O_2 \longrightarrow CO_2 + 2H_2O\\
&CH_4 + \frac{3}{2}O_2 \longrightarrow CO + 2H_2O
\end{aligned}
```

- _Note:_
> When there's a fire in a place where there's little oxygen then it is more dangerous because $`CO`$ is released more than $`CO_2`$ and it is more toxic.  
> $`CO`$ and $`CO_2`$ bond to the hemoglobin but the $`CO`$ doesn't leave the hemoglobin and there's a danger of suffocation.  

## Cycloalkanes

- **Rings of hydrocarbons**.
- **General formula**:

```math
C_nH_{2n}
```

- Two less hydrogen in order to bond between two carbons and close the ring.
- Cyclopropane is a ring of 3 carbons. Those are not stable.

 ![cycloalkanes](./img/02_Nomenclature/cycloalkanes.png)

### Naming

- All rings names start with the prefix **Cyclo**.
- Substituents are named before **cyclo**_alkane_.
- Numbering the carbons starts where there's a substituent (if any)
- Same principle as the chains: 
  - The more substituents on the lowest numbers.
  - "di/tri/tetra" when identical substituents.
  - When several choices go in alphabetical order (**cyclo is included in the alphabetical order**, unlike prefixes like di, tri, sec, tert, etc).
  
 ![cyclo naming](./img/02_Nomenclature/cyclo_names1.png)
  
- **If the ring has less carbons than a chain bonded to it then the ring is on the chain. The principal name will be the one of the chain** (_see below examples_).


 ![cyclo naming](./img/02_Nomenclature/cyclo_names.png)

## Alkyl Halides

- Alkyl with halogens instead of hydrogen.
- Primary/Secondary/Tertiary in accordance to which carbon it is bonded to.

 ![alikyl halides](./img/02_Nomenclature/alkyl_halides.png)
