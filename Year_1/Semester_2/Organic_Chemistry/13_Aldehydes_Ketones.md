# Aldehydes & Ketones

- Carbonyl with an H bonded to it is an **aldehyde**.
- Carbonyl with an R' bonded to it is a **ketone**.
- A **formaldehyde** is a carbonyl with 2 hydrogens.

![aldehyde & ketone](./img/13_Aldehydes_Ketones/aldehyde_ketone.png)

![aldehyde & ketone](./img/13_Aldehydes_Ketones/aldehyde_ketone2.png)

## Nomenclature

- Aldehydes have the suffix **al** when they are the priority group.
- As a substituent its prefix is **oxo**.

![aldehyde naming](./img/13_Aldehydes_Ketones/aldehyde_name.png)

- Ketones have the suffix **one** when they are the priority group.

![ketone naming](./img/13_Aldehydes_Ketones/ketone_name.png)


## Reactions

- The partial positive charge on the carbonyl carbon causes it to be an electrophile, thus it reacts with nucleophiles.

![reactions](./img/13_Aldehydes_Ketones/reactions.png)

- Reaction with water:

![reaction with water](./img/13_Aldehydes_Ketones/water.png)

- The reaction can continue and form a new ketone/aldehyde. In order to know if there was indeed a reaction, **marked oxygen** can be used.

![marked oxygen](./img/13_Aldehydes_Ketones/marked_oxygen.png)

- The most reactive is formaldehyde, aldehyde and the least reactive ketone.
- Formaldehyde is more reactive because in addition reactions the tetraheder formed is less "crowded" than with a ketone.

![reaction level](./img/13_Aldehydes_Ketones/reaction_level.png)

### Hemicetal, Acetal, Hemiketal & ketal

- When reacting with alcohol, aldehydes and ketones can form acetals and hemicetals by "opening" the carbonyl double bond.
- An **hemicetal** is an aldehyde that has (in addition to the R & H) and OH group and OR group.
- An **acetal** is an aldehyde that has (in addition to the R & H) 2 OR groups.
- An **hemiketal** is a ketone that has (in addition to the R & R) and OH group and OR group.
- An **ketal** is a ketone that has (in addition to the R & R) 2 OR groups.

![acetal hemicetal](./img/13_Aldehydes_Ketones/acetal.png)

![acetal hemicetal](./img/13_Aldehydes_Ketones/acetal2.png)

- The alcohol bonding with the aldehyde/ketone groups is how glucose (that has an aldehyde group and an hydroxyl group) closes.
