# Alkynes Reactions

![structure of alkynes](./img/07_Alkynes_Reactions/alkynes.png)

- Akynes contain a triple bond.
- Triple bonds are rich in electrons thus are **nucleophiles**.

## Electrophilic addition mechanisms

### Hydrogen chloride to 2-butyne

- The molecule is symmetrical thus it doesn't matter which of the carbons in the triple bond the H bonds with.

![hcl addition](./img/07_Alkynes_Reactions/hcl_addition.png)

#### Steps

1. One molecule of HCl adds to form a double bond transforming the **alkyne to an alkene**.

![first step](./img/07_Alkynes_Reactions/hcl_addition1.png)

- This reaction yields (Z)-2-chloro-2-butene where the H and Cl are added on opposite sides of the molecule.

![anti addition](./img/07_Alkynes_Reactions/anti_addition.png)

2. A second molecule of HCl adds to the newly formed double bond.

![second step](./img/07_Alkynes_Reactions/hcl_addition2.png)

### Hydrogen bromide to 1-butyne

1. The molecule is asymmetrical, Markownikoff's rule is needed to know which of the carbocations is more stable.

![hbr addition](./img/07_Alkynes_Reactions/hbr_additions.png)

- The carbons within a double bond are called **vinylic**.
- When carbocation contain a carbon has a "+" carbon (with only 3 bonds) it as called a **vinylic carbocation**.
- If the "+" carbon is bonded to 2 other carbons it is a secondary vinylic carbocation and if it is bonded to only one other cabon then it is a primary vinylic carbocation.
- Vinylic carbocations are very unstable (even more than the methyl carbocations): double bonds are rich in electrons but the carbon is "+" thus something doesn't feel right $`\Rightarrow`$ unstable.

![vinylic](./img/07_Alkynes_Reactions/vinylic.png)

2. With second HBr, Markownikoff's rule is applied again and the H bonds with the carbon with the most hydrogens causing the Br to bond with the same carbon as the other Br.

- A molecule with two halogens on it is called **dihalide**.
- If the two holgens are on the same carbon it is called a **geminal dihalide**.

![second step](./img/07_Alkynes_Reactions/hbr2.png)


## Relative stabilities of carbocations

- Methyl carbocation: carbon "+" with no substituents R on it, only [H]ydrogens (the most unstable of the previous lesson).
- Vinylic carbocation: "+" on one of the carbons in the double bond (the most unstable of all).
- Allylic carbocation: "+" on a direct neighbor (carbon) of one of the carbons in the double bond (the less unstable).

![stabilities](./img/07_Alkynes_Reactions/stabilities.png)

- Those reactions (with halogens) are slow, they have higher **activation energy**.

![activation energy](./img/07_Alkynes_Reactions/activation_energy.png)


## Resonance

- **Localized electrons - אלקטרונים מאותרים** belong to a single atom or are confined in a bond between two atoms.
- **Delocalized electrons - בלתי מאותרים** are shared between 3 or more atoms that are **on the same plane**.
- Resonance doesn't have to be between the same atoms, as long as all atoms are on the same plane they can share their electrons.
- The higher the number of atoms sharing electrons the more stable the molecule is.
- Because several atoms share delocalized electrons it is hard to predict where they'll be. It is called **אל איתור**.
- _Note: carbon with $`sp^3`$ hybridization (bonded to 4 atoms) can not participate in resonance because they cannot accept more electrons.

![delocalized electrons](./img/07_Alkynes_Reactions/delocalized_electrons.png)

### Benzene $`C_6H_6`$

- Stable compound that doesn't do **addition reactions - תגובות סיפוח** like regular alkenes.
- Has 8 hydrogens less than a regular alkane with 6 carbons.
- Substitute one hydrogen with a substituent produces always the same product.
- Substitute another hydrogen and it produces 3 products.
- **Kekule** suggested that benzene isn't one compound but a **mix between 2 compounds with a fast equilibrium**.
- In the picture below we can see that the bond between atoms 1 and 2 is sometimes a single and sometimes a double bond.

![kekule](./img/07_Alkynes_Reactions/kekule.png)

- Unlike cyclohexane ($`C_6H_{12}`$), benzene is planar.

![benzene vs cyclohexane](./img/07_Alkynes_Reactions/benzene_cyclohexane.png)

- Double bonds are shorter and stronger than single bonds thus the benzene ring is not a perfect circle.

![benzene ring](./img/07_Alkynes_Reactions/benzene_ring.png)

- There can be **אל איתור** between all p orbitals creating clouds of $`\pi`$ electrons above and below the plane of the ring which is really stable.

||||
|--|--|--|
|![electrons clouds](./img/07_Alkynes_Reactions/pi_clouds_benzene.png)|![resonance](./img/07_Alkynes_Reactions/resonance1.png)|![resonance](./img/07_Alkynes_Reactions/resonance2.png)|

- Because it is not possible to know how many electrons there are in molecules when drawing them with a dashed line (or full circle), they are usually drawn with localized electrons.
- This is called **resonance contributor - תורם רזונטיבי**. The real structure is called **resonance hybrid - היבריד רזונטיבי**.

![resonance contributors](./img/07_Alkynes_Reactions/resonance_contributors.png)
![resonance contributors](./img/07_Alkynes_Reactions/resonance_contributors2.png)

- Hydrogenation of cyclohexene is exothermic (releases energy). 
- In theory hydrogenation of benzene from the above experiment should release 3 times more energy than hydrogenation of cyclohexene (benzene has "3 double bonds" while cyclohexene has one).
- In reality hydrogenation of benzene released less energy because from the start benzene is a lot more stable than expected.

![hydrogenation](./img/07_Alkynes_Reactions/hydrogenation.png)

### Carboxylic acids

- Carboxylic acids (carboxyl group COOH = carbonyl group + hydroxyl group) are stronger acids than alcohol (hydroxyl group OH)
- The conjugate base of a carboxyl acid is a lot more stable than the conjugate base of an alcohol because it has a resonance structure (for example carbonate $`CO_3^{2-}`$ which is the conjugate base of carbonic acid has 3 possible identical structures).

![carboxylate ion](./img/07_Alkynes_Reactions/carboxylate_ion.png)


## Resonance stabalized cations

- Allylic cations and benzylic cations are more stable than other carbocations.
- Secondary and tertiary are more stable (like with methy carbocations).


![relative stability](./img/07_Alkynes_Reactions/relative_stability.png)

![relative stability](./img/07_Alkynes_Reactions/relative_stability2.png)


## Dienes

- **Conjugated diene**: double bonds separated by one single bond.
- **Isolated diene**: double bonds separated by two or more single bonds.
- **Cumulated diene**: double bonds one after another.

  ![diene kinds](./img/07_Alkynes_Reactions/diene_kinds.png)
  
- Isolated diene is more stable than a conjugate diene because of the **delocalization** of the pi electrons.
- Cumulated diene is the least stable.

  ![conjugated diene](./img/07_Alkynes_Reactions/conjugated_diene.png)
  
- **The stability can be tested by hydrogenation and checking the energy released**.

  ![hydrogenation](./img/07_Alkynes_Reactions/hydrogenation.png)
  
### Reactions 

- In reactions with isolated dienes, the double bonds are completely independant and don't influence one another which means that one can react with a molecule and then the other can do the same with another molecule:

  ![isolated diene hbr](./img/07_Alkynes_Reactions/isolated_diene.png)
  
- Reactions with conjugated dienes can produce **two products: 1,2-addition & 1,4-addition** where 1,2,3,4 are the carbons around the double bond - single bond - double bond (1,2: double bond, 2,3: single bond, 3,4: double bond).
- In 1,4-addition the remaining double bond moved in the middle (2,3).

  ![conjugated diene reactions](./img/07_Alkynes_Reactions/conjugated_diene_reactions.png)
  
- After the first double bond attacked a proton, an **allylic carbocation** is formed which makes it a **resonance structure** because the double bond can attack the "+" in the carbocation thus inverting between the double bond and the "+":
- When the original double attack the original "+" thus moving the double in the middle, it will form a 1,4-addition.
- If it stays or goes back to the original structure it will form a 1,2-addition.

 ![allylic carbocation](./img/07_Alkynes_Reactions/allylic_carbocation.png)

- Mechanism for the reaction of a conjugated diene with HBr:

  ![conjugated diene reaction](./img/07_Alkynes_Reactions/conjugated_diene_reaction2.png)
 
