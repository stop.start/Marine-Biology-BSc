# Alcohols and Ethers

| Alcohol                                          | Ether                                          |
|--------------------------------------------------|------------------------------------------------|
| Alkyl group + Hydrogen attached to an Oxygen     | Two alkyl groups                               |
| ![alcohol](./img/09_Alcohols_Ethers/alcohol.png) | ![alcohol](./img/09_Alcohols_Ethers/ether.png) |


**Functional groups have different priorities when giving the IUPAC name:**  

![priorities summary](./img/09_Alcohols_Ethers/priorities_summary.png)

## Alcohol

- **Primary alcohol**: the carbon attached to the OH is also attached to 0 or 1 other carbon.
- **Secondary alcohol**: the carbon attached to the OH is also attached to 2 other carbons. 
- **Tertiary alcohol**: the carbon attached to the OH is also attached to 3 other carbons.

### Nomenclature (IUPAC)

- If the OH group (hydroxyl) is the higher priority group in the hydrocarbon chain the molecule name ends with **ol**. _Example: CH3-OH = methanol_.
- If not then the prefix **hydroxy** is used.
- If there's more than one OH then **diol/triol** are used.
- When looking for the longest chain, it has to include the OH as a direct substituent. This means that if there's a longer chain but the OH is not directly attached to it, it cannot be used as the base name.
- The carbon attached to the OH group receives the lowest possible number when numbering the carbons.

![naming](./img/09_Alcohols_Ethers/alcohol_naming.png)

### Hydrogen bonds

- Hydrocarbons don't form hydrogen bonds (electronegativity of Carbono and Hydrogen is too close). 
- Adding an **hydroxyl group OH** changes that fact and allow hydrogen bonds to form.
- The longest the hydrophobic tail the less hydrophobilic the molecule is.
- Molecules **up to 4 carbons** in the chain **dissolve well in water** (hydrophobilic).
- Because of the hydrogen bonds, the **boiling point** is higher than the regular alkane: _for example, at room temperature, methane in a gas but methanol is liquid_.

### Substitution reactions

- The OH group is really basic thus cannot undergo a nucleophilic reaction without help.
- In order for the OH to leave, it needs to be **protonated** by a **strong acid** (hydrogen + halogen) transforming the OH group into water which is a good leaving group.
- **Primary and secondary alcohols require heating for the reaction to happen. Tertiary does not require heat.**

![not happening](./img/09_Alcohols_Ethers/not_happening.png)

![substitution](./img/09_Alcohols_Ethers/alcohol_substitution.png)

#### $`S_N1`$ mechanism

1. An acid protonates the most basic atom (O of OH).
2. Water leaves, forming a carbocation.
3. The carbocation reacts (substitutes) with a nucleophile.

![sn1](./img/09_Alcohols_Ethers/alcohol_sn1.png)

- The carbocation can undergo all the changes and possibilities that were shown in previous lessons:
  - Chiral carbon:

![sn1](./img/09_Alcohols_Ethers/alcohol_sn1_2.png)

  - Hybride shift rearrangement:
  
![sn1](./img/09_Alcohols_Ethers/alcohol_sn1_rearrangement.png)

#### $`S_N2`$ mechanism

- Primary alcohols undergo $`S_N2`$ reactions because they cannot form carbocations stable enough for the $`S_N1`$ rections.

#### Alkyl Halides

- A HX (X = Cl, Br, I) can replace the OH group of an alcohol and then be replaced itself by another compound.
- This happens (and is **important!**) because **alcohols are readily available but unreactive** (they need the HX where X is an halogen in order to react) while **alkyl halides are less available but reactive**.

![alkyl halides](./img/09_Alcohols_Ethers/alcohol_alkyl_halide.png)

### Oxidation of alcohols

- In organic chemistry, oxidation is the **loss of hydrogens and/or the addition of oxygens**. Usually from C-H to C-O.
- Reduction is the opposite.
- **Tertiary alcohol don't undergo oxidation** because the carbon isn't bonded to an hydrogen thus cannot lose it.

![alcohol oxidation](./img/09_Alcohols_Ethers/alcohol_oxidation.png)


## Ether

- Can be **symmetrical/unsymmetric**: R--O--R / R--O--R'
- Simple ethers are more easily named by their **common name**: take the 2 substituents (one from the left of the O and the other from the right) and add ether in the end: _for example: ethyl methyl ether: CH3-O-CH2CH3_.
- Because, by the IUPAC rules, the ether group has the **lowest priority**, the IUPAC name uses **oxy** as the substituents suffix: _example: 1-ethoxy-3-methylpentane_
- In the IUPAC name, when looking at the 2 chains on both sides of the [O]xygen, the longest is the base name and the second is the substituent. _For example: CH3CH(OCH3)CH2CH3: CH3CHCH2CH3 is the longest chain (butane) and OCH3 is the substituent on the second carbon (2-methoxy): 2-methoxybutane._

|Common Name|IUPAC|
|--|--|
|![common name ether](./img/09_Alcohols_Ethers/ether_common.png)|![iupac name ether](./img/09_Alcohols_Ethers/ether_iupac.png)|

### Physical characteristics

- Ethers don't form hydrogen bonds thus their **boiling point** is lower than of the alcohols.
- Because of the [O]xygen, short ethers (2-3 carbons) have a slightly higher boiling than of the alkane (they are a bit polar).
- With longer chains, there is pretty much no difference of boiling point between alkanes and ethers.
- Because of the [O]xygen it can be dissolved in water (with the [H]ydrogen of the water).

### Substitution reactions

- With alcohols the leaving is OH, with ethers the leaving group is one of the O-R (ether = R-O-R').
- If the loss of one of the R forms a stable carbocation then the ether can undergo $`S_N1`$ reactions else $`S_N2`$.

#### $`S_N1`$ mechanism - ether cleavage

1. The oxygen is protonated forming a R that is an alcohol.
2. The alcohol leaves forming a carbocation.
3. The nucleophile attacks the carbocation.

![sn1](./img/09_Alcohols_Ethers/ether_sn1.png)

#### $`S_N2`$ mechanism 

1. The oxygen is protonated
2. The nucleophile attacks the less sterically hindered carbon (the less crowded which is easier to access). This means that the bigger substituent leaves with the OH.

![sn2](./img/09_Alcohols_Ethers/ether_sn2.png)

### Crown ethers - אתר כתר

- Cyclic compounds containing a positive ion.
- It helps dissolve insoluble compounds (like benzene)

![crown_ether](./img/09_Alcohols_Ethers/crown_ether.png)

