# Stereochemistry

## Isomers

- **Isomers** have the same **formula** but have a different arrangement of atoms and have different properties.
  - **Constitutional/Structural isomers** are isomers (same formula) and their atoms are bonded differently. They have different IUPAC names.
  - **Stereoisomers** are isomers (same formula), their atoms have the same connectivity but different spatial arrangement. They have the same IUPAC name base with some additions to differentiate them (like cis and trans).
    - **Geometric isomers** are a form of **stereoisomers** with a different geometric structure: Cis, Trans, E-Z.
    - **Enantiomers** are a form of **stereoisomers** that are the image of each other.
    
   ![isomers tree](./img/05_Stereochemistry/isomers_tree.png)

## Chirality

- A **chiral** molecule is non-superposable on its mirror image. An **achiral** molecule doesn't have this property.
- A **chiral center**: tetrahedral carbon (sp3 hybridized) that has four different groups. It is usually represented with a **red asterisk**.
- A carbon that is a chiral center is called **chiral carbon** or **asymetric carbon**.
- A molecule is said **chiral** if at least one of its carbon is a chiral center.
- A molecule can have more than one chiral carbon. 
- The more chiral carbons there are in a molecule, the more stereoisomers are possible. The **maximal number of stereoisomers** is $`\bold{2^n}`$ where n is the number of chiral carbons.

### Enantiomers

- **Enantiomers** are the 2 possible mirrored molecules.
- Unlike cis/trans they have (almost) the same chemical properties (like boiling point). One property is different: **the way they respond to light** (see [below](#polarized-light)).

> _Note:_  
> Two enantiomers look pretty much the same but are not. For example:  
> In biology receptors can bind to specific molecules with a specific spatial arrangement of atoms. The mirror image of the expected molecule won't be able to bind.  

![enzyme](./img/05_Stereochemistry/enzyme.png)

### Meso compounds

- A molecule which has a plane of symmetry is called **meso compound**.
- It has at least 2 chiral centers.
- Meso molecules **are not chiral** despite having chiral centers.
- Molecules with meso forms don't have $`2^n`$ stereoisomers.

![meso](./img/05_Stereochemistry/meso.png)

![meso](./img/05_Stereochemistry/meso2.png)


## Nomenclature

### R/S

- Called **Cahn-Ingold-Prelog** convention.
- The configuration is based on the chiral carbon.
- Every enantiomers couple have one R configuration and one S configuration.
- Steps:
  1. Number each of the 4 groups bonded to the carbon with the "bigger" atom being prioritized. If two atoms are the same use the next atoms (same as E/Z).
  2. Atoms with double/tripple bonds are duplicated/trippled.
  3. The group with the lowest priority (4) is put/drawn behind.
  4. Draw an arrow from the 1st group to the 2nd and then 3rd:
     - If the movement is **clockwise** then it is **R configuration**.
     - If the movement is **counterclockwise** then it is **S configuration**.

  |||
  |--|--|
  |![r/s configurations](./img/05_Stereochemistry/rs_configurations.png)|![r/s configurations](./img/05_Stereochemistry/rs_configurations2.png)|
  
- When there are more than one chiral atom, the configuration is given for each of those chiral atoms in numerical order.

### Fisher projection

- Helps when there are a lot of the chiral centers.
- From 3D to 2D representation.
- A picture is worth a thousand words:
  ![fischer projection](./img/05_Stereochemistry/fischer.png)

- In order to decide if R or S configuration:
  - Either the H (or lowest group) is on the vertical line and then we check as usual (arrow 1-2-3, clockwise or counterclockwise).
  - **If the H (or lowest group) is on the horizontal line, then clockwise means S and counterclockwise means R**.

|||
|--|--|
  |![fischer projection](./img/05_Stereochemistry/fischer1.png)|![fischer projection](./img/05_Stereochemistry/fischer2.png)|

### Cyclic molecules

- Molecules can also have chiral centers and can be enantiomers and diastereomers.

![cyclic](./img/05_Stereochemistry/cyclic.png)

## Polarized Light

- In hebrew: אור מקוטב
- Regular light contains electromagnetic waves moving in all direction. **Polarized light** moves only in one direction.
- In order to get polarized light, a special lense is required.

![polarized](./img/05_Stereochemistry/polarized.png)

- Polarized light going through **achiral compounds** will stay as is.
- Polarized light going through **chiral compounds** will **rotate** to an angle.
- Enantiomers will **rotate** the polarized light at the **same angle but in opposite direction**:
  - Clockwise is called **d** (dextrototary) or **+**.
  - Counterclokwise is called **l** (levorotary) or **-**.
- Some molecules in the R configuration will rotate in the + direction and some the - direction (same with S).
- The configuration isn't enough to know in which direction the compound will rotate the light, only with experiements it can be known.
- Chiral compounds, due to their effect on the polarized light, are said to be **optically active**.

 |||
 |--|--|
 |![achiral polarized](./img/05_Stereochemistry/achiral_polarized.png)|![chiral polarized](./img/05_Stereochemistry/chiral_polarized.png)|
 
## Racemic mixture

- **A racemic mixture contains the same amount of each enantiomer of the same molecule**.
- Because each enantiomer is present in the same amount, their **effect on polarized light is cancelled out**.
- **Diastereomers** are stereoisomers that are not mirror images of one another and are non-superimposable on one another.
- Diastereomers have **two or more stereocenters**.
- In the images below (the 2 images are the same molecules, one in drawing and one in balls and sticks):
  - 1 and 2 are enantiomers
  - 3 and 4 are enantiomers.
  - All other combinations are not enantiomers but are stereoisomers thus are **diastereomers**.

 |||
 |--|--|
 |![diastereomers](./img/05_Stereochemistry/diastereomers.png)|![diastereomers](./img/05_Stereochemistry/diastereomers2.png)|

- Unlike enantiomers, diastereomers don't have the same physical properties (boiling point, etc) and, still unlike the enantiomers, react differently with achiral compounds.
