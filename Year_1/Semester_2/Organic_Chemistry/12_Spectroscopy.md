# Spectroscopy

- Two main types: spectrometry and spectroscopy.
- 4 intrusmental techniques:
  - **Mass spectrometry (MS)**: gives information about the **molecular mass** and the **molecular formula**.
  - **Infrared spectroscopy (IR)**: gives information about the **functional groups** in the molecule with the help of the vibrations of the bonds.
  - **Ultraviolet/Visible spectroscopy (UV/VIS)**: identifies **conjugated double bonds** in the molecule.
  - **Nuclear magnetic resonance spectroscopy ($`\bold{H^1 NMR}`$)**: identifies the **location of the hydrogens** (carbon hydrogen framework) in the molecule.

## Mass Spectrometry

```math
DBE = C + 1 \bold{-} \frac{H}{2} \bold{-} \frac{X}{2} \bold{+} \frac{N}{2}
```

## Spectroscopy

- Allows to identify the structure of the molecule.
- Doesn't break the sample.
- Requires small amount of the compound.
- Allows to identify the structure of the compound via the interactions of the light with the compound.
- A **wavelength ($`\bold{\lambda}`$)** is the distance from any point on one wave to the corresponding point on the next wave.
- The **frequency ($`\bold{\nu}`$)** is the number of occurences of a repeating event per unit time.
  
![frequency](./img/12_Spectroscopy/frequency.png)

- **Frequency $`\bold{\nu}`$**  is equal to the **speed of light c** divided by the **wavelength $`\bold{\lambda}`$**.
- The frequency is inversely proportional to the wavelength.

```math
\boxed{ \nu = \frac{c}{\lambda} }
```

- The **energy (E)** is equal to **h (Planck's constant)** times **c (speed of light)** divided by the **wavelength $`\bold{\lambda}`$**.
  - **h** $`= 6.626 \times 10^{-34} Js`$
```math
\boxed{ E = h \nu =\frac{hc}{\lambda} }
```

- The **electromagnetic spectrum**: different types of electromagnetic radiations each associated with a particular energy range.

|||
|--|--|
|![spectrum](./img/12_Spectroscopy/spectrum.png)|![spectrum](./img/12_Spectroscopy/spectrum2.png)|

### IR - Infrared Spectroscopy

- IR gives information about the **functional groups** in the molecule with the help of the vibrations of the bonds.
- In molecules there are constant movements of the atoms like **springs**.
- The 2 main movements are:
  - Stretching
  - Bending
  
![stretching bending](./img/12_Spectroscopy/stretch_bend.png)

- Molecules absorb infrared waves which excite them and then they free energy in heat form in order to return to their normal state. 
- The frequency absorbed allows to identify functional groups in the molecule. In each functional group there are subdivision.
- **The frequencies absorbed are the ones that are equal to the frequencies of the bonds vibrations**.
- Not all the vibrations in the molecules absorb IR radiations, only the ones causing a change in the **moment dipole** of the bond.
- The greater the change in moment dipole the more it will absorb.
- Strong bonds vibrate faster: C=C > C-C > C-H.
- **Symetrical molecules with a pure bond don't absorb radiations**.
- No two molecules have the same IR spectrum besides **enantiomers**.
- The IR graph is divided in two: 
  - **Diagnostic** or functional group region: 4000-1400 $`cm^{-1}`$
  - **Fingerprint** region: 1400-600 $`cm^{-1}`$
- In the infrared graph the subdivisions are identified in the **fingerprint** region.

![regions](./img/12_Spectroscopy/regions.png)

- General stretching vibrations regions:

![regions](./img/12_Spectroscopy/regions2.png)

- Important stretching frequencies:

![important frequencies](./img/12_Spectroscopy/important_frequencies.png)

#### Hooke's Law

- Lighter atoms show absorption bands at larger wavenumbers:
  - C-H ~3000 $`cm^{-1}`$
  - C-D ~2200 $`cm^{-1}`$
  - C-O ~1100 $`cm^{-1}`$
  - C-Cl ~700 $`cm^{-1}`$
- Stronger bonds show absorption bands at larger wavenumbers:
  - C$`\equiv`$N ~2200 $`cm^{-1}`$
  - C=N ~1600 $`cm^{-1}`$
  - C-N ~1100 $`cm^{-1}`$

#### Other Characteristics

- Electron delocalization (resonance) affectes the frequency of the absorption: with resonance the wavenumber is lower.
- The signal for **alcohol O-H** is around 3300 $`cm^{-1}`$ and rounded:

![alcohol](./img/12_Spectroscopy/alcohol.png)

- In a **carboxylic acid** the O-H stretch is wider and around 2700-3100 $`cm^{-1}`$

![carboxylic acid](./img/12_Spectroscopy/carboxylic_acid.png)

- The signal for **N-H** also appears around 300 $`cm^{-1}`$ but looks different than O-H:

![N-H](./img/12_Spectroscopy/nh.png)

#### Other frequencies

![frequencies](./img/12_Spectroscopy/frequencies.png)

### UV/VIS

- Provides information about conjugated double bonds in the compound.
- Only organic compounds with $`\pi`$ electrons can produce UV/Vis spectra.
- It is based on the transition of electrons from one energy level to another.
- Spectrum for **vis (visible light)** between **400-800nm**.
- Spectrum for the UV between **100-400nm**.

### H-NMR

- Protons emit signals depending on their connections and the cloud of electrons surrounding them. 
- A proton with a bigger electrons cloud will send a weaker signal than a **"deshielded"** proton.
- The more electronegative the atom that the proton is bonded to is, the more deshielded the proton will be.
- In the below example the closer the protons are from the [N]itrogen atom the more deshielded they are:

![shield](./img/12_Spectroscopy/shield.png)

- Protons will send the same signal if they have the same connections. They will send different signal with different connections.

![signals](./img/12_Spectroscopy/signal.png)

- The ratio between the integrals of the signals shows the ratio between protons of the same type.
- The ratio should be with whole numbers and if the general formula is known then the number of hydrogens should equal the sum of the numbers in the ratio. For example in the H-NMR spectrum below, 1-bromo-2,2-dimethylpropane with the general formula of $`C_5H_{11}Br`$ has a ratio of 1.6:7.0 = 1:4.5 = 2:9. The 2:9 ratio is the first with whole numbers, it also matches the numbers of hydrogens in the molecule: 11 = 2+9.

![ratio](./img/12_Spectroscopy/ratio.png)

- When a hydrogen is bonded to a carbon which itself is bonded to a carbon bonded to hydrogens ($`CH_3CH_2Br`$), the signals are split in the following form: **N+1** where N is the number of signals from hydrogens bonded to the neighboring carbons and 1 is the signal of hydrogen itself.

![splits](./img/12_Spectroscopy/splits.png)
