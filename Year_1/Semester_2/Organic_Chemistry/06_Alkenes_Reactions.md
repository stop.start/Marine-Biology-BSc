# Reactions of Alkenes

- Reactions happen on weak parts of molecules like double bonds.
- The **functional group** is the part of the molecule that will react.
- All molecules (small, big) with C=C double bonds react the same way.
- **Atoms/molecules with a lot of electrons are attracted to atoms/molecules with less electrons**.
- **Electrophile**: atom/molecule that want/need electrons: molecule with a positive charge, atom that can pull a couple a electrons.
- **Nucleophile**: atom/molecule that wants to give electrons: molecule with a negative charge or neutrals with atoms that can share their electrons.
- Electrophiles and nucleophiles are attracted to each other.

## Lewis Acid/Base 

### Lewis Base

- Any molecule or ion which is able to form a **covalent bond** by giving away a couple of electrons.
- The atom will be more electronegative in the bond.
- Nucleophiles are lewis bases.
- $`OH^-`$ is a specific case of Lewis base (gives electrons to $`H^+`$).

### Lewis Acid

- Any molecule that can receive a couple of electrons and make a **covalent bond**.
- The atom will be less electronegative in the bond.
- Electrophiles are lewis acids.
- $`H^+`$ is a specific case of Lewis acid (receives electrons from $`OH^-`$).

## Sigma and Pi bonds

- $`\pi`$ bonds are weaker than $`\sigma`$ bonds.
- The cloud of electrons from the $`\pi`$ bond is above and below the $`\sigma`$ bond thus making the molecule rich in electron $`\longrightarrow`$ nucleophile.
- **Alkenes will react with electrophiles** breaking the $`\pi`$ bond.

## Carbocation

- Unstable state of a **carbon with 3 bonds and a positive charge**.
- It is a temporary molecule created when an electrophile reacts with a nucleophile.

![carbocation](./img/06_Alkenes_Reactions/carbocation.png)

## Reaction mechanism

- The transfer of electron/s is shown with rounded arrows which are drawn from the negative charged **bond** to the positive charged atom (since it shows the direction of the electrons).

| Single electron | Two electrons |
|--|--|
|![single_e_arrow](./img/06_Alkenes_Reactions/single_e_arrow.png)|![two_e_arrow](./img/06_Alkenes_Reactions/two_e_arrow.png)       |

![arrows rule](./img/06_Alkenes_Reactions/arrows_rule.png)

- Those reaction are called **electrophilic addition reaction** - תגובת סיפוח אלקטרופילי.

### Example

```math
CH_3CH=CHCH_3 \quad + \quad {\overset{\delta +}{H}} - {\overset{\delta -}{Br}}
```

- First reaction step: double carbon bond breaks, H bonds with one of those carbons leaving Brome with extra electrons.

![mechanism first reaction](./img/06_Alkenes_Reactions/mechanism1.png)

- Second reaction step: carbon with 3 bonds and a positive charge bonds with the negative brome.

![mechanism second reaction](./img/06_Alkenes_Reactions/mechanism2.png)

### Transition state vs Intermediate

- **Transition state**: state during the process of creating/breaking bonds where atoms are not yet bonded/unbonded to other atoms.
- **Intermediates** have fully formed bonds but are temporary molecules.

![transition state vs intermediate](./img/06_Alkenes_Reactions/transition_intermediate.png)


- The first step of the pi bond break and forming a bond with the hydrogen is relatively **slow**.
- The second step of the negatively charged bromide ion **rapidly attacks** the positively charged carbocation intermediate.​

![kinetics](./img/06_Alkenes_Reactions/kinetics.png)

## Markownikoff's Rule

### A/Symmetrical alkenes

- With symmetrical alkenes the new bond can be formed with either of the carbons involved in the original double bond.
- With asymmetrical alkenes the product can be different depending on which of the carbons of the double bond is formed the new bond:
  - **Major**: the product that will usually be formed.
  - **Minor**: the product that won't (usually) be formed.

### Markownikoff's rule

- Given an HX compound, the **[H]ydrogen** will bond to the carbon with **the most hydrogens**.
- This is due to the fact that the **carbocation** formed at the first stage of the reaction is unstable. The less unstable structure for the carbocation is with the hydrogen bonded to the carbon with the less substituents (hence the most hydrogens). That's because a carbon with more substituents is more stable. 

![marko's rule](./img/06_Alkenes_Reactions/marko_rule.png)

## Relative stabilities of carbocations

- The more substituents are attached to a carbocation the more stable it is.
- This is because the **alkyl groups** decreases the positive charge of the carbocation. The alkyl groups don't pull electrons and even more, will push negative charge by **hyperconjugation**.
- **Hyperconjugation**: a carbon with only 3 bonds has an empty p orbital and the electrons of an adjacent C-H bond may spread into this empty p orbital.
- Naming:

| Methyl carbocation      | Primary carbocation | Secondary carbocation | Tertiary carbocation |
|-------------------------|---------------------|-----------------------|----------------------|
| No substituent attached | 1 substituent       | 2 substituents        | 3 substituents       |

![carbocation stability](./img/06_Alkenes_Reactions/carbocation_stability.png)

### Regioselectivity

- A reaction is regioselective if it happens more in a certain part of the molecule.
- There are several degrees of regioselectivity depending on how more often the reaction happens in a certain way.
- In a reaction with a double bond:
  - If the two carbocations possible are both the same structure (methyl/primary/secondary/tertiary) then the reaction isn't regioselective because there isn't one more stable than the other.
  - If the two carbocations possible are different then the bigger the difference the higher the regioselectivity: primary/tertiary is more regioselective than secondary/tertiary because primary is a lot less stable than tertiary but secondary and tertiary are close in stability.

#### Chirality

- In the below example, the carbon [Br]ome will bond to is **chiral** thus brome can bond either above of below creating two possible enantiomers (R and S).
- Because the chance of happening either way is 50-50 the product will be **racemic**.

![chirality](./img/06_Alkenes_Reactions/chirality.png)

## Reaction with water

- In order for an alkene to react with water it needs a **catalyst** because the O-H bond is too strong to let the H act as an electrophile.
- The process is called **hydration** and uses an **strong acid catalyst** which acts as the electrophile (first step) and the water as the nucleophile (second step):
  - The strong acid will increase the hydronium ($`H_3O^+`$) concentration allowing for the reaction with the alkene to take place.
  
  ![catalyst](./img/06_Alkenes_Reactions/catalyst.png)
  
  ![water reaction](./img/06_Alkenes_Reactions/water_reaction.png)
  
## Rearrangement

- Rearrangement is when a substituent moves from one atom to another in the same molecule.
- Carbocations can undergo rearrangements if the results is a more stable molecule.

![rearrangement](./img/06_Alkenes_Reactions/rearrangement.png)

### 1,2-hybride shift

- **1,2** means from one carbon to its neighbor.
- **hydride** means that an $`H^+`$ moves from on carbon to the other.

![rearrangement](./img/06_Alkenes_Reactions/rearrangement2.png)

### 1,2-methyl shift

- **1,2** means from one carbon to its neighbor.
- **methyl** means that an $`CH_3`$ moves from on carbon to the other.

![rearrangement](./img/06_Alkenes_Reactions/rearrangement3.png)

## Halogenation

- For the reaction to happen it needs a **non polar organic solvent** like $`CCl_4`$.
- Double bond open and bond with an halogen molecule - **dihalide** ($`Br_2`$ or $`Cl_2`$).
- The addition of [I]odine is not stable.
- When a dihalide comes close to an alkene, the electrons in the alkene push away the electrons in the dihalide making it an electrophile.
- When, for example, Br bonds with one of the carbons it **does not** form a carbocation but a **bromonium ion** (the + is on the brome not the carbon) and creates a ring with the other carbon.
- There is no shifts because there is no carbocation!
- In the second step the other brome atom attacks to form the final result.

![halogenation](./img/06_Alkenes_Reactions/halogenation.png)

## Halohydrin formation

- When there's a dihalide and water with the alkene:
  - If there's a lot of water (or other polar solvent), they will attack the second carbon.
  - This means that there can be two possible path: one where the water attacks the second carbon and one where the remaining halogen attacks the second carbon.
  - Because the concentration of water is higher than of the lone halogens, the path where the water attacks will happen more (major product).
  - The water won't attack the first carbon since the water is a weak acid (the OH bond is too strong).
![halohydrin](./img/06_Alkenes_Reactions/halohydrin.png)

![halohydrin](./img/06_Alkenes_Reactions/halohydrin2.png)

## Hydrogenation

- Use of a metal as a catalyst to open the double bonds.

