# Conformations and Configurations

## Some terms

- **Rotations** occurs around single bonds
- **Conformations** are different arrangements of the atoms of a same molecule due to the rotations around single bonds ($`\sigma`$) bonds.
- **Conformer** is a specific conformation. Because atoms move all the time, the molecule go from conformer to conformer all the time.

## Conformations

- **Eclipse conformation**: Hydrogen atoms are one in front of the other. Requires more energy because the atoms are closer. The additional energy in this conformation is called **torsional strain**.
- **Staggered conformation**: There's a $`60\degree`$ angle between the hydrogen atoms of one carbon and the others. Requires less energy because there is no torsional strain.
  - **Anti**: in longer chain where the staggered conformation has the methyl groups at $`180\degree`$ apart. Requires less energy.
  - **Gauche**: in longer chain where the staggered conformation has the methyl groups at $`60\degree`$ apart. Requires more energy.
- There are an infinite number of conformations in between eclipse and staggered conformations.

## Configurations

- Two isomers molecules can have different configurations.
- Unlike conformations, the molecules cannot go from one configuration to another without breaking bonds.

## Types of strain

- The **torsional strain** is higher the closer covalent bonds are.
- The **steric strain** is the repulsion between the electron clouds of atoms of groups.
- The **angle strain** is present in rings where the angles are between the atoms of the ring.

|Eclipse|Staggered|
|--|--|
|![eclipse](./img/03_Conformations_Configurations/eclipse.png)|![staggered](./img/03_Conformations_Configurations/staggered.png)|

## Drawing 3D molecules

### Wedge-Dash Notation

- **Regular lines**: bonds on the same plane as the paper.
- **Bold lines**: bonds going "outside" of the paper (to the person looking).
- **Dashed lines**: bonds going "inside" the paper.

|Wedge-Dash|3D|
|--|--|
|![wedge-dash](./img/03_Conformations_Configurations/wedge_dash.png)|![ethane 3D](./img/03_Conformations_Configurations/ethane_3d.png)|

### Sawhorse Projections

- Less used.
- Drawing the molecule at an angle.

![sawhorse](./img/03_Conformations_Configurations/sawhorse.png)

### Newman Projections

- One carbon in front of the other.
- With this type of representation it is easier to see the eclipse and staggered conformations.
- The point at the center where the lines of all the front hydrogen meet is the front carbon.
- The circle represents the back carbon.
- In order to represent an eclipse conformation but still show that there are indeed hydrogen, the hydrogen are drawn with a bit of an angle (see pitcure).
- **Dihedral angle** or **torsional angle** is the angle between two bonds originating from two different atoms (like the angle between an CH bond from the front carbon and an CH bond from the back carbon).
- The smaller the dihedral angle the higher the torsional strain.

|Newman|Eclipse|Staggered|
|--|--|--|
|![newman](./img/03_Conformations_Configurations/newman.png)|![newman eclipse](./img/03_Conformations_Configurations/newman_eclipse.png)|![newman staggered](./img/03_Conformations_Configurations/newman_staggered.png)

- When there are more than 2 carbons in the chain, the carbon with the **lowest number** is at the **front**. 
- Instead of one or more hydrogen the rest of the chain and subsituents and written in their place.

![newman butane](./img/03_Conformations_Configurations/newman_butane.png)

- If we want to look at carbons 2 and 3 which are both bonded to a methyl group then we can see 2 new conformations that come from the staggered one:
  - **Anti**: staggered where the methyl groups are $`180\degree`$ apart. Requires less energy.
  - **Gauche**: staggered where the methyl groups are $`60\degree`$ apart. Requires more energy.
- The **steric strain** is higher in the gauche conformations than in the anti conformation.
- Of course in addition to those conformations there are also the eclipse conformations, some of which requires more energy than the others.

![anti gauche](./img/03_Conformations_Configurations/anti_gauche.png)
![energy graph](./img/03_Conformations_Configurations/energy_graph.png)


## Cycloalkanes

- Cycloalkanes are cyclic hydrocarbons with the general formula $`C_nH_{2n}`$.
- They are less flexible than open-chain alkanes thus less conformers possibles.
- When looking at the ring of carbon when it is parallel to the plane, there are subsituents above the rings and the subsituents under the ring.
- A molecule with the same name can have a specific subsituents above or under the ring where it cannot move from one state to the other like with conformations. It happens where there are more than one subsituents. Those are **isomers** and more specifically **stereoisomeres** which are **geometrical isomers**.

  ![no rotate](./img/03_Conformations_Configurations/no_rotate.png)

### Stereoisomers

- There are 2 different configurations of **stereoisomers**:
  - **Cis** where 2 subsituents are both on the same side (above or under) of the plane.
  - **Trans** where 2 subsituents are on different sides of the rinf (one above and one under).
- Cis and trans are put in front of the IUPAC name. _Examples_:
  - _cis-1,2-dimethyl-cyclopropane_
  - _trans-1,2-dimethyl-cyclopropane_

  ![cis trans](./img/03_Conformations_Configurations/cis_trans.png)

### Angle strain

- **Angle strain** happens in cyclic molecules and is the strain from the angle between the atoms of the ring.
- The best angle is $`\bold{109.5\degree}`$
- A ring from 3 atoms (cyclopropane) has a lot of angle strain thus is not stable. Also the conformation is eclipse.
- A ring with 4 atoms (cyclobutane) is not planar and prevents eclipse conformations that way.
- A ring with 5 atoms (cyclopentane) is not planar and has one carbon above the rest (looks like an envelope).

### Cyclohexane

- Most common ring.
- Has $`111\degree`$ angles which is really close to $`109.5\degree`$.
- Has 2 main conformations:
  - **Chair conformation**: all bonds are in staggered conformation thus this conformation has almost **no strain**.
    - **Equatorial bonds** are parallel to the ring plane.
    - **Axial bonds** are perpendicular to the ring plane.
    
    ![cyclohexane chair](./img/03_Conformations_Configurations/cyclohexane.png)
    
  - **Boat conformation**: has almost no angle strain but has:
    - Some bonds in eclipse conformation which causes torsional strain.
    - Because the "flagpole hydrogen" are close to each other it has also some steric strain.

    ![cyclohexane boat](./img/03_Conformations_Configurations/boat.png)

- **Ring flip** is when the molecule going from one chair conformation to the other going through the boat conformation
- After a ring flip the equatorial bonds become axial bonds and the axial ones become equatorial.
- After a ring flip, a bond that was above another bond **stays** above despite the equatorial/axial change. 

   ![ring flip](./img/03_Conformations_Configurations/ring_flip.png)

- Energy of the different conformations:

  ![energy](./img/03_Conformations_Configurations/cyclohexane_energy.png)
  
#### Substituents

- A subsituent (or more) on the ring requires less energy when in equatorial axis than in axial since it is the farther from all other atoms.
- If there are 2 subsituents, one equatorial and one axial, the energy required is lower if the bigger subsituent is equatorial.

|||
|--|--|
|![subsituent](./img/03_Conformations_Configurations/cyclohexane_substituent1.png)|![subsituent](./img/03_Conformations_Configurations/cyclohexane_substituent2.png)

