# Intro

## Carbon

### History

- **Lavoisier**
- **Vital force** theory by **Berezelious** assumes that organic compounds are only formed in living cells and cannot be prepared in labs.
- **Wohler** worked with cyanide and proved the vital force to be wrong. He synthesized organic compound (Urea) from an inorganic compound (Ammonium Cyanate):

```math
NH_4CNO \xrightarrow{heat} NH_2CONH_2
```

- Ammonium cyanite and urea are **isomers**: they have the same formula but arranged differently.
- Organic compounds are used as drugs (haldol, prozac, azt). In cancer research, is being developped molecules targeting **RAS protein** which is found in a lot of types of cancer.

### Basics

- Organic chemestry revolves around compounds containing carbons and hydrogens.
- Some are **hydrocarbons** (contain only carbon and hydrogen).
- Some organic compounds also contain O, N and/or S.
- Atoms in organic compounds make the number of bonds they need to get to the octet state.
- Organic compounds are found in all states, pH, colors, etc.
- Salts are inorganic.

![organic](./img/01_Intro/organic.png)

#### Carbon

- The **bond** between carbon and hydrogen is **non polar covalent** bond and is very _strong_.
- This bond between 2 carbons is also very strong and is even when other atoms are bonded to them.
- **The strength of the carbon covalent bond is why it is a good backbone for all living organisms (we don't need to break apart)**.
- There are several families (alkyne, alkene, alkane).
- Carbon only makes covalent bonds and no ionic bonds.

#### Drawing organic compounds

- Carbons and hydrogen are not indicated by letters.
- Hydrogens are not drawn.
- Carbons are drawn with line (bonds - can be double/tripple bonds).

|||
|--|--|
|![example1](./img/01_Intro/drawing1.png)|![example2](./img/01_Intro/drawing2.png)|


## Orbitals

- Orbitals is an area/path.
- The principle of Heisenberg: we cannot know where an electron will be at any given time.
- For some areas the statistics of finding the electrons there is 0.
- An **electron** is a particle that is also considered a **wave** (like photons). It is called wave-particle duality.
- The length of a bond is the length where the energy is the lowest (energy is released) and the atom is the most stable. 

 ![energy bonds](./img/01_Intro/energy_bond.png)
 
- The energy released when a covalent bond is created is equal to the energy required to break that bond.
- Every bond has its length and energy.

### Sigma bond $`\sigma`$

- Strong bond where the probability of finding the electron is on the imaginary line between the center of the two atoms.
- Energy is released when 2 orbitals overlap.
- The electrons of both atoms in the covalent bond are attracted to both atoms. That's what hold the 2 atoms together.
- The **bigger the overlap between orbitals (the closer the atoms) the lower the energy** until the point where getting the 2 atoms even closer require more energy due to the fact that protons cannot be too close to each other (same with electrons).

### Pi bond $`\pi`$

- The probability of finding the electrons is above or below the imaginary line between the center of the two atoms in the bond.
- It is a bond between **two p orbitals** that were **not hybridized**.
- It is in addition of a $`\sigma`$ bond and is **weaker**.

 ![hybridization](./img/01_Intro/hybridization.png)
 
- Molecules in 3D shapes use $`sp^3`$ hybridization, non-linear 2D shapes $`sp^2`$ and linear $`sp`$ (as long as there's no lone pairs of electrons).
- When talking about carbon:
  - Only $`\sigma`$ bonds: $`sp^3`$ hybridization.
  - 1 $`\pi`$ bond: $`sp^2`$ hybridization.
  - 2 $`\pi`$ bonds: $`sp`$ hybridization.
 
 ![summary](./img/01_Intro/summary.png)
 
## Acids/Bases

- Bronsted-Lowry: proton donors are acids, proton receivers are the bases.
- A conjugate acid/base is the molecule with **one** more/less H proton (for ex: acid:$`NH_3`$ <- conjugate -> $`NH_2^-`$ ).
- Strong acids have weak conjugate bases and vice-versa.
- The acidity of a molecule is due to 2 factors:
  - The more electronegative and atom is the stronger it holds onto the electron of the $`H`$ (make the H $`H^+`$). 
  - The bigger the radius of an atom the weaker the bond between this atom and.
- That's why, when looking at the period table, the atoms on the right and bottom are more acidic (see below): 

 ![acidic/basic strength](./img/01_Intro/acid_base_strength.png)

### Oxoacids

- **Acids that contain OH**.
- Part of them are the carboxyl groups.
- The acidity level depends of what other atom is bonded to the oxygen (in addition to the hydrogen):
  - If the other atom is more electronegative it pulls the charge towards it and the bond between the O and H gets longer thus making it possible for the bond the break.
  
  ![oxoacids](./img/01_Intro/oxoacids.png)

- Because oxygen is electronegative, the more oxygen atoms there are in a molecule, the more acidic the molecule is:
  - $`H_2SO_3 < H_2SO_4`$ 
  - $`HClO_3 < HClO_4`$
  - $`HNO_2 < HNO_3`$

- Common strong acids to know: 
  - HCl
  - HBr
  - HI
  - $`HClO_4`$
  - $`HNO_3`$
  - $`H_2SO_4`$
- $`H_2SO_4`$ can **release 2 protons**. The first one more easily than the second (like ionisation energy).
- Common strong bases to know have OH that is released in water.

### Weak acids and bases

#### Weak Acids

- The higher the $`K_a`$ the stronger the acid.
- The lower the $`pK_a`$ the stronger the acid.
- In reaction between two acids, the strong acid (lower $`pK_a`$) will give its proton to the other acid (which becomes the base of the solution).

```math
\begin{aligned}
&HA_{(aq)} + H_2O_{(l)} \leftrightharpoons H_3O^+_{(aq)} + A^-_{(aq)} \\
&K_a = \frac{[H_3O^+][A^-]}{[HA]} \\
&\bold{pK_a = -log{K_a}}
\end{aligned}
```

#### Weak Bases

- Unlike strong bases that have an OH to release, weak bases are proton receivers (receive $`H^+`$).

```math
\begin{aligned}
&B_{(aq)} + H_2O_{(l)} \leftrightharpoons BH^+_{(aq)} + OH^-_{(aq)} \\
&K_b = \frac{[BH^+][OH^-]}{[B]} \\
&\bold{pK_b = -log{K_b}}
\end{aligned}
```

 ![ionization constant](./img/01_Intro/ionization_constant.png)


### Carboxylic Acids

- **Functional group $`\bold{COOH}`$**.

 ![cooh](./img/01_Intro/cooh.png)

- Carboxylic acids are **weak**. The **only proton** that is released is the carboxyl one (COO**H**).
- The anion created from releasing the proton is in resonance form.

 ![cooh resonance](./img/01_Intro/cooh_resonance.png)
 

### Amines

- Amines are derivatives of **ammonia** $`\bold{NH_3}`$ where one, two or three hydrogens is replaced by an **hydrocarbon**.
- Ammonia itself isn't an organic compound but amines are because of the hydrocarbon(s) attached to it.
- Amines are **weak bases** from the lone pair of electrons of the [N]itrogen.

![amines](./img/01_Intro/amines.png)

### Amino Acids

- In all amino acids there's a **carboxyl group** and **amino group**.
- It is an acid from its carboxyl side and a base from its amino side.

![amino acid](./img/01_Intro/amino_acid.png)

- One molecule of amino acid can react with itself by transfering a proton from the carboxyl group to the amino group (zwitterion=).

![zwitterion](./img/01_Intro/zwitterion.png)
