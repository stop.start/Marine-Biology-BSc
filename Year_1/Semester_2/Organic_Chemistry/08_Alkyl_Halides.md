# Substitution Reactions of Alkyl Halides

- Alkyl Halide is a compound in which a halogen atom has replaced a hydrogen of an alkane:
  - Alkyl fluoride
  - Alkyl chloride
  - Alkyl bromide
  - Alkyl iodide
- C-X bond, where X is a halogen, is a **polar bond**.

```math
{\bold{\delta+} \atop RCH_2 \;\text{---}} {\bold{\delta-} \atop X}
```

- In a **substitution reaction**, the **leaving group** is replaced by another atom or group.
- The **leaving group** is usually an electronegative group.
- There are 2 types of substitution reactions:
  - In one step: 
    - The nucleophile attacks the partially positively charged carbon while the halogen breaks its bond with that carbon.
    - This is called $`\bold{S_N2}`$ reaction: nucleophilic substitution of second order.
  - In two step: 
    1. The carbon-halogen bond breaks forming a carbocation (slow step).
    2. After the first step a nucleophile attacks the carbocation to form a substitution product (fast step). 
    - This is called $`\bold{S_N1}`$ reaction: nucleophilic substitution of first order. Depends only on the concentration of the alkyl halide.
  
| $`S_N2`$                               | $`S_N1`$                               |
|----------------------------------------|----------------------------------------|
| One step                               | Two steps                              |
| No carbocation formed                  | Temporary carbocation                  |
| Second order                           | First order                            |
| ![sn2](./img/08_Alkyl_Halides/sn2.png) | ![sn1](./img/08_Alkyl_Halides/sn1.png) |


## $`S_N2`$ reaction

- The nucleophile attacks the partially positively charged carbon pushing the halogen which breaks its bond with that carbon.
- The nucleophile attacks on the **opposite side** of the leaving group.
- This reaction is concerted (מתוזמן).
- It is a **bimolecular** reaction because the transition state is 2 molecules in one (really unstable).

![bimolecular](./img/08_Alkyl_Halides/bimolecular.png)

- Because one atom enters while the other leaves the reaction depends on both concentrations of the alkyl halide and the nucleophile (second order): **rate = k[alkyl halide][nucleophile]**.
- The mechanism of the reaction depends on the following:
  - The structure of the alkyl halide.
  - The reactivity of the nucleophile.
  - The concentration of the nucleophile.
  - The solvent of the reaction.

### Structure of the alkyl halide

- Methyl alkyl halide: halogen bonded to a $`CH_3`$
- Primary alkyl halide: halogen bonded to a carbon that it bonded to **one other carbon**.
- Secondary alkyl halide: halogen bonded to a carbon that it bonded to **two other carbon**.
- Tertiary alkyl halide: halogen bonded to a carbon that it bonded to **three other carbon**.

![structure](./img/08_Alkyl_Halides/structure_alkyl_halide.png)

- The methyl halide is the most likely to happen as an $`S_N2`$ reaction.
- Secondary and Tertiary are likely not to happen as an $`S_N2`$ reaction. 
- The smaller the alkyl halide the more reactive it is. This is because bigger molecules will have atoms **blocking** the way for the nucleophile to get in requiring a **higher activation energy**.

![size matter](./img/08_Alkyl_Halides/size_matter.png)

- The nucleophile attacks from the back of the molecule causing the configuration to get inverted.
- During the transition state the nucleophile, carbon and halogen and hydrogen/radical are on the same plane
- If the carbon is asymmetric then after the reaction the R/S may be inverted (Walden inversion).

![attack](./img/08_Alkyl_Halides/sn2_attack.png)

![inversion](./img/08_Alkyl_Halides/sn2_inversion.png)

#### Relative rate of reaction of the leaving group

- Iodide reacts the fastest while fluoride reacts the slowest.
- Looking at the acid HX strength can help knowing if the halogen will leave easily (weak base will leave more easily).
- F is small and strong thus won't leave easily (strong base).

### Reactivity of the nucleophile

- Like bases, nucleophiles have a lone pair of electron (or a $`\pi`$ bond).
- Bases attack protons ($`H^+`$) while nucleophiles attack carbon with a $`\delta +`$.
- Bases are measured by the constant Ka while nucleophiles have a **k** constant.
- Nucleophilicity is the tendency of a nucleophile to attack an **sp3 carbon** bonded to a leaving group.
- The stronger bases are the best nucleophiles. For example $`HO^-`$ is a better nucleophile than $`H_2O`$ although in both cases O is the attacker. That's because $`H_2O`$ is a weaker base.

#### Effect of the solvent on nucleophilicity

- In an aprotic solvent (solvent without H with a $`\delta +`$) the strongest bases are the best nucleophiles.
- In a protic solvent (solvent with H with a $`\delta +`$, bonded to a N or O), the **weakest** bases are the best nucleophiles. That's because the strong bases are pulled to the protons and not the alkyl halides.
- Halogens as nucleophiles:

![halogens nucleophiles](./img/08_Alkyl_Halides/halogens_nucleophiles.png)

### Intermolecular vs Intramolecular

- Intermolecular: nucleophile in one molecule will push away a leaving group from another molecule, bonding those 2 molecules together.
- Intramolecular: nucleophile will push away a leaving group within the same molecule transforming into a ring.
- Intramolecular forming 5 or 6 membered ring is highly favored over an intermolecular reaction.

![intra/intermolecular](./img/08_Alkyl_Halides/intra_inter.png)

## $`S_N1`$ reaction

1. The carbon-halogen bond breaks forming a carbocation (slow step) while the halogen keeps the pair of shared electrons.
2. After the first step a nucleophile attacks the carbocation to form a substitution product (fast step). 

- $`S_N1`$ reactions need a stable carbocation. This means that the carbocation can undergo rearrangements (like hybrid shift).
- The rate determining step is the formation of the carbocation. **rate = k[alkyl halide]**.

![steps](./img/08_Alkyl_Halides/sn1_steps.png)

- Those reactions need **polar protic solvent**, usually water.
- Unlike $`S_N2`$ reactions, the tertiary alkyl halides are the most reactive and the methyl and primary the least reactive.
- The most reactive halogen is still iodide and the least reactive is still fluoride since the halogen needs to leave on its own.

![reactivity](./img/08_Alkyl_Halides/sn1_reactivity.png)

- Because the halogens first leaves before the nucleophile attacks, the nucleophile can come from both sides and not just from the back like in $`S_N2`$.
- This means that 2 products can be formed: one in the same configuration and one inverted. This will give a racemic mixture.
- It can form a pair of enantiomers when the leaving group is attached to an asymmetric carbon.

![mechanism](./img/08_Alkyl_Halides/mechanism_sn1.png)

- _Note:_

> If the halogen (leaving group) has not yet diffused away (is still close to the molecule) when the nucleophile attacks.  
> In this case, there is more chance that the attack will be from the back forming an inverted configuration.  

### Benzyl and Phenyl groups

- In phenyl group the benzene ring is directly in contact with the compound like Halogen, Carboxylic Acid, Alcohol, Amide, Nitro and many more. 
  - General formula: $`C_6H_5`$
- In benzyl group the benzene ring is in contact with methyl group which is in contact with the above given group such as Halogen, Carboxylic Acid, Alcohol, Amide, Nitro and many more.
  - General formula: $`C_6H_5-CH_2`$

![phenyl benzyl](./img/08_Alkyl_Halides/phenyl_benzyl.png)

- Benzylic and allylic halides can undergo $`S_N2`$ reactions (unless tertiary) as well as $`S_N1`$ reactions.

![benzylic allylic sn2](./img/08_Alkyl_Halides/benzyl_allylic_sn2.png)

![benzylic allylic sn1](./img/08_Alkyl_Halides/benzyl_allylic_sn1.png)

- **Aryl halide** is when an halogen is bonded to a phenyl ring.
- Vinylic and aryl halides **do not undergo** $`S_N2`$ reactions. That's because there are already a lot of electrons around the halogens thus the nucleophile won't get attracted.
- They also **do not undergo** $`S_N1`$ reactions because of too unstable carbocation.

![vinylic aryl](./img/08_Alkyl_Halides/vinyl_aryl.png)



![summary](./img/08_Alkyl_Halides/summary.png)
