# Unsaturated Hydrocarbons

**Unsaturated hydrocarbons**, unlike saturated hydrocarbons, don't have the maximum number of hydrogens because of the double bonds.  

## Alkenes (Olefins)

- Alkenes are hydrocarbons that contains **double bonds** and their names finish with **ene**.
- Their **general formula** is $`\bold{C_nH_{2n}}`$. Alkanes have a general formula of $`C_nH_{2n+2}`$ which have 2 more hydrogens due to not having double bonds.
- **Cyclic alkenes** have a **formula** $`\bold{C_nH_{2n-2}}`$.
- The molecule is **planar** and the bonds with the carbons in the double are $`120\degree`$.
- Methane, having only one carbon cannot have double bonds with another carbon.
- **Ethylene** is the first **alkene**.
- Because of the double bond there is **no rotation** possible around the bond.
- The **van der Waals** bonds are weaker in alkenes than alkanes because the double bonds **bend the molecules** and cause them to be farther from one another. Because of this, the **boiling point** of the alkenes is **lower** than of the alkanes.
- The double bonds are $`sp^2`$ hybridized, one $`\sigma`$ bond and one $`\pi`$ bond.
- The $`\pi`$ bond is weaker thus can be open (by absorbing heat or light energy) to make chemical reactions. This makes the alkenes more prone to chemical reactions than alkanes.

  ![ethylene](./img/04_Unsaturated_Hydrocarbons/ethylene.png)

### Naming

- Alkenes names finish with **ene**.
- The **longest chain** is the longest chain containing the double bond (even if there's a longer chain that do not contain double bonds). 
- Numbering the carbons: the carbons with the double bonds should have the lower number.
- Before the chain name (like pentene, heptene) the **numbers of the carbons with double bonds** should be specified, unless there one double bonds on carbon 1 (butadiene means that the double bonds is at carbon 1 and that there are no others).
- If there are more than one double bond the **ene** becomes **dien** or **trien**:
  - Two double bonds: pentadiene.
  - Three double bonds: pentatriene.
- The rest of the rules are the same as alkanes (naming the substituents - מתמירים - de/tri, abc rule, etc).

|||
|--|--|
|![naming](./img/04_Unsaturated_Hydrocarbons/naming.png)|![naming2](./img/04_Unsaturated_Hydrocarbons/naming2.png)|


#### Cyclic molecules
- If there's only one double bond, there is no need to specify which carbon it is (it will always be carbon 1).
- If there are substituents, they should have the lowest numbers from the bond.

![cyclo naming](./img/04_Unsaturated_Hydrocarbons/cyclo_naming.png)

#### Vinylic, Allylic

- **Vinylic carbons** are the carbons with **double bonds**.
- **Allylic carbons** are the carbons that are **next to the carbons with double bonds**.
- In this course we use IUPAC naming which this is not.

![vinyl allyl](./img/04_Unsaturated_Hydrocarbons/vinyl_allyl.png)

## Alkynes

- **Alkynes or Acetylenes** are hydrocarbons with a **tripple bond**.
- The suffix is **yne**. _Example: ethyne_.
- Same principles as alkenes for naming the molecules.

## Alkenynes

- A molecule with a double bond and a tripple bond is called **alkenynes**.
- The carbon numbering is done by giving the double and tripple bonds the lower numbers possible with no preference between  double or tripple bond (double bond can have higher or lower number than the tripple bond).
- The **suffix** is first the carbon number with the double bond with the suffix **en** then the carbon number with the tripple bond with the suffix **yne**. _Example: 1-hepten-5-yne_
- If the double and tripple bonds are at equal distance, then the double bond gets the lower number.

## Stereoisomers

- Double bonds cannot rotate like single bonds.
- A molecule with the same name can have a specific subsituents on the above or under the plane formed by a double/tripple bond or ring where it cannot move from one state to the other like with conformations. It happens where there are more than one subsituents. Those are **isomers** and more specifically **stereoisomeres** which are **geometrical isomers**.

  ![no rotate](./img/04_Unsaturated_Hydrocarbons/no_rotate.png)

### Cis/Trans

- There are 2 different configurations of **stereoisomers**:
  - **Cis** where 2 subsituents are both on the same side (above or under) of the ring.
  - **Trans** where 2 subsituents are on different sides of the rinf (one above and one under).
- Cis and trans are put in front of the IUPAC name. _Examples_:
  - _cis-1,2-dimethyl-cyclopropane_
  - _trans-1,2-dimethyl-cyclopropane_

  ![cis trans](./img/04_Unsaturated_Hydrocarbons/cis_trans.png)
  
- Cis and trans are 2 different molecules with different characteristics. _For example:_

| Trans-2-butene                   | Cis-2-butene                     |
|----------------------------------|----------------------------------|
| No moment dipole                 | Moment dipole                    |
| Boiling point = $`0.9\degree C`$ | Boiling point = $`3.7\degree C`$ |
| ![trans-2-butebe](./img/04_Unsaturated_Hydrocarbons/trans2butene.png)|![cis-2-butene](./img/04_Unsaturated_Hydrocarbons/cis2butene.png)|

> _Note:_  
> Taking oil fat which is _cis_ and making it into solid is possible by opening its double bond: this makes margarine.  
> The issue is that the opened bonds can close back and when they do they can close back as _trans_ bonds which are not healthy to the human body.

### E/Z

- The Cahn-Ingold-Prelog priority rules.
- Rules for **naming geometrical isomers**.
- **Z isomers have the highest priority groups on the same side**.
- **E isomers have the highest priority groups on opposite sides**.
- It works by looking at the **priority of the substituents** where the **priority** is given to the atoms with the higher **atomic number**.
- Cis and trans are special cases of E and Z where **cis is Z** and **trans is E** and E/Z can be used instead of cis/trans.
- Tip to remember: Z is on "*__z__*e *__z__*ame *__z__*ide".

|||
|--|--|
|![priority](./img/04_Unsaturated_Hydrocarbons/ez_priority.png)|![ez](./img/04_Unsaturated_Hydrocarbons/ez.png)|

- The double bonds is considered as two bonds to two of those atoms. _For example, C=C is like each C is bonded to two C_. Same for tripple bonds (three times bonded).
- If the atoms directly bonded to the carbons with the double bond are the same, then we look at the atoms that are bonded to them, and so on until there are different atoms which can be compared by their atomic number.
- If there is more than one double/tripple bonds that can be E/Z then they need to be numbered (2E, 4Z).
- **One atom with a higher atomic number has a higher priority than 2 or more atoms with a lower atomic number** but **two atoms of the same priority will have a higher priority than one alone**.

#### Example

![example](./img/04_Unsaturated_Hydrocarbons/ez_example.png)

- Atoms directly bonded the the Carbons in the double bonds: Cl, C, C (double bond) and  C, C, C (double bond).
- Cl is higher priority than the Cs so for the left carbon this substituent has the highest priority.
- The atoms bonded to the atoms bonded to the right Carbon: C, O.
- O has a higher atomic number thus higher priority and this substituent will be used to set the configuration.
