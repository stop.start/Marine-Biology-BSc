# Benzene

![benzene](./img/11_Benzene/benzene.png)


## Intro

- $`\bold{C_6H_6}`$
- **Aromatic rings** are called that way because the first rings discovered had a pleasing smell.
- Aromatic rings are stable due to the resonance.
- **Aromatic rings** rules:
  1. The molecule must have **4n+2** = electrons in the delocalized and conjugated p-orbital cloud (n >= 0 and integer).
  2. The molecule must be planar (all carbon in the ring are $`sp^2`$).
  3. The molecule must be cyclic.
  4. Every atom in the ring must be able to participate in delocalizing.

![aromatic](./img/11_Benzene/is_aromatic.png)

- An **heterocyclic** compound is a cyclic compound in which one or more of the ring atoms is an atom other than carbon (N,O,S). The lone pairs of electrons that are outside the ring do not participate in the resonance.

![heterocyclic](./img/11_Benzene/heterocyclic.png)
![pyridine](./img/11_Benzene/pyridine.png)
![aromatic examples](./img/11_Benzene/aromatic_examples.png)

- An **anti-aromatic** has 4n = conjugated pi electrons in cyclic planar compound. It is really unsatble.

![anti-aromatic](./img/11_Benzene/anti_aromatic.png)

- **Relative stabilities**:

![relative stabilities](./img/11_Benzene/relative_stabilities.png)

## Nomenclature

- If benzene is the **main chain** the name of the substituent is added before "benzene":

![substituents](./img/11_Benzene/substituents.png)

- Some compounds have special names and do not follow the above rule:

![special names](./img/11_Benzene/special_names.png)

- A **phenyl group** is a substituted benzene ($`C_6H_5`$).

![phenyl group](./img/11_Benzene/phenyl.png)

- An **aryl group - Ar** is a **general name** for a phenyl group with a substituent, the same way **R** is a general name for **derivatives of alkane**. For example **ArOH** can be any of those:

![aryl group](./img/11_Benzene/aryl.png)


## Benzene Reactions

- Benzene is rich in electrons.
- Benzene can undergo **electrophilic aromatic substitution reactions**: an H will be replaced by an electrophile.

![aromatic substitution](./img/11_Benzene/aromatic_substitution.png)
![carbocation](./img/11_Benzene/carbocation.png)

- $`Y^+`$ usually comes from a compound YZ forming $`Z^-`$. When the carbocation is formed $`Z^-`$ could be attracted by the + of the carbocation but it won't because:
  - With the Z as a substituent the ring isn't aromatic anymore.
  - Aromatic products are more stable.
- Benzene don't undergo electrophilic addition reaction where [H]ydrogen are NOT replaced but instead Y and Z are added to the ring which loses a double bond.

![substitution](./img/11_Benzene/substitution.png)

- During the reaction the carbocation is stabilized by resonance.

![carbocation resonance](./img/11_Benzene/carbocation_resonance.png)


- Types of reactions:

![types of reaction](./img/11_Benzene/reactions.png)

### Halogenation of Benzene

- Halogenation requires a **Lewis acid catalyst** because the benzene being aromatic causes it to be less reactive than an alkene.

![halogenation](./img/11_Benzene/halogenation.png)

- Ferric bromide ($`\bold{FeBr_3}`$) and ferric chloride ($`FeCl_3`$) are usually used. They help make $`Br_2`$ or $`Cl_2`$ polar which help form an electrophile.

![catalyst](./img/11_Benzene/catalyst.png)

#### Mechanism

![halogenation mechanism](./img/11_Benzene/halogenation_mechanism.png)

- With Iode which uses $`H_2O_2 + 2H_2SO_4`$ as a catalyst:

![iodination mechanism](./img/11_Benzene/iodination.png)


### Nitration

![nitration](./img/11_Benzene/nitration.png)

- Intuitively the acid $`HNO_3`$ becomes $`NO_3^-`$, but in order to make $`HNO_3`$ into $`NO_2^+`$ a really strong acid is used: $`H_2SO_4`$.
- $`H_2SO_4`$ releases an $`H^+`$ which attaches to an oxygen making OH a good leaving group and release $`H_2O`$

![no2](./img/11_Benzene/no2.png)

#### Mechanism

![nitrobenzene](./img/11_Benzene/nitrobenzene.png)

### Sulfonation

![sulfonation](./img/11_Benzene/sulfonation.png)

- When using a solution of the strong acid sulfuric acid $`H_2SO_4`$ they can give to each other H, releasing $`H_2O`$ and making $`SO_3H^+`$:

![sulfonium](./img/11_Benzene/sulfonium.png)

#### Mechanism

![sulfonation mechanism](./img/11_Benzene/sulfonation_mechanism.png)

#### Sulfonic Acids

- Sulfonic acid $`SO_3H`$ is also a strong acid since it results in an ion with resonance.

![so3](./img/11_Benzene/so3.png)

### Friedel-Crafts acylation

- Acyl: R-C=O
- The product of this acylation is a **ketone**.
- The acyl group is taken from either an **acyl halide** or an **acid anhydride**.

![friedel-crafts acylation](./img/11_Benzene/friedel_crafts_acylation.png)

- A Lewis acid ($`\bold{AlCl_3}`$) is required for the reaction to take place.

![acylium ion](./img/11_Benzene/acylium.png)

#### Mechanism

![acylation](./img/11_Benzene/acylation.png)


### Friedel-Crafts alkylation

- Alkyl: R
- It produces and **alkylbenzene**.
- The alkylated benzene is **more reactive** and can undergo more alkylations.
- The source of the alkyl is taken from an alkyl halide.

![friedel-crafts alkylation](./img/11_Benzene/friedel_crafts_alkylation.png)

- A Lewis acid ($`\bold{AlCl_3}`$) is also required for the reaction to take place.

![carbocation](./img/11_Benzene/fc_carbocation.png)

#### Mechanism

![alkylation](./img/11_Benzene/alkylation.png)

- The carbocation $`R^+`$ can undergo rearrangements thus the R attaching to the benzene ring might not be the same we started with.
- Because the catalysts the carbocations are not pure carbocations thus primary carbocation can be used as a substituent. The catalyst stabilizes a bit the carbocation.
- Altough primary carbocation can be used as a substituent, secondary will be more produced and if the option is primary/tertiary then tertiary will always be used.

![alkylation](./img/11_Benzene/alkylation2.png)
![alkylation](./img/11_Benzene/alkylation3.png)


#### Putting a straight alkyl chain on a ring

- Because the chain can undergo rearrangements when in carbocation form, in order to attach a specific chain to a benzene ring it is better to do a Friedel-Crafts acylation reaction.
- Once the acyl is attached to the ring, $`\bold{H_2}`$ is used to reduce to product and remove the O leaving only the alkyl chain.

![alkyl chain](./img/11_Benzene/alkyl_chain.png)

#### Oxidation

- Alkyl substituents can be oxidized into carboxyl groups

![alkyl oxidation](./img/11_Benzene/alkyl_oxidation.png)

### Reactions summary

![summary](./img/11_Benzene/reactions_summary.png)


## Nomenclature for Disubstituted Benzene

- Benzenes can have more than one substituent.
- Two substituents on a benzene can form different compounds depending on where they are placed.
- Always one of the substituent will be on carbon 1 and the second is numbered relative to the first.
- In addition to numbers, prefixes can be used which indicates the position of the second substituent relatively to the first. 

![prefixes](./img/11_Benzene/ortho_para_meta.png)

- Examples with numbers and prefixes:

![numbers vs prefixes](./img/11_Benzene/numbers_prefixes.png)
![numbers vs prefixes](./img/11_Benzene/numbers_prefixes2.png)

- With more than 2 substituents, the regular rules of nomenclature in rings apply.

## Effect of Substituents on Benzene

- Some substituents cause the benzene ring to be more or less reactive.
- **Activating substituents** (מתמיר מאקטב): they **give more electrons** to the ring will cause the ring to **react more** (makes to ring more nucleophile).
- **Deactivating substituents** (מתמיר מעכב): they **pull electrons** from the ring will cause the ring to **react less**.
- For example, Cl will pull electrons since it is very electronegative and alkyl groups will push electrons.
- The electrons can be pushed or pulled via both types of bonds $`\pi`$ and $`\sigma`$.
- Through $`\sigma`$ bond:
  - Pushing electrons is called **inductive electrons donation**.
  - Pulling electrons is called **inductive electrons withdrawal**.

- The below table shows which substituents are activators and deactivators.
- Activators:
  - Strong: substituents with N or O directly attached to the ring.
  - Moderate: N or O with a carbonyl group.
  - Weak: carbons
- Deactivator:
  - Strong: With "+", triple bond or a lot of o ($`SO_3H`$)
  - Moderate: Carbonyl group directly attached to the ring.
  - Weak: Halogens (F, Cl, Br, I)

![activators deactivators](./img/11_Benzene/activation.png)

- **All activators and weak deactivators will direct a new substituent to either ortho or para position**. 
- **Moderate and strong deactivators will direct a new substituent to meta position**.

![activator](./img/11_Benzene/activator.png)

![deactivator](./img/11_Benzene/deactivator.png)

- **There will be no acylation or alkylation if there's a strong deactivator on the ring**.

