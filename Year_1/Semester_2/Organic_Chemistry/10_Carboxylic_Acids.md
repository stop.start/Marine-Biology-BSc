# Carboxylic Acids

## Carbonyl Group

- **Carbonyl group**: carbon with a double bond to an oxygen.

![carbonyl examples](./img/10_Carboxylic_Acids/carbonyl_examples.png)

- **Acyl group** is composed of a **carbonyl group (C=O)** attached to an **alkyl (R) or aryl (Ar) group***.
- Depending on the electronegativity of the substituents attached to the acyl group the molecule will be more or less reactive: more electronegative will tend to leave.

![classes](./img/10_Carboxylic_Acids/carbonyl_classes.png)

- **Aldehyde** and **ketone**:

![aldehyde ketone](./img/10_Carboxylic_Acids/ketone.png)

## Carboxylic Acids

- Contains **COOH**.
- Planar triangular (because of the double bond C=O).

### Fatty Acids

- **RCOOH**
- R has usually between 4 and 24 carbons with single or double bonds between them
- Common notation for fatty acids: 
  - Count from the carbon which is the farthest from the carboxyl group.
  - **Cx:yWz_Config_**
    - x: number of carbons in the chain.
    - y: number of double bonds.
    - z: location of the first double bond.
    - config: cis/trans of the first double bond.
    - _Example: C18:1w9Cis: 18 carbons, 1 double bond between carbon 9 and 10 in cis configuration._

### Nomenclature

- Naming: suffix **oic acid**: _example: methane $`\Rightarrow`$ methanoic acid_.
- If the carboxyl group is not part of the parent chain the suffix **carboxylic acid** is used: _example: cyclohexane $`Rightarrow`$ cyclohexanecarboxylic acid_.
- A lot of acids are named by their common name but in this course we use IUPAC.
- **Acid anhydric**: 2 carboxylic acids molecules put together by loss of water.
  - If the 2 molecules are the same then the name will be the name of the acid + anhydride: _ethanoic anhydride_
  - If they are different then the 2 names are put before anhydride: _ethanoic methanoic anhydride_
  
  ![anhydrides](./img/10_Carboxylic_Acids/anhydrides.png)
  
### Esters

- Instead of COOH $`\rightarrow`$ **COOR'**.

#### Nomenclature

- Main chain contains the R and the C from the C=O. 
- The substituent is R' (the R that is bonded the the O which is single bonded to the carboxylic carbon)
  
  ![esters](./img/10_Carboxylic_Acids/esters.png)
  
### Acyl Halides

- The H of COOH is replaces by an halogen: 

![acyl halides](./img/10_Carboxylic_Acids/acyl_halides.png)

- Suffix **yl** (and not oic): _ethanoyl chloride_.
- For acids ending with _carboxyl acid_ it is replaced with **carbonyl halide**.

### Amides

- $`NH_2`$, $`NHR`$, $`NR_2`$ instead of the OH group.
- _oic acid_ is replaced with **amide**

![amides](./img/10_Carboxylic_Acids/amides.png)

#### Nomenclature

- If there's a substituent on the [N]itrogen it is placed first.
- If there's more than one substituent on the [N]itrogen they are named in alphabetic order.
- All substituents bonded to the [N]itrogen are named with the prefix "N-"
- After those substituents the name of the amide.

![nomenclature](./img/10_Carboxylic_Acids/amides_names.png)

## Priority

![priority](./img/10_Carboxylic_Acids/amides_names.png)

## Reactions

- The oxygen is more electronegative than the carbon thus causing the **carbonyl carbon** to be **electrophile**.

![reaction](./img/10_Carboxylic_Acids/reactions.png)

- If the **nucleophile Z** is a **weaker base** than Y, it won't stay and the molecule will be reformed.

![non reaction](./img/10_Carboxylic_Acids/non-reaction.png)

- If the **nucleophile Z** is a **stronger base** than Y, Y will leave.

-If Z and Y have are bases of similar strength there will be and equlibrium (there will be a mixture of old and new molecules).
