# Photosynthesis (Dark Reactions)


## Flowers

- Flowers can be **bisexual** or **unisexual**:
  - Bisexual: 
    - The flower has both male and female reproductive systems.
    - The male reproductive system is called **stamen** and produces **pollen** (in hebrew this is called to be **אבקנים**)
    - The male and female reproductive systems don't work at the same time. This prevent selfpollination.
  - Unisexual: 
    - The flower has only male or female reproductive system but not both.


## Photosynthesis - Dark Reactions/Calvin Cycle

- It is the step that makes **sugar** from $`CO_2`$ using ATP and NADPH.
- Happens in the **stroma** (in the chloroplast but outside of the thylakoid).
- **Carbon fixation**: incorporating $`CO_2`$ into organic molecules.

![photosynthesis](./img/08_Photosynthesis/photosynthesis.png)

### Calvin Cycle Steps 

![calvin cycle](./img/08_Photosynthesis/calvin_cycle.png)

#### First stage - Carboxylation

- $`CO_2`$ enters the cycle and is fixed to **RuBP**. This forms a **6 carbon** compound.
- This 6 carbon compound is **hydrolyzed** and generates **2** molecules of **PGA** (3-phosphoglycerate). 
- Each PGA contains **3 carbons**. That's the reason the Calvin cycle is also known as the **C3 pathway**.
- **Rubisco** (RuBP carboxylase/oxygenase) is the enzyme that catalyzes the reaction of carbon fixation.
- Rubisco can function with both carbon and oxygen. **Carbon is best**.
- The rubisco enzyme hasn't evolve much.

![pga](./img/08_Photosynthesis/pga.png)

#### Second stage - Reduction

- The PGA are reduced to **PGAL** (glyceraldehyde 3-phosphate):
  1. Get another [P]hosphate from ATP (which becomes ADP).
  2. Reacts with NADPH to become PGAL (NADPH becomes NADP+ and one phosphate is freed).

![pgal](./img/08_Photosynthesis/pgal.png)

#### Third stage - Regeneration

- 5 of 6 molecules of PGAL are used to regenerate 3 molecules of RuBP.
- That's why it's needs 3 cycles each time:
  - Each cycle produces 2 PGAL (2x3=6 carbons).
  - 3 cycles --> 3x6=18 carbon.
  - RuBP has 5 carbons: 5 PGAL of 3 carbons each can be transformed into 3 RuBP of 5 carbons each.
  - The last PGAL is put aside. After 3 more cycles, 2 PGAL are free to be used to form glucose (so glucose is formed each 6 cycles).

## C3 Plants

- 80% of plants.
- C3 plants keep they **stomata** (פיעונית):
  - **Open during the day**.
  - **Close at night**.
- During the day $`CO_2`$ can get in and oxygen out.
- On hot and dry days, **water loss due to transpiration**.
- Those conditions decrease levels of photosynthesis because the plants **close their stomata** to prevent water loss.
- Light reaction continue to produce $`O_2`$ but because the stomata are close, the oxygen stacks up inside the plant.
- Because $`CO_2`$ cannot enter its concentration decreases while the concentration of oxygen increases and, at some point, rubisco will fixate oxygen instead of carbon dioxide. This is called **photorespiration**.

### Calvin Cycle with oxygen

- With oxygen fixation instead of carbon fixation to the RuBP, **only one PGA** can be produced since it misses one carbon.
- Instead of the second PGA, **glycolate** is produced (2 carbon molecule).
- The productivity is cut by 50%.

![photorespiration](./img/08_Photosynthesis/photorespiration.png)


## C4 Plants

- 15% of plants.
- Example: corn plants and sugar cane.
- **Stomat partially closed during the day and night**.
- Light reaction still in the mesophyll cells. **Calvin cyle in the bundle sheath cells** (cells around the vascular bundles).

### Steps

- **PEP carboxylase enzyme** fixes $`CO_2`$ into temporary 4 carbon molecule called **malate**. This 4 carbon compound is the reason why this pathway is called C4.
- Malate is then transported to specialized cells (bundle sheath) where the Calvin cycle takes place.
- Once in the bundle sheath cells, malate releases the trapped $`CO_2`$ to run the Calvin cycle as usual where rubisco binds to the $`CO_2`$.

![c4 pathway](./img/08_Photosynthesis/c4.png)

- C4 is more efficient than C3 under high light and temperature conditions because it eliminates the possibility of $`o_2`$ fixation.
- PEP carboxylase is also faster at pulling in $`CO_2`$ so the stomata don't have to be open too long thus retaining more water.
   
   
**Note: At high $`\bold{CO_2}`$ availability, C3 is more efficient than C4 at all temperatures (regarding photosynthesis only)**  

## CAM plants

- 5% of plants.
- Example: cactuses, agaves, bromeliads
- In hot and dry environment.
- **Stomata closed during the day and open at night** to prevent water loss.
- Light reaction during the day.
- Calvin cycle is done when $`CO_2`$ is available.
- CAM plants grow slowly (because of the above).

### Steps

- $`CO_2`$ fixation happen at night by PEP and produces malate (process is similar to the C4 pathway).
- The malate is then transformed into **malic acid** and **stored in the vacuole**.
- During the day, when the light reaction can be done and produce the ATP and NADPH required for the dark reactions, the malic acid is transformed back into malate and used as usual.

![cam pathway](./img/08_Photosynthesis/cam.png)

## Summary

![summary](./img/08_Photosynthesis/summary.png)

- In addtion:
  - C4 pathway: separates reactions by space (different locations).
  - CAM pathway: separates reactions by time (day and night).
