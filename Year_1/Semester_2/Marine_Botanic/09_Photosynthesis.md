# Photosynthesis (Marine)

## Photoinhibition

- Photosynthesis depends on visible light.
- **PAR (Photosynthetically Available Radiation)**: quantity of light that stimulates photosynthesis.
- **P-I Curve** is the relationship between photosynthesis and irradiance (PAR).
- **Photoinhibition**: reduction of the photosynthesis due to too much light.
- PSII is more sensitive to light thus too much light damages PSII.
- In living organisms photoinhibited PSII are repaired (up to a certain point) by degradation and synthesis of the D1 protein.

![p-i curve](./img/09_Photosynthesis/pi_curve.png)
![p-i curve](./img/09_Photosynthesis/pi_curve3.png)


## Adaptation, Acclimation, Regulation in Algal Photosynthesis

- **Regulation**: happens on the spot, for example closing the stomata.
- **Acclimation**: takes, hours or days, for example synthesis of non-photosynthetic pigments (chromoplasts) that absorbs excess of light.
- **Adaptation**: evolution over generations.

### Adaptation

#### Light-harvesting pigments and associated proteins

- $`\beta\text{-Carotene}`$ absorbs the excess of light.
- Chlorophyll a/b, Phycobilin, Fucoxanthin-cholorophyll, Peridinin-cholorophyll are 4 types of complexes that absorbs light in order to protect the algae.
- **Phycobilin**:
  - THey are found in **cyanobacteria** and in **red algae**. 
  - Other photosynthetic bacteria use either light harvesting cholorophyll or chlorosomes.
  - **Phycobilisomes** are the pigments of the phycobilin complexes.
- **Fucoxanthin**: 
  - It is a xanthophyll pigment.
  - It is found in **brown algae**.
- **Peridinin**:
  - 

#### Electron Transport Chain

- The transfer of electron can be made more or less effecient depending on the conditions.

#### Xanthophyll Cycle Pigment

![xanthophyll](./img/09_Photosynthesis/xanthophyll.png)

#### Rubisco

#### Phosphoglycolate

- Glycolate is a 2 carbon chain from the rubisco oxygenase (with $`O_2`$ instead of $`CO_2`$).

#### Enzymes for protection from photooxidation stress

- **ROS (Reactive Oxygen Species)**
  - **Singlet oxygen**
  - **Hydrogen Peroxide**
  - **Superoxide**
- The protective enzymes remove ROS

### Photoadaptation

- **Short-term photoacclimation** (sec-min): synthesis of more enzymes in the calvin cycle for example to produce more PGAL, or synthesize more rubisco to make more PGA.
- **Long-term photoacclimation** (hours-days): example: change the structure of the leaves (make it smaller).

![light acclimation](./img/09_Photosynthesis/light_acclimation.png)

## $`CO_2`$

- $`CO_2`$ in water is dissolved.
- **It can also be found as bicarbonate or carbonate depending on the pH**.

![co2](./img/09_Photosynthesis/co2.png)

- **CCM - CO2 concentration mechanism**: increase the concentration of $`CO_2`$ for Rubisco.
- **Types of CCM**:
  - Based on biochemical mechanisms such as C4and CAM photosynthesis pathway.
  - Active transport of Ci (carbon inorganic) across membranes.
  - Processes involving localized enhancement of the CO2concentration by acidification of a particular cellular compartment.

### C4 and CAM

- In C4 and CAM PEP carboxylase uses **bicarbonate** $`HCO_3^-`$ as its primary substrate for fixation of $`CO_2`$ into malate.
- This means that $`CO_2`$ entering must be **hydrated** by a **carbonic anhydrase (CA)** in order to be converted into $`HCO_3^-`$.

### Cyanobacteria

- Have a microcompartment: **carboxysome** that intakes and converts $`HCO_3^-`$ to $`CO_2`$.
- At least 5 distinct Ci transport systems are known for cyanobacteria.

### Eukaryotic algae

- Relies on the pH gradient set up across the chloroplast thylakoid membrane in the light.
- In the light the pH changes accross the thylakoid membrane.
- The pH difference is important since the convertion from $`HCO_3^-`$ to $`CO_2`$ is about 6.3.