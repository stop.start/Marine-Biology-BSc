# Plant Anatomy


- **Anatomy**: structure of a plant and its cells and tissues.
- **Morphology**: physical shape/appearance of the plant.
- **Physiology**: functions of the cells, tissues and how they works from a physics/chemistry point of view.

## Cells

- Basic building blocks.
- If multicellular plant then there's communication between the cells.
- Together they form the tissues, organs etc.

## Plant organs

- 3 organs:
  - **Root - שורש**: **Anchors** the plant in the soil and **absorbs** as well as **transport water/nutrients** from it. It is also used as **storage**.
  - **Stem - גיבול**: Support, water/nutrients transport. Sometimes can do photosynthesis.
  - **Leaves - עלים**: Photosynthesis.
- All 3 organs have the following tissues:
  - **Dermal** tissues: External tissue.
  - **Vascular** tissues: Transport system..
  - **Ground** tissues: All that is found in between the dermal and vascular tissues.

![plant organs](./img/02_Plant_Anatomy/plant_organs.png) ![plant organs2](./img/02_Plant_Anatomy/plant_organs2.png)

- Some terms to know:
  - **Bud - ניצן**: French: bourgeon.
  - **Apical bud**: Also called terminal bud. The bud at the top of the plant. This bud can turn into anything. If cut the plant will stop growing tall and will grow large.
  - **Auxillary bud**: Bud on the plant which isn't the one at the top.
  - **Primodia** is found deep in the buds there's **undifferentiated tissue** that can become anything.
  - **Leaf primodia** is only partly undifferentiated, it will be a leaf but leaves have different tissues.
  
## Tissues

### Dermal tissues

- **Epidermis** layer that covers all the plant's organs.
- **Cuticle** layer that covers all that is above ground (stems and leaves). It is a waxy layer that:
   - Prevents water loss when absorbing $`CO_2`$.
   - Prevents too much water to get into the plant.
   - Prevents scratches from sand or other.

![dermal](./img/02_Plant_Anatomy/dermal_tissue.png)

- **Trichomes** (trikhoma): It is one big cell. There are several types:
  - **Epidermal hair**: prevent water loss.
  - **Glandular hair**: prevent herbivors to eat them.
  - **Root hair**: helps absorbing water.

 ![trichomes](./img/02_Plant_Anatomy/trichomes.png)


- **Cambium** is a tissues that produces specific tissue. For example, **cork cambium** produces cork cells.
- **Cork cells** are dead cells. They can be removed and new cork cells will grow as long as the **cambium** layer isn't removed.

 ![cambium](./img/02_Plant_Anatomy/cambium.png)


### Vascular tissues

- **Xylem - עצה**: Conducts nutrients from the roots up to the top of the plant.
- **Phloem - שיפה**: Conducts sugars from the photosynthesis in the leaves to the rest of the plant.

 ![phloem xylem](./img/02_Plant_Anatomy/phloem_xylem.png)

#### Phloem - שיפה

- **Live** cells that **transport organic meterials** like sugar from the leaves to the rest of the plant.
- They **lack nucleus and organelles** thus cannot divide.
- **Sieve plate** serves as a filter between phloem cells. It blocks big hunks or intruder (animals) thus protecting the rest of the plant.
- **Companion cell** decides what goes in and out.
- **Sieve tube member - STM** phloem cell surrounded by sieve plate and companion cell.

|||
|--|--|
| ![phloem](./img/02_Plant_Anatomy/phloem.png)|![phloem](./img/02_Plant_Anatomy/phloem2.png)|

#### Xylem - עצה

- **Dead** cells consisting of only a cell wall.
- **Transport water and minerals** from the root up to the rest of the plant.
- Two types of xylem cells:
  - **Tracheids**: long thin tube without perforations at the ends.
  - **Vessel elements**: short wide tubes perforated at the ends that make up the vessel (like a pipe).
- Vessel elements together make up a long **pipe** and tracheids at the end close up this pipe.
- Tracheids and vessel elements have **pits** (do not confuse with pith!) on the walls.
- **Pits** break air bubbles to smaller bubbles so they won't get bigger which could prevent the water from reaching the top of the plant.

 ![xylem](./img/02_Plant_Anatomy/xylem.png)
 
### Ground tissues

- Ground tissue that is above ground (not in the roots) is where photosynthesis is done.
- Ground tisse in the root is where storage is found.
- There are 3 types of ground tissue:
  - **Parenchyma**: Standard cells with thin walls.They carry out most of the functions in the plant including photosynthesis and storage. Two types:
      - **Palisade** parenchyma: more elongated cells.
      - **Spongy mesophyll**: spheric cells.
  - **Collenchyma**: Elongated cells with thick primary walls. They provide structural support to the growing body of the plant. The cell wall are non-lignified (lignin is a hard compound that prevent the cells to elongate).
  - **Sclerenchyma**: Mechanical support especially to the parts that are no longer elongating. Two types of cells:
    - **Sclereids** throughout the plant.
    - **Fibers** narrow and elongated cells, commonly associated with vascular tissues.
  
  ![ground tissues](./img/02_Plant_Anatomy/ground.png)

## Leaves

- Leaves are responsible for the photosynthesis.
- Because too much light can be detrimental to the plant, in areas with a lot of Sun the leaves will be smaller in order to reduce the surface area and water loss.
- Some special leaves make sugar only for the seeds.
- **Blade**: flast extended area of the leaf.
- **Veins - עורקים**: vascular tissues (xylem and phloem - עצה ושיפה).
- **Petiole**: part that connects the blade to the stem and transport material.

### Structure

![structure](./img/02_Plant_Anatomy/leaf_structure1.png) ![structure](./img/02_Plant_Anatomy/leaf_structure2.png)

- **Mesophyll tissue**: parenchyma tissue that perform photosynthesis.
- **Stomata - פיונית**: open and close in order to let $`CO_2`$ in.
- **Guard cell**: part of allows to open and close the stomata.
- There are more stomata on the down side of the leaf in order to minimize water loss when they open (the upside being in contact with sunlight).
- **Flag leaf**: contribute the assimilation products to grain for example in wheat.

## Stem

 ![stems](./img/02_Plant_Anatomy/stems_types.png)
 
- **Apical meristem** produces leaf and bud primodia.
- Lateral buds and leaves grow out of the stem at intervals called **nodes**
- The intervals on the stem between the nodes are called **internodes**. 
- The number of leaves that appear at a node depends on the species of plant; one leaf per node is common, but two or or more leaves may grow at the nodes of some species. 
- **Phytomere** is the unit consisting of **a node attached to a leaf, the internode below it and the auxillary bud at the base of the internode**.

 ![stem](./img/02_Plant_Anatomy/stem.png)
 
### Stem growth 

- **Apical meristem** undifferentiated cells. Can become anything.
- **Primary meristem** tissue is partly differentiated and can be divided into 3:
  - **Protoderm** which will become dermal tissue.
  - **Ground meristem** which will become ground tissue (parenchyma, collenchyma, sclerenchyma).
  - **Procambium** tissue which will become vascular tissue.
- **Primary tissues**: 
  - Epidermis from the protoderm.
  - Ground tissue from the ground meristem.
  - Vascular tissue from the procambium tissue.

 ![stem growth](./img/02_Plant_Anatomy/stem_growth.png)
 
### Anatomy
 
 ![basic anatomy](./img/02_Plant_Anatomy/stem_basic_anatomy.png)
 
- Stems are composed of epidermis, vascular bundles, cortex and pith where cortex and pith are the ground tissues.
- **Cortex**: 
  - Ground tissue around the vascular tissues (between the epidermis and vascular bundles).
  - Also present in roots.
- **Pith**: 
  - Ground tissue at the center and the stem.
  - Can be hollow (like in bamboo) if the growth the pith is too slow compared to the growth of the surrounding tissues.
  - Usually roots don't have pith but some do.
- Cortex and pith are both mainly composed of parenchyma cells.
- There are several arrangements of the tissues in stems depending on the plant.

 ![anatomy](./img/02_Plant_Anatomy/stem_anatomy.png)
 
- Monocots and dicots have different stem structure.

 ![cots](./img/02_Plant_Anatomy/cots.png)

### Some stems

- Potatoe are thickened stems.
- See more in Lecture 2, slides 46-50.

## Trees 

- In some trees, we can see rings that are the xylem cells, and in between phloem cells.
- It takes around one year for each ring to form.
- The thickness of the phloem rings shows how much rain there was during its year.
- The outer most ring is the dermal layer.
- **Vascular cambium** is found in woody stems, in the middle of the vascular bundle between the xylem and phloem.

||||
|--|--|--|
|![tree tissues](./img/02_Plant_Anatomy/vascular_tree.png)|![vascular cambium](./img/02_Plant_Anatomy/vascular_cambium.png)|![tree rings](./img/02_Plant_Anatomy/tree_rings.png)|

![tree growth](./img/02_Plant_Anatomy/tree_growth.png)


## Roots

- Roots **anchors** the plants.
- **Absorbs water and dissolved minerals** that will be passed on to the rest of the plant.
- Are used for **storage** (sugars surplus, starch).
- The cortex makes up most of the ground tissue in most roots.
- **Root hairs**: increase the surface area of the root in order to absorb better water and nutrients.
- Roots need air thus too much water in the ground can kill the plant.

|||
|--|--|
|![root anatomy](./img/02_Plant_Anatomy/root_anatomy1.png)|![root hairs](./img/02_Plant_Anatomy/root_hairs.png)|
 
- **Apoplastic pathway**: water goes in between the cells.
- **Sympathic pathway**: water goes through cells, from cell to cell.
- Once the water is within the **casparian strip** and **pericycle** ring it cannot go out and will go up to the rest of the plant through xylem cells.
- **Casparian strips** are part of the endodermis (innermost layer of the cortex).

 ![root anatomy](./img/02_Plant_Anatomy/root_anatomy2.png)
 

## Leaf vs Stem vs Root

- In leaves the ground tissue is mainly mesophyll which is responsible for the photosynthesis.
- In stems the ground tissue is made up of cortex and pith with the vascular bundles arranged in circle in between them.
- In root the ground tissue is only made up of cortex which surrounds the xylem and phloem.

 ![leaf stem root](./img/02_Plant_Anatomy/leaf_stem_root.png)
 
## Water transport

- Transporting water in plants uses 2 forces:
  - **Cohesive force**: molecules sick to each other.
  - **Adhesive force**: molecules stick to the surrounding wall (in plants: xylem).
- **Transpiration**: because of those forces, when the stomata (פיונית) open then water escapes and pulls more water with it. That's how the water moves up. If the plant doesn't have enough water it can't perform photosynthesis because it cannot allow itself to lose water.

## Flowers

![flowers](./img/02_Plant_Anatomy/flowers.png)

## Sugar transport

- Sugars are made in leave mesophyll cells and go through the phloem in vascular bundle.
- In order for the sugar to go through the phloem it requires energy (ATP).

## Phytohormones

### Plant hormones

- They are compounds produced by plants.
- Effective in low concetrations.

![hormones](./img/02_Plant_Anatomy/hormones.png)

#### Auxin (or IAA - Indole-3-acetic acid)

- The most naturally common is IAA.
- Plant **growth and development**.
- Help with defense responses: auxin will make the cell wall thicker.
- Promotes **cell growth**.
- Controls **fruit development**.
- The plant will grow in direction of the sun and the auxin is responsible for that.

#### Gibberellic acid (GA)

- **Growth regulator**.
- Used in modern agriculture for making products bigger.
- In addition to growth it plays a major role in diverse growth processes:
  - **Seed development**.
  - **Organ elongation**.
  - **Control of flowering**.

  ![corn](./img/02_Plant_Anatomy/corn.png)
  
- Raisins without seeds won't secrete a lot of gibberellic thus won't grow much. In order to get bigger raisins gibberellic is sprayed on them.

#### Cytokinin

- Responsible for **cell division**.
- Implicated in:
  - Germination
  - Root and shoot meristem function
  - Senescence (aging)

#### Abscisic acid (ABA)

- Responsible for the seed not to germinate on the plant.
- Seed dormancy
- Drought responses: by closing the stomata ( פיונית) in order to not lose water but this means that the plant cannot perform photosynthesis.
- Other growth processes.

#### Ethylene

- Responsible for **fruit ripening**.
- Rotten fruits secrete more ehtylen.
- Some fruits secretes more ethylene than others (for example: apples).
- Promotes (acetylene) **flowering**.

#### Jasmonic acid (JA)

- Important plant signaling molecules that mediate biotic and abiotic stress responses as well as aspects of growth and development.

#### Salicylic acid

- Central role in **plant defense response**.

#### Brassinosteroids (BRs)

- Nothing was said about it.
