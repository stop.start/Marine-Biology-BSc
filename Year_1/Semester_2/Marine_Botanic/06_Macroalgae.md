# Macroalgae, Seaweeds, Mangroves and Salt Marshes

**Algae are not a taxonomic division**  

**The classification scheme is constantly updated:**

![divisions update](./img/06_Macroalgae/divisions_update.png)

### Macroalgae and Seaweeds

- Macroalgae and seaweeds are the same.
- From the sea.
- They are the **bigger primary producers of the sea**.
- Seaweeds are more complex than unicellular algae but simpler than land plants.
- There are **3 types**:
  - Green - Chlorophyta
    - Example: Ulva (sea lettuce)
  - Brown - Phaeophyta
    - Example: Kelps
  - Red - Rhodophyta
  
#### Anatomy

- The body of a macroalgae is called **thallus**, no matter the shape.
- What appears to be leaves **are not**. When looking in the microscope no xylem, phloem, etc can be seen.
- The leaves lookalike are called **blades** which is also the **main photosynthetic region**.
- They also don't have stems or roots.
- Most seeweeds (like kelps) have a **bladder** full of gas that help them stay up straight and not fall down which allow them to maximize unlight exposure.
- The **stipe** gives support.
- **Holdfast**: looks like root but isn't. It helps hold the seeweed to the bottom but does not gather nutrients. It cannot go through sand or mud.

![anatomy](./img/06_Macroalgae/anatomy.png)

- They don't have hormones.
- How the products from the photosynthesis is passed from cell to cell by diffusion.

#### Morphology

There are **6 types** of morphologies:


||Sheet like| Filamentous group | Coarsely branched group| Thick leathery group | Crustose group| Jointed Calcareous|
|--|--|--|--|--|--|--|
| Toughness| Soft| Soft| |Tough (need a knife to cut it)| Really tough| Really tough|
| Photosynthetic activity| High| Moderate| | Low|Low|Low|
| Other characteristics| Thin (like paper)| Delicate branches (looks a bit like grass)| Coarsely branched. Looks a bit like hair. Pseudoparenchymatous (has something that looks like parenchyma)| Some are calcified. Stony texture| Calcareous. Calcified segments. Stony texture|
| Example| Ulva| Chaetomorpha||Fucus|| Corallina |
| Picture| ![sheet](./img/06_Macroalgae/sheet.png)| ![filamentous](./img/06_Macroalgae/filamentous.png)|![coarsely](./img/06_Macroalgae/coarsely.png)|![thick](./img/06_Macroalgae/thick.png)|![crustose](./img/06_Macroalgae/crustose.png)|![calcareous](./img/06_Macroalgae/calcareous.png)|

#### Succession

- The population changes over time.

![succession](./img/06_Macroalgae/succession.png)

#### Importance

- See slides - 43-46

## Seagrass

- Was a land plant until the sea got back to it.
- Endemic to the Mediterranean sea.
- They are **flowering plants**.
- They are not only primary producers but also stabalizers of sediments and provide cover for small animals.
- Grow in shallow waters, in tropical and temperate environment.
- Seagrass are monocot.
- Flowers can have 3, 6 or 9 petals.
- They don't have **stomata - פיונית**. $`CO_2`$ is taken in via the epidermal tissues.
- Chloroplasts are in the epidermal cells and not in the mesophyll like land plants.
- They have hydrophilic pollen.
- **Lacunae** are tissues that allow to store air.

#### Morphology

![morphology](./img/06_Macroalgae/seagrass_morpho.png)

#### Comparasion seagrass/seaweeds

|||
|--|--|
|![comparasion1](./img/06_Macroalgae/comparasion.png)|![comparasion2](./img/06_Macroalgae/comparasion2.png)|


## Mangroves

- Found in coastal areas all over the tropics.
- Grow in brackish water (מים מליחים): mix of salt and fresh water.
- Threatened by human construction along coasts.
- Their seeds floats.
- **Mangal**: community of organisms in the mangrove habitat. There are 6 types:
  - Basin
  - Hammock
  - Scrub
  - Riverine
  - Fringe
  - Overwash
- **Pneumatophore**: vertical root structures for air exchange (like straws).
  - **Lenticels**: tiny pores for air exchange.
  - **Aerenchyma**: tissue for air storage.
- They also have roots at the bottom to hold on.
- Dependent of the **tides**. Sometimes there can be water more salty sometimes more fresh:
  - Mangroves know how to secrete the excess of salt through roots, stems, etc.
  - Can block the salt from coming in.
- Vivapary: seeds fall on the plant. Roots grow downward to the ground (compared to regular plant where root grow from the soil upward).

### Common species

#### Red Mangrove

- Tallest
- Red roots
- Roots come from the stem down into the ground.
- Propagule is the seed that start to grow on the plant itself and already know how to do photosynthesis and all it has to do is fall into the water.

#### Black Mangrove

- Second tallest
- Key characteristics: aerial roots (pneumatophores)..
- Live in oxygen deprived sediments.

#### White Mangrove

- Smallest
- When growing in oxygen deprived sediment it develops peg roots which are similar to pneumatophores.

#### Buttonwood

- Up on the hill
- When drys up it looks like a button.

### Adaptations

- See slide 81

## Salt Marshes

- Small plants, which can survive in colder zones.
- Contain a lot of salt.
- Mechanisms to mange excess of salt like mangrove.
- The main differences with mangroves are the size and growth regions.