# Research

## Algae

- Simple plant.
- Unicellular or multicellular.
- Does not have vascular layer.
- Live in water.
- Photosynthetic.
- Part use fat to float in water.
- Live in most habitats.
- Over 30,000 species classified in 11 divisions that are divided into 29 classes.
- Can be used as food, fertilizers, pharmaceutical and more.

![algae types](./img/10_Research/algae_types.png)

- Chlorophyll a is found in all algae (not just green).
- Chlorophyll b in found in some green algae (but not all).
- Diatoms - צורניות are single celled with a cell wall made of silica.
- Blue Green algae are prokaryotes (photosynthetic bacterias).
- Dinoflagellates: 
  - 2 flagellates (2 שותונים)
  - Release toxins that cause red tides.
  - During a red tide: at the surface they do photosynthesis, underneath there's not enough $`CO_2`$ and they do respiration thus decreasing the $`O_2`$ concentration which kill the fishes.
 
### Biomass comparision
 
- Algae have the most biomass production.
 
![biomass comparision](./img/10_Research/biomass_comparision.png)

### Biodiesel

- It's great but cost too much.

## Lipids

- Non soluble in water
- Algae produces more lipids when there's a lack on Nitrogen.
- 8 classes:
  - Fatty acyls
  - Glycerolipids
  - Glycerophospholipids
  - Sphingolipids
  - Sterol lipids
  - Prenol lipids
  - Saccharolipids
  - Polyketides
- Simple lipids:
  - Fatty acids
  - Triglycerides
  - Waxes

### Fatty acids

- Nomenclature: Alpha/Omega, lipid numbers

![fatty acid](./img/10_Research/fatty_acid.png)

- Short chain: < 6 carbons
- Long chain: >= 12 carbons
- The shorter the carbon chain, the more liquid the fatty acid is.
- Unsaturated fatty acid contain double bonds and aren't straight.
- How Omega nomenclature works:

![omega](./img/10_Research/omage.png)

- Lipid numbers is written in the form of C:D where c is the number of carbon and D the number of double bonds for example 18:1 means 18 carbons, 1 double bond.
- To this notation can be added $`\Delta^x`$ for each double bond, where x is the location of that double bond. To that is added the configuration cis/trans: $`\text{cis-}\Delta^9`$, $`\text{cis-}\Delta^{12}`$

### Triglycerides

- Glycerol backbone with 3 fatty acids.
- Diglyceride, monoglyceride: glycerol backbone with 2/1 fatty acids.
- Triglycerides to biodiesel: triglycerides + methanol + catalyst => glycerol + methyl esters
- The methyl esters are the biodiesel.

- Ara (Arachidonic acid) in fishes like sole, is mandatory in order to synthesize melanine.

### Analytical technologies to identify fatty acids in organisms

- Nile Red:
  - Solution that colors fatty acids in a cell.
  - It can't differentiate between fatty acids, just shows the quantity.
  
![nile red](./img/10_Research/nile_red.png)

- Thin layer chromatography (TLC):
  - Differentiate between fatty acids
  - On a paper with its base in water, the fatty acid go up to a certain point which allows to differentiate between them.

![tlc](./img/10_Research/tlc.png)

- GC-MS
 - Thin tube with gas that heats up and can differentiate between substances. More efficient than TLC.
 - Has a library of known substances. 
 - If the substance isn't known, it shows what it is made of.
 
![gc-ms](./img/10_Research/gc_ms.png)

- LC-MS
  - Same as GC-MS but with liquid.
  
## Algae Biotechnology

- Ideal microalgae:
  - High yield on high light intensity.
  - Insensitive to high oxygen concentration.
  - Grows and produces lipids at the same time.
  - Excretes oil outside the cell.
  - Resistant to infections.
  - Large cells.
  
- With less nitrogen, the algae produces more lipids.
- Too low nitrogen, the algae dies.
- Somewhere in the middle is best to grow them.
- Extracting lipids:
  - Drying the algae
  - Oil is pressed out
  - 70%-75% of the oil is extracted.
