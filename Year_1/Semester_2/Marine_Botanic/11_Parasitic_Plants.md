# Parasitic Plants

- Parasitic plants are known since Theophrastus who was the successor to Aristole.

## Plant-Plant Parasitism

- Plants become parasitic from genes expression in cells where those genes are normally not expressed (**ectopic expression**). _For example, expression of root gene in a leaf_.
- Some parasitic plants can live without a host if they know how to do photosynthesis.
- They usually connect to the vascular system.
- Can make the host plant do more photosynthesis to get more sugar and connect to the phloem to get that sugar.
- Parasitism occurs in all types of environments except aquatic environment because **competition for water** is the main reason of parasitism.
- Parasitism doesn't occur in Antarctica.
- Some plants are resistant to certain parasites but if a parasite isn't found on certain plant it doesn't it is resistant.

## Phylogenetic Relationships

- 1% of seed plants are parasites.
- 90% of parasites are hemiparasites.
- 60% of parasites are root parasites.

- APG2: Angiosperm Phylogeny Group II system (today APG3).

## Hemi and Holoparasitism

- **Hemiparastic plants**:
  - Know how to do photosynthesis to some degree.
  - Occurs in temperate region and mediterranean climats (because they can do photosynthesis).
  - They need mostly water and nutrients from the host plant.
- **Root-hemiparasitic plant**: same as above but connects to the host via the roots.
- **Holoparasitic plants**:
  - Have no photosynthetic abilities.
  - Are usually not green but other colors.
  - Can have reduced roots but the majority of the nutrients and water comes from the host.
- Some parasites connect via the host stem (stem parasites/aerial parasites) and some via the roots (root parasites). Some via both.
- A **facultative parasite** can survive without a host but succeeds better with a host. No parasitic plant has been documented to complete its lifecycle in a natural environment without a host.
- An **obligate parasite** has to connect to a host.

## Haustorium

- A haustorium is a modified root that forms a morphological and physiological link between the parasite and host.
- The haustorium attaches the parasite to the host, penetrates the host while keeping its own tissues intact, develops a vascular continuity between the host and parasite and ultimately provides the conduit through which host and parasite materials flow.
- The ability to make haustoria distinguishes parasitic from non-parasitic plants.

## Evolutionary Origins

- 2 General hypotheses:
  1. Parasitic genes were introduced into an autotrophic progenitor by horizontal gene transfer.
  2. Parasite genes originated through neofunctionalizationof plant genes encoding non-parasitic functions.

## Mycoheterotrophic

