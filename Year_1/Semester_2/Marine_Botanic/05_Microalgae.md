# Microalgae

- The term algae has no formal taxonomic standing. 
- Some are eukaryotes and some are prokaryotes.
- They can be found in water or on land (not submerge in water).
- Divisions of the algae are in the exam material:

![divisions](./img/05_Microalgae/algae_divisions.png)

- Marine plants produce 50% of the world's primary production.
- Marine phytoplankton are the main basis for marine food chains.
- Aquatic algae are found in all types of water: from freshwater to salt lakes. 
- They can be found up to -120m in water.
- They have tolerance for a all sorts of conditions: pH, temperature, turbidity (transparency of water) , $`O_2`$ and $`CO_2`$ concentration.
- Planktonic algae (unicellular) live suspended throughout the lighted regions of all water bodies including under ice in polar areas.
- Benthic algae are attached to the bottom or living within sediments, limited to shallow areas because of the rapid attenuation of light with depth.
- Benthic algae can grow attached on stones (epilithic), on mud or sand (epipelic), on other algae or plants (epiphytic), or on animals (epizoic).
- Algae grow very fast thus there's competition between them on light for photosyntisis (one algae will draw shadows on another), respiration will take all the $`O_2`$ and other organisms will die from lack of it.

### Growth habits:
  - **Supralittoral**: grow above the high-tide level
  - **Intertidal**: grow on shores exposed to tidal cycles
  - **Sublittoral**: grow in benthic environment from extreme low-water level to around 200 m deep (when very clear water).
### Unicellular
- **Many algae are solitary cells, unicellswith or without flagella, hence motile or non-motile**.
  - **Filamentous algae**: single algae cells that form long visible chains (filaments).
  - **Siphonous algae**: one big cell caused by repeated nuclear division without forming cell walls.
  - **Parenchymatousand Pseudoparenchymatousalgae**: layers of cells that compose visible algae, there's no xylem, phloem, etc.
  - **Colonial**: aggregates of several single cells held together loosely or in a highly organized fashion, the colony.

### Pigment and Storage product per Divisions

- Slide 26:
  - All divisions have **chlorophyll a**.
  - **Phycobilins** is present only in older divisions: Cyanophyta, Glaucophyta, Rhodophyta and Cryptophyta.
  - **$`\bold{\beta}`$-Carotene** is present in all divisions.

  ![pigments per division](./img/05_Microalgae/pigments_per_division.png)

- Chlorophyll isn't the only pigment that knows to absorb light either for doing photosyntesis or protecting it just by absorbing light (too much light is not good).

  ![light absorbtion per pigment](./img/05_Microalgae/pigment_light.png)
  

## Cyanophyta

- 150 genre
- 2000 species
- Photosynthetic bacteria.
- Representatives of the very first photosynthetic organisms on Earth.
- Fix nitrogen.
- Can be found in different cellular structures: colonies, filaments, branched filaments, unicells in mucus envelope.

### Structure

![structure](./img/05_Microalgae/cyanobacteria_structure.png)

### Morphology

- Cell wall made of peptidoglycan and not cellulose.
- Trichome: row of cells.

- **False branching**: cell that is bent.
- **True branching**: 3 cells making a T-shaped branch.

- They are **facultative chemoautotrophs**: they can produce energy from redox reactions.

### Reproduction

- Asexual through:
  - Binary fission - dividing in two
  - Fragmentation of colonies
  - Endospores/Exospores
  - Hormogonia: short piece of trichome (row of cells) that detaches from parent filament and glides away.
  - Akinetes: dormant cell with thick wall triggered by extreme conditions.
  
### Habitats

- Freshwater lakes
- Terrestrial soils
- Marine systems (intertidal, open ocean)
- Extreme environments (e.g. salt flats, hot springs, glaciers, etc.)
- Endosymbiotic: diatoms, sponges, tunicates, lichens, polar bear fur, bryophytes, gymnosperms, angiosperms


### Stromatolites

![stromatolites](./img/05_Microalgae/stromatolites.png)

- Earliest evidence of Cyanophyta comes from stromatolites.
- Stromatolitesare produced by successive deposition through “grain trapping” or calcification.

### Nitrogen fixation

- Not all cyanobacteria know how to fix nitrogen (like spirulina).
- Nitrogen fixation requires **no oxygen**. The organism lives in an environment with oxygen and the system/cell that does the nitrogen fixation is **isolated from the environment with oxygen**.
- The cells that know how to do nitrogen fixation are called **heterocyst**.

### Classification

![classification](./img/05_Microalgae/classification.png)


### Toxins

- Lyngbia: releases chemical
- Cyanotoxins: released by animal ingestion: neurotoxins and hepatotoxins (liver).
- It can kill small animals.

## Rhodophyta

- Red algae (but isn't always red!).
- First multicellular algae.
- Found in the artic to the tropics.
- Can be found up to 200m deep.

### Phycobilisome structure56

- It is on the thylakoids of the chlorosplasts.
- Like an antena that catches light waves.
- Adapt the number of phycobilisomes to the quantity of light (too much light - less phycobilisomes and vice versa).
- Also can modify the composition of phycobilisomes (phycoerythrin, phycocyanin, etc..).

### Morphology of Cell Wall

- Made of cellulose.
- Phycocolliods: Mucilaginous polysaccarides that surround the microfibrills to give elasticity and flexibility.
  - agar and carageenan (also used in food industry to make liquid like milk thicker).

## Chlorophyta

- Green algae
- Came after cyanophyta and rhodophyta.
- See slides

## Diatoms

- In hebrew צורניות
- Primary producers
- Eukaryote
- Division: heterokontophyta
- Unicellular or colonial.
- Cell wall from silicon: **like a shell composed of 2 parts: epitheca, hypotheca**.
- Pigments: Chla, c, β-carotene, fucoxanthin, diatoxanthin, diadinoxanthin.
- Mostly asexual reproduction but can do sexual reproduction under specific conditions:
  - When a diatom divides to produce two daughter cells, each cell keeps one of the two halves of the shell and grows a smaller half within it while the original shell remains the same size.
  - The average size gets smaller after every division.
  - When a certain minimum size is reached, they reverse this decline by expanding in size to give rise to a much larger cell called **auxospore**.
 
## Pennales

## Dinoflagellates

- 2 flagellas: biflagellate
- Red tides (HAB): poisonous
- Autotrophs, auxotrophs and heterotrophs


 