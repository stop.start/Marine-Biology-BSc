# Introduction

## Photosynthesis

![photosynthesis](./img/01_Intro/photosynthesis.png)

```math
6CO_2 + 6H_2O + \text{Light Energy} \longrightarrow C_6H_{12}O_6 + 6O_2
```

- Drives life.
- Photosynthesis, opposite of respiration, takes $`CO_2`$ with light and water and makes glucose.
- Plants perform photosynthesis when there is light. At night they only do respiration.
- Some parts of the plants are always in the dark (the parts that are in the ground) thus never take part in the photosynthesis.
- **Starch - עמילן** is the stored energy.
- Plant's **cell wall** is made of **cellulose and pectin** and in some plant also **lignin**.

  ![plants cycles](./img/01_Intro/plants_cycles.png)
 
## Evolution

- 14.7 billion years ago: Big Bang!
- 4.5 billion years ago: formation of Earth.
- 3.5 billion years ago: inorganic matter and a lot of lightning and UV made the organic matter.
- Organic matter (N, C, H, O) make 98% of living matter.
- **Heterotrophs**: Cannot fix carbon and uses organic carbon for growth.
- **Autotrophs**: Organism that produces organic compounds from substances present in its surroundings. The most successful being photosynthesic.

  ![evolution](./img/01_Intro/evolution.png)

- During the first billion years, simple organic compounds became complex organic compounds called **protenoids** which are composed of around 200 amino acids and lipid chains.
- First life on Earth were **primitive cells**: Protenoids and lipid chains formed **microsphere** surrounded by a membrane.
- Those primitive cells consumed simple organic compound $`rightarrow`$ they were **heterotrophs**.

  ![evolution](./img/01_Intro/evolution2.png)

### Differences between prokaryotes and eukaryotes that perform photosynthesis

- **Prokaryotes**, who don't have organelles, have photosynthesic membranes while **eukaryotes** have chloroplasts (organelle) who perform the photosynthesis.
- Plants cells, in addition to the chloroplasts which perform the photosynthesis have:
  - **Cell wall**.
  - **Starch grains**: organelle where starch is stored.
  - **Vacuole**: organelle that contains water and gives the plant its structure. It also contains matters that it doesn't know how to take care of or matter that it needs to keep.
  
  ![cells](./img/01_Intro/cells.png)

- Eukaryotes got their **chloroplasts** and **mitonchondria** by **endosymbiosis** which means the cells at some point engulf those bacterias.

  ![endosymbiosis](./img/01_Intro/endosymbiosis.png)

### Autotrophs vs Heterotrophs

- Fungi are neither plant nor animal
- Some autotrophs can be heterotrophs (in use mainly in labs)

  ![feeding strategy](./img/01_Intro/feeding_strategy.png)

- 650M years ago: first **multicellular** organisms
- 450M years ago: first **macrophyte**: macro - big; phyte - plant
- 0.5M-1M: human 

  ![evolution](./img/01_Intro/evolution3.png)
  
### The 3 domains of life

- Prokarya:
  - Bacteria
  - Archaea
- Eukarya

## Plant Taxonomy

- 3 types of plants types:
  - **Species**: natural.
  - **Hybrids**: hybridization between 2 species.
  - **Cultivars**: plants that had a specific characteristic and were isolated in order to propagate this characteristic.
- Aristo was the first to classify plants. He classified them by their external characteristic (no DNA then).
- During the 18th century, Carl Linnaeus laid the foundation of modern taxonomy.

### Classification hierarchy

|Hierarchy|Example|
|--|--|
|![hierarchy](./img/01_Intro/class_hierarchy.png)|![example](./img/01_Intro/hierarchy_example.png)|

- Binomial name = genus species
- More example in the powerpoint: presentation1, slides 36-39.
- When we know the genus (סוג) but not the species (מין) the name is written _Genus sp_ or _Genus spp_ where sp/spp means that the species is unknown.
- **Cotyledon - פסיג** is an **embryonic leaf** that comes out at first when the plant starts to grow. It holds the necessary material for the plant to grow.
- **Monocot/Dicot - חד-פסיגי/דו-פסיגי**: the plants can have either one cotyledon or two.
- At first the cotyledon is white until it starts to do photosynthesis.
- Taxonomy has gone through changes:
  - 1887: **Englerian system**
  - 1988: **Cronquist System**
  - 1998: **APG Classification** - today we use **APG Classification III**.

### Types of plant lives

- **Ephemeral - בן חלוף**: Plants that are not always above ground but still live. Therophytes plants are ephemeral.
  - **Geophyte**: Perennial and ephemeral.
- **Permanent, Persisten  - בר-קיימא**: Above ground all year long.
- **Therophyte - חד שנתי**: One cycle, blooms, tries to reproduce and dies. In Israel more than 50% of the plants are therophytes.
- **Biennial Plant - דו-שנתי/בין-שנתי**: only one reproduction cycle (like the therophyte) and lives more than a year. Most plants are biennial. The first year they only grow leaves and the second year they have flowers.
- **Perennial Plant - רב-שנתי**: lives more than a year.
- **Mono-cycle - חד-מחזורי**: lives only one reproduction cycle.
- **Polycyclic - רב-מחזורי**: lives more than one reproduction cycle.

- _Note_: Plants can be more than one of the above.

### Raunkiaer classification

- Raunkiaer classified the plants by where their new leaves grow:
  - **Hemicryptophytes**: Renewal is on the ground or just underground.
  - **Chamaeophytes - כמיאופיטים**: Renewal on the ground and up to 25cm above ground.
  - **Phanerophytes**: Renewal from 25cm and above.
  - **Therophytes**: survive difficult period as seeds.
  - **Epiphytes**: Plants that grows on other plants (but are not parasite and don't kill the host plant). Example: pineapple (pineapple that grows on the ground isn't epiphyte).
  - **Cryptophytes**: Renewal underground or in the water.
    - **Geophytes**: Renewal deep underground.
    - **Hydrophytes**: Renewal on or just under the water surface. 
    - **Helophytes**: Renewal in the mud or groundwater (מי התהום).
  
  ![raunkiaer](./img/01_Intro/raunkiaer.png)

## Phylogenetics

- Study of evolutionary relationships among groups of organisms (e.g. species, populations), which is discovered through molecular sequencing data and morphological data matrices.
- LUCA: Last Universal Common Ancestor.

## Plant evolution

- Example in presentation1 - slides 58-61.
- Land plants:
  - **Nonvascular** plants: plants that don't have transport tissues (example: mosses).
  - **Vascular** plants: plants with transport tissues:
    - **Seedless** vascular plants: cannot reproduce without water.
    - **Seed** plants:
      - **Gymnosperms -seed cones**
      - **Angiosperms - seed flowering**

  ![plant evolution](./img/01_Intro/plant_evolution.png)

