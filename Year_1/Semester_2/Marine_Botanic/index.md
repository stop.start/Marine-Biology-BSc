# Marine Botanic

1. [Intro](./01_Intro.md)
2. [Plant Anatomy](./02_Plant_Anatomy.md)
3. [Plant Cell](./03_Plant_Cell.md)
4. [Biotic and Abiotic Factors](./04_Biotic_Abiotic.md)
5. [Microalgae](./05_Microalgae.md)
6. [Macroalgae, Seagrass and Mangroves](./06_Macroalgae.md)
7. [Photosynthesis (Light Reactions)](./07_Photosynthesis.md)
8. [Photosynthesis (Dark Reactions)](./08_Photosynthesis.md)
9. [Photosynthesis (Marine)](./09_Photosynthesis.md)
10. [Research in Marine Botanic](./10_Research.md)
11. [Parasitic Plants](./11_Parasitic_Plants.md)

