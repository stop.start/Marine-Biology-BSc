# Plant Cell

**The presentation on this is pretty clear. This summary is just a help to go back on this lesson but is far less detailed.**  

## Prokaryotic Plant cells

- Archaea and Bacteria.
- Don't have a nucleus.
- Circular DNA.
- Don't have organelles.
- Photosynthesic bacterias are sometimes called algae.
- Photosynthesic bacterias contain **photosynthesic membranes** whithin the cell.

## Eukaryotic Plant Cells

- Bigger than prokaryotic cells.
- Have organelles and nucleus.
- Chloroplasts are found in cells above ground and more mitochondria is found in the root in order to perform respiration.

|||
|--|--|
|![plant cell](./img/03_Plant_Cell/plant_cell.png)|![plant cell](./img/03_Plant_Cell/plant_cell2.png)|


### Cell Wall and Membranes

- Cell wall is only in plant cells
- Plasma membrane allows selective passage of molecules (with the help of protein).
- There are 3 types of membrane proteins:
  - Integral
  - Peripheral
  - Anchored
  
### Nucleus

- **Nucleus** contains the genetic information.
- **Nuclear envelope** is a double membrane surrounding the nucleus.
- **Chromatin** is the DNA-protein complex. 

### Endoplasmic Reticulum (ER)

- Network of **internal membranes**.
- Two types: 
  - **Rough ER (RER)** which is covered by ribosomes (protein synthesis).
  - **Smooth ER (SER)** which functions as a site for lipid synthesis and membrane assembly.

### Golgi apparatus

- Plays a key role in the synthesis of and secretion of complex polysaccharides and in the assembly of oligosaccharide side chains of glycoproteins.
- Consists of:
  - **Cisternae**: one or more stacks of 3-10 flattened membrane sacs.
  - **Trans Golgi network (TGN)**: network of tubules and vesicles.

### Endomembrane system

- **Vesicle**: small bubble within a cell enclosed by lipid bilayer

### Vacuole

- Only in plant cells.
- Can occupy 80-90% of the total volume.
- Surrounded by vacuolar.
- **Vacuole** are used to dispose of toxic compounds and other useless compounds. With a microscope crystals of calcium oxalate can be found.
- Crytals can be found in either in shape of a flower or of a needle.
- It is filled with water.
- Turgor pressure: is the force within the cell that pushes the plasma membrane against the cell wall. 
- Can store pigments.
- In the fall, the plant get back everything it can from the leaves before they fall. This include water as well as chloroplats and that's why the leaves change color.
- **Anthocyanins** is a pigments that gives a red/blue color and playing with the pH will change the color of the plant.

### Mitochondria

- Site of respiration which synthesize ATP from sugars

### Chloroplasts

![choloroplats](./img/03_Plant_Cell/chloroplast.png)

- Found in **mesophyll tissues**.
- Has a double membrane.
- The stroma is the fluid inside the choloroplast.
- Contains **thylakoids** which are responsible for the photosynthesis. Their shape increases the surface area.
- **Grana** (plural: granum) is a stack of thylakoids.
- The thylakoids contain what is needed to harvest light (not learned yet). It includes **chlorophyll** which gives the green color.
- The thylakoids discs are hollow and their inside is called thylakoid space or lumen.
- There are several types of chlorophyll and they differ by their efficiency to absorb the light. 
- Plants always have the chlorophyll a which is the most important and can have also chlorophyll b or chlorophyll c.
- See slide 28.
- Chloroplasts are a type of **plastids**:
  - Chloroplasts contain chlorophyll.
  - Chromoplasts give all sorts of colors.
  - Leucoplasts give no color.
  - Amyloplasts are abundant in storage tissues of the shoot and root, and in seeds.
- Chromoplasts can become amyloplasts and vice versa. 

![plastid cycle](./img/03_Plant_Cell/plastid_cycle.png)

### Light cycle

![light cycle](./img/03_Plant_Cell/light_cycle.png)

### Microbodies

- Spherical organelles with single membrane.
- Two main types:
  - **Peroxisomes**:
    - In plants they are in photosynthesic cells.
    - Removes **hydrogens** from organic substrates (which consumes oxygen).
    - Peroxides ($`H_2O_2`$) produced by the above reaction is broken in the peroxisome.
  - **Glyoxysomes**:
    - Present in oil-storing seeds.
    - Convert stored fatty acids into sugars that can be translocatedthroughout the young plant to provide energy for growth.
- **Oleosomes**:
  - Have only a half-unit-membrane with the polar head facing the surroundings and the hydrophobic fatty acid tails facing the lumen (inside).
  - Contain proteins called oleosins which prevent fusion between oleosomes.

### Cell wall

- Composed mainly of:
  - **Cellulose** (main component)
    - Made of glucose monomers.
    - The cellulose polymers are composed into microfibril about 10-25 nanometers diameter.
  - **Pectin**
    - In between the cellulose microfibrils which holds everything as a solid compound.
  - Hemicellulose
- In some plants, there can be also **lignin** which make the cell wall stronger and stiffier.
- In some plants, there can also be glycoproteins like **extensins** which strengthen the cell wall (its name comes from the fact that it was though to extend the cell wall when in fact it does the opposite).
- **Wax** can be found on the cell wall of the leaves either for protection against scratches from sands or for preying on insects (carnivor plants).
- Suberin is also a protecting layer (cork).
- Some cells have an additional wall layer called secondary wall. Those cells don't usually divide.

![cell wall](./img/03_Plant_Cell/cell_wall.png)

#### Plasmodesmata

- **Plasmodesmata** allow adjacent cells to exchange materials.

![plasmodesmata](./img/03_Plant_Cell/plasmodesmata.png)

### Cytoskeleton

- Maintain the shape of the cell and holds organelles in place.
- Types of protein filaments:
  - Microtubules
  - Actin filaments
  - Intermediate filaments

## Light

- **Photon** : light particle.
- **Quanton**: amount of energy of a photon.
- Visible light spectrum: 400-700nm.
- **Pigment**: substance that absorbs light.
- Some pigment are photosynthesic and some aren't and use the light for other purposes.
- Photosynthesis happens best with blue and red light (blue is best).
