# Photosynthesis (Light Reactions)

```math
6CO_2 + 6H_2O + sunlight \longrightarrow C_6H_{12}O_6 + 6O_2
```

- Photosynthesis is an **autotroph** process: plants and some organisms create glucose from sunlight.
- It is important because it creates **organic matter** from inorganic matter and releases **oxygen**.
- **Chlorophyll a** is the most important pigment for photosynthesis.
- Chlorophyll absorbs the sunlight.
- Photosynthesis is affected by:
  - Light: the more light the more photosynthesis.
  - $`CO_2`$: the more $`CO_2`$ the more photosynthesis.
  - Temperature: low $`\Rightarrow`$ low photosynthesis, hot $`\Rightarrow`$ increase in photosynthesis, too hot $`\Rightarrow`$ low photosynthesis.
  
## Light

- Visible light: 380nm (purple) - 750nm (red).
- **Pigments absorb visible light**. Each pigment absorbs different wavelength.
- Chlorophyll does not use green light thus reflects it.
- **Chlorophyll b** allows to **expand the spectrum** used for photosynthesis.
- **Caratenoids** absorb the light **excess** which would damage the chlorophyll.

![light and photosynthesis](./img/07_Photosynthesis/photosynthesis_light.png)

- **Absorption spectrum** is a graph showing the light absorption per wavelength for a pigment.
- Looking at the absorption spectrum for the chlorophyll a it shows that it absorbs mainly **blue and red** light.

![absorption spectrum](./img/07_Photosynthesis/absorption_spectrum.png)

- Although chlorophyll a uses mainly blue and red, other wavelength are also used by other processes.
- The **action spectrum** was demonstrated by **Theodor W. Engelmann**.
- When absorbing light the pigment goes to an exciting state, **with enough energy electrons can go free**.
- **Fluorescence** occurs when an orbital electron of a molecule/atom relaxes to its ground state by emitting a photon from an excited state. It releases light and heat.

![fluorescence](./img/07_Photosynthesis/fluorescence.png)

- Difference between chlorophyll a and b:

![chlorophyll chemistry](./img/07_Photosynthesis/chlorophyll_chem.png)

_Note: if the magnesium in the middle is replaced by and iron atom it produces hemoglobin (or something very close)._  

## Plant physiology


![structure](./img/07_Photosynthesis/structure1.png)
![structure](./img/07_Photosynthesis/structure2.png)
![structure](./img/07_Photosynthesis/structure3.png)


### Chloroplasts

- **Chloroplasts** are organelles called **plastids**.
- There are found mainly in the cells of the **mesophyll** (around 30-40 chloroplasts per mesophyll cell).
- They contain the chlorophyll.
- They are the site of the photosynthesis.
- The **ATP synthases** are located on the thylakoid membranes.
- During photosynthesis, light-driven electron transfer reactions result in a proton gradient across the thylakoid membrane.
- ATP is synthesized when the proton gradient is dissipated via the ATP synthase (like in mitcochondria).

![chloroplast](./img/07_Photosynthesis/chloroplast.png)

## Biochemistry

- Photosynthesis reverses the direction of electron flow compared to respiration.
- Photosynthesis is a redox process in which $`H_2O`$ is oxidized and $`CO_2`$ is reduced.
- Photosynthesis is an **endergonic** process: the energy boost is provided by light.
- There are two stages of the photosynthesis:
  - Light reactions.
  - Dark reaction/Calvin cycle/light independent reaction.

### Light reactions

- Called light cycle because it is **dependent** on light.
- Happens **on the thylakoid membrane**.
- It converts energy from the sunlight to ATP and NADPH.
- What it does:
  - Splits $`H_2O`$.
  - Releases $`O_2`$.
  - Reduces $`NADP^+`$ to NADPH.
  - Generates ATP from ADP.
- **Z scheme of photosynthesis** is composed of **two photosystems**: I and II. Photosystem II happens before I.
- PSI and PSII both have a special pair of chlorophyll called **P680** (PSII) and **P700** (PSI). There are found at the **reaction center**.
- P680 means that it absorbs light at 680nm wavelength best (same principe for P700).

#### General steps

![z scheme](./img/07_Photosynthesis/z_scheme.png)

- During PSII, an electron is released from the chlorophyll in P680 (due to excitation from the sunlight energy).
  - This electron is received by a **primary electron acceptor** in the **reaction center**. 
  - The primary electron acceptor is reduced when receiving the electron.
- A water molecule is oxidized thus releasing an electron which replaces the one that previously got free.
- **Splitting** the water molecule releases $`O_2`$.
- The electron goes down an **electron transport chain (ETC)** which causes the creation of ATP.
  - The electron transport chain push $`H^+`$ ions from the stroma into the inner thylakoid space.
  - Those $`H^+`$ are used by PSI to create $`NADPH`$ from $`NAPD^+`$.
  - The $`H^+`$ gradient also allows to power the ATP production by passing through the ATPsynthase.
- By the time the electron gets to the PSI, another electron from the chlorophyll in P700 was released and this electron which isn't in an excited state anymore (due to the ETC) can take its place.
- The new electron is received by a primary electron acceptor of PSI, then goes down an electron transport chain.
- PSI produces NADPH.

![thylakoid](./img/07_Photosynthesis/thylakoid.png)

#### Phycobilosomes

- In some plants (like red algae, cyanobacteria), **phycobilosomes** can be found. 
- It sits on the thylakoid membrane.
- It contains pigments:
  - Phycoerythrin
  - Phycocyanin
  - Allo-phycocyanin
- Contains also chlorophyll (?)

### Calvin cycle (dark reactions)

- The **Calvin cycle** takes place in the **stroma** of the chloroplasts.
- It does not depends on light.
- Requires: $`O_2`$, ATP and NADPH.
- It creates **sugar (glucose)** from the $`CO_2`$ and [H]ydrogen using ATP and NADPH from the light reactions.

![light reaction/calvin cycle](./img/07_Photosynthesis/light_calvin.png)
