# Geology of the Mediterranean Sea

## Intro

- The mediterranean sea can be divided into two big basins: east and west.
- The basins are separated by the Sicily strait.
- The two basins were formed at different geological times thus are different geologically.
- When looking at the volcanoes around the world, we can see that they follow the borders of the seas and oceans.

![volcanoes vs seafloor](./img/03_Geology_Mediterranean_Sea/volcanoes_seafloor.png)

- When looking at earthquakes map we can see that it follows along the same lines.

![earthquakes](./img/03_Geology_Mediterranean_Sea/earthquakes.png)

- Seafloors are mapped by sattelites looking at the water level in currents going up and down. Up means there's an obstacle beneath, down means there's a hole beneath.

## Geology Basics

- Oceanic crust is thin and dense compared to the continental crust.

![earth structure](./img/03_Geology_Mediterranean_Sea/earth_structure.png)

- Some info:

![crust](./img/03_Geology_Mediterranean_Sea/crust.png)

- Plates tectonic:
  - **Divergent** (in ocean): magma will go up = volcano.
  - **Convergent** - ocean plate and continental plate: the ocean plate goes down with a lot of water, the water wants to evaporate due to high temperature and that creates a volcano.
  - **Convergent** - continental vs continental: mountains chain is created.
  - **Transform fault**

![plates](./img/03_Geology_Mediterranean_Sea/plates.png)

- A continental plate **can** be underwater.

### In the Middle East

- Divergence in the Red sea.
- Transform fault along the Dead sea.
- Convergent in Turkey.

![middle east](./img/03_Geology_Mediterranean_Sea/middle_east.png)

- When looking at the plates and their composition, we can get to Pangea, the original continent from 200Ma.
- With model of how the continents moved up until today we can see that the **easter basin mediterranean sea is what remains of the Tethys sea** but not the western basin.
- There were multiple cycles of closing and breaking of the supercontinent.

### Other stuff

- When there's an island on a subducting oceanic plate, it will collide with the continental plate and will bound to it.
  
![collisions](./img/03_Geology_Mediterranean_Sea/collision.png)

- Erathostenes is underwater mountain that gets closer to Cyprus by subduction of the plate they're (both? or just erathostenes?) on.

![erathostenes](./img/03_Geology_Mediterranean_Sea/erathostenes.png)

## Measurement Methods

- Two methods to map the ground: **seismic** and **potential field** (magnetic field)
- The seismic method uses explosives to map the ground up to 9km downward.
- The angle of the soundwave hit is important.
- The sound waves pass trough geologic layers in different manners:
  - Reflection: hit and return
  - Refraction: hit, continues, returns.
  
![explosives](./img/03_Geology_Mediterranean_Sea/explosives.png)


## Plates

- 5.9 Ma Gibraltar strait closed and then opened back. The whole process took around 500,000 years.
- It took around 2 years to fill back the mediterranean sea.
