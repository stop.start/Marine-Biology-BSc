# Sea Turtles

- 7 species of sea turtles.
- 2 famillies: cheloniidae (צבי ים) and dermochelyidae (צבי גלדיים)

![about](./img/09_Sea_Turtles/about.png)


- The leatherback turtle (dermochelys coriacea) is a more anciant species than the familly of cheloniidae (regular sea turtles).
- The leatherback can get up to 1km below the surface.
- Its carapace is covered by skin and oily flesh.
- Lives in the open sea, eats plankton (80% meduses).
- The most common species in Israel: the brown turtle and green turtle.

![turtle species](./img/09_Sea_Turtles/turtle_species.png)

- Eastern mediterranean conditions is a good fit for sea turtles (hot and shallow).
- Beaches are where turtles lay their eggs.
- The green turtle uses a smaller area for nesting than the brown turtle (mainly Israel, Lebanon, Cyprus and Turkey)

![regions](./img/09_Sea_Turtles/regions.png)

- Males start to come to the mating site around February.
- The sexual maturation for sea turtles is around 20/30 years old.
- The turtles come back to mate on the site they hatched from.
- The females don't reproduce every year and can take pauses up to a few years:

  | Years between nesting | Brown turtles         | Green turtles         |
  |-----------------------|-----------------------|-----------------------|
  | 1                     | 25%                   | 0%                    |
  | 2                     | 30%                   | 35%                   |
  | 3                     | 25%                   | 50%                   |
  | 4,5,6                 | Descending percentage | Descending percentage |

- After reproducing, the males leave and the females start laying their eggs on the beach.
- Females can lay 80 eggs (brown)/130 eggs (green). Can lay eggs up to 3 to 5 times during a nesting season (May-August).

![mating](./img/09_Sea_Turtles/mating.png)

- From 1993 the number of hatching has increased (since the start of the turtle center).
- The turtles lay most of their eggs on the site they hatched from but one or two egg laying will be on neighbours beaches.
- Sea turtles wander the sea which makes it hard to count how many there are. The best way to count them is from **hatching season (how many hatched) + number of females laying eggs + death**.
- In 2018 around 8000 eggs of brown turtles were layed and around 2000 eggs of green turtles were layed.
- The sex of the foetus is set according to the **temperature**: higher temperature: female - lower temperature: male.
- The albedo plays a big part in this: the whiter the sand of the beach where the eggs are, the higher the albedo and the more males will hatch.
- In general most are females.
- Israel the more we go north the more females there are. 
- Although the beaches in South Israel make more males still in all there are 64% female rate.
- When the egg hatched the young turtle follow the light (form the moon? reflection of the moon?) until they are in the sea. 
- They will remember the way they do to the sea and that's how they'll know how to come back to lay their eggs. That's why it's important not to help them!
- Once in the sea they will swim up to a week, without food, to reach oceanic currents.
- In the open sea they find floating algae (or anything that floats) to live on for a while. Even adult turtles will try to float on these.
- Human interaction disturbs the reproduction cycles of the sea turtles.
- Green turtles stay mostly along the beaches and not so much in the open sea.
