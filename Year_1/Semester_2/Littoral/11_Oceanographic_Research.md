# Oceanographic Research

- Question: "השפעות בריכת ההשקטה של תחנת הכוח בחדרה על עמודת מים​"

## Sampling

- Sampled:
  - Salinity (physics)
  - Temperature (physics)
  - Depth
  - Chlorophyll fluorescence (biology & chemistry)
  - Dissolved oxygen concentration
- The depth in itself isn't interesting but required for answering the question.
- Salinity and temperature (physics of sea) give the density. If some water are more dense they will sink and water from below layer will replace them at the surface.
- Used a Rosetta with CTD instrument on it and sampling bottles.
- Tested: 
  - Chlorophyll concentration
  - Suspended material concentration (חומר מרחף)
- Temperature is measured with sensors. It is measured with an instrument that check electric conductivity.
- A constant electric power on the conductor will have a higher current with a higher temperature.

![voltage to temperature](./img/11_Oceanographic_Research/volt_temp.png)

- Salinity is also measured with sensors. The electric conductivity sensor checks the current through the water. The higher the salinity the higher the conductivity.
- One issue is that the conductivity of the water depends also of the temperature and pressure so we need to correct that. 

![voltage to salinity](./img/11_Oceanographic_Research/volt_salinity.png)

- The chlorophyll gauge checks the fluorescence in the water. The wave measured is the wave returned (photon) by the electron going through the photosynthetic process. The color is more purple/red.
- Because cells can stop doing photosynthesis or will do more photosynthesis in the morning when they're "starving", the results are proned to error. That's why in addition the chlorophyll is measured with bottles.
- When illuminating the chlorophyll in order to measure the wave gotten back, the returned waves are measured at 90 degrees from the angle of illumination in order to get only the returned waves.

## Results

### Physics

- Below, the colors represent depth of the bottom. For example the purple dot represent bottom at 5m.
- Same principe with graph for temperature and salinity.

![results1](./img/11_Oceanographic_Research/results1.png)

### Chemistry/Biology

![results](./img/11_Oceanographic_Research/results2.png)

- Using a base of known quantity of chlorophyll and measuring its fluorescence in order to compare the samples from the sea.

![results](./img/11_Oceanographic_Research/results3.png)

- From the following graph (where the the colors represent temperature), it shows that the quantity of dissolved oxygen seems to depend on the temperature (the colder the more oxygen).

![results](./img/11_Oceanographic_Research/results4.png)

- The following graph shows the dissolved oxygen measurements without the effect of the temperature (only biological) by looking at the percentage of oxygen there is according to the saturation.
- Those results show no change with depth but shows a change within the pool/outside the pool (biological activity).

![results](./img/11_Oceanographic_Research/results5.png)

- When looking at the dissolved oxygen per time of the day it shows biological activity.
