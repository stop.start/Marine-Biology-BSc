# Physics of the Mediterranean Sea


## Intro

- Mediterranean: medius=middle, terra=land
- The mediterranean sea was formed from the closing of the **Tethys sea** (from 90Ma to 15Ma).
- **Messinian event**: when the mediterranean sea was formed it was sealed off from the Atlantic and as a result almost dried out (from 12Ma to 5Ma).
- The mediterranean sea is connected to the Atlantic via the **Gibraltar strait**.
- The **West Basin** extends from the Gibraltar strait to the **Sicily strait**.
- The **Ionian Basin** in the middle up to the cretan passage.
- The **Levantine Basin** in the east.
- When talking about the **Eastern Basin**, it includes the ionian basin and the levantine basin.

![structure](./img/01_Physics_Mediterranean_Sea/structure.png)

## Some data

- Average depth: 1500m.
- Maximum depth: 5267m (in the ionian sea near Greece).
- From longitude 6 west to 36 east.
- From latitude 30 north to 46 north.
- 800km north to south.
- 4000km from east to west.
- Surface: 2,500,000 km2.
- Volume: 3,750,000 km3.

## Some definitions

- The mediterranean sea is a basin-margin - **אגן שוליים**. It is a **small body of water connected to an ocean and feeding off of it**.
- The connection (gibraltar strait) is small and thin, thus the mediterranean sea isn't too much affected from the Atlantic. It is more affected by its direct surroundings.
- A small connection to the ocean is called **half closed basin** (אגן חצי סגור).
- **Residence time** (זמן שהות) is the time a molecule of water spends in the basin.
- **Concentration basin**: 
  - Freshwater from precipitations and runoffs (נגרי עילי) are less than the evaporation rate.
  - Saltier than the ocean it is connected to.
- **Dilution basin**:
  - Freshwater from precipitations and runoffs (נגרי עילי) are more than the evaporation rate.
  - Less salty than the ocean it is connected to.
- The mediterranean is a concentration basin. Although the evaporation is high than the intake of new water, it is in an equilibrium state.
- The black sea is a dilution basin.

![basin types](./img/01_Physics_Mediterranean_Sea/basin_types.png)

## Thermohaline Circulation

- In a half closed basin the circulation of the water is called **thermohaline**.
- **Thermohaline circulation**: 
  - Because of the high evaporation rate, the level of the mediterranean sea is lower than of the Atlantic.
  - This is what drive the water from the Atlantic into the mediterranean at the surface.
  - Underneath, the salty water from the mediterranean goes out to the Atlantic in order to equilibrate the salt concentration.
  
  ![thermohaline](./img/01_Physics_Mediterranean_Sea/thermohaline.png)

### Box model

- If we take the mediterranean sea as a "box", we can use the following equations:
  - F = flow
  - S = salinity
  - P = precipitations
  - E = evaporation

1. $`F_{in} \times S_{in} = F_{out} \times S_{out}`$
  - $`F \times S`$ is the quantity of salt that goes in or out. At equilibrium, the salinity that goes in and out have to be equal.
2. $`F_{in} + P + F_{\text{runoff}} + F_{out} + E = 0`$
  - Because the level of the sea is at equilibrium adding all the changes from it equals to 0.
  
## Water

### Salinity

- Salinity is measured by grams of salt per kilogram of water (PPT or ‰: parts per thousands).
- Today the salinity is measured by the conductivity of the water and converted to grams (new unit PSU).
- Because of all the issues with the unit of measures, today the salinity has no unit, just a number.

### Density

- The density is affected by:
  - Temperature: the higher the temperature the lower the density.
  - Salinity: the higher the salinity the higher the density.
  - Pressure: the higher the pressure the higher the density (note: water are not so much compressible).
- A liter of distilled water weighs $`1000Kg/m^3`$. Salt water will be heavier than that.
- The $`\sigma`$ unit is the difference between the anomaly in density from a body of water and distilled water. _Example:_ $`\sigma = 1027.8 - 1000 = 27.8 Kg/m^3`$

### Seasonal Changes

- Those changes occur on the 100-200 meters from the surface.

#### Thermocline

![thermocline](./img/01_Physics_Mediterranean_Sea/thermocline.png)

- Also called thermal layer.
- With higher temperature, the water gets warmer. 
- Warm water is less dense thus go upward.
- But with high temperature there's also evaporation thus the water at the surface becomes more salty.
- At the peak of the summer, the water at the surface is the most salty.
- Between temperature and evaporation, temperature wins thus the warm salty water stay at the surface.
- When the temperature goes down again, at some point the high salinity of the surface will cause it to be heavier than the layer below and it will sink more and more.
  
#### Halocline

- **LSW**: Levantine  Surface Water
- **AW**: Atlantic Water
- **LIW**: Levantine Intermediate Water
- **DW**: Deep Water

![halocline](./img/01_Physics_Mediterranean_Sea/halocline.png)

- The Atlantic water (AW) is less salty than the mediterranean sea water, thus less dense.
- In winter the AW is the surface layer.
- In summer, a layer of levantine water (LSW) is a the surface because the high temperature causes it to be less dense although it still has a higher salinity.
- At the end of the summer the LSW starts to sink, and mix with the AW. This creates the LIW layer which is a mixture of the two layers.
- The deep water (DW), are local water and don't change much over time.

### TS Diagram

??

### Water that goes out of the mediterranean sea

- The LIW layer can be found in all the mediterranean sea since it is shallow enough.
- This layer is deep enough to go out the mediterranean sea to the Atlantic through the Gibraltar strait.
- It is denser than the AW thus goes downward once out of Gibraltar until they are at a depth that meets their density and then go forward.
- Those water can go as far as the Carrabean.

![LIW out](./img/01_Physics_Mediterranean_Sea/liw.png)

## Streams

- Controlled by thermohaline.
- Coriolis.. Coriolis
- High/low tide - the moon
- **Bathymetry** is the topography of the seafloor.
- Some currents follow the bathymetry lines.
- Streaming equations can help making models.
- General currents in the mediterranean sea:

![general currents](./img/01_Physics_Mediterranean_Sea/general_currents.png)

- Detailed currents:

![detailed currents](./img/01_Physics_Mediterranean_Sea/detailed_currents.png)

- BiOS mechanism: Bi-modal Osciliating system: at the north of the ionian basin, there's a circular current that can change direction (counterclockwise to clockwise).
- This change changes all the currents in the area and causes the water to have even a higher salinity.

## Deep Water Anomaly

- In the 90s.
- The origin of the **deep water** in the easter is the **Adriatic sea**.
- At some point (might be linked to the BiOS) the source of the water wasn't the Adriatic sea anymore but the **Aegean** sea.
- Within 2-3 years, through code winter, the volume of the Aegean sea flows into the easter basin of the Mediterranean sea.
- Some says it is because of the volcanic eruption of Pinatubo (91) which, despite being in the Phillipines, caused cold winters all around the globe.
- The deep water found during this period was completely different (including higher salinity). 
- 
