# Chemistry of the Mediterranean Sea

- In oceanography we look at:
  - Physics
  - Chemistry
  - Biology
- What affects the repartition of the different matters' concentration in the sea:
  - How much gets in from the land and atmosphere.
  - How much gets out of the sea (to land and atmosphere).
  - Chemical and physical processes within the sea.
- Photosynthesis and primary production drive life.
- Solar radiations warm the earth, most of it are returned to space and part of it is trapped by gases in the atmosphere (greenhouse effect). This allow the Earth to keep its warmth.
- For the last few years the greenhouse effect started to be more effective and the temperature on Earth increases.
- This causes changes in climate.
- Oil formation and trapping:
  - Organisms like phytoplankton fix $`CO_2`$ in other forms (like carbohydrates and sugars).
  - Those organisms sink to the bottom when they die.
  - Sediments pile up above them, increasing the pressue and temperature.
  - At some point this all transform into neft.
- Plants and the sea can intake atmospheric $`CO_2`$.
- An increase in phytoplankton in the sea will cause a decrease in $`CO_2`$ in the sea which will cause more dissolving atmospheric $`CO_2`$ into the sea (good thing).

## Salt

- Eastern mediterranean salinity: 40gr/Kg
- The salt concentration changes from physical processes like, currents, evaporation, condensation (precipitations).
- The salt concentration directly affects the density of the water.
- The density of the water directly affects the currents which are caused by difference in density between bodies of water.
- Two bodies of water with different densities (= temperature + salinity) won't mix.
- In the mediterranean sea, the salinity increases going east.

![mediterranean salinity](./img/02_Chemistry_Mediterranean_Sea/mediterranean_salinity.png)

- With increase in salinity, the sea level decrease and pull in water from the Atlantic.
- In summer, what keeps the salty water at the surface is its temperature which decreases the density.
- The residence time of water in the mediterranean sea is around 100 years.
- Leibig's law of the minimum: the growth of a plant is set by the limiting factor.
- In the deep, the limiting factor is the sun. Since everything sinks to the bottom, in the deep see there more fertilizers.
- In the sea surface the limiting factor is fertilizer (דשן): Nitrogen and Phosphorus.
- The eastern mediterranean sea is especially poor in nutrients at the surface because the water comes from the Atlantic. By the time the water from the Atlantic arrives east, most of the nutrients have already been taken on the way.
- The nutrients fall deep where the currents flow west thus never come east.
- Chlorophyll: low concentration at the surface (too much light) and deep, high concentration in the upper middle.

|||
|--|--|
|![phosphorus](./img/02_Chemistry_Mediterranean_Sea/phosphorus.png)|![chlorophyll](./img/02_Chemistry_Mediterranean_Sea/chlorophyll.png)|

- When comparing the nutrient concentrations between summer and winter, in winter there are more fertilizers in all depths. That's because in winter the water is colder but still salty and sink causing the layers underneath to go up with their fertilizers.

![summer vs winter](./img/02_Chemistry_Mediterranean_Sea/summer_winter.png)

- Estuary is a connection between freshwater and seawater.
- Alexander river is an estuary.
- The salinity of the Alexander river is 5 at the surface and 20/30 deep under.
- Most of the year, there's a lack of oxygen at the bottom thus is the limiting factor.
- Eutrophication: a body of water becomes overly enriched with minerals and nutrients that induce excessive growth of plants and algae. This process may result in oxygen depletion of the water body.
