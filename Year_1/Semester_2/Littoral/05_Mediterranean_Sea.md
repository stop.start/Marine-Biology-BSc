# The Mediterranean Sea

## About

- Area: 2,500,000 $`Km^2`$
- Maximum Length: 3,860Km
- Maximum Width: 700Km
- Average Depth: 1,500m
- Maximum Depth: 5,267m
- Salinity: 3.5%-3.9%

- The eastern basin is hotter and has a higher silinity than the western basin.
- The eastern basin has less nutrients than the western basin. The eastern basin is thus **oligotrophic**.
- The chlorophyll and plankton concentrations as well as the primary production are lower in the eastern basin than in the western basin.
- The western basin has more marine species than in the eastern basin because of the above conditions.
- Suez Canal developped by Ferdinand de Lesseps 150 years ago.
- The Suez canal caused species from the Red sea to come to the Mediterranean sea.
- Caulerpa Taxifolia is an **invasive algae** from an accident in an aquarium in Monaco. It spread quickly all over the Mediterranean sea. It lives all year round. It is toxic (at to the species in the Mediterranean sea). It prevents other good species to spread.
- Tides in Israel are not always clear if it's high tide or low tide.

