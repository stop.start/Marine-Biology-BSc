# Biology of the Mediterranean Sea

- The ocean is driven by thermohaline processes (density gradients - temperature + salinity - that create currents mainly at the surface).
- This causes an inequal repartition of the nutrients in the ocean.
- Plankton are organisms that cannot swim against the currents.
- Plankton are organisms from a few nanometers to centimeters.
- Microalgae are plankton.
- **Decomposers** break down all the organic matter (that fall down from plankton poop and dead plankton) and return it as dissolved inorganic matter.
- The surface area increases by exponent 2. This means that the surface area of a $`4\mu m`$ plankton is 100 times bigger than of a $`0.4\mu m`$ plankton ($`10^2=100`$).
- The volume exponent 3. This that the volume of a $`4\mu m`$ plankton is 1000 times bigger than of a $`0.4\mu m`$ plankton ($`10^3=1000`$).
- The smaller plankton have a large surface area comparing to their volume. It affects their metabolism and lives which ares faster than of the bigger plankton.
- There are $`10^{29}`$ bacteria in the sea which is around **90% of the total oceanic biomass**.
- The microbial organisms (single celled) perform most of the processes in the sea. The sea could function normally without multicellular organisms (including the fishes, mammals, etc).
- The microbial organisms constitute the most surface area and biomass in the sea.

![biomass](./img/06_Biology_Mediterranean_Sea/biomass.png)

- How much organisms multiply per year:

![reproduction](./img/06_Biology_Mediterranean_Sea/reproduction.png)

- Mammals reproduce once in very 10 years. Fishes once per year.
- Bacterias hundreds times a year: they are eaten at a high rate as well.
- The concentration of plankton in the sea are negligible especially in a oligotroph sea.

![biogeochemical_loop](./img/06_Biology_Mediterranean_Sea/biogeochemical_loop.png)
![biogeochemical_loop](./img/06_Biology_Mediterranean_Sea/biogeochemical_loop2.png)

- Phytoplankton uses nutrients at the surface and make glucose and carbohydrates. Poop + death it all falls.
- Without the bacteria using this organic matter and transforming it as dissolved inorganic matter, the surface would deplenish from all those nutrients.
- In summer there's all the issue of the warm water at the surface not mixing with the cold water underneath.
- The inorganic matter comes back up by upwelling.

![carbon pump](./img/06_Biology_Mediterranean_Sea/carbon_pump.png)

- Organic carbon is fixed by photosynthesis and ends temporarily or permanently trapped at the bottom.
- The organic matter that is burried under sediment becomes gas/neft.

![biomass per region](./img/06_Biology_Mediterranean_Sea/biomass_region.png)
![biomass per region](./img/06_Biology_Mediterranean_Sea/biomass_region2.png)

- In polar areas the water is always cold and mix all the time thus no thermocline which means lots of nutrients. As soon as there's enough sunlight (summer) there's an big increase in biomass.
- In tropic areas there's always a thermocline thus the water at the surface don't mix with the water underneath causing a lack in nutrients all year long thus low biomass all year long.
- In temperate regions, there's sometimes thermoclines but not all year long, the water will mix causing the nutrients to get back up allowing for moderate to high biomass all year long.

