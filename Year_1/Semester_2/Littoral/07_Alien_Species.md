# Alien Species

- **Exotic species introduction**: introduction by humans of species in an envorinment they don't belong to.
- Can be **accidental or deliberate**.
- Can be a threat to local species: **ecosystem alteration**.
- **Invasive species** are alien species that cause dramatic changes, like changes in the habitat. Their attributes:
  - High dispersability
  - High growth rate (high r)
  - Long reproduction season
  - Wide geographic range in the source region (means they can live in multiple habitats).
  - Environmental tolerance
  - High competitive ability
- The **zebra mussel** is one of those species:
  - Its origin is from Eastern Europe in the Caspian sea and Black sea.
  - In 1988 it appeared in the US.
  - It is freshwater mussel and does not live in seawater.
  - It is thought to have crossed the Atlantic ocean via boats in ballast water (מי נטל) which is the water used to stabalize boats.
  - The boats went through channels of freshwater once in the US and that's how it would have been introduced.
  - By 1997 it invaded almost half the US freshwater (mainly eastern US).
  - Because it stucks to everything in the water (boats, pipes, etc) it causes economic damage.
  - They can cause the extinction of other species:
    - It also will stuck to living organisms which makes it harder for those to survive.
    - **Invading species** which are alien species that cause dramatic changes, like changes in the habitat. The zebra mussel filters the water and empties it from plankton.
- The **brown tree snake** is also an invasise species introduced in Guam.
  - The species living on islands have no natural predators.
  - The snake is a predator and caused some species to go extinct.
  - In addition it caused some economic damages.
- The **nile perch** is another invasise species.
  - Deliberate introduction by the English in the Lake Victoria for the locals to grow and fish them.
  - Responsible for the extinction of at least 200 species.
- The number of invasive species increases with the years.
- How humans contribute to the dispersal of species (non deliberate):
  - Ship ballast 
  - Trade
  - Fouling organisms: when ships move from water body to another, if the ship isn't cleaned it can introduce species that attach to it.
  - Marine agriculture
  - Aquarium
  - Canals (like Suez canal)
- The best way to battle the invasive species is education and prevention.
- Also monitoring known invasive species and eradicate them if they pop up in other places. This is called **maintenace control**.
- Once an invasive species comes and successfully spread in a new region, it changes the habitat and lessen the competition from the local species and it opens the door for other invasive species to come.
- Suez canal was widened over the years.
- In addition to the Suez canal which gives the opportunity for alien species to cross, human interaction in the eastern mediterranean sea disrupt the local habitats giving more opportunities for alien species to successfully spread.
- Human interaction includes:
  - Natural gas extraction
  - Seawater desalination
  - Fishing
  - Habitat degradation
- Apart from the fauna, there is not much difference between the levant basin the the red sea:

|             | Levant                      | Red Sea                     |
|-------------|-----------------------------|-----------------------------|
| Temperature | $`16\degree C-30\degree C`$ | $`20\degree C-28\degree C`$ |
| Salinity    | 3.9%                        | 4-4.2%                      |
| Nutrients   | Oligotrophic                | Oligotrophic                |
| Sea Type    | Atlantic                    | Tropical                    |

- Around 400 species in the mediterranean sea are from red sea.

