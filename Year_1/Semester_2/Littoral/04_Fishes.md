# Fishes

- The number of species of fish represent 63% of the overall number of species of vertebrates.
- There are around 36,000 species of fishes.
- Fishes are aquatic chordates (מיתרניים אקוואטים).
- They are **poikilothermic** meaning that their blood is at the temperature of their surroundings.
- Most fishes have fins (some don't), gills (some also have lungs or breath through their skin) and scales.
- Fishes live pretty in all habitats except ice pole and one other.
- Some species of fish can be out of the water for hours.
- There are fishes in all depths.
- Fishes that live really deep in the dark have special ways to recognize fishes of their species (for reproduction). For example, dots on their skin that have a special arrangement.
- Those fishes are hermaphrodites.
- Fishes can be as small as 8mm and as big as 20m.
- 41% of the fishes live in freshwater.
- Freshwater is 0.1% of the water and contain almost half the species of fish.
- In the mediterranean sea there's no specific species of fishes in deep regions.
- All lands in the mediterranean are no more than 37km of the next land.
- 0.7% of the surface of all the water. 0.2% of the volume of all the water.
- Gibraltar strait is shallow (370m depth). Fishes that swim deeper won't enter the mediterranean from the Atlantic and if they do they won't survive (the temperature is too high).
- 150 years ago the mediterranean sea was connected to the Red sea:
  - From an hydrology point of view nothing was changed (water movement, level, salinity, etc).
  - From a biology point of view a lot of changes.
- The only source of new water taken in consideration is from the Atlantic through Gibraltar.
- The fishes that enter through Gibraltar are species that live in cold water thus they cannot survive in the eatern mediterranean which is hotter. Of the 700 species in the mediterranean, only 380 in the eastern mediterranean.
- **Lesseptsian migration**: migration of marine species accross the Suez Canal from the Red sea to the Mediterranean sea (not so much in the opposite direction).
- The species from lessepsian migration survive better in the eastern mediterranean sea since it's hotter and closer in condition than the west.
- **Indigenous** species are species that are local to a certain place but can be local to other places as well.
- **Endemic** species are species only found in a unique location. Species that are endemic to the mediterranean sea are found only in the mediterranean sea.
- For some species that live in the eastern mediterranean sea, the conditions are suboptimal (too hot, not enough nutrients). The same species in the western mediterranean sea are bigger.
- According to Dany Golany the fact there are not a lot of fishes in the eastern mediterranean sea isn't not because of too much fishing but because of the conditions.
- The reason the lessepsian migration is only on way is because:
  1. The species coming from the Atlantic live in suboptimal conditions (in the eastern basin).
  2. The species coming from the Red sea are more competitive (from the conditions in the red sea) and the conditions are closer to theirs.
- Fishing is measured by energy invested per quantity fished: horsepower of the boat X number of fishing day.
- The problem started when Israelis went abroad to fish:
  - More days for sailing to Turkey for only a few days (2) of fishing.
  - The quantity of fishes is bigger in Turkey.
  - Only the few days of fishing were taken into consideration when measuring.
  - The results were skewed because of that.
