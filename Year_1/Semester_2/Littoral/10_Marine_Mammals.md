# Marine Mammals

- Marine mammals are originally from the land (sea $`\rightarrow`$ land $`\rightarrow`$ sea).
- After the first vertebrates moved from the sea to the land, some went back to the sea:
  - Arthropods (פרוקי רגליים) (insects)
  - Reptiles (זוחלים)
  - Birds (עופות)
  - Mammals (יונקים)
- Insects have several way to get air under the water: some use their tail to get air from outside the water, some manage to get an air bubble stuck to their body.
- Reptiles: turtles, crocodiles, snakes, lezards
- Marine mammals are mammals like land mammals:
  - Birthed from the female
  - Breath air 
  - Constant body temperature
  - Fur
- There are around 4000 species of mammals.
- Marine mammals:
  - Feed from the sea
  - Can stay underwater for long periods of time
  - Good swimmers
  - Reproduce either in the sea or close to it.
- Some info:
  - Phyllum: Chordata
  - Subphyllum: Vertebrata
  - Class: Mammalia
- Marine mammals are divided in 4 groups:
  - Cetacea (לוויתנאים)
  - Sirenia (תחשאים)
  - Pinnipedia (סנפירגליים)
  - Predator species from the land that came to the sea

## Orders

### Cetacea  - לוויתנאים

- They are closest in DNA from the horses (סוערי הפרסה).
- They don't have posterior limbs and move with a tail.
- They lost their fur.
- In order to replace the fur as a mean to keep their body temperature, they developped a layer of fat.
- They are predators. Depending on the species they feed from big prey to plankton.
- They are originally from the sea but some species move to rivers.
- **Convergent evolution** (אבולוציה מתכנסת): species that live, move, prey underwater will get (through evolution) an aerodynamic form. That's why dolphins look like sharks.
- There are several orders (סדרות):

#### Mysticeti - Baleen Whales - לווייתני מזיפות

- Example: Humpack whale
- They have **baleen - מזיפות** instead of teeth made of keratin (like in human fingernails) and hair.
- These baleen are arranged as plates and hanged from the upper jaw.
- With the baleen the whales filter the water to eat. With its tongue it push back all the water.
- They have 2 **blowholes - פתח נשימה** on top of the head so they stay submerge while surfacing for air.
- The water we see when they breath out is the water that was arounf the nostrils when they were closed and underwater. The whales breath out only air.

#### Ondotoceti - Toothed Whales - לוויתני שיניים

- All their teeth are the same.
- Only 1 blowhole.
- They use their **sonar (איכון-הד) to hunt prey** when they cannot see (like at night).
- The echo of the sonar comes back through fat in their lower jaw.

### Sirenia - תחשאים

- They are close to elephants and rabbits.
- They also lost their posterior and move with their tail.
- They also pretty much lost their fur but still have some short body hair on their upper muzzle.
- They are vegeterians.
- Found in hot places in the sea and rivers.
- Only 4 species remaining.

### Pinnipedia - Seals - סנפירגליים

- Suborder of predators.
- Close to the bears.
- Have their fur.
- Examples:
  - Sea lions (kept their fur). They can lift their upper body and stand on their front limbs and go forward (walk) on land.
  - Seals (כלב ים). Can't walk on land or lift their upper body. They use their back and front limbs in order to move.
  - Walrus (ניבתן). 
- Eat in the sea but reproduce or on land or on ice.
- Birth and breasfeed on land.

### Predators (not an order)

- Sea otter (לוטרת ים)
- Polar bear.

## In the Mediterranean Sea

### Seals

- One species of seals:
  - Mediterranean monk sea - כלב הים הנזירי הים תיכוני  - (monachus monachus).
  - Population around 300/400.
  - Danger of extinction.

### Sirenia

- There isn't any sirenia in the mediterranean sea but there are some in the red sea.

### Toothed-whales

#### Dolphins

- There are mainly two species in the mediterranean sea but overall there are four:
  - **Common bottlenose - דולפינן ים תיכוני/ דולפינן מצוי**: 
    - Most common. 
    - Group size: around 5-6.
    - Diet shows contact with fishing boats.
  - **Common dolphin - דולפין מצוי**: mostly south - between Ashdod and Ashkelon.
    - Average size of a group: 22 (5 to 50)
    - From Herzlya to south.
    - Shallow waters
    - Diet shows contact with fishing boats.
  - Rough-toothed dolphin - דולפין תלום-שן - lives in tropical areas (in the Atlantic around the Canaries). All sightings are in the eastern basin. It is thought to have been common in the mediterranean sea and when the climate got colder they all concentrate in the eastern basin which is warmer.
  - Striped dolphin - סטנלה פסוסה - the most common is Israel.

#### Others

- Grampus - only dolphin in the genus Grampus. Commonly known as Monk dolphin.
- False killer whale - קטלן מדומה
- Sperm whale - ראשתן: the biggest toothed-whale. Big difference between males and females. Blowhole on the top/side which causes the water blowed not straight up. They have a white patch on one of their fins.
- Fin-whale (לוויתן מצוי) : the second largest species on Earth after the blue whale. 
  - Eat in the western basin during summer and in winter come east where it is warmer.
  
## Eilat

- 4 species of dolphins:
  - Spinner dolphin (סטנלות ארוכות חרטום ): like to spin
  - Pantropical spotted dolphin ( סטנלה ברודה)
  - Indo-Pacific bottlenose dolphin (דולפינן אנקולית)
  - Indian Ocean humpback dolphin ( דולפינים מגובננים): there was a sighting in north Israel (lessepsian immagration)
  - Grampus like to put their tail up, we don't know why.

## IMMRAC - מחמלי

### Danger to the survival of the mammals

- Samaller living area (especially beaches)
- Accidental trapping in fishing nets.
- Slaughter of the marine mammals when they compete for food resulting in damage of fishing material (nets and such). Not in Israel though.
- Have to compete with fishing boats for food (overfishing).
- Pollution (chemical, biologic - diseases -, plastic, acoustic).
- Big cetacean get hurt by boat collision.
- Climate change: too hot for krill survival.
