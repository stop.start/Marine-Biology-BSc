# Intro

- Microorganisms exist as **single cells** or **cell clusters** and must require a microscope to be viewed (_micro_organisms).
- Types of microorganisms: bacteria, archaea, fungi, protozoa, microalgae, viruses.
- In agriculture, changing each year what is grown allows for the pests to not persist since they are specific per product grown.
- **Bioremediatio**: microorganisms that degrade toxic waster materials.
  - Some bacteria can break down fuel, spilled oil.
- Some organisms can transform $`NO_3^-`$ into $`N_2`$. $`N_2`$ being a gaz, it deprive the soil of nitrogen which is already a limiting nutrient.
- Organisms that can fix atmospheric nitrogen have the advantage of not having to have to take it from the soil.
- Nitrogen is important for DNA and amino acid synthesis.
- Plants that have a symbiotic relationship with microorganisms that can fix nitrogen survive better.
- Mold won't (usually) grow on dry stuff or if it's too sweet (like jam).
- Methane is an inflammable gaz thus needs to either be used to do work or released in the atmosphere (not good of the environment).
- Most copper is found as copper-sulfat. In order to get the copper itself, microorganisms are used.
- In strawberries, the black parts contain melanine which is good against UV radiations (in other words against tough times).
- Some cells can move.
- In order to produce ATP, fermentation bacteria will usually add a phosphate to and ADP (and not with ATPase). This is called substrate-level phosphorylation.
- The first bacteria were chemolithotrophic (?).
- Then the bacteria started to produce oxygen.
- In photosynthesis, $`H_2O`$ donates electrons releasing the oxygen. 
  - $`H_2O`$ isn't the only that can be used to donate electrons ($`H_2S`$ which releases sulfure and is called anoxygenic photosynthesis).
- **Cyanobacteria** is the first photosynthetic bacteria.
- **Spirulina** is a descendant of cyanobacteria.
- With the apparition of photosynthetic organisms, oxygen began to be more present in the atmosphere.
- Eukrayotes started to appear around 2 billion years ago.
- 2 types of microscopes: 
  - Light microscope: >1μm
  - Electron microscope: <100μm. Check youtube on 2 options on how they work.
- Humans have more bacteria than cells. This is called **microbiota** or **microflora**.
  - Some are beneficials.
  - Some are opportunistics and can become pathogenic.
  
  > Note:  
  > Antibiotics can cause stomach aches because some of the useful bacteria can be sensitive to the antibiotics.  
  
## History
  
- Antony van Leeuwenhoek (1632-1723):
  - Built microscopes.
  - Studies water in Amsterdam.
- **Golden age of microbiology (1857-1914)**.
- **Louis Pasteur (1822-1895)**
  - Pasteurization is a cycle of heat and cold allowing to kill both bacteria sensitive to heat and/or cold.
- Pasteur's experiment with the **swan-necked** flask showed that there is no sponteneous production of bacteria.
  - Bacteria present in the neck of the flask don't grow no matter how long they stay there.
  - Bacteria present in the neck of the flask will grow if the liquid reach them and bring them down into the flask.
- **Joseph Lister (1827-1912)**
  - He developed a system of antisepetic surgery which prevents microorganisms from entering wounds: sterilization of surgical instruments with heat and the use of phenol prevented wound infection by killing bacteria.
  - Developed **listerine** which contain **40% ethanol** (phenol is not used because it can cause cancer).
- **Robert Koch (1843-1910)**
  - Established the relationship between Bacillus anthracis and anthrax.
  - Discovered that the cause of tuberculosis is a rod shaped bacterium called Mycobacterium tuberculosis.
  
  ![koch postulate](./img/01_Intro/koch_postulates.png)

- The Koch's postulates lead to the development of:
  - **Petri dish** (Richard Petri)
  - **Agar**: developed from algae, liquid at 100°C and used in a solid state. Almost none of the bacteria will eat it.
  - Nutrient broth and nutrient agar.
  - Methods for isolating microorganisms.
- **Sir Alexander Fleming (1881-1955)** discovered the first antibiotic (penicillin) from **penicillum** in 1928.
- **Sergei Winogradsky (1856-1953)** isolated nitrifying bacteria.
  - [Winogradky column](https://en.wikipedia.org/wiki/Winogradsky_column): simple device for culturing a large diversity of microorganisms.
  
  ![winogradsky column](./img/01_Intro/winogradsky_column.png)

- **Cornelius Bernadus van Niel (1897-1985)** isolated purple sulfur bacteria and contributed to the chemistry of photosynthesis.

```math
\text{A can either be Sulfur or Oxygen}\\  
2H₂A + CO₂ → CH₂O + 2A + H₂O
```

- **Robert E. Hungate (1908-2004)**: methods for isolating anaerobes.
  - In the guts there are systems that are anaerobic.

## Classification

- Eukaryotic:
  - Algae
  - Fungi
  - Protozoa
- Prokarytic:
  - Bacteria
  - Archaea
- Classification can be done based on visual characteristics, biochemical characteristics. It can also be done based on genetic characteristics.
- Visual classification is easier.

### Bacteria

- Classification:
  - By shape:
    - Spherical = **cocci**
    - Cylindrical = **rods/bacilli**
  - By gram stain:
    - **Gram poisitive**: thicker cell wall
    - **Gram negative**: thinner cell wall
- Prokaryote cell shapes:

[prokaryote cell shapes](./img/01_Intro/cell_shapes.png)

#### Gram stain method

- **Steps**:
  - Heat/dry
  - Stain with violet stain (violet dye)
  - Iodine fix: fix the dye
  - De-stain with alcohol
  - Stain with sfranin (red dye)
- Steps in other words (from Miki last year):
  - Associates with peptidoglycan so all bacterias are going to stain purple.
  - After using a decolorizing solution, the violet stain will strongly remain in bacterias that have a much thicker peptidoglycan layer (gram-positive cells).
  - After using the decolorizing solution, the red dye is used in order to identify easily the gram-negative cells.
- **Gram positive**:
  - Retain the violet dye.
  - Cell wall made of a thick layer of peptydoglycan and underneath a plasma membrane.
- **Gram negative**:
  - Retain the red dye. 
  - Cell wall made of a thin layer of peptydoglycan between two plasma membranes. 

![gram stain](./img/01_Intro/gram_stain.png)

- E. Coli is gram negative.
- Staphylococcus is gram positive.
- Coliforms means "like E. coli".
- Some antibiotics damage the synthesis of the cell wall of the cell. Those can be given to gram+ organisms to kill them.

#### Proteobacteria

- Largest phylum of Bacteria.
- E. coli is a **proteobacteria**.
- Species of proteobacteria are:
  - **Pseudomonas**: many of them can degrade complex or toxic natural and synthetic organic compounds.
  - **Azotobacter**: fix nitrogen.
- Pathogens includes: **Salmonella**, **Rickettsia**, **Neisseria**.
- **Mitochondria** have evolutionary roots within the Proteobacteria.

#### Gram Positive

- **Bacillus** and **Clostridium** are rod-shaped and can produce spores.
- Botox is produced from clostridium and neutralizes muscles.
- **Streptomyces**: filementous, produce spores and produce **antibiotics**.
- Lactic acid bacteria like Streptococcus.
- Some bacteria don't have a cell wall but are still categorized as gram+ because they look like gram+ that lost the ability to build the cell wll. Those are called **mycoplasmas**. Many of them are pathogenic and are hard to kill.

### Archaea

- Extremophils.
- Two types: Euryarchaeota and Crenarchaeota.
- Pyroccocus: sphere shaped and thermophilic. Their particularity is their optimal growth at 100°C.
- Halophilic methanogens: live is water saturated or nearly saturated with salt that also produce methane.

### Fungi

- Like cockroaches and crabs their cell wall is made of **chitin**.
- Break down organic matter.
- Mold.
- Yeast.

### Protozoa

- Eukaryotes
- May be motile

### Virus

- Acellular.
- Can be DNA or RNA.
- Core surrounded by a coat.
- Can reproduce only within a host.

## Taxonomy Notes

- ppt1/slide 92-end

## Cell Stuff

- 50% of the cell is carbon.
- 20% is oxygen.
- Sea water lacks a lot of required elements for cell growth.
