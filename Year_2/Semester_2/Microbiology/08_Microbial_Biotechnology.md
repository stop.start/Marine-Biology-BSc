# Microbial Biotechnology

## Genetically engineering plants

- 5 steps:
  - DNA isolation
  - Single gene cloning
  - Gene designing
  - Cell transformation
  - Backcross breeding
- DNA extraction form organism with desired trait
- Locate and copy the desired gene.
- Insertion of the gene in a single plant cell hoping for it to be successful.

![process](./img/08_Microbial_Biotechnology/process.png)

- When designing a gene three regions need to be taken into consideration:
  - **Promoter**: signal how much protein to produce
  - **Coding region**
  - **Termination sequence**
- Using the 35S promoter, from cauliflower mosiac virus, allows the gene to be expressed in all cells all the time.
- PEP Carboxylase promoter from photosynthetic enzyme gene, allows the gene to be expressed in photosynthetic tissues only.
- PPTA/slide 9
- Transformation was done with recombinant DNA but nowadays are done with other methods like microinjection, electro and chemical poration, and bioballistics.​
  - PPTA/ slides 14-17

## Plant Breeding

- After genetically engineering seeds, the plant needs to be bred by a plant breeder.
- Breeding techniques are used to make those plants high yielding lines.
- Usually the genes are introduced into standard plants.
- **Transgenic line**: plant which was introduced the desired gene.
- **Elite line**: plant with a desired characteristic.
- The first step is inbred line from transgenic line.
- Second step is the crosspollination from transgenic to elite (F1 seeds).
  - The offsprings have 50% genes from the transgenic and 50% from the elite.
- F1 plants are crossed with elite inbred. This is called **backcrossing** and he offsprings are called BC1.
  - The offsprings (BC1) contain 75% elite genes and half will contain the transgene.
- After 5-6 generations, the plants will contain 98% of the elite genes as well as the transgene.

### Agrobacterium

- **_Agrobacterium tumefaciens_** is a bacterium that is used to transfer foreign genes into target plants.
  - It transfer to the host plant part of a **Ti plasmid** (tumor inducing). This part of the plasmid is called **T-DNA** (tranferred DNA).
  - In the tranffered part of the plasmid:
    - Gene **O**  coding for opine-synthesizing enzyme.
    - Group of three genes called **onc** coding for enzymes involved in plant hormones.
    - Enclosing the tranferred part two sequenced called R (25 base pairs). 
  - On non transferred part of the plasmid, a gene called **vir** essential for the tranfer process is used as control.

![ti plasmid](./img/08_Microbial_Biotechnology/ti_plasmid.png)

- The genes on the T-DNA can be replaced by desired genes.
  - The recombinant plasmid in transferred into the agrobacterium which has another plasmid containing the vir gene but no T-DNA.
  
![t-dna](./img/08_Microbial_Biotechnology/t_dna.png)

### Other methods

- Particle bombardment was used with papaya.

## Diatom Biotechnology

- Eukaryotic algae.
- Cell wall of diatoms is made of silicon dioxide and is called **frustule**.
  - Two part: epitheca and hypotheca.
  - The epithca overlaps the hypotheca.
- SDV (silicon deposite vesicle) is involved in the final structure of the frustule.
- During asexual division, the parental frustule is used as an epitheca for both new cells.
- The most common species of diatoms in research is called _Phaedactylum tricornutum_.
  - Salt water species.
  - The only species in the genus _Phaedactylum_.
  - Can grow in absence of silicon.

### Biometerials

- Usually the algae are used for their ability to create fatty acids and monomers.

#### Next gen biomaterials

- Materials that can react with the environment.
- 3 types:
  - Functional materials
    - Those have particular properties like piezoelectricity, magnetism, etc.
    - They are found in all classes of materials: ceramics, metals, polymers and organic molecules.
  - Self-assembling biomaterials
    - Self assemble without any external forces.
    - Atoms, molecules, micelles, etc.
  - Nano materials
    - 1-200nm.
    - They have unique chemical, physical, electrical and magnetic properties.

#### Nanoparticles

- 1-100 nm
- A lot of consuumer products use them (textiles, detergents, etc).
- Silver nanoparticles have unique optical, electrical, and thermal properties and are being incorporated into products that range from photovoltaics to biological and chemical sensors.​
- Some algae (like _Phaedactylum tricornutum_) can produce silver nanoparticles when in presence of silver and under stress.
- Experiment with _P. tricornutum_ show that adding 1mg/l $`Ag^+`$ will reduce the growth by ~50% (compared to growth without silver).
- Letting the algae grow for a few days and then adding the $`Ag^+`$ will yield better results. If the $`Ag^+`$ is added from the start the algae might not be able to deal with it thus the growth is a lot lower.
- $`Ag^+`$ can enter the cell either with the help of ATPase or by diffusion.
- The atoms can be reduced and undergo some processes to get solid.

![algal growth](./img/08_Microbial_Biotechnology/algal_growth.png)
