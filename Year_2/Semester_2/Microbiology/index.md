# Microbiology

1. [Intro](./01_Intro.md)
2. [Bacteria](./02_Bacteria.md)
3. [Metabolism](./03_Metabolism.md)
4. [MicroGenetics 1](./04_Microgenetics1.md)
5. [MicroGenetics 2](./05_Microgenetics2.md)
6. [Fungi](./06_Fungi.md)
7. [Plant Pathology](./07_Plant_Pathology.md)
8. [Microbial Biotechnology](./08_Microbial_Biotechnology.md)
9. [Intro to Immunology](./09_Intro_Immunology.md)
10. [Intro to Virology](./10_Intro_Virology.md)
