# Intro to Immunology

## Intro

### Some terms

- **APC**: antigen presenting cell.
- Phagocytic cell asbsorbs pathogens and then exhibits the pathogen's antigen on its membrane.
- The antigen is then transfered the antigen to CD4 cells.
- **_Staphylococcus aureus_** is a bacterium that can cause infections in the respitory tract.
- **Innate immunity** is a basic immune system that responds against unknown pathogens. It knows general structure that definie a lot of bacteria.
  - It is reponsible for **inflamations**.
- **Adaptive immunity** is the immune system that is taught from known pathogens (that were already encountered).
- The cells of the immune systems are **pluripotent** stem cells: from one non determined cell they can go into two type of cells:
  - **Lymphoid stem cell** (T lymphocyte, NK lymphocyte, B lymphocyte).
  - **Myeloid stem cell** (erythrocyte, megakaryocyte, macrophage, granuclotytes).
- **Epithelial cells** are the cells lining the outer surface of the body and its organs and blood vessels.
  
![pluripotent stem cell](./img/09_Intro_Immunology/pluripotent.png)

### Pathogens

- They are four categories of pathogens:
  - **Viruses**: they are obligatory intracellular (need a host). They usually (but not always) kill their host by lysis during replication.
  - **Bacteria and Archaea**: some produce toxins (_Staphylococcus aureus_).
  - **Fungi**
  - **Parasites**
- Some example of pandemic:
  - The Black Death or bubonic plague (דבר): the bacteria is called **_Yersinia pestis_**.
    - During the 14th century in Europe (25% of the population were killed).
    - During the 19th century in China and India killing 12M people.
  - Spanish influenza in 1918: **H1N1 influenza virus**.
  - Flu pandemic or Swine flu in 2009: new version of H1N1 influenza virus.

### Vaccines

- Vaccines use attenuated viruses (disabled or weak) to teach our immune system what antibodies to produce agains the disease.
- Smallpox was eradicate by the use of vaccination.
- Measles started to spread again from lack of use of vaccination.
- Covid-19: 7 strains (most of them in animals).
- SARS (2002), MERS (2012) and COVID-19 are 3 types of dangerous coronavirus for humans.
- 2 methods of vaccins:
  - Passive: plasma taken from people that recovered from the disease and from it is isolated IgG which are given to other people with the disease. This is a drug more than a vaccin and the effect will disappear with time because since it comes from another person, the body will react against those IgG.
  - Active:
    - Inactivation: take in a dead virus/bacterium
    - Take in an attenuated virus/bacterium
    - Fractionation: "disassemble" the virus/bacterium and use only the protein that causes the immune system to react.
    - DNA vaccine: take in the DNA without the whole virus/bacterium thus eliminating the risk of real infection. The issue is to get the DNA into the cells.
      - The foreign DNA encodes for the virus/bacterium proteins which will be synthesized by our cells and will provoke a reaction from our immune sysmtem since they are foreign to our boody.
      - Not all the DNA is required. Just a plasmid with the relevant gene.
      - Using mRNA is a new approach to this. From Morderna company. It won't be intergrated in the cells like DNA.

![mRNA vaccine](./img/09_Intro_Immunology/mrna_vaccin.png)

## Immune System

![summary](./img/09_Intro_Immunology/summary.png)

### Cell types

- **Pluripotent cells** are present in the **bone marrow**.
- The **lymphoid stem cells** are present in **lymph nodes** which are spread in several places in the body.
- The **spleen** isn't connected to the lymphoid system but antigens will get into the spleen from the blood flow and will encounter B and T cells.
- **T cells** are produced in the **Thymus**.
- **B cells** are produced in the **bone marrow**.

### Immunity types

- Innate immunity recognizes shared structures among pathogens although some pathogens don't have those structures and thus can go through this first line of defense.
  - During the infection stage the pathongens will encounter APC (antigen presenting cells): macrophages, NK cells.
- In the adaptive immunity the response is delayed since the cells responding to the pathogen need first to encounter the antigen and activating T and B cells and their proliferation.
- There are several types of T cells. Two of them are **CD8** and **CD4**
  - Both are membrane proteins.
  - CD8 are cytoxic knows **MHC class I**.
  - CD4 are helpers (for example they active B cells) knows **MHC class II**.
  
![immunities](./img/09_Intro_Immunology/immunities.png)

### Defense types

- First line of defense: **external barriers**:
  - Skin
  - Mucuous membranes
  - Secretion of skin and mucuous membranes
- Second line of defense: **internal defenses**:
  - White blood cells
  - Defensive proteins
  - The inflammatory response
- Third line of defense: **immune system**:
  - Antibodies
  - Lymphocytes
- The first and second lines of defense are non-specific.
- The second and third lines of defense involve the lymphatic system.

![defenses](./img/09_Intro_Immunology/defenses.png)

### Bacteria and Antibiotics

- Most bacteria cannot survive on the skin from the **inhibitory effect of lactic acid and fatty acids** in the sweat and sebaceous secretion and the low pH.
- The **sebum (חלב)** is secreted by sebaceous glands. Together with hair follicles, it contains fatty acids and lactic acid which inhibit bacterial growth.
- Tears and saliva contain **lysozyme** which is an enzyme that kill bacteria by degrading their cell wall.
- Mucus secreted at the inner surface of the body blocks the adherence of bacteria to epithelila cells. They are then removed by coughing, sneezing, etc.
- **Commensalism** is a type of relationship between two different organism that live together but neither benefit or harm each other.
- Our body contain commensal bacteria (~4kg-5kg or our body weight - $`10^{14}`$ microorganisms which is 10 times the number of cells in the human body).
  - Those bacteria use a lot of space thus preventing other pathogens to grow as well as compete for nutrients and produce inhibitory substances.
- When taking antibiotics, they kill some of the commensal bacteria, giving space for other pathogens to grow.

### Innate Immune Response

- It is activated by **PPRs** (Pattern Recognition Receptors) that react with **PAMPs** (Pathogen Associated Molecular Pattern).
- **Effector cells** (dentric cells, macrophages, etc) are located just under the skin.
- Surface wound will introduce the bacteria to those effective cells.
- Macrophage start the phagocytose process.
- This is done by recognizing the bacteria through receptors.
- The process by the macrophage releases **cytokines** that are important for bringing in the immune system.
- The release of cytokines causes **vasodilation** which is the widening of blood vessels. This process increases blood flow and vascular permeability so fluids, proteins can leave blood and enter tissue (monocytes leave blood cells to go into the wounded tissues).
- The infected tissue becomes inflamed.

![innate immune system](./img/09_Intro_Immunology/innate_immune_system.png)

### Immune Symtem Cell Types

- The immune system is based on two systems:
  - **Effector cells**: dentric cells, macrophages, NK cells, B cells, T cells...
  - **Molecules** called **complement**. Those are ~20 proteins produced in the liver.
- Most cells of the immune system come from the bone marrow, but during the embryo phase cells like macrophage originate from the **yolk sack (שק החלבון)** or **fetal liver**.

![embryo](./img/09_Intro_Immunology/embryo.png)

- White blood cells (**leukocytes**) are used by both the innate and adaptive immune response.
- Leukocytes includes:
  - Lymphocyte
  - Monocyte
  - Eosinophil
  - Basophil
  - Neurophil
- The cells participating in the immune system originate from the bone marrow as **hematopoietic stem cells**.
- **Hematopoietic stem cells** divide into two cells of different types: one stem cell that will divide again and one that will undergo differenciation process.

![hematopoietic](./img/09_Intro_Immunology/hematopoietic.png)

- The hematopoietic stem cells give rise to two types of cells:
  - **Common lymphoid precusor**
  - **Common myeloid precursor**
- **Megakaryocytes** are giant cells (fusion of multiple precursor cells) and are permanent resident of the bone marrow.
- **Platelets** are small packets of membrane enclosed cytoplasm that break off from megakarycytes.
- **Neutrophils** are brought by the blood flow to the inflammated area.
  - Those are effector cells that can undergo the phagocytosis process.
- **Granulocytes** are cells containing vesicules and granules. Their nucleus is different than regular cells. 3 types of cells:
  - Neutrophils
  - Eosinophils
  - Basophils
- **Monocyte** can develop into either macrophage or dendritic cell.
  - They are present under the skin.
  - Monocyte are brought by the release of cytokines.
- Macrophages are like sweepers. They get ride of pathogens as well as cells that underwent apoptosis.

![infection](./img/09_Intro_Immunology/infection.png)

### Molecules

- **Molecules** called **complement**. Those are ~20 proteins produced in the liver.
- Bacterium encounters a complement which activates the complement by cleavage. This is called **opsonization (?)**.
  - Opsonization: antibody opsonization is a process by which a pathogen is marked for ingestion and destruction by phagocytes.
- Monocyte that has a receptor for this activated completement will bind to it and engulf the bacterium.
- Complements can be activated by other means (by binding to the FC part of the antibody).

![complement](./img/09_Intro_Immunology/complement.png)

### Adaptive Immune Response

- Lymphoid system. 3 types of cells:
  - B cells
  - T cells
  - NK cells (Natural Killers)
- NK cells are big cells that will kill cells that were infected by viruses.
  - The infected cells present new proteins (antigens) on their membrane from the virus allowing the NK cells to recognize them.
- B and T cells have on their membrane **BCH (B-Cell Receptor)** and **TCH (T-Cell Receptor)**.
  - Those are the antibodies.
  - One part is called constant region and the other the variable region containing the **antigen-binding site** that recognizes a specific antigen.
  - The variaible region can identify either continuous amino acids sequences or discontinous amino acids sequences (meaning, for example, identifying the 5th and 8th amino aicds of a sequence and not what's in between).
- The BCH can be removed from the cell (disolved) but not the TCH.
- When the TCH/BCH encounter antigens a signal is sent within the cell.
- More about B and T cells [here](https://www.youtube.com/watch?v=NMOHWry8EDc)

![antibodies](./img/09_Intro_Immunology/antiboies.png)

#### B-Cells

![activation](./img/09_Intro_Immunology/b_cell_activation.png)

- B-cells contain a **light chain** and **heavy chain** both of which contain a variable region.
  - The heavy chain contain the constant region but not the light chain.
- Because B-cells' variable regions are specific to each antigens their production from gene to protein need some variability.
  - The B-cell genes are called **DNA segments** (V, D and J in heavy chain and V and J in light chain) that recombine to form unique receptors (there are several segments of each type).
  - First D segment and J segment recombine, then V and DJ segments recombine.
- After the recombination of the DNA, transcription and splicing happen.
- Once a B cell has underwent recombination it can be reversible thus the cell has become specific to an antigen.
- The recombination process is random and gives us an infinity of B cells.
- The light chain is created only after the cell divided a few times. This allows for the same heavy chain to be paired with different light chains.
- The differenciation from encountered unknown pathogen takes around 10-14 days.

![antibody gene](./img/09_Intro_Immunology/antibody_gene.png)

- When a B cell encounters a matching antigen, a signal is sent within the cell causing the proliferation of this specific B cell (with its specific receptor).
  - Now those are effector cells since they are activated and in action.
  - They are considered **plasma cells** and release antibodies when encountering pathogens with matching antigens.
  - Some plasma cells stay in the bone marrow, some go to the infected site and some become **memory cells**.
- Memory cells are the cells allowing for a fast response against known pathogens.

![clonal theory](./img/09_Intro_Immunology/clonal_theory.png)

#### T-Cells

- Two classes of T-cell receptor: α:β T-cell and γ:δ T-cell.
- T-cells are specific to a part of the antigen and not the whole antigen.
- Once a T-cell encounters its antigen, it undergoes differenciation in one of 3 possible ways (killing, activation, regualtion):
  - **Cytoxic T-cells**: they kill the infected cells from viruses or other pathogens.
  - **Helper T-cells**: they send signals that activate B cells to produce antibodies. Some also activate macrophages.
  - **Regualtory T-cells**: they suppress the activity of other lymphocyte to control the immune response.
- T cells can also become **memory cells**.

### Lymphatic System

- **Primary (or central) lymphoid tissues**, where lymphocytes develop and mature. This includes the bone marrow (B cells) and the thymus (T cells).
- **Secondary (or peripheral) lymphoid tissues hwere mature lymphocytes become stimulated to respond to pathogens (lymph nodes).

![b and t cells](./img/09_Intro_Immunology/b_t_cells.png)

### Intracellular and Extracellular Pathogens

- Antibodies can defend against external pathogens.
- Antibodies cannot get into the cells in order to defend agains internal pathogens.
  - **CD8 T cells** can identify those cells by their MHC-I and produce **cytokines** that will cause **lysis** of the infected cell.

| T cells CD4                               | T cells CD8                               |
|-------------------------------------------|-------------------------------------------|
| ![cd4](./img/09_Intro_Immunology/cd4.png) | ![cd8](./img/09_Intro_Immunology/cd8.png) |


### Antibodies

- Several roles:
  - Can neutralize toxins.
  - Recruiting other cells and molecules. The FC part of the antibody is responsible for this: macrophages have FC receptors that will identify the antibdobies bonded to bacteria allowing the phagocytosis.
  - Complement activation through FC part.
  - Neutralizing viruses.

![antobodies roles](./img/09_Intro_Immunology/antibodies_roles.png)

- The **constant region** of antibodies contains one of **five basic heavy chains sequences** (isotype).
  - The heavy chain isotype determines the class of the Ab.

![antibody structure](./img/09_Intro_Immunology/antibody_structure.png)

- IgM and IgE have an FC part longer than IgG, IgD and IgA.
- The location and number of **disulfide bonds** is different in each type of antibody.
- The hinge (the part connecting the FC part to the rest is different in classes and subclasses (IgG1, IgG2, IgG3, IgG4).
- There are two classes of light chains: **kappa** and **lambda**.
- IgG, IgE and IgD have a regular structure but IgM and IgA can have different structure:
  - IgM can be a **pentemere** (as well as monomere).
  - IgA can be a **dimer** (as well as monomere).
- In order to get a high response from the immune system (also with vaccines), two exposures are needing.
  - One for primary immune system which releases IgM and one for secondary immune system which releases IgG.
- Foetuses get the mother's IgG until 9 months after birth. The peak being right before birth.
  - IgM, IgG and IgA increase slowly from birth.
- Blood types: [see here](../Semester_1/Genetics/04_Extensions_Mendelian_Genetics.md#the-a-and-b-antigens).
- Giving the wrong blood: agglutination.
- During delivery, Rh antigens can enter the mother's blood cause her body to produce anti-Rh antibodies which can be dangerous for the next pregnancies.
  - Anti Rh antibodies are of the type IgG thus can cross the placenta.
  - Anti ABO antibodies are of the type IgM and cannot cross the placenta.
  - **Coombs test** allows to test for rhesus incompatibilities. There are two types: **direct** and **indirect**.
  - Indirect Coombs test: maternal serum + $`Rh^+`$ red cells + rabbit anti-humanantibody $`\rightarrow`$ see if there's agglutination (incompatible).
  - Direct Coombs test: take sample from the foetus + maternal antibodies  + rabbit anti-humanantibody $`\rightarrow`$ see if there's agglutination.
  - In case of incompatibility, at the 28th week of the pregnancy, the mother is injected with **anti-Rh IgG** which inhibits the primary response against RhD.
### Complements

- Around 20 proteins are complements.
- One role is to increase phagocytosis.
- One central role is to perforate the target cell.
- There are 3 pathways a complement can perforate a targeted cell:
  - **Alternative pathway**: first to act: 
    - When a complement binds to a bacterium it is activated and undergo cleavage that goes to the next complement and so on and so on.
    - Recruitment of inflammatory cells.
  - **Lectin pathway**: second to act:
    - Manose-binding protein.
    - Opsonization of pathogens (facilitating phagocytosis).
  - **Classical pathway**: third to act:
    - Antibody reacts to antigen causing its FC part to be available for complement to be activated.
    - Perforation of pathogen cell membrane.
- **IgM** and **IgG** are the antibodies that are able to bind to complements (not IgE ir IgA).
- **Complement cascade**: processes activate one another. 
 
![complement cascade](./img/09_Intro_Immunology/complement_cascade.png)

- Complement cleavage results in two parts: c3a that recruits phagocytic cells and c3b that are covalently attached to the pathogen.

## Summary

![summary](./img/09_Intro_Immunology/summary_all.png)
