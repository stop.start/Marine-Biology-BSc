# MicroGenetics
 
## Recombination

- Recombination generates new genes/alleles, allow the integration of DNA element/virus and is involved in DNA repair.
- Types of recombination:
  - **Homologous**: repair harmful breaks in DNA double strand $`\righarrow`$ double-strand breaks (during meiosis - crossover).
  - **Site-specific**: SSRs (site-specific recombinases) perform DNA segments rearrangements by recognizing and binding to short DNA sites.
  - **Transposition**: DNA segment moves from on location to another (either on the same chromosome or another).
- **Genetic recombination**: transfer of DNA from one organism to another. The DNA may be integrated into the recipient's chromosome.
  - In bacteria the process includes: **transformation, transduction, conjugation**.

### Transformation

- DNA fragment from **dead, degraded bacterium** enters a competent recipient bacterium.
- The fragment is exchanged with a DNA fragment of the recipient's DNA.
- Can be natural or artificial.
- Not all bacteria are transformable.
- Those who can have special proteins (competence-specific proteins) that are involved in the process.
  - This includes a membrane-associated DNA-bindin protein, a cell wall autolysin and various nucleases.
- In order to make a cell allow DNA to enter **detergents** or **electrical fields** are used.
- **Electroporation** is a technique used to get DNA into organisms that are difficult to transform (especially those with thick cell walls):
  - Cells are mixed with DNA and then exposed to brief high-voltage electrical pulses.
- **RecA** protein allows the genetic exchange.
- The recipient of foreign DNA is called **transformant**.
- Transformation will happen more around similar genes.

## Phage Genetics

![phage structure](./img/05_Microgenetics2/phage_structure.png)

- Two main types of phages: lytic and lysogenic.
- Phages inject in bacteria their genetic material. Phages are produced within the cell until lysis of the cell releasing the phages. This is called the **bacteriophage lytic cycle**.
- **Lysogenic phage** also called temperate phage or prophage, doesn't produce new phage right after injection, instead the its DNA is incorporated in the bacteria DNA.
  - The bacteria replicate which also replicate the phage DNA.
  - At some point (called **induction**), the phages are produced and will cause lysis of the cells.
- Putting phages on agar plates with bacteria will show "holes" because of the lysis of the bacteria caused by the phages.
- **λ phage** is a lysogenic phage found in gut bacteria like E. coli.
- **T4 phage** is a lytic phage also found in E. coli.
- The genetic material of phages dictates what kinds of bacteria they can attack.

![phage genome](./img/05_Microgenetics2/phage_genome.png)

### Transduction

- Happens from mistake in the DNA that is passed on to the newly assembled phages.
- Two types of transductions:
  - **Generalized transduction**: DNA from lytic bacteriophage.
  - **Specialized transduction**: DNA from lysogenic bacteriophage.

#### Generalized Transduction

- Phage received fragment of bacterium's DNA by mistake.
- Lytic cycle cause the bacterium to explode.
- Phage with bacterium's DNA injects it in new bacterium which then can be incorporated in this new bacterium's DNA.

![generalized transduction](./img/05_Microgenetics2/generalized_tranduction.png)

- Transduction can be used to map genes since the closer the genes the higher the chance for both of them to enter the phage.

#### Specialized Transduction

- In addition of part of the phage DNA, some bacterium's DNA in picked up and replicates as part of the phage's genome.
- Because only part of the phage DNA is passed on to the phage it makes it defective.
- The phage then injects the mixed DNA into new bacteria that is then incorporated into the new bacterium's DNA.


## Conjugation

- Mixing two bacteria strains, both of which lack certain properties (proteins) needed to grow colonies, but together some bacteria suddenly have all the required properties meaning that the genetic material was transfered between bacteria.

![conjugation](./img/05_Microgenetics2/conjugation.png)

- Usually the transfer of DNA between bacteria requires a **sex pilus**.
- There are **3 conjugative processes**:
  - F⁺conjugation
  - Hfr conjugation
  - Resistance plasmid conjugation
- **Plasmids**:
  - Extrachromosomal replicons.
  - They play a significant role in bacterial adaptation and evolution.
  - Most are circular and double stranded.
  - Genes encoded by plasmid can be essential for growth.
- **Sex pilus**:
  - Encoded by plasmid F.
  - Bacteria carrying the F plasmid are called males, the ones don't are called females.

![sex pilus](./img/05_Microgenetics2/sex_pilus.png)

- **F⁺conjugation**: 
  - Genetic recombination where a male transfer an F⁺plasmid to a female.
  - Other plasmids, like antibiotic resistance, may also be transfered.
  - Only plasmids are transfer, not the chromosome.
- **Hfr conjugation**:
  - Hfr = high frequency recombinant.
  - Still through sex pilus.
  - Fragemtns of DNA from a male donor are transfered to a female recipient following the insertion of an F⁺plasmid into the donor (male) DNA.
  - Usually the F⁺plasmid is not complete and so the recipient usually remains female (F⁻).

![hfr](./img/05_Microgenetics2/hfr.png)

- **Resistant plasmid conjugation**:
  - Also called R-factor or RTF).
  - Tranfer of an **R plasmid** which encodes for multiple antibiotic resistant and sex pilus.
  - Also uses a sex pilus for the transfer.
- Summary:

![summary](./img/05_Microgenetics2/summary.png)
