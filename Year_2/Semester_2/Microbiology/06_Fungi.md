# Fungi

## Intro

- Eukaryotes.
- Most are multicellular.
- Cell wall made of **chitin**.
- **Heterotrophic**.
- In order to obtain nutrients, they release an enzymes outside of their body (exoenzyme).
- They can be:
  - **Decomposers**: mostly plants and some animals.
  - **Symbionts** (commensals, mutualists): lichen, mycorrhizas and others.
  - **Parasites**: mostly plants and some animals.
- Example of fungi: molds, mildews, mushrooms, yeast.
- Plants obtain food by photosynthesis, animals by eating plants, animals, etc. Fungi obtain food by **absorbing** small molecules (with the release of enzymes).
- Every year more species of fungi are discovered.
- Ecosystems depend on fungi as decomposers since they can decompose food, wood (lignin) and even plastic.
- Some fungi are pathogens.
- Some are **saprobes**: break down dead organic matter.
- Fungi are the only organisms that are able to digest both **cellulose** and **lignin** found in wood.
- **Mycorrhizae** is an example of mutualistic fungi.
  - Lives on plant roots which give it sugars while the fungi give the plant nutrients containing N or P.
- Around 30 fungi species can cause diseases in humans, usually non threatening.
- Parisitic fungi can damage crops (wheat, corn).
  - Many developped resistance to fungicides.
  - A solution could be like a vaccine, by injecting beforehand the crops with benign strain of the pathogen.
  
## Anatomy

- **Anatomy** - 2 growth forms:
  - **Multicellular**:
    - Made of a **network of filaments**.
    - Each filament has a thickness of only one cell - 2 growth forms:
      - **Multicellular**:
        - Made of a **network of filaments**.
        - Each filament has a thickness of only one cell.
        - One filament is called **hypha** (pl. hyphae).
        - Mass of hyphae are called **mycelium**.
        - Reproductive portion is called **fruiting body**.
      - **Unicellular**: called **yeast form**.
- Some fungi are **dimorphic**: they can switch between both growth forms depending on the conditions.
- **Mycelia** are the ecologically active bodies of fungi.

![fungi anatomy](./img/06_Fungi/fungi_anatomy.png)

- Yeast cannot grow as multicellular (only unicellular).
- In all fungal growth forms:
  - All cells are next to the environment (none are completely surrounded by other cells).
  - This allows to absorb more nutrients.
  - No cells depends on others for nutrition.

| **Septate Fungi**                      | **Aseptate Fungi**                       | **Parasitic Fungi**                        |
|----------------------------------------|------------------------------------------|--------------------------------------------|
|Hyphae are divided into cells by crosswalls called **septa** with pores to allow nutrients and organelles to flow freely|Hyphae lack those crosswalls (coenocytic)|Modified hyphae called **haustoria** which penetrates the host tissue while remaining outside|
| ![septate](./img/06_Fungi/septate.png) | ![aseptate](./img/06_Fungi/aseptate.png) | ![haustoria](./img/06_Fungi/haustoria.png) |

- Although both fungi and plants cells have a cell wall, fungi cells lack cellulose and chloroplasts.
- **AVC** (Apical Vesicular Complex): 
  - Found in **ascomucetes** and **basidiomycetes** fungi.
  - Found at the actively growing hyphal tip.
  - It is a mass of vesicles that surround a structure called **Spitzenkorper** which contains an enzyme called **chitosome** responsible for producing chitin.
  - Since it needs energy (to grow), there are several mitochondria.
- Fungi cell wall has a thickness between 50nm and 250nm and is composed of:
  - **Chitin**: polymer of sugar N-acetylglucosamine (NAG) produced by **chitin synthase**. The chitin is arranged in **microfibrils**.
  - **Glucans**: polymer of glucose bound with covalent bonds. Amophous (not arranged in any pattern).
  - **Proteins**: water-resistant hydrophobins (6% of all proteins) that make a dehydration resistant layer when the hypha is in contact with the air.
  - **Melanin**: black pigment preventing UV radiation damage. Formed by phenoloxidase enzymes.

![fungi cell wall](./img/06_Fungi/cell_wall.png)

- Fungi cell wall are diverse in their composition.
- Besides fruiting bodies, spores can be formed directly on hyphae or inside **sporangia**.
- Funfi nuclei:
  - 1-3 µm diameter.
  - 3-40 chrommosomes.
  - Up to 13-40 million base pairs DNA.
  - 6000-13000 genes.
- Beadle and Tatum research yeast.

## Reproduction

- Reproduction of fungi is done either sexually or asexually, both through **spores** (נבגים). Different species have different spores processes.
- **Fruiting body**: structure that contains the spores.
- Spores are usually unicellular and haploid.
- Spores are formed either:
  - **Sponrangiospores**: produced in sponrangia.
  - **Conidiospores**: produced at the tips of specialized hyphae.
- In good conditions, the reproduction is done asexually (mitosis) by producing great quantity of spores.
- Sexual reproduction (meiosis) occurs when the conditions are not good since it results in a greater genetic diversity thus more chance to survive.
- Asexual reproduction can also be done by:
  - **Fragmentation**: hyphae break off.
  - **Budding**: small part of hyphae pinches off.
- **Sexual reproduction** consists of 3 phases:
  - **Plasmogamy**: fusion of protoplasts.
  - **Karyogamy**: fusion of nuclei, in some species this phase does not occur immediately after plasmogamy and instead form a **dikaryon** (two nuclei thus n+n and not 2n) until karyogamy takes place.
  - **Meiosis**: 
- Both plasmogamy and karyogamy are **fertilization** phases.

![reproduction](./img/06_Fungi/reproduction.png)

- PPT FungiPartB / slides 23-26.
- Fungi are uare usually haploid but can be diploidd.
- They can have one or more nulceus per cell (without septa fungi are **multinucleate**).
  - One nucleus per cell (hypha): **monokaryon**
  - Two nuclei per cell (hypha): **dikaryon**.
  - Multiple nuclei that are identical: **homokaryotic**.
  - Multiple nuclei that are different: **heterokaryotic**.
- **Ascomycota**: type of fungi that:
  - Fuse their cells.
  - Nuclear fusion happens immediately after.
  - Meiosis and mitosis happen resulting in structures called **ascus** containing 8 spores called **ascospores**.

![ascomycota](./img/06_Fungi/ascomycota.png)

- ppt FungiPartB/ slide 29.
- **Spermatia**: spore that is carried by air, water, insects, etc until it meets a somatic receptive hyphae.
  - The content of the spore is transfered to the hypha resulting it becoming dikaryotic.
- **Somatogamy**: fusion of two somatic hyphae resulting in a dikaryon from which a dikaryotic mycelium may develop.
  - In the **Basidiomycota** the mycelium continues to grow in the dikaryotic state for some time until fruiting bodies form.

## Evolution

- Four species of fungi:
  - **Chytridiomycota**: only one with flagella.
  - **Zygomycota**: the one that grows on bread.
  - **Ascomycota**
  - **Basidiomycota**

### Chytridiomycota

- Earliest fungi.
- Produces motile spores (flagella).
- In aquatic habitats.
- Saprobes and parasites.
- Can be seen as Protists but have a lot in common with fungi:
  - Absorb nutrients
  - Chitin cell wall
  - Presence of hyphae
  - Metabolism
- Reproduction asexual and sexual.

### Zygomycetes

- Need water to grow.
- Grow on food like bread, fruits, veggies.
- Grow rapidely.
- They produce **mycorrhizas** (root fungi).
- Reproduce with **zygosporangia**.
- Sexual reproduction occurs by fusion of **gametangia**.
  - Dikaryotes are formed (dykaryotic zygosporangia).
  - Hyphae without septa.
- Asexual reproduction through hyphae that produce clumps of erect stalks called **sporangiophores** forming sporangia.
- ppt FunfiPartC / slides 13-17.

![zygomycetes](./img/06_Fungi/zygomycetes.png)

### Ascomycota

- Sexual reproduction with asci (pl. of ascus).
- Parasite and saprobe to plants.
- Yeast and most lichen.
- Spores in sacs.

### Basidiomycota

- **Basidia**: make spores.
- Enzymes that decompose wood (lignin).
- Make Mycorrhizas.
- Hanging spores.

### Yeast

- Unicellular.
- Reproduction asexual and sexual.
- Some are problemetic.
- Adapted to liquid (slide 25).
- 5500 genes in 16 chromosomes.
- **Molds**: slide 30.

### Mycorrhizas

- Mutualism between fungus and plant. Fungus brings nutrients and water to the plant while the plant gives carbohydrates to the fungus.
- It helps the plants to grow more.
- Types of mycorrhizae (ppt FungiPartC/slide 34-37):
  - **Ectomycorrhizae**: 
    - Ascomycota and Basidiomycota. 
    - Grow on the surface of plant roots without hyphae penetrating the cells, just grow in between the cells (ppt FungiPartC/slide 34-35).
    - Brings nitrogen to the plant.
  - **Arbuscular mycorrhizae**:
    - Zygomycota.
    - Hyphae penetrate the cells
    - Brings phosphorus to the plant.

### Lichens

- Symbiose between fungus and alga or cyanobacterium.
- Grow in tundra habitats usually on rocks and break them down to form soil.
- Fungus can be parasitic.
- Biomonitors since they are very sensitive and act like sponges.
- Grow slow.
