# Intro to Virology

- Viruses can have genetic material either DNA or RNA.
- Needs a host to reproduce and "live".
- Viruses know how to use ribosomes, mitochondria and other organelles that it lacks in order to replicate itself within the host cell.
- **Virions** are the different component of the virus that are synthesized by the host ribosomes and then assembled to produce a virus copy.
- After replicating itself, it leaves the cell either by lysis of the cell or by using part of the membrane to get out.
- It is made of:
  - Genetic material (RNA/DNA).
  - Coat capsid (protein coat) around the genome to protect it.
  - Some viruses have enzymes that host cells cannot provide (single strand RNA viruses have RNA polymerase and cancer viruses that are RNA viruses and integrate their genome into the cell have reverse transcriptate to make DNA from RNA).
- Viruses are very small (30nm-1000nm).
- Viruses exist at least since dinausores.
- They exist everywhere.
- Estimated around 8% of our genome is from viruses.
- Classification by:
  - RNA or DNA
  - Symmetry of protein shell (capsid).
  - Presence or absence of lipid membrane (envelope).
    - Viruses that get out of the cell by lysis don't have a lipid membrane, those that leave throught the membrane do have a membrane.
  - Dimension of virion & capsid.
- The attachement step, where the virus binds to the target cell, is done by reactions between proteins on the virus' envelope and proteins on the target cell membrane.
  - Viruses cannot attach to any cell (**tropism**).
- The virus' genome penetrates the host after attachment.
- Steps of viral replication:
  - Recognition of the target cell.
  - Attachment
  - Penetration
  - Uncoating
  - Synthesis of macromolecules (proteins, mRNA, RNA or DNA genome).
  - Assembly of virions.
  - Budding (when membrane) or release of virions.

![cycle](./img/10_Intro_Virology/cycle.png)

- Virus cultivation requires host cells. There are 3 types:
  - **Primary human** foreskin fibroblasts (temporary, the cells will die from shortened telomeres).
  - Mouse fibroblast **cell line** (3T3) - cancer cells that reproduce indefinitely.
  - Human epithelial cell line (HeLa) - cancer cells.
- Another way to cultivate viruses is inside **eggs**.
- Isolating viruses from culture is done via **density gradient** (Opti prep density gradient).
- **Recombinant virus (rAAV)** (adeno associated virus): usually weakened virus that contain external proteins and that can infect cells. This allows to produce antibodies against the protein (?).

![isolation](./img/10_Intro_Virology/isolation.png)

- There are two modes of viral infection:
  - **Lytic infection**: regular cycle
  - **Lysogenic infection**: viral DNA is dormant for a while before entering the lytic cycle.
- **Plaque assay**:
  - Counting viruses in culture cells is done based on the the lytic cycle: holes in the culture mean that a virus entered a cell, replicated causing lysis of the cell.
  - There's a second layer of agar that limits the movement of the virus to limit a secondary infection: only the neighboring cells of the initial infected cell will be infected.
  - The virus solution in diluded with x10 dilution: each dilution is put on an agar plate, the more viruses there are the less cells will be left and vice versa.
  - Titer: the last dilution giving positive result.

![counting](./img/10_Intro_Virology/counting.png)

- Another way to count viruses is by using one of their characteristics:
  - **Hemagglutination**: many viruses bind to the surface of red blood cells caused by surface proteins (like hemagglutinin in Influenza virus).
    - With this method we look for the last dilution cause hemmglutination.
- Today **Elisa** with PCR is used to count viruses.
- Outcomes from viral infection:
  - **Cytopathogenic**: most common - cell death.
  - **Persistent**: continuous production of infectious virus, achieved either by **survival of infected cells** or by a limited spread of virus to neighboring cells (death of cells is counterbalanced by cell division)​. _Example: virus SV-40 in monkeys (it can have 3 different outcomes)_.
  - **Latent**: exists but not exhibited, DNA is integrated into host genome. _Examples: herpes, adeno associated virus_.
    - EBV – Epstein Barr Virus (member of the herpes family) infect B cells. Its linear DNA circularized and not integrated into cellular DNA.
  - **Transforming**: cause cancer, very rare. Mutations caused by entering and exiting genome. _Examples: HPV, HBV, MCV_.
  - **Abortive**: Any cell that will have the appropriate receptor will be infected, yet cells do not replicate a virus with the same efficiency​.
- Oncogenic viruses or retroviruses are the ones causing cancer
- Contact inhibition: stop dividing from contact. In cancer cell it doesn't happen thus they can grow over one another.
- Peyton Rous: chicken sarcoma virus (1909).
  - **v-src** is the oncogene responsible.
  - Its genome was compared to another similar virus that did not cause cancer by first getting their DNA through reverse transcriptase and fragmenting the DNA and then hybridizing those fragments. This allowed to isolate the part causing cancer.
  - This part was found in regular chicken cells.
  - This means that the virus got this part from entering and exiting the chicken cell genome (c-src).
  - Src is a protein kinase.
  - When the virus integrate again in the genome, if it does under a promotor for a highly translated protein then it will translate a lot the part causing cancer.
  - C-src is a proto-oncogene.
- Burkitt's lymphoma: c-myc is moved from chromosome 8 to chromosome 14 under the immunoglobulin promotor.
- In order to find the tumor suppressor genes that are missing thus causing cancer, the **fusion paradox** was used:
  - Fusion of normal and ​transformed cells​ usually results in cancer cells but normal cells were produced.
  - After a while cancer cells were produced.
  - In cancer cell, chromosome 11 is missing which contains the tumor suppressor genes.
- Cancer critical gene types:
  - Tumor suppressor genes
  - Oncogenes
- **Ras**: monomeric GTPase which helps transmit signals from cell surface receptors to the cell interior – in human tumors the Ras has a mutation forming a hyperactive Ras that can not hydrolyze GTP to GDP.

![hpv](./img/10_Intro_Virology/hpv.png)
