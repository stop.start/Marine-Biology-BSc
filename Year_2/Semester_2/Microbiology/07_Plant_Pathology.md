# Plant Pathology

- **Diseases** are plant pathogens or environmental factors that interfere with plant physiology.
  - They cause changes in plant appearance.
- Diseases result from:
  - Direct damage to cells.
  - Metabolism is affected by: toxins, growth regulators, other byproducts.
  - Interfere with the intake of nutrients and water.

![disease triangle](./img/07_Plant_Pathology/disease_triangle.png)

- **Mazz's Disease Pyramid** adds to the above triangle time factor.
- Humans can also be added to the factors of plant diseases (for example not washing the tools used with plants can transfer diseases from one plant to another).

## Factors

### Plants

- All plants can be considered hosts:
  - **Genetics**
  - **Age**: some pathogens will affect more old plants.
  - Some plants are **immune** to certain pathogens, **susceptible** (can be infected), **resistant** (may or may not be infected and may prevent dying from the pathogen).

### Pathogens

- Amount of pathogen (inoculum): too little of it may not affect the plant and kill the pathogen itself.
- Genetics
- Virulence
- Type of reproduction: reproduce once a year or more (monocyclic, polycyclic).
- How it is spread: 
  - Air
  - Soil
  - Seed (seeds are already infected and produced sick plants)
  - Vector (like with the help of bugs).

### Environment

- ppt plantPhytoA/slides 32-35.
- Moisture.
- Temperature.
- Dispersal agents.
- Soil pH.
- Human effect:
  - **Monoculture**: if every year the same plant (veggies) are produced on the same soil, pathogens specific for this plant can grow in the soil and at some point infect the whole culture of this plant.
  - Amount of inoculum: seed quality, disease residues (left after the products were collected).
  - Introduction of new pathogens

## Diseases

- Disease development, although specific for each pathogen, has 6 general steps:
  1. Dispersal of the pathogen to the host.
  2. Penetration and infection of the host.
  3. Invasion and colonization of the host.
  4. Reproduction of the pathogen.
  5. Pathogen dispersal.
  6. Pathogen survival between growing seasons or between hosts.

### Fungi

- More than 100,000 species.
- Most of them are **saprophytes** which means they live on dead organic matter.
- Around 8000 species attack plants.
- ppt plantPhytoA/slide 11
- The symptoms are similar to drought and starvation.

### Bacteria

- Live as single cells or as filmentous colonies.
- Reproduce via mitosis (2 identical daughter cells).
- Don't usually produce spores and need a host to grow and survive.
- Plant infecting bacteria usually require warmth and moist conditions to spread.
- Plant bacterial diseases are less common than fungal or viral diseases.
- They can be:
  - Parasites
  - Saprophytes (live off dead material)
  - Autotrophs (photosynthesis or Chemosynthesis)​
- We can see the symptoms as spots of the leaves, scars and such (see ppt for exact terms).
- Usually cannot invade healthy tissue (need wound or opening to infect).
- Antibiotics are not used on a large scale to not create resistance.
- **Bacterial galls**: plant tissues of roots, stem or leaves grow abnormally.
- **Bacterial leaf spot disease**: bacteria enters through leaf stomata.
  - Symptoms: water-soaking, slimy texture, foul odor.
- Infected plant leaves undergo **hypersensitive response** (HR): spotting of leaves and reduced photosynthetic activity as well as reduced respiration.

### Viruses

- **Obligate parasites**: they can only replicate within a host's cell.
- In virus infected plant, the production of chlorophyll may cease (chlorosis, necrosis).
- Cells either grow and divide rapidly or very slowly (unable to divide).
- More than 400 viruses that infect plants.
- They are transmitted via living factors (insects, mites, fungi, nematodes) or non living factors: rubbing, abrasions, etc.
- Sometimes also transmitted in seeds.
- **ppt plantPhytoA/ slides 22-23**.
- Four categories of symptoms:
  - Lack of chlorophyll formation.
  - Stunting or other growth inhibition.
  - Distortions.
  - Necrotic areas or lesion.
- Pathogens (fungi, bacteria and viruses) can move through the plant cells via the plasmodesmatas.
  - **Movevemnt proteins (MP)** are proteins dedicated to enlarging the pore size of plasmodesmata.

### Nematodes

- Transparent microcopic roundworms up to 4mm long.
- Feed with **stylet**: pierce plants and pull out everything inside (to eat).
  - They can be beneficial by pulling and killing arthropods in the process.
- Their feeding on the plant weakens and stress the plant.
- They also can infect plants by being vectors to viruses. 
- They can infect plants from below ground or from above ground.
  - **Shoot nematodes**: foliar nematodes feed inside leaves between major veins causing chlorosis and necrosis.
  - **Root nematodes**: cause symptoms of lack of nutrients and water as well as growth issues.

## Management and Strategies

- Initial inoculum: eliminate it, reduce it or delay it.
- Shorten exposure to favorable conditions of pathogen reproduction.
- **Sanitation**:
  - Grow on clean soil and keep it clean with clean tools.
  - Inspect new plants and quarantine them if needed.
- **Fungicides** and **bactericides**:
  - Spread with sprays, fumigants...
  - Different types work on different parts: foliar, soid, seed, wound, post-plant application.
  - They can also help with insects.
- **Host Plant Resistance**:
  - Vertical resistance (**oligogenic**): against some genotypes of a pathogen.
  - Horizontal resistance (**polygenic**): not limited to certain genotypes.
- **Crop rotation**:
  - Good against disease in the soil.
  - Some plants might left residues that work against some pathogens.
- **Temperature**:
  - Heat for soil sterilization.
  - Refregiration slows disease progress in harvest material.
- **Biological control**: some bacteria/fungi might allow other fungi/bacteria to enter the plant.
- **Organic amendments**: when using mulch, don't use diseased plants.

## Diseases

### Late Blight

- Pathogen: **_Phytophthora infestans_** (fungus).
- Crops: Tomato and Potato.
- In temperate and tropical climates.
- Symptoms:
  - Leaf lesions
  - Stem lesions
  - Fruit lesions
- Sporangiophores arise through stomata and produce lemon-shaped sporangia that usually release zoospores.​
- Sporangiophores are branched, and indeterminate, with swellings (knees) at the point where sporangia were produced zoospores.​
- Conditions for disease development:
  - 12h of water (high humidity).
- Because it can attack both tomatoes and potatoes, those are not grown next to each other.
- Potatoes are grown is dry areas and are watered in the morning to avoid nights in wet conditions.
- To prevent/control the disease:
  - Use disease-free seeds.
  - Use fungicides.

![late blight](./img/07_Plant_Pathology/late_blight.png)

### Fusarium Wilt

- Pathogen: **Fusarium oxysporum** (fungus).
- Crops: tomato, potato, pepper, eggplant, and more.
- **Forma specialis** (f. sp.): pathogen within the same species. They are host-specfic.
  - f. sp. batatas: in sweet potatoes.
  - f. sp. cubense: in bananas.
  - f. sp. lycopersici: in tomatoes.
- Symptoms:
  - Yellowing that starts at the lower foliage and progresses up the plant.
  - Plant wilts slowly.
  - Vascular browning extends far up the stem and into large petioles.​
- Condition for development:
  - Warm weather
  - Acidic, sandy soils.
  - The pathogen is soilborne and persists many years in the soil without a host.
- To prevent/control the disease:
  - Use resistant cultivars.​
  - Raise soil pH to 6.5 - 7.0.​
  - Clean equipment to avoid infesting new fields.​
  - 5 - 7 year rotation reduces losses but does not eliminate the pathogen.​

![fusarium wilt](./img/07_Plant_Pathology/fusarium_wilt.png)

### Wheat Stem Rust

- **Puccina graminis** (fungus)
- Infects wheat tissues, steal water and nutrients, reproduces (a lot) until the plant tissues errupts sending the pathogen in the air to infect other crops.
- At the end of the season, when fall/winter starts, the wheat plant dies. The pathogen doesn't produce more regular spores (urediniospores), instead it produces black overwintering spores called **teliospores**.
- Teliospores cannot infect wheat but can survive winter.
  - During this stage, the two haploid nuclei of the pathogen fuse together allowing new genetic variations.
  - Cold temperature break the dormant state of the teliospores allowing them to germinate and perfoming meiosis to produce **basidiospores** (they infect other species but not wheat).
  - At some point aeciospores and produced and from them urediniospores are produced.
  
![wheat stem rust](./img/07_Plant_Pathology/wheat_stem_rust.png)

### Ergot of rye

- Pathogen: **_Claviceps purpurea_**
- Crops: Rye, barley, oats, wheat and more.
- **Ergot**: any fungus in the genus _Claviceps_ which are parasitic on grasses.
- **Sclerotia**: compact mass of hardened fungal mycelium containing food reserves. One role of sclerotia is to survive environmental extremes. In some higher fungi such as ergot, sclerotia become detached and remain dormant until favorable growth conditions return (wikipedia).
- There are **toxic alkaloids** in the ergots that can cause health issues.
- _Cleviceps purpurea_ infects only the ovary of cereal and grass plants.
- The alkaloid ergotamine in the ergot scelrotium os the toxic part.

![ergot of rye](./img/07_Plant_Pathology/ergot_of_rye.png)
