# Metabolism

## Metabolism and Physiological Diversity

- Metabolism is all the reactions that occur in an organism. Can be divided in two:
  - Anabolism: biosynthesis of macromolecules and other components.
  - Catabolism: production of energy from energy sources like chemicals and light.

![metabolism](./img/03_Metabolism/metabolism.png)

- Four functions of metabolism:
  - Obtain chemical energy.
  - Convertion of nutrient molecules into cell molecules.
  - Polymerization of monomeric precursors into macromolecules.
  - Degradation and synthesisis of molecules required in specialized cellular functions.
- Regarging the energy and carbon sources, there are different types of organisms from two main types:
  - Phototrophy:
    - **Phototrophs**: energy from light.
      - Phototrophs using organic carbon are called **photoheterotrophs**.
        - Some purple and green bacteria
      - Phototrophs using inorganic carbon $`CO_2`$ are called **photoautotrophs**.
        - Cyanobacteria
        - Some purple and green bacteria
  - Chemotrophy:
    - **Chemolithotrophs**: energy from inorganic chemicals.
      - Chemolithotrophs using organic carbon are called **Mixotrops**.
      - Chemolithotrophs using inorganic carbon $`CO_2`$ are called **chemolithoautotrophs**.
        - Many archaea
        - A few bacteria
    - **Chemoorganotrophs**: energy from organic chemicals.
        - Many bacteria
        - A few archaea

![energy](./img/03_Metabolism/energy.png)

![carbon](./img/03_Metabolism/carbon.png)

- In redox reactions, the bigger the reduction potential between the first electron donor and the final electron acceptor, the more energy the cell can get.
- Ideal electron acceptor in $`O_2`$ but also can be $`NO_3`$ (nitrate), $`CO_2`$ and others.
- Usually NADH is involved in the electron transfer of redox reactions and ATP is the product of those reactions.
- **Dissimilation** (פירוק): production of energy.
- **Assimilation** (הטמעה): inorganic matter (like nitrate, sulfate) that is reduced for biosynthesis (building things, usually sugar). For example: photosynthesis.
- Matter can be dissimilatoric or assimilatoric depending on the reaction.

### Summary

![summary](./img/03_Metabolism/summary.png)

## Respiration and Fermentation

- Two types of respiration:
  - Aerobic: electron acceptor is $`O_2`$.
  - Anaerobic: electron acceptor is not molecular oxygen. Produces less energy than aerobic pathway.
- Appelation of microorganisms according to their $`O_2`$ relation:

![o2 relations](./img/03_Metabolism/o2_relations.png)

- Growing bacteria in testing tube with water and knowing that the dilution of oxygen in water is low, it can be observed that:
  - Aerobic bacteria will grow around the surface of the water where it meets the oxygen.
  - Anaerobic bacteria will grow at the bottom.
  - Falcultative bacteria will grow everywhere in the water (more above the surface).
  - Microaerobic bacteria (don't like too much oxygen) will grow just under the surface and a bit above it.
  - Aerotolerant bacteria (don't care about oxygen) will grow everywhere in the water.
  
![test tubes](./img/03_Metabolism/test_tubes.png)

### ROS - Reactive Oxygen Species

![ros](./img/03_Metabolism/ros.png)

- A **radical** is an atom or molecule with an unpaired valence electron. Usually because of the unpaired electron, radicals are more chemically reactive.
- Radicals are forms either by the loss or gain of a single electron in a non-radical atom/molecule.
- Some ROS exist as free radicals and some as excited forms of atmospheric oxygen.

![radicals](./img/03_Metabolism/radicals.png)

- Formation of ROS in cells are due to stress (can be drougt, temperature, UV, etc).
- There are several mechanisms in cells to take care of ROS. One of them is called **superoxide dismutase** which takes two $`O_2^-`$ and one $`H_2`$ and produces $`O_2 + H_2O_2`$.

![sod](./img/03_Metabolism/sod.png)

- PPT3A/slide 30.

### Fermentation

- Producing ATP without electron acceptor.
- ATP is produced by **substrate level of phosphorylation**.
- PPT3B/slide 3.
- With glucose and without oxygen, the pyruvate resulting from glycolysis can be coupled with NADH to form (depending on the organism):
  - Lactat + $`NAD^+`$
  - Ethanol + $`CO_2`$ + $`NAD^+`$
  - Acetat + $`NAD^+`$
- **Clostridium acetobutilicum**: bacteria from which Weizmann produced aceton and butanol.

## Photosynthesis 

- **Oxygenic photosynthesis**: 
  - Water is the electron donor.
  - Produces sugar and ATP.
  - **Produces $`\bold{O_2}`$**.
- **Anoxygenic photosynthesis**
  - Sulfide $`H_2S`$ or sulfur $`S^0`$ is the electron donor.
  - Produces sugar and ATP.
  - Doesn't produce oxygen but **sulfate $`\bold{SO_4^{2-}}`$.
  - For anaerobic bacteria

## Mehtnogensis

- Bacteria can produce methan $`CH_4`$.

```math
\begin{gathered}
\textbf{Methanogenesis}\\
\boxed{4H_2 + CO_2 \longrightarrow CH_4 + 2H_2O}
\end{gathered}
```

- It usually occurs in **anoxic** environments (oxygen free) or in oxic environment that became anoxic.
- Happens more in **freshwater** and **terrestrial** environment than in salt water because of **sulfate reducers** in the ocean which has a higher affinity with $`H_2`$.
- $`CO_2`$ can be replaced by several compounds that will be fermented before methanogenesis:
  - Methanol $`CH_3OH`$
  - Formate $`HCOO^-`$
  - Methyl Mercaptan $`CH_3SH`$
  - Acetate $`CH_3COO^-`$
  - Methylamines
- Methanogens can be **endosymbionts** (indide protozoa or termites).

## Litotrophic Bacteria

- Rock eaters bacteria.
- Found in waste water, volcanoes, atmosphere, mines and more.
- Produce energy from reduction of inorganic molecules.
- Oxygen is usually the terminal electron acceptor but not always.
- Carbon source: $`CO_2`$.
- They play an important role in the movement of nitrogen, sulfur and carbon through the biosphere.
- Example of bacteria: **Thiobacillus ferrooxidans**.
  - Their environment becomes more and more acidic but it doesn't impact them.

## The Nitrogen Cycle

- Nitrogen is 14% of the dry cell.
- It participates in several processes.
- **Nitrogen cycle**:
  - **Dinitrogen $`\bold{N_2}`$** is 78% of air.
  - $`N_2`$ converts into **amonium $`\bold{NH_4^+}`$**.
  - Amonium converts into **nitrite $`\bold{NO_2^-}`$**.
  - Nitrite converts into **nitrate $`\bold{NO_3^-}`$**.
  - Nitrate converts into **nitrous oxide $`\bold{N_2O}`$**.
- **Dissimilative reduction** of nitrate: from nitrate to dinitrogen.

![n-cycle](./img/03_Metabolism/n_cycle.png)

- Some bacteria oxidize ammonia $`NH_3`$ and use it as an electron donor.
- Some bacteria oxidize nitrite $`NO_2^-`$ and use it as an electron donor.
- **Nitrification** is the process of oxidizing ammonia to get nitrite and **nitrite** is also oxidized to get **nitrate**.
  - **It's an aerobic respiration with inorganic electron donors and oxygen as the electron acceptor**.
  
  ```math
  \begin{aligned}
  & \textbf{Ammonia Oxidation:}\\
  & NH_3 + 1\frac{1}{2}O_2 \rightarrow NO_2^- + H^+ + H_2O \\
  & \textbf{Nitrite Oxidation:}\\
  & NO_2^- + 1\frac{1}{2}O_2 \rightarrow NO_3^-
  \end{aligned}
  ```
  
- **Denitrification**: nitrate accepts electrons and becomes nitrite which becomes $`N_{2(gas)}`$.
  - Anaerobic respiration with nitrate as terminal electron acceptor.
  - Takes place in anaerobic habitats.
  - Usually by faculattive bacteria.
  - Electron donor is usually organic.
  - It is a **catabolic** and dissimilatory** process.
  
  ```math
  \begin{aligned}
  & \textbf{Denitrification:}\\
  & NO_3^- + 6H^+ + 5e^- + \rightarrow \frac{1}{2}N_2 + 3H_2O
  \end{aligned}
  ```
  
  ![de_nitrification](./img/03_Metabolism/de_nitrification.png)
  
- Microorganisms usually prefers ammonia but can work with other types of nitrogen molecules.
- Some bacteria can do the nitrification process and some can do the denitrification process.
- Some bacteria oxidize ammonia and use it as the electron donor.

### ANAMMOX - Anoxic Ammonia Oxidation

- **Oxidation of ammonia in anaerobic conditions**.
- Process of two molecules with nitrogen reacting to produce $`N_2`$ and water:
  - Ammonia donates to nitrite electrons.

```math
\boxed{NH_4^+ + NO_2^- \longrightarrow N_2 + 2H_2O
```

- This process happen only in specific **obligatory** prokaryotes in anaerobic conditions.
- The anammox process happens in a compartment of the prokaryote called **anammoxosome**.

## Fixation of Free Nitrogen

- Two types of bacteria that can fix nitrogen:
  - **Free living**: aerobic like cyanobacteria and azotobacter, anaerobic like clostridium, anoxigenic photosynthetic bacteria like green and purple bacteria.
  - **Symbiotic**: bacteria living in plant roots (example: Rhizopia).
- Nitrogen fixation requires **16 ATP**.
- The enzyme complex involved in the process is called **nitrogenase**.
- There's an industrial process for fixing nitrogen called **Haber process** that requires high temperatures.
- The presence of ammnoia will prevent the synthesis of nitrogenase.
- Nitrogenase can also catalyze the reduction of acetylene to ethylene with is used as an assay  for the enzyme (easier to test).

### The Nitrogenase Complex

- Nitrogenase enzyme complex breaks the triple bond of $`N_2`$ to produce atomic nitrogen and then $`NH_3`$ (ammonia).
- Two proteins compose the complex: **dinitrogenase** and **dinitrogen reductase**.
- The complex is highly sensitive to oxygen thus bacteria living in aerobic environment need to isolate the process from the oxygen by:
  - Having a thick membrane impermeable to oxygen.
  - Processes requiring oxygen are done on the membrane.
- **Heterocyst**: bigger cell lacking oxygen allowing the fixation of nitrogen.
- If there's enough nitrogen in the environment then there's no need to build heterocyst.
- **תועלת הדדית**: 
  - The plant excretes **flavonoids** which attracts the bacteria.
  - The bacteria penetrates the plant and then excretes **nod factors** which causes root hairs to curl.
  - The bacteria reproduce: **infection thread**.
  
### Nitrogen Assimilation

- Two mechanisms:
  - L-glutamate dehydrogenase takes α-ketoglutarate and produces glutamate.
  - The second mechanism takes the glutamate produced from the α-ketoglutarate and produces glutamine. This allows for take in more and more ammonia.
  
### Summary

![summary](./img/03_Metabolism/nitrogen_cycle_summary.png)
