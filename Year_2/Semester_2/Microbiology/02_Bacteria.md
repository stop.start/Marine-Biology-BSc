# Bacteria & Archea

## Bacteria

- The **cytoplasmic membrane** is:
  - Good as a selective permeability barrier but doesn't protect against osmotic lysis.
  - Fluid: **low-viscosity**.
  - 6-8nm thick and can be seen with an **electron microscope**.
  
  ![cytoplasmic membrane](./img/02_Bacteria/cytoplamic_membrane.png)
  
- Some bacteria can strengthen the cytoplasmic membrane with molecules called **hopanoids**.
- 3 types of proteins:
  - **Integral**
  - **Peripheral**: bound to membrane by non covalent bonds.
  - **Anchored**: bound to membrane by lipid molecules.
- **Bacteria** and **Eukarya** have **ester bonds** between fatty acids and glycerol.
- **Archaea** have **ether bonds** between the glycerol and the hydrophobic side chains.
  - Their lipids don't have true fatty acids but **hydrophobic five-carbon hydrocarbon isoprene**.
- 3 classes of membrane transport proteins:
  - **Simple transport**: driven by proton moving down their concentration gradient.
  - **Group translocation**: chemical modification of the transported substance driven by phosphoenolpyruvate.
  - **ABC transorter**: periplasmic binding proteins are involved and requires ATP.
  
  ![transport proteins](./img/02_Bacteria/transport_proteins.png)

- 3 types of transporter: uniporter, symporter and antiporter.
- Function of prokaryote envelope: ppt2/slides 18.
- Cell wall of bacteria:
  - Composed of components found nowhere else in nature.
  - Most important sites to attack by antibiotics.
  - Cause symptoms of disease in animals.
  - Provide immunoligical distinction and variation amonf strains of bacteria.
- Gram positive/negative schema:

![gram](./img/02_Bacteria/gram.png)

- The thickness of the peptidoglycan layer is what causes the gram to be positive (thick) or negative (thin).
- In gram negative there's a cytoplasmic membrane above and below the peptidoglycan.

![gram2](./img/02_Bacteria/gram2.png)

### Bacteria's Cell Wall

- The high concentration of dissolved solutes creates an osmotic pressure around 2atm.
- The cell wall can withstand this pressure and prevent the cell from bursting.
- The cell wall give the bacteria its shape.
- It causes the bacteria to be gram+/-.
- In gram negative bacteria the cell wall contains other components.
- **Peptidoglycan** is a polysaccharide composed of two sugar derivatives (NAG - N-acetylglucosamine - and NAM - N-acetylmurmic acid -) and a few amino acids (L/D-alanine, D-glutamic acid and lysine or DAP). The components form a repeating structure.
  - The bond NAM-NAG is a β(1,4) bond.
  - NAM and NAG look alike but NAM has a **carboxilic group** alowing a affluent (?) bond.
  - **Lysozyme** can break the NAM-NAG bond.
  
  ![petptidoglycan](./img/02_Bacteria/peptidoglycan.png)
  
- Chains of peptidoglycan are connected with **cross-links of amino-acids**.
- Glycosidic bonds are on the x axis.
- Peptide bonds are on the y axis.

![peptidoglycan structure](./img/02_Bacteria/peptidoglycan_structure.png)
![cross-link](./img/02_Bacteria/cross_link.png)

- ppt2/slide 14.
- Peptidoglycan is only found in Bacteria. NAM and DAP have never been found in Archaea or Eukarya.
- Some Bacteria have **lysine** instead of DAP. The difference between them is the carboxylic group in DAP where a hydrogen is found in lysine.
- **Penicillin** prevents the formation of the peptide bond of peptidoglycan.
- In the tetrapeptide (NAG, NAM, lysine, DAP) of peptidoglycan lysine and DAP alternate.
- The backbone of peptidoglycan always alternate NAM and NAG despite having a composition that can vary.
- Some prokaryotes can live without a cell wall.
  - **Mycoplasms** and the **Thermoplasm group** (Archaea) lack cell wall.
  - They survive either because their cytoplasmic membrane is tough (**mycoplasmas have sterol**) or because they live in a compatible osmotic environment.
- In gram-negative bacteria:
  - Peptidoglycan is **10%** of the total cell wall.
  - Most of the wall is composed of the **outer membrane** which is a second lipid bilayer called **LPS - lipopolysaccharide**.
  - The difference between the 2 lipid bilayers is that the outer one is composed of more than just phospholipids and proteins like the cytoplasmic membrane but also **polysaccharides**.
  - In the outer membrane the lipids and polysaccharides are linked to form a complex.
- Polysaccharide portion of LPS is composed of:
  - **Core polysaccharide**
  - **O-polysaccharide**
- In Salmonella species, the core polysaccharride is composed of:
  - KDO (ketodeoxyoctonate)
  - Heptoses (seven-carbon sugars)
  - Glucose 
  - Galactose
  - NAG
  
![LPS](./img/02_Bacteria/lps.png)

- The O-polysaccharide is composed of 4/5 membered sequences of:
  - Galactose
  - Glucose
  - Rhamnose
  - Mannose
  - Various dideoxyhexoses.
- The lipid portion of LPS is called ** lipid A**.
- Lipid A is not a typical glycerol lipid: the fatty acids are connected through the amine groups from a disaccharide composed of glucosamine phosphate and attached to the core polysaccharide through KDO (see above picture).
- In the inner half of the outer membrane are **lipoproteins** that anchor the outer membrane to peptidoglycan.
- The outer membrane is **toxic to animals**.
- Pathogenic species (for humans and mammals) like Salmonella, Shigella, Escherichia cause intestinal symptoms because of their toxic outer membrane (lipid A) which is called **endotoxin**.

![lps2](./img/02_Bacteria/lps2.png)

- The outer membrane is permeable only to small molecules (not proteins) via **porins** (channels).
- The **periplasm** is a region outside of the cytoplasmic membrane where certain proteins function. It is around **15nm wide**.
- Because of the high concentration in protein, the periplasm has a gel-like consistency.
- The **Sec protein-exporting system** in the cytoplasmic membrane allows proteins to reach the periplasm.
- Porins can be specific or nonspecific.

#### Comparasion

![comparasion](./img/02_Bacteria/comparasion.png)

## Archaea

- Doesn't contain peptidoglycan or outer membrane.
- Archaea's cell wall contain:
  - Polysaccharides
  - Proteins
  - Glycoproteins
- Certain methanogenic Archaea contain a molecule similar to peptidoglycan called **pseudomurein** which is a polysaccharide.
- Pseudomurein is composed of alternating **NAG** and **N-acetyltalosaminuronic acid**.
  - The glycosidic bond a **β-1,3** (and not β-1,4)
  - The amino acids are all L-stereoisomers.
- Methanosarcina species have a thick polysaccharide cell wall.
- Halococcus have a similar cell wall but also contain sulfate ($`SO_4{-2}`$) due to beinf halophilic.
  - The negative charge of the sulfate bind to $`Na^+`$ present in saline environments which help stabilize the cell wall in polar environment.

### S-Layers

- Most common cell wall in Archaea also called **paracrystalline**.
- It is arranged in various symmetric manners (hexagonal, tetragonal, etc).
- Some Archaea species only have S-layers where sometimes are found polysaccharides.
  - Example: **Bacillus brevis**
- Besides osmotic lysis protection, S-layers function as:
  - Selective sieve (allow passage of small solutes).
  - Retaining proteins.
- Archaea are resistant to lysozyme and antibiotic penicillin both of which destroy peptidoglycan (absent in Archaea).

## Antibiotics

- In the 1900s the main cause of death were microbiologic (Influenza, Tuberculosis, Gastroenteritis).
- From the 2000s the main cause of death were less microbiologic and more (heart disease, cancer and such).
- The microbiologic diseases decreased or disappeared from the use of antibiotics.

### Penicillin

- Fleming discovered penicillin, the first antibiotic.
- **Penicillum** is a fungi.
- One of the last steps of building a cell wall is cross-linking the peptidoglycan layers (binding of amino acids. Two enzymes are responsible for it (one of them being transpeptidase) whic are also called **penicillin binding proteins**.
- Penicillin is a group of antibiotics that contain **β-lactam**.
- The **β-lactam ring** is able to bind to the enzymes responsible for cross-linking the peptidoglycan layers thus preventing the cross-linking.

![beta-lactam ring](./img/02_Bacteria/beta_lactam_ring.png)

- Antibiotic resistance can be: 
  - The production of altered penicillin binding protein which decreases the affinity for β-lactam.
  - Production of enzymes deactivation β-lactam called **β-lactamase**.
    - In gram positive it is inducible in presence of β-lactam.
    - In gram negative it is produced continuously (even when the antibiotic isn't present).
  
| Altered penicillin binding protein                        | β-lactam deactivating enzyme                               |
|-----------------------------------------------------------|------------------------------------------------------------|
| ![altered](./img/02_Bacteria/altered_binding_protein.png) | ![deactivating](./img/02_Bacteria/deactivating_enzyme.png) |


### Antibiotics Producers

- Because bacteria develop resistance against antibiotics there's always the need for newer antibiotics.
- On petri dishes, we can identify antibiotic producers by the clear zone around them while outside of this clear zone are bacteria colonies.
- In order to test if a an organism for antibiotic properties:
  - Put streaks of it one side of a petri dish.
  - Let it grow.
  - Put streaks of other bacteria above it and incubate.
  - See if the streaks manage to grow close or not to the organism.
  
  ![testing antibiotics](./img/02_Bacteria/testing_antibiotics.png)
  
- There are **3 types of antimicrobial agents**:
  - Green line: living cells
  - Red line: visible cells (dead or alive)

| Bacteriolytic                                                             | Bacteriocidal                                                                                                      | Bacteriostatic                                          |
|---------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------|
| Causes lysis in the bacteria cell to kill it                              |                                                                                                                    |                                                         |
| ![bacteriolytic](./img/02_Bacteria/bacteriolytic.png)                     | ![bacteriocidal](./img/02_Bacteria/bacteriocidal.png)                                                              | ![bacteriostatic](./img/02_Bacteria/bacteriostatic.png) |
| Since the bacteria cell explodes the dead cell isn't visible (green line) | Kills the cell but without lysis thus we can still see them (constant red line) but the viable count decreases (green line) |  Prevents cell division: constant green and red lines since the number of living bacteria doesn't change  |

- **Kirby-Bauer agar disk diffusion**:
  - Petri disk with grown bacteria.
  - Small paper disks that contain antibiotics are placed on the petri dish.
  - It is incubated allowing for the antibiotics to diffuse from the paper disk onto the agar plate with the bacteria.
  - After incubation if the diameter of the clear zone around the paper disks (if any) indicates how effective is the antibiotic.
- β-lactam antibiotics:
  - Penicillins
  - Cephalosporins
  - Carbapenems
  - Monobactams
  - Caphamycins
- β-lactam antibiotics work by binding to **PBPs** (penicillin binding proteins) which are enzymes involved in cross-linking bacterial cell wall components.
- Cell wall synthesis is only one possible target for antimicrobial drugs.
- All **targets for antimicrobial drugs** (**ppt2C/slide 19):
  - Cell wall synthesis.
  - Protein synthesis.
  - DNA & RNA synthesis.
  - Nucleic acid functions.
  - Membrane functions.
  - Metabolic pathways.
- **Mechanisms of antimicrobial resistance** (one or more can be used by the organism):
  - Mutation on the protein that is the target of the antibiotics.
  - Synthesis of enzymes that can break down the antibiotics.
  - Become impermeable to the antibiotic.
  - Overproduction of the enzyme that is the target of the antibiotics: not enough antibiotic to kill the bacteria.
  - If the antibiotic destroys a metabolic pathway the bacteria can use another pathway.
- Bacteria can spread the resistance by transfering plasmids.
- To counteract the antibiotic resistance, we can use a **trojan horse** that is able to get into the bacteria and then release the antibiotic.

## Microbio Growth

- **Proliferation**: increase in **cell number**.
- **Growth**: increase in **cell mass**.
- Growth rate: change in cell number or mass per unit of time.
- Generation time: time required for a population of microbioal cells to double.
- Growth in **batch culture** has 4 phases:
  - Lag phase: before the organisms start dividing.
  - Exponential phase: organisms divide.
  - Stationary phase: every new organism comes instead of another that died.
  - Death phase
- In batch culture, nutrients are not added, thus at some point the culture dies.
- During the death, there is a decrease in viable count but the turbidity (optical density) might not show the decrease if the organism produces some pigment or color.
- Counting organisms is done by either:
  - Using a grid and counting the organisms per squre with the microscope.
  - Dilution (1/10) until it is possible to count the organisms per plate and deduce the original count by multplying by the dilution number.
- Microbial growth depends on the environmental conditions:
  - Temperature
  - pH
  - Osmotic pressure
  - Oxygen (for aerobic organisms)
- Different types of organisms grow in different conditions. The following show the types according to temperature:

![temperature](./img/02_Bacteria/temperature.png)
