# Microgenetics

## Mutations

- **Mutant phenotype** is an altered phenotype resulted from a mutation.
- Wild-type strain can be mutant.
- Some mutations affect only the genotype and some affect the phenotype as well.
- Nomenclature for mutations: write the gene in italic and to add the mutation number (e.g hisC gene mutations: _hisC1_, _hisC2_).
- Using +/- to indicate if an organism has a certain phenotype: $`His^+`$, $`His^-`$
- Some mutations are **selectable** giving the organism some advantage (like resistance to antibiotics).
  - The advantage is revealed under certain environmental conditions.
- **Non-selectable** mutation example is color loss in a pigmented organism which usually doesn't grant any advantage or disadvantage (at least in labs, in nature it can be different).

### Selecting Mutants

#### Negative Selection (or Replica plating)
 
- Master plate with complete medium (all bacteria can grow) and bacteria grown on it.
- Sterelized velveteen is put on a cylinder block.
- The master plate imprints bacterial colonies on the the velveteen.
- Two plates are imprinted from the velveteen:
  - One with complete medium where all the colonies can grow.
  - One with minimal medium where only certain strain can grow.
- Instead of minimal medium the plate can contain antibiotics and thus show the bacteria that are resistant to antibiotics.

![replica_plating](./img/04_Microgenetics1/replica_plating.png)

### Kinds of mutations

- **PPT MicroGenetics1PartA/slides 13-18** (I just put the first slide here).
- **Auxotroph** phenotype: loss of enzyme in biosynthetic pathway.
  - Detected on medium lacking the nutrient.
- **Sugar fermentation** phenotype: loss of enzyme in degradative pathway.
  - Detected on medium containing sugar and pH indicator by the lack of color change.

![mutant table](./img/04_Microgenetics1/mutant_table.png)

- Some mutations are lethal.
- Some mutations are **permissive** (advantage) or **restrictive** (disadvantage).

## Molecular Basis of Mutation

- Mutations can be spontaneous or induced (either from the environment or deliberately by humans).
- Mutations can be caused by natural radiations, chemicals (including oxygen radicals that can convert guanine into 8-hydroxyguanine).
- **Point mutation**: mutation in one base (substitution, deletion or addition).
  - This can result in a change in an amino acid thus changing the polypeptide. 
  - Even if the amino acid sequence isn't altered it still can affect the phenotype byt changing the efficiency of translation of an mRNA.
- **Silent mutation**: the mutation in the third nucleotide of a codon did not affect the amino acid it codes for.
- **Missense** mutation are mutation usually in the first or second place in a code that do result in a change in the polypeptide.

![types of mutations](./img/04_Microgenetics1/mutation_types.png)

- **Transition**: change in **purine** base (A or G) or change in **pyrimidine** base (C or T).
- **Transversion**: change in purine with pyrimidine and vice versa.
- **Frameshifts** have serious consequences.

## Mutation Rates

- When induced it depends on what is used to cause the mutation.
- Because there are mechanisms of repair DNA, it is important to look at the rate of the repairs in addition to the rate of mutations.
- Spontaneous rate: $`10^6-10^7`$ per kilobase pairs of DNA.
- The smaller the genome the higher the error rate. DNA viruses have a high error rate. RNA viruses even higher.
- Statistically there is a higher chance of missense mutations (and a less higher of silent mutation) than nonsense (because of less stop codons in the dictionnary).
- Areas of DNA containing **short repeats** are hot spots for mutations.

## Mutagenesis

- **Mutagen** are agents used to cause mutations.
- **Base analogs** are mutagens that ressemble purine and pyrimidine bases but have faulty pairing properties.
  - 5-Bromouracil is analog to thymine.
  - 2-Aminopurine is anolog to adenine.

![analog](./img/04_Microgenetics1/analogs.png)

![bromouracil](./img/04_Microgenetics1/bromouracil.png)

- **Alkylating agents** are mutagens that induce mutations more frequently than base analogs.
  - Donate **alkyl group** (isopropyl, methyl).
  - Induce transitions, transvertions, frameshifts, chromosome aberration.
  - Example: nitrosoguanidine.
- Two types of chemical mutagens:
  - Mutagenic to both **replicating and non-replicating DNA**
  - Mutagenic **only to replicating DNA**.

![mutagens](./img/04_Microgenetics1/mutagens.png)

### Radiations

- Two main categories of electromagnetic radiations: **ionizing** and **non-ionizing**.
- UV (non-ionizing) are the most used.
  - Purines and pyrimidines absorbs UV strongly (at 260nm).
  - **Photodimers** are the most common consequence of UV on DNA: two adjacent pyrimidines bases join and form a dimer (like thymine).
  - UVs can be used to sterilize.
  - In presence of water, UVs can cause the **hydrolysis of cytosine** which can cause mispairing during replication.

![uv](./img/04_Microgenetics1/uv.png)

- **Forward mutation** is the mutation of the wild-type allele to a mutant allele.
- **Reverse mutation** is a mutation that restores a former mutation to the wild-type allele.
  - **Back mutation**: second mutation at the same site.
  - **Suppressor mutation**: second mutation at a different site.
- Crossing a wild-type with a suppressor mutation can yield 4 types of results:
  - Parent alleles (no recombination):
    - wild-type
    - suppressor
  - With recombination:
    - Mutant allele
    - Unknown with only the suppressor mutation.
    
![crosing](./img/04_Microgenetics1/crossing.png)

## Carcinogenis

- A mutagenic matter can be carcinogenic.
- The **Ames test** uses bacterial mutations to detect potentially hazardous chemicals.
  - Extract from rat liver that requires histidine (cannot produce histidine) is used.
  - The extract is put on two plates: one without histidine (control plate) and one with the chemical we want to check (also without histidine).
  - Because of the lack of histidine there should not be growth in bacterial colonies.
  - If there is growth on the plate with the chemical it highly suggests the presence of a strong mutagen.

## DNA Repair Mechanisms

- Types of repair (E.coli):
  - Light-dependent repair (photo-reactivatio).
    - **Photolyase** cleaves **thymine dimers**.
  - Excision repair.
    - **DNA repair endonuclease** removes the damage base/s, then DNA polymerase fills the gap and DNA ligase seals the break.
    - There are two types: **base excision repair** (repairs simple bases) and **nucleotide excision repair** (larger defect like thymine dimers).
  - Mismatch repair.
  - Post-replication repair.
  - SOS reponse: error prone.
- 
