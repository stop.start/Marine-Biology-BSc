# Intro to Biotechnology

- Mixed of life sciences, engineering and chemistry.
- 4 types of biotechnology:
  - **Green biotechnology**: agriculture
    - Example: Golden rice is a genetically modified rice which produce beta-cartotene which the body converts into vitamine A (help with vitamine A deficiency).
  - **Red biotechnology**: medicine
    - Examples: production of insulin, production of hormones, etc.
  - **White biotechnology**: industrial
    - Examples: use of biological methods to optimize industrial processes, laudry detergents, use of enzymes.
  - **Blue biotechnology**: aquatic
    - See in next lecture
- **Fermentation**:
  - Definition: The chemical breakdown of a substance by bacteria, yeasts, or other microorganisms, typically involving effervescence(bubbles in a liquid) and the giving off of heat.
  - Fermentation as a process of alcohol production: sugars converted to ethyl alcohol.
- Basic stages of biotechnology:
  - 1. **Inoculum**: cell culture of the cells we want to work on.
  - 2. **Bioreactor**: keep stable temperature and pH.
  - 3. **Production**
  
  ![steps](./img/01_Intro/steps.png)

- **Lyofilization**: low temperature dehydration process[1] that involves freezing the product, lowering pressure, then removing the ice by sublimation.
- Cells are put in the bioreactor when they are in the log phase (exponential phase).

![details](./img/01_Intro/details.png)

## Innoculum - bacterial growth

### Step 1

- Medium for **bacterial growth** needs:
  - Water
  - Energy and carbon sources (carbohydrates, sugars and fatty compounds)
  - Nitrogen source (ammonia, nitrate salts, amino acids from protein mixture)
  - Phosphate salts, sulfate salts and trace elements.
- Some mediums are poor but have specific and known components.
- Some mediums are rich but not all components are known.

### Step 2

- **Seed culcture**: prepared from a stored culture that is revived in order to obtain a culture in proliferation state.
- The origin culture is stored in steril conditions.

### Step 3

- **Sterilization of the system**: prevent culture contamination with foreign microorganisms.
  - Done by heating the substrate to a high temperature ($`121\degree C`$ for 20 minutes in autoclave) or by filtration.
- Contamination causes competition on substrate components which reduce growth and can cause the degradation of the product.
- Production of byproducts makes it difficult to separate the desired product.

## Bioreactor - fermentation

- Controlled parameters: 
  - Temperature
  - pH
  - Ionic strength
  - Air supply
  - Mixing speed
- During the fermentation, samples are taken to check the process.
- **Batch production**: raw materials (mediums and cells) are fed into the reactor and the production stage continues until the end of utilization of the raw materials and / or to obtain the desired level of output. (example : yeast – methanol).
  - The product is then separated and the bioreactor cleaned.
- **Continuous production**: continuous feeding of raw materials through a controlled flow. The process is carried out at the production facility. Medium and products from the reactor are fed into a separation facility (each time only part of the product is taken: the cells stay in the biorector and the medium is taken out).

## Product Production

- When the concentration of product reaches a certain level the fermentation is stopped.
- Yeast microbes are separated from the medium and the separation starts.
- Sometimes the product is not in the medium but within the microbes or yeast.
