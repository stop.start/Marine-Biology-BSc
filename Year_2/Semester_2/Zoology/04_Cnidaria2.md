# Cnidaria (Anthozoa)

## Intro

- Corals
- [See intro of the previous lesson](../Cnidaria1.md#Intro)
- 2 subclasses:
  - **Hexacorallia**
  - **Octocorallia**

![subclasses](./img/04_Cnidaria2/subclasses.png)

## Hexacorallia

### Order: Actinaria 

![anemones](./img/04_Cnidaria2/anemones.png)

- **Sea anemones**
- Solitary polyp
- On the left picture, there a several anemones next to each other. This is called aggregation.
- **Pharyx**: food will go through it and reach the gastric chamber.
  - At the center: epidermis since it is an external layer in the middle.
  - Some cells secrete saliva which makes it easier for the food the go through.
- **Pedal disc**: to fix on the floor (or other platform).
- Radial divisions of polyp: number of parts can be divided by 6.

![anemone_schema](./img/04_Cnidaria2/anemone_schema.png)

- Zooxanthellae can be present (through phagocytosis) in the nematocysts.
- When is stress they can secrete white gastric filaments containing a lot of burning cells. 

### Order: Scleractinian Corals

![scleractinia](./img/04_Cnidaria2/scleractinia.png)

- Most live as colony (some are solitary).
- Limestone external skeleton (גיר, calcaire) secreted by the epidermis.
- Tissues bind the members of the colony (קשר ריקמתי).
- The gastric space are linked between members of the colony allowing food sharing.
- The colony rises because the limeston secreted elevates it from underneath.
- Also have partitions but those are now separated by the external skeleton.
  - Those are called **sclerosepta**.
- All the polyps in the colony have all roles (meaning there's no repartitions of roles and everybody do everything).
- Hermatypic/Ahermatypic = zooxanthellate/azooxanthelate
- Some are hermaphrodites, some are sequencials..
- Planula with eyelashes to move. Also can already have zooxanthellae.

### Other orders

- Corallimorpharia
- Zoanthidae
- Antipatharia

## Octocorallia

- Radial divisions: 8 hunting tentacles only.
  - The surface area is increased with pinnules on the tentacles.
- Live in colony although in deep water there's a species that is solitary.
- Mesoglea usually very thick.
- **Coenosarc**: mesoglea that links all the members of the colony.
  - Gastric tunnels are linked.
- Reproduction: broadcasting as well as brooding depending on the species.

### Order Alcyonacea

![alcyonacea](./img/04_Cnidaria2/alcyonacea.png)

  
#### Suborder: Gorgonacea

![gorgonacea](./img/04_Cnidaria2/gorgonacea.png)

- Organic skeleton made of protein gorgonine.
  - Flexible allowing to be sitted in areas with strong currents.

#### Suborder: Stolonifera

- Stolon = שלוחה
- Leaves skeleton after death (?).

### Order Pennatulacea

![pennatulacea](./img/04_Cnidaria2/pennatulacea.png)
