# Intro

![oceanic_biozones](./img/01_Protista/oceanic_biozones.png)

- 6 big extinctions:
  - **Ordovician [~500Ma]**
  - **Devonian [~345Ma]**
  - **Permian [~250Ma]**
  - **Triassic [~180Ma]**
  - **Cretaceous [~65Ma]**
  - **Pleistocene [~0.01Ma]**

![periods](./img/01_Protista/periods.png)

- The ressemblence between some of the current invertabrates and their fossils "ancestors" is evident.
  - Trilobite $`\longrightarrow`$ Limulus (horshoe crab)
  - Ammonite $`\longrightarrow`$ Nautilus

![living fossils](./img/01_Protista/living_fossils.png)

- During the course we'll learn:

![course](./img/01_Protista/course.png)

- Categories of taxonomy:

![taxonomy](./img/01_Protista/taxonomy.png)

# Protista

- Studying eukaryotes.
- Studying by their way of movement.

## Flagellated Protozoans

### _Euglena_ sp.

|                                           |                                               |
|-------------------------------------------|-----------------------------------------------|
| ![euglena](./img/01_Protista/euglena.png) | ![euglena](./img/01_Protista/euglena_pic.png) |

- Move with the help of a **flagella (שותון)**.
- Can change its shape (compress/expend).
- Move in direction of the light.
- The flagella moves in a circular movement (like a proppelor).
- The flagella is composed of **microtubles**: 9 doublets on the outsise + 2 inside.

![flagella](./img/01_Protista/flagella.png)

### Dinoflagellates

![dinoflagellates](./img/01_Protista/dinoflagellates.png)

- Various species (_Ceratium_ sp., _Noctiluca_ sp., etc).
- Posess 2 flagella.
- The **red tide** phenomenon is caused by dinoflagellates (_Gonyaulax_ sp. & _Gymnodinium_ sp.)
- The _Noctiluca_ sp. can cause bioluminescense.
  - The reaction is between a substrate called **luciferin** and enzyme called **luciferase** when in presence of **oxygen** which creates **oxyluciferin + light**.
  
  ![bioluminescense](./img/01_Protista/bioluminescense.png)
  
- _Pfiesteria piscicida_ attacks fish by releasing toxins in the water which do **phagocytosis** on the fishes' skin.
- _Gambierdiscus toxicus_ which sticks on **brown algae** which are then eaten by fish poisoning them and go up the food chain.
- **Zooxanthellae** live in symbiosis with corals (**_Symbiodinium_ sp.**):

| Algae                                                 | Cora                                              |
|-------------------------------------------------------|---------------------------------------------------|
| Provide corals with photosynthetic products           | Provide safe environment                          |
| Provide corals with compounds that act as UV blockers | Provide algae with nutrients (nitrate, phosphate) |
| Enhance corals calcification                          | Provide $`CO_2`$ for algal photosynthesis         |

- **Bleached corals** happens when the symbiosis is broken.

### Choanoflagellates

- Example: _Proterospongia_ sp.:

![colony](./img/01_Protista/colony.png)

- Live in colonies (מושבות) where the individuals are connected by a **jelly-like matrix**.

## Ciliated Protozoans

- Example: _Paramecium_ sp. (סנדלית).

|                                                 |                                                    |                                                         |
|-------------------------------------------------|----------------------------------------------------|---------------------------------------------------------|
| ![paramecium](./img/01_Protista/paramecium.png) | ![paramecium 2](./img/01_Protista/paramecium2.png) | ![paramecium pic](./img/01_Protista/paramecium_pic.png) |

- Those are the fastest single cell organisms.
- They have "eyelashes" called **cilia** all around them, in **ciliary rows**, that allow them to move.
- The eyelashes are a lot shorter than the flagella and they move like a paddle.
  - Also their movement is not synchronised.
- The paramecium is so small that the medium it moves in feels viscous.
- The paramecium lives in fresh water and needs to perform **osmoregulation** because fresh water gets "in".
  - The osmoregulation is done by **contractile vacuaole** (that looks like the sun) which contracts to push out the water out.
  - The "rays" of the vacuaole sun are the ones to absorbs the water.
  - Contracted state is called **systolic**.
  - Relaxed state is called **diastolic**.
- It has a macronucleus and a micronucleus.
- **Trichocysts** in the membrane seem to be use for protection (might be released when needed).
- They perform **conjugation** or **binary fission** to reproduce.
  - **Binary fission**: the cell divide in 2.
  - **Conjugation**: dna recombination can be perform but only the **micronucleus** takes part in this process.
  - Both can be identified by the way the cells are positioned:
  
![reproduce](./img/01_Protista/reproduction.png)

- Ciliated protozoans can also form **colonies**.

## Ameboid Protozoans

### Ameboa

- Move by **cytoplasmic extension** via **actin-myosin** movement.
  - **Pseudopodia** are like legs.
  - **The movement is direction of the pseudopodium** (there can be several).

![amoeba](./img/01_Protista/amoeba.png)

- Has **contractile vacuoles**.
- **Endoplasm**: more liquid cytoplasm inside.
- **Ectoplasm**: harder cytoplasm outside.
- Reproduce by **binary fission**.

### Foraminiferans

- Can build גירני skeleton on the outside.
  - One cell can be up to 3cm.
- Live in the water or on the sed bed (bentos).
- **Reticulopodium**: cytoplasmic extension all around (ameboa like).
  - Also microtubules but made differently.

![foraminiferans](./img/01_Protista/foraminiferans.png)

### Radiolarian

- Build skeleton of silica.
- **Axopods**: spine like cytoplasmic extensions allowing movement.
