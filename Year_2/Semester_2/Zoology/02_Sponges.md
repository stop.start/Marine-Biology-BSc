# Sponges

![tree](./img/02_Sponges/tree.png)

- Phylum: **Porifera**
- Choanocytes are the sister group of the porifera (same ancestor).
- The Porifera are the oldest multicellular organism.
  - They have no brain or nervous system.
- Considered secondary metabolites.
- In this phylum (ממלכה) there are **4 Classes** (מחלקות).
  - **Homoscleromorpha**
  - **Calcarea**: גירני
  - **Hexactinellida**: זכוכיתיים
  - **Demospongiae**: צורניים
- Hypothetical protometazoan:
  - Cells are closed to one anotther so they can communicate.
  - Internal and external ecm (extracellular matrix)  which allow cells to undergo specialization.
  - There's a polarization: anterior pole (front) and posterior pole (back).
- Sponges are filter feeds but not only.

## Design types

- 3 types of sponge designs:
  - **Asconoid**: the simplest. Around 2cm.
  - **Sycon**: increased its face area with folding. Around 0.5cm.
  - **Leucon**: increased even more its face area (most sponges). Around 50cm.

![sponges types](./img/02_Sponges/sponges_types.png)
  
### Asconoid sponge design

![asconoid](./img/02_Sponges/asconoid.png)

- Like a cone, some sponges have several cones joined at the base.
- The inner space is called **spongocoel** (=atrium).
- The **choancytes** face the spongocoel building some kind of layer called choanoderm.
  - Their **flagella** are not used to move but to **filter food**.
- **Pinacocytes** are protective cells on the outside forming the pinacoderm.
- Limestone skeleton (fr: calcaire he: גיר): **sclerocyte** cells.
- **Archaeocytes** cells move in an amoeba movement.
  - They have different roles.
  - They can also be differentiated as other types of cells. This is called **totipotent**.
- The water enters from the outside of the sponge, move through the spongocoel and leave through the **osculum** (space at the edge of the cone).
  - The water leaving the inner space causes the pressure to drop which sucks water in through **porocyte** cells.
  - The opening in the porocyte is called **ostium** which can close if needed.

![asconoid closeup](./img/02_Sponges/asconoid_closeup.png)

### Leuconoid sponge design

|                                        |                                                        |
|----------------------------------------|--------------------------------------------------------|
| ![leucon](./img/02_Sponges/leucon.png) | ![leucon closeup](./img/02_Sponges/leucon_closeup.png) |

- Choanocytes (which are the cells that get the food) form the inner layer of **flagellated chambers**.
- The water enters and exits through **excurrent canals** (exits trough the osculum).
- The flagellated chambers are big compared to the incurrent/excurrent canals and osculum.
  - This causes the water to move slower in the chambers (0.5mm/sec vs 7mm/sec).
  - That's because it needs to stays there more in order to filter the food.
- Food & digestion:
  - Big particles cannot go through the ostium.
  - Archaeocytes can push out inorganic particles that can block the canals.
  - Dissolved organic carbon can also be a source of food.

![feeding leucon](./img/02_Sponges/feeding_leucon.png)

> Prokaryotic microorganisms (e.g. cyanobacteria) are among the most important components of the sponge diet. Dinoflagellates, viruses and organic debris are also included in their diet.  
> Following capture, food particles are moved into the mesohyl interior and are phagocytosed by amoeboid cells, termed archaeocytes.  
> DOC (Dissolved Organic Carbon) could be also removed from seawater via the filtering system of the sponge.  

- **_Chondrocladia lyra_** and **Lycopodina hypogea** carnivorous sponges.

## Classes

### Demospongiae

- 90% of the sponges.
- They are of **leucon** design.
- Different kinds of skeleton.
  - Silica spikes
  - Spongine fiber
  - Mix of the above
  - None of the above but a collagen skeleton

### Hexactinellida

- Design betweem sycon and leucon.
- Skeleton made of silica spikes.
- Usually live in deep water.

### Calcarea

- Skeleton spikes made of limestone.
- Of all design types (ascon, sycon, leucon).

### Homoscleromorpha

- Leucon design
- Membrane made of collagen.

## Sponge spicules

- All shapes and forms

![spicules](./img/02_Sponges/spicules.png)

- **_Monorhaphis chuni_** one long silica spike (in deep water).
- **Venus flower-basket** is a Hexactinellida sponge that can conduct light through luciferin/luciferase reaction.

## Reproduction
 
### Sexual Reproduction

- Most sponges are hermaphrodites.
  - Can be **silmutanously** : sperm and eggs at the same time.
  - **Sequencing**: period the spong is male and periods it is female.
- The fecondation is done in the **mesohyl**.
- Brooding is done in most sponges (הדגרת עוברים).
  - But in some the sperm and eggs are released in the water where the fecondation is done called broadcasting or spawning.
- The larva are released in the water.
- The larva do not stay long in the water, the need to find a spot to sit on and grow into a young sponge.
- The larva of demosponges are called **parenchymella**.
- The larva of calcerous sponges are called **amphiblastula**.
- Sperm that is caught by choancytes will cause them to lose their flagella and direct the sperm to the eggs.
  - After fecondation the larva is released in the water.
 
![larva](./img/02_Sponges/larva.png)

### Asexual Reproduction 

- **_Tethya_ sp.** (תפוזון) creates and releases buds.
- In fresh water (which is not stables), the sponges release **gemmules** (archaeocytes).

![gemmules](./img/02_Sponges/gemmules.png)
