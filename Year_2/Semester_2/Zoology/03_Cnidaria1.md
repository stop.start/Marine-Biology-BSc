# Cnidaria (Medusozoa)

![tree](./img/03_Cnidaria1/tree.png)

## Intro

- **Cnida** = burning cell (תא צורב).
- Phylum: **Cnidaria**
- Subphylum:
  - Anthozoa (next lecture)
  - **Medusozoa** (this lecture)
  - Myxozoa is a new subphylum of cnidaria that are parasites (not in this course).
- There are still arguments about which came first: Anthozoa or Medusozoa, but it goes more towards Anthozoa first, Medusozoa second.
  - From an anatomy point of view Anthozoa seems to have come second.
  - From a molecular point of view it is the opposite.
- Class (in this lecture):
  - **Hydrozoa**
  - **Scyphozoa**
- Cnidaria is the **sister group** of the **Bilateria**.

![subphylum](./img/03_Cnidaria1/subphylum.png)

- Two forms of life found in both Hydrozoa and Scyphozoa:
  - **Polyp**: usually for bentic life (but can be pelagic), can be solitary or in colony.
  - **Medusa**: only pelagic.

![life_forms](./img/03_Cnidaria1/life_forms.png)

- Two types of symmetry:
  - **Radial symmetry** (symmetric around a central point).
    - Advantage: can feel/eat/etc from anywhere.
  - **Bilateral symmetry** (symmetric around an axis).
- Cnidaria are **diploblastic**: they have two real layers:
  - **Epidermis**: on the outside.
  - **Gastrodermis** on the inside.
  - In between the **Mesoglea** which is not considered as a real layer.
- A real layer is a layer of cells that is supported by a **basal lamina** (basic membrane) which is a collagenic layer.
  - The Mesoglea isn't a real layer since it doesn't have any cells.
- Cnidaria don't have a blood system or a developped digestive system, they just have a **gastrovascular cavity** (like a big chamber where the food can be used).

## Hydrozoa

- **Fire corals** are not real corals, there are hydrozoans and are also called **hydrocorals**.
  - Builds **limestone** (= calcaire) skeleton.
- Fresh and sea water.

### Hydra: a good example of Hydrozoa

- It is a **solitary polyp**
- Green or brown.

|                                                    |                                                      |
|----------------------------------------------------|------------------------------------------------------|
| ![green hydra](./img/03_Cnidaria1/green_hydra.png) | ![green hydra2](./img/03_Cnidaria1/green_hydra2.png) |

- They have an **hydrostatic skeleton** which is the liquid in the gastrovascular space.

![hydra structure](./img/03_Cnidaria1/hydra_structure.png)

- **Epitheliomuscular cells** found in both the epidermis and the gastrodermis are epithelial cells with muscle filaments at the base (filaments that can shrink and stretch).
  - In the epidermis, the filaments are **longitudinal (אורכיים)**.
  - In the gastrodermis, the filaments are **circular** (around the gastric chamber).
  - In the gastrodermis, those cells are **nutritive**: they form **food vacuole**.
- **Cnidocyte**: burning cell (תא צריבה).
- **Nematocyst**: the hunting system in the cnidocyte.
  - There are different types depending on the species. The hydra has a type called **stenotele**.
- **Cnidocil** is a modified cilium (like a sensor) that decides when to release the nematocyst.
- Once the nematocyst has been released the cell needs to be replaced (one time shot)

![nematocyst](./img/03_Cnidaria1/nematocyst.png)

- **Receptor cells** are sensors that inform (through synapses) the **motorneurons** if there's a need to move.
- **Interstitial cells** are multipotent cells (they can differentiate into several types of cells but not any type of cell).
  - They can differentiate into cnidocyte.
- In the gastrodermis there are also secretion vesicles that release enzymes in the gastric chamber.
- The reproduction is done through **asexual buds** that break and form a new polyp.

### Life Cycles

- Several types of life cycles.

#### _Obelia sp._

- In the below schema we can see the polyp step as well as the medusa step.
- When in medusa step, it is called **hydromedusa** because it is not a proper medusa.
- Polyp step is in **colony** and not solitary like the hydra.
- The polyp colony is **polymorphic** and bentic:
  - The zooids (=polyp =structure unit) have different roles.
  - **Gastrozooids** are zoids with tentacles that are responsible for providing food.
  - **Gonozooids** are reponsible for producing [hydro]medusa (=reproduction).
- **Hydromedusa** are either female or male (the colony itself is hermaphrodite).
  - Sexual reproduction. 
  - It will then release either an egg or sperm.
  - **Planula** is the larva stage.
  - The planula transforms into a young polyp which will produce buds (=other polyps) that stay attached and have different roles.
- The obelia has a **protective external skeleton made of chitin** called **theca**.

![obelia](./img/03_Cnidaria1/obelia.png)

#### _Hydra_

- There's no medusa phase.
- Just polyp.

![hydra life cycle](./img/03_Cnidaria1/hydra_life_cycle.png)

#### _Gonionemus_

- Both medusa and polyp phases.

![gonionemus life cycle](./img/03_Cnidaria1/gonionemus_life_cycle.png)

#### _Aglaura_

- No polyp phase, just medusa.

![aglaura life cycle](./img/03_Cnidaria1/aglaura_life_cycle.png)

### Example of species

- _Halocordyle sp._:
  - Monomorphic colony
  - Has an hydromedusa phase
- _Aglaophenia pluma_:
  - Polymorphic colony
  - Burning cells called **nematophore**
- _Hydractinia sp._:
  - Can live on Hermit crabs.
  - Polymorphic colony
  - Has **dactylozooids**: like nematophores.
- _Millepora sp._:
  - Fire hydrocoral
  - Has dactylozooids
  - Gastrozooids surrounded by 6 dactylozooids
  - Hydromedusa that are released (in season)
  - External skeleton made of calcium carbonate
- _Physalia physalis_:
  - Portugese Man-O-War
  - Pelagic polymorphic colony
  - 4 types of zooids
  - Tentacle of dactylozooids that can reach several meters
  - Gastrozooids tentacles
  - Pneuomatophore: buoye keepint it afloat.
- _Turritopsis dohrni_:
  - The immortal hydroid
  - Bentic colony with pelagic medusa phase
  - When is stress, the hydromedusa can sink and go back to be a polyp.
  - Transdifferentiation: somatic cells transforms into other somatic cells (in order to go back from medusa to polyp)

## Scyphozoa

### _Aurelia aurita_ 

- Polyp phase as well as medusa phase called **scyphomedusa** (those are the real medusa).
- Like the Hydrozoa, the medusa is the carrier of gametes.
- The polyp is bentic.
- The medusa becomes adult.
- The planula (=larva) becomes **scyphistoma** which in turn (in the right environmental conditions) becomes **strobila**.
  - Strobila is the one that release young medusa called **ephyra**.
  - The scyphistoma phase is the "eating" phase.
- In some species the strobila can go back to being a polyp (scyphistoma).
- A difference between the Scyphozoa and Hydrozoa: 
  - Polyp phase in Hydrozoa is big and the medusa is small. In Scyphozoa is the opposite.
  - Thicker mesoglea in Scyphozoa.
  - In Hydrozoa the surrounding tentacles are for hunting and at the center for sensory purposes.
  - In Scyphozoa in addition to surrounding hunting tentacles, there are 4/8 oral arms.
  - Gastric chamber is divided in Scyphozoa with filaments that make burning cells.
  - In Hydrozoa, 4 radial tunnels for food transport are enough.

![aurelia](./img/03_Cnidaria1/aurelia.png)

- Peripheral nervous system.
- Surrounding muscles allow for movement of the adult medusa.
- Has mouth tentacles in addition to the hunting ones.
- **Rhopalium** (see picture): 
  - Sensory area

![rhopalium](./img/03_Cnideria1/rhopalium.png)

### _Rhopilema nomadica_

- Similar to the _Aurelia aurita_ except that the scyphistoma can bud out more scyphistoma (asexual reproduction).

![rhopilema](./img/03_Cnideria1/rhopilema.png)
