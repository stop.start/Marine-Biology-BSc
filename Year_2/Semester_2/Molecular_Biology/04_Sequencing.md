# Sequencing

- Sequencing is determining the base sequence of a DNA fragment.
- 2 teams developed methods for sequencing in 1975.

- 2 mehtods:
  - **Maxam-Gilbert** (not in use anymore).
  - **Sanger**

## Maxam-Gilbert

- Taking a double stranded DNA fragment (a lot of it) and putting a **radioactive label ($`^{32}P`$) on the 5' ends**.
- The 2 strands are separated.
- The fragments are separated into 4 test tubes.
- A solution is added to the first test tube that breaks the DNA only on G nucleotide (if the fragments stays too long in the solution it will break at every Gs which is not what we want, so the DNA is removed right away in order to break only on a small part).
- In the second test tube the solution breaks on A and G.
- The third on T and C.
- The fourth on C.

![maxam-gilbert](./img/04_Sequencing/maxam-gilbert.png)

- The visible fragments are the ones with the radioactive label on it. This means that the broken fragments (from mixing the solutions) cannot be seen.
- Each DNA fragments breaks at a different random location (but always according to the solution mixed like G).
- Looking at the length of the resulting fragments we can assertain parts of the locations of nucleotids:
  - With the G solution, if a fragment broke at the nucleotide #5 and another at location #12 we can deduce that nucleotides number 5 and 12 are G.
- In order to determine the length of the fragments (=where they broke), the fragments are run in a gel.
- The gels of G and A/G are compared (same with the C and T/C).
- In addition all the gel are compared, the smallest is the first nucleotide, the second smallest the second, etc..

![maxam-gilbert chart](./img/04_Sequencing/maxam-gilbert_chart.png)

## Sanger

- Sanger used **dideoxiribose** for the last nucleotide instead of deoxiribose.
- Deoxyribose has a hydroxyl group on C3 (but not C2 like ribose).
- Dideoxyribose doesn't have a hydroxyl group on C3 and neither on C2.

![dideoxynucleotide](./img/04_Sequencing/dideoxynucleotide.png)

- DNA fragments are separated into 4 test tubes.
- In each test tube there is a standard quantity of 3 nucleotides (dATP, dCTP, dTTP, dGTP) where the fourth has part of it as regular nucleotide and part with dideoxyribose (ddATP, ddCTP, ddTTP, ddGTP).
- When a dideonucleotide is attached the chain stopped since it cannot attach other nucleotides.
- For each nucleotide different strands will stop at different at length.
- The quantity of fragments synthesized can be followed by using radioactive $`\alpha ^{32}P`$dATP (dATP has the phosphate alpha radioactive).
- The polymerase used is T7 that works at 37°C. It was chosen because it could work with dideoxynucleotides.
  - Today polymerases can be modified but this method was developed before we could modify polymerases.
- The fragments are then run in a gel.

### Improving the Sanger Method

- **Dye-deioxy Sequencing**: in order to use only one test tube, fluorescent molecules were added to the dideonucleotides (use of "leash" so they can be attached by the polymerase).

![dye-dideoxy](./img/04_Sequencing/dye_dideoxy.png)

- Dying allowed to use only one lane sequencing in the gel instead of four.
- ppt4/slide 13: automatic sequencing:
  - The gel runs without being stopped.
  - There's a scanner that scans the fluorescent dye so a computer can analyse the output.
  - The output is called a **chromatogram**.
- PCR with Taq polymerase:
  - Allows to use high temperatures (that Taq can withstand) in order to denaturate secondary structure in the DNA that usually prevents most polymerases to read the DNA and get stuck.
  - The high temperatures also allows the primers to attach at the right places since at low temps they can attach at wrong places.
  - Need less initial template.
  - The primer is only on one end and the the synthesis is linear.
- **Capillary sequencing** allows to do electrophoresis in a standartized way.

## HGP (Human Genome Project)

- 1990 start of the project.
- Ended in 2003: 99% of the human genome was sequence.
- The last 1% is non coding areas that are more difficult to sequence.
- Two groups with two different ideologies worked on the project until they joined forces as the HGP.

### HPG's Strategy

- The HGP approach was to first create a genomic map based on known genetic markers (like polymorphic allele).
- With the development of technology, the mapping process was evolving. Instead of using the [_centimorgan unit_ (cM)](../Semester_1/Genetics/05_Chromosomes_Mapping.md#chromosomes-mapping-in-eukaryotes), the number of **nucleotides** was used (physical map).
- Sequencing was organized and was a worldwide effort: labs around the world received specific parts (derived from the genetic map) to sequence.
- The results of the sequencing were appended according to the physical map.

### Celera

- Craig Venter founded Celera in 1998 and started working on the project as well.
- Both HGP and Celera used the same tools to sequence but did it with different strategies:
  - Celera's strategy is called **shotgun**.
  - Celera did not map the genome before sequencing.
  - The technlogy was at a point where sequencing was easy to do and fast and that's what Celera did: fragmenting the genome pretty much at random and sequencing them.
  - All the fragment were entered in a database and then a program was to search overlapping sequences in the fragment to rearrange them in the right order.
    - The issue was that, at the time, the systems were not fast enough to do it since it required to compare millions of base pairs.
    - What made things more difficult was the repeated sequences that were confusing to put back in the right place.
- Because of the difficulties, HPG and Celera joined forces: HPG brought the genomic map and Celera the massive data. Together they finished the project.

### Sequencing ecosystems

- After sequencing the human genome, Vanter went to sequence ecosystems (not just one organism but a whole ecosystem).
- Before this, the way to sequence ecosystem was to:
  - Take a sample.
  - Separate and grow bacteria and then sequence them.
  - Join everything in the ecosystem.
  - The issues were: take a long time and some organisms could not be grown.
- With the new approach, the DNA of all organisms is sequenced: it isn't known which DNA sequence belong to which organism but it allows to sequence everything even the unculturable organisms.
- This project allowed to found new genomes and phenotypes.
