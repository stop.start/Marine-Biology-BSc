# DNA Transcription

- Ribozomes are the most needed in the cell thus the gene (actually genes) is tricribed a lot.
- The **major groove** is bigger and contains more data than the minor groove.
    - The data in major groove allows to differentiate between A-T or T-A for example (same with G-C/C-G).
    
![grooves data](./img/08_DNA_Transcription/grooves_data.png)

- **Footprinting** (deprecated method):
  - Using a small concentration of nuclease (whichs breaks the DNA) on a big volume of DNA, this (statiscally) will break the DNA in every possible way.
  - The experiment is done without proteins and in parallel with proteins.
  - When separating the fragments by gel electrophoresis:
    - Without proteins the gel shows fragments of all lengths.
    - With proteins the gel shows that some length don't have (or have less) fragments. This is because the proteins protect those areas of the DNA from nuclease when they do transcription.
    
![footprinting](./img/08_DNA_Transcription/footprinting.png)

- The fragments with proteins on them can be used to isolate both proteins and DNA. The DNA can then be sequenced.
- **CHIP** - chromatin immuno precipitation: on all the genome. Not sure what it is. Mike missed this.
- Specific parts of the proteins recognize and attach to the DNA. There are few possible motifs (most using helices).
  - **Helix turn helix motif**: most common. Many act as dimers.
  - **Homeodomain**: variation of the HTH motif only for homeodomain proteins, has a hydrophobic core and can also interact with minor groove.
  - **Zinc fingers**: Zn atom encapsulated by either 2 cys and 2 his or 4 cys. Usually in clusters where each finger knows a specific nucleotide. Those can be used in engineering to attach proteins to wanted DNA areas.
  - **Beta sheets** can be placed in major groove.
  - **Leucine zippers**: 2 α-helices with leucines every 7 amino acids which form a hydrophobic core. The end of the helices can bond with the DNA.
  - **Tal effectors** (transcription activator-like)
    - Isolated from plant pathogen **xanthomonas** where it modulates the transcription of the host.
    - More specific and easier to work with than zinc fingers.
    - DNA binding domain is based on conserved repeats of ~34 amino acids.
    - Within the 34 amino acids, 2 amino acids (12 and 13 called RVD) are the ones that bond to the DNA.
    - Each conserved domain of 34 amino acids is part of a "finger".
    - Tal effectors have a lot of "fingers".
    - Each couple of RVD (12 and 13 amino acids in each 34 conserved repeats) has been mapped in a table to the nucleic acids they bind to. _For example NI binds to A, HD binds to C_.
    - It was used for genome editing, gene activation, gene repression. It has more potential uses but this method is less used since CRISPR has been discovered (CRISPR is a lot faster to use).
- The longer the motif the higher specificity and affinity.

## RNA Polymerase

### Prokaryotes

- In prokaryotes, almost all RNA polymerase belong to one protein superfamily. In eukaryotes, there are 3 where RNA polymerase II symthesizes mRNA and I and III rRNA.
- **Holoenzyme** is a complex enzyme build of a **core** and **σ factor**.
- The σ factor is the **transcription initiation factor**.
  - It recognizes the DNA promotor and binds the RNA pol to the DNA.
  - The recognition is on two sites: -10 and -35.
  - It dissociates from the core after initiation.
  - [See here](../Semester_1/Genetics/12_Genetic_Code.md#prokaryotes)
- There are serveral σ factors, each can identify a different promotor consensus.
- Promotors are never at their optimum. This results in the gene being transcribed in quantity needed in the cell (not all genes need to be transcribed a lot).
- Sometimes the RNA pol cannot separate from the σ factor causing the **abortive initiation** process and restarting the whole process.
- **Termination**:
  - [See also here (at the end, before eukaryotes)](../Semester_1/Genetics/12_Genetic_Code.md#prokaryotes)
  - Hairpin structure followed by polyU causes the termination.
  - Some genes are **Rho dependent** meaning that they need the Rho terminator protein to attach to the RNA and help cut the new RNA.
    - The Rho factor is a homo hexamer similar to helicase.
    - It uses ATP.

### Eukaryotes

- [See here](../Semester_1/Genetics/12_Genetic_Code.md#eukaryotes)
- There are three RNA polymerases: RNA polymerase II symthesizes mRNA, I and III synthesize rRNA, tRNA and other types. We'll talk mostly about RNA Pol II.
- In the active site there's Mg (magnesium).
- Doesn't have a σ factor but does have similar complexes. For example, one that attaches to the TATA box (TBP factor).
- TBP factors bends the DNA.
- In all, RNA Pol is surrounded by multiple complexes that help it doing its job.

![tbp](./img/08_DNA_Transcription/tbp.png)

- **CTD - C Terminal Domain**, the tail of the RNA Pol, heptamer (7 nucleotides) repeats (in human 52 repeats).
  - Contains proline (which has a ring).
  - Contains hydrophilic amino acids.
  - Located close to where the new RNA exits.
  - Serines at 2 and 5 locations of each repeat is phosphorilated when the RNA Pol starts synthesizing.
    - The phosphorylation of the 5 serine is linked to the 5' capping.
    - The phosphorylation of the 2 serine is linked to elongation and splicing.
    - After the release of DNA the tail is dephosphrylated.
- **5' capping**:
  - [See here](../Semester_1/Genetics/12_Genetic_Code.md#capping)
  - Happens during the beginning of transcription (~25 nucleotides).
  - The ezymes responsible for it are recruited by the CTD.
  - 3 reactions that adds modified guanine (7-methylguanine) from 5' to 5' of the RNA.
  - After the capping has be successful, **CBC** binds to the cap (cap-binding complex).
- **Splicing**:
  - [See here](../Semester_1/Genetics/12_Genetic_Code.md#splicing)
  - Exons are shorter than introns and more uniform in length.
  - There are consensus sequences but those are not completely understood.
  - **Lariat**: the lassot created when joining the exons.
  - Adenine is used to create the lassot.
  - **Transesterification**: breaking an esteric bond to create a new one.
  
  ![splicing](./img/08_DNA_Transcription/splicing.png)

- **Spliceosome**: 
  - snRNPs: ribo-nucleic-proteins.
  - Complex of proteins (up to 200) and 5 snRNA (small RNA molecules) that performs the splicing (removing the introns).
  - snRNA: u1, u2, u4, u5, u6 (no u3).
  - Requires ATP for assembly and dissassembly.
  - Not completely understood.
  - We know that the spliceosome is assembled during transcription but we are not sure when the splicing itself takes place.
  - After splicing, EJC, exon junction complex, is a protein complex positioned on each splice site and is like a stamp that the splicing was successful.
- **Alternative splicing**:
  - [See here](../Semester_1/Genetics/12_Genetic_Code.md#alternative-splicing)
- **Polyadenilation**:
  - **PAP** poly-A-polymerase performs this tasks by adding around 200 nonencoded ATPs.
  - Once it's added the polyA tail is covered by proteins (pap-binding proteins).
- After all those processes the mRNA binds to an export receptor to get out of the nucleus.
- Just after the mRNA left the nucleus and ribosomes attach to it, the cap is removed and in parallel the mRNA is checked for issues.
  - Once all of the above has been done, the mRNA becomes cyclic.
- **Nonsense mediated**: if the splicing was not done right, there will be and EJC after a stop codon. This is a way to know the mRNA isn't right and is send to degradation.

![nonsense](./img/08_DNA_Transcription/nonsense.png)

- **Nonstop mediated**: another possibility is that there isn't any stop codon and the ribosome gets to the polyA tail and the mRNA is sent to degradation.
- **No-go mediated**: when the ribosome gets stuck in the middle, it is freed and then the mRNA is sent to degradation.
- **Selfsplicing introns**: pre-mRNA that is able to fold in a way that join the exons and removes the introns. Two ways of performing selfsplicing:
  - Group I: introns fold on themselve with a G that "attacks", are cut from the exons and the exons join. They can also do the opposite and insert introns.
  - Group II: A "attacks" causing a loop (lariat) and the cut part attacks the uncut part removing the intron.

![selfsplicing](./img/08_DNA_Transcription/selfsplicing.png)

### rRNA

- No amplification: mRNA can be translated several times in the cytoplasm, for rRNA the number of transcripts is the number of ribosomes since the transcript folds as a ribosome.
- In humans, aroudn 200 genes codes for rRNA. There are in 5 clusters per haploid copy.
- No capping and no polyA but **there is splicing**.
- The transcript is modified before it is spliced:
  - It starts with being built from 4 nucleotides (G,C,A,U) and after the modifications it contains tens of different nucleotides (those are modified nucleotides).
  - **snoRNAs** or guide RNA direct the modifications in the **nucleolus**.
  - Many snoRNA (small nucleolar RNA) are encoded in introns of ribosomal proteins.
- Nucleolus is the part where rRNA transcription takes places as well as assembly of the ribosome. The area is well defined but with no membrane.
- Chromosomes are anchored in the nucleus. The parts of chromosomes with clusters with the ribosome genes are extended to be with the nucleolus (10 chromosomes have those clusters).
- During mitosis the chromoses pull back those clusters and the nucleolus dissapears.
- **Cajal bodies**: responsible for assembly and disassembly of snRNAs.
