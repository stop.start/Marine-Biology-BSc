# PCR - Polymerase Chain Reaction

## Intro

- **Kary Mullis** patented the PCR method which started to be commercialized in 1988 (CETUS company).
- PCR can create a large number of defined DNA fragments fast (a lot faster than with plasmid and bacteria that take several days to multply in the same large quantity).
- [See here for how it works](https://gitlab.com/stop.start/Marine-Biology-BSc/-/blob/master/Year_2/Semester_1/Genetics/18_Recombinant_DNA_Technology.md)
- The cycle:
  - First the DNA undergoes denaturation at high temperature (94°C).
  - The temperature is then lowered to 55°C which allows the hybridization with the primers to take place.
  - The temperature is set at 72°C which is optimal for **Taq polymerase** to replicate the DNA strands.
- The amplification is exponential:

```math
\begin{aligned}
& \boxed{(1 + K)^{\text{cycles}}}\\
& \footnotesize\bold{0<K<1}
\end{aligned}
```

- The K value depends on a lot of variables, 1 means that everything went perfect and 0 means that nothing changed.
- The middle length DNA fragments multiply linearly (each cycle one long fragment creates one middle length fragment, so if we started with a double strand, then each cycle 2 middle strands are formed).
- The longest DNA fragments do not multiply thus their number is constant.
- The target sequence fragments will multiply exponentially.
- On the gel the longer fragments won't be seen since there are too few of them.
- The machine used to perform PCR is simple since it doesn't require any difficult process like centrifugation.

## Methods that use PCR

### AFLP

- Use of PCR combined with restriction enzymes:
  - Gene with two alleles: one can be cut with a restriction enzyme, the other cannot.
  - PCR is used to isolate the gene and while amplificating it (multiply the number of strands).
  - Restriction enzyme is used and will cut only the one specific allele resulting in two smaller while the other allele stays as is (one bigger fragment).
  - The results are run in a gel:
    - One heavy band means homozygote for the allele that cannot be cut.
    - Two lighter band means homozygote for the allele that can be cut.
    - Three bands means heterozygote.

![aflp](./img/03_PCR/aflp.png)

### Multiplexing

- One test tube with multiple primers (needs precise preparation!).

![multiplexing](./img/03_PCR/multiplexing.png)

## Issues and Solutions

### Primer Design

- Checking if mRNA is present in the cell ($`\Rightarrow`$ means the gene is expressed).
- Poly T is used to pull out RNA strands.
- The RNA is then converted to cDNA.
- PCR is then performed with primers that match the gene that is being tested.
- An issue is that when pulling the mRNA some genomic DNA can be pulled as well thus also being duplicated in PCR.
- There are 2 ways to handle this:
  - Using 2 primers: one on the first exon (**send primer**) and one on the last exon (**antisense exon**).
    - The introns in the DNA can be too big for the formation of PCR product or the the product will be alot bigger than the product from RNA.
  - Putting the primers on the sequence that join two exons (see picture).
    - The primers won't be able to sit on the DNA.
    
    ![primer design](./img/03_PCR/primer_design.png)

### Hot Start

- Most DNA polymerase used in PCR and especially Taq polymerase can function also at low temperature (although not as well as at the optimum temp) causing **misshybridization** with the primers while increasing/lowering the temperature. Those product can cause problems during the reaction.
- Another issue is that primers might form dimers. Having a primer with a palyndrome at one of the extremities it will increase the number of dimers. This will form unwanted PCR products at low temperature.
- Hot Start means that all the reagents are combined only at high temperatures.
  - An example is buying Taq polymerase (which is at low temp) inactivated (by an antibody).

### Other Tricks

- Primers are synthesized in a lab which allows us to add sequences to the DNA.
- Mutations can be added with primers. This is called **Mutagenesis with PCR**:
  - On a long enough primer, a nucleotide can be replaced by another causing a missmatch on the first hybridization and afterwards all products will be formed with this mutation.
- In order to add a mutation in the middle, 2 PCR reaction are performed (in two different test tube):
  - A primer with the mutation matches the sequence in the middle giving in both reactions a product with a partial DNA that includes the mutation which is now on the extremity.
  - The products are then put in the same test tube which is warmed up so the strands undergo denaturation become single stranded.
  - When lowering again the temperature, a fraction of the single strands will attach at the mutation extremity (meaning one strand from tube A and one strand from tube B). Other strands will reattach with strand from their test tube.
  - The strands that attached at the 3' end will be able to undergo PCR reaction (DNA polymerase works from 5' end to 3' end) and recreated the long DNA fragment with the mutation in the middle.
  
  ![mutation in the middle](./img/03_PCR/middle_mutation.png)
  
- Taq polymerase (and only Taq) sometimes adds an overhang nucleotide (usually A) at the 3' end.
  - Some enzymes can be used to remove the A overhang.
  - Another solution is to use it with T overhangs that are added to strands by putting them with Taq polymerase and only T nucleotides which will force Taq to add a T overhang. This is called **TA cloning**.
  
## qPCR - Real Time PCR

- At the end of each cycle the number of product is checked.
- **TaqMan** is one of the methods for real time PCR:
  - A probe is located on a strand (see picture). When encountering this situation, some DNA polymerases can be stuck, some will remove it and others will break it.
  - Taq polymerase break the probe (not removing it and does not get stuck). This is called **exonuclease activity**.
  - The probe has a **fluorescent molecules** attached to it called **[R]eporter** (5' end) as well as a molecule called **[Q]uencher** (3' end).
    - The reporter emits fluorescent light as long as the quencher isn't close to it because the quencher absorbs the light emitted by the reporter (this is called **FRET**).
    - The quencher is closed and block the reporter has long as it is still attached to the strand.
    - When the DNA polymerase starts removing the proble it releases the reporter which starts emitting light.
