# Forensics

## DNA Fingerprinting

- 1985 - Sir Alec Jeffreys.
- At first southern blot was used. Today STR regios are used with PCR.
- The loci used are polymorphic.
- **CODIS** (Combined DNA Index System): 20 loci + sex locus checked with the same methods.
  - Those loci were chosen to work for all populations.
- Mitochondrial DNA: motherly lineage.
- New DNA Tool for Law Enforcement: Parabon Snapshot Puts A Face On Crime: phenotype prediction (not age though).
- Microbiome is also used to indentify people.
