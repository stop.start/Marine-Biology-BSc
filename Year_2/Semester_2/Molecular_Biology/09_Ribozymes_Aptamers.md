# Ribozymes & Aptamers

_**Not well summarized cause the lecture was shit**_  

## Ribozymes

- **Tetrahymena** is a eukaryote whose **ribosomal RNA** was used to study splicing.
  - The RNA was cleaned from proteins and still undergo splicing which indicated that proteins were not responsible for the process.
- **Ribozyme** (ribonucleic acid enzyme) is an enzyme made of RNA (and not protein).
- Natural ribozymes:
  - **Cleavers**: cut RNA
    - Involved in the concatenation and circularization of viral rolling circle replication. This means that during replication the strand is cut and fold in a circle and this is done again and again.
    - Example: **Hammerhead ribozyme** has 3 stems (base pairs) and a fixed bubble in the middle. The stems can be anything. used for drugs because of its simple structure.
    - Example 2: **HDV** very compact and hard to denature.
  - **Groups I and II intron**
    - The splicing system that exists today evolved from Group II intron.
  - **RNase P**:
    - In pretty much all cells and organelles that synthesize tRNA.
    - It cuts the 5' part of the tRNA.
    - True enzyme (compared to the ribozymes that work on themselves).
  - **Ribosomes**
  - **Transcription swtiches**
- **IVE - In Vitro Evolution**: building our own ribozymes
  - "Natural evolution and selection" within a test tube.
  - Taking pools ($`10^15`$) of oligonucleotides (DNA or RNA).
  - The pools can be random. Those are called deep blue.
  - The pools can also be semi random. Mutated known ribozymes built from synthesized DNA (?). Those are called doped.
- **Bartel ligase**: RNA ligase - enzyme that is able to make covalent bonds.
- In order to filter the wanted ribozymes, they are "fished out", amplified and then refished until only the ones wanted are left.

## Aptamers

- DNA or RNA that has a fixed 3D shape and is used to bind to its mirror image.
- Short nucleic acid sequence that binds specifically a ligand.
- **SELEX**: method to select molecules from a pool by using a ligand that binds to them. Unlike IVE that works for enzymes SELEX works for all types of molecules.

## End

- Aptamers can act similarly to antibodies – blocking or activating specific targets.​
- Ribozymes can modulate mRNAs by cleaving or splicing (destroying or correcting them). ​
