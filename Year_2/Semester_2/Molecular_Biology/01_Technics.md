# Technics

## Hybridization

- Denaturaion/Renaturation of double strand (DNA/DNA, RNA/RNA, DNA/RNA) is **reversible** and depends on:
  - Length: the longer the more difficult it is to separate the two strands.
  - Concentration: the higher the concentration the faster the reaction.
  - Ionic strength: salt concentration, the lower the concentration the less hybridization there will be.
  - Temperature: the warmer the more difficult it is for the hybridization to take place.
  - Mismatches and secondary structures can happen during the process.

## Restriction Enzymes

- Found in bacteria that have the ability to restric the growth of phages.
- **EcoRI**: first restriction enzyme isolated from E. coli strain R.
- There are different classes of restriction enzymes. The most commonly used/talked about is **class II**
- The recognition site (for the restriction enzyme) and cleavage site are the same.
- The recognition site is a palindrome and the restriction enzyme is a homodimer: this means that both are symetric.
- No ATP required.
- 2 types of cuts:
  - **Blunt** (קצוות כהים): cut in the middle.
  - **Sticky** (צוות דביקים): cut not in the middle creating overhanging ends.
  
  ![cut types](./img/01_Technics/types_cuts.png)

- **HpaII** and **MspII** both use the sequence CCGG (cutting between the two Cs) and produce the same types of ends.
- Sometimes one of the Cs undergoes methylation. 
  - HpaII is sensitive to methylation whereas MspI isn't.
  - To know if a DNA strand underwent methylation both HpaII and MspI are used to cut the DNA, if MspI cut at some locations that HpaII didn't then it can be determined that the strand underwent methylation. 
- Statistically, the CCGG sequence can be found on average every $`4^4 = 256`$ bases (the first C is likely to be found every 4 bases, same for the second C and both Gs thus 4x4x4x4).
  - After cutting the DNA, the fragments can be of different length but the average should be close to 256 bases.

## Gel electrophoresis 
- Two common gels - agarose and acryl amide.
- The DNA samples which are negative (phosphate) will run to the positive end.
- Once **ethidium bromide** was used to color the DNA in the gel results. It is an intercalating agents (between bases). They cause the DNA to deform a bit.

![intercalating agents](./img/01_Technics/intercalating_agent.png)

## Southern Blot

- [See this link](https://www.onlinebiologynotes.com/southern-blotting-principle-procedure-application/)
- Mixing of the 3 above technics.
- Separation of DNA fragments by gel electrophoresis $`\rightarrow`$ gives the number of fragments produced by restriction digestion and give their molecular weight.
- Because of the amount of DNA in the human genome the result of the gel is a continous smear.
- Hybridization of the fragments using labeled probes.
  - DNA is cut into fragments with restriction-enzymes which are then separated by gel electrophoresis.
  - Those fragments are then paired with probes (hybridization).
- **Electro blotting**: dna is extracted from the gel with electricity.
- **RFLP**- Restricton Fragment Length Polymorphism​:
  - Can identify mutations: restriction enzymes cut at specific sites. If there's a mutation that causes the enzyme not to cut at a specific site, running the DNA sequences and comparing to others will show one band instead of two.
  - In the picture the parents are heterozygous for the wild-type/mutation allele. The daughter (white) is homozygote for the wild-type allele. The son is sick and the gene can't be cut by the restriction enzyme due to the mutation.
  
  ![rflp](./img/01_Technics/rflp.png)
