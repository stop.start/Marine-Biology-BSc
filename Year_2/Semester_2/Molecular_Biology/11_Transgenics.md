# Transgenics

- An organism or cell of one species into which one or more genes of another species have been incorporated.
- Uses:
  - Study function of genes (either by insertion or deletion (knock out-KO))​.
  - Add/remove traits to organisms of commercial/health interest.​
- **Transient**: injection of mRNA or expression vector.​
- **Stable**: stable introduction into the genome.
​- Two strategies to create transgenic animals:
  - Injecting DNA into fertilized eggs.
    - 1982
    - Insert of rat growth hormone in mouse. The promotor of the gene (metallothionein) can be "open" in presence of metals (via diet) and close with non metal diet.
    - With the correct diet that includes metals and open the promotor, the mouse grew bigger.
    - When the DNA is inserted there is no control of where it's going to be integrated (if it is integrated) and how many copies are integrated.
    - The best founders are selected.
  - Introduction of manipulated embryonic stem cell (ES) to embryo.
    - More complex and precise than the first method.
    - Replacing a gene with another at the same locus.
    - An altered gene is synthesized and inserted in ES cells culture.
    - Test the colonies and use the ones with the altered gene.
    - Inject the ES cells into early embryo (blastula). This is called a **chimeric bastula**.
    - Some of the offsprings should have the wanted phenotype.
    
    ![es](./img/11_Transgenics/es.png)

- The insertion of foreign DNA into ES cells is called **transvection** and is done by **electric shock** which creates temporary holes in the cells membranes allowing the DNA to get in.
  - Part of the DNA that enters the cells will get out, some will be inserted at the wrong locus, thus the ES cells need to be filtered to select the correct modification.
  - The filterin is done by positive and negative selection.
  - Positive selection: insert a foreign DNA with the wanted gene along with another one called **$`neo^r`$ that protects against G418. The cells without the genes die from G418 while the one with $`neo^r`$ live.
  - Negative selection: cells with HSV-tk will die from ganciclovir.
  - The vector used contains the altered gene, close to it the $`neo^r`$ gene and around them two homologous regions. Outside the homologous regions is placed the tk gene.
  - The homologous regions match region around the gene to be replaced. This allows for recombination to happen.
  - When recombination happens, the tk gene is left out.
  - If the whole vector (including the tk gene) is inserted into the genome, it means that it has been inserted in the wrong place without the expected recombination.
  - The positive selection allows to filter out the cells which didn't incorporate the vector at all.
  - The negative selection allows to filter out the cells which inserted the vector at the wrong location.
  - It is very rare that what we want actually happen.
  
  ![positive/negative selection](./img/11_Transgenics/positive_negative_selection.png)
  
- **Cre-loxP system**: 
  - Used for site/sequence specific recombination in the **P1 phage**.​
  - **Cre recombinase** is a protein of 38kDa that mediates site specific recombination between **two loxP sites**.
  - When cre recombinase meets two loxP, it sets them one on top of the other and does recombination between them.
  - Those loxP sequences can be used to make recombinations. Depending on how they are placed there are different outcomes.
    - A (inversion): loxP face one another with a gene in the middle $`\rightarrow`$ inversion of the gene.
    - B (translocation): the two loxP are on different chromosomes $`\rightarrow`$ translocation happens.
    - C (deletion): The loxP are in the same direction: the gene in the middle is deleted (with one of the loxP).
  
  ![loxp](./img/11_Transgenics/loxp.png)
  
  ![cre-loxp](./img/11_Transgenics/cre_loxp.png)
