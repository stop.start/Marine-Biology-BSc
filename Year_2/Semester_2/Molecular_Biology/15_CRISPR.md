# CRISPR

## Intro

- Identified in 1987: unusual repeats with unknown funtion.
  - After 20 years it was identified to have viral origin and function as a defense mechanism.
- It found in both bacteria and archea.
- When a virus attacks a bacterium, the bacterium (if it has the ability), can cut a part of the genetic material injected by the virus and integrate it to its own DNA within the **CRISPR locus**.
- The CRISPR locus contains the viruses' DNA and in between them constant sequences.
- **Cas genes**, located upstream to the CRISPR locus, are a group of proteins that perform all the required tasks for this process: cutting the virus DNA, integrating the foreign DNA in the local DNA, etc.
- The CRISPR locus is constantly transcribed. The RNA produced is called **pre-crRNA** (pre-CRISPR RNA).
  - The RNA contains the constant sequences + the virus RNA.
  - It is then cut. Each part contain one viral DNA + constant sequence which allow the binding to a cas protein (cas9) that knows how to cut.
  - When a known virus attacks, the cas protein with the matching virus DNA will identify the virus and cuts its DNA.

![cas genes](./img/15_CRISPR/cas_genes.png)

- **tracrRNA** (trans-activating crRNA) binds to the constant sequences of the pre-crRNA and allows the pre-crRNA to be cut in the right places.
  
![tracrRNA](./img/15_CRISPR/tracrrna.png)

## Genome Editing

### Old methods

- **ZFNs**:
  - Binds nucelotide triplets.
  - Use zinc "fingers".
- **TALENS**: 
  - Binds single nucleotides.
  - Identify the desired sequence in the DNA with a created oligonucleotide.
  - Anything can be attached to the oligonucleotide (like restriction enzymes) which will perform the desired task on the desired sequence.

### CRISPR

![crispr prokaryote](./img/15_CRISPR/crispr_prokaryote.png)

- Today CRISPR is used.
- **Guide RNA**: crRNA + tracRNA.
- The complementary sequence designed and used (in bacteria this would be the viral DNA) is around 20bp.
- An oligonucleotide can be added to the RNA (crRNA + tracRNA) in the cas proteins.
- The cas protein identifies also 3 nucleotides calle **PAM** within the targeted DNA.
  - The PAM sequence limits designed the complementary sequence.
    - For example, if we want to modify a human chromosome, we need a sequence close to the PAM triplet.
- Cas9 cuts the two strands of DNA.
  - **NHEJ** (non homologous end joining): when that happens repair DNA starts. Sometimes the repair inserts an error.
  - **HDR**: cut and inserts at the same time new DNA.
- **Cas nickase**: 
  - Increases specificity (less off targets = doesn't cut at the wrong place).
  - By using 2 guide RNA: one target on each side of PAM and cutting for each target only one strand. 
  - One if the two nicks happen, the double strand is cut (meaning we got now two parts).

![nickase](./img/15_CRISPR/nickase.png)

- CRISPR can be used to target any enzyme to a specific address on the DNA​
  - Label​
  - Deamination (base editing)​
  - Methylation​
  - Use your imagination
  
