# Cancer

- The term is from Hippocrates 5th century B.C. It describes diseases with uncontrolled tissue growth.
- This happens when during differenciation cells that should lose their capacity to divide keep dividing thus losing the balance between cell division and cell death.
- **Neoplasm or tumor** is a mass of tissue proliferating in an uncontrooled manner.
  - **Benign tumors** are restricted to a local area​.
  - **Malignant tumors** can spread and invade other tissues​.
- **Carcinoma**: epithelial cells that lost balance control.
- **Sarcoma**: connective tissue that lost balance control.
- **Lymphoma and leukemia**: blood and lymphatic tissue that lost balance control.
- From experimenting on mice, injecting melanoma cells then removing lung metastases and injecting them in another mouse (and so on), results in the tumor cells improving their ability to metastasize. This shows that tumor cells evolve (they are **heterogeneous**).
- **Cancer cells are characterized by**:

![cancer characteristics](./img/14_Cancer/cancer_characteristics.png)

- Limitless replicative potential: regular cells can divide until a certain number of times. Cancer cells don't have the ability to "count" the number of division and undergo apoptosis.
- Sustained angiogenesis: cancer cells ignore external signals but do required blood flow and oxygen.

## Cancer Causes

- All cancers have a genetic background :​
  - Inherited​.
  - Accumulation of random mutations​.
  - Induced by the environment​.
- Exposure to UV radiations:
  - Cancers from UV radiations are mostly found in hotter areas.
  - Can cause **pyrimidine dimer** formation (thise can cause mutations).
  - In skin cancer is mostly found the mutation CC $`\rightarrow`$ TT.
  - In internal cancer is mostly found the mutation C $`\rightarrow`$ T (not dipyrimidine site).
- Smoking:
  - The more cigaretters per day, the younger the person started smoking, the higher the risk of developing cancer.
  - The longer after quitting smoking the less the risk.
- Ageing:
  - The older the higher the risk on breast and colon cancer.
- 2-Naphthylamine, is a compound that in itself isn't carcinogen but when ingested, it undergoes some modifications in the liver and then becomes carcinogen.
- **Ames test**:
  - Bacteria lacking histidine on a growth plate (they cannot grow without hisitidine).
  - Keep one control plate with only bacteria without histidine.
  - Add to the other plate the substance being tested and liver homogenate (a lot of substance are activated in the liver, like 2-Naphthylamine).
  - If bacteria grow it means that the substance is mutagenic (it mutated the histidine gene operon back to working).
  - [See here for more](Semester_1/Genetics/14_Gene_Mutation_DNA_Repair.md#the-ames-test)

![ames test](./img/14_Cancer/ames_test.png)

- Some viruses can cause cancer. Those are called **oncogenic viruses**.
  - The first discovered was the **RSV** (Rous Sarcoma Virus).
  - They can cause cancer by either:
    - Stimulating the proliferation of infected cells.
    - Infecting tissue causing mutations resulting in cancer.
- **Philadelphia chromosome**:
  - **Translocation** between chromosome 22 and chromosome 9 (part of 22 transfers to 9).
  - The translocation fuses 2 genes on chromosome 22: **ABL1** and **BCR**.
    - BCR is expressed a lot in B cells.
    - ABL1 is involved in cell division.
  - The ABL1 becomes controlled by the promotor of the BCR gene thus is expressed a lot more.
  - It is associated with **CML** (Chronic Myelogenous Leukemia).

![philadelphia chromosome](./img/14_Cancer/philadelphia.png)

## Genes involved and Mechanisms

- Multiple Hit Model: one mutation isn't enough to cause cancer since there are repair and control mechanisms.
  - This can explain why there is a higher risk for cancer whith ageing.
- Some genes are known to be associated with specific cancers:
  - Colorectal cancer (מעי) is associated with several genes including **APC** and **P53**.
- Most of the mutations causing cancer are directed towards three fundamental pathways:
  - Alteration in cell proliferation.
  - Alteration in DNA damage response.
  - Alteration in cell growth.
- **Oncogenes** are genes that code for proteins that **stimulate excessive cell proliferation** and/or promote cell survival by **inhibiting apoptosis**.
  - The first oncogene discovered was Src (from RSV).
  - The first human oncogene was **Ras**.
- **Isolating oncogenes**:
  - Fragemented DNA from human tumor cells in taken and put on a plate.
  - Those fragment are transfected into mouse 3T3 cells.
  - Some of the mouse cells will get human DNA that will cause them to divide and for colonies.
  - DNA from cells from those colonies are taken, fragmented and tranfected to new mouse cells (second cycle).
  - As before, some of the cells will create colonies.
  - DNA is extracted from those colonies and put into phages (phage library).
  - The DNA extracted is mouse DNA but some fragments have parts of human DNA.
  - In order to isolate the human DNA from the phage library, probes created from human repetitive DNA sequences are used.
  - The isolated human DNA is then transfected to mouse cells to test if it is in fact the cause of the cell growth.
  
![oncogenes](./img/14_Cancer/oncogenes.png)

- Oncogenes are regular genes that "lost control" (like Philadelphia chromosome).
  - It can be because of mutation, gene amplification, translocation, DNA rearrangement, insertional mutagenesis (virus).
- **Burkitt lymphoma**:
  - From translocation.
  - MYC gene is translocated from chromosome 8 to 14 under an active region.
  - c-myc disregulation​ (c-myc means cell myc vs v-myc viral myc).
  - Burkitt lymphoma can be associated with EBV infection.
- **Retinoblastoma** is the cancer of the retina​.
  - Rb identified in hereditary retinoblastoma families.
  - The gene responsible was found on chromosome 13.
  - The Rb gene controls cell cycle (progression from G1 to S)​.
  - It inhibits promotors until it undergoes phosphorylation, the cell can then enter the S phase.
  - Many mutations causing cancer are either on the Rb gene itself or its network.
- Carriers of retinoblastoma (heterozygotes) are not sick but are more susceptible to get sick since one chromosome is already mutated thus a single mutation on the gene of the other chromosome will cause cancer.
  - A mutation on the second gene is called **Loss of heterozygosity (LOH)**.
  - Non carriers need two mutations in order to get sick.
- Oncogenes of the **Human Papillomavirus (HPV)​**:
  - HPV is a familly of viruses involved in cancer development.
  - For example, it can disrupt the ability of Rb to inhibit the transition from G1 to S.
  - It can also send P53 to degradation.
- **APC** gene: its protein sits inside the cell and controls the Wnt receptor which inhibits cell division (by inhibiting β-catenin) until Wnt protein binds from outside.

### P53

- **P53**:
  - Controls Cell cycle​.
  - Hub for many control networks checking DNA damage​.
  - Important role in directing cells towards apoptosis​.
  - More than 50% of cancers have mutations in P53​.
- P53 is NOT an oncogene despite:
  - Bound to the large-T antigen of SV40​.
  - High levels of expression in many tumors.​
  - Transfection of cells with P53 can lead/assist in transformation​ (proliferation).
- **SV40**:
  - Virus that attack monkeys' cells.
  - Sequenced in 1978.

> __Fun fact:_  
> _When polio vaccine was first distributed, it was tainted by the SV40 virus which wasn't known then._  

- In cancer cells P53 is mutated.
- Familial clustering of cancer can be caused by inheritance of mutated P53.
- Transfection of sick cells with wild-type P53: the transformation is suppressed. P53 is a tumor suppressor.
