# Translation

## tRNA

- In eukaryotic cells, there are 3 RNA Polymerases.
- RNA Pol II is the one translating protein-coding genes (among other types of genes).
- RNA Pol III translates tRNA genes (among other types of genes).
- tRNAs are 75-90 bases long.
- Like mRNA the transcribed product isn't the finished product and undergoes modifications (nucleotides are modified into new nucleotides).
- Most modifications are in the loops of the tRNA.
- **All tRNAs have a nonencoded 3' CCA-OH end that is added**.
- RNAse P (RNP ribozyme) cleaves the 5' end of tRNA in all organisms.

![tRNA secondary structure](./img/10_Translation/secondary_structure_trna.png)

- In the anticodon the 3rd base is less strict (wobble).
- 61 codons encodes for amino acids and 3 stop codons.
- There are less than 61 different anticodons (in human: 48).

## aaRS

- Aminoacyl-tRNA synthetase.
- They attach amino acids to their tRNA by an aminoacyl bond.
- Each aars is specific for an amino acid.
- Attaching an amino acid to its tRNA is done in a **two steps reaction** by the aaRS:
  1. Adenylation of the amino acid by using ATP (AMP is added to the amino acid).
  2. The amino acid is attached to the 3' of the tRNA and the AMP is released.
  
![aars](./img/10_Translation/aars.png)
![aars](./img/10_Translation/aars2.png)

- Although all aaRS behaves more or less the same, there are one from one family of proteins but from **two different families of proteins**. They are called **Class I** and **Class II**.
- aaRS doesn't identify the tRNA by its anticodon but by other areas.
- The two classes attach to the tRNA from opposite sides.
- One class attaches the amino acid directly onto the carbon 3 of the tRNA site, the other first attaches the amino acid to the carbon 2 and then moves it to the 3 (the catalytic site workd differently).
- In case of error in the amino acid - tRNA matching, aaRS can edit and rectify the error.
- The error rate is ~1:40,000.
- In some bacteria and all archea Glutamine and Asparagine amino acids are synthesized directly on the tRNA (a precusor is attached to the tRNA and then the amino acid is synthesized).
- There's a 21st amino acid: **Selenocysteine (Sec)**. It has a similar structure as Serine and Cysteine (only one atom differ).

![selenocysteine](./img/10_Translation/selenocysteine.png)

- Proteins containing this amino acid are called **selenoproteins**.
- Selenocysteine is formed by:
  - aaRS attached Serine to tRNA.
  - Serine is then modified into Selenocysteine.
- Conditions of incorporation of Selenocysteine:
  - UGA stop codon followed by a specific secondary structure (without this secondary structure the stop codon is just a stop codon).
  - Presence of certain proteins.
- Most energy for the proteins synthesis comes from GTP and not ATP.

## Ribosomes

- Built from several rRNA parts.
- In prokaryotes there's rRNA 16S parallel to 18S rRNA in eukaryotes.
- Mitonchondria also have the 16S rRNA.

![ribosomes subunits](./img/10_Translation/ribosomes_subunits.png)

- In the subunit 16S rRNA (prokaryotic small subunit) there's a conserved sequence near the 3' end.
- The small subunit of the ribosome is the one that binds the mRNA and verifies correct codon-anticodon match.
- The large subunit catalyzes the peptide bond formation.
- **Translation initiation**:
  - In prokaryotes there can be more than one AUG in the mRNA (operons: several genes one after another in the same mRNA). 
    - Before each AUG, there's a **ribosome binding site (RBS)** which is the **Shine-Delgarno sequence** that binds to the conserved sequence in the small subunit (mentioned earlier).
  - In eukaryotes, there's only one start AUG in the mRNA. The small subunit of the ribosome attaches at the 5' end of the mRNA and scans it until it finds the **Kozak sequence** containing the AUG codon.

![translation initiation](./img/10_Translation/translation_initiation.png)

- After the mRNA leaves the nucleus, the CBC disassociates and is replaced by **eIF4 proteins**. Those proteins interact with the polyA tail forming a circular mRNA (eukaryotes).
- The translation always starts with Methionine amino acid. 
  - In prokaryotes, the first methionine is modified (fMet - N-formyl methionine) and has its own tRNA ($`tRNA^{fMet}`$). After the translation, the fMet is usually removed.
  - In eukaryotes, the Methionine isn't modified but still uses a special tRNA called $`tRNA_i^{met}`$.
- When the big subunit attaches to the small subunit, GTP bound to the first tRNA is hydrolized to GDP and leaves the site allowing the big subunit to sit.
- **Leaky scanning phenomena**: the small subunit doesn't start at the first AUG but at another one. Usually it is a wanted process in order to build a different protein.
- **EF-Tu** in prokaryotes and **EF1** in eukaryotes are proteins that protect the tRNA and its amino acid in the cytoplasm until it sits correctly in the A site of the ribosome.
  - Once the tRNA sits correctly there's a change in conformation and a GTP is hydrolyzed into GDP.
- **EF-G** in prokaryotes and **EF2** in eukaryotes are involved in the move of the tRNA from one site of the ribosome to the next one. This also hydrolyzes a GTP into GDP.

![elongation](./img/10_Translation/elongation.png)

- When stop codon is in the A site and no tRNA can enter the site, there's a release factor that enters instead causing water to enter hydrolyzing the P site and cutting the polypeptide chain from the ribosome. The ribosome then disassociates.
- The release factor is a protein but has similar shape than the tRNA.

## Other stuff

- **Polysome** is a mRNA with several ribosomes on it translating.
- A lot of drugs target ribosomes especially since the prokaryote and eukaryote ribosomes are structurally different.
- Part of the amino acids in the polypeptide chain are used as address to where to send the polypeptide.

![signals](./img/10_Translation/signals.png)

- Proteins that finish forming in the cytoplasm stays in the cytosplasm (including in organelles).
- Proteins that finish forming in the RER then Golgi either leave the cell or are destined to the membrane or go to lysosome.
- Differentation between the two types of proteins:
  - At the start of the translation some of the proteins will start with a signal (the first few amino acids).
  - An **SRP (signal recognition particle)**, which is a type of RNP, comes on the ribosome which pauses the translation.
  - The SRP brings the ribosome to the RER and fixes it on the membrane vie hydrolysis of GTP to GDP.
  - The signal sequence is then cut off.
  
  ![srp](./img/10_Translation/srp.png)

- M13 phage display should not be in the exam.
