# Molecular Biology

1. [Technics](./01_Technics.md)
2. [Libraries](./02_Libraries.md)
3. [PCR](./03_PCR.md)
4. [Sequencing](./04_Sequencing.md)
5. [DNA Synthesis](./05_DNA_Synthesis.md)
6. [Next Generation Sequencing](./06_Next_Generation.md)
7. [DNA Replication](./07_DNA_Replication.md)
8. [DNA Transcription](./08_DNA_Transcription.md)
9. [Ribozymes & Aptamers](./09_Ribozymes_Aptamers.md)
10. [Translation](./10_Translation.md)
11. [Transgenics](./11_Transgenics.md)
12. [DNA Chips](./12_DNA_Chips.md)
13. [Cell Cycle](./13_Cell_Cycle.md)
14. [Cancer](./14_Cancer.md)
15. [CRISPR](./15_CRISPR.md)
16. [Forensics](./16_Forensics.md)
