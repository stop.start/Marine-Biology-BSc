# DNA Chips

## Dot-Blot

- In order to determine the sex of an organism, a probe is used: the fluorescent reverse complement of the sex chromosome (for example Y chromosome is the one that sets the sex) and check if it underwent hybridization.
- Dot-Blot is the method to do perform the process.
  - It can work on several samples in parallel.
- It is not in use anymore.

## DNA Chips

- **HTS** (high throughput screening): smaller dot blotter with higher parallelism
- Genotyping: different probes are used with the same DNA allowing to determine multiple alleles, SNPs and HLA.
- RNA expression profiling: determines how much RNA there is by the strengh it attaches to the probe. The stronger the more RNA there is thus it can be determined that this gene is expressed a lot.
- Two types of chips:
  - Spotted array: printed chips
    - Can be bought for mainstream organisms.
    - On a microscope plate. 10,000s of chips can be printed on those.
  - Synthesized on site: DNA is synthesized on the chips (Affymetrix).
    - With time we can insert more and more chips per square.
    - **Photolithographic synthesis is used** in order to synthesize the DNA on the chips
      - A mask is used in order to reveal only part of the chips and add a specific nucleotide to them. This process is repeated for each nucleotide to be added.

| Printed Chips                              | Synthesized                                                     |
|--------------------------------------------|-----------------------------------------------------------------|
| ![printed](./img/12_DNA_Chips/printed.png) | ![synthesized](./img/12_DNA_Chips/synthesized.png)              |
|                                            | ![photolithographic](./img/12_DNA_Chips/photolithographic1.png) |
|                                            | ![photolithographic](./img/12_DNA_Chips/photolithographic2.png) |

- Chips have lighted dots so when the image of the chipd is used m a grid can be placed on it to know what is where.

### RNA Expression Profiling

- RNA is taken from cells.
- cDNA is produced from the RNA.
- cRNA is produced from the cDNA.
- Amplification is performed (PCR).
- The products are broken into fragments because smaller strands hybridize faster.

![RNA profiling](./img/12_DNA_Chips/rna_profiling.png)

### Comparative Hybridization Experiment

- DNA arrays are used.
- It is done to compare, for example, the gene expression caused by certain events.
  - RNA from the source which is tested + RNA from a control source.
  - RNA from each source is labeled with different fluorophores.
  - They are put on the array to undergo hybridization.
  - With a light filter, we can then look for the result of each RNA (how much of which RNA there is).

## RNA Editing

- Insertion, deletion or modification of bases post transcriptionally​
- Two Mechanisms:​
  - Site specific deamination (C or A)​.
    - **ADAR** (adenosine deaminase acting on RNA)​.
    - From C to U or from A to I (O instead of NH2).
  - Guide RNA directed insertion and deletion of uridine​
    - In mitochondria of trypanosomes​
    - Extensive insertion and deletion of uridine (up to 50% of mature mRNA)​
- This is done in:​
  - Plastids of plants (C to U)​
  - Animal cells​
- This is done to:
  - Change of coding sequence​
  - Anti viral defense
