# Next Generation Sequencing

- Today, there are several methods for suquencing. We'll go over two of them: **454** and **Illumina/Solexa**.
- All of the methods are based on Craig Venter's shotgun method.

## 454 Sequencing

### Pyrosequencing principle

- Developped by Pyrosequencing AB, a Swedish company
- It is intended for **microsequencing** (2-3 nucleotides chains).
- SNP genotyping (Single Nucleotide Polymorphism) is an example of how microsequencing can be used.
- **Pyrosequencing**:
  - Every cycle one nucleotide (dNTP) is added to DNA.
  - If the nucleotide attaches to the existing chain PPi is released (two phosphates).
  - Those phosphate will react with APS (adenosine 5' phosphosulfate) to produce ATP.
  - Phosphate in the ATP can react with **luciferin** (through an enzyme called luciferase) and produce **light**.
  
  ![pyrosequencing](./img/06_Next_Generation/pyrosequencing.png)
  
- Pyrosequencing can be used with an existing unsequenced strand to produce its complement:
  - If no light was emited then we wash the solution and try another nucleotide.
  - If there is light the nucleotide has been attached and we can deduce the nucleotide from the existing strand.
  - If there are several same nucleotides in a row (AAA, CC, and such) then the light emited will be stronger.

![pyrograph](./img/06_Next_Generation/pyrograph.png)

- Because dATP can react with luciferase and produce light before it attaches to the strand, **dATPαS** was used.

### 454 Sequencing using pyrosequencing

- DNA is randomly cut (around 2000bp per fragment).
- Linkers are added on both ends. The only fragments that will participate in the reaction have different primers on each end. The ones with the same primer on both ends won't participate.
- Small beads are prepared with small DNA sequences that can undergo hybridization with the primers put on the DNA fragments.
- The beads are put in excess compare to the DNA fragments so that each bead will get at most one DNA fragment.
- Then Taq polymerase is added along with dNTPs and other required reagents as well as a stirrer to emulsify the beads in a water-in-oil solution.
- This will give bubbles each with at most one bead along with all the reagents required for undergoing PCR.
- With PCR on the bead there are more and more DNA sequencing.
- Then the emulsion is broken.
- The beads are then put in a plate where each well has room for just one bead.
- It was approved in 2005.
- At first only 45 bases per well and later 500 bases per well. Only bacteria genome could be sequenced.
- In 2008 they announced they could sequenced the human genome.

## Illumina - main method today

- **Polonies**: PCR + colonies
- Slide with primers (of both types) radomly attached on it.
- DNA cut and with **adapters** is dispersed on the slide.
- With the right dilution the DNA fragments will be dispersed with enough room between them.
- Because of all the primers randomly attached, the DNA fragments will attached on both sides (it will bend).
- PCR is done.
- Temperature is increased to dettached the double strands.
- Because each DNA fragment start far enough from one another, when PCR is done, there are groups of duplicated DNA of the same fragments. Those groups are called polonies.
- The polonies washed to leave only the strands in one direction.

![illumina](./img/06_Next_Generation/illumina.png)

- Sequencing can be done on all the polonies at the same time since all the fragments ends are known (primers + adapters).
- Special nucleotides are used:
  - Each nucleotide is colored.
  - Carbon 3' is blocked which means that only one nucleotide can be attached (polymerized) at a time (unlike the "dideoxy" method, the oxygen is still there but blocked).
- Because each polonie contains the same fragments, the same nucleotide (that are colored) will attach each cycle. Each time the color emitted is recorded.
- Each cycle a reagent is added to deblock the oxygen. The color of the previous nucleotide is also removed.

## Nanopre - the nex generation

- Single cell sequencing allowing to compare between the genomic data of different cell of an individual and check for mutations..
- Sequencing of unculturable microorganisms.
- In order to sequence a single cell, first we do PCR in order to get a large amount of DNA.
  - The issue with this is that some fragments will undergo amplifaction better and some not as well.

### Phi 29 DNA polymerase

- Phage ϕ29 has a DNA polymerase which has the following properties:
  - 3' to 5' exonuclease $`\longrightarrow`$ can do proofreading by going backward.
  - If encounters a double stranded part, it will **displace** it (strand displacement).
  - **High fidelity** $`\longrightarrow`$ can polymerize long strands.
  - Low error rate

### MDA - Multiple Displacement Amplification

- Properties:
  - Isothermal reaction (30°C)
  - **Random hexamer** priming
  - High DNA yield
  - **Should** be indiscriminative
- Several primers are set on a circular DNA.
- DNA polymerize starts working.
- When it encounters a primer, it displaces it (including the strand that was polymerized from the primer).
- Now there are single strands with primers on it thus DNA polymerase will attach and start working on them.
- More primers are added and attach on the single strands, allowing the cycle to continue.
- See video.
- In order for the ϕ29 DNA polymerase to not proofread primers that did not attach well (which can destroy them because they are short - 6 nucleotides), the nucleotides in the primes include **thiophosphate** instead of regular phosphate. The sulfate on the phosphate will prevent the DNA polymerase to remove the primer.

### MALBAC - Multiple Annealing and Looping Based Amplification Cycles

- The amplification should be indiscriminative but some parts will undergo amplication more easily than others. A solution is to slow down the amplification of those parts.
- In order to slow down their amplification loops are made by:
  - Specific sequences are attached to the random haxemers.
  - At first single strands are sythesized with those specific sequences on one end.
  - Primers can attached on those strands, they will synthesize the reverse completement including of the specific sequence. This means that the single strands have on each ends specific sequences that are the complement of each other and thus can attach and create a loop.
  
  ![malbac](./img/06_Next_Generation/malbac.png)

- Although those loops will also happen to some of the harder fragment to amplify they will mainly slow down the ones that undergo the process more easily.

## Other stuff

- **CNV**: copy number variation
