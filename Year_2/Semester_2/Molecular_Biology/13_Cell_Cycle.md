# Cell Cycle

- 3 major checkpoints:
  - Before the S phase (after G0).
  - Between G2 and M phases.
  - During M phase (between metaphase and anaphase).
- Cell cycle is regulated by fluctuation of protein concentration and de/phosphorylation.
- The core control is based on **pairs of cyclins and CDKs (cyclin dependent kinases).
  - The cyclins regulate the activity of the CDKs.
  - If the CDK isn't paired with a cyclin then it is not active.
  - The concentration of CDK is stable while the concentration of cyclin fluctuates.
  - The higher the concentration of cyclins the higher the activity of the CDKs.

![cdk](./img/13_Cell_Cycle/cdk.png)

- There are different types of cyclins which fluctuates at different stages of the cell cycle.
  - S-cyclin concentration increases in the middle of G1 and decreases in the middle of M phase.
  - M-cyclin concentration increases durin G2 and decreases suddenly during M phase.

![cyclin](./img/13_Cell_Cycle/cyclin.png)

- Cyclin can regulate themselves and undergo degradation which inactivate the paired CDKs.
- Kinases/phosphatases can controll the activity of CDK and inhibit them even if a cyclin is paired with them.
- P27 can inhibit the whole cyclin-CDK complex.
- Rb protein stops inhibiting transcription​. It is disabled when phosphorylated through cdk-cyclin pair after Ras pathway.
- Damage to DNA activates P53​ protein by phosphorylation.
  - If there is too much phosphorylated P53 then the cell goes through apoptosis (too much damage to repair).
  - If there is enough but not too much P53 then the cell cycle stops to repair the damage. 
    - This initiates transcription of P21​
    - P21 binds and inhibits Cdk-complex​
- Prereplicative complex is loaded during G1​
  - S-Cdk activates the complex and loads DNA pol.​
  - S-Cdk marks Cdc6 for degradation to inhibit re-replication​.
- M-CDK maintain a positive feedback with its activator.

 ![positive feedback](./img/13_Cell_Cycle/positive_feedback.png)

![cdc20](./img/13_Cell_Cycle/cdc20.png)

![summary](./img/13_Cell_Cycle/summary.png)
