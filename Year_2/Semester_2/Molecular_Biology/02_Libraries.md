# Libraries

- With DNA the goal is to be able to search, take and use a specific fragments.
- Types of libraries:
  - **Genomic DNA library**: slicing DNA in order to be able to pull whatever needed sequence afterwards.
  - **cDNA library (expression library)**: from RNA that is converted to DNA (cDNA). RNA can also be expressed (proteins).
  - **Synthetic DNA library**: DNA that was synthesized in a lab.
  
## Genomic Library

- DNA can be sliced with **restriction enzymes** which gives very specific fragments.
- The DNA can also be randomly cut with acid or mechanical tools.
- The fragments are mixed with plasmids that were cut with the same ends (sticky, blunt).
- The plasmids are then placed within bacteria.
- In order for the hydrophilic DNA plasmid to go through the hydrophobic membrane of the bacteria, we need to make it more permeable. It was done with $`CaCl_2`$ but now it is done electrically (electroporation).
- Each bacteris holds a different plasmid.

![genomic libraries](./img/02_Libraries/genomic_lib.png)

- In order to take the bacteria with the wanted plasmid, bacterias are put on the petri dish and separated by **insulation seeding (?) -  זריעת בידוד** and create colonies for every plasmid.
- **Colony lifting**: a synthetic membrane is used to "stick" on the petri dish thus transfering a part of the bacteria on it. Those bacteria are then hybridized and expose to x ray, revealing the wanted colonies.

![colony liftin g](./img/02_Libraries/colony_lifting.png)

- Some issues:
  - Some bacteria don't get any plasmid.
  - Plasmids without DNA entered the bacteria.
  - DNA without plasmid entered the bacteria.
- Those types of bacteria need to be killed with antibiotics.
- In the plasmid, is put a gene that is resilient to the antibiotics so that the bacteria with plasmid won't be killed as well.
- **Blue-white selection**: 
  - White are with the plasmid.
  - Blue colonies are without the plasmid.
  - [See more here](https://gitlab.com/stop.start/Marine-Biology-BSc/-/blob/master/Year_2/Semester_1/Genetics/18_Recombinant_DNA_Technology.md)
  
## cDNA Library

- Take RNA from cells and build DNA from it.
- DNA is then put into plasmid like with genomic libraries.
- The enzyme **reverse transcriptase** is a DNA polymerase that uses an RNA template.
  - Requires primer (like other DNA polymerase).
  - The primer can be:
    - **Specific**: used for molecules with a specific primer.
    - **Poly T**: (complement is poly A). Used to differentiate general categories (for example tRNA and rRNA don't have poly A but mRNA does). Starts at the 3' end and works for shorter sequences because the DNA polymerase stops at some point.
    - **Random hexamer** (6 bases): the advantage is that there will be several primer along the RNA (chances are the hexamer is present more than once on the RNA) thus work with longer sequences.
- Poly T can be used in a colone to filter only mRNA.
- After the reverse transcription is done, the template is removed with **RNAseH** (digests RNA that is attached to DNA) or with a **base**.

![reverse transcriptase](./img/02_Libraries/reverse_transcriptase.png)

## Subtractive cloning

### mRNA

- Specific mRNA can be used to identify cells.
- In order to get those mRNA:
  - The mRNA of the cells that are being studies is transformed into cRNA (reverse complement)
  - Then those cRNA are mixed with mRNA from regular cells forming double stranded RNA.
  - The double stranded RNA are then removed leaving only the specific mRNA.
  
  ![subtractive cloning](./img/02_Libraries/subtractive_cloning.png)
  
### Mitochondrial DNA

- Mitochondrial DNA is used for a number of reason:
  - There is more than genomic DNA.
  - Genomic DNA can be damaged (when used with remains of our ancestors).
- In order to "fish" the mitochondrial DNA from the sample, modern (current humans) mitochondrial DNA is used to do hybridization.

### TCR (T Cell Receptor)

- Tak Mak by mistake
- Mark Davis
- Membrane bound
- Expressed in T cells but not in B cells.
- Undergoes genomic rearrangements.
