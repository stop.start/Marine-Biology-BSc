# DNA Synthesis

- Synthesis of DNA is done **from 3' end to 5' end**, the reverse of how it is done naturally!!
- The synthesis is done in a small column.
- The oligonucleotide is synthesized on a solid platform.
- CPG (Controlled Pore Glass) are glass beads with pores that allow for a big surface area with a few beads.

![cpg](./img/05_DNA_Synthesis/cpg.png)

- The CPG columns contains the first nucleotide, this means that if we want to synthesize an oligonucleotide that starts with T we need a column with this first nucleotide.
- The first nucleotide, or actually several first nucleotides are attached to the glass beads and they are called **DMT**.
  - The more nucleotides per bead the more oligonucleotides can be synthesized.
- **Phosphoramidites** are modified nucleosides (deoxiribose + nitrogenous base).
  - All chemically active sites are disabled.
  - The protecting groups can be removed each by its own reagent. This allows to control how molecules interact.

![DMT](./img/05_DNA_Synthesis/dmt.png)

- In order to add nucleotides to the chain:
  - Reagent that removes the 5' blocking molecule (DMT) is added.
  - Phosphoramidites are added (either A, T, G or C): nucleotides with all chemically active groups disabled except for the 3' end.
  - Because some of the nucleotides won't attached, after adding the second nucleotide, a blocking reagent is added that will never be removed.
  - The cycle continues.
- **Amonia** is used to cut the link between the oligonucleotides and the glass bead. Then it is dried to remove the amonia.
- The result of the synthesis can be cleaned to remove the ones that were not completed.
- **Random hexamers**: they can be sythesized by using in each cycles bottles with 1/4 A, 1/4 T, 1/4 G, 1/4 C. This will give all the possible combinations since in each cycle a quarter of the oligonucleotides will attach an A, a quarter will attach a G, etc.
- Usually synthesized oligonucleotides are around 60-70 bp. This is because every cycle some oligonucleotides are uncomplete which means each cycle there are less and less complete oligonucleotides.

## Synthetic Biology

- In order to synthesize a whole genome, fragments are synthesized and then attached together.
- The first genome to be synthesized was the mitochondrial DNA of mouse (Craig Venter).
