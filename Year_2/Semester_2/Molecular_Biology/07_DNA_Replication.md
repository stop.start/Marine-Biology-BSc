# DNA Replication

## DNA Polymerase

- Synthesizes (write) from 5' to 3'.
- Can only extend not from scratch (not de novo).
- Needs a template.
- The [Meselson-Stahl](../Semester_1/Genetics/10_DNA_Replication.md#replication-mode) experiment shows how DNA is synthesized (meaning double strands separate to be use as template). They used radioactive nitrogen.

![meselson-stahl](./img/07_DNA_Replication/meselson-stahl.png)

- Bacteria have one origin, eukaryotes have many.
- It takes _E. coli_ around 20min to duplicate although 40min to replicate under optimal conditions.
  - This is possible because it starts replicating again before it finished the first replication.
- In _E. coli_ replication starts at **OriC** and creates a **replication bubble** when starting to replicate.
  - **DnaA** binds to a 9 bases recognition site.
  - This causes DNA to bend and adjacent 13 bases sites rich in AT bases melt.
  - **DnaB** which is an helicase is placed on each replication fork.
  - Single stranded DNA is covered in **SSB** proteins (single strand binding) to prevent it from reannealing.
- **Helicases**:
  - Hexamer.
  - Unwind double strands of DNA or RNA.
  - Sits on the lagging strand (prokaryotes). In eukaryotes sits on the leading strand.
- **Topologigal problem**: when unwinding a double strand it coils the strands.
  - **Topoisomerases** take care of this issue by cuting and ligating the strands to release the pressure.
  - Topoisomerase I: no need for ATP and only one strand is cut and ligated.
  - Topoisomerase II: requires ATP and breaks and ligates both strands.
- **Gyrase** is a type of toposiomerase II.
- **SSBs** prevent annealing of single stranded DNA and protect DNA from nucleases. They are found in all organisms, and many act as tetramers.
- **Primase** is a RNA polymerase that synthesizes primers on the **lagging strand**. In _E. coli_ is called DnaG.
- Primases are different families in prokaryotes and eukaryotes.
- Okazaki fragments by using temperature sensitive bacteria.
  - By increasing the temperature, ligase was not active anymore.
  - The number of primers (fragments?) of the lagging strand increased since they couldn't be joined by ligase..
- **DNA Polymarase III** is the one synthesizing DNA in the replication fork.
  - It doesn't have 5'-3' exonuclease activity.
  - It is an **holoenzyme**.
- Both DNA polymerases on both strands advance in the same direction (one of the strand is bent).

![direction](./img/07_DNA_Replication/direction.png)

- DNA polymerases are mostly right hand shaped.
- In order to add a new nucleotide, the template strand is bent 90̣° (by the DNA polymerase) which reveal the template nucleotide completely.

![dna polymerase](./img/07_DNA_Replication/dna_polymerase.png)

- In the DNA polymerase, there a pocket where the nucleotide to be attach sits. This pocket is too small for ribonucleotides (their OH can't get in) thus only dNTPs can be added.

![pocket](./img/07_DNA_Replication/pocket.png)
 
- On each side of the 3 phosphates (negative) of the new nucleotide are positive molecules:
  - Metal ion with $`2^+`$ (usually **magnesium**) helps deprotonating 3' OH of primer.
    - **EDTA** can pull all the magnesium.
    - Calcium can be used instead of magnesium but it's bigger thus less effective.
  - Two amino acids (lysine and arginine)

![dna polymerase2](./img/07_DNA_Replication/dna_polymerase2.png)

- **Rolling circle replication**, used on small genome (a few thousands of bp, usually viruses).
  - From a circular DNA, a linear DNA is replicated constantly, then cut and the circle is closed (see [here](https://www.youtube.com/watch?v=z0PMLofObxk) and [here](https://www.youtube.com/watch?v=ZDqsojQ8A5k)).
- Telomerere/ase: [see here](../Semester_1/Genetics/10_DNA_Replication.md#end-of-replication)
- **Telomeres**: 
  - Organinsm specific repeat, in vertebrate TTAGGG.
  - 3' overhang
  - Vary in length.
- **Telomarese**:
  - RNP - Ribonucleic Protein (mix between protein and RNA).
  - Reverse transcriptase.
  - It contains the RNA template.
  - Synthesizes the template then jump to synthesize again at the end of what it just synthesized.
  - Extends the 3' overhang.
  - Lagging strand can then be synthesized.
  - A short 3' overhang will always remain.
  - **T-loop**: G-rich tails loop back on themselves to form a G-quartet (quadruplets?) called t-loop.
- Different types of cells have different levels of telomerase activity.
- **Senescence**: cell cycle stops due to telomeres being too short.
- Telomere length can be used to count cell divisions.
- **G-quadruplex**: 3D form of DNA in G-rich areas.
- **I-motif**: 3F form of DNA in C-rich areas.
