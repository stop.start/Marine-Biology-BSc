# Variability of the Oceans

- Oceans get energy mostly from Sun and wind (heat and waves).
- Major windows of energy input:
  - From atmospheric cyclones and anticyclones (100km-1000km)
  - Coriolis and tides (1km-100km)
  - Wind driven waves (1m-100m)
- Dissipation of energy appears mainly as turbulent flows.
- The transfer of energy occurs mainly from eddies of large scale to those of smaller scale.
- All ocean processes are linked and coherent:

![oceans processes](./img/13_Variability_of_Oceans/processes.png)

- **Small scale phenomena** created mainly by wind and have a time scale of 1-10sec to 10-100min and horizontal space scale of 1-100m to 1-1000m:
  - Wind waves
  - Internal gravity waves
  - Mixing processes
- **Short time mesoscale phenomena** created mainly by tide generation forces, Coriolis, local weather and diurnal fluctuation of insolation.
  - Time scale of 1 hour to 1 day and space scale from 10km to the basin scale.
  - Tidal oscillation
  - Inertial currents
  - Diurnal mixed layer fluctuation
- **Synoptic variability or mesoscale phenomena** created by cumulative effect of atmospheric disturbance (wind, thermal changes), Coriolis and hydrodynamic instability.
  - Time scale 3-5 days to 1-6 months and space scale from 10km to 100km
  - Weather mixed layer fluctuations
  - Geostrophic Eddies
  - Current stream meandering
  - Synoptic sea level changes
  - Oceanic long waves (Kelvin waves, Rossby waves): 4-6 weeks.
- **Seasonal variations** created by the seasonal variation of the ocean heat budget.
  - Time scale of 12 months and space scale is the basin scale.
  - Seasonal mixed layer fluctuations.
  - Seasonal fluctuation of sea level connected with sea water density changes.
- **Interannual variability** created by cumulative effect of atmospheric ocean interaction, disturbance and fluctuation of the atmosphere transparency due to volcanic activity.
  - Time scale 2-10 years and space scale is the ocean scale.
  - Mediterranean deep water transient
  - North Atlantic Oscillation (this can cause the transient)
    - Icelandic low and Azores high send wind to Europe. Depending on their size/strengh it will be more or less wind:
      - **Positive phase**: if Azores high increases then Icelandic low decreases.
      - **Negative phase**: if Azores high decreases then Icelandic low increases.
    - See below pictures
  - South Oscillation (el nino)

![north atlantic oscillation](./img/13_Variability_of_Oceans/nao.png)

- **El Nino** is an **interannual fluctuations** in the ocean-atmosphere interaction in the tropical Pacific and Indian Ocean.
  - Time scale: 2-7 years
  - Space scale: the whole Pacific
  - Causes warm surface water off the coast of Ecuador and Peru from around Christmas.
  - In this region the water temperature is lower than the rest of the latitude (see left picture below).
  - There is also less rain than in other regions at this latitude where the rain can reach 3m/year (see right picture below).
  - This is due to the trade winds pushing the warm water towards Australia causing an upwelling around Peru.
  - Sometimes the trade winds get weaker cause the water to get warmer closer to Peru. This causes a decrease in nutrient in the area thus less fish. Also more rain because of the warm water and drought in Australia.
  
![el nino](./img/13_Variability_of_Oceans/el_nino.png)

- Because of the topography, the ITCZ is not a straight line on the equator.
  - In the Pacific (most of it), the ITCZ is north of the equator in summer as well as in winter.
  - Between the North and South equatorial currents: the equatorial counter current which is the release of the set up from the N&S equatorial currents.
  
![equatorial currents](./img/13_Variability_of_Oceans/equatorial_currents.png)

- Although Coriolis doesn't affect the equatorial zone, its effect is strong even for low latitudes.
- Since the ITCZ and trade winds are more north than in theory, part of the south trade winds cross the equator:
  - The part that do not cross is diverted to the left and the part that do cross is diverted to the right (Coriolis).
  - From the Ekman transport theory: the water will go north in the northen hemisphere while in the southern hemisphere it will go south.
  - This causes an upwelling at the equator.
- The area between the N. equatorial current and the equatorial counter current zone is a divergent zone since they go in opposite direction and Coriolis/Ekman diverges them to their right.

![doldrum](./img/13_Variability_of_Oceans/doldrum.png)
![doldrum](./img/13_Variability_of_Oceans/doldrum2.png)

- **EUC (Equatorial Undercurrent)** or Cromwell current: 
  - Because of the trade winds, there a set up at the west coast thus high pressure gradient.
  - Water at the surface "want" to go back to equalize the gradient but the winds are too strong.
  - An undercurrent forms (there's no wind under the surface so it is easier for this current to form) and since it is located at the equator there's no Coriolis effect.
  - It happens below the thermocline which is where the wind stops affect the UML (difference in density).
  
  ![euc](./img/13_Variability_of_Oceans/euc.png)
  
- Because the winds are weaker, the upwelling stops thus the thermocline lowers (in the east).
- After el nino, la-nina can happen.
