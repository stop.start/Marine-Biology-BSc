# Sea Waves

## Definition

| Wave Type             | Description                                                                                                     |
|-----------------------|-----------------------------------------------------------------------------------------------------------------|
| **Capillary**         | A few cm without energy. Wind can create them. Surface tension is the force that pulls the wave back downward.  |
| **Chop** (גלי רוח)    | Created by wind.                                                                                                |
| **Swell** (גלי גיבוע) | Formed by inertia (after wind stopped)                                                                                  |
| **Seiche**            | Created by wind.                                                                                                |
| **Tsunami**           | Created by earthquakes. In open sea they are not high and get higher close to the shore. They last a long time. |
| **Tide**              | Created by wind. Last the longest.                                                                              |


![wave types](./img/11_Sea_Waves/waves_type.png)

- Waves characteristics:
  - Height (H): difference between the lowest and highest points.
  - Wavelength (L): distance between two successive crests (highest point) or trough (lowest point).
  - Steepness (תלחלות): the ratio of wave height to wavelength ($`\delta = H/L`$).
  - Period (T): time for one full wave to pass a fixed point.
  - Phase velocity (c): wave propagation speed (c = L/T).
  - Frequency (f): number of wavelengths that pass a fixed point per unit time (1/T thus c = L*f)
  
![wave characteristics](./img/11_Sea_Waves/characteristics.png)

- Waves transport only **energy** and not material.
- The waves advance horizontally because of the hydrostatic pressure from the change in surface height.

![animation](./img/11_Sea_Waves/animation.gif)


## Linear wave theory for small amplitude waves (H/L < 20)

- Assumptions:
  - Horizontal seabed
  - No friction
  - Only external force working is gravity
- Also:
  - Coriolis force is negligible (Rossby number > 1)
  - The wave propagates faster than water particles.
  - Constant density
  - Vertical acceleration and gravitational acceleration cancel one antoher.

| **Progressive waves**                                                            | **Standing waves**                                                                  |
|----------------------------------------------------------------------------------|-------------------------------------------------------------------------------------|
| the wave forms can be observed to travel (but not the water itself)              | Sum of two progressive waves of equal dimension but traveling in opposite direction |
| Water particles move in circle. Big circle at the surface and smaller with depth |                                                                                     |
| ![progressive waves](./img/11_Sea_Waves/progressive_wave.gif)                    | ![progressive waves](./img/11_Sea_Waves/standing.gif)                            |


![circles](./img/11_Sea_Waves/water_particles.gif)

- **Dispersion equation**: the phase velocity C depends on the wavelength and the depth in the direction the wave advances.
  - When the sea is a lot deeper than the wavelength, then its speed depends only on its length.
  - When the wavelength is a lot longer than the sea depth, then its speed depends only on the sea depth.
  - d: sea depth
  - L: wavelength
  - tanh: hyperbolic tangent
  
```math
\begin{aligned}
C=\sqrt{\frac{g L}{2 \pi} \tanh \left(\frac{2 \pi d}{L}\right)}
\end{aligned}
\text{} \\
\Downarrow \\
\begin{aligned}
&d / L \rightarrow \text { infinity } \tanh \rightarrow 1 ; \text { deep water waves } \quad C=\sqrt{\frac{g L}{2 \pi}}\\
&d / L \rightarrow 0 \quad \tanh \rightarrow 2 \pi d / L ; \text { shallow water waves } C=\sqrt{g d}
\end{aligned}
```

- Classification by water depth vs wavelength (d/L):

| Classification      | d/L               | or                                     |
|---------------------|-------------------|----------------------------------------|
| Deep water waves    | 1/2 to $`\infin`$ | d > L/2 $`C=\sqrt{\frac{g L}{2 \pi}}`$ |
| Transitional waves  | 1/20 to $`1/2`$   | L/2 > d > L/20                         |
| Shallow water waves | 0 to 1/20         | L/20 > d $`C=\sqrt{g d}`$              |

![shallow deep](./img/11_Sea_Waves/shallow_deep.png)

### Deep water waves

- Short wave (in deep water) - d/L>1/2: their velocity is not dependant on bottom depth.
- In deep water the particles move in circular paths with speed $`c_{part}=\pi H/T`$ which is less than the phase velocity c=L/T: $`\bold{c_{part} < c}`$
- Waves with larger periods propagate faster and form swell outside of the storm region.
- Sea waves and swell are different: sea waves are short period waves being created by wind while swell are regular waves with a long period created after the wind stopped.
- In Israel swell form.
- Two waves interacting with each other can produce either a larger or smaller wave.
  - Waves arrive from different part of the sea. Sometimes they interact with one another.
  - When surfers are waiting for the bigger waves, they are waiting for the positive interaction.

![interaction](./img/11_Sea_Waves/interaction.png)
![interaction](./img/11_Sea_Waves/interaction2.gif)

- **Mixed interference**: when swell waves come from a lot of different directions creating unorganized waves.
- **Maximal waves**:
  - In mixed interference zones, $`\bold{H_{1/3}}`$ is the average of the top third highest waves in the series. This is used for sailors since the dangerous waves are the highest.
  - In typical conditions: $`\bold{H_{max}} = 1.86 \cdot H_{1/3}`$.
  - **Giant waves**: rare 20-30m waves from constructive interference.
  - Steepness of a wave is H/L and the **theoretical limit** for it is about **H/L = 1/7**.

![mixed interference](./img/11_Sea_Waves/mixed_interference.png)

### Transitional waves

- Friction with the sea floor starts affecting the waves.
- At the surface, the particles still move in circles. With depth they move in an elliptical movement and close to the bottom they move horizontally (back and forth).
- With depth their movement is smaller.

![transitional wavess](./img/11_Sea_Waves/transitional_waves.png)

### Shallow waves

- Affected strongly by sea floor (the less shallow the faster are the waves).

| L[m] | c for d=50m | c for d=5m | c for d=3m |
|------|-------------|------------|------------|
| 100  | L/2 [m/s]   | L/20 [m/s] | [m/s]      |
|      | 12.5        | 7.0        | 5.4        |

![shallow waves](./img/11_Sea_Waves/shallow_waves.png)

- When close to the shore the waves take the shape of the shore since the closer to the shore the more shallow the water thus the slower the wave will advance.

![shore waves](./img/11_Sea_Waves/shore_waves.png)

- The closer the wave gets to the shore (linear doesn't apply because the seabed forms a slope):
  - Same period
  - Speed drecreases
  - Wavelength shorten
  - Height increases
  - Shorter wavelength + bigger height = steepness increased $`\implies`$ the wave breaks.

![wave transformation](./img/11_Sea_Waves/wave_transformation.png)

- **Wave refraction**:
  - Differetial speed along the crest (top of the wave).
  - Close to the shore the bottom is irregular in depth (takes the shape of the shore).
  - Causes the waves to be parallel to the shore.
    
![wave refraction](./img/11_Sea_Waves/wave_refraction.png)

- **Waves collapse (break)** when the steepness as an H/L ratio higher than 1/7 ($`\bold{H/L \geq 1/7}`$).

> **Types of breaking waves**  
> _not for the exam_  
> **Spilling breaker**:  
> Top of the wave crest 'spills over' the wave.  
> The energy is released gradually across the entire surf zone.
> **Pluging breaker**:  
> The crest 'curls over' the front of the wave.  
> The energy dissipates quickly.  
> Common at shorelines with steep slopes.  
> **Surging breaker**:  
> Never breaks as it never attains critical steepness.  
> Common along upwardly sloping shore faces or seawalls.  

![wave types](./img/11_Sea_Waves/wave_types.png)


## Non linear theory

- **Stokes drift**: particles don't close the circle and advance with the movement.
  - When the waves are too steep or water too shallow.
  - Can be observed in swell (no wind).

![stokes drift](./img/11_Sea_Waves/moving_water.gif)

- Because of the Stokes drifts and winds water builds a slope near the shore.
  - The water will need to get back to the open sea (to equalize the pressure gradient).
  - This will happen in an area of the shore that is deeper (decrease in the topography).
  - This is called **rip current**.
  
![rip current](./img/11_Sea_Waves/rip_current.png)
![rip current](./img/11_Sea_Waves/rip_current_animation.gif)

- Increasing energy from wind increases wave height, length and speed.​
- **Capillary waves**: the smallest waves formed at the lowest wind speeds. The principal restoring force is surface tension (wavelength less than 1.7 cm).​
- **Gravity waves**: the next stage of waves formed by increasing wind speeds. Named for their principal restoring force which is the Earth gravitational force​.
  - From 0.3-1m/s capillary waves start to become gravity waves.
  - When the wind ($`W_{10}`$) is faster than phase velocity c, it pushes (transfers energy) on one front of the wave: high pressure, the other side has a low pressure.
  
![gravity wave](./img/11_Sea_Waves/gravity_waves.png)

- When waves are already formed and the wind is now slower than the phase velocity ($`W_{10} < c`$), there is still a transfer of energy because the water particles are slower than the phase velocity and wind ($`C_{part} < C`$).
  - The wind doesn't push the wave but the transfer is done on the **crest** of the wave.
- Bernoulli's principal also applies since the wind takes a longer path above the crest than above the toughs:
  - Air pressure lower above the crest than tough.
- Wave energy is lost from the wave breaking when the waves become too steep thus unstable (H/L>1:7): white-capping (ברבורים).
- Some energy is transfered to surface currents, sound and turbulence.
- Wave energy is increased by:
  - Wind speed
  - Duration
  - Fetch (distance from the shore over which the wind blows in a single direction - the closer to the shore the smaller the waves).
- **Fully developped sea**: when the waves have the maximum possible height for a given wind speed and fetch.
- The duration increases the waves up to the limit for given wind and fetch.
- A wave field can be expressed as the sum of a number of **harmonic waves**.
  - $`\zeta=\sum_{i=1}^{\infty} a_{i} \cos \left(\omega_{i} t+\varepsilon_{i}\right)`$

![harmonic waves](./img/11_Sea_Waves/harmonic_waves.png)

- ppt11/ frequency - direction spectrum (around 59) -- not clear.
- Numerical waves models: calculation of sea surface fluctuation with resolution less than the wave length​.
  - **WAM** - 10m above sea surface wind. Daily forcast.
  - **SWAN** (Simulating WAves Near Shore) - spectra model
  
- **Storm surge**: rise in sea level resulting from low atmospheric pressure (storm) and the accumulation of water driven shoreward by storm winds.​
  - Need a shallow shore (<200m).
  - Storm causes the water to be pushed toward the shore: the level of the water increases.
  - If tides are also a parameter it can be dangerous (increasing even more the sea level).
  - If there's also a river it is really dangerous.
- **Tsunami** due to earthquakes or volcanic eruptions.
  - Really long waves than can have a low height.
  - When the wave gets to the shore, it breaks (transfer of mass).
  - d/L<1/20.
  - d usually 4.5km
  - Very fast waves.
  - No Coriolis (Rossby > 1.5)
  - Tsunami detection exists
