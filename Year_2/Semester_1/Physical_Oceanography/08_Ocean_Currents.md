# Ocean Currents

- **Tangential speed**: higher the bigger the circle.
  - R = radius (Earth's is 6400km)
  - $`\Omega = \frac{2 \pi}{24 \times 3600} = 0.729\times10^{-4}[rad/s]`$
  
```math
V_{tang} = \Omega \times R
```
- The tangential speed drecreases from the Equator to the poles since radius is short.

## Coriolis Effect

- The equator rotates faster than the poles.
- When throwing an object from the equator north, the object starts with a higher velocity than the direction it goes to, giving the illusion of going to the right.
- When throwing an object from the north pole to the equator, the object starts with a lower velocity than the direction it goes to, giving the illusion of going to the left.
- When throwing an object from the equator south, the object starts with a higher velocity than the direction it goes to, giving the illusion of going to the left.
- When throwing an object from the south pole to the equator, the object starts with a lower velocity than the direction it goes to, giving the illusion of going to the right.

![coriolis](./img/08_Ocean_Currents/coriolis.png)

- Coriolis acceleration:
  - $`\phi`$: latitude
  - $`\Omega`$: angular speed of Earth
  - V: velocity
  - $`f = 2\Omega sin \phi`$ is called **Coriolis Parameter**

```math
a_c = 2\Omega (sin \phi)V
```

- Coriolis isn't always taken into account:
  - One revolution $`\bold{2\pi / \Omega}`$
  - Time scale **T** of motion relative to $`\Omega`$ (if it's too fast Coriolis doesn't have the time to affect much).
    - Coriolis considered if $`\bold{T \approx > 2\pi \Omega}`$
  - The Coriolis effect is taken into consideration if an object has a motion of **length scale L** and **speed U** and the **Time T = L/U** to travel the distance L at speed U is **longer than one revolution**.
  - This is calculated with the **Rossby number**:
  
```math
\boxed{R = \frac{2\pi U}{\Omega L} < \approx 1} 
```

## Wind and Current Directions

- Wind direction: where it comes from:
  - $`0 \degree`$: notherly (from the north)
  - $`90 \degree`$: easterly
  - $`180 \degree`$: southernly
  - $`270 \degree`$: westerly
- Current direction: where it goes to:
  - $`0 \degree`$: notherward
  - $`90 \degree`$: eastward
  - $`180 \degree`$: southward
  - $`270 \degree`$: westward

### Wind Stress

- Kinetic energy of the wind is transfered to the surface layer of the ocean.
  - Part of it is expended in **gravity waves** and part in **driving currents**.
- **Wind stress** $`\bold{\tau}`$: **frictional force** per unit area acting on the sea surface from the wind.
- Current is about 3% of the wind speed.

- [Global Circulation](https://gitlab.com/stop.start/Marine-Biology-BSc/-/blob/master/Year_1/Semester_1/Climate_Theory/11_Global_Circulation.md#global-circulation)
  - Trades winds (or easterlies) created by the air coming down the Hadley cells at 30$`\degree`$. Because of the Coriolis effect the winds are deviated in the direction of the ITCZ.
  - At the equator itself the trades winds are weak since it is the "low pressure" area where the air rises.
  - Horse latitude at 30$`\degree`$ between the Hadley and Ferrel cells is the "high pressure" area with weak wind gradients thus not much wind.
  - 

![global circulation](./img/08_Ocean_Currents/global_circulation.png)

- Trade winds generate warm North and South Equatorial currents.
  - Because of the Coriolis effect those currents are brought to 60$`\degree`$N and S.
  - Those currents are warm thus causing the rising of air (between Ferrel and Polar cells).
  - This in turn causes the westerlies winds (weaker than the trades) also resulting in currents.
  - In the sourthern hemisphere, because of the lack of lands, the westerlies can reach hurricane intensity and generate **westward wind drift current** or **antartic circumpolar current** (anticyclonic current around the Antartic).
  - In the Artic, because of all the land the winds don't develop much.
  
|                                                               |                                                                   |
|---------------------------------------------------------------|-------------------------------------------------------------------|
| ![trade currents](./img/08_Ocean_Currents/trade_currents.png) | ![antartic current](./img/08_Ocean_Currents/antartic_current.png) |


### General Currents

![general circular currents](./img/08_Ocean_Currents/general_circular_currents.png)

- Five major subtropical oceanic gyres:

![oceanic gyres](./img/08_Ocean_Currents/oceanic_gyres.png)

- Intensified western boundary currents:
  - **Gulf stream** in the north atlantic gyre (very strong and affect the earth).
  - **Kuroshio current** in the north pacific (very strong and affect the earth).
  - **Brazil current** in the south atlantic
  - **East australian current** in the south pacific
  - **Agulhas current** indian ocean
- The Gulf stream is part of a gyre that is created by the trade winds in the south and westerlies in the north causing it to be **anticyclonic**.
  - Starts around 36$`\degree`$N.
  - It can reach velocity near 2m/s.
  - Because of Coriolis part of it goes to the right creating weaker meander rings.
- The Gulf Stream is stronger than the other parts of the gyre because:
  - There's a slope (sea level in the south higher than in the north). That's because of the winds: trade wind bring water in this part and westerlies drag it out.
  - The Coriolis effect intensifies the speed at the turn close to Florida.

![gulf stream](./img/08_Ocean_Currents/gulf_stream.png)

- The Gulf stream creates meanders on its path.
  - Some of the meanders dettach creating rings that live a few weeks (1-2 months).
  - Rings north of the stream are anticyclonic with at its center colder water.
  - Rings south of the stream are cyclonic with at its center warmer water. 
  
![gulf stream rings](./img/08_Ocean_Currents/gulf_stream_rings.png)

- Drifting buoys were used to map the Gulf stream and its meanders.
