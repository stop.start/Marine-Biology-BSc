# Heat, Water and Salt Budget

- Most of the energy is from the Sun (isolation).
- The energy is then scattered by currents.
- The atmosphere and ocean affect one another:
  - Sea surface wind waves.​
  - Turbulence (chaotic motions) in the sea upper layer and inner layers.​
  - Sea surface wind currents.​
  - General ocean currents.​
  - Heating and Cooling of upper layers of oceans.​
  - Long-term weather fluctuations​
  - Determination of Climate​
- Air and sea exchange:
  - Heat
  - Moisture
  - Mechanic energy
  - Gas and salt
  
## Heat and Moisture exchange

- Heat and moisture affect the water density and generate motion.
- Radiations from the Sun: **electromagnetic energy**.
- **Wein's law**: the maximal energy is radiated at a wavelength of $`\lambda_{\mu m} = 2897/T_{abs}`$
  - $`T_{abs}`$: absolute temperature of a body.
- The range of radiations is important for natural processes: $`\bold{80\mu m - 0.5\mu m}`$.

![radiations range](./img/03_Heat_Water_Salt/radiations_range.png)

- Surface temperature of the Sun: $`6000 \degree K`$.
- The maximum of the Sun's energy (according to Wiens's law) is at $`0.5\mu m`$ wavelength.
- Most of the Sun's energy is short wave radiation and half is visible.
- Earth's temperature is $`17 \degree C = 290 \degree K`$ which gives maximum energy at $`10 \mu m`$ wavelength.
- Earth's energy also affects natural processes.
- The atmosphere is transparent for short-wave radiations but the longer the radation the less transparent the atmosphere is for it (I didn't understand the explaination but this it the conclusion).
- **Heat flux (שטף חום)** is the transfer of heat accross a surface in unit of time.
- On Earth to keep the temperature stable the heat that leaves and the heat that enters need the be equal (more or less).
- The sum of the heat fluxes into and out of a volume of water is the **heat budget**.​
- The major terms of the budget for a water column (from surface to bottom) with unit sea surface area (1 square meter) are:​
  - Insolation $`Q_{SW}`$ (short wave radiation), the flux of sun energy into the sea.​
  - Net Infrared Radiation $`Q_{LW}`$ (long wave radiation), net flux of infrared radiation from the sea.​
  - Sensible Heat Flux $`Q_S`$, the flux of heat out of the sea due to conduction (הולכה).​
  - Latent Heat Flux $`Q_{LE}`$, the flux of heat carried by evaporated or condensated water.​
  - Advection $`Q_V`$: warmth moves through currents.
  
### Heat Budget

![heat exchange](./img/03_Heat_Water_Salt/heat_exchange.png)

- Short-wave radations from the Sun $`Q_{SW}`$ (looking at the sea):
  - Part are returned from the clouds.
  - Part is absorbed by the sea.
  - Part is reflected.
- The ocean sends to the atmosphere long wave radations $`Q_{LW}`$: the atmosphere isn't transparent to them and return them to the surface.
- Evaporation $`Q_{LE}`$: cools the ocean (gives warmth to the atmosphere) 
- Heat conduction $`Q_S`$: since the ocean is warmer than the atmosphere (from the Sun's radiations), the warmth goes from the ocean to the atmosphere (from contact: heat $`\rightarrow`$ cold).
- Advection $`Q_V`$: warmth moves through currents.

<br/>

- **Heat budget**:
    - Where Q is the rate or heat gain or loss
  - Units watts per $`m^{2}`$: $`J/m^{2s} = W/m^{-2}`$

```math
Q = Q_{SW} + Q_{LW} + Q_S + Q_{LE} + Q_V
```

- Multiplying Q by a unit of time (for example a day) then we get the amount of energy that went through the surface in that time: $`\Delta E = Q \times time`$
- Dividing by the specific heat and mass we get the change in temperature for that unit of time: $`\Delta T = \Delta T = \Delta E/(C_p m)`$
  - m: mass of the water column
  - $`C_p`$: specific heat (חום סגולי) which is around $`4000 J\cdot kg^{-1} \cdot \degree C^{-1} \quad \rightarrow`$ 4000 joules of energy are required to heat 1 kg of sea water by 1$`\degree C`$.
- Exchange of heat between oceans and the atmosphere:
  - Up to 100m depth
  - Density of water = $`1000kg/m^3`$
  - Mass in contact with the atmosphere: **density x volume = 100 x 1000 = 100,000kg**
- Exchange of heat between land and the atmosphere:
  - Up to 1m depth
  - Density of rock= $`3000kg/m^3`$
  - Mass in contact with the atmosphere: **density x volume = 1 x 3000 = 3000kg**
- Because the specific heat of water is much higher than of rock, oceans store a lot more energy than rock.
- **Net isolation**: seasonal change due to the inclination of the Earth.
  - **Shortwaves radiations (insolation)**:
    - Heats the oceans.
    - Always > 0 since it's the energy the earth receives from the sun.
    - $`30 Wm^{-2} < Q_{SW} < 260Wm^{-2}`$
    - At night: $`Q_{SW} = 0`$
- **Net infrared flux**: infrared (long wave radations). From the infrared send from sea/land to the atmosphere part is returned back to the sea/land (and a small part escaped to space) $`\rightarrow`$ net flux.
  - **Longwaves radiations (infrared)**:
    - Cools the oceans.
    - Oceans always radiate more than they receive from the atmosphere thus the net infrared flux is always from the sea to atmosphere.
    - Because the oceans and atmosphere are around the same temperature, the atmosphere returns more or less the same amount of radiations that the oceans sent, thus the net flux is close to 0 (but less).
    - $`-60 Wm^{-2} < Q_{LW} < -30Wm^{-2}`$
- The net flux at the equator and at the poles is close:
  - The water sends more radiations at the equator than the poles (since it received more radations).
  - The atmosphere returns more radations at the equator than at the poles (since the equator sent more).
  - In all the difference between what the sea sends to the atmosphere and what the atmosphere returned is the same.
- **Latent heat flux**: energy taken from the ocean to evaporate part of it into the atmosphere: 
  - **rate of loss of heat = latent of evaporation = rate of evaporation**
  - Wind speed and relative humidity are what affect the evaporation rate (not the temperature!).
  - In the ITCZ: no wind and high humidity thus no evaporation. Because ITCZ moves with the seasons the rate of evaporation as well.
  - $`-130 Wm^{-2} < Q_{LE} < -10Wm^{-2}`$
- **Sensible heat flux**: warmer water in contact with colder atmosphere.
  - Energy from water to atmosphere. It is very rare that the opposite happens but it happens.
  - No seasonal change but there is changes with the Gulf stream.
  - The Gulf stream happens due to the hot water current at the equator from Africa to America which then goes upward along the coast causing high temperature difference between water and atmosphere.
  - $`-42 Wm^{-2} < Q_{S} < -2Wm^{-2}`$
- **Zonal averages**:
  - Averages per latitude
  - $`Q_{SW}`$: there are more shortwave radiations around equator than around the poles.
  - At the equator the values of $`Q_{SW}`$ are a bit lower than at the latitudes around it. This is because of the ITCZ (more clouds = more albedo).
  - Minimum evaporation at the ITCZ because of the humidity and lack of winds thus less loss of energy from evaporation ($`Q_{LE}`$).
  - In the graph below, the graph of total heat flux isn't accurate since it should reflect that the sea loses heat and not gain.
    - This is because there is not a lot of data from the poles.

![zonal averages](./img/03_Heat_Water_Salt/zonal_averages.png)

### Water Budget

- Water budget for a **partially enclosed** sea:
  - $`V_{in}`$: volume that gets in.
  - $`V_{out}`$: volume that gets out.
  - $`V_{earth}`$: volume that gets in from runoffs.
  - P: precipitations (rain)
  - E: evaporation

```math
\boxed{\Delta V = V_{in} + V_{out} + V_{earth} + P + E}
```

- **World ocean budget**:
  - Average evaporation -153 cm/year​
  - Average precipitation 135 cm/year​
  - P + E = -18 cm/year (E <0)​

```math
\boxed{\Delta V = V_{earth} + P + E}
```
- For a stable climatic system $`\Delta V = 0`$ thus $`V_{earth} = P + E`$
- Climatic changes increases $`V_{earth}`$ (water from runoffs) making $`\Delta V`$ positive thus ocean level rises.
- From the below graph, the link between evaporation and salinity is clear (where evaporation is higher than precipitation the salinity is high).

![zonal averages](./img/03_Heat_Water_Salt/zonal_averages2.png)

### Salt Budget

- Incoming salt:​
  - From runoff 65%​
  - From ground water 25%​
  - Others (precipitation, dilution of sediment , volcanic eruptions, dilution of organic matter, aeolian transport ) 10%​
- Outgoing salt:​
  - Absorption by sediment and organic matter 40%​
  - Salt precipitation during mixing of runoff water and sea water 25%​ (where the runoff enters the seawater).
  - Salt precipitation due to evaporation in shallow seas 25%​
  - Wind salt transfer from sea to land 10%
- Salt budget for a **partially enclosed** sea:
  - We don't look at the ions that enter with the precipitations (take it as fresh water).
  - Evaporation 
  - Ions from water that gets in.
- Calculate the salt budget for a partially enclosed sea:
  - $`V_i`$: in $`m^3\cdot s^{-1}`$. It is the rate of water exchange for component of i, (from water budget).
  - $`S_i`$: in ‰ or g/kg. It is salinity of the water.​
  - $`\rho_i`$: in $`kg\cdot m^{-3}`$. It is density of the water.
  - $`\Delta M`$: in $`kg \cdot s^{-1}`$. It is the rate of salt mass changes in the sea. ​
  - The sum is done for every factor affecting the salinity budget.
  - Fresh water from precipitations will have a $`S_i = 0`$
  
```math
\Delta M = \Sigma (V_i \times \rho_i \times S_i \times 10^{-3})
```

- **Bulk formula** helps estimate fluxes: sensible heat and evaporation are difficult to calculate at sea (due to the big waves, strong winds and storms).

## Climate Change

### Greenhouse Effect

- The more $`CO_2`$ the less transparent the atmosphere is to long wave radiations.
- The increase in temperature has been demonstrated by 3 organisations.
- The globally averaged combined land and ocean near to surface temperature data (2m above the earth) as calculated by a linear trend, show a​ warming of​ 0.85 [0.65 to 1.06] °C,​ over the period​ 1880 to 2012​.
- We can see in the north Atlantic a part where there is a decrease in temperature and not increase: 
  - It is on the Gulf stream path. 
  - The Gulf stream is a hot stream that warm the areas on its path.
  - The icebergs melt and release cold fresh water which are lighter than salt water thus stay above the Gulf stream and thus $`\rightarrow`$ decrease in temperature in this area.
- Rise of sea level: ice caps + thermal expansion.
