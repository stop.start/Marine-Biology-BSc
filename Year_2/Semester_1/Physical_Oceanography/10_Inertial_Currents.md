# From Special Cases of Motions to Oceanic General Circulation Models (OGCM)​

![equation of motion](./img/10_Inertial_Currents/equation_motion.png) 

## Simplified equations of motion for ideal inertial currents

- Assumptions: 
  - Strong winds that suddenly stops (thus no tangential stress).
  - No friction (no land, no friction between layers).
  - No horizontal pressure gradient (no slopes, no change in atmospheric pressure or water density).
  - No vertical motion.
  - Vertical pressure gradient is balanced (equals) by gravity.

![equation of ideal motion](./img/10_Inertial_Currents/equation_ideal_motion.png) 

- What's left of the equations of motion that only take into consideration the Coriolis effect:
  - Those equations need to be solved.
  - It won't change because the state is stable.
- **f**: Coriolis parameter ($`= 2\Omega \sin(\varphi)`$)

```math
\Downarrow \\
\begin{aligned}
\boxed{
\begin{aligned}
&\frac{du}{dt} = fv \\
&\frac{dv}{dt} = - fu 
\end{aligned}
}
\end{aligned}
```

- Solution
  - **V**: initial speed.
  - **t**: periodic time (?)
  - **u**: horizontal movement on a circle (because without wind, the current will go in circle from Coriolis).
  - **v**: vertical movement on a circle

```math
\begin{aligned}
& u = V \sin(ft) \\
& v = V \cos{ft} \\
& V^2 = u^2 + v^2
\end{aligned}
```

- Because there is no friction it is an infinite circle.
- Diameter of the circle: $`\bold{D_i = 2V \div f}`$
- The period of the circle (how long it takes to go around): $`\bold{T_i = (2\Pi) \div f = 24[h] \div (2\sin(\varphi))}`$
- The higher the latitude the stronger Coriolis thus the smaller and faster the circle:

| $`\varphi [\degree]`$ | $`T_i [hr]`$ | $`D_i [km]`$ |
|-----------------------|--------------|--------------|
|                    90 |        11.97 |          2.7 |
|                    35 |        20.87 |          4.8 |
|                    10 |        68.93 |         15.8 |

- In reality the circles are not perfect and infinite, but looking at the paths of drifters we can see some circles

![drifters](./img/10_Inertial_Currents/drifters.png)

## Ekman Layers

- Assumptions as written above excepted for friction between layers being present.
- Steady state wind and current: $`\tau_{z=0} = \rho_aC_dW^2_{10}`$
  - **W**: wind speed
  - **C**: is the water flat or with waves
- Turbulent viscosity coefficient (friction between layers) is constant with depth ($`A_z = const`$): $`\tau_z = A\frac{\delta u}{\delta z}`$
- **Simplified equations of wind driven currents**:
  - $`\bold{\tau}`$: tangential stress between layers, $`\tau_0`$ is wind stress.
  - $`\bold{A_z}`$: eddy viscosity.
- The below equation gives the transfer of energy between layers.

```math
\begin{aligned}

&\boxed{
\begin{aligned}
& fv + \frac{1}{\rho}\frac{\delta \tau_x}{\delta z} = 0 \\
& -fu + \frac{1}{\rho}\frac{\delta \tau_y}{\delta z} = 0 \\
\end{aligned}
}

\\
\text{}\\

&\text{Where:}\\

& \frac{\delta \tau_x}{\delta z} = A_z \frac{\delta^2 u}{\delta z^2} \\
& \frac{\delta \tau_x}{\delta z} = A_z \frac{\delta^2 u}{\delta z^2} \\

\end{aligned}
\\
\text{}\\
\Downarrow \\
\text{}\\
\begin{aligned}
\boxed{
\begin{aligned}
& fv + \frac{A_z}{\rho}\frac{\delta^2 u}{\delta^2 z} = 0 \\
& -fu + \frac{A_z}{\rho}\frac{\delta^2 v}{\delta z^2} = 0 \\
\end{aligned}
}
\end{aligned}
```


- The solutions for wind driven currents for the northen hemisphere (z=0):
  - $`\bold{V_0}`$: current velocity at the surface (= starting speed).

```math
\begin{aligned}
& \boxed{
\begin{aligned}
& u = V_0 e^{az} \sin\left(\frac{\pi}{4}-az\right) \\
& v = V_0 e^{az} \cos\left(\frac{\pi}{4}-az\right) \\
\end{aligned}
}  

\\
\text{}\\
&\text{Where:}\\

& V_0 = \frac{\tau_0}{\sqrt{\rho f A_z}} \\
& a = \sqrt{\frac{\rho f}{2A_z}} \\
\end{aligned}
```
```math

\\
\text{}\\
\Downarrow \\
\text{}\\

\boxed{
\begin{aligned}
& \text{Solution for surface current (z=0):} \\
& u(0) = V_0 \sin\left(\frac{\pi}{4}\right) \\
& v(0) = V_0 \cos\left(\frac{\pi}{4}\right) \\
& \hookrightarrow u(0) = v(0) \implies \textbf{surface current is directed at } \bold{45 \degree} \text{ to the right of the wind.}
\end{aligned}
}
```

- Each layer below the previous is directly slightly to the right of the previous layer thus a larger angle from the surface water.
  - This continues until all the wind momentum is converted into water motion.

![ekman spiral](./img/10_Inertial_Currents/ekman_spiral.png)

- **Ekman layer depth $`\bold{D_E}`$**: at this depth the direction of the current is opposite to the surface current.
  - At the equator the Coriolis effect doesn't exist thus Ekman's solutions do not apply.
  - At $`D_E`$ depth, the current velocity is about 23 times less than at the surface.
  - $`\bold{W_{10}}`$: wind at 10m height.

```math
\boxed{7.6W_{10} / \sqrt{\sin(\varphi)}}\\
\Downarrow
```

| $`W_{10}`$ [m/s] | Lat $`0\degree`$ | Lat $`15\degree`$ | Lat $`45\degree`$ | Lat $`90\degree`$ |
|------------------|------------------|-------------------|-------------------|-------------------|
|                5 | $`\infin`$       | 75m               | 45m               | 38m               |
|               20 | $`\infin`$       | 300m              | 180m              | 152m              |

- **Ekman Transport** (integral water transport): average direction of the water is $`90\degree`$ from the wind direction.
  - The water transport is up to $`D_E`$ (Ekman layer depth), below it goes in the opposite direction (creating up/downwelling).
- In the mediterranean, between Cyprus and Israel the wind is often from west to east thus the water current is from Cyprus to Israel.
  - This creates an upwelling in Cyprus, thus more nutrients in this area.
  - In Israel there's downwelling from those winds.
  
![water transport](./img/10_Inertial_Currents/water_transport.png)

- Cyclonic winds: water goes away from the center causing the water level at the center to lower in turn causing upwelling at the center and the thermocline to rise.
- Anticyclonic winds: water comes to the center causing the water level to rise at the center causing  downwelling and the thermocline to lower.
- Those changes in water level are usually a few centimeters.
- Those water movements can also happen from difference in density.

![anti/cyclonic](./img/10_Inertial_Currents/cyclonic.png)

- **Ekman pumping**: there's a divergence at the equator from the water going in opposite direction in the northen and southen hemisphere.
  - This causes upwelling at the equator, this is why the water is colder there than in the areas around.

![equator](./img/10_Inertial_Currents/equator.png)

## Geostrophic Balance

- **Geostrophic currents**: balance between the Coriolis force and pressure gradient (**geostrophic balance**) causing the current to stop being pushed by the gradient (slope $`\implies`$ gravity) but only Coriolis thus going around the isobar in circle (when no friction).

![geostrophic current](./img/10_Inertial_Currents/geostrophic_currents.png)

- For geostrophic balance to occur:
  - Steady state (no acceleration)
  - Large (> tens of kilometers)
  - More than a few days
  - No friction
  - No vertical movement

![equations](./img/10_Inertial_Currents/geostrophic_equations.png)

- The simplified equations:
  - The xy axis are balanced by the Coriolis force thus "=0".

```math
\begin{aligned}
& \text{Simplified equations:} \\
& -\frac{1}{\rho}\frac{\delta p}{\delta x} + fv = 0 \\
& -\frac{1}{\rho}\frac{\delta p}{\delta y} - fu = 0 \\
& -\frac{1}{\rho}\frac{\delta p}{\delta z} - g = 0 \\
\\
\text{} \\
\Downarrow \\
\text{} \\
& u = - \frac{1}{\rho f} \frac{\delta p}{\delta y} \\
& v = \frac{1}{\rho f} \frac{\delta p}{\delta x} \\
\end{aligned}
```

- When two columns of water have different densities with the lower column having a higher density:
  - Just below the surface the bigger mass of the higher column has a stronger effect than density thus there movement from the pressure gradient (PGF).
  - Deeper, the bigger mass from the higher column still causes some movement but the difference is weaker.
  - At some depth (around 1000m) there's is no more movement because the pressures (density vs mass) are equal.
  
![mass vs density](./img/10_Inertial_Currents/mass_density.png)

- **Dynamic height (DH)** (calculated from CTD data) is the potential energy of water at a specific point relative to a geopotential surface.
  - From density (calculated from temperature and salinity) calculate the PGF (pressure gradient force).
- **Low/Left** method: left hand on the low DH, the current is in the "nose" direction.

![low/left](./img/10_Inertial_Currents/low_left.png)

- Numerical models: used to simulate oceanic flows with realistic and useful results. They can resolve h
  - 3D time dependent flow ​
  - Containing mesoscale eddies​ (tens of kilometers)
  - Realistic coasts and sea-floor features​
  - Synoptic atmospheric forcing.​
- ppt10/45-end for how it works.
- SELIPS: South Eastern Levantine Israeli Prediction System (numerical model).
