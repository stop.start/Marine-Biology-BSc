# Tides

- **Diurnal tide**: one high and one low tide per day $`\implies`$ period = 24h.
- **Semidiurnal tide**: two high and low tide of equal heights per day $`\implies`$ period = 12h.
- **Mixed semidiurnal tide**: two high and low tide of unequal heights per day $`\implies`$ period = 12h.
  - Second tidal range (=high-low) has a lower amplitude.
- Tides are the longest oceanic waves (longer than tsunami).
  - d/L << 1/20 thus shallow water waves.
- They occur regularly (predictable).
- Earth rotation and gravity DO NOT generate tides.
  - The earth rotation causes a **centrifugal effect** and produces a buldge (בליטה) around the equator. This does not change in time though.
- Newton looked at the external force: moon and sun.
  - Sum is a lot but a lot farther than the moon thus it has a lower effect on the tides.
  
  |              | Sun                    | Moon                   |
  |--------------|------------------------|------------------------|
  | Mass         | $`2.0\times 10^{33}g`$ | $`7.3\times 10^{25}g`$ |
  | Distance     | 150,000,000km          | 385,000km              |
  | Tidal effect | 0.46                   | 1.00                   |

- **Barycenter**: center of the mass of a system 
  - Here between the moon and earth: the earth has a bigger mass thus the barycenter is not only closer to it but actually within it.
  - Both earth and moon rotates around the barycenter once every 27.3 days.

![earth_moon](./img/12_Tides/earth_moon.png)

## Equilibrium Theory

- Assumptions: no land and no friction.
- Centrifugal force around the barycenter pulls the water in opposite direction of the gravity of the moon ([here is an explaination](https://youtu.be/erpJ_EhQ9KM?t=218)).
- The gravitational force of the moon pulls the water (and everything everywhere): stronger on the face of the earth that is closer to the moon but also on the other side.
- At the equator the forces are parallel to the earth gravity which is a lot stronger thus cancels the centrifugal and moon gravity forces.
  - Because everywhere else on earth those 2 forces are not parallel to the earth gravity (see picture below), they do affect the water and push it (=**tractive force**) to the zenith of the moon (near the equator).
  - Between those forces: low tide.

![forces](./img/12_Tides/forces.png)

- Semidiurnal tides have periods of **12h and 25min**.
  - Although the earth rotates every 24h the semidiurnal tides are not every 12h.
  - Because the moon rotates in the same direction of the earth, the earth needs to rotates a bit more to get back into the same position from the moon.
  - **Lunar day** is 24h and 50min thus semidiurnal tides are half that.
  
  ![lunar day](./img/12_Tides/lunar_day.gif)

- The **moon declination** changes: the plane of the moon orbit is at an angle to the plane of the earth's equator.
  - When it crosses the plane of the equator the declination is 0.
  - The declination moves between an angle of $`28.7\degree`$ above and below the equator's plane.

![moon declination](./img/12_Tides/moon_declination.png)

- The moon declination also explains the difference in range (difference in amplitude between low and high) every 6 days in mixed semidiurnal tides:
  - At high declination, on one side (closer to the moon) the tide is high.
  - Rotating a quarter of the earth rotation causes low tide.
  - Then another quarter (furthest from the moon) high tide but smaller than the first one.
  - The last quarter is at low tide.
- Close to the pole: diurnal tide (one high and one low per day).
- At declination 0 semidiurnal tides.

![mixed tides](./img/12_Tides/mixed_tides.png)

- **Spring tides**: interaction of solar and lunar tides when the sun and moon are in line.
  - This causes a big tide.
  - Happens shortly after new and full moon.
  
![spring tides](./img/12_Tides/spring_tides.png)

- **Neap tides**: when the sun and moon are at $`90\degree`$ (lowest tides).

## Dynamic theory

- Tides are shallow water waves thus their speed is limited by the ocean depth:
  - $`c=\sqrt{g d}, \quad g=9.8 m s^{-2}, d=5000 m, c \approx 220 m s^{-1}`$
  - The water buldge should move at 220m/s but the earth rotates too rapidly thus we need to take into consideration lands and friction $`\implies`$ Newton's theory cannot work.
  - Laplace worked a a dynamic theory.
- The dynamic theory takes into account:
  - Bathymetry
  - Coriolis
  - Inertia and friction
- New equation: **acceleration + Coriolis = pressure gradient force + tractive force**
  - The tides propagate like **Kelvin waves**.
- **Kelvin waves** (trapped waves) are tides that are affected by pressure gradient and Coriolis force:
  - When a wave gets to a land it builds up (pressure gradient increases) thus needs to go backward.
  - Coriolis won't let it go backward thus the wave will move along the land to the right (pressure/Coriolis balance).
  - At the equator: waves pass the equator, for example to the south, Coriolis sends them back north, once north of the equator Coriolis sends them back south, etc.
    - The waves will stabilize and go in a straight line along the equator (El Nino).
  - **Tidal lag**: around the equator high tide usually arrives hours after the passage of the moon (6 hours delay). At latitude $`65\degree \rightarrow`$ no delay.

![kelvin wave](./img/12_Tides/kelvin_wave.gif)

- Usually the tides are around 2m at the shore. Some places which are narrow it can reach high amplitudes due to **resonance**, for example at the **bay of Fundy** (Canada) it can reach 17m.
  - Bay of Eilat: 1.5m
  - Bay of Gabes (Tunis): 2.5m
- **Resonance**: the time it takes to the wave to go from the mouth of the bay to the shore is pretty much the same as the period from on tide to another (12h24m vs 12h26m) reinforcing the tides.
- Using Laplace theory tide prediction models can be creating. Using **harmonic analysis** tide can be predicted more accurately at a specific location (introduced by Kelvin).
  - Waves are divided into their composing regular waves (semidiurnal, diurnal, etc) which can be predicted thus predicting the tide at a certain location.
  
  ![prediction](./img/12_Tides/prediction.png)
