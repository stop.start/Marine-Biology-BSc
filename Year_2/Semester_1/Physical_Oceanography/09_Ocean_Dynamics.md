# Aspects of Ocean Dynamics

## Basic Principles of Newtonian Mechanics

- Conservation of mass
- Conservation of energy
- Conservation of momentum
- Newton's 3 laws of motions
- Conservation of angular momentum
- Newton's law of gravitation

### Conservation laws leading to basic equations of fluid motions

- Conservation of mass $`\longrightarrow`$ **continuity equation**
- Conservation of energy $`\longrightarrow`$ conservation of heat $`\longrightarrow`$ **heat balance**
- Conservation of momentum $`\longrightarrow`$ **Navier-Stokes equation**:

```math
\begin{aligned}
& p = mv \\
& \Delta p = F \Delta t  \\
& F = \frac{dp}{dt} = m \left(\frac{dv}{dt}\right) = ma \quad \text{Newton's second law}  \\
\end{aligned}
```

#### Momentum equation

- Acceleration of a particle is proportional to the sum of forces acting on the particle: $`\frac{dU}{dt} = \frac{1}{m} \sum F`$
- **For fluid motion**: all forces are assumed to be forces "per unit volume" $`F^v`$ (this means that the force needs to be applied all along the defined volume?).
  - Mass of unit volume $`m`$ is replaced by density $`\rho`$.

```math
\begin{aligned}
& \boxed{\frac{dU}{dt} = \frac{1}{\rho} \sum F^v} \\
\\
& \text{For each vector (x,y,z):} \\
& \frac{du}{dt} = \frac{1}{\rho} \sum F_x \\
& \frac{dv}{dt} = \frac{1}{\rho} \sum F_y \\
& \frac{dw}{dt} = \frac{1}{\rho} \sum F_z \\
\end{aligned}
```

## Main Forces Acting on a Fluid Particle in the Ocean

### Primary forces that cause motion:

- Pressure gradient
- Wind stress 
- Gravity (varying weights of water produce vertical motion and change horizontal pressure gradients)

> Primary forces can exist but not cause movement if the state is stable.  
> Buoyancy acts against gravity thus can neutralize it (and so there's won't be movement downward).  
> If theres a wall then wind stress won't cause movement (the wall applies the opposite force).  

### Secondary forces (acting when water starts moving):

- Coriolis force
- Friction tending to oppose the motion (act around the fluid as well as inside)

$`\boxed{\text{density} \times \text{particle acceleration = pressure gradient + Coriolis + wind stress + friction + gravity}}`$

#### Pressure gradient

- A particle moves from high pressure to low pressure.
- **The gradient is defined from low pressure to high pressure but the force is from high pressure to low pressure**.
- Acceleration is proportional to the pressure gradient.​
- The x axis is always from left to right.
- Horizontal pressure gradient can happen when:
  - Changes in water density (water goes from high density area to low density area).
  - Sloping water surface (which causes a higher water column on one side).
  - Changes in atmospheric pressure.
- Equations:
  - The minus is because the particle is accelerated from high pressure to low pressure (while the gradient is defined from low to high).

```math
\begin{aligned}
& \frac{du}{dt} = - \frac{1}{\rho}\frac{\delta p}{\delta x} + \text{other forces} \\
& \frac{dv}{dt} = - \frac{1}{\rho}\frac{\delta p}{\delta y} + \text{other forces} \\
& \frac{dw}{dt} = - \frac{1}{\rho}\frac{\delta p}{\delta z} + \text{other forces} \\
\end{aligned}
```

#### Coriolis [pseudo-]force

- Coriolis parameter: $`f = 2 \Omega \sin{\varphi}`$
  - $`\Omega`$ is the angular velocity of the Earth (full revolution in 24h)
- When the water advances on the Y axis, Coriolis gives it force on the X axis (to the right in the northen hemisphere) thus "v" is used.
- When the water advances on the X axis, Coriolis gives it force on the Y axis downward thus "-" and "u" are used.

```math
\begin{aligned}
& F^{Cor}_x = fv \\
& F^{Cor}_y = -fu \\
& F^{Cor}_z = 0 \\
\end{aligned}
```

#### Friction in the laminar flow due to molecular viscosity

- **Tangential stress $`\bold{\tau}`$** develops between two layers of fluid.
- Newton's law of friction:
  - $`\mu`$: molecular viscosity.
  
```math
\begin{aligned}
&\text{Friction at the surface:}\\
&\boxed{\tau = \mu \frac{\delta u}{\delta z}}
\\
&\text{Friction force between layers:}\\
& F_x^{fric} = \frac{\delta \tau_x}{\delta z} = \mu_z \frac{\delta^2 u}{\delta z^2} \\
& F_y^{fric} = \frac{\delta \tau_y}{\delta z} = \mu_z \frac{\delta^2 v}{\delta z^2} \\
\end{aligned}
```

- In oceans laminar flow (molecular mixing) doesn't really exist. The flow is usually **turbulent** (turbulent mixing).
  - Turbulent mixing is more intensive than molecular mixing.
  - Turbulent mixing is also called **eddie mixing**.
  - Molecular viscosity $`\mu`$ is replaced by **eddy viscosity coefficient A [$`m^2s^{-1}`$]**
  - A big difference between laminar mixing and turbulent mixing is that turbulent mixing in **vertical direction (downward) is opposed by buoyancy but not in horizontal direction**. That's why $`A_z`$ (vertical) << $`A_h`$ (horizontal).
  - $`A_z \approx 10^{-5} \; \text{to} \; 10^{-1}m^2s^{-1}`$ 
  - $`A_x \approx 10 \; \text{to} \; 10^{5}m^2s^{-1}`$ 

```math
\begin{aligned}
&\text{Eddy stress at the surface:}\\
&\boxed{\tau = A \frac{\delta u}{\delta z}}
\\
&\text{Friction force due to eddy viscosity between layers:}\\
& F_x^{fric} = \frac{\delta \tau_x}{\delta z} = A_z \frac{\delta^2 u}{\delta z^2} \\
& F_y^{fric} = \frac{\delta \tau_y}{\delta z} = A_z \frac{\delta^2 v}{\delta z^2} \\
\end{aligned}
```

- Stability prevents vertical mixing.
- When the layer at the surface gets energy (e.g from the wind) it will pass this energy to the layers below until the picnocline which throw out any molecule trying to pass trough.

![az](./img/09_Ocean_Dynamics/az.png)

#### Gravity

- g = 9.81 $`ms^{-2}`$

```math
\begin{aligned}
& F_x^{grav} = 0 \\
& F_y^{grav} = 0 \\
& F_z^{grav} = -g \\
\end{aligned}
```

#### Equation of motion - Navier-Stokes equations

```math
\begin{aligned}
& \frac{du}{dt} = \overbrace{-\frac{1}{\rho}\frac{\delta p}{\delta x}}^{\text{pressure gradient}} + \overbrace{fv}^{Coriolis} + \overbrace{\frac{1}{\rho}\frac{\delta \tau_x}{\delta z}}^{surface friction} - \overbrace{Ju}^{internal friction} \\

& \frac{dv}{dt} = \quad\; - \frac{1}{\rho} \frac{\delta p}{\delta y}  \space\space\space\space\space  - fu \quad\space\space  + \quad \frac{1}{\rho}\frac{\delta \tau_y}{\delta z} \space\space\space\space\space\space - \qquad Jv \\

& \frac{dw}{dt} = \underbrace{-\frac{1}{\rho}\frac{\delta p}{\delta z}}_{\text{pressure gradient}} - \underbrace{g}_{gravity} \qquad\qquad\qquad\>\;\, - \underbrace{Jw}_{internal friction} \\
\end{aligned}
```

#### Continuity equation

- Based on continuity of mass.
- Water that goes out needs to be replaced.
- The sum of the acceleration is 0.

```math
\frac{\delta u}{\delta x} + \frac{\delta v}{\delta y} + \frac{\delta w}{\delta z} = 0
```

![continuity mass](./img/09_Ocean_Dynamics/continuity_mass.png)
