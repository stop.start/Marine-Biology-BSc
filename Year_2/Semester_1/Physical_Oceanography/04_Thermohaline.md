# Ocean Thermohaline Structure

- Sea Surface Temperature (SST).
- Surface temperature: up to one meter deep.
- **AVHRR** - Advanced Very High Resolution Radiometer:
  - Space-borne sensor that measure the reflectance of the Earth in 5 relatively wide channels​.
  - Radiometer that converts infrared radiation into an electrical voltage.
- Upper layer of the sea:
  - **Surface bulk layer**: 0-1m (Tb on the below graph). At night it is bigger (up to 10m).
  - **Skin layer**: 0.1-1mm (Ts on the below graph)
- The skin layer is colder due to the evaporation and sensible heat flux (heat is transfered to the atmosphere) thus doesn't reflect well the state of the current layer.
- The surface bulk layer is less sensitive thus reflects better the climatic conditions.
- The lower the wind the bigger the temperature difference between the skin layer (colder) and bulk layer.
  - $`dT = Tb - Ts = f(wind speed)`$
  
![temperature profile](./img/04_Thermohaline/upper_layer.png)

- The salinity depends on the difference between precipitations and evaporation (P-E).
- Precipitation - Evaporation:
  - Maximum P-E is within 5°-10° belt around the equator where Precipitation is high and Evaporation is low due to low winds and high humidity. Known to mariners as the Doldrums.​
  - Minimum P-E are in regions located to north and south from equator where humidity is low and winds are strong (Trade winds). ​
- Surface salinity:
- Mean sea surface salinity clearly reflects the fresh water balance. Lowest salinities occur in regions of maximum P-E, although the relationship is not straight forward.​
- Significant deflections from this relationship occur in regions of strong influence of currents (salt advection) and runoff (fresh water input from land).  ​
- The Amazon region release a lot of fresh water in the ocean.
- Upwelling areas also affect the salinity since deep water that are less salty go up to the surface.
- Currently salinity is measured from boats but a sattelite is in the work: **SMOS (Soil Moisture and Ocean Salinity)**.
- Heat and moisture fluxes (precipitation, evaporation, warm/cold, etc..) affect the density of the upper layer of the sea: **virtual vertical mass flux** (virtual because it is not really a flux, it is just the density that changes).
  - Evaporation affects the density in 2 ways: evaporation and cooling (since evaporation causes the cooling of the water).
- The density of the water is lower in the equator and higher in the poles:
  - The temperature is what determines the increase or decrease the density over the change in salinity.
  - Altough in the equator the salinity is higher than in the poles the temperature is a lot higher thus the density in lower than in the poles where the temperature is the lowest.
  
![vertical structures](./img/04_Thermohaline/vertical_structures.png)

- Thermocline and Halocline can be the opposite of the above graph but not the Pycnocline:
  - There can be location where the surface temperature is lower than the temperature a deep.
  - The halocline will follow the thermocline.
  - The density cannot be higher at the surface, this is unstable and the water will sink.
- **Gradient**: rate of change of a physical parameter Y in an arbitrary direction (x).​ In other words **derivative**.

## Mixing in the Sea

- **Molecular mixing** isn't a big player in the mixing of the oceans.
- **Turbulent mixing**
- **Convection**: vertical mixing
- **Double diffusion**: molecular mixing + conveection

### Turbulent mixing

- **Laminar flow**: between layers, no mixing, slowly flows in the movement direction.
- **Turbulent flow**: general direction but also creation of whirlpools. Also some vertical movements.

![turbulent flow](./img/04_Thermohaline/turbulent_flow.png)

- **Reynolds number**: boundary between laminar and turbulent flow.
  - The faster the more turbulent.
  - The more viscosity the less turbulent.
  - U: velocity
  - L: horizontal scale (pipe diameter)
  - $`\eta`$: shear viscosity
  - $`\rho`$: density
  - When $`R_E > 2000`$ it changes from laminar to turbulent.
 
```math
R_E = (UL\rho)/\eta
```

- Reynolds worked with water with the same density.
- Horizontally the ocean _is_ always turbulent but not vertically.
- In oceans the density changes vertically and it stabilizes the turbulence and that's why there are halocline/pycnocline/thermocline.
  - For example at the surface the water that are turbulent horizontally cannot break into the below layer.
- Oceanic turbulence comes from:
  - Wave motions​: breaking of the waves
  - Vertical or lateral current shear (תזוזה): variations of velocities with depth or across the flow.​
  - Water movement over an irregular sea-bed or along an irregular coast (also in depth): if there's a hill (also underwater hill) the water will go over it back down behind causing turbulence. ​
  - Tidal currents.​
  - Traveling eddies. ​

#### Static stability and stratification

- The density layers (light at the top, heavy at the bottom) increases the stability (no mixing).
  - A body a water with low density that "tries to dive" into a heavier layer will triger the buoyancy force (see below).
- Vertical displacement of a parcel of water between depths with different densities:
  - **V**: water volume
  - **g**: gravity
  - **$`\bold{\rho_1 - \rho_0}`$**: different of densities between the "diving" water body and the water around it.
  - Archimedes: buoyancy: $`\bold{F_b = Vg(\rho_1 - \rho_0) = \pm g\Delta\rho}`$
- Stability of water: how much the water "don't want" to be turbulent/mixing. 
- Stability without taking into account the compressibility (from change in pressure) and adiabatic changes (changes from change in temperature):
  - $`E \approx \frac{1}{\rho} \; \frac{d \rho}{dz}`$
  - **E**: stability
  - $`\frac{d \rho}{dz}`$: vertical gradient of density (z = depth). Above 0 density increases with depth (vice versa below 0).
  - $`\frac{d \rho}{dz} > 0`$: buoyancy force F returns the body of water to the layer above (stable).
  - $`\frac{d \rho}{dz} < 0`$: F will sink the body of water (unstable).
  - $`\frac{d \rho}{dz} = 0`$: neutral.
- Precise equation that includes the gradients of temperature and salinity that make up density:
  - $`\alpha`$: how the density changes per unit of temperature.
  - $`\beta`$: how the density changes per unit of salinity.
  - The higher the gradient of the density the higher the stability.
  - The lower the gradient the less stable (turbulent)
  - Equal 0: neutral
- The stability (because of density) depends on temperature and salinity:

![equation](./img/04_Thermohaline/equation.png)

- In summer, the thermocline shows higher temperature at the surface which decreases with depth. 
- Under a certain depth everything is stable. The upper layer is more chaotic.
- In the first 100m there is not change in salinity and temperature thus no density gradient which mean the stability is around 0.
- With depth, the potential temperature decreases which increases the potential density and thus the stability.
- With depth, the salinity decreases which decreases the potential density and thus the stability.
- In the winter the potential temperature stays the same from the surface to deep and thus there is no more stability and thus there is mixing.
- The below graph shows:
  - On the left, the potential temperature and salinity don't change in the top 200m, below that, they both decrease.
    - If we look at deeper water than shown the salinity will increase again.
  - On the right, the potential density (pink line) doesn't change in the first 200m then increases.
    - This matches the potential temperature and salinity graph: temperature affects more the density than salinity thus a decrease in temperature will increase the density despite the decrease in salinity.
  - The decrease in temperature with depth shows an increase in temperature stability $`E_t`$.
  - The decrease in salinity with depth shows a decrease in salinity stability $`E_s`$ (decrease in salinity decreases the density).
  - Since temperature affects more than salinity, the overall stability E increases with depth.
  
![stability](./img/04_Thermohaline/stability.png)

- Stability frequency N:
  - Buoyancy frequency or Brunt-Vaisala frequency.
  - Vertical frequency from vertical displacement of a fluid parcel. Like a vertical spring, that goes up and down more or less fast.
  - At the surfaces there are a few cycles per minute Deep, a few cycles per hour (more stable).
  - The higher the frequency the more stable the water is.
  
> Explanaintion:  
> A higher frequency is the result of well defined layers that push back a body of water that a different density, while a lower frequency means the absence of layers or layers that are not too different in density.  
> Since the presence of layers means stability, higher frequency means higher stability.  

```math
N^2 = g\cdot E
```

#### Dynamic Stability

- **Richardson's Number**
- Reynolds work was for a stable density.
- Richardson saw that there's mixing between 2 layers of different density.
- The mixing occurs when there's current and specically when there's a "cut" in the current:
  - Stronger current above and weaker one just below.
- Richardson's number: static stability divided by the gradient of speed with depth of the current: the bigger the difference between the speeds of the current the less stable it is.
  - Ri > 0.25: stable flow
  - Ri < 0.25: velocity shear enhances turbulence

```math
Ri = N^2/(dU/dz)^2
```

- **The static stability can be very stable (density increases with depth), but there can be turbulence because of the low dynamic stability caused by difference of speed in the the different layers**.

#### Unstability

- **Convection slope**: 
  - A decrease in temperature (or other process that increases density) will increase the the density at the surface layer.
  - A thiner layer above ground will get denser faster than a layer above the ocean which will be bigger.
  - The density being higher on this higher ground can go down the convection slope which is called **cascading**.

![cascading](./img/04_Thermohaline/cascading.png)

- **Double diffusion**:
  - Small scale instability from molecular mixing.
  - This happens even without turbulence.
  - Heat diffuse 100 times faster than salt: heat is the transfer of energy (a fast moving molecule that bump into another making it moving faster), while salt diffusion is the actual moving of molecule in space (a lot slower).
- Two phenomenons from double diffusion: salt fingers and staircase
- **Salt finger**: in a stable state (above: hotter, saltier, less dense), if the water is "hit" (like from wind), the hot water will loose heat faster than it will loose salt thus the "finger like" structure will get colder, still saltier than the layer below and thus get denser and sink. At some point it will be homogeneous.

![salt fingers](./img/04_Thermohaline/salt_fingers.png)

- **Staircase**: step like structure from a stable state that becomes more stable: 
  - If there is hot water at the bottom (from cascading), and the gradient isn't really defined.
  - When a parcel of water tries to go up (because it is hotter) it will get cold fast without loosing salt and go back down sharpening the gradient.

![two layers](./img/04_Thermohaline/two_layers.png)

- **UML (upper mixed level)**: upper layer is homogeneous:
  - The atmosphere can create the UML by causing turbulence through winds and waves.
  - Heat flux usually sets the limits of the UML size.
  - Cooling and evaporation (positive mass flux) in the UML causes the pycnocline to sink and increase the size of the UML.
  - Heating and precipitation (negative mass flux) creates a new pycnocline and creates a new UML.
  - Stormy weather develops the UML.
  - When the upper layer gets colder, at some point all will be homogeneous (the upper layer will disappear).
  - The thermocline/halocline/pycnocline sets the lower limit of the UML.

### Seasonal changes

- Around the equator (-60/60 latitude) there's a permanent thermocline:
  - Deep (really cold) water formed at the poles spread throughout the oceans and the temperature doesn't change.
  - At the top, the UML is formed with seasonal changes but still a lot warmer than the bottom.
  - In the middle: thermocline.
  
![permanent thermocline](./img/04_Thermohaline/perm_thermocline.png)

- **Water mass**: an oceanographic water mass is an identifiable body of water with a common formation history which has physical properties distinct from surrounding water. Properties include temperature, salinity, chemical.​
- Common water masses in the world ocean are:​
  - Antarctic Bottom Water (AABW)​
  - North Atlantic Deep Water (NADW),​
  - Circumpolar Deep Water (CDW),​
  - Antarctic Intermediate Water (AAIW),​
  - Subantarctic Mode Water (SAMW),​
  - Arctic Intermediate Water (AIW)​

### Global oceanic converyer belt

- Greenland: water sinks
- 2 permanent upwelling.
- Thousands of years to do a full circle.
- Climate change can cause the conveyor belt to move faster.

![converyer belt](./img/04_Thermohaline/conveyor_belt.png)

