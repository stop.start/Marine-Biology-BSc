# Characteristics of Sea Water: Temperature, Salinity, Density

| Term                          | Equation                                    | Definition                                                                                           |
|-------------------------------|---------------------------------------------|------------------------------------------------------------------------------------------------------|
| $`\bold{\rho}`$               |                                             | Density (0 is distilled water)                                                                       |
| $`\bold{\sigma}`$             | $`\sigma = \rho - 1000`$                    | Density anomally from distilled water                                                                |
| $`\bold{\sigma_t}`$           | $`\sigma_t = \sigma(T,S,0)`$                | Density without pressure                                                                             |
| $`\bold{T_{\text{in situ}}}`$ |                                             | Temperature in the water as is                                                                       |
| $`\bold{T_A}`$                |                                             | Adiabatic change in temperature. For water $`0.2 \degree C km^{-1}`$                                 |
| $`\bold{\theta}`$             | $`\theta = T_{\text{in situ}} - T_A`$       | Potential temperature: without the adiabatic effect, simulate like it was at the surface             |
| $`\bold{\sigma_{\theta}}`$    | $`\sigma_{\theta} = \sigma (\theta, S, 0)`$ | Potential density: no effect of pressure including in the temperature (passing $`\theta`$ and not T) |
| **dbar**                      |                                             | Decibar, unit pressure used. For seawater with regular density ($`\approx`$ 35): **1 dbar = 1m**     |
| **Isopycnal**                 |                                             | Lines showing equal density on a map                                                                 |
| **Isohaline**                 |                                             | Lines showing equal salinity on a map                                                                |
| **Isotherm**                  |                                             | Lines showing equal temperature on a map                                                             |

## Temperature

- Temperature isn't the amount of heat but the kinetic energy.
- ​Practical temperature scale is necessary to have accurate identical temperature estimations. The definitions are based on temperatures of fixed points as triple point for water. The most recent scale the International Temperature Scale was introduced in 1990.
- At $`0\degree C`$ water is found in all 3 states: solid, liquid, gas.
- Temperature of sea water can be measured with:
  - A **thermometer** which needs to be inside the body of water. This is called **in situ** temperature.
  - **Remote measurer of sea surface temperature (SST)**, less precise.
- Temperature is measured up 0.01 precision.
- **Minimum freezing point**: $`-1.87\degree C`$ for 35g/Kg ($`-1.65\degree C`$ at the bottom of the Antarctic)
- **Maximum temperature** of water: around $`40-45\degree C`$ in the Dead Sea. In some places it can reach up to $`60\degree C`$.
- 90% of the oceans water is under $`4\degree C`$.

## Salinity

- Pure water doesn't exist in nature. There are always ions and gases.
- **Salinity is the amount in grams of solids in gases dissolved in a kilogram of water**. 
- Today salinity doesn't have a unit since we measure the salinity by **conductivity of the water**.
- Ice has no salt in it (unless there are some crack in it where salt deposited).
- Salinity of drinking water less 1 g/kg​
- Salinity of **brackish** (מליחים) water less than **25 g/kg**​.
  - Up to 25 g/kg (this include fresh water in addition to brackish) the anomaly of water exists, above this it disappear (salt water).
- Ocean salinity about 33-37 g/kg​
- Salinity of Eastern Mediterranean Sea Water is about 40 g/kg​
- Salinity of the Dead Sea is about 280 g/kg​
- Total amount of salts in the Oceans is 49.2·1018 kg (distribution of the total amount over the Earth surface will bring layer of 40 m or 150 m for distribution over land surface only).​
- The same 11 ions compose the ocean water. The amount of ions can change but their proportion stays more or less the same.
- Sodium and Chloride are the main ions (85%).
 
![sea composition](./img/02_Sea_Water/sea_composition.png)
### Measuring the Salinity
- Methods:
  - Evaporation (which gave the g/kg - deprecated)
  - Titration (chemical method - deprecated)
  - Conductivity (used today)
- The **chemical method** (deprecated) to measure salinity is to use **chlorinity**: 
  - Define chlorinity (Cl‰) by titration and calculate salinity S‰ = 1.80655·Cl‰ (since ratio between elements and total salinity is about constant).​
  - Gives good results but is long and expensive.
- Measuring the salinity can also be done by **electrical conductivity**:
  - The conductivity of sea water (denoted by C) depends on the number of dissolved ions per volume (i.e. salinity) and the mobility of the ions (i.e. temperature and pressure).
  - The units used are mS/cm (milli-Siemens per cm) although today it is less used.
  - Conductivity increases by the same amount with a salinity increase of 0.01‰, a temperature increase of 0.01°C, and a pressure increase of 20 dbar (about 20 m depth).​
  - In order to measure the conductivity, we use **CTD** (Conductivity-Temperature-Depth probes).

> _Note:  
> Siemens the SI unit of conductance: the conductance of one ampere per volt in a body with one ohm resistance.  
> SI: International System of Units​_  

- Definition of salinity (physical method): $`\bold{S = f(Cond, Temp, Pres)}`$
- After taking samples with CTD, the salinity is calculated with an **autosalinometer** which gives the ratio $`\bold{K = C_{sample} : C_{standard}}`$
- **Standard conductivity** (מליחות חשמלית סטנדרטית) - $`C_{standard}`$: solution of potassium chloride **KCl**.

> _Note:_  
> At first the standard sample was taken from the center of the Atlantic but the conductivity wasn't stable, then Copenhagen water which was too expensive.  

## Hydrostatic Pressure

- **Pascal's law**: Any change in the pressure applied to a completely enclosed fluid is transmitted undiminished to all parts of the fluid and the enclosed walls.
- Force applied uniformly over a surface and measured as **[F]orce per unit of area ([S]urface)**:
  - $`P = \frac{F}{S}`$
  - Units $`Newton/m^2`$ which is called **pascal - Pa**.
- **Hydrostatic pressure**:
  - g: gravity acceleration.
  - $`\rho`$: water density.
  - z: height of water column.

```math
\boxed{P = \rho gz}
```

- Note that the difference in pressure within the water column isn't taken in consideration.
- Changes in density (ρ) affect the pressure a lot less than changes in height (z) thus the height is what really matters in terms of pressure.
- The atmosphere puts pressure on the water surface thus also on the water column: $`P_z = P_0 + g\rho z`$
  - $`P_0`$ = 1atm = 1 bar = **10 decibar (dbar)**. 
- The atmospheric pressure is **removed** from the CTD measurements.
- Hydrostatic pressure in proportional to depth: $`10m \approx 10dbar`$, $`1000m \approx 1000dbar`$ 
  - Calculated with the above equation as the average density of sea water since most of sea water is very close to the average.
- This works for regular sea water but not places like the Dead Sea where the density is higher.
- **Archimedes' principle**: 
  - The buoyant force on an object in a fluid is equal to the weight of the fluid that object displaces.​
  - The buoyant force  (or force of Archimedes) is the result of difference in hydrostatic pressure on upper and lower parts of a body:
    - The upper surface of a body, being higher, is subject to a smaller column of water thus lesser pressure.
    - The lower surface of a body, being lower, is subject to a bigger column of water thus bigger pressure.
    - The difference of pressures (the force applied above < the force applied below) is what allows for buoyancy.​
  
![archimedes](./img/02_Sea_Water/archimedes.png) 
  
- $`\rho`$: fluid density
- A: surface of the object (top and bottom)
- M: mass of the fluid that object displaces ($`= \rho V_{object} = \rho Ah`$ where h is the height of the body) 
- P: P1 - pressure at the top, P2 - pressure at the bottom

```math
F_A = F_2 - F_1 = P_2A - P_1A = g\rho h_2A - g\rho h_1A = g\rho A(h_2 = h_1) = g(\rho Ah) = gM
```

## Density

- The density of sea water ρ depends on temperature T, salinity S and pressure p. This dependence is known as the​ **equation of State of Sea Water (EOS)**.

```math
\rho = \rho(T,S,p)
```

- **Oceanic density: $`1025 kg \cdot m^{-3}`$** (usually)
- Oceanographers use the **anomaly density** from $`\bold{1000 kg \cdot m^{-3}}`$ which is defined by the letter $`\sigma`$
  - $`1000 kg \cdot m^{-3}`$ is the density of **distilled water**.
  - The anomaly is the "distance" from the distilled water. Seawater density: $`\sigma = 25 kg m^{-3}`$
 
 ```math
\boxed{\sigma = \rho - 1000} 
 ```
 
- **Homogeneous ocean**: 
  - Constant density
  - EOS $`\rightarrow \rho (z) = \text{const}`$
  - _For example, Eckman's model showing the effect of the winds uses constant density._
- **Barothropic model of the ocean**:
  - Density is function of hydrostatic pressure only (not temperature nor salinity) which depends on the depth.
  - EOS $`\rightarrow \rho (z) = f(p)`$
  - _For example, waves like tsunami or even tide, the effect of temperature and salinity is negligible._
- Taking into account temperature and/or salinity. Density depends on:
  - Temperature only: $`\rho = \rho_0[1 - \alpha (T-T_0)]`$
    - $`\alpha`$: coefficient of thermal expansion
  - Temperature and salinity - **baroclinic model of the ocean**: $`\rho = \rho_0[1 - \alpha (T-T_0) + \beta (S-S_0)]`$
    - $`\beta`$: coefficient of saline contraction
    - This approaches the real equation of state but is simple and linear.
- Real model: $`\rho (z) = \rho(T,S,p)`$
  - Takes into account everything (EOS1980)
  - The real ocean is baroclinic.

### Effect of Temperature, Salinity and Pressure

- Temperature:
  - The wamer the less dense.
  - Not linear.
- Salinity:
  - Affect faster the density than the temperature.
  - The higher the salinity the denser the water.

![density temp](./img/02_Sea_Water/density_temp.png)

- Pressure:
  - Seawater is compressible but a lot less than a gas.
  - The higher the pressure the denser the water.
  - If the water wasn't compressible then the level of the oceans would be 30.4m higher.
  - Water that sinks and is not at the surface anymore is **conservative**: temperature and salinity don't change much (for temperature there actually is, more in the temperature part).
    - Temperature and salinity allow to follow a block of water and know where this block came from.
  - In order to know what the pressure of a block of water was at the surface, the density need to be calculated without pressure: $`\sigma_t = \sigma(T,S,0)`$. This is called **sigma-t**. This helps know where the water came from geographically by comparing the pressure at the surface of the water vs where there was the same pressure.

### Max Density and Freezing Point

- At 0 salinity, freezing point at $`0 \degree C`$ and max density at $`4 \degree C`$.
- At 25 salinity, the freezing point and max density are equal.
- Above 25 salinity, there is no more "water anomaly".

![graph](./img/02_Sea_Water/graph.png)

- **Isopycnal line**: line with the same density.
- The below graph show density per salinity combine with temperature.

![isopycnal](./img/02_Sea_Water/isopycnal.png)

## Temperature

- When a block of water sinks, there is a change in temperature with the increase in pressure.
- **Adiabatic temperature**:
  - Adiabatic temperature is the changes of temperature in a block of something **internally**, without heat exchange with the surroundings.
  - When a fluid expands it loses internal temperature.
  - When a fluid compresses it gains internal temperature.
- Air that rises decreases $`9.8\degree CKm^{-1}`$. Water that rises decreases $`0.2 \degree CKm^{-1}`$.
- Calculating the temperature for a block of water that is at the bottom without the effect of pressure, meaning kind of like it was at the surface altough it is not, is called **potential temperature $`\bold{\theta \degree C}`$**. The actual temperature of this block of water in called **in situ $`\bold{T_{\text{in situ}} \degree C}`$**.
- Calculating $`\theta`$:
  - $`\Delta T_A`$: adiabatic changes
  
```math
\boxed{\theta = T_{\text{in situ}} - \Delta T_A}
```

> _Example:_ the Dead Sea  
> In the graph below, the blue line shows in situ water temperature, meaning this includes the adiabatic effects on the water.  
> The red line in the potential temperature ($`\theta`$).  
> In the Dead Sea the adiabatic changes affect more the water because it is denser.  
> Removing those adiabatic effects allows us to see a clear change in temperature at the bottom that is **not** due to pressure increase. This water comes from an **evaporation basin** thus the water is hotter.  
> Without removing the adiabatic effects this characteristics is masked by the influence of pressure on the water.  

![dead sea](./img/02_Sea_Water/dead_sea.png)

## Density

- When there is denser water above then the water are not stable and this water sinks.
- A denser block of water will sink and get even denser from the hydrostatic pressure (which also causes the temperature to increase).
- When looking at the change of density with depth we want to look at it without the effect of pressure ($`\sigma_t = \sigma(T,S,0)`$). This way though the given temperature is affected by pressue (adiabatic changes).
- The adiabatic compression increases the temperature which lower the density. This is why looking at the density _in situ_ $`\sigma_t`$ shows a decrease in density and we could conclude that the water is not stable.
- $`\bold{\sigma_{\theta}}`$ is the **potential density** which is the density it had when it was at the surface, i.e without pressure. It is calculated with the potential temperature $`\theta`$.

```math
\boxed{\sigma_{\theta} = \sigma (\theta, S, 0)}
```

![potential density](./img/02_Sea_Water/potential_density.png)

## Salinity

- Vertical lines on a map showing salinity per depth and distance are called **isohaline**. 
- Isohaline lines show equal salinity.

![isohaline](./img/02_Sea_Water/isohaline.png)

## Maps

### T/S Diagram

- Potential temperature per salinity per potential density.
- The map shows isopycnal lines (equal density) because we can calculate the density per potential temperature and salinity.
- Blocks of water with the same potential temperature, potential density and salinity (all 3 of them) come from the same location (but we don't know where with this graph).

![ts diagram](./img/02_Sea_Water/ts.png)

