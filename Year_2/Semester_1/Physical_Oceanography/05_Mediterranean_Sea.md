# Hydrography of the Mediterranean Sea

## Basins and Water Exchange

- The mediterranean sea is split into 3 basins:
  - **Western Basin**
  - **Ionian Basin**
  - **Levantine Basin**
- Ionian Basin and Levantine Basin are sometimes included together and called **Eastern Basin**.
- **Sicily strait** separate the Western and Eastern basins. It is relatively shallow (430m) and let pass only surface and intermediate water.
- **Gibraltar strait**: connection between the Mediterranean sea and the Atlantic ocean (300m).
- **Bosphorus**: connection between the **Black sea** and **Marmara sea** (40m). **Dardanelles strait** between **Marmara sea** and the **Aegean sea**.
- **Cretan Arc straits** are important because they link the Aegean sea and the Levantine basin.
- **Otranto strait**: important because there's an exchange of deep water between the **Adriatic sea** and the **Ionian basin**.

![basins](./img/05_Mediterranean_Sea/basins.png)

- Most of the Mediterranean sea is up to 2000m deep.
- Maximum depth: around 4500m next to Greece.
- Sicily strait and Adriatic sea are the most shallow.
- Mediterranean sea and Black sea **water budget**:

```math
V_{in} + V_{out} + V_{runoff} + P + E = 0
```

![budget](./img/05_Mediterranean_Sea/budget.png)

- The evaporation in the Mediterranean sea is high.
- Although the amount of water from runoffs is bigger in the Mediterranean sea than in the Black sea, it is a small number compared to its surface while the Black sea is a lot smaller than the Mediterranean and thus gets more water from runoffs per surface.
- There is more water entering the Mediterranean from the Black sea than the opposite because there is more (per surface) freshwater entering the Black sea (from runoffs).
- Only from the straits (not precipitations or others): the Mediterranean receives water and the Black sea loses water.
- The Mediterranean sea is a **concentration basin**.
  - Because of the high evaporation, the water level in the Mediterranean sea is lower than in the Atlantic thus there's always water entering at the surface from the Atlantic.
  - Because of the difference in salt concentration between Mediterranean (higher) and the Atlantic (lower) $`\rightarrow`$ current from Mediterranean to the Atlantic (diffusion).
  - Because of the high evaporation, in the summer the **halocline is overturn** (saltier at the surface than the bottom).
  - In the winter, the water at the surface gets colder and because of the high salinity that build up at the surface during summer, there's a convection (water sinks) and there's mixing of water.
  - Because of the mixing there is oxygen up to the bottom of the sea.
- The Black sea is a **dilution basin**.
  - The level of water is higher than in the Mediterranean thus the water at the surface goes from the Black sea to the Mediterranean because the water from runoffs and precipitations are higher than the evaporation.
  - Mostly freshwater enters the sea thus it creates a stable state: **permanent halocline**.
  - Because of the stable state, there's no mixing of layer and thus no oxygen (and no life) deep in the sea.
  - There's a bit of oxygen at the bottom from the water that enters deep from the Mediterranean.

| Mediterranean Sea                                                    | Black Sea                                                        |
|----------------------------------------------------------------------|------------------------------------------------------------------|
| Concentration basin: runoffs + precipitations < evaporation          | Dilution basin: runoffs + precipitations > evaporation           |
| $`R(0.11) + P(0.50) < E(1.27) [m/year]`$                             | $`R + P > E`$                                                    |
| ![concentration](./img/05_Mediterranean_Sea/concentration_basin.png) | ![dilution basin](./img/05_Mediterranean_Sea/dilution_basin.png) |

- Luigi Marsigli: model of two ways currents.
  - Hydrostatic pressure $`P=\rho g z`$ (density, gravity, height).
  - In his experiments, gravity and height are the same but not density (which is affected only by salinity) $`\rightarrow`$ different pressure on each side of the box.
  - The difference of pressures causes the currents.
  - From Richardson's number there's turbulence between the currents.

![marsilli](./img/05_Mediterranean_Sea/marsilli.png)

- Rate of water passing through a strait is measured in **Sverdrup = $`\bold{10^6 m^3s^{-1}}`$**. Sverdrup is the name of the scientist.
- Atlantic water getting in the Mediterranean has a salinity of 36.3‰. 
- The salinity of the water getting out of the Mediterranean has a salinity of 37.8‰.

## General Circulation - Upper Mixed Layer

- The overall circulation of the Mediterranean is cyclonic (counterclockwise).

![general ciculation](./img/05_Mediterranean_Sea/general_circulation.png)

- The salinity on the map from 10m depth because skin layer is too sensitive to small changes.
- The salinity gets higher from west to east due to the evaporation.
- The entry of Atlantic water lowers the salinity at the Gibraltar strait.
- In the north Aegean sea the salinity is really low. That's because of the freshwater coming from the Black sea which is less dense and thus stays above the salty water of the Mediterranean.

![salinity](./img/05_Mediterranean_Sea/salinity.png)

- Atlantic water (AW) is called modified Atlantic water a bit farther than in the Gibraltar strait. By the time it got to the Levantine basin it has lost all its characteristics from the Atlantic and is just called Levantine water.
- The Atlantic water (AW) brings water with lower salnity. From the below salinity graph, it is shown that this water goes deeper and deeper from west to east. That's because of turbulence mixing the water.
- From the TS graph $`\rightarrow`$ the stations 510 and 511c are created in the same place (see line joining). 

    ![salinity ts](./img/05_Mediterranean_Sea/salinity_ts.png)

- In the winter, deep water (DW) with maximum density is created in Lyon gulf (**WMDW**) and Adriatic sea (**EMDW**).
  - Orange on the map
  - Because the Lyon gulf is deeper than its surrounding (2000m), it cannot diffuse to the rest of the Mediterranean.
  - The deep water (DW) from the Adriatic diffuses to the eastern Mediterranean.
  - The EMDW is saltier than the WMDW
- Regions of where density of the UML > 28.8 are regions of Intermediate water formation.​
- Regions of where density of the UML > 29.1 are regions of the deep water formation.​

![winter density](./img/05_Mediterranean_Sea/winter_density.png)

- The **EMDW** (eastern mediterranean deep water) comes from the Adriatic sea.
- The **LIW** (Levantine intermediate water) comes from the south of Aegean sea.
- **Rhodes gyre** gives the characteristics that allow the formation of intermediate water.
  - It is a cyclonic gyre which means that at the center the water rises (can be only a few centimeters).
  - Because of the rise of water the halocline, thermocline and isopycnal also rise: it looks like a hill on the graphs.
  - The cold water rising from below cools the super saline LSW which gets cold fast and thus sinks.
  - The cold LSW is not heavy enough to get through the isopycnal so it "slides" on its sides until it reaches a point of equal density: the LIW is created.

|                                                              |                                                                  |
|--------------------------------------------------------------|------------------------------------------------------------------|
| ![LIW creation](./img/05_Mediterranean_Sea/liw_creation.png) | ![cyclonic hills](./img/05_Mediterranean_Sea/cyclonic_hills.png) |

- In winter, the water get even colder and can break the isopycnal creating a downward convection (the water doesn't go up anymore).
- Every winter there's a formation of LIW between those gyres.
- **Deep convection chemney**:
  - Deep convection is usually present where there's a cyclonic circulation.
  - Where there's a cyclonic ciculation there's usually a isopycnal hill (density).
  - The top cools faster creating a convection (chemney) because of the density and until it is homogeneous.
  - The hill can form again if the cold stops.
- Just under Rhodes gyre there's the anticyclonic **Mersa metruh gyre**. It isn't always there though.
- Between Rhodes gyre and Mersa Metruh there's a rise of water. 
- Around the Sicily strait, the LIW circulate deep at the bottom from East to West while the AW circulate at the top from West to East.
- Anticyclonic gyre: downwelling (sinking of water).
- Cyclonic gyre: upwelling (rise of water).
- Looking at the temperature profile of the water, only the LSW can be seen. The others (as well as LSW) can be seen on the salinity profile (and TS).

| Summer                                     | Winter                                            |
|--------------------------------------------|---------------------------------------------------|
| LSW present                                | No LSW                                            |
| ![LSW](./img/05_Mediterranean_Sea/lsw.png) | ![LSW](./img/05_Mediterranean_Sea/lsw_winter.png) |

## Climatological Mediterranean Conveyor Belt​

- Deep water formed in the Gulf Lion and in the Adriatic sea cannot move to the sides and are stuck there (because of the shape of the bottom of the sea).

![conveyor belt](./img/05_Mediterranean_Sea/conveyor_belt.png)

- Currents to know in the Levantine basin:
  - **Long shore** current: cyclonic current along the eastern shores.
  - **MMJ** (Mid-Mediterranean Jet): current in the middle of the basin and in between gyres.
  - **Rhodes gyre** (described earlier): cyclonic.
  - **Mersa metruh gyre**: anticyclonic and above Rhodes gyre.
  - Migration of the Mersa Metruh gyre and Cyprus gyre to 8b on the map.
  - Iera petra gyre.

![gyres](./img/05_Mediterranean_Sea/gyres.png)
![gyres](./img/05_Mediterranean_Sea/gyres2.png)

- **Meanders**: unstable circulation of water that can detach and become eddies.
  - The MMJ was hard to discover because of the meanders in the region.

![meanders](./img/05_Mediterranean_Sea/meanders.png)

- Chlorophyll can be used to know the direction of current:
  - Chlorophyll taken inside the sea by the meanders:

![chlorophyll](./img/05_Mediterranean_Sea/chlorophyll.png)

## Eastern Mediterranean Transient Phenomenon

### What is it?

- In 1989 the source of deep water stopped being the Adriatic sea and instead it was the Aegean sea.
- The source of deep water in the Mediterranean is mostly the Adriatic sea (until 1989).
  - Ont the T/S graph: monotic decrease in temperature and salinity, all starting at a point equals to the adriatic sea measures.
- In 1992, unusual winter and there was convection up to the bottom.
- In 1994, the anomaly stopped.

#### Before Transient

|                                                                      |                                                                       |
|----------------------------------------------------------------------|-----------------------------------------------------------------------|
| ![before transient](./img/05_Mediterranean_Sea/before_transient.png) | ![before transient](./img/05_Mediterranean_Sea/before_transient2.png) |

#### After Transient

|                                                                    |                                                                     |
|--------------------------------------------------------------------|---------------------------------------------------------------------|
| ![after transient](./img/05_Mediterranean_Sea/after_transient.png) | ![after transient](./img/05_Mediterranean_Sea/after_transient2.png) |

### How is was found

- At some point, new highly saline water was found at the bottom (which was not before) of the sea in the Cretan basin.
  - The **maximum of salinity was found at the bottom**
  - It was not from direct convection (not from the water at the surface): 
    - In March 1987 (spring), the density found in the Cretan basin was 29.1.
    - In October 1987 (just after summer), the density at the very bottom was 29.2.
    - Since in summer there is no convection, the water must have come from advection, measing from somewhere else.
  - The same water density was found in the Aegean sea.

![deep water anomaly](./img/05_Mediterranean_Sea/dw_anomaly.png)

### How it happened

- Water with density of 29.2 can be found in the Aegean sea next to Lesbos and Lemnos islands.
- Salty surface water enter the Aegean sea from the Cretan basin and get colder the more north they travel.
- The highest cooling of this water is at the continental shelf (same level as the islands).
- Usually, this water meets and mixes with water from the Black sea (not very saline) and thus do not sink (at least not deep).
- If the was doesn't meet and mix with Black sea water then the water being high in salinity and getting very cold, sinks to the bottom and go south to the Cretan basin.

|                                                                      |                                                                       |
|----------------------------------------------------------------------|-----------------------------------------------------------------------|
| ![origin of deep water](./img/05_Mediterranean_Sea/origin_of_dw.png) | ![origin of deep water](./img/05_Mediterranean_Sea/origin_of_dw2.png) |

### What happened after?

- At some point the Aegean stopped being the dominant deep water source.
- The following T/S diagram shows:
  - Monotic descrease in temperature, salinity and density.
  - Then increase in density from the transient (warmer and saltier).
  - Then at the bottom, new water that is cold and lower in salinity but more dense.
  
![ts after](./img/05_Mediterranean_Sea/ts_after.png)

### Other effects

- The Aegean water from the transient lifted the old water in the Cretan sea which was low in oxygen and replaced it with new water high in oxygen.

### Why it happened - BIOS

- **BIOS** (Bimodal Oscillating System)
- In the north of Ionians sea, the current can be anticyclonic or cyclonic (north ionian gyre).
- When anticyclonic, part of the AW will go as usual to the eastern basin and a **bigger part enters the Adriatic sea**.
  - This causes the Adriatic water to be lower in salinity than usual and thus less advection (more stable) and less deep water created.
  - Because in the Levantine basin less AW gets in, the water is higher in salinity.
  - When the Levantine water gets in the Aegean sea it increases its usual salinity.
- When the current is cyclonic, the AW goes all in the Levantine basin and not in the Adriatic making the Levantine water lower in salinity and the Adriatic water higher.
    - This makes the Adriatic more effective at creating deep water and the Aegean less effective.
  
![bios](./img/05_Mediterranean_Sea/bios.png)

## Effects of the Mediterranean on the world

- The water of the Mediterranean is a lot higher in salinity than the water of the Atlantic.
- When it get out through the Gibraltar strait, it spread all the way to the American coasts and north close to Greenland.
- Since the area near Greenland is the area where the Global Conveyer Belt "starts" (water sinks), it is a part of the process (it increases the process due to the high salinity).
- Blocking the water from going through Gibraltar could cause world issues.
