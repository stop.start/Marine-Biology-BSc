#  The Hydrography of the Red and Black Seas

## Red Sea

- Long and thin sea.
- Gulf of Eilat and gulf of suez on one side.
- Bab el-Manded strait of the other side.
- It is between tectonic plaques.
- The gulf of Eilat is around 600m/700m deep.
- The gulf of Suez is shallow.
- At the center the depth reaches around 2000m.

### Some numbers

- Area: 450,000$`km^2`$.
- Evaporation: 2m/year.
- Precipitation and Runoff: 0.07m/year.
- Most saline part of the oceans.
- The salinity of the water leaving the Red sea is around 40.5.
- The salinity of the water entering the Red sea is around 36.5.
- Bab el-Mandeb strait depth is 130m (deep water cannot go through).
- It is a **concentration basin** (R+P<E).
  - Water enters due to the evaporation which removes water from the basin.
  - Water leaves to equilize the pressure from the difference in density/salinity.
  - It has an overturn halocline.

### Circulation

- The overall ciculation in **cyclonic**.
- The north of the Red sea is less warm than the south.
  - The south is closer to the equator.
  - The high evaporation cools down the north a lot.
  - Salinity and density are thus higher in the north.
  - The south gets an influx of water from the ocean less saline and hotter.
- In the winter, since the density in the north is higher, the intermediate water (LIW) is created there.
- Deep water is created usually in the gulf of Suez (some part is created in the gulf of Eilat).
  - The gulf of Suez is quite shallow.
  - In the summer, with the high evaporation, the salinity is high.
  - In the winter, the water becomes really dense and when it reaches the slope it slides at the bottom.
- The depth of the UML in the north part of the Gulf of Eilat:
  - Varies depending on the climatic conditions.
  - In 1991/1992 the weather was unusually cold from the volcanic eruption of the Pinatubo volcano. It allowed for a deeper convection in the winter (important to bring up nutrients).
  
![deep water](./img/06_Red_Sea_Black_Sea/deep_water.png)
  
- 1% of the Red sea reaches over 2000m.
  - The temperature can reach up to 60$`\degree`$C and the salinity up to 280.
  - This changes the chemical composition, meaning that it can't be compared to the standard composition of the oceanic water thus CTD cannot be used to measure salinity or density.
  - The water is really stable and **double diffustion** can happen there:
    - No salty fingers since it needs an upper layer warmer and saltier and here it is less warm and less salty.
    - **Staircase** can happen (when a parcel of water tries to go up (because it is hotter) it will get cold fast without loosing salt and go back down sharpening the gradient).
- The water is hotter at the bottom from volcanic actions due to the separation of the tectonic plaques.
  
## Black Sea

### History

- Once the Black sea was completely closed off getting water only from the runoffs.
- The sea level was lower than today.
- With the Earth getting warmer, the ocean level rose fast (60m/month).
  - In turn, the Mediterranean sea level rose which caused it to break through the Bosphorus strait.
  - This made the Black sea level rise and people left their home on the coast which were drown.

### Facts

- Deep at the bottom, hydronium sulfate (which is black) can be found.
- Runoffs and precipitations are higher than the evaporation.
- Bosphorus: influx of salt water from the Mediterranean.
- Influx of fresh water:
  - Danube river (big influx) 
  - Dneiper river
  - Sea of Azov (shallow sea - 13m)
  - A lot of other rivers

### Some numbers

- Area: 410,000$`km^2`$.
- Evaporation: 0.9m/year.
- Precipitation and Runoff: 1.4m/year.
- Fresh water budget (fresh water from the runoffs): 0.5m/year.
- The salinity of the water leaving the Black sea is around 35.
- The salinity of the water entering the Black sea is around 17.
- The sill (Bosphorus) depth is 90m (deep water cannot go through).
  
### Circulation

- It is a **dilution basin** (R+P>E).
  - Water leaves due to the extra water from the runoffs.
  - Water enters to equilize the pressure from the difference in density/salinity (weak diffusion).
  - Permanent halocline.
- The water from the runoffs:
  - In summer, it mixes with the LSW (top 20m).
  - In winter, up to 50m/100m.
  - This causes the **oxygen to not get to the bottom**.
  - The oxygen can get to the halocline, underneath it's hydronium sulfate which is toxic.
- The overall ciculation is **cyclonic**.
- It has a **long shore jet** (cyclonic).
- Two big cyclonic gyres (west and east).
  - Those cause the water to go upward (upwelling), increasing the surface salinity and decreasing the temperature.

![black sea](./img/06_Red_Sea_Black_Sea/black_sea.png)

### Parameters

- **Salinity**
  - In the below graph, in green the salinity profile, in red the gradient (the change with depth).
  - The temperature doesn't affect much the density, the salinity is the main parameter.
  - No winter can break the permanent halocline (only the seasonal), it is very stable. This is because the deeper water are saltier since they come from the Mediterranean.

![salinity gradient](./img/06_Red_Sea_Black_Sea/salinity_gradient.png)

- **Stability**:
  - In the winter, the permanent halocline is between 50m and 100m.
  - There's also maximum stability at the surface in the west of the sea due to fresh water from runoffs.
  - In the summer the permanent halocline is at the surface caused by the runoffs and the atmostphere that can't mix more 20m.
  - In the summer the stability, where the winter halocline was, hasn't change but the surface is even more stable.
  - Because the influx of fresh water from the Danube is so big, the seasonal halocline remains even in winter (on the left in the winter diagram).

![stability](./img/06_Red_Sea_Black_Sea/stability.png)

- The convection cannot go deeper than 100m. This causes a **lack of oxygen** in the deeper water.
  - In the Danube area, because of the fresh water influx the convection is pretty shallow.
  - It is also shallow in the gyres which cause upwelling.
  - The convection is deeper (still less than 100m) around the Bosphorus and a few other areas around the gyres (middle upwelling but around downwelling).
- **ICL** (Intermediate Cold Layer): remains of water from the winter which cannot break through the halocline (underneath the temperature increases from the Mediterranean water).

### Long term changes

- From 1945 to 1985 there was an increase in runoffs from the Danube.
  - This means a decrease in salinity at the delta.
  - In from runoffs: $`400km^3`$
  - In from Bosphorus: $`350km^3`$: water more dense thus sink right away after entering.
  - Out from Bosphorus: $`200km^3`$
  - Under the permanent halocline $`H_2S`$ is found but also in the summer under the seasonal halocline next to the Danube delta.
  - This is from the increase in runoffs causing the permanent halocline to rise from the increase in stability (less mixing from the addition of freshwater).
  - This causes an increase in salinity under the halocline and the rise of $`H_2S`$.
  
|                                                        |                                                        |
|--------------------------------------------------------|--------------------------------------------------------|
| ![long term](./img/06_Red_Sea_Black_Sea/long_term.png) | ![long term](./img/06_Red_Sea_Black_Sea/long_term.png) |


- $`H_2S`$ is created from the lack of oxygen (anoxia).
