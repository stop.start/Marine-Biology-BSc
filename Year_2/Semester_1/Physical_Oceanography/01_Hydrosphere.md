# Hydrosphere

## Intro

- Physical oceanography focuses on predicting the future physical behavior of the oceans.
- The physical properties of the oceans are:
  - Physical properties of sea water.​
  -	Air-Sea Interaction (השפעה הדדית).​
  -	Ocean motions such as Currents, Water Mixing, Sea Level Changes, and Waves.
- There are **2 approaches** to physical oceanography:
  - **Descriptive approach**: start with an observation and find some regularities and parametization. _For example, the change of temperature between summer and winter. Collecting the temperatures and put them on a graph which shows the fluctuations up and down of the temperatures (high in summer, low in winter)._
  - **Theoretical approach**: applying physical laws to the oceans
- The descriptive approach started from sailor that reported what they saw but nothing was official until 1872 when the H.M.S Challenger (British) was sent to do research. This was the first research boat.
- Research boats have since been sent to take sample at several stations and use interpolation (between the measurement) to "fill the gaps" and extrapolation (oustide the measurements, predictions).
- Today we use autonomous sea and space platforms in order to take the measurements: 
  - Sattelites: temperatures at sea level
  - Buoys with sensors. Those can have a system to broadcast their measurements to sattelites.
  - Argo floats: that can float or dive depending on what is programmed and can wander up to a year. It can get dragged in current so the itinerary cannot be predicted with precision.
  - Sea glider: has a motor and although it is autonomous it still can be controlled by pilots to correct its direction.

## Hydrosphere

- Earth has:
  - Atmosphere
  - Hydrosphere
  - Lithosphere
  - Biosphere
- The **hydrosphere** includes all the water + all the glaciers.

### Some numbers

- Earth area 540 x 106 km2  (**R=6400 km**)​
- World Ocean area 361.3 x 106 km2  (**71%**)​
- Land area  149 x 106 km2 (29%)​
- Volume of Hydrosphere 1360 x 106 km3​
- **World Ocean 96.5% from Hydrosphere**​
- Volume of World Ocean is 1/800 from Earth volume​
- If one erases the entire earth topography and spreads all the waters on the global surface the resulting ocean depth would be about 3000 m.​

### Origins of the Hydrosphere

- When Earth was formed, the gases escaped the ground to form the atmosphere. This process is called **degassing**.
- The hydrosphere was formed after the atmosphere: 
  - The gases formed the atmosphere 4.6 billion years ago
  - 2.5 billion years ago, with the Earth getting colder, drops started to form and rain started to fall which formed the hydrosphere.
- Life became abundant 600M years ago and humans 2-3M years ago.

### Today

- Repartition of the water on Earth:
  - **97% in the oceans**
  - **0.001% in the atmosphere**
  - **2.8% on land**
- Most water on land are from glaciers (as ice). This ice is the main source for the rise of the water level with the global warming.
- The **residence time** (זמן שהות) is the average time spent in the water body (sea, lake, etc) of a water molecule.
  - The residence time in **oceans** is around **4000 thousands years**.
  - The residence time in the **atmosphere** is **around 10 days**.
  - The residence time on **land** is around **380 hundred years**.

![water on land](./img/01_Hydrosphere/water_land.png)

### Earth

- The globe has one side with almost only water (mainly Pacific ocean) and one side with almost only land (all the continents). This affects the climate since the radiations from the Sun are not absorbed the same way on land and in oceans (on land warming is faster than in the oceans).

#### Coordinate System

- The Earth's radius is 6360km.
- The equator is at latitude 0.
- A **meridian** is the half of an imaginary great circle on the Earth's surface, terminated by the North Pole and the South Pole. All meridians have the same length. 
- The **prime meridian** is the longitude 0.
- A longitude is the angle λ between a meridian and meridian 0 (Greenwich).
- If we cut the Earth in half (passing through the center) wether it's around the equator or another angle it will give the same result. This is called the **great circle** and has a surface of 39941km ($`S = 2r\pi = 2 \cdot 6360 \cdot \pi`$)
- The latitude is the angle φ from the equator.
- 1° of a Meridian = 39941/360 = 111 km.
- 1° of a Parallel   = 111cosφ km; where φ is latitude. ​
- 1 minute of a Great Circle of the Earth = international nautical mile = 111000/60 = 1852 m  ​

![coordinates](./img/01_Hydrosphere/coordinates.png)

#### Mercator projector

- **Mercator projector** is a map projection with longitudes and latitudes as lines with $`90\degree`$ angles between them. Since the Earth isn't flat, some parallels are farther apart than others in order to stay true to the real distance.
- With this kind of map, we can use a ruler and compas to trace a line from start to end point but the farther the destination is from the starting point the bigger the error margin.

|                                                     |                                                    |
|-----------------------------------------------------|----------------------------------------------------|
| ![mercator map](./img/01_Hydrosphere/mercator2.png) | ![mercator map](./img/01_Hydrosphere/mercator.png) |

- A straight line on the map is the great circle on the map (or part of it), this means that **a straight line on the map isn't the shortest route**.
- The straight line is called **loxodrome**. It seems to be the shortest way from A to B but because the map represent the globe it is actually as spiral and not a straight line thus not the shortest way.
- The line called **orthodrome** is actually the shortest way from same A and B if the loxodrome.
- That's why for shorter travels, it is a viable option, the margin error is smaller (the route is more straight).
- Another issue from the mercator map is that it doesn't preserve the sizes of the lands, for example Africa and Greenland have the same size although when sailing it isn't a relevant issue.

![loxodrome and orthodrome](./img/01_Hydrosphere/loxodrome.png)


### Oceans

- The average depth of the oceans is **4000m**.
- Horizontally between **5000km-15000km**.
- **Hypsographic curve** is an histogram that show the frequencies of **heights and depths** on Earth with the height of Mount Everest and Mariana trench as the scale's maximum and minimum.
- The curve shows that most land heights are between 0-1000m (20%) and most underwater depths between -4000m and -5000m (23%).

![hypsographic curve](./img/01_Hydrosphere/hypsographic_curve.png)

- Continental Shelf (מדף היבשת) continuation of tectonic plate gradient of 1/500, depths of 0 – 200m​
- Continental Slope  (מדרון היבשת) gradient 1/20​
- Deep Sea Bottom (קרקעית הים) relatively horizontal​
- Mid Ocean Ridge  (רכס מרכז אוקייני )​
- The globe is a **geoid** which means that it is not smoothly round but has ups and downs.
- In order to measure the depth of the oceans, today we use **echo sounders** and **satellite altimetry**.
- A sea level surface is everywhere perpendicular to gravity. This causes the water to have a higher level above underwater hills.​
- A two-kilometer high seamount would produce a bulge of approximately 10 m.​
- The sea level is also affected by winds, tide and such but the effect is weaker than the effect of gravity and the geoid shape.

![sattelites](./img/01_Hydrosphere/sattelites.png)

#### Properties of water

- **Anomaly of water**. Between 0 and 4 degrees. The reasson the ice floats.
- Specific heat is the amount of heat needed to increase 1g of matter 1 $`\degree C`$. $`Q = cm\Delta T`$
- **Specific heat** (חום סגולי): $`4.18\times 10^{3} JKg^{-1}\degree C^{-1}`$. 
- It has the highest specific heat after liquid amonia $`NH_3`$. This is the reason it is good at preventing extreme temperatures.
- **Latent heat of fusion** (חום כמוס להתכה): $`3.33\times 10^{5} JKg^{-1}`$.
- It has the highest latent heat of fusion after amonia.
- Latent heat is the reason water with ice in it don't get warm: all the energy goes to break bonds in the ice and slowly melt it.
- **Latent heat of evaporation**: $`2.25\times 10^{6} JKg^{-1}`$.
- Water has the highest latent heat of evaporation.
- **Surface tension** less important in oceanography since it only affects small waves. Bigger waves are affected by gravity.
- **Dissolving power** (כושר המסה): the power to break down substances.
- **Transparency**
- **Conduction of heat**: water has the highest of all substances.
- **Low viscosity**
