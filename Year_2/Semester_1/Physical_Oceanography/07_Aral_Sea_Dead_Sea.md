# The Aral Sea and the Dead Seas

## Aral Sea

|                                                          |                                                  |
|----------------------------------------------------------|--------------------------------------------------|
| ![geography](./img/07_Aral_Sea_Dead_Sea/geography.png)   | ![rivers](./img/07_Aral_Sea_Dead_Sea/rivers.png) |

### Some numbers up to 1960

- Area: 67,000 $`km^2`$
- Max depth: 60m
- Runoff: 57 $`km^3`$/year
- Evaporation: 63 $`km^3`$/year
- Diversion (what's used in agriculture): 2
- The diversion increased from 1960 (for cotton production) and started to dry up the sea. The sea level dropped from 60m to 30m.

![comparasion](./img/07_Aral_Sea_Dead_Sea/comparasion.png)

- From 2016 there is a **decrease in evaporation**. This is because of the high level of salinity (the ion molecules block the evaporation).
- The sea was used for fishing but all life has been killed.

## Dead Sea
 
- On the **Rift Valley** (between 2 tectonic plaques - Arabian & Sinai).
- From 1901 **PEF** (indicates the level of the Dead sea) decreased a lot and like the Aral sea the evaporation rate decreased from the high salinity.
- From 1995 the decrease in level is a rate of 1m/year.
- The ion composition is different from the oceanic composition.
  - Sea water dominant ion: **Sodium chloride**
  - Dead sea dominant ion: **Magnesium chloride** (this gives the oily sensation of the water).
- In the below table: 
  - TDS=Total Dissolved Salt
  - DS=Dead Sea
  - End brine = "בריכות אידוי"
- When in regular sea water the TDS is 35, in the Dead sea it is 274.5 (in 1993) and 275 (in 2003).

![ion composition](./img/07_Aral_Sea_Dead_Sea/ion_composition.png)

- Checking the composition on the water cannot be done with traditional methods:
  - Evaporation won't work because some water molecules stay bonded to the ions.
  - CTD cannot be used because it is based on the conservative composition of sea water.
  - The composition of the Dead sea is not stable (because of the sinking of salt) thus new equations cannot be developped.
- In order to work on the Dead sea and compare to regular sea, **density at $`\bold{25\degree C}`$** is used (see above table). This is called **quasi-salinity**.
- Once, pitch (asphalt, זפת) could be found at the surface of the Dead sea and was used for boat construction.
  - In 1980, halites came to saturation and started to sink thus blocking the springs pitch came from.
- Factories take the magnesium and potassium from the the water in pump station (בריכות אידוי) and throw back the water which thus has a different composition.
  - From north to south, from one station to another until the final product is received - **carnallite** (from this magnesium chloride and potassium chloride).
  - The water that has a higher salinity (from the evaporation) than before (end brine) $`\rightarrow`$ 353.4.

### Water budget

- Fresh water inflow: 265-335 $`10^6 m^3`$/year.
  - 1996-2010: drought thus: 60-150 $`10^6 m^3`$/year.
- Evaporation: 1.1-1.2m/year.
- Water deficit: 290 $`10^6 m^3`$/year.
  - 36% due to industrial activities
  - The rest because of the the Yarden river used to make potable water.
- The evaporation rates are derived from the energy budget:
  - Period from $`t_1`$ to $`t_2`$
  - Tw = water temperature: used to compare winter and summer.
  - m = mass of the water in the whole basin (calculated from the volume).
  - q = $`2.25\times10^6 \; JKg^{-1}`$: latent heat of evaporation (חום כמוס)
  - The temperature of the water in winter is the same in the whole water column but in summer calculations and interpolations need to be done.
  - $`\Delta Q`$ = changes in heat from $`t_1`$ to $`t_2`$
  - Since $`\Delta Q`$ is the sum of all the energy fluxes, shortwave radations and longwave radiations can be measured, sensitive heat flux in trivial, then the evaporation can be calculated.
  - $`\Delta m`$ = mass lost due to evaporation

```math
\begin{aligned}
& \Delta Q = Tw(t_2)C_pm(t_2) - Tw(t_1)C_pm(t_2) \\
& Q_{LE} = \Delta Q - (Q_{SW} + Q_{LW} + Q_S) \\
& \Delta m = Q_{LE} \ q
\end{aligned}
```

- To the question "will the Dead sea dry up?", Yechiely researched the water balance and came to the conclusion that in 400 years it will reached a stable state.

### Meaurements in the Dead Sea

- Solar radiations are pretty muche the same as in the Mediterranean cost despite being lower (-400m so expected less radiations).
- Solar radiations and temperature have been pretty stable although the temperature increased a bit.
- Looking at the temperature and humidity, there are the opposite of one another.
- There are two maximums of temperature: one at around 2pm and one at around 7pm.
  - That's because of the brise during the day from the Mediterranean coast which goes up at the Jerusalem hills (the air block gets colder) and goes down in the evening dry and lower than it came from getting warmer than it was $`+10\degree C`$ (adiabatic changes).

![temperature humidity](./img/07_Aral_Sea_Dead_Sea/temp_humi.png)

- The humidity is low and constant.
- The air pressure is higher in winter from being cold thus air is denser.
- The air pressure increases with time since the sea level decreases increasing the atmospheric column.

![air pressure](./img/07_Aral_Sea_Dead_Sea/air_pressure.png)

- **Meromictic**: 
  - No mixing in winter because the salinity in lower in the upper layer, same in summer.
  - Temperature lower at the surface in winter but not in summer.
  - Because there is no mixing in winter, only the upper layer gets colder from the atmosphere.
  - Example: Black sea
- **Holomictic**: 
  - In winter mixing
  - In summer higher salinity in the uppper layer.
  
![mero_holomictic](./img/07_Aral_Sea_Dead_Sea/mero_holomictic.png)

- The Dead sea was meromictic from the fresh water getting in from the Kineret.
  - Now it is holomictic.
  - The overturn happened the first time in 1978-79.
  - $`H_2S`$ went up from the mixing (not a lot though). 
- There were two short meromictic periods: around 80-82 and around 91-92 (because of pinatubo?).

![stability dead sea](./img/07_Aral_Sea_Dead_Sea/stability_dead_sea.png)

- The deep layer average temperature during the meromictic periods did not change a lot.
- During the holomictic periods seasonal and annual changes can be observed due to the mixing with the UMl.
- In the below graph we can see:
  - Until the late 70s the sea was still meromictic, although some data is missing between 1960-1975, 1990-1995 and no deep layer data for 1980. 
  - In meromictic periods the UML temperature is independant of the below layer thus in 1960 the UML is colder than the below layer but the late 70s it is higher.
  - From the early 80s, in winter, the deep layer has the same temperature as the UML (the lighter line doesn't go below the thicker line).
  - There a clear incease in temperature over the years.

![temperature dead sea](./img/07_Aral_Sea_Dead_Sea/temp_dead_sea.png)

- In the below graph can be observed the end of the meromictic period of the early 90s and the layers mixing more and more until 95 where the mixing was total.
  - This happens because the quasi-salinity gets higher from the lack of fresh water influx thus the UML gets heavier each year.
  
![thermocline](./img/07_Aral_Sea_Dead_Sea/thermocline.png)

- The water from the evaporation ponds (end brines), creates a **convection slope** in the summer in the Dead sea because of the high salinity, cause an increase in temperature with depth.
- This cause **staircase double diffusion** (cannot be seen in the picture below).

![convection slope](./img/07_Aral_Sea_Dead_Sea/convection_slope.png)

- In the below graph: 
  - In 2007 the temperature is really low. This is most likely from a flood.
  - The overturn in each winter is clear from the change in temperature getting slower: up the circled area only the UML is getting colder from the atmosphere; under the circle the rate is slower because of the mixing with the below layer (overturn).

![holomictic winter](./img/07_Aral_Sea_Dead_Sea/holomictic_winter.png)

- Sometimes, in winter there are some hotter days which start to create a small UML at the surface. It is easily broken when the sun goes down.
- In Februar the salinity and temperature are the lowest.
- In august the double diffusion can be seen.
- In october, no more staircase.
- Despite the high salinity which should slow the decrease in sea level, there's an acceleration in sea level due to the closing of multiple rivers.

### Sinkholes

- Ground water goes with the ground and is released in the water.
- Sinkholes form because ground water gets in the water at a lower level than before washing the salt on its path creating sinkholes.

![sinkholes](./img/07_Aral_Sea_Dead_Sea/sinkholes.png)

### Solutions

- Pipe from Mediterranean to the Dead sea (not being considered).
- **Peace conduit**: canal from the Red sea to the Dead sea with a desalinization in the middle (end brine goes into the Dead sea).
- **Alternative**: returning the natural order by desalinization in the north, filing with it the Kineret and opening the dam so the water can flow in the Dead sea.

