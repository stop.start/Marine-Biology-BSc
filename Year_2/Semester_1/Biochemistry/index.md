# Biochemistry
_Based on: **Biochemistry (8th edition) - Berg, Tymoczko, Gatto, Stryer**_

1. [Intro](./01_Intro.md)
2. [Proteins](./02_Proteins.md)
3. [Proteins & Proteomes](./03_Proteins_Proteomes.md)
4. [Enzymes](./04_Enzymes.md)
5. [Lipids & Cell Membranes](./05_Lipids_Cell_Membranes.md)
6. [Membrane Transport](./06_Membrane_Transport.md)
7. [Cell Signaling](./07_Cell_Signaling.md)
8. [Intracellular Membrane Traffic](./08_Intracellular_Membrane_Traffic.md)
9. [Apoptosis](./09_Apoptosis.md)
10. [Metabolism](./10_Metabolism.md)
11. [Glycolysis](./11_Glycolysis.md)
12. [Krebs Cycle](./12_Krebs_Cycle.md)
13. [Electron Transport Chain](./13_ETC.md)
14. [Glycogen & Glyconeogenesis](./14_Glycogen.md)
