# Metabolism

- Metabolism occurs in the cytosol. Mitochcondria is the main organelle.
- The inner membrane of the mitchondria has folds called **cristae** that increase the surface area.
- Metabolism is the overall process through which living systems acquire and use free energy to carry out their various functions.
- **Catabolism**: processes that break down macromolecules and usually provides to the cell free energy (ATP).
- **Anabolism**: processes that synthesize needed molecules and usually require energy (ATP).

![metabolism](./img/10_Metabolism/metabolism.png)

- ATP is used as all kind of energy (chemical, mechanical, heat, light/electromagnetic).
- Energy is the capability to do work.
- Energy doesn't disappear just changes state (1st law of thermodynamics).
- **Calorie**: amount of heat required to increase the temperature of 1gr of water 1℃.
- 1cal=4.184J
- Spontaneous reactions $`\rightarrow`$ entropy increases (second law of thermodynamics).
- **Enthalpy 𝚫H**: energy within the chemical bonds.
- **Entropy 𝚫S**: disorder.
- **Free energy 𝚫G**: $`\bold{\Delta G = \Delta H + T\Delta S}`$
- **Endergonic** reaction requires energy to happen. 𝚫G > 0.
- **Exergonic** reaction release energy when it happens. 𝚫G < 0.
- **Activation energy** required to start a chemical reaction.
- Enzymes don't change the chemical reaction (amount of energy freed or used) but lower the activation energy in order to speed up the process.
- $`\Delta G^0`$ in equilibrium.
- 3 criterions of metabolic pathways:
  - Reactions are specific (enzymes are specific).
  - Reactions occur in steps.
  - The sum of all reactions need to be thermodynamically favorable (𝚫G < 0).
- Using other reactions that are thermodynamically favorable can be used to make unfavorable reactions happen.
- The bonds between the phosphates in ATP are called **phosphoanhydrid bonds**.

![atp](./img/10_Metabolism/atp.png)

- In catabolic events, oxidation of a carbonic compounds is the main source of energy required to make ATP.
  - The more C-H bonds there are in a molecule the more oxidation can be done thus fatty acids are a good source (more than sugars).
  
  ![energy bonds](./img/10_Metabolism/energy_bonds1.png)
  ![energy bonds](./img/10_Metabolism/energy_bonds2.png)
  
- Acetyl CoA, NADH, $`NAD^+`$
- NADH can make 3 molecules of ATP.
- $`FADH_2`$ can make 2 molecules of ATP.

![krebs](./img/10_Metabolism/krebs.png)

- Proteins are broken down into amino acids.
- Carbohydrates (פחמימות) are broken down into monomers of glucose.
- Fats are broken down into fatty acids glycerol.
