# Apoptosis

- 2 main roles:
  - Mainenance of tissue size requires that cells die at the same rate as they are produced.
    - During development, orchastrated patterns of cell death help determine the size and shape of limbs and other tissues (like hands and feet).
  - Cells also die when they are infected or damaged beyond repair allowing to keep the organism healthy.
    - Apoptosis eliminates developing T and B lymphocytes that either fail to produce useful antigen-specific receptors or produce receptors that are self reacting.
- Cells that die not randomly but as a programmed process are then eaten by other cells.
- **Apoptosis**: genetically dictated programmed cell death (PCD).
- Cells going through apoptosis, shrink and condense, the cytoskeleton collapses, nuclear envelope disassembles and the chromatin condense and breaks up into fragments.
  - The cell surface breaks up into membrane enclosed fragments called **apoptotic bodies**.
  - The apoptotic bodies surfaces are chemically changed so they can be engulfed by neighboring cells or macrophages.
  - **Phosphatidylserine** (PS) leaves the cell through the membrane which bring **macrophages** to cleanup.
- An excess level of apoptosis is a factor in neurodegenerative diseases and autoimmune disorders, while failure of apoptosis to destroy aberrant cells is a key element of cancer.​
- **Cell necrosis** happen when cells die from trauma or lack of blood supply.
  - Cells going through this process swell and burst. This causes and inflammatory response.
- Reducing the size of a rat liver will cause cell division to regrow the liver, causing the liver to grow more than it should will cause apoptosis to increase to the reduce the size. The liver is kept to its original size.
- Unrepairable DNA damage will cause apoptosis.

![apoptosis vs necrosis](./img/09_Apoptosis/apo_necro.png)

## Laboratory analysis of apoptosis

- 3 assays:
  - Phosphatidylserine exposure
  - DNA fragmentation
  - TUNEL assay

### Phosphatidylserine exposure

-  A universal phenomenon during cellular apoptosis is the loss of phospholipid asymmetry in the cell membrane. 
  - This results in the exposure of **phosphatidylserine (PS)** on the cell membrane surface.
  - **Annexin V** binds to PS exposed on the cell membrane, enabling quantification of apoptotic cells by staining them with fluorochrome-conjugated Annexin V.​
- Annexin V specifically binds to phosphatydilserine located in the outer leaflet of the apoptotic cell membrane​.
- **PI (propidium iodine ?)** is used to mark the nucleus. This is possible after the membrane gets fragmented (so the PI can enter).

![annexin](./img/09_Apoptosis/annexin.png)

### DNA fragmentation

- Together with chromatin condensation, genomic DNA fragmentation is considered a hallmark of the terminal stages of apoptosis. Activation of CAD (caspase-activated DNase) by the caspase cascade leads to specific cleavage of the DNA at the inter-nucleosomal linker sites between the nucleosomes, generating fragments of ~200 base pairs.​
  - ICAD is the CAD inhitor.
- These so called DNA ladders can be visualized by agarose gel electrophoresis. ​

![icad](./img/09_Apoptosis/icad.png)

### TUNEL assay

- Terminal deoxynucleotidyl transferase (TdT) dUTP Nick-End Labeling (TUNEL) assay has been designed to detect apoptotic cells that undergo extensive DNA degradation during the late stages of apoptosis. ​
- The method is based on the ability of TdT to label blunt ends of double-stranded DNA breaks independent of a template​.

## Intrinsic and Extrinsic Pathways

- Apoptosis is triggered by a **family of specialized intracellular proteases**.
  - They cleave specific sequences in numerous proteins inside the cell.
    - Example of target proteins: cytoskeleton & adhesion proteins, nuclear lamins, endonuclease inhibitor iCAD (in order to cut DNA).
  - **CASPAses**: **proteases** that have a **cystein** at their active site and cleave their target proteins at specific **aspartic acids**. 
- 2 types of caspases:
  - **Initiator** caspases
    - They begin the apoptosis process.
    - In their inactive form they are soluble monomers in the cytosol.
    - In the inactive form:
      - **Amino terminus**: adaptor binding domain. The signal (adaptor) protein will bind there.
      - **Carboxy terminus**: protease domain (active site).
    - An apoptic **signal** triggers the assembly of large protein platforms that bring multiple initiator caspases together into large complexes.
    - It activates the executioner caspases.
  - **Executioner** caspases
    - Exist as inactive dimers.
    - Cleaved by an initiator caspase which modifies the active site to an active state.
    - One initiator can activate multiple executioners.
    - The executioners catalyze the protein cleavage events that kill the cell.
- There are two triggers that activate the initiators assembly:
  - External triggers will start the extrinsic pathway.
  - Internal trigger based on the mitochondria will start the intrinsic pathway.
  
### Extrinsic pathway

- Triggerd by proteins binding to cell-surface death receptors.
- Death receptors: extracellular death domain, a single transmembrane domain, an intracellular death domain.
- The receptors are **homotrimers** and belong to the tumor necrosis factor (TNF) receptor family.
- Example extrinsic pathway: **Fas**:
  - Fas ligands (e.g **cytoxic T cell**) binding on the surface of cell trigger the extrinsic pathway of apoptosis by activating the seath domains.
  - The cytosolic tails of Fas death receptors bind intracellular adaptor proteins which then bind initiator caspases forming a **death-inducing signaling complex (DISC)**.
  - Initiator are dimerized and activated in the DISC and induce the apoptosis $`\rightarrow`$ activation and cleavage of **caspase-8**.
  
  ![fas](./img/09_Apoptosis/fas.png)
  
- Many cells produce inhibitory proteins that act to restrain the extrinsic pathway:
  - Some cells produce the protein **FLIP** which resembles an initiator caspase but has no protease activity (it lacks cystein in its active site).
  - FLIP dimerizes with caspase-8 keeping it inactive thus apoptotic signal is blocked.
  - Those mechanisms help prevent the inapropriate activation of the extrinsic pathway of apoptosis.

### Intrinsic pathway or Mitochondrial pathway

- Depends on mitochondria.
- Triggered within the cell.
- Usually in response to DNA damage or in response to developmental signals.
- Mitochondral proteins which are usually in the **intermembrane of the mitochondria** are found in the cytosol which activates a caspase proteolytic cascade in the cytoplasm and lead to apoptosis.
- A key protein in the **cytochrome c** (component of the mitochondrial electron-transport chain).
- When **cytochrome c** is released in the **cytosol** it binds to an adaptor protein called **Apaf1** which then opens and forms an heptamer called **apoptosome**.
- The Apaf1 in the apoptosome recruits the initiator **caspase-9** proteins.
- Caspase-9 then activates the executioners.

![intrinsic pathway](./img/09_Apoptosis/intrinsic.png)
    
- **Bcl2** proteins regulate the intrinsic pathway of apoptosis by controlling the release of cytochrome c and other intermembrane mitochondrial proteins into the cytosol.
- Some Bcl2 proteins promote apoptosis by enhancing the release (pro-apoptosis) and some inhibit apoptosis by blocking the release (anti-apoptosis).

![intrinsic2](./img/09_Apoptosis/intrinsic2.png)

- Pro-apoptosis and anti-apoptosis proteins can bind to each other and inhibit each other.
- **Pro-apoptotic Bcl2** family proteins consist of two subfamilies:
  - The **effector Bcl2** family proteins.
    - **Bax** and **Bak** similar to Bcl2 but lack the BH4 domain.
    - Bak is located on the mitochondrial membrane.
    - Bax is located in the cytosol and binds to the membrane after the apoptosis signal is received.
  - The **BH3-only proteins**.
    - Share sequence homology with Bcl2 only in the BH3 domain.
    
    ![bcl2](./img/09_Apoptosis/bcl2.png)

- Intrinsic pathway is triggered, effector Bcl2 family proteins (like Bax and Bak) become activated and induce the release of cytochrome c and other intermembrane proteins.
- Activation of Bax and Bak usually depends on activated pro-apoptotic BH3-only proteins.
- **Anti-apoptotic Bcl2** family proteins like Bcl2 itself and **$`\bold{BclX_L}`$** are also on the cytosolic surface of the outer mitochondrial membrane.
- They inhibit by binding to pro-apoptotic Bcl2 family proteins either on the mitochondrial membrane or in the cytosol.
  - On the outer mitochondrial membrane they bind to Bak thus preventing the release of cytochrome c and other intermembrane proteins.
- **BH3-only** proteins promote apoptosis by binding to anti-apoptosic Bcl2 family proteins. This allows the aggregation of Bax and Bak on the surface of mitochondria which releases the intermembrane mitochondrial proteins that induce apoptosis.
- BH3-only proteins are a crucial link between apoptotic stimuli and the intrinsic pathway of apoptosis:
  - Extracellular survival signals will block apoptosis by inhibiting the synthesis or activity of BH3-only proteins.
  - In response to DNA damage that cannot be repaired the tumor suppressor protein **p53** accumulates and activates the transcription of BH3-only proteins genes **Puma** and **Noxa** which then trigger the intrinsic pathway.
- The extrinsic pathway can recruit the intrinsic pathway through the BH3-only protein called **Bid**.
- Bid inhibits anti-apoptotic proteins on the outer mitochondrial membrane which amplifies death signal.

![mitochondria](./img/09_Apoptosis/mitochondria.png)

## Controlling Apoptosis

- **IAPs (inhibitors of apoptosis)** proteins help control caspases.
- First discovered in insect viruses that encoded for IAP which prevent an infected host cell from killing itself.
- IAPs have one or more BIR (baculovirus IAP repeat) domains which allow them to bind to and inhibit activated caspase-3.
- Some IAPs cause the **ubiquitylation of caspases**: the caspases are tagged to be degraded (and thus prevent apoptosis).
- There are also proteins that are **anti IAPs** called **Hid** that are released from the mitochondria.
- In Drosophila, IAPs can be neutralized by anti-IAP proteins that bind to the BIR domain of IAPs preventing the domain form binding to a caspase.
- In animal cells, intercellular signals regulate most activities (example Fas ligand).
- **Survival factors** are signals that inhibit apoptosis.
- In animal cells, cells required continuous signaling from other cells to avoid apoptosis.
- Survival factors bind to cell-surface receptors which activate intracellular signaling pathways that suppress the apoptotic program (by regulating Bcl2 family proteins).
- 3 ways that extracellular survival factors can inhibit apoptosis:
  - Increase production of anti-apoptosic Bcl2 proteins
  - Inactivation of pro-apoptotic BH3-only proteins (**AKT kinase**)
  - Inactivation of anti-IAPs

## Removing Apoptotic Cell

- Apoptotic fragments do not break open (which would cause an inflammatory reaction) and are efficiently eaten.
- The apoptotic cell's surface changes to display signals that recruit phagocytic cells.
- Phospholipid bilayer flips (?).

## Apoptosis and Diseases

- Excess or insufficient apoptosis can contribute to diseases.
- Heart attacks and strokes: excessive apoptosis.
- Decrease in apoptosis contributes to tumors.
- P53 is mutated in almost 50% all human cancer.
- Given the no apoptosis in cancer cell, due to mutated P53 means that their tumor cells​ can divide and proliferate even with DNA damage​.
- Many cancer drugs induce apoptosis by a P53 dependent mechanism.
