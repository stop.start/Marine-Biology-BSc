# Intro

## The liver
- The legend of Prometheus that stole the fire and was punished by Zeus to have an eagle eats bits of his liver every day.
- The liver **regenerates**.
- The liver builds from several parts (**עונות**).
- When a part of the liver is surgically removed, it doesn't grow back but the other parts grow bigger to take its place.
- If a part grows too much then other part will undergo apoptosis to keep the liver stable.
- Unless, there's a need for growing/reducing parts, the liver cells do not undergo cell division/apoptosis a lot.

## Circadian clock

- Hall, Rosbach and Young received the Nobel price for their discovery of the circadian clock (2017).
- Some genes are expressed depending on the day/night cycle.
- **Melatonin** is an hormone secreted at night.
  - It allows us to sleep.
- During the day **cortisol** (a steroid) is secreted.
- The **PER protein**:
  - Increases during the night and decreases (is degraded) during the day.
  - The mRNA exits the nucleus, is translated to the PER protein, the PER protein goes back into the nucleus (when binded to TIM).
  - **Feedback inhibition**: the PER protein builds up inside the nucleus **at night** which at some point (when the day comes) inhibits the expression of the gene.
- The **TIM protein**:
  - It forms a complex with PER allowing it to get back into the nucleus and sit on the regulating area of the gene to prevent its expression.
- The gene **dbt** is responsible for the enzyme kinase that phosphorylates PER in order to degrade it.
- Blue light activates the **cry gene** that degrades TIM.
- The **eye** sends signals to the **hypothalamus** area of the brain.
  - The **SCN** area (suprachiasmatic nucleus) in the hypothalamus sends signals in a brain area controlling hormones, body temperature and other functions that affect our sleepy/awake state.
  - **Fluorescent light** can make our body think it is daylight.
- At night we secrete **cytokines**.

## Intro

- Biochemistry is the study of life processes.
- In 1828, it was discovered that biological molecules could be synthesized from nonliving components (urea).
- With the creation of the microscope, it was discovered that organisms are made of cells which suggested that all organisms (from big to microscopic) have a lot in common.
- Biochemistry is focused on 2 types of molecules:
  - **Biological molecules** which are large molecules like _proteins_ and _nucleic acids_.
  - **Metabolites** low-molecular-weight molecules like _glucose_ and _glycerol_.
- Those molecules are common in all organisms:
  - DNA is present in all organisms.
  - Proteins are made of the same 20 amino acids.
  - Some proteins with similar roles have similar 3D shape in different organisms (example: tata box protein has a similar shape in 3 different organisms).
- Some processes, like the conversion of glucose and oxygen into $`CO_2`$ and water, are present in many organisms from simple bacteria to human beings.
- All of this suggest that all living things have a common ancestor.
- The 3 domains of life, Eukarya, Bacteria and Archaea are differenciated based on biochemical properties.
- Bacteria and Archaea were one domain until Carl Woese discovered biochemical differences in 1977. 

## DNA

- 1940: discovery that DNA is the carrier of genetic information.
- 1953: proposal for the structure of DNA.
- Rosalind Franklin captured the first image of the structure of DNA called _Photograph 51_.
- The structure of DNA makes its function very efficient.

### Hershey-Chase experiment: DNA is the genetic material

- [See here](https://gitlab.com/stop.start/Marine-Biology-BSc/-/blob/master/Year_2/Semester_1/Genetics/09_DNA.md#hershey-chase-experiment)

### Structure of DNA

- Backbone monomers made of sugar (deoxirobose) and phosphate.
- Sugars are oriented the same way giving each strand its directionality.
- Each sugar is bonded to one of 4 bases: [A]denine, [G]uanine (purines); [C]ytosine, [T]hymine (pyrimidines).
- Purines and pyrimidines are found in other molecules as well, not only in DNA.
- The 2 DNA strands are connected by their bases: either A-T or C-G. Those are **hydrogen bonds**.
- The advantage of hydrogen bonds is that they are weak enough to be broken when needed but strong enough to hold the double helix.

![dna structure](./img/01_Intro/dna_structure.png)

## Concepts from chemistry

> Given two synthetically created DNA strands, when put in the same solution they will form a double helix.  
> Why and how do they automatically bind to each other?  

### Bonds

- **Covalent bonds** are the strongest. Atoms bonded via covalent bonds share pairs of electrons.
  - The energy is given in kJ or kcal.
- **Resonance** are alternative arrangements of single and double bonds in a molecule. Molecules with resonance structure are more stable.
- **Noncovalent bonds**:
  - Ionic structure
  - Hydrogen bonds
  - Van der Waals interactions
  - Hydrophobic interactions

#### Water

- Most common solvent in biochemical reactions.
- Essential for macromolecules formation.
- Two properties:
  - **Polar molecule**: 
  - **Highly cohesive**
  
#### Ionic interactions 
- Part of a molecule that is charged will attract another part of a molecule (can be itslef) that is oppositely charged.
- The energy is given in Coulomb energy:
  - $`\bold{q_1}`$ and $`\bold{q_2}`$: charges of the atoms
  - **r**: distance between the atoms
  - **D**: dielectric constant
  - **k**: proportionality constant
    
  ```math
  \boxed{E = \frac{kq_1q_2}{Dr}}
  ```
- Attractive interactions have a **negative energy**.
- The dielectric constant is higher the more it affects the bonds formation. 
  - For example, a polar solution (like water) will have a higher effect.
  - The dielectrice constant of:
    - Water is 80.
    - Organic solution is 2.
    - Vaccum is 1.

#### Hydrogen bonds

- **Hydrogen bond donor**: group with the hydrogen.
- **Hydrogen bond acceptor**: atom linked to the hydrogen.
- A lot weaker and longer than covalent bond (range 4-20 kJ/mol).
- The stronger bonds tend to be straight.

#### Van der Waals interactions

- The electrons are always moving causing the charge of a molecule to not be symmetric sometimes which makes it kind of polar.
- When this happens atoms/molecules are attracted to each other like ionic ionic interactions (+ and - attract).
- The energy of this type of bonds range 2-4 kJ/mol.

#### Hydrophobic interactions

- Non polar molecules do not participate in hydrogen bonds or ionic bonds.
- Water molecules form cages around non-polar molecules by forming hydrogen bonds between them thus they are more well ordered (entropically unfavorable).
- **Hydrophobic effect** when in water, they will aggregate (not bond), reducing the number of water molecules around them thus it is less entropically unfavorable.
- When two hydrophobic molecules come together, a **water molecule is freed**.

![hydrophobic effect](./img/01_Intro/hydro_effect1.png)
![hydrophobic effect](./img/01_Intro/hydro_effect2.png)

## Bonding of two strands of DNA

- The double helix is formed with the help of ions, van der Walls bonds and the hydrophobic effect.
- **The phosphate** group has a negative charge thus repulse one another despite being **10 $`\textbf{\AA}`$**(unfavorable ionic interactions).
- The negative charge of phosphate is partly neutralize by the **high dielectric constant of water** and by the **cations** present in the solution. In water ions Mg and Na neutralize the negative charge of phosphate.
- Single DNA strands in a water solution will form **hydrogen bonds with water**. Those bonds will be broken when the **complementary bases** come together. Non complementary bases will not be able to brake all the hydrogen bonds made with water.
- The **number of bonds broken equals the number of bonds created**.
- Base pairs of DNA are **3.4 $`\textbf{\AA}`$** apart and are parallel.
- The hydrophobic effect helps with the stacking.
- When the 2 strands bonds **60kCal/mol** is released.

## Thermodynamic laws

- Thermodynamic laws determine the conditions under which a specific process can or cannot take place.

### First law of thermodynamics

- **The total energy of a system and its surroundings is constant.**
- Energy cannot be neither created nor destroyed.
- Energy can be in different forms like heat and potential energy.

### Second law of thermodynamics

- **The total entropy of a system plus that of its surroundings always increases**.
- **Entropy** is a measure of randomness in a system.
- Local decrease in entropy is usually accompagnied by the release of heat increasing the entropy of the surroundings.

![local order](./img/01_Intro/local_order.png)

- When a double helix forms from 2 single strands (resulting in more order), heat is released (around 250 kJ/mol) increasing the entropy.

## Acids/Bases

- Lots of biochemical processes requires **covalent bonds** to be created or broken.
- For example, in **acid/base reaction $`H^+`$ is either added or removed**.
- **Acids** will add protons in a solution.
- **Bases** will remove protons in a solution.
- **pH is the concentration of $`\bold{H^+}`$ ($`H_3O^+`$) in the solution**.

```math
\boxed{pH = -\log{[H^+]}}
```

- The pH of water is 7 which means: $`7 = -\log{10^{-7}}`$
- In a solution with a pH equal or higher than 9, the double helix of DNA start to separate. This is because the high concentration of $`OH-`$ react with the base pairs and remove protons.
- In a solution with a pH equal or lower than 5, the double helix of DNA start to separate as well. This is because the high concentration of $`H^+`$ protonate the base pairs which can no longer form an hydrogen bond.
- When $`pH = pK_a`$ the protonated form of the molecule is equal to the deprotonated form.

### Buffers

- As seen above, keeping a certain pH is important for biologic processes and molecules.
- Buffers allow to keep solutions at a certain pH by absorbing or releasing protons.
