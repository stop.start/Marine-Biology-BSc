# Membrane Transport

- The barrier function of membrane (lipid bilayer) is to maintain concentration of solute, ingest essential nutrients, excrete metabolic waste and regulate ion concentration.
- In a protein-free membrane and given enough time every molecule will diffuse across its concentration gradient:
  - Diffusion across membrane depends on the molecule **size** and on its solubility in oil (lipids).
  - $`O_2`$ and $`CO_2`$ are small nonpolar molecules thus can diffuse rapidely across the membrane. Small polar molecules like water or urea also diffuse across the lipid bilayer but much more slower.
  
![permeability](./img/06_Membrane_Transport/permeability.png)

- Transport proteins make up 15-30% of the membrane proteins in all cells.
- All known membrane tranport proteins are **multipass transmembrane proteins**.
- Two main classes of membrane transport proteins:
  - **Transporters** bind to the solute and undergo sequential conformation changes to transport the solute across the membrane. Transport can be active or passive.
  - **Channels** form narrow pores which allow passive transmembrane transport (water and inorganic ions).
    - Channels that allow the passage of water are called **aquaporins**.
- Transport by channels is much faster than by transporters.
- The specificity of tranport proteins was shown in 1950 by studying a bacteria with a single gene mutation which couldn't transport sugars across the membrane.
  - In humans cystinuria is a disease in which patients cannot transport cysteine from urine or instestine to blood. This results in cysteine stone in kidneys.
- **Ion concentration gradient**: difference in concentration on two sides of the membrane. The potential energy drives transport. It is used is processes like ATP production.

![transport proteins](./img/06_Membrane_Transport/transport_proteins.png)

- Channels are a form or **passive transport**. Uncharge molecules will go through channels down their **concentration gradient** (downhill).
- Charge molecules are affected by their concentration gradient as well as the **electrical potential difference** across the membrane. 
  - The force driving this kind of molecules is called **electrochemical gradient** (electrochemical gradient = concentration gradient + electrical gradient​).
- Usually, plasma membranes have an electrical potential (voltage): **negative inside**.
  - This favorizes the **entry of positively charged** ions and opposes the entry of negatively charged ions.
- Active transport allow molecules to go "uphill": pumps coupled with a source of energy like ions gradient or ATP hydrolysis.

## Active transporters

- Each transporter has one or more specific binding sites for its solute (substrate).
- Transfer the solute across the membrane by undergoing reversible conformational changes. At some point the solute is separated from both sides of the membrane.
- The rate of transport is said to be maximal $`\bold{V_{max}}`$ when all binding sites are occupied (the transporter is said to be saturated).
- Diffusion doesn't have a $`V_{max}`$.
- $`V_{max}`$ measures the rate at which the carrier can flip between conformations.
- $`K_m`$ is the affinity of a transporter for its solute and is equal to the concentration of solute when the transport is half its maximum rate.

![km](./img/06_Membrane_Transport/km.png)

- Binding of solute can be blocked by either competitive inhibitors (which may or may not be transported) and noncompetitive inhibitors.
- Transition between states occurs randomly​.
- Does not depend on whether binding site is occupied​.
- Transport is driven by concentration​.
- **Uniporters** transport the molecule depending on its $`V_{max}`$ and $`K_m`$.
- **Symporters** and **antiporters** are transports couples with the transport of a second molecule (same direction and opposite direction respectively). This uses the electrochemical gradient of one solute to power the transport of another solute.
- **Primary active transports** are transports driven by ATP.
- **Secondary active transports** are transports powered by other molecules going down their grandient.
- Another type of active transport is **light driven pumps**.

![porters](./img/06_Membrane_Transport/porters.png)


- $`Na^+`$ is usually the co-transported ion, bring with it into the cell sugars or amino acids.
- Its gradient: from outside the cell, inside.
- The $`Na^+/K^+`$ pump along with ATP allows $`Na^+`$ to get out of the cell against it concentration gradient.
- A lower concentration of $`Na^+`$ means less transport.
- The binding of $`Na^+`$ and glucose is **cooperative** meaning that binding either of them causes conformational changes that increase the affinity for the other.
  - When the symporter opens inside the cell, due to the low concentration of $`Na^+`$ it will dissociate quickly, this causes the glucose to dissociate because of the cooperative effect.

![na+ symporter](./img/06_Membrane_Transport/na_symporter.png)

- **Neurotransmitters**, after they were released out of the cell, are taken by  $`Na^+`$ symporter pumps into the target cell (?).
  - Neurotransmitters signal at synapses. In order to prolong this signaling, drugs like cocaine and antidepressants **inhibit** those pumps thus the neurotransmitters are not cleared efficiently.
  
  ![neurotransmitters](./img/06_Membrane_Transport/neurotransmitters.png)
  
- Transporters can work in reverse direction with adjusted ion and solute gradient (this is done experimently).
- In bacteria, yeast, plants and many membrane-enclosed organelles of animal cells, most ion-driven active transport systems depend on $`H^+`$ grandient rather than $`Na^+`$.
  - In bacteria, sugars and amino acids is driven by $`H^+`$ grandient.
- The transporters are built from bundles of 10 or more $`\alpha`$ helices in the membrane.
  - The binding sites are located midway through the membrane.
  - In those sites, some helices are broken or distorted and amino acids side chains can form binding sites.

## pH

- Most proteins operate optimally at a certain pH thus the cell has to control the pH of intracellular compartments.
- Cytosolic enzymes function best at a pH around 7.2 whereas in lysosomes the pH is at around 5.
- Different type of $`Na^+`$ antiporters in the plasma membrane help maintaning the pH at 7.2.
  - $`H^+`$ present in the cell either leaked into it or was produced there.
  - The $`Na^+`$ transporters pump out the excess of $`H^+`$.
- There are 2 ways to regulate $`H^+`$ excess:
  - **$`\bold{H^+}`$ is directly transported outside**. Several transporters:
    - One couples the **influx of $`\bold{Na^+}`$** with the **efflux of $`\bold{H^+}`$** $`\rightarrow`$ $`Na^+-H^+`$ exchanger.
    - Another one that couples the **influx of $`\bold{Na^+}`$** and $`\bold{HCO_3^-}`$ with the **efflux of $`\bold{H^+}`$** and $`Cl^-`$ $`\rightarrow`$ $`Na^+`$ driven $`Cl^--HCO_3^-`$ exchanger. It is more effective than the previous one since that for each $`H^+`$ it gets out, it neutralizes another one with the $`HCO_3^-`$.
  - **$`\bold{HCO_3^-}`$ (bicarbonate) is brought into the cell to neutralize $`\bold{H^+}`$**: $`HCO_3^- + H^+ \rightleftharpoons H_2O + CO_2`$
- There's also a pump to lower the pH if the cytosol becomes too alkaline. With this pump, $`HCO_3^-`$ go out of the cell down its electrochemical gradient.
- In addition to those secondary active transports, the excess of $`H^+`$ is also directly regulated by **ATP-driven pumps**. Those are used to control the pH of many intracellular compartments.
  
## Transcellular Transport

- Epithelial tissues line the outer surfaces of organs and blood vessels throughout the body, as well as the inner surfaces of cavities in many internal organs.
- Epithelial cells (e.g those that absorb nutrients from the gut) have tranporters distributed in the plasma membrane and contribute to the **transcellular transport** of absorbed solutes.
  - This allow to transport solute across the epithelial cells into the extracellular fluid and then passed into the blood.
  
  ![epithelial cell](./img/06_Membrane_Transport/epithelial.png)

## ATP driven pumps

- ATP driven pump are called **ATPases**.
- They hydrolyze ATP to ADP and phosphate and use the energy released to pump ions or other solutes across the membrane.
- 3 main classes:
  - **P-type pumps**
  - **ABC transporters** (ATP-Binding Cassette transporters)
  - **V-type pumps**

| **P-type pumps**                                                | **ABC transporters**                              | **V-type pumps**                                    |
|-----------------------------------------------------------------|---------------------------------------------------|-----------------------------------------------------|
| Multipass transmembrane proteins                                | Mainly pump small molecules across cell membranes | Turbin like protein machines                        |
| Phosphorylate themselves durin the pumping cycle                |                                                   | Composed of several subunits                        |
| Includes many of the ion pumps like $`Na^+, K^+, H^+, Ca^{2+}`$ |                                                   | **Transfer $`H^+`$ into organelles like lysosomes** |


- **F-type** pump is structurally related to V-type pump.
  - They are called **ATP synthases** because they work in reverse by using the $`H^+`$ gradient across the membrane to drive the synthesis of ATP.
  - They are found in:
    - **Plasma membranes of bacteria**.
    - **Inner membrane of mitochondria**.
    - **Thylakoid membrane of chloroplasts**
    
![atpases](./img/06_Membrane_Transport/atpases.png)

### P-type Pump

- P-type pumps structure: 10 transmembrane $`\alpha`$ helices connected to 3 cytosolic domains.

#### $`Ca{2+}`$ Pump

- Pumps $`Ca^{2+}`$ into the sarcoplasmic reticulum in muscle cells.
- The cell contain a very low concentration of $`Ca^{2+}`$ in the cytosol.
- This low concentration is what allow to transmit extracellular signals rapidly (by the flow of $`Ca^{2+}`$ down their steep concentration gradient).
- Two pumps help maintaning the $`Ca^{2+}`$ gradient:
  - **$`\bold{Ca^{2+}}`$ ATPase (P-type)** .
  - **$`\bold{Na^+-Ca^{2+}}`$ exchanger (antiporter)**.
- In the muscle cells, the sarcoplasmic reticulum (SR) serves as an intracellular store for $`Ca^{2+}`$.

![p-type](./img/06_Membrane_Transport/ptype.png)

- $`Ca^{2+}`$ pump: amino acid side chains protrude from the transmembrane helices form 2 binding sites for the ions.
  - The binding sites are accessible only from within the cell.
  - When two ions bind there's a change in conformation and the protein closes.
  - There's a phosphotransfer from ATP to an aspartate (pretty much in all P-types).
  - ADP is replaced with a new ATP. This causes a new conformational change.
  - The protein opens an the ions exit.
  
![ca pump](./img/06_Membrane_Transport/ca_pump.png)

#### Sodium-Potassium Pumps

- The concentration of $`K^+`$ is 10-30 times higher in the cell than outside. $`Na^+`$ is the opposite.
- All animal cells have a $`Na^+-K^+`$ ATPase.
- ATP driven antiporter that pump $`3 Na^+`$ out and $`2 K^+`$ in.
- In animal cells, $`Na^+`$ grandient drives the tranport of most nutrients and regulates cytosolic pH.
  - It is **electrogenic**: it drives a net electric current across the membrane creating an electrical potential: negative inside the cell, positive outside. 
  - This actually isn't the main contribution to the membrane potential.
  
  ![na-k pump](./img/06_Membrane_Transport/na_k_pump.png)
  
- Ouabain – inhibits Na,K,ATPase.​
- Pump out Na by the Na, K, ATPase is crucial in reducing ​the osmolarity inside the cell and works nonstop.​
  - The issue: a cell that does nothing to control its osmolarity will have a higher concentration of solutes inside than outside. As a result, water will be higher in concentration outside the cell than inside. This difference in water concentration will cause water to move continuously into the cell by osmosis. ​
- ppt6/slides 41-43.
