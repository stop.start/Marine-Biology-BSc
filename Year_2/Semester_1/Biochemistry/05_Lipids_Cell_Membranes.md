# Lipids and Cell Membranes

- Plasma membrane = cell membrane
- Oraganelles also have membranes.
- Membranes are made of lipids (which are hydrophobic) with proteins within that allow to connect two hydrophilic environments.
- Common features of biological membranes: 
  - Sheetlike structure. Most membranes have a thickness between $`60\text{\AA}`$ and $`100\text{\AA}`$.
  - Mainly made of lipids and proteins. Also some carbohydrates linked to lipids and proteins.
  - Membrane lipids have both hydrophilic and hydrophobic parts. They spontaneously form closed bimolecular sheets in aqueous environments.
  - Specific proteins mediate distinctive functions of membranes.
  - They nonconvalent assemblies: lipids and proteins are all bonded by nonconvalent interactions.
  - They are asymmetric
  - They are a fluid structure.
  - Electrically polarized: the inside is negativelly charged (usually 60mV).
  - Membrane is associated with cytoskeletal proteins.
  - Senses signals through receptors.
  
![cell membrane](./img/05_Lipids_Cell_Membranes/cell_membrane.png)

## Lipids

### Fatty Acids

- Hydrophobic part of the lipids.
- Fatty acids are long hydrocarbon chains (saturated or unsaturated) with a carboxylic acid group at the end.

![fatty acid](./img/05_Lipids_Cell_Membranes/fatty_acids.png)

- Fatty acids are called by the name of the hydrocarbon but ending with _-oic acid_. _Example: $`C_{18}`$ is called octadecane, its fatty acid is called octadecenoic acid_.
- The notation _18:0_ means that the fatty acid is saturated (no double bonds) while _18:2_ means that it has 2 double bonds.

![fatty acids examples](./img/05_Lipids_Cell_Membranes/fatty_acids2.png)

- Numbering/naming rules:
  - Fatty acids are numbered starting at the carboxyl terminus.
  - Carbon 2 and 3 are called $`\alpha`$ and $`\beta`$.
  - The last carbon in the chain is called $`\omega`$.
  - The position of a double bond is represented as follow: $`cis-\Delta^9`$ (cis double bond between carbons 9 and 10); $`trans-\Delta^2`$ (trans double bond between carbons 2 and 3).
  - A double bond can also be numbered from the $`\omega`$ carbon and written as $`\omega-3`$ (double bond between the third and fourth carbons from the end).

![fatty_acids_rules](./img/05_Lipids_Cell_Membranes/fatty_acids_rules.png)

- In most unsaturated fatty acids the configuration of the double is **cis**.
- At physiological pH fatty acids are ionized.
- In biological system, fatty acids contain an even # of carbons typically between 14-24.​
- The 16 and 18 fatty acids are most common.
- Double bonds are usually separated by at least one methylene group ($`CH_2 \rightarrow`$ two single bonds).
- Unsaturated fatty acids melting point are lower than saturated (saturated ones are closer, more "stacked").
- Shorter chains have a lower melting point than longer ones.
- **Shorter chains and unsaturation increase the fluidity of fatty acids**.

### Membranes

- Lipids are insoluble in water but soluble in **organic solvents**.
- Lipids in membranes are **amphipatic**: they contain both hydrophilic and hydrophobic parts.
- **Biological roles**:
  - Fuel molecules
  - Energy stores
  - Signal molecules
  - Components of membranes
- **3 major kinds of membrane lipids**:
  - Phospholipids
  - Glycolipids
  - Cholesterol
  
#### Phospholipids

- In all biological membrane
- Components: one or more fatty acids, a platform (what the fatty acid is attached to), a phosphate, an alcohol attached to the phosphate.
- Hydrophobic tail and hydrophilic head.

![phospholipid schema](./img/05_Lipids_Cell_Membranes/phospholipid_schema.png)

- The **platform** can be **glycerol** or **sphingosine**.
- Glycerol phospholipids are called **phosphoglycerides** $`\rightarrow`$ glycerol + 2 fatty acids + phosphorylated alcohol.

![glycerol](./img/05_Lipids_Cell_Membranes/glycerol.png)
  
- **Phosphoglyceride** or **phosphatidate** is an intermediate state and only a small amount is found in membranes.
  - It contains 2 fatty acids and an esterified phosphoric acid (=bonded via esteric bond).
- Major phosphoglycerides are derived from phosphatidate by the formation of an ester bond between phosphate and OH of various alcohols​.

![phosphatidate](./img/05_Lipids_Cell_Membranes/phosphatidate.png)

- **Ester bond**: between a carbon and an oxygen.
- **Sphingomeyelin** is a phospholipid found in membranes derived from sphingosine (and not glycerol).

![phosphoglycerides vs sphingosine](./img/05_Lipids_Cell_Membranes/phospholipids_types.png)

#### Glycolipids

- **Contain sugar**.
- In animal cells, it is derived from **sphingosine** (like sphingomeyeling).
- Glycolipids differ from sphingomyelin by the group attached to the OH, (one or more sugars are attached)​.
- **Cerebroside** is the simplest glycolipid. It contains a single sugar either glucose or galactose.
- **Gangliosides** are more complex and can contain up to 7 sugars.
- **Glycolipids are always on the extracellular side of the membrane**.

#### Cholesterol

- **Lipid based on a steroid nucleus**.
- Different structure from that of phospholipids: **steroid built from four linked hydrocarbon rings**.
  - The rings cause some level of toughness.

![cholesterol](./img/05_Lipids_Cell_Membranes/cholesterol.png)
![cholesterol](./img/05_Lipids_Cell_Membranes/cholesterol2.png)

- Parallel to the fatty acid chains of the phospholipids and the hydroxyl group interacts with the nearby phospholipid heads.
- Cholesterol increases the **permeability** of the membrane: its hydroxyl group OH interacts with the polar heads of the phospholipids.
  - Cholesterol does not make the membrane less fluid!
- Not found in prokaryotes.
- Constitutes 25% of the membrane lipids in nerve cells.
- Not found in intracellular membranes.

### Properties in aqueous environments

- Phospholipids form membranes because they are amphipatic: hydrophilic heads toward water and the hydrophobic tails turn to each other.
- **Micelle** globular form.

![micelle](./img/05_Lipids_Cell_Membranes/micelle.png)

- Lipid bilayer: two lipid sheets. Also called bimolecular sheet.
- The **favored structure** for phospholipids and glycolipids in aqueous solutions is a bimolecular sheet (and not micelle).
  - The two fatty acid chains are too big for the micelle.
  - Salts of fatty acids which contain only one fatty acid readily form miccelle​.
- The phospholipid bilayer form spontaneously in water where the hydrophobic interactions are the main drive of the formation.
  - Water molecules are released from the hydrocarbon tauls of membrane lipids.
  - Van der Waals forces between the tails favor close packing of the tails.
  - Between the heads: electrostatic and hydrogen bonding.
- Lipid bilayers are **self sealing** because holes are energetically unfavorable.
- **Liposomes** are vesicles from phospholipid bilayers.
  - Average diameter 50nm.
- Sonicating phospholipids allow the formation of liposomes which can trapped ions or molecules present in the solution.
  - Way to test permeability to certain substance (test how much escape the vesicle).
  - Specific proteins can be added.
- Liposomes can be used to deliver drugs directly into the cells which less its toxicity.
- Despite the fluidity of the membrane, liposomes do not fuse​ with one another spontaneously when suspended in water because the heads bind to water and need to be displaced to allow the fusion.
- The **hydration shell** that keeps liposomes apart also insulate the​ many internal membranes in eukaryotic cell and prevent their ​uncontrolled fusion. ​
- The fluidity of a lipid depends on both its composition and temperature:
  - Lowering the temperature can tranform the layer into a gel state.
  - Layers with more shorter chains or with cis-double bonds are harder to freeze because they don't pack as well as long straight chains.
- **Planer bilayer** allows measurements of permeability​ and conductance of lipid bilayer. ​

![planer bilayer](./img/05_Lipids_Cell_Membranes/planer_bilayer.png)

![phospholipids structures](./img/05_Lipids_Cell_Membranes/phospholipids_structures.png)

- Lipid bilayers are impermeable to ions and most polar molecules.
- One exception is water which goes through the membranes quite easily because of:
  - Low molecular weight
  - High concentration
  - Lack of complete charge
- Using a lipid molecule with a fluorescent dye or gold particle attached to its polar head group allow to follow the diffusion of molecules in a membrane.
  - Another option is to modify a lipid head group to carry a **spin label** like nitroxide group **=N-O**.
  - This group contains an unpaired electron whose spin creates a **paramagnetic signal**.
  - This signal can be detected by **ESR** (electron spin resonance).
- **Flip-flop**: lipid that goes from one layer of the bilayer to the other (on the other side): takes hours (slow) for a lipid molecule to flip-flop.
  - Cholesterol flip-flop rapidely (it is the only exception).
- Lipids move rapidely within their monolayer. This allow for **rapid lateral diffusion**.
- Phospholipids are produced in only one monolayer of a membrane: **cytosolic monolayer of the ER membrane**.
  - Major phospholipids made in the ER: **phosphatidylcholine (=lectin)**, phosphatidylethanolamine, phosphatidylserine.
  - Minor phospholipids made in the ER: phosphatidylinositol 
- In synthetic bilayers, lipids are stuck in their monolayer which is an issue when new lipids are formed in ER. This is solved by using membrane **translocators** proteins called **flippase** and **scrambelase** which catalyze the flip-flop.
  - **Flippase**: specific, fueled by ATP flips phosphatidylserine and phosphatidylethanolamine (free amino groups in their head) to cytosolic leaflet.
  - **Scrambelase**: activated only in **apoptosis**, flips phosphatidylserine to the surface of the cell. Non specific $`\rightarrow`$ present in both ER.

![translocators](./img/05_Lipids_Cell_Membranes/translocators.png)

- In the below table we can see that bacterial plasma membranes are usually composed of one main type of phospholipid and doesn't contain cholesterol.

![lipid composition in membranes](./img/05_Lipids_Cell_Membranes/membranes_lipids.png)

- **Lipid rafts**: specialized domains where lipids segregate. They are formed with cholesterol and certain lipids (sphingomyelin) and glycolipids.
- Some cytosolic enzymes bind to specific lipid groups exposed on the cytosolic face of a membrane.​
  - Activation of receptor leads to increased phosphorylation of phospholipids (=phospholipids are added) which serve as docking site for specific signaling protein​.

![signaling complex](./img/05_Lipids_Cell_Membranes/signaling.png)

- If the viscosity of the membrane is too high, enzymatic activity ceases.
- The fluidity depends on composition as well as temperature.
- **Lipid droplets** are stored lipids that cells can used to retreive lipids for membrane or food.
  - Lipid droplets are surrounded by a single layer of phospholipids.
  - Some cells contain one big lipid droplet, while others have smaller ones.
  - Fatty acids can be released from the lipid droplet and exported to other cells through bloodstream.
  - Lipid droplets store **neutral lipids** (tricylglycerol, cholesterol ester) synthesized in the E.R membrane.

![lipid droplet](./img/05_Lipids_Cell_Membranes/lipid_droplet.png)

- Lipid bilayer membranes have a very **low permeability for ions and most polar molecules**. 
  - Water being an exception because of its small size, high concentration and because it doesn't have a complete charge.
  - For a small molecule to traverse the membrane, it needs to first remove the "shell" of water and then dissolve in the hydrocarbon core (non polar part of the the lipid bilayer) and come out on the other side.
  - $`Na^+`$ traverses membrane very slowly because replacing the shell of water by nonpolar interactions is very **unfavorable energetically**.

## Proteins

- Some proteins function only in the presence of specific phospholipids head groups.
- Lipids form a permeability barrier while proteins are responsible for almost all other function.
- Proteins transport chemicals and information across a membrane.
- Membranes have different protein compositions depending on their roles:
  - **Myelin** membrane serves as an electrical insulator around nerve fibers and has a low content of proteins because lipids are good at insulating. Most other cells have a lot more proteins.
- Most cells membrane content is about 50% proteins.
- Internal membrane of mitochondria and chloroplasts have a content of 75% proteins.
- The proteins components of a membrane can be visualized by SDS-polyacrylamide gel electrophoresis which depend on the mass of the proteins.
- Some proteins are more tightly embeded in the membranes than others:
  - Some can be easily extracted with a solution of high ionic strength. Those proteins are **peripheral** thus are usually linked to the membrane by electrostatic and hydrogen bonds which can be disrupted by adding salt or changing the pH.
  - Other can be solubized only by using a detergent or an organic solvent. Those proteins are **integral** thus have much more interactions with the bilayer core.
- **Membrane proteins are amphiphilic: hydrophobic and hydrophilic parts**.
- Proteins can associate in different ways with the membrane:
  - **Transmembrane proteins**: hydrophobic region interact with the hydrophobic tail of the lipids within the bilayer and the hydrophilic region are exposed to water outside the membrane on each side (1,2,3 in the picture below).
    - Some are attached with covalent bonds to fatty acids into the cytosolic monolayer (1 in the picture below).
  - Some proteins are **entirely in the cytosol** and are attached to the cytosolic monolayer either by:
    - Amphilic $`\alpha`$ helix (4 in the picture below).
    - One or more convalently attached lipid chains (5 in the picture below).
  - Other proteins are **entirely outside of the cell** and are attached by convalent linkage to a **lipid anchor** (6 in the picture below).
    - Those are finished to be synthesized inside the ER lumen.
    - They get into the ER with the help of the **signal sequence** on their carboxylic end (C-terminus). Once the protein is inside the signal sequence is cleaved off.
  - **Membrane-associated proteins** do not enter the bilayer at all: they are bound to other membrane proteins by nonconvalent bonds (7,8 in the picture below). Those are also called **peripheral proteins**.

![membrane proteins](./img/05_Lipids_Cell_Membranes/membrane_proteins.png)

- The way a protein is attached to the membrane is linked to its function:
  - Only transmembrane protein are able to transport molecules across the membrane.
  - Cell-surface receptors are transmembrane proteins that bind signal molecules outside of the cell and generate a different signal within the cell.
- Anchors as lipid groups convalently attached to the proteins:
  - **Myristic acid** (14 carbon fatty acid) can be added to the N terminal end of the protein during its synthesis on the ribosome.
  - **Palmitic acid** (saturated 16 carbon fatty acid) that binds to a cysteine.
  - In the ER enzymes covalently attached glycosilphosphatidyl-inositol (GPI) anchor to the​ C terminus of some proteins. While linkage is formed in the lumen of ER the transmembrane segment of the protein​ is cleaved off.
  
![lipid anchor](./img/05_Lipids_Cell_Membranes/lipid_anchor.png)

- _ppt5/slides 50-51_
- Most transmembrane proteins crosses the bilayer with an $`\alpha`$ helix conformation. This form maximize the number of hydrogen bonds that can be formed by the peptide bonds within the part of the protein that is inside the lipid bilayer.
- Another way for transmembrane proteins to cross the lipid bilayer hydrophobic core is to form $`\beta`$ sheets and form a cylinder called **barrel**.
  - Used to go through the membrane multiple times (multipass membrane protein). 
  - More rigid than $`\alpha`$ helix thus first to be captured in x-ray crystallography.
  - The number of $`\beta`$ strands vary from barrel to barrel.
  - Abundant in bacteria, mitochondria and chloroplasts.
  - Some barrels are filled with water creating a channel allowing hydrophilic molecules to cross the membrane (**porins**).
  - Loops of the polypeptide chains are found inside the lumen of the channel in order to be more selective of which molecules can go through.
- **FepA** protein is a $`\beta`$ barrel transport protein. It transports iron ions. The mechanism for the transfer of iron across the membrane is not known.
- Most transmembrane proteins are glycosylated. ​
  - The sugar residues are added in the **lumen of the ER** and Golgi apparatus (like glycolipids).
  - Oligosaccharide chains and disulfide bonds are all on the non-cytosolic side of the membrane​.
  - Sugars tend to self associate by van der Waals and hydrogen bonds.
  - **Proteoglycans**: long polysaccharide chains (sugars) covalently linked to a protein core. Found mainly outside of the cell in the extracullar matrix (ECM).
    - For some proteoglycans the protein core goes through or is attached to (by a GPI anchor) the lipid bilayer.
- **Cell coat** refers to the coat of carbohydrates on the cell surface (from all the sugars attached to the proteins).
  - One of the functions of the cell coat is to protect cells against mechanical and chemical damage and to minimize unwanted cell-cell interactions.
  - Unlike amino acids that bonds in the same way in a polypeptide chain, sugars can bond in different ways allowing for more diversity in the end result. That's why there are useful in specific cell recognition processes.
- Most sugars are attached to molecules found inside the membrane. Some proteoglycans and glycoproteins come from outside the cell and attached to the cell surface.
- Membrane proteins can be **solubilized** and purified in **detergents**.
  - Usually, only agents disrupting hydrophobic associations and destroy the lipid bilayer can solubilize membrane proteins.
- **Detergents**: small amphiphilic molecules of variable structure.
  - They are much more soluble in water than lipids.
  - They can be charged or not (ex: Triton).
  - When mixed with membranes, the hydrophobic ends of the detergent binds to the ​hydrophobic region of the membrane proteins and because of their polar end they form detergent-protein complexes.
  - Strong detergents like SDS can solubilize even the most hydrophobic membrane proteins. Those detergents though **denature** (unfold) the protein by binding to their internal hydrophobic core thus making them inactive.
  - Using mild detergent allow to solubilize and purify protein in an active form. Unlike strong detergents they do not denature the protein.
    - Reducing the concentration of detergent membrane protein do not remain soluble but instead (with excess of phospholipids) will incorporated into small liposomes. This allow to study the functionaly of specific membrane proteins.
    
![detergent](./img/05_Lipids_Cell_Membranes/detergent1.png)
![detergent](./img/05_Lipids_Cell_Membranes/detergent2.png)

- Transmembrane helices can be predicted from amino acid sequences.
  - Most of their residues are nonpolar and none are charged.
  - In order to know if the segment fit more in a hydrocarbon environment or in water we need to check the free energy changes for the transfer of individual amino acids residues from a hydrophobic to an aqueous environment.
  - A helix formed only by Arginine is favorable whereas a helix formed from phenylalalnine is unfavorable.
  - The hydrocarbon core of a membrane is $`30 \text{\AA}`$ which can be traversed by an $`\alpha`$ helix of 20 residues.
  - A **window** is a span of 20 chosen residues to caculate the free energy change when a hypothetical helix is transferred from the interior of a membrane to water (windows: 1-20, 2-21, 3-22, ...).
  - **Hydropathy plot**: free energy of each window in plotted against the first amino acid at the window. Transmembrane protein can escape detection like $`\beta`$ barrels.
  - **+84 kJ/mol** means a possible membrane segment.
  - The strong drive to maximize hydrogen bonding in the absence of $`H_2O`$ suggests that polypeptides entering the membrane are likely to go through and not change direction​.
  - In multiple transmembrane there are regions in which proteins don’t come in contact with hydrophobic core of the lipid bilayer. In such regions proteins don’t have to maximize hydrogen bonds and thus can have structures including helices that extend only part way across the lipid bilayer​.
  
![free energy](./img/05_Lipids_Cell_Membranes/free_energy.png)

- The movement of membrane proteins was tracked by fusing mouse cells and human cells. The proteins were each on their "species" side at first then diffused and mixed.
- **FRAP** (fluorescence recovery after photobleaching) is a method that allows to measure **lateral diffusion rates**. 
  - Some proteins move faster than others (average $`1 \mu m^2/sec`$).
  - Some proteins hardly move – fibronectin is anchored to actin filaments on the inside of the plasma membrane.​

![frap](./img/05_Lipids_Cell_Membranes/frap.png)

- Cells can confine proteins and lipids to specific **domains** within a membrane.
- Intercellular junctions like **tight junctions** allow to set up barriers to proteins and lipids. This allow for certain types of proteins to stay in certain parts of the cell membrane.

![tight junctions](./img/05_Lipids_Cell_Membranes/tight_junctions.png)

- Cells can create domains without intercellular junctions: protein-protein interactions in membranes are thought to create small raft domains that function in signaling and membrane trafficking.
- 4 ways to immobilize specific membrane proteins through protein-proteins interactions:

![immobilize proteins](./img/05_Lipids_Cell_Membranes/immo_proteins.png)

- Proteins containing several $`\alpha`$-helices that are next to one another (could contain water) is because the protein get into the membrane with each $`\alpha`$-helix touching the memebrane before they join together.

![multi helix](./img/05_Lipids_Cell_Membranes/multi_helices.png)
