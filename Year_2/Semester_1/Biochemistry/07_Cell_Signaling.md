# Cell Signaling

- Unicellular organisms lead mostly independent lives they do communicate with other cells.
- They respond to physical and chemical changes in their environment including mechanisms to respond to the presence of other cells.
- **Quorum sensing** allow bacteria to coordinate their behavior including their motility, spore formation, antibiotic production and sexual conjugation.
- **Saccharomyces cerevisiae (yeast)​**: haploid cells, when ready to mate they send a **mating factor** (peptide) that signals other cells to stop proliferating (dividing via mitosis) and prepare to mate (by undergoing meiosis). Then they produce a diploid zygote.
  - The use of mutants reveals the nature of the components involved in signaling​.
  - Mutants that are unable to mate have revealed​ many proteins that are required for signaling​.
- In multicellular organisms, the well being of the organism as a whole is more important than that of a single cell within this organism.
- **Extracellular signal molecules** allow cells in multicellular organisms to communicate.
- In multicellular organisms, most cells send and receive signals.
- **Receptor proteins** allow to receive specific signals. Those are usually (but not always) at the cell surface.
  - Those receptors bind the signal molecule which activate the receptor which in turn activates one or more **intracellular signaling pathways**.
  - Usually receptors are transmembrane proteins. They also can be inside the cell which means that the signal molecule needs to be small and hydrophobic in order to get into the cell.
- In the human genome there are more than 1500 genes coding for receptors.
- **Effector proteins** are the intracellular targets of the signal received. They are responsible for changing the behavior of the cell.
- Intracellular pathways depend on intracellular signaling proteins which process the signal received and distribute it to the appropriate **effector proteins**.
- Effector proteins (targets of signals) can be:
  - Transcription regulators
  - Ion channels
  - Compenents of a metabolic pathway
  - Parts of the cytoskeleton

![signaling schema](./img/07_Cell_Signaling/signaling_schema.png)

## Types of signaling

- Many signal molecules are hydrophobic making their way by binding to a carrier protein.
- Extracellular signals can act over short or long distances.
- **4 forms of intercellular signaling**:
  - Contact-dependent (short distance)
  - Paracrine (short distance)
  - Synaptic (long distance)
  - Endocrine (long distance)
  
  ![forms of signaling](./img/07_Cell_Signaling/signaling_forms.png)
  
- **Contact dependent signaling**: extracellular signal molecules that do not move from the surface of the signaling cell but influence the cells that come in contact with it.
  - This type of signaling can be considered long distance if cells extend long processes to make contact.
- **Paracrine signaling**: signal cells secrete signal molecules into the extracellular fluid.
  - The secreted signaling molecules can be **local mediators** that act on cells in the local environment.
  - Usually this type of signaling is between different type of cells.
  - Cells can also use this type of signaling on themselves (can be another cell but the same type). This is called **autocrine signaling**. Cancer cells will use this to stimulate their proliferation.
- Large multicellular organisms (like humans) need long range signaling mechanism.
- **Neurons** (nerve cells) are cells with branches that allow them to receive signals (**synaptic signaling**).
  - The **axon** is part of the neuron cell. It is a long branch through which the signals go as an **electrical signal** and reach the **synapse** (gap between 2 neurons) where the signal is **chemically** transmitted (**neurotransmitter**).
  - Ending neurotransmitters signaling: 
    - Dissocation
    - Hydrolysis by enzymes
    - Uptake by nerve terminal or neighboring cells
  
  ![neuron](./img/07_Cell_Signaling/neuron.png)
  
- **Endocrine cells** secrete into the bloodstream **hormones** as signal molecules. The blood carries the molecules until they reach target cells (can be anywhere in the body).
- Endocrine signaling is much slower than synaptic signaling.
- Endocrine signaling act at a low concentration ($`< 10^{-8}`$).

![endocrine vs synaptic](./img/07_Cell_Signaling/endocrine_synaptic.png)

- Epithelial cells (tissue cells like for organs and skin) are coupled electrically and metabolically by **gap junctions**.
  - Small molecules can go through those gap junctions like $`Ca^{2+}`$, amino acids, nucleotides, vitamins, intracellular mediators.
  - The communication goes both ways.
  - The gap is spanned by channel forming proteins, of which there are two families called **connexins** and **innexins** (have different sequence but same shape and function). In vertebrate both are present though connexin is predominant. The channel formed is termed **connexon**. 
  - It is similar to autocrine signaling in that that it helps similar cell types communicate.
  - Gap junctions are dynamic and can be assembled or disassembled.
  - Increase in  $`Ca^{2+}`$ or decrease in pH with cause the gap junctions to close. For example if one cell is hurt and there's an influx in  $`Ca^{2+}`$, the gap junction closes to not affect the joined cell.

![gap junctions](./img/07_Cell_Signaling/gap_junctions.png)

- Cells respond to a combination of signals. Different combinations can cause cells to divide or kill themselves by apoptosis.
  - Cell proliferation usually depends on a combination of signals promoting cell division, survival and cell growth.

![multiple signals](./img/07_Cell_Signaling/multiple_signals.png)

- A same signal molecule can have a different effect depending on which cell type it binds to.
  - _For example acetylcholine have a different effect wether it binds to heart pacemaker cells (decrease heart rate), salivary gland (stimulates saliva secretion) cells or muscle cells (induces contraction)._
  - The difference comes from the different intracellular signaling proteins, effector proteins and genes that are activated.
  - This means that the signal itself **contain little information**.  

![acetylcholine](./img/07_Cell_Signaling/acetylcholine.png)

## Surface Signaling

- Most receptors are one of 3 types:
  - **Ion-channel-couples receptors**: rapid signaling between nerve cells and other nerve cells or muscle cells.
    - The neurotransmitters bind to an ion channel and cause it to open or close.
  - **G-protein-coupled receptors**: inderiectly regulate the activity of another plasma-membrane protein (usually either an enzyme or ion channel).
    - G-protein mediates the interaction between the activated receptor and the target protein.
    - The activation of the target protein can change the concentration of small intracellular signaling molecule (for enzymes) or change the ion permeability of the membrane (ion channel). This causes intracellular signaling.
  - **Enzyme-coupled receptors**: either function as enzymes or associate with enzymes that they activate. They usually have their ligand-binding site outside the cell and enzyme-binding site inside the cell (single-pass transmembrane protein).
    - Most are protein kinases or associate with them. They phosphorylate specific sets of proteins in the target cell when activated.

> Protein kinase: protein that binds a phosphate to an amino acid to activate the enzyme. 
> Serine/threonine & tyrosine.  
    
![types surface](./img/07_Cell_Signaling/surface_types.png)

### Intracellular Signaling

- Intracellular signaling molecules relay the signals received by surface receptors.
- The signal is passed until it reaches the effector protein.
- **Second messengers** are the small chemicals that act as intracellular signaling molecules.
- Those molecules are generated in large amounts in response of receptor activation and spread in the cell.
- **Cyclic AMP** and $`Ca^{2+}`$ are second messengers.
  - Those are water soluble and diffuse in the cytosol.
- **Diacylglycerol** is also a second messenger.
  - It is lipid soluble and diffuse within the plasma membrane.

#### Switching on/off signaling proteins

- Switching "on/off" the signaling proteins is mostly done by **phosphorylation**.
- Two main **molecular switches** are responsible for de/activating signaling proteins:
  - Protein kinase/Protein phosphatase
  - GTP-binding/GTP hydrolysis
- A **protein kinase** convalently adds one or more phosphate groups on the hydroxyl group of the signaling protein.
- A **protein phosphatase** removes the phosphate groups.
- **Kinase cascade**: protein kinase that is activated by phosphorylation by another protein kinase which itself was activated that way and so on.
- The protein kinase attaches the phosphate group to the OH group of specific amino acids:
  - Serine/threonine kinases attach the P group to serines and threonines in their protein target (both of them not just one).
  - Tyrosine kinases attach the P group to tyrosines.
  - Some kinases know how to attach to both.
- Around 2% of our genes encode for kinase.
- **GTP-binding proteins**:
  - GTP bound = on
  - GDP bound = off
- When activated, those signaling proteins have a lot of GTPase activity.
- To shut themselves down they **hydrolize their GTP to GDP**.
- 2 types of GTP-binding proteins: 
  - **Large trimeric GTP-binding proteins** (G-proteins).
    - They relay signals from G-protein-coupled receptors.
  - **Small monomeric GTPase** relay signals from different cell surface receptors.
- Both GTP-binding proteins are regulated by **GAPs** (GTPase-Activating Proteins).
  - Those drive the proteins into an off state by increasing the rate of hydrolysis of GTP.
  - Also known as **RGS** (Regulators of G protein signaling proteins)
- **GEFs** (Guanine nucleotide Exchange Factors) activate GTP-binding proteins by promoting the release of GDP which allow a new GTP to bind.

![switch](./img/07_Cell_Signaling/switches.png)
![gap/gef](./img/07_Cell_Signaling/gap_gef.png) ppt49

- Most signaling pathways contains inhibitory steps.
- A sequence of two inhibitory steps can have the same effect as one activating step.

#### Interactions

- Intracellular signaling proteins sometimes bind to the wrong receptor.
- One way to prevent errors is high affinity for specific molecules.
- Another way is for the response to be triggered when a lot of target proteins are activated.
- There are random variations in the concentration or activity of intracellular signaling molecules of cells as well as variations in the interactions between them. This variability also interfere with the precision and efficiency of signaling.
- In order to increase the specificity and reduce unwanted interactions, is to **localize the signaling molecules** in the same part of the cell.
  - Usually **scaffold proteins** are the ones that hold groups of interacting signaling proteins into **signaling complexes**.
  - The proteins being closed to one another allows for them to be activated rapidly and selectively as well as avoiding cross-talk with other signaling pathways.
  - The signaling complexes can form before or after an external signal has been received. The complexes that form after the external signal has been received, dissamble after they're done transmitting the signal.
    - In many of these cases activated receptor is phosphorylated following signal and becomes a docking site for the assembly of other signaling proteins.
    - In other cases, receptor activation leads to the production of modified ​phospholipid molecule called **phosphoinositide**​.
  
  ![scaffold protein](./img/07_Cell_Signaling/scaffold1.png)
  ![scaffold protein](./img/07_Cell_Signaling/scaffold2.png)
  ![docking site](./img/07_Cell_Signaling/docking_site.png)

- Sometimes, intracellular signaling proteins can be activated just by being close to each other.
  - **Interaction domains** are found in many intracellular signaling proteins and allow for the formation of signaling complexes triggered by proximity.
- Types of interaction domains in signaling proteins: 
 
| Type of interaction                                                                          | What is does                                                                 |
|----------------------------------------------------------------------------------------------|------------------------------------------------------------------------------|
| - **SH2 domains** (src homology 2)<br>- **PTB domains** (phosphotyrosine-binding) | Bind to phosphorylated tyrosines in a particular peptide sequence on activated receptors|
| - **SH3 domains** (src homology 3)    | Bind to short proline-rich amino acids sequences                                                                                    |
| - **PH domains** (pleckstrin homology)| Bind to the charged head groups of specific phosphoinositides produced in the plasma membrane in respoonse to extracellular signal  |

- ***Phosphoinositides (PI)** are molecules made of glycerol + 2 fatty acids + phosphate bonded to a sugar.
  - It can undergo phosphorilation and dephosphorialtion on different sites allowing it to be a docking sation for different proteins.
  - **PI(5)P** means that it was phosphorylated at the 5' carbon of the sugar.
  - **$`PI(3,4)P_2`$** means that it was phosphorylated at the 3' and 4' carbons of the sugar.
- Below, example of a large signaling complex around the receptor for the hormone **insulin** (IRS1 insulin receptor substrate 1).

![interaction domains](./img/07_Cell_Signaling/interaction_domains.png)

- Signaling systems work differently, some just pass the signal (molecule) and some process the signal in different ways.
  - **Response timing**: depends on the speed required for the response (synaptic signaling has to be fast but other processes can take days).
  - **Sensitivity** to extracellular signals: high sensitivity means it will react at low concentration of the signal.
  - **Dynamic range** of a signaling system is related to the sensitivity.
  - **Persistence** of a response.
  - **Signal processing**: different ways to process the signal.
  - **Intergration**
  - **Coordination**
- Changes in proteins (like gate proteins) is done in milliseconds while changes in gene expression with protein synthesis can take up to hours.

![response time](./img/07_Cell_Signaling/response_time.png)

- External signal can alter the concentration of intracellular molecules that are **short-live** (unstable).
  - Once the signal is gone the molecules go through degradation (tuneover).
- Many cell responses to extracellular signals depend on the conversion of intracellular signaling proteins from inactive to active form rather than on synthesis and degradation of molecules (inactive/active is faster).
  - **Phosphorylation or the binding of GTP** commonly activates proteins (to deactivate the protein by dephosphorylation or hydrolysis of GTP to GDP).
- Some system respond gradually in response of a wide range of extracellular signal concentrations (see picture - hyperbolic).
- Others generate significant responses only when the signal concentration rises above a threshold value (see picture - sigmoidal or all-or-none).
  - Sigmoidal: low concentrations have no effect but then the response is fast. This allow to filter inappropriate response.
  - All-or-none: when the signal reaches a threshold the response is activated. This is usually how to choose between two alternative cell states.

![response concentration](./img/07_Cell_Signaling/response_concentration.png)

- In order to generate a sigmoidal response, several mechanisms:
  - More than one intracellular signaling molecule must bind to its target protein to induce a response.
    - _Example: 4 cyclic AMP molecules must bind to PKA to active the kinase._
  - Phosphorylation at more than one site is another mechanism.
- Responses are sharpened when signal ​molecule activates an enzyme and at the ​same time inhibits another enzyme as is​ the case with glycogen breakdown.​
- Positive feedback can generate sigmoidal response and if strong enough can generate all-or-none response.
  - Frog egg (oocytes) stimulated with progesterone $`\rightarrow`$ response assessed by activation of **MAP kinase**.
    - It seems that the change is gradual in a population of cells but actually cells have a all-or-none response.
- Positive feedback can cause permanent change in the cell. Muscle-cell specification in an example.
- Negative feedback limits the level of the response. This makes the system less sensitive to pertubations.
- Cells can adapt to detect the same percentage of change in a signal over a wide range of stimulus strengths: prolonged exposure to a stimulus decreases the cell's response to that level of stimulus.
  - This is due to negative feedback.
  
## Signaling through G-Protein-Couples Receptors

- **GPCRs (G-protein-coupled receptors)** are the largest family of cell-surface receptors.
- They receive signal from hormones, neurotransmitters, other cells and local signals (ligand molecules that bind to them).
- Sight, smell and taste senses are based on them.
- A single signal molecule can activate several GPCRs.
  - Adrenaline: 9 receptors.
  - Serotonin 14 receptors.
  - Acetylcholine: 5 or more receptors.
- All types of GPCR use G proteins to relay the signal into the cell interior.
- Extracellular signal molecule binds to a GPCR $`\rightarrow`$ the receptor undegoes conformational changes and activates a **trimeric GTP-binding proteins (G protein)**.
  - The G-protein couples the receptor to enzymesor ion channels in the membrane.
- There are various types of G-proteins that are specific to set of GPCRs and target proteins in the membrane.
- G proteins are composed of **3 protein subunits**: $`\alpha`$, $`\beta`$, $`\gamma`$.
  - When the G protein is inactive the **$`\bold{\alpha}`$ subunit has GDP bound**.
  - Active state: acts like a GEF (guanine exchange factor) and the $`\alpha`$ subunit releases the bound GDP thus allowing GTP to bind instead.
    - The binding of the GTP releases the G protein from the GPCR and the G$`\alpha`$ subunit dissociates from the G$`\beta\gamma`$ subunit.
    - The subunits then interacts with various targets (enzymes, ion channels) in order to relay the signal.
    - The active state is short-lived and the GTP is hydrolized into GDP to deactivate the subunit (as mentioned above short-lived signal molecules are more effective).
  
  | Inactive                                                              | Active                                                            |
  |-----------------------------------------------------------------------|-------------------------------------------------------------------|
  | ![inactive g protein](./img/07_Cell_Signaling/inactive_g_protein.png) | ![active g protein](./img/07_Cell_Signaling/active_g_protein.png) |
  
  
- The $`\alpha`$ subunit is a **GTPase**. It becomes inactive when it hydrolizes its GTP to GDP.
- The GTPase activity is enhance by the binding of the $`\alpha subunit`$ by either a target protein or a **RGS (regulator of G protein)**.
  - Those RGS act as $`\alpha`$ subunits specific GAPs (GTPase activating proteins). They help shut off G protein mediated responses in all eukaryotes.
- RGS proteins have emerged as novel drug targets in numerous disease states, such as hypertension, cancer and Parkinson’s.

### Cyclic AMP - cAMP

- Mediator
- Some G proteins regulate the production of cAMP.
- cAMP act as a second messenger in some signaling pathways.
- It is a small molecule that can be diffused fast.
- An extracellular signal can increase cAMP concentration which requires rapid synthesis and breakdown of the molecules.
- cAMP is synthesized from ATP by an enzyme called **adenylyl cyclase**.
- Many extracellular signals work by increasing cAMP concentrations inside the cell which, in turn, activate GPCRs that are couple to a **stimulatory G protein ($`G_s`$)** and in turn acttivate adenylyl cyclase.
- Other extracellular signals reduce cAMP levels, through other GPCRs, by activating an **inhibitory G protein ($`G_i`$)** thus inhibiting adenylyl cyclase.
- $`G_s`$ and $`G_i`$ are both targets for medically important bacterial toxins (cholera toxin).
- In the table below: responses mediated by $`G_s`$-stimulated increase in cAMP concentration.

![camp](./img/07_Cell_Signaling/camp.png)

- In most animal cells, cAMP works by activating cyclic-AMP-dependent protein kinase (PKA).
  - This kinase phosphorylates specific serines or threonines on selected target proteins which regulates their activity.
- In inactive state, PKA consists of 2 catalytic subunits and 2 regularoty subunits (called A-kinase).
  - Binding of cAMP to the regularoty subunits causes them to dissociate from the complex.
  - The catalytic subunits are thus activated and free to phosphorylate target proteins.
- **AKAP**: A-kinase anchoring proteins, are the regularoty subunits of PKA. They are important for the localizationthe kinase inside the cell.
  - AKAP around the nucleus of heart muscle cells, binds PKA and a phosphodiesterase and activates the PKA.
- In cells that secrete the peptide hormone **somatostatin**, cAMP activates the gene that encode this hormone (slow response).
  - The regularoty region of the gene contain a sequence called **CRE (cAMP response element)** (also found in other genes activated by cAMP).
  - A transcription regulator called **CREB (CRE-binding) protein** recognizes this sequence.

  > Phosphodiesterase $`\rightarrow`$ stops cAMP (as mentiioned the signal molecules need to be short-lived).  

![summary](./img/07_Cell_Signaling/summary.png)

![g protein pathways](./img/07_Cell_Signaling/g_protein_pathways.png)

> The gene transcription from the activation of cAMP (through G-protein) is a slow response.  
> Synthesis of glucose from glycogen is a rapid response (cannot be done when the reverse reaction is in process).  

### Glycogen metabolism

- Regulated by **adrenaline**.
- cAMP activates PKA causes phosphorylation of:
  - An enzyme that breaks down glycogen into glucose + phosphate
  - Glycogen synthase which inhibits it so the process of glucose synthesis is sharper.

> There are other mediators besides cAMP like $`Ca^{2+}`$

### Phospholipids

- Some G proteins signal via phospholipids.
- Many GPCRs work by G protein activating the plasma membrane bound enzyme **phospholipase C-$`\beta`$ (PLC$`\beta`$)**.
- It is done by a G protein called $`G_q`$ which activates phospholipase C-$`\beta`$.
- The activated phospholipase breaks down **phosphoinositides (PIPs)** into two products:
  - **Inositol 1,4,5-triphosphate** ($`IP_3`$)
  - **Diacylglycerol**
- From the products, two pathways:
  - Inositol $`\bold{IP_3}`$ diffuses through the cytoplasm to release $`Ca^{2+}`$.
  - **Diacylglycerol** which remains in the membrane as an anchor and helps activates PKC (kinase C).
  
![phospholipase](./img/07_Cell_Signaling/phospholipase.png)

## Not summarized

- Gas nitric oxide NO can act a signal (5-10 seconds).
- It regulates smooth muscle cells.

ppt7 - slide 108-end (page 847)

## Summary

![summary](./img/07_Cell_Signaling/summary2.png)
