# Intracellular Membrane Traffic

- Cells change the composition of their plasma membrane and internal compartments continually in response to changes in the environment.
- To achieve that they use elaborate membrane systems to add or remove cell-surface proteins.
- **Exocytosis** is the process allowing the delivery (to eithe the membrane or extracellular space) of newly synthesized proteins, carbohydrates and lipids.
- **Endocytosis** is the process allowing the removal of plasma membrane components and deliver them to **endosomes** that can recycle those component in other parts of the plasma membrane. If the components are not recycled they are sent to **lysosomes** for degradation.
- Endocytosis is also used to bring in nutrients.

![endo-exocytosis](./img/08_Intracellular_Membrane_Traffic/endo_exocytosis.png)

- **Transport vesicle** is a membrane-enclosed compartment that can travel.
- The **lumen** of vesicles along the secretory (exocytosis) and endocytic pathways is equivalent to the lumen of most other membrane-enclosed compartments and to the cell exterior.
- **Chaperons** bound to unfolded proteins to keep them in the ER and not go into vesicles.
- In eukaryotic cells, vesicles fuse and bud off membranes to pass along a **cargo**.
- The **secretory pathway**: endoplasmic reticulum (ER) $`\rightarrow`$ Golgi apparatus $`\rightarrow`$ cell surface (there's also a side route leading to lysosmes).
- The **endocytic pathway**: from the plasma membrane into the cell.
- The **retrieval pathway**: balances the flow of membrane, bringing membrane proteins back to the compartment of origin.

![pathways](./img/08_Intracellular_Membrane_Traffic/pathways.png)

## Coating

- Compartments have specific sets of **molecular markers** on the surface membrane. This ensures that transport vesicles fuse only with the correct compartment.
- Cells separate proteins into membrane domains using a special protein coat on the membrane's cytosolic face.
  - Most transport vesicles form from coated regions of membranes.
  - Before fusing with another membrane the vesicle discards its coat.
- The coat has 2 main functions:
  - **Inner coat**: concentrates specific membrane proteins in a specialized patch. This will be the vesicle membrane. This allows to select the right membrane molecules for transport.
  - **Outer coat**: it forms a curved lattice that deforms the membrane patch thus shaping the vesicle.
- 3 major types of coated vesicles:
  - **Clathrin-coated**: mediates transport from the endosome or Golgi apparatus and from the plasma membrane.
    - Major protein component: clathrin which form the outer layer.
    - Clathrin subunit consist of **3 large and 3 small polypeptide chains**. Together they form a **triskelion** (three legged structure).
    - The triskelions form the coated buds by assembling into a basketlike framework.
    - **Adaptor proteins** are another major component of clathrin coat. They form an inner layer of the coat. They bind the clathrin coat to the membrane and trap various transmembrane proteins including cargo receptors.
    - Adaptor proteins are **specific to cargo receptors** $`\rightarrow`$ they decide which proteins are transported.
    - Adaptor proteins are opened when binding to phosphoinositide PI(4,5)P2 allowing them to bind to cargo receptors.
  - **COPI-coated**: mediates transport from the Golgi cisternae.
  - **COPII-coated**: mediates transport from the ER.
  
|                                                             |                                                                       |                                                                                  |                                                                                   |
|-------------------------------------------------------------|-----------------------------------------------------------------------|----------------------------------------------------------------------------------|-----------------------------------------------------------------------------------|
| ![coats](./img/08_Intracellular_Membrane_Traffic/coats.png) | ![triskelion](./img/08_Intracellular_Membrane_Traffic/triskelion.png) | ![clathrin coated pit](./img/08_Intracellular_Membrane_Traffic/clathrin_pit.png) | ![adaptor proteins](./img/08_Intracellular_Membrane_Traffic/adaptor_proteins.png) |

> Adaptor protein AP2 (between clathrin and receptor):  
> At first it is closed.  
> When it binds to a phosphoinositide, it alters its conformation which exposes binding sites for cargo receptors.  
> The simultaneous binding to the cargo receptor and lipid head greatly enhances the binding of AP2 to the membrane​.  
> The binding of AP2 to the membrane induces membrane curvature and this allows additional AP2 to bind.

|                                                                         |                                                       |                                                         |
|-------------------------------------------------------------------------|-------------------------------------------------------|---------------------------------------------------------|
| ![ap2 binding](./img/08_Intracellular_Membrane_Traffic/ap2_binding.png) | ![pi](./img/08_Intracellular_Membrane_Traffic/pi.png) | ![pi2](./img/08_Intracellular_Membrane_Traffic/pi2.png) |

- Inositol phospholipids can undergo rapid cycles of phosphorylation or dephosphorylation to produce various types of **phosphoisonitides** (PIPs).
- Organelles have distinct sets of PIP kinases (add P) and PIP phosphatases (remove P).
- Distribution of PIPs varies from organelle to organelle (also within the membrane from one region to the other) defining specialized membrane domains​.
- Many proteins involved in vesicle transport contain domains that binds specifically to the head groups of a particular PIP.
- The clathrin coat isn't enough to shape and pinch off the vesicle. **BAR domains** are domains in membrane-bending proteins and help with the process of bending to shape the vesicle.
- BAR domain proteins are directed to the membrane via PIPs. BAR domains have a positive charge while the inner membrane have a negative charge thus attracted to one another.
- Pinching off the vesicle is mediated by a complex of proteins including local assembly of actin filaments that introduce tension to help pinch off and propel the forming vesicle away from the membrane​.
- **Dynamin** has 2 domains:
  - PI(4,5)P2 domain that assembles at the neck of the bud.
  - GTPase domain regulates the rate at which the vesicle pinch off the membrane.

![dynamin](./img/08_Intracellular_Membrane_Traffic/dynamin.png)

- Once the vesicle is released from the membrane it loses its clathrin  coat.
- An HSP-70 chaperone function as an un-coating ATPase using the energy released by the hydrolysis of ATP to peel off clathrin coat​.
- **GEFs (guanine nucleotide exchange factors) activate proteins by catalyzing the exchange of GDP to GTP**.
- **GAPs (GTPase activating proteins) inactivate proteins by triggering the hydrolysis of GTP into GDP**.
- **Coat-recruitment GTPases** control the assembly of clathrin coats on endosomes and the COPI and COPII coats on Golgi and ER membrane.
  - They are members of the **monomeric GTPase** family.
  - **ARF proteins** are responsible for the assembly of COPI and clathrin coat at Golgi membranes.
  - **Sar1 protein** is responsible for the assembly of COPII coats at the ER membrane.
    - It is a G protein.
    
> [Video here](https://www.youtube.com/watch?v=taRDwlml1tA) 
> Step 1: Sar1 is activated by Sar1-GEF.  
>         Its amphipatic helix expands and docks in the membrane.  
> Step 2: COPII binding proteins called **Sec23** and **Sec24** that make up the **inner coat**.  
> Step 3: COPII proteins **Sec13** and **Sec31** that make up the **outer coat**.  
> Step 4: Hydrolysis of GTPase causing Sar1-GDP to pop out the leg from the membrane causing the **coat to disassemble**.  

![COPII](./img/08_Intracellular_Membrane_Traffic/copii.png)

- Coat-recruitment GTPases are usually found inactive (GDP bound state) and in high concentration in the cytosol.
- The coat-recruitment GTPases are also involved in coat disassembly.
- Sar1 coat may stay until the vesicle fuse again. Vesicles with clathrin and COPI coats shed them as soon as they pinched.
  - For COPI vesicles, the curvature of the vesicle membrane serves as a trigger to begin uncoating. The trigger is done by an ARF-GAP that senses the level of curvature of the vesicle and activates when the vesicle is closed to be done.
- More force is required to form a vesicle on the plasma membrane which is relatively flat than on the Golgi membrane (or similar membrane) that have regions that are already curved (most vesicle will form in those regions).
- Not all vesicles are sphericle: **collagen** is assembled in the ER as 300nm long procollagen rods and secreted from the cell.
  - COPII vesicles need to be bigger to carry procallagen cargo.
  - Procollagen binds to **packaging proteins** in the ER which control the assembly of the COPII coat and allow to build a bigger coat.
- Endosomes and trans Golgi network continually send out long **tubules** on which coat proteins attach. Those tubules then regress or pinch off (with the help of dynamin) to form transport vesicles.

## Fusing

- The transport vesicle identifies the right membrane to bind to in **2 steps**:
  - **Rab proteins** and **Rab effectors** direct the vesicle to specific spots on the correct target membrane.
  - **SNARE proteins** and **SNARE regulators** mediate the fusion of the lipid bilayer.

### Rab Proteins

- **Rab proteins** are (the largest family of) **monomeric GTPases** (thus use the GDP/GTP cycle).
  - Each Rab protein is associated with one or more membrane-enclosed organelles of the secretory or endocytic pathways.
  - Each organelle has at least one Rab protein on its cytosilic suface.
  - Rab proteins can function on transport vesicles, target membranes or both.
  - Rab proteins cycle between their GDP-bound inactive state in the cytosol and their GTP-bound active state on a membrane.
  - During its inactive state Rab is bound to an **inhibitor GDI**
  - Membrane bound Rab-GEFs activate Rab proteins on both vesicles and target membrane.
  - In their active state, Rab proteins bind to **Rab effectors** proteins which actually attach the vesicles to the membrane.
  - The rate of hydrolysis sets the concentration of active Rab.
- **Rab effectors** can have various roles: they can be used to tether the vesicle to the membrane, or propel the vesicle to its target.
  - Rab effectors can also interact with SNAREs to couple membrane tethering to fusion.
  
 |                                                                               |                                                         |
 |-------------------------------------------------------------------------------|---------------------------------------------------------|
 | ![rab organelles](./img/08_Intracellular_Membrane_Traffic/rab_organelles.png) | ![rab](./img/08_Intracellular_Membrane_Traffic/rab.png) |
 
#### Motor Proteins

- **Microtubules** are responsible for a variety of cell movements, including intracellular vesicle transport, separation of chromosomes, and beating of cilia and flagella. ​
- Movement along microtubules is based on **motor proteins** that utilize energy derived from ATP hydrolysis. ​
- Members of two large families of motor proteins: the **kinesins** and the **dyneins** are responsible for powering the variety of movements in which microtubules participate.​
- Kinesin and dynein, the prototypes of microtubule motor proteins, move along microtubules in opposite directions **kinesin toward the plus** end and **dynein toward the minus** end​.

#### Rab5 Domains

- Rab5 assembles on endosomes and mediates the capture of endocytic vesicles arriving from the plasma membrane.
- Without Rab5 the entire endosomal and lysosomal membrane system disappears.
- It starts assembling on the endosomal membrane when a Rab5-GDP/GDI complex encounters a Rab-GEF.
  - This allows the Rab5-GTP to extend a "leg" which is tethered to the membrane.
- Once activated, Rab5 domains are able to receive **incoming endosomal vesicles**:
  - PI 3-kinase converts local PI to PI(3)P which in turn binds Rab effectors including tethering proteins (positive feedback).
- A Rab domain can be disassembled and replaced by a different Rab domain thus **changing the identity of an organelle**. This is called **Rab cascade**.
  - _For example, Rab5 domains replaced over time by Rab7 domains on endosomal membranes converts the endosome from an early endosome to a late endosome (see table obove)._ This process is called **endosome maturation** and is  unidirectional and irreversible.
  - **Rab7** is an **outgoing site for vesicles**.
  
### SNAREs

- Once a transport vesicle has been tethered to its target membrane, it unloads its cargo by membrane fusion.
- Fusion is **energically unfavorable** due to the need of displacing water from the hydrophilic surface of the membranes (so they can fuse). Thus this process requires specialized proteins.
- The **SNARE proteins** catalyze the membrane fusion.
- There are >=35 different types in an animal cell each associated with a particular organelle in the secretory/endocytic pathways.
- They come in complementary sets:
  - **v-SNARE** on the vesicle membrane. It is a single polypeptide chain.
  - **t-SNARE** on the target membrane. It is composed of 3 proteins.
- v-SNARE and t-SNARE have **helical domains** that wrap around each other. This forms a complex that locks the 2 membranes together.
  - They are specific so the cargo will go to the right organelle.
- The energy released from the the bounding of the helical domains is used to catalyze the membrane fusion.

![snare](./img/08_Intracellular_Membrane_Traffic/snare.png)

- Rab effectors accelerate the process of fusion.
- In target membranes, t-SNAREs are often associated with inhibitors that need to be released before t-SNARE can function.
  - Rab proteins can their effectors trigger the release.
  
![snare process](./img/08_Intracellular_Membrane_Traffic/snare_process.png)

- SNARE complexes need to disassemble before they can be used again.
- **NSF** protein is the one catalyzing this process.
- NSF is an **ATPase** (using the energy from ATP hydrolysis to unravel the helices).

## Transport from the ER to the Golgi Apparatus

- Newly synthesized proteins, undergo transformation while going from the ER to the Golgi to the cell surface.
- The Golgi apparatus is the major site of carbohydrate synthesis.
- Proteins that enter the ER and destined for the Golgi (or beyond) are packed in **COPII coated** vesicles.
- Vesicles bud from specialized regions where there are no ribosomes. Those are called **exit sites**.
- Entry to the vesicle can be a selective process or happen by default.
- Cargo membrane proteins display exit signal that is recognized by the adaptor protein.
- Exit from the ER to Golgi is a major check point at which quality control is exerted. 
- To exit ER Proteins must be properly folded. If built from subunits they need to be completely assembled.​
- Those (proteins) that are misfolded or not assembled correctly transiently remain bound to chaperone (BiP) in the ER.
- Chaperon may cover exit signal or anchor the protein in the ER. 
- Some like T cell receptor or acetylcholine receptor more than 90% are misfolded​.

## Transport from the Trans Golgi Network to Lysosomes

### Lysosomes

- There **4 pathways** to degradation:
  - **Phagocytosis** $`\rightarrow`$ phagosome $`\rightarrow`$ lysosome.
  - **Endocytosis** $`\rightarrow`$ early endosome $`\rightarrow`$ late endosome $`\rightarrow`$ lysosome.
  - **Macropinocytosis** $`\rightarrow`$ late endosome $`\rightarrow`$ lysosome.
  - **Autophagy**: autophagosome $`\rightarrow`$ lysosome.

![overall](./img/08_Intracellular_Membrane_Traffic/overall.png)

- The **trans Golgi** network sorts the proteins passing through the Golgi apparatus according to their final destination.
- **Lysosomes** are the principal site of intracellular digestion.
  - They are filled with soluble hydrolytic enzymes that digest macromolecules. Those enzymes are all **acid hydrolases** that work best in acidic pH (nuclease, protease, lipases, and more - page 722).
  - Those enzymes are activated by **proteolytic cleavage** which requires and acid environment.
  - The lysosome allows for those acidic conditions with a pH of 4.5-5.0.
  - The required digestive enzymes get to the lysosome from the **ER through the Golgi**
- **$`\bold{H^+}`$ ATPase** in the lysosome membrane uses the energy from ATP hydrolysis to pump $`H^+`$ into the lysosome. This allows the lumen to stay acidic.
- The $`H^+`$ gradient is also a source of energy for the transport of small metabolites (waste) accross the membrane.
- The cytosol is thus protected by the lysosome's membrane as well as the fact that the digestive enzymes cannot work at a higher pH (the cytosol has a pH of 7.2).
  - If the lysosome gets damaged, the digestive enzymes won't cause issues in the cytosol since they won't be able to function from the high pH.
- Most of the lysosome **membrane proteins are glycosylated** which helps to protect them from the lysosome proteases in the lumen.
- Transport proteins in the lysosome membrane carry the final products of the digestion of macromolecules (amino aicds, sugars, nucleotides) to the cytosol where the cell can reuse or excrete them.

![lysosome](./img/08_Intracellular_Membrane_Traffic/lysosome.png)

- Formation of lysosome:
  - Late endosome containing material from plasma membrane (by endocytosis) and newly lysosomal hydrolases.
  - This late endosome fuses with preexisting lysosome and form an **endolysosome**.
  - Multiple endolysosomes fuse with one another.
  - When most of the material from the late endosome has been digested it becomes a regular lysosome (until they fuse again).

### Peroxisomes

- **Oxidative organelle**: molecular oxygen serves as a co-substrate form which hydrogen peroxide ($`H_2O_2`$) is formed.
- Peroxisomes are diverse and contain different set of enzymes.
- Unlike mitochondria and chloroplasts, peroxisomes are surrounded by only a single membrane and lack DNA or ribosomes.
- All eukaryotic cells have peroxisomes.
- They contain oxidative enzymes like **urate oxidase** and **catalase**.
- They contain one or more enzymes that use molecular oxygen to remove hydrogen atoms from specific organic substrate (R) that produce hydrogen peroxide: $`RH_2 + O_2 \rightarrow R + H_2O_2`$.
- **Catalase** uses $`H_2O_2`$ to oxidize a variety of toxic substrates like formic acid and alcohol. Peroxidation reaction: $`H_2O_2 + R'H_2 \rightarrow R' + 2H_2O`$.
- Those reactions are important in the liver and the kidney cells where peroxisomes detoxify various harmful molecules that enter the bloodstream.
- The excess of $`H_2O_2`$ in the cell is converted by catalase to $`H_2O`$.
- Peroxisomes breakdown fatty acid molecules converting them to acetyl CoA.
- An essential function in animal peroxisomes is to catalyze the **first reactions in the formation of plasmalogens** (most abundant class of phospholipids in myelin).

### Endoplasmic Reticulum (ER)

- ER's membrane is continuous with the space between the inner and outer nuclear membranes.
- The internal space is called the **ER lumer**.
- It usually occupy more than 10% of the total cell volume.
- Central role in **lipid and protein synthesis**. Site of production of all the transmembrane proteins and lipids for most of the cell's organelles (including itself) as well as proteins to be secreted outside the cell.
- Serves as an intracellular $`Ca^{2+}`$ store that is used in many cell signaling responses.
- Mammalian cells begin to import most proteins into the ER before the polypeptide chain has finished to be synthesized. This is called a **co-translational** process.
- In co-translational transport ribosomes synthesizing the protein are bound the ER membrane: **rough ER (RER)**.
- **Cytosolic ribosomes** complete the synthesis of the protein and release it prior to post-translational translocation.

## Transport into the cell from the plasma membrane

- It starts with **endocytosis**: plasma membrane + fluids + solutes + macromolecules + particulate substances.
- **Endocytosed cargo**: receptor-ligand complexes + nutrients and their carriers + extracellular matrix components + cells debris + bacteria + viruses (and sometimes other cells).
- Endocytosis allows the cells to change its plasma membrane composition in accordance to changing extracellular conditions.
- **Pinocytosis**: endocytic vesicles allow to take in fluids and small molecules.
- **Phagocytosis**: allows to take in larger particles.
- Endocytic vesicles fuse with an early endosome where the cargo is stored.
- Some cargo are returned to the plasma membrane, directly or via a **recycling endosome**.
- Some cargo are sent to be degraded in a late endosome (which then fuses with other late endosomes and become lysosomes).
- **Endosome maturation**: the conversion from early endosome to late endosome (and then lysosome).
  - The protein composition of the endosome membrane changes during the process.
  - Part of the membrane is incorporated to the organelle.
  - As it matures it stops to recycle material to the plasma membrane and commits its remaining content to degradation.
  - Late endosomes fuse with one another and lysosomes to form an endolysosome.
  
  ![endosome](./img/08_Intracellular_Membrane_Traffic/endosome.png)

- Macrophages ingest 25% of their own volume of fluid each hour, in half an hour it ingest 100% of its plasma membrane (3%/minute).
- Fibroblasts endocytose: 1% of the plasma membrane per minute.
- Endocytosis starts at **clathrin-coated pits**. Those regions occupy about 2% of the total plasma membrane area.
- Clathrin-coated pit are short lived, it takes only minutes for all of it to be used to form vesicles.
- 2500 clathrin coated vesicles leave the plasma membrane of cultured fibroblast every minute​.
- **Macropinocytosis** is another clathrin-independent endocytic mechanism that can be activated in practically all animal cells.
  - It doesn't work continuoulsly but in response to cell surface receptor activation by specific ligands.
  - The ligands cause a change in actin dynamics and the formation of **ruffles** (protrusion in the cell surface).
  - When the ruffles collapse back onto the cell, large fluid-filled endocytic vesicles form. Those are called **macropinosomes**.
- Macropinosomes acidify and then fuse with late endosomes or endolysosomes without recycling their cargo to the plasma membrane​.

![macropinocytosis](./img/08_Intracellular_Membrane_Traffic/macropinocytosis.png)

### Autophagy

- Cleanup of the cytosol.
- Porter & Ashford in 1962 saw organelles like mitochondria in lysosomes after the cell was treated with **glucagon** which is an hormone responsible for catabolic activities in the body thus uses amino acids and fatty acids which causes starvation in the organism causing autophagy.
- Cells dispose of obsolete parts by a lysosome-dependent: autophagy.
- It helps restructure differenciating cells as well as a response to stress.
- It can remove large obects (like macromolecules, protein aggregates) that proteasomal mechanisms can't.
- It is important in cell growth and normal development as well as in infections and stress (like starvation).

> Crohn's disease: inflammatory bowel disease  
> The body's immune system attacks the gastrointestinal tract possibly, directed at microbial antigens.

- During starvation, the cell disassembles proteins increasing the quantity of amino acids in the cell.
  - At first some vesicles come in and assemble as a **crescent shaped membrane**.
  - This membrane, called **phagophore**, will expand and close around an organelle (like mitochondria) to disassemble.
  - Autophagosome, phagophore closed around the organelle, will fuse with a late endosome.
  - The digestion can happen.
- 3 main pathways of autophagy:
  - **Macro autophagy**: 
    - Major pathway used to removed damaged organelles or unused proteins.
    - The autophagosome formation is induced by PI-3-kinase (and others).
  - **Micro autophagy**: direct engulfment of cytoplasmic material into the lysosome.
  - **Chaperon mediated autophagy** (CMA).
- Damage to the cell can be also caused by an increase in toxic product like **ROS** (reactive oxygen species) which can damage the DNA and cause cancer.
- Autophagy can either kill the cell (necrosis) or stimulate apoptosis:
  - **Beclin 1**: protein that stimulate autophagy and binds to **Bcl-2** which is an anti-apoptosis protein. This **inhibits** Bcl-2 thus **promotes** the apoptosis pathway.
  - **Atg-5**: fragment of Beclin 1 and goes to the mitochondria and promotes apoptosis.
  
> Ligionels pneumophila  
> Bacteria that lives inside the phagosome and prevents its fusion with the lysosome.  
> This gives it a good environment to replicate.  
>  
> HIV infects $`CD4^+`$ cells which are T cells.  
> This induce apoptosis and autophagy processes in surrounding cells (which are mostly the same type of cells important for the immune system).  
