# Krebs Cycle

- Amphibolic process (both catabolic and anabolic).
- Mitochondria produce ATP by oxidizing compounds that came from breaking down nutrients in the cytosol.
- The inner space (inside the inner membrane) is called the **matrix**.
- Oxidizing the carbons by the electron transport chain produces $`CO_2`$.
- The Krebs cycle is catabolic but also anabolic since molecules produced during the cycle can be used as precursors (חומרי קדם) to energy storage, cholesterol, amino acids, etc.
- The different sources of energy (carbohydrates, fatty acids, amino acids) first break down into **Acetyl-CoA**.

![general](./img/12_Krebs_Cycle/general.png)

- Krebs cycle is composed of **8 reactions** which takes Acetyl-CoA and release $`CO_2`$ while producing NADH, FADH2 and GTP.
- The pyruvates go through the outer membrane of the mitochondria and then into the matrix through a transporter located on the inner membrane.
  - The pyruvates go through reactions catalyzed with the enzyme **pyruvate dehydrogenase (PDH)** which produce $`CO_2`$, NADH, Acetyl-CoA (which enter the Krebs cycle).
  - Also some electrons are released from the NADH (?).
- The first reaction is **citrate** and the last is **oxaloacetate**.
- Oxaloacetate is needed in the first step of the cycle and in the last step it is recycled.
- **Krebs cycle reaction**: 

```math
\boxed{3 NAD^+ + FAD + GDP + P_i + \text{acetyl-CoA} \rightarrow 3 NADH + FADH_2 + GTP + CoA}
```

- During the Krebs cycle, 2 molecules of $`CO_2`$, 8 electrons and one molecule of GTP are produced.
- The 8 electrons come from 6 molecules of NADH (one per molecule) and 2 from one molecule of $`FADH_2`$.
- Oxaloacetate ($`C_4`$) + Acetyl-CoA ($`C_2`$) $`\rightarrow`$ Citrate ($`C_6`$)
  - One NADH and one $`CO_2`$ are produced taking one carbon from the citrate making a $`C_5`$
  - One NADH and one $`CO_2`$ are produced taking one carbon from the $`C_5`$ making a $`C_4`$
  - Then GTP, $`FADH_2`$ and NADH are produced
  - Oxaloacetate is reused.
  
  ![krebs1](./img/12_Krebs_Cycle/krebs1.png)

- **Acetyl-CoA formation**:
  - Enzyme: Pyruvate dehydrogenase (which is 3 enzymes E1, E2, E3 and 5 coenzymes)
  - 5 steps in the reaction which releases $`CO_2`$ and the last acetyl group is attached to Coenzyme A.
  - Requires coenzymes: 
    - TTP
    - lipoaminde
    - Coenzyme A
    - FAD
    - NAD
  - The pyruvate is oxidized into acetyl and $`CO_2`$ is released.
  - $`NAD^+`$ gets the electron from the oxidation and converts into NADH.
  - Each pyruvate produces one NADH thus each glucose produce 2 NADH.
  - The reaction is exergonic.
- **Oxidative decarboxylation** reaction:

```math
\boxed{Pyruvate + CoA + NAD^+ \rightarrow + acetyl-CoA + CO_2 + NADH}
```
  
- The bond between acetyl and CoA is called **thioester bond** (עתיר אנרגיה).
- **Cofactors**: non-protein molecules required for the an enzyme to function.
  - 2 types: 
    - **Inorganic**: ($`K^+, \; Fe^{++}, \; Mn^{++}, \; Zn^{++}, \;Cu^{++}`$)
    - **Organic**: coenzymes.
- Cofactors can be loosely bound or tightly bound (**prostetic group**).
- Cofactors that are coenzymes: 
  - Nucleotides triphosphate: UTP, GTP, ATP 
  - Nucleotides that are **vitamins**: $`B_3`$ (contained in NADH and NADPH), $`B_2`$ (in $`FADH_2`$), $`B_5`$ (in acetyl-CoA)
- The vitamin in $`NAD^+`$ is what allow it to bring 2 electrons and a proton and convert into NADH.
- FAD takes in 2 electrons and 2 protons.

![acetyl coA](./img/12_Krebs_Cycle/acetyl_coa.png)

## 8 Reactions

- Citrate
- Isocitrate
- ⍺-ketoglutarate
- Succinyl-CoA
- Succinate
- Fumarate
- Malate
- Oxaloacetate

### Citrate

- Acetyl-CoA together with oxaloacetate make citrate (C4 + C2 $`\rightarrow`$ C6).
- Enzyme: Citrate Synthase
- Release $`H_2O`$ and CoA
- **𝚫G < 0**

### Isocitrate

- Citrate is converted into isocitrate
- Reverse isomerisation by aconitase
- The reaction is done by remove and reinserting a water molecule.

### ⍺-ketoglutarate

- Isocitrate is converted into ⍺-ketoglutarate.
- Isocitrate dehydrogenase catalyzes the reaction: **oxidative decarboxylation** of the isocitrate into ⍺-ketoglutarate.
- The first NADH and $`CO_2`$ are produced.
- The carbon chain contains 5 carbons (C5).
- **𝚫G < 0**

### Succinyl-CoA

- Like pyruvate dehydrogenase, ⍺-ketoglutarate dehydrogenase is composed of three enzymes (E1, E2, E3).
- The carbon chain is now a 4 carbon chain (C4).
- Another NADH and $`CO_2`$ are produced.
- **𝚫G < 0**

### Succinate

- **GTP is produced** by succinyl-CoA synthetase.
- GTP is produced by mammals, plants and bacteria produce ATP.
- The energy required to produced GTP is taken from the thioesteric bond.
- $`Co_AS`$ is replaced by a phosphate group (succinyl-CoA is transformed into succinyl-phosphate).

### Fumarate

- Succinate dehydrogenase is the only enzyme attached to the membrane.
- $`FADH_2`$ enter the electron transport chain in the inner membrane.

### Malate

- Hydration of fumarate into malate.

### Oxaloacetate

- Malate and $`NAD^+`$ catalyzed by malate dehydrogenase to form oxaloacetate and NADH + a proton

## Summary

- Summary including the numer of ATP per steps.

![summary](./img/12_Krebs_Cycle/summary.png)

## Monitoring and Regulating

- Pyruvate dehydrogenase, which links between the glycolysis and the Krebs cycle, is regulated by:
  - Its products (Acetyl-CoA and NADH competiting with CoA and $`NAD^+`$).
  - De/Phosphorylation (on/off) of E1. This is done by enzymes that are regulated by **insuling** (when sugar is present) and **glucagon** (when sugar is lacking).
- Insulin takes care of the excess of glucose.
- The regulation is done by product inhibtion, concentration of reactants, delay by intermediate products.
  - The concentration of NADH delays some enzymes in the cycle (feedback inhibtion).
- The reactions producing $`CO_2`$ (3 and 4) are irreversible.

## Other facts

- Citrate can get out of the mitochondria into the cytosol to make fatty acids and cholesterol.
- $`\alpha`$-Ketoglutarate can be transformed into amino acid glutamate by reduction.

![other](./img/12_Krebs_Cycle/other.png)
