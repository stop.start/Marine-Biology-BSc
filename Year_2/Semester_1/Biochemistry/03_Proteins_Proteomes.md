# Exploring Proteins and Proteomes

- The first step in order to study a specific protein is the separate it from other components.
- Purifying the protein allows for the amino acids sequence to be determined. Many genomes sequences are available in databases which makes it easier to identify the amino acids sequence.
- After purifying the protein, its function needs to be determined.
- **Antibodies** are used to locate and quantify proteins.
- **X-ray crystallography** and **NMR - nuclear magnetic resonance** spectroscopy are used to determine the shape of the protein.
- The complete DNA genomes of many organisms is available:
 
|                        | C. elegans (roundworm) | Droshophila | Human    |
|------------------------|------------------------|-------------|----------|
| Bases                  | 97M                    | 180M        | 3Billion |
| Protein-encoding genes | 19000                  | 14000       | 23000    |

- Not all proteins in the genome are expressed.
- The **proteome** is the entire set of proteins expressed by a genome in a cell, tissue or organism. It is the **set of expressed proteins in a given type of cells or an organism at a given time under defined conditions**.​
- Proteome: **proteins** expressed by the **genome**.
- The proteome varies with cell type, development stage, environmental conditions (like presence of hormones). The genome is always fixed (no changes).
- The proteome is larger than the genome, especially in eukaryotes, in the sense that there are more proteins than genes. This is due to alternative splicing of genes and post-translational modifications like glycosylation or phosphorylation.​

## Protein purification

- Isolating the wanted protein type in a sample can be done by separating other components based on **size** or **charge**.
- To know if the purification was successful a test called **assay** is used. _For example, with enzymes, the assay would be to measure enzyme activity_.

  > Lactate dehydrogenase catalizes the below reaction by reacting with $`NAD^+`$.  
  > $`NADH`$ absorbs light at 340nm while $`NAD^+`$ doesn't.  
  > Using this fact, we can test how much light is absorbed by a sample over a period of time (1 minute) after adding the enzymes.  
  > With enzyme activity the light absorbance should increase allowing to indirectly measure the concentration of the protein.  
  > During the purification process, the concentration as well as light absorbance should rise.  

![assay](./img/03_Proteins_Proteomes/assay.png) 

- In order to take out the proteins from the cell source, the cell is fractioned into components.
- Protein separation is based on 4 characteritics each with its own separation technique:
  - **Solubility** 
  - **Size**
  - **Charge**
  - **Specific binding affinity**

### Disrupting the cell membrane

- This can be done by either:
  - **Osmotic shock**: hypotonic medium which causes water to enter the cell in order to equalize the pressure ending in the cell exploding.
  - **Detergent**
  - **Sonication**
- This gives us an **homogenate**.
- The mixture is fractionated by centrifugation. 
  - The **supernatant** (upper part) is centrifugate again at a different force. 
  - Each time we use a known force to remove from the supernatant known parts like nuclear pellet, mitochondrial pellet.
  - The process is repeated until we get a **pellet** (lower part) with proteins.

### Salting out

- The more hydrophilic amino acids a protein has on its surface the more soluble it is in water.
- Proteins are less soluble at high salt concetration.
- Each protein precipitate at different salt concentration.
- In the case of proteins it is the charged amino acids that allow selective salting out to occur. Charged and polar amino acids such as glutamate, lysine, and tyrosine require water molecules to surround them to remain dissolved. In an aqueous environment with a high ionic strength, the water molecules surround the charges of the ions and proteins. At a certain ionic strength, the water molecules are no longer able to support the charges of both the ions and the proteins. The result is the precipitation of the least soluble solute, such as proteins and large organic molecules.​
- Dialysis can be used to remove the salt if necessary.
- Cannot differenciate between proteins thus not precise.

### Dialysis

- Allow to separate proteins from small molecules like salt.
- **Semipermeable** membrane (like cellulose) are used to achieve this. 
- The mixture is placed in a solution that doesn't have the small molecules we want to remove.
- The membrane has pores big enough to let out only the small molecules, not the proteins.
- This technique removes well small molecules but cannot separate between different type of proteins.

![dialysis](./img/03_Proteins_Proteomes/dialysis.png)

### Gel filtration chromatography


- Uses a column to filter the proteins based on their size.
- In the column, beads which can let through them molecules: some beads will let through really small molecules, other average molecules.
  - The goal being to remove proteins that are smaller than the one we want. Of course in the end we'll still have other proteins with equal or bigger size.
- The beads create a longer path for the smaller molecules to go through thus they take a longer time to leave the column, unlike the bigger ones who go pretty much straight downward.
- The size of the column is important because the proteins need to have room the separate according to their size.
- Sephadex, Sepharose or Biogel – commercial preparations of these beads​.

![gel filtration](./img/03_Proteins_Proteomes/gel_filtration.png)

### Ion exchange chromatography

- Column which separates protein depending on their charge.
- Depending on the charge of the wanted protein, the beads will have the opposite charge in order to bind it.
  - A protein with positive charge at pH 7.0 will bind to negatively charged beads​. Negative charge proteins won't.
- The not bound proteins are then washed away (process done several times).
- After washing the unwanted proteins, the bound proteins can be released by increasing the concentration of sodium chloride NaCl (or other salt).
  - Given enough sodium ions ($`Na^+`$), they will compete with the protein's positive charged groups causing them to unbind.
- There will still be other proteins in the result but less than before.

![ion exchange](./img/03_Proteins_Proteomes/ion_exchange.png)

### Affinity chromatography

- Highly selective for the protein of interest.
- Proteins can have groups that have more or less affinities with other molecules, for example **concanavalin A**, a plant protein that binds carbohydrate (glucose).
- Beads with glucose, bind the proteins.
- Unwanted (and unbound) proteins are washed away with a buffer.
- To release (elute) the protein from the glucose beads, glucose (a lot of it and without beads) is inserted, competing for the proteins and, given enough of glucose, binding to them thus washing them away.

![affinity chromatography](./img/03_Proteins_Proteomes/affinity.png)

### High performance liquid chromatography

- **HPLC** high performance liquid chromatography.
- Enhanced version of the column techniques.
- The column material are much more finely divided, as a consequence there are more interactions sites and thus greater resolving power.
- Since the column is build of fine material ​there is a need for pressure to achieve ​adequate flow rate​.

## Testing protein purification success 

- Two ways:
  - Check that the specific enzyme activity increases.
  - Decrease in number of proteins in each step. Done by **electrophoresis**.

### Gel electrophoresis

- A molecule with a net charge will move in an electric field.
- This allows to separate proteins and other macromolecules (DNA, RNA).
- The velocity of migration of a protein is tested in an electric field which is characterized by its strength.
- **The velocity depends on the strength of the electric field, the net charge of the protein and the frictional coefficient**:
  - $`\bold{v}`$: velocity
  - $`\bold{E}`$: strength of the electric field
  - $`\bold{z}`$: net charge on the protein
  - $`\bold{f}`$: frictional coefficient
  
```math
\boxed{v = \frac{Ez}{f}}
```

- The **frictional coefficient - f** depends on both the **mass and shape** of the molecule and **viscosity** of the medium.
- This technique is usually done in porous gels or on paper because they enhance the separation: small molecule will move easily in the gel's pores while bigger molecules will be pretty much immobile.
- The proteins move **from the negative electrode to the positive electrode**, usually it is from top to bottom.
- **Polyacrylamide gels** are chosen because they are chemically inert.

![electrophoresis](./img/03_Proteins_Proteomes/electrophoresis.png)

- Proteins can be separated based on mass by electrophoresis in polyacrylamide gel under **denaturing conditions**.
- **SDS** (sodium dodecyl sulfate) disrupts nearly all noncovalent interactions in native protein.
  - It also negatively charges the proteins. 
- One SDS anion binds for every 2 amino acid residues.
- **$`\bold{\beta}`$**-Mercaptoethanol is added to reduce disulfide bonds. 
- The negative charge on the SDS is a lot bigger than the positive charge on the protein and is (the negative charge) proportional to the mass of the denatured protein.
- The mobility of most proteins under these conditions ​is linearly proportional to the logarithm of their mass​.
- Protein that differ in mass by about 2% (50 and 52 Daltons) close to 10 amino acids can be distinguished.
- Staining proteins with Coomasie blue allows to display $`0.1 \mu g`$ of a protein.
- Staining proteins with sliver stain allow to displau $`0.02 \mu g`$ of a protein.

![electrophoresis](./img/03_Proteins_Proteomes/electrophoresis2.png)

### Isoelectric focusing

- Based on the relative content of acidic and basic groups.
- **Isoelectirc point** is the pH at which a protein’s net charge (z) is zero $`\rightarrow v = \frac{Ez}{f} \rightarrow`$ if z is equal to 0 then the velocity is 0 and the protein stops moving.
- Proteins undergoe electrophoresis in a **pH gradient** in a gel without SDS. Proteins move until they reach a position where the pH is equal to their pI (isoelectric point) and then stop. This method is called **isoelectric focusing** and then stop. This method is called **isoelectric focusing**.
- Proteins that differ by only one net charge can be separated.

![isoelectric focusing](./img/03_Proteins_Proteomes/isoelectric.png)

- Acidic amino acids have acidic side chains at neutral pH while basic amino acids have basic side chains at neutral pH. ​
- Carboxylic group is the side chain for acidic amino acids  while basic amino acids contain nitrogen containing groups.
- The number of the acidic and basic ​groups determine the IP.

### Two dimensional electrophoresis

- Isoelectirc focusing can be combined with SDS-PAGE to obtain very high resolution sperations.
- Horizontal separation based on isoelectric point (isoelectric focusing) and vertical separation based on mass (gel electrophoresis).

![two dimensional](./img/03_Proteins_Proteomes/2d.png)

- Using those methods with E.Coli resolve more than a thousand proteins.
- Displaying proteins doesn't mean that they are identified.
- Using **mass spectrometry** techniques in addition to two dimensional gel electrophoresis allows to identify proteins.

### Determining the success of protein purification

- At each step, several parameters are measured:
  - **Total protein**
  - **Total activity**: enzyme activity
  - **Specific activity**: dividing total activity by total protein
  - **Yield**: percentage of initial activity
  - **Purification level**: measure of increase in purity, calculated by dividing the specific activity by the specific activity of the initial extract (=at the begining)
- In the below table we can see that after the first step (salt fractionation) the purity level increased from 1 to 3 and that almost all the target were recovered (yield = 92%). We can see all the details for each step.

![purification](./img/03_Proteins_Proteomes/purification.png)

- From the figure below (SDS-PAGE analysis), we can see that the number of bands decreases in proportion to the level of purification and the amount of protein of interest increases as a proportion of the total protein present.
  - Same amount is put in the gel, but each step has less and less unwanted proteins thus there are less and less lines.
  - Since the same amount of protein is tested each time, if there are less unwanted proteins, then there are more wanted proteins thus the line is thicker.

![purification](./img/03_Proteins_Proteomes/purification2.png)

## Immunology methods

- Purication allows to explore a protein's function and structure in a controlled environment. The downside is that it is outside of its native environment, i.e the cell.
- Immunology allowed for the use of **antibodies** in order to explore the functions of proteins **within the cell**.
- Antibodies are extremely specific of their target proteins thus they can be used to target a protein to be isolated, quantified and visualized.
- Antibodies are also called **immunoglobin - Ig**.
- Antibodies are also proteins whose role is to target foreign substance called **antigen**.
- The antibody identify a specific group of amino acids on the target molecule called **epitope** or **antigenic determinant**. The shapes of the antibody and the epitope are complementary.
- The introduction of an antigen in an animal will cause the proliferation of antibodies-producing cells which will start to produce a lot of antibodies.
- In order to produce antibodies, a rabbit is injected twice at 3 weeks apart with a protein that acts as an antigen. This causes the cells that produce antibodies to replicate.
- Drawing blood from the rabbit and centrifuge it to separate blood cells from the **serum** which is called **antiserum** and contains antibodies to all antigens in the rabbit including the ones for the injected proteins.
- Several antibodies might be produce for one antigen, each indetifying one specific part of the antigen. Those antibodies are called **polyclonal** and are produced from several antibody-producing cells.
- **Monoclonal antibodies** - one antibody for one protein:
  - Antibody-producing cells have a short life spans outside of an organism.
  - Immortal cell lines that produce monoclonal antibodies are derived from a type of cancer that produces those specific cells in large quantities.
  - Fusing one of those cell with a short lived antibody-producing cell allows to produce the desired antibodies (the cells producing the right antibodies are isolated and grown).
  
### Enzyme-linked immunosorbent assay (ELISA)

- More sensitive than Bradford method.
  - Bradford cannot distinguish between quantities (at least not small ones) whereas ELISA has a sensitivity in $`ng`$.
- Enzyme that reacts with a colorless substrate to produce a colored product.
- Using monoclonal antibodies is more reliable.
- The enzyme is covalently linked to the antibody.
- When the antibody binds to the antigen, and after the addition of substrate, the linked enzyme catalyzes the reaction generating colored product.
- Used to identify HIV.
- There are several methods to ELISA, 2 of them are the **indirect** and the **sandwich** methods:
  - The **indirect** method is used when determining the concentration of a sample of a specific antigen (no other antigen in the sample).
    - Knowing for a fact that the antigen is in the sample and should be in high quantity, allows to put the sample first.
    - This allows to be sure that every antibody in the well is bonded to antigens.
  - The **sandwich** method is used when looking for the antigen in an unknown sample (if it is present or not and how much of it).
    - This method allows to filter out other molecules since only the wanted antigen can bond with the antibody.
    - If indirect method used then other molecules would bind to the well.
- The **indirect** method: 
  - Coat the wells with antigens and wash.
  - Block (coat with unrelated proteins what was not coated by the antigens). Wash
  - Add antibodies and wash.
  - Add enzyme-linked antibody and wash.
  - Add substrate and check the OD.
- The **sandwich** method: 
  - Coat the wells with andtibodies and wash.
  - Block (coat with unrelated proteins what was not coated by the antibodies). Wash
  - Add antigens and wash.
  - Add enzyme-linked antibody and wash.
  - Add substrate and check the OD.

![elisa](./img/03_Proteins_Proteomes/elisa.png)

### Western blotting

- A sample goes through electrophoresis on an SDS-polyacrylamide gel.
- Proteins are transferred onto a polymer sheet.
- An antibody, called the **primary antibody** specific for the protein is added and reacts with the antigen.
- A **secondary antibody** specific for the primary antibody is added.
- This secondary antibody is fused to an enzyme that producesa chemiluminescent or colored product, or contain a fluorescent tag allow the identification of the protein.
- Used to identify hepatitis C.

![western blotting](./img/03_Proteins_Proteomes/wb.png)

### Fluorescent markers

- Allows to examine proteins within the cell.
- Cells are stained with fluorescence-labeled antibodies and examined by fluorescence microscopy (nm range) to find the protein of interest.
- For example actin filaments (identified through antibodies to actin).

![actin filaments](./img/03_Proteins_Proteomes/actin.png)

### Ultracentrifugation

- Ultra-centrifugation can help learning mass and  density of molecules. We can also learn about the shape of the molecules and their interaction with one another.
​- A particle will move through a liquid medium when subjected to a centrifugal force​.
- An issue with this is that the medium (buffer) will go upward as the particles go down disrupting their layer.
  - A sucrose gradient is used to help stabilize the process.
- A convenient mean of quantifying the rate of movement​ is to calculate the sedimentation coefficient S: 
  - **m**: mass
  - **v**: partial specific volume (reciprocal of density)
  - $`\bold{\rho}`$: density of medium
  - **f**: friction of particle (measure of shape of particle)
- The smaller the S value the slower a melecule moves in a centrifugal field.

```math
\boxed{S = \frac{m(1-vp)}{f}}
```

- Conclusions from the above equation:
  - The sedimentation  velocity of a particle depends in part on its mass.
  - Shape influence sedimentation velocity – it affects viscous drag.
  - Dense particles move more rapidly than a less dense particles. 
  
![centrifugation](./img/03_Proteins_Proteomes/centrifugation.png)

- **Velocity sedimentation**: Sedimentation velocity separate proteins **differing in S coefficient** by a factor of two or more. If left too long all the particles will reach the bottom.
- **Equilibrium sedimentation**: **Based on density** of components and surroundings. No matter how long the particles are in the medium they cannot reach the bottom.

![velocity sedimentation](./img/03_Proteins_Proteomes/velocity.png) ![equilibrium sedimentation](./img/03_Proteins_Proteomes/equilibrium.png)

## Unsummarized

- PPT3 - slides 71-75
