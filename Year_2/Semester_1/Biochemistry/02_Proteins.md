# Proteins

- **GFP** is a gene that gives jellyfish their fluorescent colors. It was isolated and succefully put in mice.
- Proteins are important macromolecules used in various biological processes. 
- They constitute most of the cell dry mass.
- They are polymers build from amino acids (monomers).
- Almost all species have the same alphabet of 20 amino acids.
- There are 3 or 4 steps to build a protein:
  - **Primary structure**: polypeptide chain.
  - **Secondary structure**: 3D structure formed by _hydrogen bonds_ between amino acids.
  - **Tertiary structure**: more complex structure made by long range of interactions between amino acids.
  - **Quarternary structure**: complexes made by interactions between multiple tertiary structures. Those tertiary structures cannot function alone.
- The **shape of the protein determines its funtions**.
- The functional groups in proteins are essential to their function. Each functional group giving different properties.
- Some of the functional groups:
  - Alcohols $`R-OH`$ -- those are very active
  - Thioles $`R-SH`$
  - Thioethers $`R-S-R'`$ -- volatile and has foul odor, similar to ether.
  - Carboxillic acids $`R-COOH`$ -- very active
  - Carboxamides $`R-CO-NR_2`$
- Proteins can interact with one another and with other macromolecules to form complex assemblies expanding their capabilities as single units.
- Proteins can be **rigid or flexible**.
- They are the major building blocks of the cells and constitute most of a cell dry mass.
- **Rigid** proteins are used as structural elements in the cytoskeleton or in connective tissue.
- **Flexible** proteins are used as hinges, springs, or levers.

## Amino acids

- ɑ-amino acid: central carbon (ɑ-carbon) linked to an amino group, carboxilic group, an hydrogen and an R group.
- The R group is also called the **side chain**.
- **Aliphatic compound**: hydrocarbon with or without ring that doesn't contain a aromatic ring.

![amino acid](./img/02_Proteins/amino_acid.png)

- Amino acids are **chiral** meaning they can exist in one form or their mirror image (L and D isomers).
- **Only L isomers are found in proteins**. It is thought that L isomers were "chosen" to be used in proteins due to the fact that they are a bit more soluble than D isomers. Racemic solutions of L and D isomers tend to form crystals.

![chiral](./img/02_Proteins/chiral.png)

- At **neutral pH**, the amino group and the carboxilic group are **ionized**: $`NH_3^+`$ and $`COO^-`$.
- In **acidic** solutions the amino group is protonated $`NH_3^+`$ but the carboxilic keeps its H $`COOH`$.
- In **basic** solutions the carboxilic group is deprotaned $`COO^-`$ but the amino group isn't protonated $`NH_2`$.

![amino acid](./img/02_Proteins/ionized_aa.png)

- Amino acids can be sorted into 4 groups:
  - **Hydrophobic**: non polar R group.
  - **Polar**: neutral R group but with unvenly distributed charge.
  - **Positively charged**: R group has a positive charge.
  - **Negatively charged**: R group has a negative charge.
- Below are listed the amino acids per group that need to be known:

### Hydrophobic amino acids

- Glycine, Alanine, Valine, Leucine, Methionine, Isoleucine
- **Glycine** is **achiral**. Its R is a lone hydrogen, because the ɑ-carbon is already bonded to another hydrogen, this amino acid is achiral.
- **Alanine**: its R is a **methyl group**.
- **Methionine** has a **thioether group (R-S-R)**.
- Proteins with hydrophobic area will take advantage of the hydrophobic effect to get close to one another.

| Glycine                                   | Alanine                                   | Methionine                                      |
|-------------------------------------------|-------------------------------------------|-------------------------------------------------|
| ![glycine](./img/02_Proteins/glycine.png) | ![alanine](./img/02_Proteins/alanine.png) | ![methionine](./img/02_Proteins/methionine.png) |

### Polar amino acids

- Serine, Threonine, Cysteine, Proline, Asparagine, Glutamine.
- Proline has a (non aromatic) ring between the ɑ-carbon and the amino group which makes it less flexible, there are less conformation possible.
- Serine and Threonine have and $`OH`$ group making them hydrophilic and reactive.
- Cysteine, looks like serine but with a thiol group ($`-SH`$) instead of the $`OH`$ group. The thiol group is much more reactive. This group can form **disulfid (SS) bonds** which are important to stabilize the protein and form tertiary structures.

| Cysteine                                    | Serine                                  | Threonine                                     | Proline                                   |
|---------------------------------------------|-----------------------------------------|-----------------------------------------------|-------------------------------------------|
| ![cysteine](./img/02_Proteins/cysteine.png) | ![serine](./img/02_Proteins/serine.png) | ![threonine](./img/02_Proteins/threonine.png) | ![proline](./img/02_Proteins/proline.png) |

### Positively charged amino acids

- Lysine, Arginine, Histidine
- Those are highly **hydrophilic**.
- Lysine and Arginine have relatively long side chains that terminate with positive charge at neutral pH.​
- Histidine has an aromatic ring that can be positively charged. Histidine is often found in enzyme since it can either bind or release a proton.

| Lysine                                  | Arginine                                    | Histidine                                     |
|-----------------------------------------|---------------------------------------------|-----------------------------------------------|
| ![lysine](./img/02_Proteins/lysine.png) | ![arginine](./img/02_Proteins/arginine.png) | ![histidine](./img/02_Proteins/histidine.png) |

### Negatively charged amino acids

- Aspartate, Glutamate
- There are some proteins that do accept protons, often functionally important.

### Ionization of R

- As said above, the amino group and the carboxilic group can be ionized at neutral pH. The side chain R can also be ionized, $`\bold{pK_a}`$ is used to express when the side chain is ionized. _For example, cysteine is ionized at a $`pK_a`$ of 8.3._
- **7 of the 20 amino acids have ionizable side chain**.
- **$`\bold{pK_a}`$ is the pH at which half of a chemical group is ionized and the other half isn't**.
- The ionization of R (donate or accept a proton) facilitates reactions and forming ionic bonds.

<br><br>
<br><br>

- More than 20 amino acids exists but only 20 of them are used in synthesizing proteins. Why? We don't really know but some thoughts are:
  - Those amino acids are different from one another allowing for versatility.
  - Other amino acids might be too reactive to form stable proteins.
  - Availability from pre-biotic reactions that occurred before the origin of life.​
- Other amino acids are too reactive to be in proteins. _For example: homoserine and homocystein tend to form 5 member cyclic forms that limit their use in proteins._

## Structures

### Primary Structure

- Proteins are formed by **linking the ɑ-carboxyl group of one amino acid to the ɑ-amino group of another amino acid**.
- This forms linear polymers called **polypeptide chain**.
- The link between the amino acids is called a **peptide bond**.
- The bond formation between the amino acids causes the loss of a water molecule.
- Each amino acid in the polypeptide chain is called a **residue**.
- The reaction equilibrium tends to go more on the side of **hydrolisys** but because the hydrolisys is so slow (1000 years) the peptide bonds are actually stable (kinetically). They do require an input of free energy.
- The polypeptide chain has a **direction** since both ends are different: the amino residue starts the chain and is called the **N-terminal** and the carboxyl residue end the chain and is called the **C-terminal**. 
- The **backbone** N-C-C-N-C-C contains a lot of hydrogens.
  - Carbonyl groups (C=O) which is a good hydrogen-bond acceptor. 
  - The NH group is a good hydrogen-bond donor.
  - Those two groups come from the carboxyl and amino groups of the amino acid.

![backbone](./img/02_Proteins/backbone.png)

- Polypeptide chains can have between 50 and 2000 amino acids.
- **Titin** is a muscle protein that has 27000 amino acids
- The molecular weight (MW) is usually expressed in **daltons** where 1 dalton = 1 amu (~ the mass of the hydrogen atom). _A protein with and MW of 50000g/mol has a mass of 50000daltons (50kDa)_.
- Cysteines can bond with **disulfide bonds**. Two bonded cysteines are called **cytine**.
  - Dissulfide bonds are usually found in **extracellular proteins** (outside of the cell).
  - Dissulfide bonds are created by oxydation and broken by reduction.
- **Frederick Sanger**:
  - Amino acid sequence of insulin.
  - The insulin is composed of two polypeptides (A & B) linked by dissulfide bonds.
  - Proteins have a defined amino acid sequence consisting only of L amino acids.
- In the late 50s early 60s, it was revealed that the amino acid sequence comes from the nucleotide sequences of genes.
- Each amino acid is coded by **codons**, sequences of three nucleotides.
- The nucleotides in DNA allows for a **complementary** RNA sequence to be made which is used to create the amino acid sequence.
- The polypeptide chain determines how the protein will fold i.e its 3D shape. Also its function can be deduced by the sequence.
- Altering the amino acid sequence can:
  - Create proteins with new properties.
  - Lead to abnormal function and disease such as sickle-cell anemia.
- Proteins can look alike if they have a common ancestor. This helps retrace evolution events.

#### Geomotry of the polypeptide chains

- It is mostly **planar**.
- The backbone is made of single bonds which allow rotations but there is **resonance** between the C=O group and C-N group which allow less conformations.

![resonnance](./img/02_Proteins/peptide_bond.png)

- The length of the C-N bond is between the length of a single and double bond of this type.
- The peptide bond is uncharged.
- It can have two configurations: cis and trans. **Trans is prefered** since it has less steric strain.

![cis and trans](./img/02_Proteins/cis_trans.png)

- Cis configuration is prefered with **X-proline** linkage. The ring makes is so the steric strain is lessened in cis configuration.

![x pro linkage](./img/02_Proteins/x_pro.png)

- The single bonds are free to rotate and are specified by the **torsion angles**:
  - The angle of rotation between ɑ-carbon and nitrogen is called _phi_ $`\bold{\phi}`$.
  - The one between the ɑ-carbon and carbonyl carbon is called _psi_ $`\bold{\psi}`$.
- 3/4 of the $`\phi`$ and $`\psi`$ combinations are not possible because of steric strain.
- A 2D plot called the **Ramachandran plot** shows the the possible combinations.
- Unfolded polymer exists as a random coil with each copy having a different conformation. ​
- The favorable entropy associated with a mixture of many possible conformation opposes folding and must be overcome by interactions favoring the folded form. ​

### Secondary Structure

- In 1951, Pauling and Corey proposed the structure called  ɑ-helix and $`\beta`$ pleated sheet.

#### Alpha helix

- Coiled structure stabalized by intrachain **hydrogen bonds between NH and CO groups**.
- The CO group of each amino acid forms a hydrogen bond with the NH group of the amino acid situated **4 residues ahead**.
- Amino acids that are 2 residues apart are on opposite side of the helix and wont react together.
- The side chain point outward.
- Right handed helices are more common than left handed since there is less steric strain between the side chains and the backbone. Pretty all helices are right handed.
- Ferritin is a protein with 75% of residues are ɑ helices.
- Many proteins within the biological membranes also contain ɑ helices.
- Not all amino acids can be part of ɑ helices. _Proline, because of its ring, lacks the NH group (lacks the H to give)_.

![alpha helix](./img/02_Proteins/alpha_helix.png)

#### Beta sheet

- Composed of at least two polypeptide chains called **$`\bold{\beta}`$ strands**. The two chains can come from the same bigger polypeptide chain.
- $`\beta`$ strands are extended, the distance between amino acids in $`\beta`$ sheets is 3.5$`\text{\AA}`$ whereas in ɑ helices it is 1.5$`\text{\AA}`$.
- Like ɑ helices, $`\beta`$ sheets are formed by **hydrogen bonds**.
- The strands can run in the **same or opposite direction (parallel and antiparallel)**.
- In an **antiparallel $`\bold{\beta}`$ sheet** the CO and NH groups of two amino acid will bond (two amino acids form two bonds).
- In a **parallel $`\bold{\beta}`$ sheet** one amino acid will bond to two other amino acids: its CO group will bond with the NH group of one and its NH group will bond to the CO group of another.

![antiparallel and parallel](./img/02_Proteins/parallelism.png)

- β-sheets usually have 4 or 5 strands but they can have as many as 10.
- They can be all anti/parallel or mixed.
- Most β-sheets adopt a twisted shape.
- The β-sheets are important in **fatty acid binding proteins** which is important for lipid metabolism.

#### Turns and Loops

- Polypeptide chain can change direction by making reverse turns and loops.
- The globular shape of proteins is due to the reversal in the direction of their polypeptide chains.
- **Reverse turn** or **hairpin turn** causes a turn by having a CO group bonded to an NH group 3 residues ahead.
- **Loops** or **Ω loops** are more complex and also allow to stabilize turns.
- Turns and loops are located on the surface of the proteins and thus **participate in interactions between proteins and other molecules**.

#### Special structures

- In **ɑ-keratin** and **collagen** are found special helices.

##### ɑ-keratin

- In ɑ-keratin there are **superhelices** also called **ɑ-helical coiled coil**: two right-handed ɑ helices intertwined to form bigger **left-handed** helix.
- Those are made to provide **structural support** for cells and tissues.
- The two helices are cross linked by **van der Waals** forces and **ionic interactions**. **Disulfide bonds** between cysteines may also take place in those helices​.
- They can be at least $`\text{1000\AA}`$ long.
- They also require a little bit less residues per turn than ɑ helices (3.5 instead of 3.6).

![superhelices](./img/02_Proteins/superhelices.png)

> Wool or curly hair return to their curly form when extended.  
> When streching them, weak bonds are broken in the superhelices. The disulfide bonds resist breakage allowing the wool or hair to go back to its original form.  
> Horn and claws contain a lot more disulfide bonds which make them a lot more resitant.  

##### Collagen

- Collagen is the most abundant protein in mammals. It is present in skin, bone, tendon, cartilage and teeth.
- In collagen the helix present is about $`\text{3000\AA}`$ long.
- It is made of **3 helical polypeptide chains**.
- **Glycine** appears at every third residue and the sequence **glycine-proline-hydroxyproline** recurs frenquently.
- The helix in collagen is stabalized by **steric repulsion of the proline's ring** and **not** by hydrogen bonds.
- Strands are held by hydrogen bonds between them, Glycine on one hand, NH and CO on other residues. ​
- OH on proline also participate in hydrogen bonding.​
- Only glycine can fit inside the helix. That's why it is present every 3 amino acid. The rest of them are located on the outside.
- **Osteogenesis imperfecta** is a disorder due to glycine being replaced by another amino acid which causes the improper folding of collagen.

### Tertiary Structure

- Water soluble proteins fold into compact structures with nonpolar cores.
- **Myoglobin**, the protein charged with storing oxygen in muscles, was the first to be seen in atomic detail.
- It is an extremely compact protein of 153 amino acids.
- **Heme** is a **prosthetic group** that allows myoglobin to bind oxygen.
- 70% of the main chain is folded into ɑ helices and the rest has a lot of turns and loops.
- **Inside** most residues are **nonpolar**. **Charged** residues aren't found inside.
- **Outside** are found polar and nonpolar residues. In an **aqueous** environment the nonpolar residues fold within.
- There are some exceptions like **porins** proteins placed in membranes (membranes are mainly composed of hydrophobic blocks). Their hydrophobic residues are outside creating a water filled hydrophilic channel.
- Without water, unpaired amino acid of a protein (like in a binding site) will react together which is good in α-helix and β-sheet.

### Quaternary Structure

- **Subunits** are polypeptide chains composing the quaternary structure.
- Those cannot be predicted (like tertiary can be from the polypeptide chain).
- A **dimer** composed of indentical subunits is the simplest quaternary structure.
- The **Cro** protein is composed of this type of dimer.
- **Hemoglobin** is composed of 4 subunits of two different types (two of one type and two of the other).
- Viruses have limited genetic information. In order to make the most of it, they build the same kind of subunits again and again and create a **coat** with them which is a sort of capsule.

## Amino acid sequence

- **Affinsen** in the 50s, with the enzyme ribonuclease that has 124 amino acids and 4 SS bonds, showed the relation between the amino acid sequence and it conformation.
- **8M Urea** and **guanidinium chloride** can break proteins non-convalent bonds as well as prevent them to form.
- **$`\bold{\beta}`$-mercaptoethanol** is a reagent which is able to reduce the disulfides (cystines) back into cysteines.
- A **denatured** protein is a protein which has lost its shape and function.
- Using **dialysis** to remove the 8M urea and guanidinium, the enzyme (protein) is **oxidized** by air and refolds spontaneously and regains its function.
- Reoxidizing the enzyme while still in 8M urea and then removing the urea causes it to regain only 1% of its enzymatic activity, because the **wrong disulfides formed**. This is called **scrambled** ribonuclease.
- Adding **traces of $`\bold{\beta}`$-mercaptoethanol** allows the rearrangement of the disulfide bonds. After 10 hours the enzyme regains all of its enzymatic activity.
- The process is driven by decrease in free energy as scrambled conformation is converted into native more stable conformation (thermodynamically preferred structure)​.
- Not all denatured proteins can be restored: **aggregates** are tangled proteins that could not be restored to their original form.
- In cells the **chaperons** proteins are responsible for the well folding of the proteins and block all undesirable interactions like heat.
- When under stress, some chaperons called **heat shock proteins** (like **hsp70**) help protect the cell by stabilizing unfolded or misfolded peptides, giving the cell time to repair or re-synthesize damaged proteins.
- Some amino acids are more common in certain strutures (i.e ɑ-helix, $`\beta`$-sheets, turns):

| ɑ-helix   | β-sheet    | Reverse turn |
|-----------|------------|--------------|
| Glutamate | Valine     | Glycine      |
| Alanine   | Isoleucine | Proline      |
| Leucine   |            | Asparagine   |

- Looking at the branching at the β-carbon explain why certain amino acids are more common in certain structures:
 - Steric clashes happen when valine, threonine and isoleucine are in ɑ-helices.
 - Proline lacks the NH group which causes disruption in ɑ-helices and β-sheets.
 - Hydrogen bond donors/acceptors compete with the main chain NH and CO groups thus are not fitted for ɑ-helices (serine, aspartate, asparagine).
 - Glycine fits all structures but because of its flexibility it is most common in reverse turns.
- Predicting secondary structures from amino acids sequences is sometimes difficult du to some sequences like **VDLLKN** that are found in some proteins in α-helix and in others in β-sheet.
- The folding/unfolding of a protein is a sharp transition: all or nothing. If even only a part of the protein is denatured, it will break its bond with the other part of the protein which will then also get denatured. This is called **cooperative process**.
- Although it always appear as if there is no intermediate state, this impossible because all reactions go through intermediate reactions thus to go through folded $`\rightarrow`$ unfolded state and vice versa, the protein has to go through unstable intermediates.
- **Levienthal's paradox**: enormous difference between the time calculated to fold a protein vs the time it actually takes.
- Proteins don't fold by trying all comformations it would take too long. Instead they must go through intermediates building on top of one another while stabilizing each other. It called **cumulative selection**.
- **Nucleation Condensation Model** suggests that certain pathways may be preferred, and the protein follows a general pathway rather than a precise pathway from unfolded state to folded state​.
- There are two approaches to try and predict 3D folding:
  - **ab initio** (from the beginning) prediction: without prior knowledge about similar sequences. Computer based calculations.
  - **knowledge based methods**: check compatibility with other known structures that resemble the unknown one.
- Some proteins can adopt 2 different structures, one of which will result in aggregation and pathological conditions.
  - **Intrinsically Unstructured Proteins (IUPs)**: they have regions without a specific shape. The unstructured regions are rich in charged and polar amino acids which give them versatility. The protein can take different shapes and interact with different molecules. This is important in signaling and regulatory pathways​.
  - **Intrinsically Disordered Proteins (IDPs)**: they lack a fixed or ordered three-dimensional structure.
  - **Metamorphic proteins**: they exist in multiple forms of about equal energy and are in equilibrium. Ex: **chemokine**: signaling molecule in the immune system, each form has its function, one form cannot perform the function of the other.
- **Amyloidoses** are diseases (e.g: Huntington, Alzheimer, Parkison) caused by improperly folded proteins resulting in the deposition of protein aggregates called **amyloid fibrils or plaques**.
- **Prions** are neurological diseases which are transmitted by agents consisting of proteins and not viruses. ​
  - Bovine Spongiform Encephalopathy (mad cow disease)​, in humans Creutzfeldt-Jacob disease, in sheeps the Scrapie.
- Prions are composed of proteins called **PrP** which are normal proteins in the brain. Prions are the aggregated form of the protein which loses its 3D shape for unknown reasons.
- It is thought that a lot of tuners and ɑ-helices are converted into β-sheets which stack on one another causing the amyloid form.
- Proteins can be synthetically modified by attaching functional groups:
  - Adding OH group to many proline residues **stabilizes fibers of newly synthesized collagen** (this helps with vitamin C deficiency).
  - Addition of carbohydrates to asparagine residues – make proteins more hydrophobic and allow them to interact with other proteins. ​
  - Many hormones like epinephrine (adrenaline) activates enzymes by Phosphorylation​ of the hydroxyl amino acids serine and threonine – signal transduction ​
  - Insulin act by triggering phosphorylation of the OH group in tyrosine residues.
