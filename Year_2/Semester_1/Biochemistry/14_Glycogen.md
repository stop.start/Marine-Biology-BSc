# Glycogen & Gluconeogenesis

- Synthesis of glucose from sources that are not carbohydrates (e.g amino acids).
- Anabolic process
- If there's enough glucose, it is stored as glycogen (in plants as starch).
- Most storages are located in the liver and muscles.
- The brain gets its energy only from glucose and not from fat.
- The liver provides most of the glucose in the blood.
- Muscles can use all of their stored glucose with an hour (thus stored glycogen is not enough as a source of glucose).
- If the body is starved it will first get the stored glycogen and will undergo glycogenesis after the glycogen is emptied.

## Glycogen

- Polymer of D-Glucose linked with bonds $`\alpha 1,6`$ and $`\alpha 1,4`$.
  - The $`\alpha 1,4`$ bond creates chains of glucose.
  - The $`\alpha 1,6`$ bond links between chains and creating branches.
  - **Reducing ends** are located at the end of chains with a free 1 carbon.
  - **Nonreducing ends**  are located at the end of chains with a free 4 carbon.
  
![polymer](./img/14_Glycogen/polymer.png)

### Breakdown

- **Glycogenolysis**: breaking down of glycogen with the help of 3 enzymes:
  - **Glycogen phosphorylase**: 
    - Speed up the break down of one glucose from the non-reducing end (if there's no branch within 5 units).
    - Produces **G1P (glucose-1-phosphate)**.
    - Can be in two states: active (phosphorylated) or less actice (not phosphorylated).
  - **Glycogen debranching**: breaks down branches (slower process).
  - **Phosphoglucomutase**: converts G1P to G6P which then can be oxidized in glycolysis. It does it by moving a phosphate group from the 1 carbon to the 6 carbon.
- **Glucagon** is a catabolic hormone responsible for starting the process of glycogenolysis.
- **Insulin** is the anabolic hormone responsible for storing glucose as glycogen and inhibiting glycogenolysis.

![glycogenolysis](./img/14_Glycogen/glycogenolysis.png)

- In the liver G6P is converted into glucose and then passed on into the bloodstream.
  - It is done by hydrolysis by **glucose-6-phosphatase** in the ER.
  - G6P is brought to the ER by G6P translocase.
  - Afterwards, **GLUT2** brings the glucose to the bloodstream.

### Synthesis

- G6P is converted into G1P (this is actually in equilibrium).
- UDP-glucose is obtained by phosphorylating a UTP molecule (like ATP).
- **Glycogen synthase** synthesises glycogen by releasing the UDP from the glucose (which is attached to the chain).

![summary](./img/14_Glycogen/summary.png)

## Gluconeogenesis

- The liver can synthesize glucose from lactate, pyruvate and amino acids.
- Gluconeogenesis ressembles glycolysis in reverse.
- Some of the reactions in glycolysis are exergonics (release free energy and thus are not reversible).
  - Those are replaces by other reactions (in red in the picture below).
  
  ![reverse glycolysis](./img/14_Glycogen/reverse_glycolysis.png)

- First reaction is 2 steps (1 in glycolysis): pyruvate to phosphoenolpyruvate.
  - Pyruvate carboxylase requires ATP (pyruvate into oxaloacetate).
  - PEP carboxykinase requires GTP (oxaloacetate into PEP).
  - The first step is done in the matrix of the mitochondria.
  - Because oxaloacetate cannot go through the membrane it is temporarily converted into malate and back once in the cytoplasm.
    - In the matrix this requires an NADH which is recreated once in the cytoplasm.
    - The real reason for the conversion is the need of NADH in the cytoplasm in a later step.
- If lactate and not pyruvate, it is first converted into pyruvate in the cytoplasm by creating an NADH.
  - It is transformed into PEP inside the matrix of the mitochondria (PEP then goes to the cytoplasm).
- **Creation of one glucose from 2 pyruvates requires 6 ATP (4ATP + 2GTP).**
- Glycolysis creates 2ATP thus recycling the pyruvates from it **generates a net of 6-2=4ATP.**

![cori cycle](./img/14_Glycogen/cori.png)
