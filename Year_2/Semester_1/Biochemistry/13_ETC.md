# Electron Transport Chain

- Respiration:

```math
\begin{aligned}
& \text{Oxidation of glucose (from glycolysis to ETC in the mitochondria:)}\\
& \boxed{C_6H_{12}O_6 + 6O_2 \rightarrow 6CO_2 + 6H_2O}\\
& \text{Glycolysis + Krebs cycle:}\\
& C_6H_{12}O_6 + 6H_2O \rightarrow 6CO_2 + 24 H^+ + 24e^- \\
& \text{Electron transport chain in the mitochondria:}\\
& 6O_2 + 24H^+ + 24e^- \rightarrow 12H_2O \\
\end{aligned}
```

![glucose oxidation](./img/13_ETC/glucose_oxidation.png)

- The Krebs cycle produces **8 electrons** that are then send to the electron transport chain which creates a proton gradient of **36 $`H^+`$** which is the required energy to then produce **9** ATP.
- **Oxidative phosphorylation** (חימצון זירחוני) is the process which produces ATP as a result of the transport of electrons from $`FADH_2`$, NADH, to $`O_2`$ with the help of protein complexes in the internal membrane of the mitochondria.
- The transport of electron creates a proton gradient (meaning pH gradient) on both sides of the membrane. This creates an electric potential which creates the **proton motive force** (force moving the protons).
- The 24 electrons in the reaction come from **10 NADH and 2 FADH** (for one glucose, 12 per pyruvate).
- NADH and FADH are oxidized (give their electrons). Those electrons go through the redox centers and then are used to reduce oxygen into $`H_2O`$.
- During the transport of electrons, protons are released in the intermembrane space and create an electric gradient which dirves the production of ATP.
- The outer membrane of the mitochondria has **porins** which allow the passage of big proteins.
- The inner membrane of the mitochondria is permeable to water, oxygen and $`CO_2$ through transporters.
- **ADP-ATP translocase** export ATP from the matrix and import ADP.
- The proton gradient also allows to bring phosphate ($`Pi-H^+`$).
- The electrons go from low reduction potential to high reduction potential.
- The electrons go through complexes: Complex I, Complex II, Coenzyme Q, Complex III, Cytochrome c, Complex IV.

![diagram](./img/13_ETC/diagram.png)

- **Complex I** oxidizes NADH to give electrons to CoQ via ISC (iron-sulfur clusters) and passes **4 protons** into the intermembrane space.
  - Contains cofactor FMN.
- **Complex II** oxidizes $`FADH_2`$ via **succinate** and, like complex I, transfer the electrons to CoQ.
- **Coenzyme Q** transfers the electrons to complex III.
- **Complex III** transfers the electrons to cytochrome C and passes **4 protons** into the intermembrane space.
- **Cytochrome c** transfers the electrons to complex IV.
- **Complex IV** transfers the electrons to an $`O_2`$ molecule to form water and passes **2 protons** into the intermembrane space.
  - Contains cytochrome a3, cytochrome a, reduction center CuA and CuB.
  - In all, 4 protons are passed to the intermembrane space (one for each oxygen molecule).
  - $`4 \text{Cytochrome c}(Fe^{2+}) + 4H^+ + O_2 \rightarrow 4 \text{cytochrome c}(Fe^{3+}) + 2H_2O`$

![etc1](./img/13_ETC/etc1.png)
![etc2](./img/13_ETC/etc2.png)

- The reduction potential of NADH is lower than of $`FADH_2`$.
- More energy is exracted from NADH than $`FADH_2`$.
- Cofactor FMN and coenzyme Q both can be in 3 different oxidized state: oxidized, free radical, reduced.
- Electrons from glycolysis are sent to the mitochondria's membrane with the help of an enzyme on the mitochondria's membrane that fixes NADH and transfers its electrons to FAD (loss of energy in this process).
- Coenzyme Q can receive 2 electrons. This transforms it from **ubiquinone (Q)** into **ubiquinol ($`QH_2`$)**.
  - This creates an issue because cytochrome c can only transfer one electron at a time.
- **4 cytochrome c** are needed to reduce one molecule of $`O_2`$.
- **ATP synthase** is the enzyme that creates ATP.
  - Protons go through it down their gradient.
  - Composed of 2 functional units: F0 in the internal membrane and F1 in the matrix.
  - F0 is composed of 8 polypeptides.
    - In E.coli it contains **3 units**: a1b2c12 (= 1 of a, 2 of b and 12 of c).
    - C units form a ring: **the rotor**. There can be between 10 and 15 depending on the organisnm.
    - A turn of $`120\degree`$ in the rotor = 1 ATP (4 $`H^+`$).
    - In E.coli, to make a turn of $`360\degree`$ in the rotor (= 3 ATP) 12 protons are needed.
  - F1 is composed of 5 subunits polypeptides ($`\alpha`$, $`\beta`$, $`\gamma`$, $`\delta`$, $`\varepsilon`$).
    - $`\alpha \beta`$ couples have each their affinity to ADP and ATP.
    - Each $`\alpha \beta`$ couple can be in one of 3 states:
      - O (open)
      - T (strongly attached)
      - L (weakly attached)


![f1](./img/13_ETC/f1.png)
  
  ![atp synthase](./img/13_ETC/atp_synthase.png)`

- To use 1500-2000 calories in a day 200mol of ATP need to be hydrolized.
- The amount of ATP at any time in the body is 0.1mol.
- Anaerobic glycolysis = 2 ATP
- Aerobic glycolysis = 32 ATP (actually 30 ATP).
- Partial reduction of oxygen can create ROS (reactive oxygen species) like superoxide radical.
  - This happens for about 2% of the electrons (meaning that for 98% of the electrons water is formed).
- It is possible to break the alliance between the electron transport chain and the ATP synthesis by affecting the electrical gradient.
  - Compounds like 2,4-Dinitrophenol bond with protons and cross the membrane, canceling the gradient. 
  - The energy used to create ATP is released as heat (the body is thus getting hot).
- There are also compouds that inhibits the transport of electrons.
