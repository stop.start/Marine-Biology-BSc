# Glycolysis

## Sugars

- **Carbohydrates** (פחמימות) or **saccharides** are the most common biological molecules.
- They are built from carbon, hydrogen and oxygen with a ratio of: 1:2:1 ($`\bold{C_nH_{2n}O_n}`$).
- Monosaccharides: glucose, fructose, galactose.
- Polysaccharides: 
  - Disaccharide: sucrose, lactose 
  - Polysaccharides: starch (עמילן), glycogen
- Aldose: carbonyl group is an aldehyde.
- Ketose: carbonyl group is a ketone.
  
|                                                         |                                               |
|---------------------------------------------------------|-----------------------------------------------|
| ![carbohydrates](./img/11_Glycolysis/carbohydrates.png) | ![carbonyl](./img/11_Glycolysis/carbonyl.png) |
  
- Examples of ketoses and aldoses sugars:

![aldose ketose](./img/11_Glycolysis/aldose_ketose.png)

- Sugars can be found in linear or ring forms. There's an equilibrium between the two forms.
- Sugars can form 5 membered rings called **furan** or 6 membered rings called **pyran**.
  - **Sucrose**: Fructose (ketose and furan) + Glucose (aldose and pyran)
  - **Lactose**: Galactose (pyran) + Glucose (pyran)
  
![pyran furan](./img/11_Glycolysis/pyran_furan.png)
  
- The most common dissaccharide is sucrose.
- Artificial sugars (saccharin - gives cancer -, aspartame, stivia) are not sugars. They only taste like sugar and do not go through the metabolism process and are excreted as is.
- Stored energy:
  - Starch (plants)
  - Glycogen (animals)
- Building blocks:
  - Cellulose
  - Chitin
- Cellulose and chitin can not be broken down by vertebrates.
- Cellulose is the source of 50% of the carbon in the biosphere.

## Glycolysis

- Only pathway forming ATP without the need of oxygen.
- Started 3.5M years ago in anaerobic bacteria (before there was oxygen on Earth).
- 10 reactions that transforms glucose into 2 **pyruvates** and from them 2 ATP molecules.
- Glucose appears in the bloodstream by breaking down polysaccharides into monosaccharides.
- Glucose gets into the cell via specific transporters.
- **Gluconeogenesis**: creation of glucoses when there's a lack of it.
- Glycolysis happens also in red blood cells that don't have mitchondria and muscle cells without the use of oxygen.
- With oxygen pyruvates can make ATP but without oxygen they will make **lactate**.

### General Steps

- Two general steps.
- First step: **using energy**
  - 2 ATP are used in order to phosphorilate the glucose molecule and make 2 **GAP** molecules (glyceraldehyde-3-phosphate).
- Second step: **getting energy**
  - For each GAP, a pyruvate is created and 2 ATP are formed during this process as well as one NADH.
  - In addition NADH is also formed.


```math
Glucose + 2NAD^+ + 2ADP + 2P_i \longrightarrow 2 pyruvate + 2NADH + 2ATP + 2H_2O + 4H^+ 
```

![glycolysis](./img/11_Glycolysis/glycolysis.png)

- From this there's 2 pathway possible:
  - Aerobic: using oxygen to get ATP
  - Anaerobic:
    - Animals: pyruvate is transformed into **lactate** (in muscles).
    - Yeast: pyruvate is transformed into **ethanol**.

![pyruvate](./img/11_Glycolysis/pyruvate.png)

### Detailed Steps

#### Enzyme Types

| Enzyme            | Function                                     |
|-------------------|----------------------------------------------|
| **Kinase**        | Adds phosphate group                         |
| **Phosphatase**   | Breaks down phosphate                        |
| **Isomerase**     | Changes the type of sugars to other isomeres |
| **Mutase**        | Moves a phosphate within a sugar               |
| **Dehydrogenase** | Moves or removes hydrogen bonded to a P      |
| **Anolase**       | Dehydration                                  |
| **Aldolase**      | Splits or creates an aldolic bond            |

#### First Part

- First step:
  - **Hexokinase** phosphorilate the glucose with ATP and with the help of magnesium.
  - Product: glucose-6-phosphate (G6P).
  - Phosphate bonded to the glucose prevents it to get back to the bloodstream.
- Second step:
  - Enzyme: **phosphoglucose isomerase**.
  - Isomerase doesn't change the chemical composition of the molecule, only its structure.
  - G6P becomes F6P (fructose-6-phosphate).
  - 6 membered ring to 5 membered ring.
- Third step:
  - Enzyme: **phosphofructokinase (PFK)**.
  - Product: FBP (frunctose-1,6-biphosphate).
  - Another phosphate is added.
  - This a **rate limiting step** in the glycolysis.
  - This an **allosteric** enzyme.
- Fourth step:
  - Enzyme: **aldolase**.
  - Product: GAP and DHAP.
  - The FBP is cut (via aldolase) into and aldose (DHAP) and a ketose (GAP).
- Fifth step:
  - Enzyme: **triose phosphate isomerase (TIM)**.
  - **Needs energy**.
  - Depends on the diffusion of molecules in the active site.
  - DHAP is tronsformed into GAP.

![first part](./img/11_Glycolysis/first_part.png)

#### Second Part

- Sixth step:
  - Enzyme: **GAPDH**
  - Phosphorilation and oxidation of GAP.
  - Endorgenic reaction (requires energy).
  - From GAP and $`NAD^+`$ forms **NADH** and 1,3-BPG.
  - The phosphate used doesn't come from ATP.
- Seventh step:
  - Enzyme: **PGK** (phosphoglycerate kinase).
  - **First ATP created!**
  - This reaction allows the previous one (which is endergonic) to happen.
- Eigth step:
  - Enzyme: **PGM** (phosphoglycerate mutase)
  - Transfer a phosphate group.
  - Product: 2PG
- Ninth step:
  - Enzyme: **enolase**
  - Dehydration of 2PG (2-phosphoglycerate)
  - Product: PEP (phosphoenolpyrovate) and $`H_2O`$
- Tenth step:
  - Enzyme: **PK (pyruvate kinase)**
  - PEP transfers its phosphate group to an ADP resulting in an ATP + pyruvate.
  - Product: pyruvate and ATP
  
  ![second part](./img/11_Glycolysis/second_part.png)

### Glycolysis Monitoring

- The goal is to keep the amount of ATP in the cell stable.
- An organism that is suddenly more active (muscle activity), lack of oxygen and such require more ATP thus more ATP need to be created to keep the cells stable.
- A reaction far from equilibrium is exorgonic and not reversible.
- Reactions in equilibrium are reversible.
- Reactions in equilibrium are speed up by an enzyme following the model Michaelis-Menten and is quick.
- Reactions far from equilibrium are speed up by an allosteric enzyme with a low catalytic power.
  - **Allosteric regulation**: regulation of an enzyme by binding an effector molecule at a site other than the enzyme's active site.
- The rate limiting enzyme (האנזים הקוצב) sets the rate for the whole pathway (which included several reactions).
- **Rate limiting enzymes**: 
  - Hexokinase: when too much of its product (G6P), the enzyme is inhibited. 
  - Phosphofructokinase (PFK): competition between ATP & ADP to attach to PFK. ADP/AMP will speed PFK and ATP/citrate will inhibits it.
  - Pyruvate kinase (PK): F1,6P speed up the enzyme, ATP inhibits it.
  
![rate limiting](./img/11_Glycolysis/rate_limiting.png)
  
- Only 3 reactions in the glycolysis have a big change in free energy (<0).
  - Those are through the rate limiting enzymes.
- The rest of the reactions are close to equilibrium.
- PFK is the **main monitor** of flux in muscles.
- In the muscles, if the source of G6P isn't **glycogen** then hexokinase (HK) isn't required.
- PFK has two states: R and T.
  - ATP binds to T.
  - F6P, ADP and AMP bind to R.
- When there's a lot of ATP the activity of PFK decreases.
- When AMP binds to the R site the PFK activity increases.

### Hormonal Monitoring

- Monitoring outside the cell.
- Hormones are substances produced in one part of the body and secreted to other parts (organs or tissues) where they have a specific function.
- Hormone that regulate the sugar level (glucose) in the bloodstream is **insulin** produced in the pancreas.
  - Diabetes is the lack of insulin hormone.
- GLUT - glucose transporter.
- GLUT4 transporters fuse with the cell membrane to increase the cell capacity to transport glucose.
- After the glucose enters the target cells, insulin starts enzymatic processes.

### Hexoses

- 6 membered rings: glucose, galactose, mannose, fructose.

![hexose](./img/11_Glycolysis/hexose.png)

- **Fructose**:
  - In order to use it, it requires: one enzyme in the muscle, 7 in the liver.
  - Sweeter than sucrose.
  - In the liver it isn't regulated by PFK.
  - The liver pathway can result in fat building.
  - Causes the flux of glycolysis to make fat.
  
  ![fructose](./img/11_Glycolysis/fructose.png)
  
- **Galactose**:
  - Hydrolysis of lactose.
  - Isn't identifies by hexokinase (like fructose, glucose and mannose).
  - Requires process of אפימריזציה
  - Happens after the conversion of glucose to UDP.
- **Galactosomy** is a genetic disorder which prevent metabolism to galactose which thus adds up in the liver causing several issues (slow growth and stuff).
- **Gluconeogenesis** is the conversion of non carbohydrates substances (lactic acid, amino acids,glycerol) back to glucose.
- Glycolysis and gluconeogenesis are not the opposite of one antother!
- Usually both of those processes don't happen at the same time in a cell (different signals and regulations).


## Fermentation

- **Homolactic fermentation**: "the 11th reaction".
  - Happens when the demand for ATP is high and there's not enough oxygen.
  - NADH is recycled as $`NAD^+`$
  - ATP is produced by glycolysis which is faster and not by phosphorilation of oxygen.
  - 𝚫G=0
  - The lactate from the muscle goes to the liver to make glucose (Cori cycle).
- **Cori cycle** is the conversion of lactate to glucose in the liver and glucose to lactate in the muscle.

![cori](./img/11_Glycolysis/cori.png)

- **Alcoholic fermentation**: 
  - $`CO_2`$ in dough (בצק) and beer foam.
  - Ethanol: wine, beer.
  
  ![alcoholic fermentation](./img/11_Glycolysis/alcoholic.png)
  
- **Effect Pasteur**:
  - Yeast that grow in anaerobic environments and use more sugar than yeast that grows in aerobic environments.
  - Up to 2 ATP (unlike the 32 in aerobic respiration).
  - Anaerobic glycolysis is a lot faster.
- **Effect Werberg**:
  - In dividing cells and cancerous cells.
  - Cancerous cells don't perform cellular respiration only glycolysis even if there's oxygen.
  - Cancerous cells are in the hypoxic state (lack of oxygen).
