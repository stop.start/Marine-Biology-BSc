# Enzymes

## Intro

- The enzyme **Aequorin** in **luminescent jellyfish** catalyze the oxidation of a compound in the presence of calcium and release $`CO_2`$ and **light**.
- 25% of the genes in the human genomes encode for enzymes.
- The **active site** is where the **catalysis** take place.
- Almost all enzymes are proteins except the catalytic activity of **RNA**.
- Enzymes know how to bring two substrates together by making and breaking bonds.
- Enzymes stabilize **transition states** (highest energy parts in reactions).
- Without enzymes, reactions would either happen a lot lot slower or at a higher temperature (but cells have to keep a certain temperature and don't have eternity).
- Enzymes usually catalyze a single reaction or closely related reactions.
- Carbonic anhydrase is an enzyme that allow the hydration of carbon dioxide $`10^7`$ faster than the uncatalyzed reaction.
- **Proteolytic enzymes** catalyze proteolysis (**hydrolysis of a peptide bond**). 
- In vitro those enzymes catalyze the hydrolysis of an ester bond.

![proteolic enzyme](./img/04_Enzymes/proteolic.png)

- **Papain**, another proteolytic enzyme found in papaya, cleave almost any peptide bond. This characteristic is why it is used in meat-tenderizing sauces.
- Other proteolytic enzymes like **trypsin** and **thrombin** are very specific.
- **Cofactors** (metals) and **coenzymes** (small organic molecules) can be required for an enzyme to catalyze a reaction. They usually perform reaction that cannot be performed by the regular 20 amino acids.
- An enzyme without a cofactor is called **apoenzyme** and with the cofactor (catalytically active) is called **holoenzyme**.
- Coenzymes that are tightly bound to the enzyme are called **prosthetic groups** and loosely bound are like **cosubstrate** because they bind to the enzyme and then are released from it.
- Coenzymes can be used by several enzymes with similar mechanisms.
- **Matrix metalloproteinases**: a family of 25 zinc dependent **extracellular endopeptidases** which differ widely in substrate preferences (collagens, elastin, proteoglycans).​
- By regulating the integrity and composition of the extracellular matrix, these enzymes play a pivotal role in the control of signals elicited by matrix molecules that regulate ​cell proliferation, differentiation, and death​.
- Living cells convert energy to one form to another and enzymes help with those processes.:
  - Photosynthesis converts light energy to chemical bond energy. 
  - In cellular respiration, mitochondria convert free energy from food into free energy of an ion gradient and then into ATP.
- **Myosin enzyme** converts ATP into mechanical energy of contracting muscles.
- Pumps in cell membranes can be thought as enzymes the move substrates (instead of altering them). The chemical and electrical gradients that formed are used to perform other reactions.

## Gibbs free energy

- Gibbs free energy is the energy that can do work.
- The free energy difference $`\bold{\Delta G}`$ (between products and reactants) determines wether the reaction will be **spontaneous**
  - $`\Delta G < 0 \Rightarrow`$ spontaneous. Spontaneous reactions are called **exergonic**.
  - $`\Delta G = 0 \Rightarrow`$ equilibrium. Nothing happens.
  - $`\Delta G > 0 \Rightarrow`$ not spontaneous. Non spontaneous reactions need an input of energy to happen and are called **endergonic**.
- A loss in free energy means that this energy can be used to do work.
- The energy required to initiate the reaction (activation energy) determines the **rate** of the reaction. $`\Delta G`$ has nothing to do with the rate of reaction.
- $`\Delta G`$ isn't dependent on the path taken by the reaction.
- Some energetically unfavorable reactions are coupled with energetically favorable reactions in order to happen spontaneously.
- The transition state (peak of the activation energy) has a free energy higher than that of the reactant and product. This state is between reactant and product and is the least stable state.
- Enzymes only affect the activation energy by lowering it.
- **Enzymes catalyze reactions by binding to a substrate and alter that substrate's structure to facilitate the formation of the transition state**.
- The binding of the substrate onto the enzyme forms a complex called the **enzyme-substrate (ES) complex**.

![enzyme-substrate complex](./img/04_Enzymes/es.png)

- **3 evidence of an enzyme-substrate complex**: 
  - At constant concentration of enzyme $`\rightarrow`$ reaction rate increases with substrate concentration until a maximal velocity is reached. The maximal velocity is attained when all the enzyme are in use. Uncatalyzed reactions do not show this effect.
  - The **spectroscopic characteristics** of many enzymes and substrates change when in ES complex. It is very notable when the enzyme contains a colored **prosthetic group** (e.g enzyme **tryptophan synthetase** contains prosthetic group PLP)
  - **X-ray crystallography**: images of substrates bound to the active sites of many enzymes.
- A **catalytic group** is a group of residues in the active site of the enzyme where the substrate binds and that are responsible for making/breaking bonds.
- **Generalizations about enzymes' active sites:**
  - It is a 3D crevice formed by groups of amino acids that are far apart in the sequence (e.g lysosome).
  - The active site is only a small part of the volume of the enzyme. Minimum size for an enzyme is 100 residues.
  - Usually non polar and without water (unless part of the reaction). Sometimes there may be some polar residues essential for substrate binding, this is an exception to the rule that all polar residues are on the outside of the protein.
  - Substrates are bounded to enzymes by **weak attractions** (hydrogen bonds and van der Waals).
  - Enzyme/substrate shapes need to match perfectly due to the close range forces required for the binding.
- **The energy required by the enzyme in order to lower the activation energy of a reaction comes from all the weak interactions between the enzyme and substrate**.

## The Michaelis-Menten equation

- It acounts for kinetics properties of many enzymes.
- Rate of a chemical reaction: rate V of quantity of reactant A that disappears in a specified unit of time which is equal to the rate of appearance of product P in the same unit of time.

```math
\boxed{V = \frac{\Delta A}{\Delta T} = {\Delta P}{\Delta T}}
```

- The rate of a **first order** reaction is directly related to the concentration of A by a proportionality constant k (rate constant)
  - The unit is $`s^{-1}`$

```math
\boxed{V = k[A]}
```

- The rate of a **second order** reaction is related to the concentrations of two reactants also proportionality constant k (rate constant)
  - Those reactions are called **bimolecular reactions**.
  - The unit is $`M^{-1}s^{-1}`$

```math
\boxed{V = k[A]^2} \quad \text{or} \quad \boxed{V = k[A][B]}
```

- Second order reactions can appear to be first order reactions when one reactant is in excess and the other reactant in low concentration. Those are called **pseudo first order reactions**.
- **Zero order reactions** are reactions that are independant of the concentration of reactant. 
- Enzyme catalyzed reactions can approximate zero order reactions under certain circumstances.
- To investigate the rate of a reaction, the easiest way is the follow the increase of product per unit of time.
- With enzymes the higher the substrate's concentration the higher the rate of reaction (until it reaches equilibrium then the rate is steady). The rate of catalysis rises linearly with substrate concentration.
- We can define the rate of catalysis V0 as the amount of moles of product produced per second when the reaction is just beginning (when the reverse reaction in insignificant).

![rate](./img/04_Enzymes/rate.png)
​
- Model proposed by **Michaelis and Menten** to account for the kinetics which includes ES complex as a required intermediate in catalysis.
- Enzyme E and substrate S combine into ES complex at a rate $`k_1`$ then either continue to form product P at rate of $`k_2`$ or reverse into enzyme an substrate at a rate of $`k_{-1}`$. The ES complex can also be reformed from E and P at a rate of $`k_2`$ although to simplify we look only at times close to 0 thus with small product formation and thus no backward reaction (and $`k_{-2}`$ is negligible).

```math
\boxed{ E + S \xrightleftharpoons[k_{-1}]{k_1} ES \xrightleftharpoons[k_{-2}]{k_2} E + P}
```

- Rate of formation of ES: $`k_1[E][S]`$
- Rate of breaking down of ES: $`(k_{-1} + k_{2})[ES]`$
- Steady state assumption: [ES] stays the same even if the concentrations of reactant and product change: 

```math
\begin{aligned}
& k_1[E][S] = (k_{-1} + k_2)[ES] \\
& \frac{[E][S]}{[ES]} = \frac{k_{-1} + k_2}{k_1} \\
\\
& \boxed{K_M = \frac{k_{-1} + k_2}{k_1}}
\end{aligned}
```

- $`\bold{K_M}`$ is the Michaelis constant and has the units of concentrations.
- Km values depend on the particular substrate, pH, temperature, and ionic strength. Important characteristic of enzyme-substrate interaction.​
- It is the concentration of substrate at which half of the active sites are filled.
- At values below $`K_M`$ enzymes are very sensitive to changes in substrate concentration but display little activity​. It is a first order reaction since it changes according to the substrate concentration.
- At substrate values above $`K_M`$ enzymes have great catalytic activity yet are insensitive to changes in substrate concentration​. It is a zero order reaction: no matter how more substrate there is the rate stays the same.

![slide 4/46](./img/04_Enzymes/slide46.png)

- The Michaelis-Menten model cannot account for the kinetics properties of many enzyme, the **allosteric enzymes** are such an example.
- **Allosteric enzymes** consist of multiple subunits with multiple active sites.
- The Michaelis-Menten model works with **hyperbolic plots** of rate vs substrate concentration while allosteric enzymes have a **sigmoidal plot**.
- Allosteric enzymes are **key regulators of metabolic pathways**:
  - The binding of a substrate to one active site can alter the properties of another active site.
  - When the alteration facilitates the binding to another active site, this substrate is said to be **cooperative**.
  - Activity of allosteric enzymes may be altered by regulatory molecules (not substrate) that are reversibly bound to the specific sites​.

## Inhibition of enzymes

- Enzymes can be inhibited by the binding of specific small molecules and ions.
- Those inhibitors bind thightly convalently or nonconvalently.
- This serves as a control mechanism in biological systems.
- Enzymes inhibition can be either reversible or irreversible.
- Many drugs and toxic agents act by **inhibiting enzymes** (irreversible). _For example:_
  - **Penicillin** which binds covalently to transpeptidase enzyme and prevent the synthesis of bacterial cell wall thus killing the bacteria.
  - **Aspirin** inhibites cyclooxygenase thus reduce the synthesis of signaling molecules in inflammation.
- Reversible inhibitions are usually from **competitive inhibition**: the competitive inhibitor ressemble the substrate and binds in its place at the active site.
- **Competitive inhibitors** reduce the rate of catalysis. Increasing the substrate concentration helps with the competitions.
  - **Methotrexate** is a potent competitive inhibitor of the enzyme dihydrofolate reductase​ which is part of synthesizing purines and pyrimidines.
  - It binds 1000 times as tightly as the natural substrate. 
  - It is used to treat cancer.
- **Uncompetitive inhibitors** bind to ES complex only (site of binding is created only after ES is formed) and cannot be overcome by excess of substrate.
- With **non competitive ihibitors** the substrate and inhibitor can bind simultaneously to the enzyme. The inhibitor can bind to free enzyme or ES complex. 
  - It decreases only the concentration of functional enzymes.
  - Cannot be overcome by increasing the concentration of subtrate.
- **Suicide inhibitors** are modified subtrates:
  - Start by binding normally and start the normal reaction. 
  - This generates an intermediate that inactivate the enzyme through **covalent modification**.
  
### Penicillin

- Penicillin irreversibly inactivates a key enzyme involved in bacterial cell wall synthesis.
- It is a suicide inhibitor.
- Thiazolidine ring fused to $`\beta`$-lactam ring and to R-group (attached through peptide bond).

![penicillin](./img/04_Enzymes/penicillin.png)

- Penicillin interfere with the synthesis of the Staphylococcus aureus  bacterial wall.
- The cell wall is made of peptidoglycan (macromolecule made of polysaccharide chains).
- Penicillin blocks the last step in the cell wall synthesis: the cross linking of the peptidoglycan strands by the enzyme transpeptidase.

   
