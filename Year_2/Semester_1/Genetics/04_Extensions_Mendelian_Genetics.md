# Extensions of Mendelian Genetics

## Partial Dominance

- **Wild-type allele (זן הבר)** is the allele that occurs the most in nature, thus also called "normal" allele.
- A mutant allele is allele that is different from the wild-type or "normal" allele.
- Mutations will more likely cause a reduction or loss of a function. Those mutations are called **loss-of-function mutations** and if the function is completely lost it is called a **null allele**.
- When the mutation enhance a function it is called **gain-of-function mutation**.
- A **neutral mutation** is a mutation that has no effect on the function.
- Other representation of the alleles:
  - $`e^+ \rightarrow`$ wild-type allele and $`e \rightarrow`$ the mutant allele.
  - In shorthand: +/e means heterozygote wild type/mutant and the phenotype is wild type.
  - When there is no dominance uppercase is used with supscript: $`R^1 \text{and} R^2`$, $`L^M \text{and} L^N`$, $`I^A \text{and} I^B`$
  - It can be also several letters: $`Wr^+/Wr`$ or $`Leu^+/Leu^-`$
- **Incomplete** or **Partial dominance** occurs when the two alleles are expressed and an **intermediate phenotype** is produced.
- With no dominant/recessive alleles the **genotype ratio** produced from an $`F_2`$ cross is identical as Mendel's but the **phenotype ratio** equals the genotype ratio and not Mendel's 3:1 ratio.

![no dominance](./img/04_Extensions_Mendelian_Genetics/no_dominance.png)

- In the above picture, the partial dominance is characterized by:
  - The pigment color red is synthesized by a certain enzyme. 
  - The color white has apparently lost the ability (mutation) to produce the enzyme that makes the red pigment.
  - Heterozygote will thus produce only half the enzymes required to make the red pigment making the flower pink.
- Sometimes an allele seems to be dominant but it isn't the case, like with the Tay-Sachs disease:
  - One allele producing the enzyme is enough for the good functioning of the body.
  - The mutant allele affects the number of enzymes produced thus isn't actually recessive although only in homozygotes are sick.

## Codominance

- **Codominance** happens when two alleles are **expressed** into two different products.

![codominance](./img/04_Extensions_Mendelian_Genetics/codominance_joke.png)

- One example of codominance is a glycoprotein found on red blood cells that acts as an antigen. It can have two forms: M and N and is called the **MN system** 
- The possible alleles are $`\bold{L^M}`$ and $`\bold{L^N}`$.
- Those alleles are found on **chromosome 4**.
- Possible genotypes and phenotypes:

| Genotype       | Phenotype |
|----------------|-----------|
| $`L^M \; L^M`$ | M         |
| $`L^M \; L^N`$ | MN        |
| $`L^N \; L^N`$ | N         |

- The genotype of offsprings of two heterozygous MN parents will have a 1:2:1 ratio:

![codominance ratios](./img/04_Extensions_Mendelian_Genetics/codominance_ratios.png)

## Multiple Alleles

- **Multiple alleles** (more than 2) have to be studied within a whole population since every individual can have at most 2 alleles.
- **ABO blood groups** are an example of multiple alleles (here 3).
- It is called the **ABO system** and like the MN system produce and antigen on the red blood cells.
- The alleles are on **chromosome 9**.
- The A and B alleles codes for different antigen while O doesn't produce any antigen.
- In order to test for ABO blood type, a blood sample is taken and mixed with an antiserum containing type A or type B antibodies. If a reaction happens (clumping, agglutination) it means that the tested antigen is present in the blood.
- The alleles symbols: $`\bold{I^A}`$, $`\bold{I^B}`$ or $`\bold{i}`$ for the O allele.
- The $`\bold{I^A}`$ and $`\bold{I^B}`$ alleles are **codominant to each other and both dominant to the allele $`i`$**.
- The genotypes and phenotypes are as below:

![abo](./img/04_Extensions_Mendelian_Genetics/abo.jpg)

### The A and B Antigens

- Both antigens are **4 sugars** (carbohydrates) that are bound to lipids (fatty acids).
- They are on the membrane of red blood cells.
- Both antigens start as a 4 sugars H antigen (see below) and, with their enzyme, bind a fourth sugar to produce A or B antigen depending on the allele.
- An individual with both alleles will have red blood cells with both antigens.
- A good explaination about the H substance: https://www.ncbi.nlm.nih.gov/books/NBK2268/
- The **H substance** is a 3 sugars molecule (galactose -Gal-, N-acetylglucosamine -AcGluNH- and fucose).
- The H substance is built from the 2 sugars (Gal and AcGluNH) **H substance precursor** and Fucose is added by an enzyme.
- It is located on the red blood cells and is needed in order to attach either fourth sugar for A or B antigens to it.
- The $`I^A`$ allele produces an enzyme that attaches an **AcGalINH** sugar to the H substance making it a 4 sugars molecule.
- The $`I^B`$ allele is a mutation (after several mutations) of the A allele and produces an enzyme that attaches **Gal** to the H substance making it also a 4 sugars molecule.
- Note that type O individual don't have the enzymes to attach sugars to the H substance which thus stays a 3 sugars molecule.
 
![antigens A B](./img/04_Extensions_Mendelian_Genetics/antigens.png)

### The Bombay Phenotype

- A weird mutation has been found the first time in Bombay.
- A woman with O blood type but had one parent with AB blood type which means that she had to be either A, B or AB.
- She also had children with B allele which her husband lacked.
- It was discovered that she was homozygous for a rare recessive mutation in the gene coding for the enzyme that attaches the Fucose to the H substance precursor.
- Without the fucose sugar, the 2 sugars H substance cannot attach the fourth sugar for either A or B antigen.
- Those individuals can only get transfusion from donors with the same mutation.

![bombay phenotype](./img/04_Extensions_Mendelian_Genetics/bombay.png)


### The white locus in Drosophila

- **Locus**: location of a gene in the chromosome.
- In Drosophila, there are over 100 alleles for the eye color (discovered by Morgan and Bridges in 1912).
- The white mutation doesn't affect the making of pigments but instead it affects their transport.

### Lethal Alleles

- **Recessive lethal allele**: homozygous individuals dies but not heterozygotes.
- **Dominant lethal allele**: one copy is enough to cause death (heterozygotes dies as well as homozygotes).
- **Huntington disease** is an example of dominant lethal allele.
- Dominant lethal alleles are rare since the carrier need to live long enough to reproduce.
- In mice, the yellow gene is dominant to the agouti gene and is recessively lethal. The yellow gene overwrites part of another gene (Merc) which is lethal in homozygotes (heterozygotes produce enough).

## Extensions of Mendelian Genetics

- Mendel's dyhibrid ratio 9:3:3:1 works when the 2 genes are on different chromosomes.
- **Genetic linkage** is defined by two or more genes on the same chromosome thus inseparable.
- Mendel's model also relies on the fact that the genes have 2 alleles, one dominant and one recessive.
- Considering 2 individuals that both have the recessive gene for albinism and are both AB blood type, the genotype and phenotype probability for their offsprings doesn't match the dihybrid ratio of Mendel since the ABO system of blood types isn't just a recessive/Dominant type of alleles:

![non dihybrid](./img/04_Extensions_Mendelian_Genetics/non_dihybrid.png)

- Some phenotypes are affected by **more than one gene**. This is called **gene interaction** altough it doesn't mean necessarily that the genes directly interact on one characteristic.
- **Epigenesis**: complex organs need multiple steps of development.
- Complex organs are formed by a cascade of steps requiring more than one gene. The genes do not interact directly, instead each has its part to contribute to the overall formation of the organ.

> For example:  
> The inner ear in mammals includes several characteristics to be able to capture, transmit and convert the sound waves.  
> This requires many genes, each coding a specific characteristic but together they produce the whole inner ear.  
> One bad mutation can lead to hereditary deafness.  
> In human, more than 50 genes are involved in the development of the inner ear and there are a few alleles that are responsible for deafness.  

- **Epistasis**: one gene pair masks or modifies the effect of another gene pair.
- In epistasis, the 2 gene pairs can affect the same phenotypic characteristic or they can be complementary or cooperative:
  - Homozygous recessive pair can mask other alleles at a second locus (Bombay phenotype).
  - A single dominant allele can mask other alleles at a second locus.
  - Two gene pairs can complement each other.
- The pair masking the other pair is called **epistatic**.
- The pair masked by the other is called **hypostatic**.
- When looking at the blood type as a single characteristic, the genotype ratio is expressed in 16. That's because of the H substance which is coded by one gene and the ABO system coded by another. 2 genes responsible for one phenotypic characteristic.
- When looking at a single phenotypic characteristic, if the ratio is in 16, it suggests that two genes are interacting in the expression of the phenotype.

### Examples

#### Mice - Recessive epistasis example

- The wild-type coat color in mice is agouti.
- Agouti allele (_A_) is dominant to the black allele (mutation _a_).
- The recessive mutation _b_ at a separate locus causes the loss of pigment (affects the _A/a_ expression).
- This means that _A-bb_ as well as _aabb_ will yield an albino phenotype. The recessive _b_ gene masks the genotype of the coat color _A/a_. This a **recessive epistasis**.
- The modified **phenotypic ratio: 9:3:4**.

![mice coat color](./img/04_Extensions_Mendelian_Genetics/mice.jpg)

#### Squash - Dominant epistasis example

- In squash, the dominant allele _A_ yields a white fruit color regardless of the _B_ allele genotype.
- With the recessive allele _a_ the _B_ allele can be expressed: _B-_ yields yellow color while _bb_ yields green color.
- This is called **dominant epistasis**.
- The modified **phenotypic ratio: 12:3:1**.

![squash color](./img/04_Extensions_Mendelian_Genetics/squash.jpg)

#### White-flowered sweet peas - Complementary gene interaction

- A cross between two true breeding strains of **white-flowered** plants yields **all purple** F1 plants.
- The F2 plants are either purple or white with a **phenotypic ratio: 9:16**.
- If the first cross was done with two different genotypes: _AAbb_ x _aaBB_ both giving the color white, their cross will result partly in the genotypes _A-B-_ which result in a purple color. This means that both dominant alleles _A_ and _B_ are required for the purple color to be expressed. This is called **complementary gene interaction**.

![sweet peas flowers](./img/04_Extensions_Mendelian_Genetics/flowers.jpg)

### Novel phenotypes

- Some gene interactions result in novel phenotypes.
- A type of squash has either disc shape fruits (_AABB_) or long shape fruits (_aabb_).
- When crossing F2 (all disc shape which is the dominant form) in addition to the disc and long shapes phenotypes, a sphere shape phenotype also appears.
- The sphere shape happens when only one dominant allele is present while the disc shape happens when 2 dominant alleles are present and the long shape happens with only recessive alleles.

![novel shape](./img/04_Extensions_Mendelian_Genetics/novel_phenotypes.jpg)

#### Drosophila melanogaster - eye color phenotype

- Mendel's dihybrid ratio 9:3:3:1 works for 2 genes each responsible for a phenotype (round/wrinkle and green/yellow).
- In the drosophila, the ratio with 2 genes is the same (9:3:3:1) but the result is one phenotype: eye-color.
- Crossing between two recessive phenotypes, brown and scarlet, gives the wild-type phenotype and genotype (F1).
- Crossing between two F1 gives a 9:3:3:1, wild-type:brown:scarlet:white.
- The wild type color comes from the crossing of the two pigment groups (scarlet and brown): each has its own pathway catalyzed by a different enzyme (one enzyme per gene).
- The two mutations, scarlet and brown, have each a mutation on a different gene: 
  - The brown mutation has a mutation on the scarlet gene which disables the enzyme required to produce the scarlet pigment.
  - The scarlet mutation has a mutation on the brown gene which disables the enzyme required to produce the brown pigment.
- When crossing between two F1, 1/16 of the phenotypes are white since those get the 2 possible mutations blocking the production of both pigments.

![drosophila](./img/04_Extensions_Mendelian_Genetics/drosophila.png)


### Complementation analysis

- When two mutations produce similar phenotypes, **complementation analysis** allows to determine wether those two mutations are alleles of the same gene or two mutations of two different genes.
- The process is to **cross two mutant strains** and look at the F1 generation. Examples with the drosophila wings vs mutations $`\rightarrow`$ no wings:
  - Offsprings develop normal wings: the mutations are on different genes (AAbb, aaBB) and the F1 generation is heterozygous (AaBb). It is said that **complementation occurs**.
  - Offsprings don't develop wings: the mutations are alleles of the same gene. **Complementation does not occur**.

### Single gene expression

- The expression of a single gene can affect multiple phenotypes. This is called **pleiotropy**.
- **Marfan Syndrome**:
  - This condition results from autosomal dominant mutation in the gene encoding the connective tissue protein **fibrillin**.
  - This protein is important in the structural integrity of several tissues like the lens of the eye, the lining of vessels, bones, etc.
  - A mutation in the expression of this protein causes deffects in all those tissues resulting in many health issues.

### X-Linkage

- **Autosomal** genes are genes that are located on chromosomes that are **not** sex chromosomes.
- Most genes that are present on the X chromosomes, aren't in the Y chromosomes which results in different inheritance patterns than with autosomal genes. This is called **x-linkage (תאחיזה למין)**.

#### Drosophila and the eye color

- The wild type eye color has both genes' dominant alleles.
- When crossing a female with wild type with a male with white eyes, it results in all offsprings with wild type eyes.
- When crossing a white eyed female with a wild type eyed male, it results in females having wild type eye color and males having white eye color.
- When crossing the F1 generation of each one of the above crossing we get the results shown in the below image and its genetics explication.
- **Hemizygosity** is the possession of only one copy of a gene, i.e males have only one copy of most genes present on the X chromosome.

![x linkage](./img/04_Extensions_Mendelian_Genetics/xlinkage.png)

#### In humans

- Conditions transmitted by the X chromosome:
  - Colorblindness 
  - Hemophilia A/B
  - Duchenne Muscular Dystrophy​
  - G-6-PD deficiency
- Lethal conditions affecting individual prior to reproductive maturation, like the Duchenne Muscular Dystrophy, affect only males:​
  - Only heterozygous females can be carriers.
  - Males die before the reproductive maturation thus won't pass on their X chromosome and thus no homozygous females can be born.

### Chromosome Y

- Not many genes are found on the Y chromosome.
- One important gene that is found there is the **TDF (Testis-Determining-Factor)** which is the gene responsible for the overall male phenotype.
- An individual with XY chromosomes but without the TDF gene will be mostly female.
- Areas of the Y chromosomes:
  - PAR: pseudoautomosal region: region that is found on both X and Y chromosomes
  - NRY: non-recombining region
  - SRY: sex-determining region
  
![y chromosome](./img/04_Extensions_Mendelian_Genetics/ychromosome.png) 

### Sex-limited and sex-influenced inheritance

- Autosomal genes can be affected by the sex of the individual because of the different hormones in males and females.
- Some autosomal genes can be active only in males and some only in females.
- **Sex-limited inheritance** happens when a phenotype is limited to one sex. 
  
  > _For example, hens have shorter feathers than cocks_:  
  > Here one gene's expression is affected by the individual's hormones.  
  > The genotype _H-_ will result in a "female" apparence even in males.  
  > The recessive hh genotype is the male appearence in males **only**. Females still show the female appearence.  
  
- **Sex-influenced inheritance**: **heterozygote** phenotype is dependant of the hormone constitution of the individual. Male and female will have **opposite trait per genotype**. _For example baldness:_

![baldness](./img/04_Extensions_Mendelian_Genetics/baldness.png)



