# Mendelian Genetics

- Gregor Mendel 1822-1884
- He experimented on "garden peas" and laid the ground for today's genetics.
- Garden peas are self fertilizing but can be easily cross breed.
- During his experiment he watched 7 pairs of traits:
  - **Seed shape**: round/wrinkle
  - **Seed color**: yellow/green
  - **Pod shape**: full/constricted
  - **Pod color**: yellow/green
  - **Flower color**: violet/white
  - **Flower position**: axial/terminal (flowers on the sides or at the top of the plant)
  - **Stem height**: tall/darwf
- **True-breeding** strains (זני יסוד): strains with traits that appeared unchanged for generations.
- **Reciprocal cross**: A from dad/ B from mom then B from dad/ A from mom. It is used to check the affect of sex in genetic inheritance.
- **Monohybriding cross**: crossing between true-breeding strains allowing to track specific traits in the offsprings.
  - First we look at the **first generation of offsprings (F1)**.
  - Then at the offsprings of **selfing** (self fertilizing) **(F2)**.
- Mendel started by crossing **tall stems with dwarf**:
  - **F1 generation were only tall plants**.
  - **F2 generation (selfed) were 3/4 tall and 1/4 dwarf**. This means that the dwarf trait skipped a generation.
- Mendel crosses were also **reciprocal** which mean that the results were not sex-dependent.
- The below table shows the results for all the crossing:

![crossing](./img/03_Mendelian_Genetics/mendel_table.png)

- Mendel explained those resutls with each trait having a **unit factor** (genes were not yet discovered). 
- He also explained that those factors are the **basic unit of heredity**.
- As a reminder, **somatic cells** are any cell other than a gamete, germ cell, gametocyte or undifferentiated stem cell. **Autosomal** chromosomes are found in somatic cells.

## The 3 principles of inheritance of Mendel

- Following his experiments and their results in F1 as well as F2, he gave the hypothesis of 3 principles of inheritance.

### Unit factors in pairs

_Genetic characters are controlled by unit factors existing in pairs in individuals organisms_.  

- Since each factors occur in pairs there are 3 possible combinations for the offsprings: tall/tall, dwarf/dwarf, tall/dwarf.

### Dominance/Recessiveness

_When two unlike unit factors responsible for a single character are present in a single individual, one unit factor is dominant to the other, which is said to be recessive_.  

- The dominant trait is the one expressed, while the recessive one is the one that isn't expressed. "Tall" is the dominant trait over "dwarf".

### Segregation

_During the formation of gametes, the paired unit factors separate randomly so that each gamete receives one or the other with equal likelyhood_.   

- Having tall/tall will produce "tall" gametes.
- Having tall/dwarf will randomly produce either "tall" or "dwarf" gametes.
 
<br>

- The combinations in F2 (tall/tall, tall/dwarf, dwarf/tall, dwarf/dwarf) from F1 parents will have equal frequency which align with the results off 3/4 tall and 1/4 dwarf.

![inheritance](./img/03_Mendelian_Genetics/inheritance.png)
 
## Some modern terms (after Mendel)

- **Gene** (unit factor) is the unit of inheritance.
- A same gene can have different forms (size=tall or size=dwarf). The different forms of a gene are called **alleles**.
- Dominant alleles are written as an italic capital (e.g. _D_). Recessive alleles are written as an italic lowercase (e.g. _d_).
- The **genotype** is all the possible combinations of alleles.
- An **homozygote** has two same alleles (_DD_ or _dd_).
- An **heterozygote** has two different alleles (_Dd_).
- **Phenotype**: physical expression of a trait.

## Punnet Squares

- Visualization of the resulting phenotype and genotype from gametes combinations.
- Column represent the female parent possible gametes and the rows the male parent possible gametes.
- Filling the square show the genotypes and phenotypes.

![punnet square](./img/03_Mendelian_Genetics/punnet.png)

### Testcross

- Tall plants can have either DD or Dd genotype. How to know?
- Crossing an unknown genotype with a **known recessive** genotype will reveal the unknown genotype. _For example: the known recessive genotype dd (dwarf/dwarf) combine with DD will result only in tall plants while dd combine with Dd will result in half of the offsprings to be tall and half to be dwarves._

![testcross](./img/03_Mendelian_Genetics/testcross.png)

## Dihybrid Cross

- **Dihybrid crossing** is crossing between individuals that have 2 contrasting traits (e.g. color and shape).
- The first 2 monohybrid crosses will reveal the dominant traits.
- Self crossing should confirm this.
- With one trait that has 2 alleles, one dominant and one recessive, 3/4 of the offsprings will have the dominant phenotype and 1/4 of them will have the recessive phenotypes.
- With two independant traits (with 2 alleles) the probabilities are combined:
  - 3/4 x 3/4 = 9/16
  - 3/4 x 1/4 = 3/16
  - 3/4 x 1/4 = 3/16
  - 1/4 x 1/4 = 1/16
- Testcross can also be perfomed to test two traits by using an individual with two recessive traits (i.e. green and wrinkle _ggww_).

|                                                             |                                                                   |
|-------------------------------------------------------------|-------------------------------------------------------------------|
| ![dihybrid cross](./img/03_Mendelian_Genetics/dihybrid.png) | ![dihybrid cross](./img/03_Mendelian_Genetics/dihybrid_stats.png) |


### Punnet Squares

![dihybrid cross punnet square](./img/03_Mendelian_Genetics/dihybrid_punnet.png)

### Independent Assortment - the fourth principle of Mendel
 
_During gamete formation, segregating pairs of unit factors assort independently of each other_.  

- This means that traits are passed randomly and independently from each other: Gg will get separated and passed randomly as well as Ww.

## Maths

- Calculating the number of possible gametes/genotypes/phenotypes can be done by:
  - Taking the number of heterozygous gene pairs **n**. This means that if an individual has AaBBCc then n=2 because BB is homozygous.
  - The number of **different gametes** is $`\bold{2^n}`$.
  - The number of **different genotypes** is $`\bold{3^n}`$.
  - The number of **different phenotypes** is $`\bold{2^n}`$.

![maths](./img/03_Mendelian_Genetics/genetic_math.jpg)

## Chi-Square calculation and the Null hypothesis

- Mendel's ratios (monohybrid 3:1, dihybrid 9:3:3:1) are based on the following assumptions:
  - Alleles are either dominant or recessive
  - Segregation always occurs
  - Independent combinations
  - Fertilization is random
- The two last assumptions are based on chance and are subject to random fluctuations. This is called **chance deviation**. 
- The **null hypothesis ($`\bold{H_0}`$)** assumes that there is **no real difference between the ratio of the results and the expected one**. The difference between the expected ratio and actual ratio is because of chance.
- Chance might not be the only factor explaining the difference between the expected results and the actual ones, that's why the null hypothesis can be rejected.
- Assessing the null hypothesis can be done with the **chi-square analysis ($`\bold(\chi^2)`$)**.
- This method takes the deviation from the expected ratio and the sample size and turns it into a single value that estimates how frequently this deviation can be expected by chance.
- Chi-formula where: 
  - **o**: the actual (observed) value
  - **e**: the expected value
  - **d**: $`o - e`$
  
  ```math
  \boxed{\chi^2 = \Sigma{\frac{(o - e)^2}{e}} = \Sigma{\frac{d^2}{e}}} 
  ```

- In order to interpret the $`\chi^2`$ values, the **degrees of freedom (df)** needs to be calculated: 
  - **n** being the number of possible outcomes, $`df = n - 1`$
  - _For example, a monohybrid cross will expected 2 possible outcomes thus df = 1_.
- Once df has been calculated, a table or a graph can be used to get to the **probability** (those probabilities were calculated and put into table or graph which allow us not to have to calculate them).
- In order to accept or reject the deviation, a **critical value** needs to be chosen for the **probability p**. Here we'll choose 0.05. All values of p below the critical values will be rejected, so if we have a p equals to 0.26 then the null hypothesis is accepted because 0.26 > 0.05.
- It is important to note that rejected null hypothesis aren't necesserily wrong. Some external factors might influence the results. _For example, testing segregation of genes on fruit flies: if the dihybrid cross yields wingless flies that don't survive thus not included in the test. The test results will then be skewed giving the impression that segregation is wrong hypothesis._
  
![chi square](./img/03_Mendelian_Genetics/chi_square.png)

![probabilities](./img/03_Mendelian_Genetics/probabilities.png)

## Pedigree Analysis

- In order to study inheritance in humans, where experiments aren't made, a **family tree** called **pedigree** is used.
- Below the conventions used for the pedigree:

![pedigree conventions](./img/03_Mendelian_Genetics/pedigree_conventions.png)

- The **proband** is the individual whose phenotype brought attention to the family.
- The family tree shows the inheritance of a specific trait. In the picture below the tree (a) show the inheritance of the recessive trait "alibinism" and the tree (b) the inheritance of the dominant trait Huntington disease.
- In tree (a) we can assume almost with certainty that alibinism is recessive because it skipped a generation while in tree (b) we can assume that the trait doesn't skip a generation. It can not be passed but if an offspring has it then a parent has it also.
- Autosomal dominant diseases are rare but when someone is homozygous for this type of allele then it usually suffers more consequences.

![family trees](./img/03_Mendelian_Genetics/family_tree.png)

- Looking at Tay-Sachs disease helps understanding recessive disorders. The gene affected by the disease code for a protein active in lysosomes within cells and is responsible for breaking down gangliosides GM2 (lipid in nerve cell membrane). Heterozygotes produce only 50% of the proteins but are unaffected by the disease. This means that one gene produces enough proteins for the individual to function properly. Homozygotes don't produce those proteins and are affected by the disease.
