# Developmental Genetics

- At the cellular level development is marked by 3 events:
  - **Specification**
  - **Determination**: it is decided what the cell will become.
  - **Differentiation**: the cell gets its final form and function.
- **Variable gene activity hypothesis**: idea that differentiation is done by activating/inactivating genes at different times and in different cell types.
- Size and shape of animal bodies are controlled by a common set of genes and developmental mechanisms.
- Differences in shape between different animals (organisms) are the result of different patterns of expression in a single gene set called the **homeotic genes** (Hox) and not by species-specific genes.
- Homeotic genes from a wide range of organisms have a common ancestor.
- Evolution has generated several new ways of transforming a zygote into an adult.

## Drosophila

- The life cycle of the Drosophila is around 10days.
- Phases:
  - Embryo
  - 3 larval stages
  - Pupal stage
  - Adult stage

![life cycle](./img/17_Developmental_Genetics/life_cycle.png)

- Right after fertilization, the zygote nucleus undergoes a series of nuclear divisions without cytokinesis.
  - _Cytokinesis is the part of the cell division process during which the cytoplasm of a single eukaryotic cell divides into two daughter cells._
- **Syncytium** is the resulting cell from the above step.
- After 10 divisions, the nuclei move to the edge of the egg where the cytoplasm contains **gradients of maternally derived mRNA transcripts and proteins**.
  - mRNA and proteins in the egg are a third of the mother's genetic material.
- After more divisions, the nuclei becomes enclosed in plasma membranes.
- Interactions between the nuclei and the cytoplasmic components of these cells initiate the embryonic gene expression.
- Germ cells form at the posterior pole of the embryo. Those cells will produce gametes through meiosis.

![embryo](./img/17_Developmental_Genetics/embryo.png)

- The **embryonic development** is controlled by 2 gene sets (in Drosophila):
  - **Maternal-effect** genes.
    - mRNA and proteins.
    - Most are distributed in a gradient or concentrated in specific regions of the egg cytoplasm.
    - The distribution also defines the anterior-posterior axis (what's up and down, front and back).
    - They encode **trancription factors, receptors and proteins** that regulate gene expression (they determine what to do).
    - The way the material (mRNA and proteins) decide what the cells will be: the cells are all around the zygote, the mother's material is unevenly distributed. The material will decide how the cells that are nearby will develop.
    - Females homozygous for deleterious recessive mutations of maternal-effect genes are sterile.
  - **Zygotic genes**. 
    - Those genes are transcribed in the embryonic nuclei which is formed after fertilization.
    - The products of the embryonic genome are differentially transcribed in specific regions of the embryo in response to the distribution of the maternal-effect proteins.
- The genes that control development in Drosophila are categorized into 3 groups:
  - **Gap** genes
  - **Pair-rule** genes
  - **Segment polarity** genes
- **Embryonic development is initiated by maternal-effect gene products**.
- Zygotic genes = embryonic genes.
- The molecular gradient form the maternal-effect product is interpreted by 2 sets of embryonic genes:
  - **Segmentation genes** (gap, pair-rule, segment polarity genes).
    - Those divide the embryo into segments and define the number, size and polarity of each segment.
  - **Homeotic selector genes** (Hox).
    - Those specify what cells will be in each segment as well as the adult structure that will be formed from each segment.
    
![development](./img/17_Developmental_Genetics/development.png)

- 2 important genes that are in gradient (ppt17/slides 27-28):
  - **Bicoid gene**: front/anterior region. The cells around this gene will develop based on it (meaning they will form the front of the Drosophila).
  - **Nanos**: posterior region. The cells around this gene will develop based on it (meaning they will form the back of the Drosophila).
- Both of those genes have mRNA that is not translated on the 3' end.
  - The bicoid mRNA will attach this part on microfilament on the front pole of the embryo.
  - The nanos mRNA will attach this part on microfilament on the back pole of the embryo.
- Expressing those 2 genes into the proteins creates a protein gradient (bicoid proteins around the head, nanos proteins around the back).
- Zygotic genes, **Hunchback** will be expressed around the bicoid and **Caudal** around nanos.

### Segmentation Genes

- 20 segmentation genes.
- They determine the anterior-posterior axis. Another set of genes determine the the dorsal-ventral axis.

![segmentation genes](./img/17_Developmental_Genetics/segmentation_genes.png)

- **Gap genes**: 
  - When expressed they divide the embryo into regions: head, thorax and abdomen.
  - They also activate pair-rule genes.
- **Pair-rule genes**: 
  - When expressed, the products divide the embryo into smaller regions (2 segments wide).
  - Their expression activate the segment polarity genes.
- **Segment polarity genes**:
  - They divide each segment into anterior and posterior regions.

#### Expression of the genes
  
- Expressing gap genes:
  - Each gap genes require a certain amount of bicoid and nanos proteins. Their gradient allow for the gap genes to be expressed exactly where they are supposed to be.
  - **Hunchback**: involved in head and thorax structures (requires more bicoid than nanos).
  - **Kruppel**: involved in thorax and abdominal structures (requires equal amount of bicoid and nanos).
  - **Knirps**: involved in abdominal structure (requires more nanos than bicoid).

  ![gap](./img/17_Developmental_Genetics/gap.png)
  
- Expressing pair-rule genes:
  - Expressed in a series of 7 narrow bands.
  - It sets the boundaries of segments and sets how the cells will develop within each segment by controlling the expression of segment polarity genes.
  - Those segments, although the same, were formed by the expressions of different genes (different loci on the DNA). Proteins synthesized from the gap genes and maternal-effect proteins can transcribe those pair-rule genes with each combination allowing to transcribe a different pair-rule gene.
  
  ![pair rule](./img/17_Developmental_Genetics/pair_rule.png)

- Expressing segment polarity genes:
  - They become active in a single band of cells around the embryo's circumference.
  - Divide the embryo into 14 segments.

#### Segmentation genes in humans

- **Runt** is a pair-rule gene in Drosophila that is involved in sex determination and formation of the nervous system.
- In mice the runt gene is basically the same as in Drosophila.
- Mice with two mutant copies of the genes have no bones, only cartilage.
- In humans the homolog of runt is CBFA (in the book it is called RUNX2). Mutations in this gene causes **CCD** (cleidocranial dysplasia). This affect the skeleton.
- Those show that runt is important in the formation of bones.

### Homeotic Selector (Hox) Genes

- Determine which adult structures will be formed by each body segment (in Drosophila: antennae, mouth, wings...).
- They are called selector because they select the pathway of development for a given segment.
- **Homeotic mutants** are mutant genes which causes one segment to form the same structures as another segment.
  - **Antp** is an allele that specifies the pathway for the development of legs. Mutations causes the gene to be expressed in the head and thorax resulting in flies having a leg on their head instead of an antenna.
- The Drosophila genome contains two clusters of homeotic selector genes:
  - The **Antennapedia (ANT-C)** cluster contains **5 genes** specifying for the head and first two segment of the thorax.
  - The **bithorax (BX-C)** complex contains **3 genes** specifying for the rest of the thoracic segments and the abdominal segments..
  
  ![homeotic genes](./img/17_Developmental_Genetics/homeotic_genes.png)

- Hox genes from different species have 2 properties in common:
  - **Homeobox**: 180-bp (base pairs) sequence. It encodes a DNA-binding region of 60 amino acids called **homeodomain**.
  - The genes are organized and expressed colinearly from anterior to posterior.
  
  ![hox](./img/17_Developmental_Genetics/hox.png)
  
- **Imaginal discs**: cell clusters located at specific locations that are differentiated will development into a specific organ.
  - Moving those clusters to another location will cause the organ to be developped in the wrong place.

- Hox genes are found in the genome of most eukaryotes with segmented body plans.

#### Hox in humans

- Most vertebrates have 4 clusters of Hox genes: **HOXA, HOXB, HOXC, HOXD** (in Drosophila there's just one cluster).
  - 39 genes total.
- In vertebrates structures are formed from a combination of Hox genes.
  - This means that mutations in Hox genes in vertebrates affect less than in Drosophila.
- Mutations in HOXD can cause malformations.
  - Mutations in HOXD13 cause **synpolydactlyly (SPD)** (extra fingers).

### Switches

- **Binary switches genes**: certain genes can act as switches during development. This decreases the number of possible pathways for the cell development.
- They can initiate complete development of an organ or tissue type.
- In combination with signaling pathways they form **gene-regulatory networks**.
- The binary switch gene **eyeless**: 
  - As wild-type, it programs the cells to follow the eye development pathway instead of antennae.
  - Recessive mutant allele: homozygotes will have eyes reduced in size.
- The eyeless gene is part of a network of 7 genes (master control) - ppt17/slide 84.
- In mammals the homolog of eyeless is called Pax6.
- Puting Pax6 in Drosophila will result in triggering the formation of eyes.


## Plants

- Plants and animals have diverged 1.6 billion years ago.
- Basic mechanisms of developmental pattern formation evolved independently in animals and plants.
- The development of plants has been studied on **Arabidopsis thaliana**.
- The **floral meristem** is a cluster of undifferentiated cells that give flowers.
- Each flower is composed of **4 organs**:
  - Sepals: עלי גביע ​
  - Petals: עלי כותרת  ​
  - Stamens: אבקנים   ​
  - Carpels: עלי שחלה 
  
  ![flower](./img/17_Developmental_Genetics/flower.png)
  
- There are 3 classes of **floral homeotic genes** that control the development of those organs:
  - **Class A**: specifies sepals.
  - **Classes A and B**: specify petals.
  - **Classes B and C**: specify stamen.
  - **Class C**: carpels.
- The cells of the floral meristem are in rings conformation and each organ comes from a different ring:

![rings](./img/17_Developmental_Genetics/rings.png)

- Like in Drosophila, mutations cause organs to form in wrong locations.

## Signaling Between Cells 

- Signaling pathways are used to regulate development.
- The **Notch signaling pathway** works through direct cell-cell contact to control how the cells will develop.
- The Notch gene encodes for a **signal receptor protein embedded in the plasma membrane**.
  - The signal is also a membrane protein. It is encoded by the **Delta gene**.
  - When they attach, the tail of the Notch protein is cleaved off and binds to a cytoplasmic protein which moves into the nucleus and activates trancription of a gene set controlling a specific development pathway.
  
  ![notch](./img/17_Developmental_Genetics/notch.png)

### C. elegans

- Small number of cells.
- Genome has been sequenced.
- Males: XO (only one chromosome - X) - Females XX (hermaphrodites that can make both eggs and sperm).
- Embryonic development of 2 days.
- The Notch receptor gene determines which cell becomes gonadal cell and which becomes the precursor of the uterus.

#### Apoptosis

- Predetermines cell death for tissue and organ formation.
  - Vertebrates rely on apoptosis for the formation of fingers.
- In C. elegans during its development there is a defined number of cells that die purposedly:
  - 131 in females
  - 147 in males
- Timing of death is precise and uses the same pathway for all cells.
- 15 genes that are responsible for the decision, execution, absorption and digestion of the dead cells.

![apoptosis](./img/17_Developmental_Genetics/apoptosis.png)

- Homolog of ced-9 in humans is bcl-2.
