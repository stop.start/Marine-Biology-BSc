# DNA 

- Central dogma of molecular genetics: **"DNA makes RNA which makes proteins"**
- Genetic material must have 4 characteristics:
  - **Replication** (mitosis, meiosis).
  - **Storage** of information. 
  - **Expression** of information (transcription - synthesis of mRNA, translation - synthesis or the polypeptide chain)
  - **Variation** by mutation (variability)
  
## Griffith experiment - DNA might be the genetic material

- Until 1944 it was thought that proteins carried the genetic information (Avery, MacLeod, McCarty - let's call them AMM).
- The work of AMM was based on the work of **Griffith from 1927** which was done on a strain of the pneumonia bacteria (pneumococci). Two types of strains:
  - **Virulent**: the bacteria had a polysaccharide capsule. **Smooth colonies (S)**. Not easily engulfed by phagocytic cells.
  - **Avirulent**: no capsule. **Rough colonies (R)**. Readily engulfed by phagocytic cells.
- Serotypes are different types of a certain strain that differ in the chemical structure of the polysaccharide of the capsules. They're numbered with Roman numerals (I, II, III,...).
- Griffith used IIR and IIIS in mice:
  - Injecting IIR $`\rightarrow`$ mice don't get pneumonia.
  - Injecting IIIS $`\rightarrow`$ mice get pneumonia.
  - Injecting heat-killed IIIS (killed by heat) $`\rightarrow`$ mice don't get pneumonia
  - Injecting IIR + heat-killed IIIS $`\rightarrow`$ mice get pneumonia.
- The presence heat-killed IIIS bacteria converted IIR into IIIS bacteria. This is called **transformation**.
  - The dead cells of IIIS served as nutrient for IIR cells.
  - In transformation the DNA from the dead is ingested by the IIR living cells.
- AMM continued Griffith experiment by making a solution of the content of the IIIS dead cells and then treating the solution to neutrilize each time a "suspect" of the source of the transformation:
  - Treated with protase (neutrilizes proteins): the transformation still occurs.
  - Treated with riobonuclease (neutrilizes RNA): the transformation still occurs.
  - Treated with deoxyribonuclease (neutrilizes DNA): the transformation doesn't occur $`\rightarrow`$ source of the transformation.
- **The transformation causes the synthesis of the IIIS capsular polysaccharide and it is heritable**.

![amm experiment](./img/09_DNA/amm_experiment.png)

## Hershey-Chase experiment

![phage](./img/09_DNA/phage.png)

- 1952-1953
- Study of **E. coli** and one of its infecting viruses _bacteriophage T2_ reffered as **phage**.
- Virus is made of a protein coat surrounding the DNA. 
- Virus structure is an exagonal head with a tail.

|                                                   |                                                   |
|---------------------------------------------------|---------------------------------------------------|
| ![bacteriophage](./img/09_DNA/bacteriophage1.png) | ![bacteriophage](./img/09_DNA/bacteriophage2.png) |

- **Lytic cycle** - life cycle of the phage:
  - The phage cling onto the bacteria's cell wall.
  - It releases some genetic material into the cytoplasm.
  - Reproduces inside the cell.
  - Cell exploded and the viruses are free to infect more bacteria.
  
![lytic cycle](./img/09_DNA/lytic_cycle.png)

- The experiment was done by growing two batches of E. coli with either radioisotopes $`^{32}P`$ or $`^{35}S`$ which allow to follow the molecular components of the phages during infection.
  - Phosphorus isotope allows to follow DNA (no phosphorus in protein).
  - Sulfure isotope allows to follow proteins (no sulfur in DNA).
- When the virus infects a bacteria with the isotope, their offsprings will have the radioisotope from the E. coli batch (either $`^{32}P`$ or $`^{35}S`$).
- Those labeled viruses were then mixed with unlabeled bacteria.
- The now labeled bacteria were separated from the labeled viruses by centrifuge (in a blender). This allows the bacteria to be analyzed separately.
- Only $`^{32}P`$ was found within the newly infected bacteria. In the sulfure batch, the $`^{32}S`$ remained outside of the cells in the viruses.
- The new viruses produced from the phosphorus contained $`^{32}P`$. The ones from the sulfure batch did not contained any radioisotope.
- This experiment shows that DNA holds the genetic material and not protein.

![hc_experiment](./img/09_DNA/hc_experiment.png)

- **Transfection**: using only the DNA to infect bacteria (without the protein coat) $`\rightarrow`$ bacteria successfuly infected (experiments done by other scientists).

## Evidence in Eukaryotes

### Indirect evidence

#### Distribution of DNA

- DNA is found only where primary genetic functions occur:
  - In the nucleus as part of the chromosomes.
  - In mitochondria and chloroplasts.
- Proteins are found everywhere in the cell.
- Since it was known that chromosomes contain the genetic material, comparing the amount of the substance holding the genetic material should be half in gametes than in regular cells.
  - It was found that indeed there was half the quantity of DNA in gametes whereas proteins didn't show the same behavior.

#### Mutagenesis

- UV light can cause mutations in the genetic material.
- Irradiating organisms with different wavelengths of UV light cause mutations, certain wavelengths more than others.
- Comparing the action spectrum of the UV light to the absorption spectrum of any molecule suspected to hold the genetic material will show which molecule absorbs UV lights and is mutagenic thus should be the one holding the genetic material.
- Most mutations happen with a UV light wavelength between 1 of 260nm.
- DNA and RNA absorb UV light at 260 nm while proteins at 280nm.

### Direct evidence

- Using **recombinant DNA** (mixing during meiosis):
  - Eukaryotic DNA (specific genes) can be introduced into bacterial DNA and put into bacterial cell.
  - If eukaryotic protein appear this means that the eukaryotic DNA is functional in bacterial cell.
- This was used with the **insulin** gene.

## Retroviruses

- In some viruses, RNA holds the genetic material (an exception to the general rule that this is the role of DNA). Those are called **retroviruses**.
- Their replication is done by **reverse transcription** (synthesizing complementary DNA). This done by the enzyme **reverse transcriptase**.
- This DNA is incorporated into the genome of the host cell. 
- When the host DNA is transcribed, it also produces the virus' RNA.
- HIV is a retrovirus.

## DNA Structure

- DNA is a nucleic acid.
- Nucleotides are the build blocks of nucleic acids.
- 3 components of nucleic acids:
  - **Nitrogenous base**:
    - **Purines**: double-ring (there are 9 of them) $`\rightarrow`$ in nucleic acids are found **A** (adenine) and **G** (guanine).
    - **Pyrimidines**: single-ring (there are 6 of them) $`\rightarrow`$ in nucleic acids are found **C** (cytosine), **T** (thymine) and **U** (uracil).
  - **Pentose sugar** (5-carbon sugar)
  - **Phosphate group**
- In DNA $`\rightarrow`$ T.
- In RNA $`\rightarrow`$ U.

![nitrogenous bases](./img/09_DNA/nitro_bases.png)

- Carbons in the purines and pyrimidines rings are numbered with unprimed numbers (i.e without the ').
- The pentose sugar determines wether it is DNA or RNA: **ribose** sugars will make a **ribonucleic acid (RNA)** and **deoxyribose** sugars will make **deoxyribonucleic acids - DNA**.
- The carbons in the sugars are numbered with numbers with a prime sign (e.g 3').
- Unlike ribose, deoxyribose has an **hydrogen instead of an OH group at C-2'**.

![pentose](./img/09_DNA/pentose.png)

- **Nucleoside** = purine/pyrimidine + ribose/deoxyribose
- **Nucleotide** is a nucleoside with a phosphate group.
- The C-1' atom of the sugar bonds to the nitrogenous base.
  - Purines bond with the sugar on N-9.
  - Pyrimidines bonds with the sugar on N-1.
  - Phosphate can bond with sugar on C-2', C-3' or C-5'. The most common bond in biological systems is to **C-5'**.
- **NMP** - nucleoside monophosphate $`\rightarrow`$ one phosphate.
- **NDP** - nucleoside diphosphate $`\rightarrow`$ two phosphates.
- **NTP** - nucleoside triphosphate $`\rightarrow`$ three phosphates.
- **ATP** (adenosine triphosphate)and **GTP** (guanine triphosphate) are important due to the release of energy from their hydrolysis.
- When a nucleotide/nucleoside has a deoxyribose sugar "d" is added in front of the name: dNDP (deoxynucleoside diphosphate), dTDP (deoxythymidine diphosphate).

![ndp](./img/09_DNA/ndp.png)

- The bond between two mononucleotides is called **phosphodiester bond**.
- The phosphodiester bond is between **C-5'** and **C-3'**.
- **Erwin Chargaff** between 1949-1953 worked on the ratio between the nucleotides in DNA looking at different organisms:
  - A/T ratio is around 1
  - G/C ratio is around 1
  - (A+T)/(G+C) ratio is NOT around 1 $`\rightarrow`$ Not all four bases are present in equal amount.
  - _Note: the amount of G+C ratio is not the same in different species._
- Rosalind Franklin confirmed the **3.4Å** and suggested the double helix structure.

### The Watson-Crick Model

```math
\boxed{
\begin{aligned}
& \text{ - Right handed double helix. }\\
& \text{ - Two antiparallel chains: C-5' to C-3' orientation run in opposite direction. }\\
& \text{ - The bases are flat and perpendicular to the axis. They are stacked on one another at 3.4\AA apart. }\\
& \text{ - The pairing of A/T and G/C is due to the formation of hydrogen bonds (2 between A and T and 3 between G and C). }\\
& \text{ - A complete turn of the double helix is 34\AA high and it takes 10 base pairs to do a turn. }\\
& \text{ - Major groove and minor groove alternate. }\\
& \text{ - The double helix has a diameter of 20\AA. }\\
\end{aligned}
}
```

![dnastructure](./img/09_DNA/dna_structure.png)


#### Base pairing

- $`A = T`$ and $`G \equiv C`$ have respectively 2 and 3 hydrogen bonds between them.
- Paring of purine-purine (A-G) or pyrimidine-pyrimidine (C-T) isn't possible because it would disrupt the $`20 \text{\AA}`$ diameter of DNA. Also the hydrogen bonds don't match.
- A-C and and G-T although they are purine-pyrimidine pairs cannot bond due to the hydrogen bonds not matching.

![hydrogen bonds](./img/09_DNA/hydrogen_bonds.png)


### Other DNA structures

- In biological conditions (aqueous, low salt) two main form are found: **A-DNA** and **B-DNA**.
- A-DNA is a bit more compact (9 base pairs are required to do a full turn).
- A-DNA is right handed but the orientation of the bases is a bit different (tilted).
- We doubt that A-DNA exist in physiological conditions (i.e just in labs).
- With other condiditions (salinity and solute) other forms can be found (right handed: C-DNA, D-DNA, E-DNA, P-DNA).

![a-dna b-dna](./img/09_DNA/abdna.png)

- **Z-DNA** (disconvered in 1979) contains only $`G \equiv C`$ pairs.
- Left handed.

## RNA Structure

- Differences with DNA:
  - Single stranded instead of double helix.
  - Has ribose sugar and not deoxyribose.
  - Has Uracil and not Thymine.
- RNA can fold on itself to form double stranded regions of complementary base pairs.
- Some viruses have RNA as a double stranded helix.
- 3 important types of RNA used during genes expression:
  - **mRNA** - messenger RNA: carries genetic information from the DNA in the nucleus to the ribosome outside the nucleus.
  - **rRNA** - ribosomal RNA: builds ribosomes.
  - **tRNA** - transfer RNA: carries amino acids to the ribosome during translation.
- All 3 types originated as complementary copies of one of the DNA strands during **transcription (שיעתוק)**.
- Svedberg coefficient (S) is the amount of each RNA is organisms.
- Ribosomal RNA (rRNA) constitute about 80% of all RNA in E. coli cell.
- Other types of RNA:
  - Telomerase RNA and RNA primers are involved during **DNA replication**.
  - snRNA (small nuclear RNA) is involved in **processing mRNAs**.
  - antisense RNA, microRNA (miRNA) and short interfering RNA (siRNA) are involved in **gene regulation**.

## Analytical Techniques of DNA and RNA

### Centrifuge

- Density gradient and finding the **sedimentation equilibrium**.
- Separating by speed of sedimentation: **sedimentation velocity centrifugation** (Svedberg coefficient units).

### UV Light Absorption

- Nucleic acids absorb UV lights between **254-260nm** due to the interactions with the bases' rings.
- In aqueous solution the peak is at 260nm.
- Single strand absorbs more than double strand.

### Denaturation and Renaturation of Nucleic Acids

- Heat and stress can cause DNA to denature (lose its shape and function).
- During denaturation only the hydrogen bonds break not the convalent ones $`\rightarrow`$ the double stranded helix separate as 2 single strands.
- Since single strands absorb mmoew UV light the UV light absorption increases. This is called the **hyperchromic shift**.
- The more $`G \equiv C`$ pairs there are in the DNA the higher the temperature required to denature the DNA (3 hydrogen bonds is more stable than 2) thus this technique helps learning about the **composition of the DNA**.
- The **melting temperature $`\bold{T_m}`$** represents the point where 50% of the strands are denatured.
- At the proper temperature the double helix can reform.

### Molecular hybridization

- Renaturation between 2 single strands of DNA from 2 different organisms (as long as they have enough complementary base pairs).
- Can also be done between DNA strand and RNA strand.
- Technique that helps identify certain segments of DNA like the result of transcription.

#### Fluorescent in situ hybridization (FISH)

- Refinement in the molecular hybridization.
- Use of fluorescent probes to monitor hybridization.
- Mitotic or interphase cells are fixed to slides and subjected to hybridization conditions. Single strands are then added (DNA or RNA).
- The resolution of FISH is precise enough to track single genes.

#### Reassociation Kinetics and Repetitive DNA

- Also an extension of molecular hybridization.
- Analyzes the **rate of reassociation** of complementary single DNA strands.
- This technique gives an indication as to the genome size because the bigger the genome the more time it requires to reassemble.
- Parts of the DNA reassemble more quickly than thought at first. Those fragments are copies of the same sequence.

### Electrophoresis of Nucleic Acids

- Important technique in current research.
- Electrophoresis separates molecules by causing them to migrate under the influence of an **electric field**.
- Molecules migrate towards the electrode with the opposite net charge.
- The porous gel used determines the resolution of the experiment:
  - Two differenct polynucleotide chains will move at the same speed because they are both negatively charged and have the same mass/charge ratio.
  - Using a gel with smaller pores will cause the bigger chain to move more slowly.
  
![eletrophoresis](./img/09_DNA/eletrophoresis.png)
