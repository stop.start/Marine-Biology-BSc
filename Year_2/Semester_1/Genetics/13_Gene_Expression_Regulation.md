# Regulation of Gene Expression in Prokaryotes

- Not all genes are expressed at all times in all situations.
- Bacteria regulate gene activity in response to change in their environment and cellular activity like replication, recombination, cell division.
- Enzymes produced when certain substrates are present are called **inductible enzymes**.
- Enzymes produced continuously no matter what are called **constitutive enzymes**.
- A mechanism where the gene expression is stopped under certain conditions is said to be **repressible**.
- **Negative control**: genetic expression occurs unless stopped by a regulator molecule.
- **Positive control**: genetic expression occurs only if a regulator molecule stimulates the process.

## Lactose Metabolism in E. coli

### lac Operon

- 1965: Francois Jacob, Jacques Monod.
- In the absence of lactose, the gene activity is repressed.
- In the presence of lactose, the gene activity is induced: the concentration of enzymes responsible for its metabolism increases rapidly.
  - This means that the enzymes responsible for lactose metabolism are inductible.
- In prokaryotes, genes tend to be organized in clusters with a regulatory region for the transcription located upstream (5') on the same strand ($`\rightarrow`$ cis-acting site).
- The cis-acting regulatory regions bind molecules that control the transcription of the gene cluster. The binding can regulate either positively or negatively.
- The regulatory regions together with the gene cluster is called the **lac(tose) operon**.

![lac operon](./img/13_Gene_Expression_Regulation/lac_operon.png)

- **Structural genes** code for primary structure of enzymes.
- In the lac operon the **lacZ** gene encodes for **β-galactosidase**.
  - **β-galactosidase** is an enzyme that converts the disaccharide lactose to the monosaccharides glucose and galactose.
  - Glucose is an energy source for glycolysis.

![lacz](./img/13_Gene_Expression_Regulation/lacz.png)

- The **lacY** gene encodes for the **permease** enzyme that facilitates the entry of lactose into the bacterial cell.
- The **lacA** gene encodes for the **transacetylase** enzyme which seems to be involved in the removal of toxic by-products.
- The importance of the genes in lactose metabolism has been studied by isolating mutations on those genes (mainly lacZ and lacY). Cells failing to produce β-galactosidase or permease are unable to use lactose as an energy source.
- Mapping the genes revealed the order lacZ-lacY-lacA.
- All 3 genes are transcribed as a single unit. That type of mRNA produced is called **polycistronic mRNA** (cistron refers to a part of mRNA coding for a single gene).
  - On the mRNA: 3 Shine-Dalgarno sequences, 3 start codons AUG, 3 stop codons.
  - This method allows to coordinate the regulation for all 3 genes.

### Repressor gene

- The **lacI** gene is located close to the cluster but isn't part of it. This is is called a **repressor gene** and codes for a protein that prevents the transcription of the 3 lactose genes (lacZ,lacY,lacA).
  - This gene is expressed when there is no need for lactose enzymes.
  - **The molecule that the lacI gene specifies for sits on the regulator region (located between the promoter and the 3 genes) thus preventing the RNA polymerase to transcribe the gene after it attached to the promoter** (picture b).
  - If the repressor isn't needed **a lactose sugar (called inducer) binds to it, causing a conformational change and the repressor cannot bind to the regulatory region** (picture c).
- The repressor actually has several binding sites and subunits to bind to them (and they bind to one another) --- (ppt13/slide 24 | page 438/439).
  
  ![repressor](./img/13_Gene_Expression_Regulation/repressor.png)
  
- A bacteria with mutation(s) prevent the synthesis of certain molecules (like the repressor) can be "helped" by adding a **plasmid** (e.g **F plasmid**) which adds DNA for certain genes in the bacteria.
  - This allows the bacteria to synthesize the needed proteins to function normally.
  - The $`I^-`$ mutation prevent the synthesis of the repressor or the repressor formed isn't able to bind to its site, thus adding a plasmid with $`I^+`$ gene will fix that.
  - The $`O^c`$ mutation means that the regulatory region is altered and prevents the repressor to bind to it. In this case adding a plasmid with $`O^+`$ region will not help since it needs to be adjacent to the genes to be repressed.
- In the below picture:
  - Z represents the structural genes (lacZ,lacY,lacA).
  - The genes after F' means that they were inserted by plasmid.
  - The part C of the table added mutated genes ($`I^-`$ and $`O^c`$).
  - $`I^S`$ is a mutation causing the repressor to always bind to the O region (regulatory region or Operator region). This means that adding and $`I^+`$ plasmid won't help.

![lactose mutations](./img/13_Gene_Expression_Regulation/lactose_mutations.png)

### Catabolite-Activating Protein (CAP)

- When lactose is present and the lac genes expressed, the β-galactosidase converts lactose into glucose and galactose then galactose is converted to glucose.
- When lactose is present but there's also enough glucose, expressing the lac genes is not energetically efficient:
  - A **catabolite-activating protein (CAP)** is a molecule whose role is to decrease the expression of the lac operon when glucose is present. This process is called **catabolite repression**.
  - CAP actually facilitates the process of RNA polymerase binding to the promoter region.
  - In the absence of glucose, CAP exerts positive control by binding to the CAP site (on the promoter) and facilitating the RNA polymerase binding at the promoter site.
- The **cAMP** molecule (cyclic adenosine monophosphate) is involved in the CAP binding: CAP must be bound to cAMP in order to bind to the promoter.
  - The level of cAMP depends on an enzyme which catalyzes the conversion of ATP to cAMP.
  - Glucose inhibits the activity of this enzyme: more glucose $`\rightarrow`$ less cAMP $`\rightarrow`$ CAP not binding $`\rightarrow`$ RNA polymerase binds a lot less.
  
  ![camp](./img/13_Gene_Expression_Regulation/camp.png)
  

### IPTG

- Lactose is an inducer of the gene expression (as well as the substrate that's going to be broken down into glucose).
- Inducer in hebrew: משרן
- IPTG is a non natural inducer used in laboratories.
- IPTG cannot be used as substrate.

## Tryptophan (trp) Operon in E. coli

- Repressible operon.
- Tryptophan is an amino acid that was studied as an example of how amino acids are regulated.
- If there is enough tryptophan the required enzymes for its synthesis are not produced.
- 5 contiguous genes encodes enzymes involved in tryptophan synthesis. They are part of an operon.
- The repressor is inactive (cannot interact with the operator region) until tryptophan binds to it which causes a conformational change.
- This is a negative control system and since tryptophan itself is part of it, it is said to be **corepressor**.

![tryptophan](./img/13_Gene_Expression_Regulation/tryptophan.png)

- In addition, a sequence on the mRNA (meaning after transcription occured thus not repressed) called **leader sequence** contains another regulatory site called **attenuator**.
- Binding to those sites alters the secondary structure of the mRNA and causes premature transcription,termination or repression of translation.
- Two types of regulations: **attenuation** and **riboswitches**.

### Attenuation

- Even when the trp operon is repressed, initiation of transcription still occurs at a low level but is terminated prematurely. This is called attenuation.
- The RNA produced from the trp operon can fold into a secondary structure.
- In prokaryotes, ribosomes start the translation before the transcription is finished.
- When there's no tryptophan, the RNA forms one loop and the ribosomes advance slowly due to the lack of tryptophan.
- In presence of tryptophan, the RNA forms two loops and since tryptophan is present the ribosomes advance quickly and cause the RNA polymerase to fall off.

![attenuator](./img/13_Gene_Expression_Regulation/attenuator.png)

### Riboswitches

- _Not for the exam_
- Parts of the RNA can bind to ligands whose synthesis in controlled by the genes encoded by the mRNA.
- Binding of the ligands causes conformational changes. In the end there transcription terminator structure that is created.

## The ara Operon

- _Not for the exam_
- Both positive and negative control.
