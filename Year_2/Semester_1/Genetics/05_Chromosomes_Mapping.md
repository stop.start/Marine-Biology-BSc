# Chromosomes Mapping in Eukaryotes

- Genes on the same chromosomes are said to be linked (**linkage**).
- During meiosis **crossing over** happens which causes **recombination** of the alleles.
- The frequency of crossing over is proportional to the distance between two loci (the frequency increases with the distance).
- **Chromosome maps** show the relative location of genes on a chromosome.
- **Parental** or **noncrossover** gametes are gametes that were formed without any crossing over.
- Genes that close to each other show **complete linkage**.
- Complete linkage causes to produce only parental gametes.
- **Morgan**, who discovered the x-linkage with crossing involving only one gene, made crossing involving two x-linked genes and got different ratios of parental (expected):recombinant (not expected) 99.5%:0.5% with one crossing and with another crossing involving other genes he got 65.5%:34.5%.
- Morgan proposed the theory of the crossing over where the closer the genes are on the chromosome the more likely they will be passed on together (without recombination between them).
- **Sturtevant**, student of Morgan, used Morgan's findings to **map linked genes**: if two genes are always passed on together they must be really close on the chromosome, the farther they are the more they will be separated during gametes formation.
  - Using this principle Sturtevant mapped the yellow, white, miniature genes of the Drosophila as follow:
  
  ![map](./img/05_Chromosomes_Mapping/map.png)

- The **map unit (mu)** is the percentage of recombinations in F2.
- Crossing over happens mainly in females.
- Because the cross over is a random process, the probability of getting a certain combination of two genes, i.e _Ab_, _AB_, is 50%.
- Double cross over (DCO) happens. The probability of a specific double cross over is equal to the product of the probability of each single cross over:
  - Probability of cross over between A and B is 0.20
  - Probability of cross over between B and C is 0.30
  - Probability of double cross over: 0.2 x 0.3 = 0.06 = 6%
- Mapping 3 or more genes by using enough heterozygous organisms for all genes where the genotype can be seen in the phenotype. _For example:_
  - Testing on 10000 Drosophila with the following genes: yellow body, white eye color, echinus eye shape.
  - Those genes are x-linked which facilitates the results.
  - Propose an order for those genes: _y, w, ec_
  - The P1 female is homologous for all recessive alleles. The P1 male has all dominant alleles.
  - The F1 females are thus all heterozygous for all genes and the F1 males all have the recessive alleles.
  - There are **4 options** for the F2 generation:
    - No cross over (**NCO**)
    - Single cross over (**SCO**) - between _y_ and _w_.
    - Single cross over (**SCO**) - between _w_ and _ec_.
    - Double cross over (**DCO**)
  - From the table below we can see that:
    - NCO is the majority (94.44%)
    - SCO between _y_ and _w_ happens 1.5% + 0.06% from the DCO thus in total 1.56%.
    - SCO between _w_ and _ec_ happens 4% + 0.06% from the DCO thus in total 4.06%.
  - We can conclude that _y_ and _w_ are closer than _w_ and _ec_.
  
  ![three point mapping](./img/05_Chromosomes_Mapping/mapping.png)
  
- Determining the right sequence for 3 genes can be done with the following method by using the above process:
  - Listing all the possible orders for the 3 genes. With the above example: _w, y, ec_ or _y, ec, w_ or _y, w, ec_.
  - In the F2 generation the DCO is the least common thus easy to spot and use. Those rare cases have to be from a double cross over.
  - In the above example, there 2 phenotypes from DCO (listing only the recessive traits): yellow body + echinus eye shape and white eye.
  - The first option (_w, y, ec_) doesn't match because a DCO would have yield: white eye + echinus eye shape and yellow body.
  - The second option (_y, ec, w_) doesn't match either because a DCO would have yield: yellow body + white eye and echinus eye shape.
  - The third option does match.

  ![method 1](./img/05_Chromosomes_Mapping/method1.png)
  
- Determining the correct sequence for 3 genes on autosomal chromosomes (using maize and genes _bm, v, pr_ where _+_ means $`bm^+ \;or\; pr^+ \;or\; v^+`$):
  - One parent is heterozygous on all alleles, the other has all recessive alleles.
  - We don't know which alleles is on which chromosome for the heterozygous parent (recessive, wild type alleles can be mixed on the chromosomes). We only know that they are heterozygous for all alleles.
  - When looking at the offprings, we can conclude from the NCO that the alleles from the heterozygous parent are distributed as follow: one chromosome with _+, v, bm_ and its homolog with _pr, +, +_. Note that the order on each homologous chromosome is still unknown! From the NCO we can just determine which alleles are on the same chromosome.
  - Listing all the arrangements possible for each chromosome and looking at the DCO we can eliminate 2 possibilities and confirm the third (see pictures below): _v, +, bm_ and _+, pr, +_
  - Once the order has been confirmed, looking at the SCO allow to determine the distance between the genes: distance between _v_ and _pr_ is 7.8 + 14.5 = 22.3 and between _pr_ and _bm_ 7.8 + 35.6 = 43.4
  
 ![mapping maize](./img/05_Chromosomes_Mapping/maize.png)
 
- In reality, it is difficult to detect multiple crossovers especially when the number is even because the original order of the genes that are followed can be restored.
- In addition to that the farther two genes are the more inaccurate the calculated distance between them is.

![nondetectable](./img/05_Chromosomes_Mapping/nondetectable.png)

- Also if we look at the maize example we can see that the calculated DCO frequency is higher than the actual frequency: $`22.3% \times 43.4% = 9.7% > 7.8%`$. 
- That's because of **interference (I)**: other crossovers happen in addition to those that we're looking for. There can be **positive interference** or **negative interference**.
- Harriet Creighton and Barbara McClintock demonstrated that the chiasmata formation during meiotic prophase I are the visible manifestation of crossover.
  - They used chromosome 9 on maize which has a special homolog: the homolog has on one end a **knob** and on the other a **translocated segment** from another chromosome (8).
  - The phenotypes gotten from a crossover matched the transfer of those "special ends" from one homolog to the other.
  
  ![chiasmata](./img/05_Chromosomes_Mapping/chiasmata.png)
  
- The **log score analysis** is a statistic model that helps measuring the probability of two genes being linked.
- Using markers - known genes that are easy to observe and that seem to be close to the gene of a trait of interest - is a way to follow other traits:
- Known genes can actually be marked.
- **Somatic cell hybridization** helps assigning human genes to their respective chromosomes.
  - Two cells in culture can be fused into one single cell. Human and mouse cells can be fused.
  - At some point the two nuclei fuse together.
  - After several generations, the chromosomes of one of the parental species are almost completely lost leaving only a few random. In human/mouse fusion the human chromosomes disappear.
  - This process helps isolate human chromosomes.
- In mitosis crossovers also happen when holomog chromosomes are next to each other (rare). Two options are possible:
  - The two chromatids that exchanged genetic information end up in the same cell: there is no real change because all the same alleles are still there. The dominant allele is still dominant even if it went to the homolog chromosome.
  - The two chromatids that exchanged genetic information end up in seperate cells: there is no real change because all the same alleles are still there. The dominant allele is still dominant even if it went to the homolog chromosome.
   
  ![mitosis](./img/05_Chromosomes_Mapping/mitosis.png)

