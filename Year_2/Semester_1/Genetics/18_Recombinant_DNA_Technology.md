# Recombinant DNA Technology

- **Recombinant DNA**: DNA created by joining together pieces of DNA from different sources (and also naturally by crossing-over).

## Cloning DNA

- **Restriction enzymes**: enzymes allowing to cut DNA.
  - They are procuced by bacteria as a defense mechanism against infection by bacteriophage by degrading the DNA of invading viruses.
  - The restriction enzyme binds to DNA at a specific nucleotide sequence called **recognition sequence** or **restriction site**.
  - The enzyme cut both strands of the DNA by cleaving the phosphodiester backbone of DNA.
- **Restriction sites** are present randomly in the genome and are usually 4-6 bases long and the restriction enzymes will cut at every sequence recognized.
  - They form a palindrome when reading from 5' to 3' on both strand (GAATTC on one strand from 5' to 3' will be the same on the other strand from 5' to 3').
  
  ![palindrome](./img/18_Recombinant_DNA_Technology/palindrome.png) 
  
- **EcoRI** and **HindIII** are restriction enzymes making offset cuts leaving single stranded overhanging "sticky" ends that are called **cohesive ends**.
- **AluI** and **BalI** are restriction enzymes that cut both strands at the same nucleotide pair which procuces DNA fragments with double stranded ends called **blunt ends fragments**.

![types of cuts](./img/18_Recombinant_DNA_Technology/types_cuts.png)

- EcoRI is a restriction enzyme from E. coli (R strain, the I to be identified).
- The fragments cut with one enzyme can be joined together since they are all cut the same way and all have the same complementary ends.
- Overhanging ends are easier to use in order to join fragments since they will form hydrogen bonds. Blunt ends don't have this advantage.
- DNA ligase is used to finish joining the DNA fragments.

![ecori](./img/18_Recombinant_DNA_Technology/ecori.png)

- **Cloning vectors** are DNA molecules that accept DNA fragments and replicate these fragments when vectors are introduced into hosts cells.
- Different vectors can enter different host cells.
- Vectors contain several restriction sites that allow the insertion of DNA fragments.
- **Bacterial plasmids** were the first vectors developed derived from naturally occuring plasmids.
- **Plasmids** are extrachromosomal, double stranded circular DNA molecules that replicate independently from the chromosomes within bacterial cells. They usually contain one gene and one origin of replication (ORI).
- External plasmids are introduced into bacteria via **transformation**.
- Plasmids with the recombinant DNA is also given a gene for antibiotics resistance (like ampicilin) so the bacteria that didn't get the plasmid can be killed.
- Plasmids inside the cell replicate to produce many copies.
- **Blue-white selection**:
  - 2 genes are incorporated into plasmids: the gene for antibiotics resistance and lacZ gene.
  - lacZ gene encodes for β-galactosidase which breaks down lactose into glucose and galactose.
  - Cells that didn't take in plasmids or whose plasmids didn't take in the antibiotics resistance gene are killed with antibiotics.
  - Human DNA is then incorporated instead of the lacZ gene in the plasmids.
  - The enzymatic activity of the β-galactosidase allows to stain in blue the cells that did not take in the human DNA. The cells that did take it in will be white.
  
![blue white](./img/18_Recombinant_DNA_Technology/blue_white.png)

- **Insulin** was the first gene to be synthesized by recombinant DNA.
- TPA (tissue plasminogen activator) for dissolving blood clots is also a product of recombinant DNA.
- The whole list: ppt18/slides 42-43.
- Other types of cloning vectors: 
  - **Phage vector systems** - bacteriophage λ whose genome has been modified to incorporate important features of cloning vectors (like a multiple cloning site).
    - Phage vectors can carry bigger DNA than plasmids.
  - **Cosmid** plasmid-like DNA that a phage can pack (?).
  - **BAC** (bacterial artificial chromosome): replace the whole chromosome of the bacteria. Used to clone large fragments of DNA.
  - **YAC**: yeast artificial chromosome (eukaryotic system).
  - **Expression vectors** are designed to ensure that the mRNA of a cloned gene is expressed (the other vectors just insert the DNA but do not cause its transcription).
  - **HAC**: human artificial chromosome. Need centromeres since the cells divide and need telomeres that are at the end of the chromosomes (that are not circular).

|Phage|HAC|
|--|--|
|![phage vector](./img/18_Recombinant_DNA_Technology/phage_vector.png)|![human vector](./img/18_Recombinant_DNA_Technology/hac.png)|  

![dna size clone](./img/18_Recombinant_DNA_Technology/dna_size_clone.png)

## DNA Libraries

- DNA libraries are a collection of cloned DNA.
- 2 main types of libraries:
  - **Genomic DNA** libraries.
  - **cDNA (complementary DNA)** libraries.
- Genomic libraries: many overlapping fragments of the genome with at least one copy of every DNA sequence in an organism's genome.
- In order to create a genomic library:
  - DNA is extracted from cells or tissues and cut randomly with restriction enzymes.
  - The fragments are inserted into vectors.
  - It is important to select the vectors so they can carry the fragments but still need to be as small as possible in order to be usable.
  - Genomic DNA is the foreign DNA in the vectors.
  - Vectors can carry coding and noncoding segments of DNA and they can contain more than one gene or only a part of a gene.
  - For the human genome YACs were mostly used.
- cDNA libraries: 
  - Made from mRNA molecules representing genes being expressed in cells at the time the library was made.
  - Contains only expressed genes.
  - cDNA is complementary to the nucleotide sequence of the mRNA and does not contain noncoding sequences (unlike genomic DNA).
- In order to create a cDNA library:
  - mRNA is isolated from cells.
  - Eukaryotic mRNA has a polyA tail at 3'.
  - Adding a single stranded sequence of T nucleotides which will bond to the poly-A tail.
  - The enzyme **reverse transcriptase** extends this primer (made of T) and synthesizes the complementary DNA copy of the mRNA sequence.
  - This gives an hybrid mRNA-DNA double stranded molecule.
  - The mRNA is then degraded and replaced with a DNA strand (via DNA polymerase).
  - cDNA molecules are then inserted into plasmid.
- Several cDNA libraries exist for different type of cells (that express different genes).

![cdna](./img/18_Recombinant_DNA_Technology/cdna.png)

- **Library screening**: finding a specific gene.
  - **Probe**: DNA or RNA sequence complementary to some part of a cloned sequence present in the library.
  - The probe is tagged then used to bind to complementary DNA sequences in clones in the library (this means that those clones contain the wanted or part of it).
- Screening can be done with probes that are DNA sequences from other species but close enough to be used to screen (some Drosophila genes are closed enough to humans' to be used).
  
  ![screening](./img/18_Recombinant_DNA_Technology/screening.png)

- **Restriction mapping**: gives the number, order and distances between the restriction enzyme cleavage sites along a cloned segment of DNA. This gives us information about the length of the cloned sequence and the location of restriction-enzyme cleavage sites.
- Restriction maps were created by cutting DNA with different restriction enzymes and separating DNA fragments by gel electrophoresis.
- **Southern blotting** is a technique for detecting DNA-RNA hybrids.
  - It can be used to identify which clones in a library contain a given DNA sequence.
- Southern blotting has 2 components:
  - Separation of DNA fragments by gel electrophoresis $`\rightarrow`$ gives the number of fragments produced by restriction digestion and give their molecular weight.
  - Because of the amount of DNA in the human genome the result of the gel is a continous smear.
  - Hybridization of the fragments using labeled probes.
    - DNA is cut into fragments with restriction-enzymes which are then separated by gel electrophoresis.
    - Those fragments are then paired with probes (hybridization).
- Other types of blotting:
  - RNA blotting which is called Northen blot analysis.
  - Western blotting is used to identify proteins.
- **RFLP**- Restricton Fragment Length Polymorphism​:
  - Can identify mutations: restriction enzymes cut at specific sites. If there's a mutation that causes the enzyme not to cut at a specific site, running the DNA sequences and comparing to others will show one band instead of two.
  - In the picture the parents are heterozygous for the wild-type/mutation allele. The daughter (white) is homozygote for the wild-type allele. The son is sick and the gene can't be cut by the restriction enzyme due to the mutation.
  
  ![rflp](./img/18_Recombinant_DNA_Technology/rflp.png)

## PCR

- PCR - polymerase chain reaction.
- Revolutionized recombinant DNA methodology.
- Rapid DNA cloning method which doesn't always require host cells.
- It requires:
  - DNA sample (double stranded)
  - DNA primers (single stranded). One is complementary to the 5' end of one strand and the other complementary to the 3' of the other.
  - Raw nucleotides
  - DNA polymerase
  - Stable solution (with $`Mg^{2+}`$ which is a cofactor of enzymes).
- How it works:
  - The DNA sample is **denatured** into single strands.
  - Then the primers bind to their complementary sequence. This is called **annealing**.
  - DNA polymerase can extend the primer sequences.
- One complete reaction is called a **cycle** and doubles the number of DNA molecules.

![pcr](./img/18_Recombinant_DNA_Technology/pcr.png)

- Because heating is required to denature the DNA, at some point proteins like DNA polymerase are also denatured.
- The DNA polymerase used is **Taq polymerase** which can withstand extreme temperature changes.
- Cloning only a specific (target) sequence of the DNA means that the primers are not placed at the end of the strand.
  - This means that the target sequence is at first cloned with non relevant information.
  - On the first cycle, the irrelevant information between the start of the primer and the 3' end of the template strand is "cut off" (not copied).
  - On the second cycle, the other irrelevant information is not copied.
  - From the third cycle relevant target DNA copies are created.
  - Because the longer DNA strands with irrelevant information are still in the solution they will also be duplicated but at a lower rate.
  - See: [video](https://www.youtube.com/watch?v=iQsu3Kz9NYo)
- From the above we get that:
  - First cycle: 0 target molecules
  - Second cycle: 0 target molecules
  - Third cycle: 2 target molecules
  - Fourth cycle: 8 target molecules (2 from the previous cycle + 2 from those molecules + 4 from the "half molecules")
  - Fifth cycle: 22 target molecules
  - Number of relevant molecules on the $`n^{th}`$ cycle: $`\bold{2^n}`$.
  - Number of irrelevant molecules on the $`n^{th}`$ cycle: $`\bold{2n}`$.
- PCR should have exponensial result but at some point it reaches a plateau. This because after denaturation, the strands can bind together instead of the primers binding to them.
- PCR can be done on a single-stranded DNA molecule.
- RNA can also be used by using reverse transcription which result in a single stranded DNA molecule.
- **Primer dimer**: two primers bonding (not good).
- Custom sequences can be added to the 5' end of the primer thus created new molecules while cloning.
- **Real-time PCR**: see PCR happen in realtime.
- **Hot Start PCR** prevent the primers to bind before the first denaturation (so they won't bind to one another). An enzyme on an antibody is used.
- **TA cloning**: adds an A at the 3' end each cycle. Used to build vectors with identifyable AAA ends and plasmids with TTT overhangs.
- **AFLP**: Amplification Fragment Length Polymorphism: first PCR and then used with restriction enzyme.
- **PCR Finferprinting**: using repeat sequences (?)

## CRISPR

- Gene editing within the cell.
- CRISPR:
  - Clustered ​
  - Regularly ​
  - Interspaced ​
  - Short ​
  - Palindromic ​
  - Repeats​
- Protein that allow bacteria to cut (from a phage) and insert a DNA sequence in order to "remember it".
- **Cas9**: an enzyme that binds to the DNA and cuts it.
- Cas genes: cutting proteins
- In E. coli, it is like an immune system: if the virus DNA is know it will trigger the protein for this DNA and kill it. If not known it will trigger the protein that cuts DNA and inserts it into its genome.
