# DNA Organization in Chromosomes

## Viral and Bacterial Chromosomes

- Single molecule.
- No associated proteins.
- Viruses chromosomes can be either DNA or RNA and can either be circular or linear.
- Bacteria chromosomes are circular and bound to proteins. 
  - The area where the chromosome is found is called **nucleoid**
- Circular chromosomes are denser than linear chromosomes of the same molecular weight.
- A **supercoiled** DNA is even denser than just circular DNA:
  - The **linking number L** is the number of turns of a molecule.
  - Unwinding X turns in the double helix, causes more tightly packaging. 
  - In order the relieve the energy strain caused by this, the molecule changes conformation by forming supercoils. 
  - The number of supercoils is equal to the number of turns unwound.
- **Topoisomers** are identical molecules that differ by their linking number (some turns were unwound and supercoils formed).
- **Topoisomerases** regulate the energy strain by helping forming supercoils.

![supercoils](./img/11_DNA_Organization/supercoils.png)

## Eukaryotes

- 2 main states of the chromosomes:
  - During **interphase** the chromosomes are in **chromatin** form: not condensed. 
    - In this state the DNA can be used, i.e expressed into proteins, duplicated, etc.
  - Chromosomes are visible when they are condensed during **mitosis**.
    - After G2, the chromosomes are condensed which facilitates their equal distribution in the daughter cells.
- All 46 chromosomes of the human DNA put together and extended would be 2 meter long.
- The nucleus is a lot smaller than the size of DNA (around 5-10μm).
- **Histones** are positively charged proteins (nonhistones are less positively charged) that can bond electrostatically to the **negatively charged phosphate groups** of nucleotides.
- There are serveral types of histones.
- The DNA wraps around histones of different types.
- A **nucleosome** is a basic unit of DNA packaging in eukaryotes, consisting of a segment of DNA wound in sequence around eight histone protein cores (octamer).
  - **H3 and H4** form one dimer and a tetramer (two H3-H4 dimers), same with **H2A and H2B** form another tetramer (two H2A-H2B dimers).
  - Those two tetramers form the octamer.
  - They can be seen in chromatin as beeds on a string.

![octamer](./img/11_DNA_Organization/octamer.png)

- The area of linker DNA is the nuclease cut site.
- Regular DNA is 2nm diameter.
- A nucleosome beed has a diameter of 11nm.
- **Solenoids** are condensed beeds (nucleosomes) and have a diameter of 30nm.
  - Histones H1 allow to tie up and hold the solenoids.
- Those solenoids curl up around protein filament into a structure of 300nm.
- An additional condensation gives a structure of 700nm diameter for the entire chromosome (1400 together with its homolog).
- [Most info in histones from biology first year](../../Year_1/Semester_1/Biology/09_DNA_to_Protein.md)

![chromatin](./img/11_DNA_Organization/chromatin.png)

- Chromosomes condensation stages:

![condensation](./img/11_DNA_Organization/condensation.png)

- **Chromatin remodeling** allows the DNA to be exposed in order used (replication, transcription) since the histone octamers can be in the way.
  - **Histone tails** are not folded in the core of nucleosomes but protrude from it.
    - H3 and H2B tails protrude from the minor groove.
    - H4 tails connect with adjacent nucleosomes.
  - **Enzymes attach amino acids to those tails to regulate gene expression**.
  - **Acetylation** and **methylation** are linked to gene activity.
    - Methylation and gene activity depends on where the mark is placed.
     
    > Lysine acetylation is linked to gene activation.  
    > Methylation** of lysine and arginine has been linked to gene activity.  
    > Serine phosphorylation during cell cycle linked to unfolding and condensation during and after DNA replication.  
    
- Chromatin remodeling (histonic code in the ppt) is a topic of **epigenetic**.
- Epigenetics: Instructs cells how and when to read the DNA blueprint.

### Heterochromatin and Euchromatin

- Some parts of the chromosome remain condensed even during interphase.
- Euchromatin and heterochromatin respectively describe the parts of chromosomes that are uncoiled and those that remain condensed.

![heterochromatin](./img/11_DNA_Organization/heterochromatin.png)

- Heterochromatin are usually inactive from lack of genes or contain repressed genes.
- **2-10%** only of the genes code for proteins. Most of them are repetitive sequences.
  - Telomere and centromere are heterochromatin regions that are repetitive.
- Heterochromatin is only in eukaryotes.
- The mammalian Y chromosome is entirely heterochromatic.

- Repetitive DNA:
  - **VNTRs**: variable number of tandem repeats (minisatellites) - **tens of base pairs**.
  - **STRs**: short tandem repeats (microsatellites) - **couple of base pairs**.
  - **CNVs**: copy number variation (12% of the human genome) - **thousands of base pairs**.
  
![repetitive dna](./img/11_DNA_Organization/repetitive_dna.png)

- The length of these regions varies from individual to individual and STRs were used in forensic technique called  **DNA fingerprinting**.
- **SINEs** (short interspersed elements) are less than 500 base pairs and may be present 1500000 times or more in the human genome.
- **LINEs** (long interspersed elements) same idea as above but really long and they are present 850000 times.

### Replication

- During replication, H3 and H4 remain attached to the DNA but not H2A and H2B in order to allow the replication fork.
- Histone chaperone are negatively charged proteins that help reform the octamers on the DNA.

### Other stuff

- The smallest genome is one chromosome (male ant, haploid). 
- Biggest: 630 pairs of chromosomes (something something).
- There is no such thing as junk DNA.
- **CpG Islands**: regions with a lot of G and C.
  - Methylation of those regions allow MeCP proteins to attach.
  - Gives histone deacetylase complex.
  - Causes a heterochromatic region (gene isn't expressed).
