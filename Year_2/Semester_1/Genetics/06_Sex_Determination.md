# Sex Determination

## Intro

- Several reproduction modes exist: sexual, asexual, alternating between both.
- In most diploid eukaryotes reproduction is done sexually.
- In mammals, the XY chromosomes characterize the sex of an individual although in many species specific genes actually determine the sex of an individual.
- **Primary sexual differenciation**: involves only the gonads where gametes are formed.
- **Secondary sexual differenciation**: overall appearance of the organism, including mammary glands and external genitalia.
- Chlamydomonas - the green alga:
  - Reproduce mainly asexually by mitosis (haploid organisms).
  - When in stress, for example **nitrogen depletion**, they reproduce sexually and produce a diploid organisms until the conditions are more favorable, then they go back to reproduce by mitosis.
  - There isn't much differences between haploid and diploid organism.
  - The gametes that fuze to produce a diploid organism aren't distinguishable but there are some genetic differences and gametes will fuse with gametes that have a certain opposite trait ($`mt^+`$ and $`mt^-`$).
- C. elegans - nematode (roundworm):
  - Either hermaphrodite (99%) or male (1%).
  - Self fertilization of hermaphrodites. Males can mate with hermaphrodites producing half males and half hermaphrodites.
  - The genes that determine if a nematode is male or hermaphrodite is located on the X chromosome as well as autosomal chromosomes.
  - Nematodes have no Y chromosomes: hermaphrodites have two X chromosomes and males have only one.
- Butterfly Protenor also has females with two X chromosomes and males with only one. The presence or absence of the second X chromosome determine the sex of the individual.
  - Male produce gametes either with an X chromosome or without.
- The bug Lygaeus turcicus has X and Y chromosomes, meaning that females have two X chromosomes and males have XY chromosomes.
  - Gametes will be produced with an X chromosome in females and either with an X or Y chromosome in males (50%-50%).
  - Males are said to be **heterogametic** while females are **homogametic**.
- In some species the female is heterogametic while the male is homogametic. In this case, the convention is to use the notation ZZ/ZW instead of XX/XY (ZZ=male, XX=females).

## X and Y chromosomes

- Although in humans the Y chromosome is found only in males, its presence alone doesn't prove that this the case:
  - The Y chromosome could have no role.
  - Having two X chromosome could determine the female sex, then missing one X would cause the male sex.
- Two conditions, **Klinefelter syndrome (47,XXY)** and **Turner sundrome (45,X)** helped indentifying Y as the determining sex chromosome:
  - In Turner syndrome, the presence of one X chromosome is enough to present female characteristics (although no complete).
  - In Klinefelter syndrome, despite two X chromosomes, the presence of the Y chromosome gives male characteristics (although not complete).
- Other karyotypes for Klinefelter syndrome: 48,XXXY, 48,XXYY, 49,XXXXY, 49,XXXYY.
- Klinefelter syndrome characterizes with:
  - Tall with long arms and legs and large hands and feet.
  - Male genitalia and internal ducts.
  - Steril.
  - Slight enlargement of breasts and rounded hips (female characteristics).
- Turner syndrome characterizes with:
  - Female genitalia and internal ducts.
  - Steril.
  - Short and big neck.
  - Underdeveloped breasts.
  - Congnitive impairement.
  - 1/2000 female. Some might die utero so maybe the frequency is higher.
- **47,XXX** syndrome:
  - 1/1000 females
  - Usually the females with this syndrome are unaware of it and develop normaly. 
  - Some might be steril, have mental retardation, delayed development of language and motor skill.
  - **48,XXXX** and **49,XXXXX** are similar but show more issues than 47,XXX.
- **47,XYY** condition:
  - Males with below average intelligence and personality disorders.
  - Tall.
- Every human embryo start to develop with the potential to go either male or female. In the presence of a Y chromosome, around the 7th week, the male development starts and disable all female development. In the absence of a Y chromosome, around week 12, the female development starts.

### The Y chromosome

- The Y chromosome has around 50 genes compared to around 1000 on the X. 
- Regions of the Y chromosome:
  - **PAR**: pseudoautosomal region, this is the region with shared genes with the X chromosome.
  - **MSY**: male specific region of the Y doesn't recombine with X.
  - **SRY**: sex determining region Y is a critical gene that controls male sexual development.
    - At 6-8 weeks of development, SRY gene becomes active in XY embryos.
    - It encodes a protein that causes undifferentiated gonadal tissue of the embryo to form testes. The protein is called testis-determining factor **TDF**.
- Human males with two X and no Y: usually the SRY gene got attached to one X chromosome.
- Human females with X and Y: usually the Y chromosome is missing the SRY gene.
- SRY gene has been found in all mammals examined.

![tdf par](./img/06_Sex_Determination/tdf.png)

- The proportion male to female ratio isn't 1:1. 
- The **sex ratio** is assessed in two ways: 
  - **Primary sex ratio**: ratio males to females conceived.
  - **Secondary sex ratio**: ratio males to females born.
- In the US the secondary ratio was 1.06. Data from embryos that did not survived show that most of them are males setting the primary ratio at around 1.079 (male:female).
- One plausible explaination for the ratio would be that since the Y chromosome is smaller and lighter, the y-bearing sperm have a higher mobility thus more likely to fertilize the egg.

## X-linked genes and Dosage compensation

- Because female have two X chromosomes they have the potential (good or bad) to produce twice as much of products of X-linked genes.
- Males and females having more X chromosomes than they should are thought to have a dosage problem.
- Three strategies can be found among different species:
  - Decrease the gene expression of X chromosomes in females (worms) 
  - Increase the gene expression of X chromosomes in males (flies)
  - Inactivate one X chromosome in females (mammals)

### Barr Bodies

- Barr and Bertram experimented with female cats.
- They observed a **darkly staining body** in interphase nerve cells of female cats but there was none in males.
- The Barr bodies can be found in human as well (females not males).
- They are **condensed structures** around 1㎛ in diameter and are located against the **nuclear envolope**.
- The Barr body is an **inactivated X chromosome**.

![barr body](./img/06_Sex_Determination/barr_body.png)

- From observations, the number of inactivated X chromosomes is N-1 where N is the number of X chromosomes. This raises the question why the females with an unconventional number of X chromosomes aren't normal?
- Males with two X chromosomes have also Barr bodies, which then raises the same question as above.
- The inactivation starts after there are already 100 cells in the foetus, meaning that the two X chromosomes (or more) can be expressed during that time. Having only one X or more than X can thus be detrimental to the development of the individual.
- Also the inactivation does seem to cover all of the genes.
- Which of the two X is inactivated is **completely random in each of the first cells** (some will inactivated the dad's X and some the mom's X), then during cell division they pass on which X is inactivated.
- Statistically, an heterozygous females will stay that way with half cells inactivating one X chromosome and the other half the other X chromosome. In reality, some females have more of a specific X chromosome inactivated which can bring the recessive genes to be "active".

> Calico cats have three colors and are almost exclusively females.  
> This is because the gene for the coat color (either black or orange) is one the X chromosome while the gene for the white color is on another chromosome.  
> The random inactivation causes some cells to code for black coat, some for orange and some have the white coat activated from the other gene. This results in a 3 colors coat.  
> Males have only one X chromosome and that can't be inactivated thus there is only one coat color gene, plus the white coat gene: at most 2 colors.  
> This is called the Lyon hypothesis.  

### The mechanism of inactivation

- **Imprinting** is the process of passing on which genes are not expressed (more or less, not exact definition).
- **Xic - X inactivation center** is a region that is expressed in inactivated X chromosomes.
- Xic contains the **XIST - X-inactive specific transcript** gene which is critical for X-inactivation.
- **Tsix** is also on the Xic region and believed to play an important role in the X-inactivation.
    
![xic](./img/06_Sex_Determination/xic.png)

- The Xist gene is transcribed into RNA which attaches on the same X chromosome. Many RNA like this are build and together they allow the X chromosome to be condensed.
- Tsix is the reverse (complementary) gene that allow for the second X chromosome not to be condensed.

![xist rna](./img/06_Sex_Determination/xist_rna.png)

## Drosophila

- Although Drosophila have a Y chromosome, it does not play a role in sex determination. Instead the X chromosome along with some autosomes are responsible for sex determination.
- Bridge's work showed that abnormal genotype XXY didn't cause maleness and the flies were normal females. The X0 flies were steril males.
- The ratio number of X chromosomes vs number of autosomes: the higher the ratio the more female the Drosophila is, the lower the ratio the more male the Drosophila is.
  - The females with a 1.5 ratio cannot survive.
  - Females with 2X:2A are normal females. Triploid females 3X:3A and the normal females are fertile.
  - Normal males: XY:2A, the other male/metamale are infertile.

![drosophila](./img/06_Sex_Determination/drosophila.png)

- Females Drosophila have 2 X chromosomes thus 2 copies of each X-linked genes while males have only one.
- Drosophila do not inactivate X chromosomes. In order to compensate, **males transcribe twice as much as females the X-linked genes**.
  - This is done by a gene that codes for a protein that increases gene expression.
  - This is called **DCC - dosage compensation complex**
  - The DCC is activated only in males, in females it is deactivated.

## Other sex determination process

- TSD: temperature dependent sex determination (turtles)
