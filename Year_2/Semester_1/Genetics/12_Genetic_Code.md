# The Genetic Code, Transcription

- **Central dogma**: from the data in DNA, RNA can be created. From the data in RNA, polypeptides can be created.

![central dogma](./img/12_Genetic_Code/central_dogma.png)
![central dogma](./img/12_Genetic_Code/central_dogma_2.png)

## The Genetic Code

1. Linear. The ribonucleotide bases of the mRNA is derived from the complementary nucleotide bases in DNA.
2. Codon. One codon is 3 ribonucleotides and codes for one amino acid.
3. Unambiguous. One codon codes for only one amino acid.
4. Degenerate. A given amino acid can be coded by several codon (i.e multiple codons codes for the same amino acid).
5. Start codon and Stop codon.
6. Commaless. No punctuation or breaks in the mRNA sequence.
7. Nonoverlapping. Each ribonucleotide is part of only one codon.
8. Colinear.
9. Universal. The coding dictionary is used by almost all viruses, prokaryotes, archaea and eukaryotes.

## Studies

- Codons of 2 letters give not enough option to code for 20 amino acids ($`4^2=16`$ but $`4^3=64`$).
- **Reading frame** contiguous sequence of nucleotides encoding a polypeptide.
- Inserting one nucleotide in a DNA sequence alters radically the polypeptide. This is called **frameshift mutations**.
- Inserting 3 nucleotides in a DNA sequence will alter a lot less the polypeptide, at some point the reading frame is reestablished.

### Nirenberg and Matthaei - 1961

- Synthesizing polypeptides:
  - In 1961, before mRNA was isolated, using an enzyme that usually degrades RNA with a lot of NDPs allowed to use it to synthesize RNA.
  - Some amino acids in vitro have to be radioactive in order to follow the process.
  - Because the process is done in vitro and not in vivo and doesn't have a DNA template, the NDPs are added at random (the probability of a specific nucleotide to be added to the chain depends on the concentration of this type of nucleotides relative to the other 3).
  
  ![synthesizing polypeptide in vitro](./img/12_Genetic_Code/invitro_polypeptide.png)
  
- **RNA homopolymers** (RNA containing only one type of ribonucleotide) were used to follow which amino acids were added in proteins.
  - For example an RNA homopolymer UUUUUU... will add only phenylalanine (they used $`^{14}C`$-phenylalanine). Assuming the codons are triplets, this allowed to understand that UUU codes for phenylalanine.
  - The same way the amino acids encoded by AAA and CCC were discovered. GGG was a bit more challenging because the strand folded on itself.
- The use of **RNA heteropolymers** with known proportions of each ribonucleotides allowed to discover more codons and their associated amino acids.
  - Knowing the proportions is important in order to know the frequency of the possible codons (see below table).
  - Comparing the known frequencies with the frequencies of the amino acids in the polypeptide produced allow to "match" the codons and their amino acids.
  - Most codons were still not exactly known though (see explaination below).
  
  > Example: in a certain polypeptide with the frequencies as in the table below:  
  > Proline has a frequency of 69.  
  > None of the possible codons has a frequency close to this but:  
  >  - CCC has a frequency of 57.9% and is already known to code for proline.  
  >  - The ratio 1C:2A can code for 3 codons (AAC, ACA, CAA) each of them has a frequency of 11.6% (34.8% together).  
  >  - 57.9% + 11.6% $`\approx`$ 69%. Thus is can be assume that both CCC and one of the codons 1C:2A codes for proline.  
  
  ![heteropolymers](./img/12_Genetic_Code/heteropolymers.png)
  
### Nirenberg and Leder - 1964

- **Triplet-binding assay**: in vitro ribosomes containing RNA with only 3 ribonucleotides will attract tRNA with the matching **anticodon**.
- Ribosomes with RNA triplet and radioactive tRNA were placed on a filter that can retain only ribosome (tRNA are too small). 
- If radioactivity remains that means that tRNA has bonded to the mRNA. If not then the amino acid tested doesn't match the codon.
- Most triplets could be matched with their amino acid.
- Conclusions: the genetic code is **degenerate** (one amino acid can be coded by multiple codons) and it is **unambiguous** (a specific codon codes only for one amino acid).

### Har Gobind Khorana - early 60s

- Also in the early 60s, Har Gobind Khorana found a way to synthesize long RNA consisting of short RNA sequences put together.
  - Dinucleotide: two repeating nucleotides $`\rightarrow`$ two repeating triplets (see picture below).
  - Trinucleotide: three repeating nucleotides $`\rightarrow`$ three repeating triplets (see picture below).
  - Tetranucleotide: four repeating nucleotides $`\rightarrow`$ four repeating triplets (see picture below).
- The mRNA can be initiated at different location in order to repeat the wanted triplet.
- Combining the experiments (experiment with dinucleotide, experiment with trinucleotide, experiment with tetranucleotide) it is possible to deduce which codon codes for which amino acid.
- Stop codons were also discovered this way.

![khorana](./img/12_Genetic_Code/khorana.png)


## The Coding Dictionary

![dictionary](./img/12_Genetic_Code/dictionary.png)

- 64 codons, 61 coding for amino acids and 3 coding for termination.
- Only tryptophan and methionine are encoded by a single codon (all others by several codons).
- The codons that codes for the same amino acids are grouped: starting by the same 2 letters (most of the time).
- The **wobble hypothesis**: the 2 first ribonucleotides of triplets are more critical than the third to attract the correct tRNA.
  - The codon-anticodon hydrogen bonding is less constrained thus can be flexible to which base it bonds to $`\rightarrow`$ a single tRNA can pair with several codons.
  - The below table shows the possible pairing between the anticodon first bases and codon third bases (remember antiparallel strand!). In tRNA there's a modified bases called I that can pair with A,U or C.
  - Because of the wobble only 30 tRNA (more or less) are required for pairing with all the amino acids.
  
![base pairing](./img/12_Genetic_Code/base_pairing.png)

- Amino acids that share **similar chemical properties** will often also have the same **middle bases** in the codon.
  - _For example: U or C is often present in the middle of codons that code for hydrophobic amino acids._
- The potential of the **ordered genetic code** is that it buffers potential mutations.

### Initiation, Termination and Suppression

- In prokaryotes, the **initial amino acid** is a **N-formylmethionine (fmet)** which is a modified form of methionine.
- The only codon for methionine is **AUG**. 
- AUG codes for the two forms of methionine, the fmet that appears only as a starter of the polypeptide chain and regular methionine which appears within the chain.
- After the protein is synthesized, either the formyl group is removed form the initial methionine or the fmet is removed altogether.
- **In eukaryotes, the initial amino acid is regular methionine and as in bacteria it can be removed upon completion of the protein synthesis**.
- 3 codons (UAG, UAA, UGA) code for termination.
- **Nonsense mutation**: mutation that happen in the middle of the gene resulting in a termination codon, the polypeptide chain is thus partially done.

### Exception to the Universal Dictionary

- There are some exceptions to the universal code.
- **mtDNA** (DNA from mitochondria) from yeast and human as well as some bacteria show differences:
  - The termination code isn't the same.
  - Other amino acids differ in their codons.
- A lot of times the difference is just the third codon: AUA specifies isoleucine in the cytoplasm but methionine in the mitochondrion. Methionine in the cytoplasm is specified by AUG.
- The suggestion is an evolutionary trend to reduce the number of tRNA required (in mitochondria only 22 tRNA required).
  
![exceptions](./img/12_Genetic_Code/exceptions.png)


### Overlapping Genes

- The genetic code is nonverlapping but genes can be.
- mRNA can have **several initiation points for translation**.
- Genes can be referred as ORF (Open Reading Frame).
- This has been shown in the phage (virus) $`\phi`$X174 which contain more protein that its circular chromose should have been able to encode.

![orf](./img/12_Genetic_Code/orf.png)

- The problem with this is that a single mutation can affect several genes.
- Overlapping has been found in eukaryotes including mammals (mice, humans).
- It has been found than a whole gene was embeded within another gene.

## Transcription - DNA to RNA

- Transcription is the process of copying genetic information by creating mRNA from DNA.
- Evidence of the RNA as an intermediate between DNA and proteins:
  - DNA is found in the nucleus while proteins in the cytoplasm.
  - RNA and DNA are similar.
  - RNA migrates from the nucleus to the cytoplasm.
  - RNA quantity proportional to the quantity of proteins.
- An experiment in 1961, Brenner/Jacob/Meselson, showed that ribosomes are not specific (they build any kind of proteins) by infecting E. coli with phages.
- In 1959, RNA polymerase was discovered.
- **RNA polymerase** is similar to DNA polymerase with the exceptions that it uses ribose and not deoxiribose and that it doesn't require a primer, just a promoter which is a DNA sequence indicating the start of the gene.
- RNA can fold on itself, creating loops (**secondary structure**). Those structure are structural and used in tRNA and ribosomes.
- **Template strand** is a **non coding strand** also called **antisense strand**.
- **Non template strand** is the strand that has the same code as the RNA created from the template strand. It is also called **coding strand** or **send strand**.
- The transcription will be done from one of the DNA strands depending on where the promoter is placed. The reading is always from 3' to 5' and the writing from 5' to 3'.
- **Cis** elements are elements on the DNA (like promoters).
- **Trans** elements are elements that come attach to the DNA (like polymerases).

![gene strand](./img/12_Genetic_Code/gene_strand.png)

### Prokaryotes

- Synthesis of a single stranded RNA molecule.
- It is complementary to a region on a specific DNA strands which is called the **template strand**.
- RNA polymerase in E. coli consists of several subunits: $`\alpha, \; \beta, \; \beta', \; \omega, \; \sigma`$.
- **Core enzyme**: $`\alpha_2\beta\beta'\omega`$ (2 alphas).
- **Holoenzyme** is the active form of RNA polymerase and contains the subunits $`\alpha_2\beta\beta'\sigma`$.
- $`\beta`$ and $`\beta'`$ provide the catalytic mechanism and active site for transcription.
- $`\sigma`$ factor allow the binding to a promoter. It **dissociates** after the synthesis of 8-9 nucleotides.

![rna polymerase](./img/12_Genetic_Code/rna_polymerase.png)

- **Template binding** happens when RNA polymerase $`\sigma`$ subunit recognizes the **promoter** on the DNA template strand.
- The promoters are located before the gene itself (5' region). The **transcription start site** is the point where transcription actually starts.
- The core enzyme binds strongly to the DNA while the holoenzyme binds weakly.
- Ribosomes start translating the RNA while it still transcribes the DNA (don't forget this is prokaryotes).
- **Consensus sequences** are DNA sequences that are similar in different genes of the same organism or in one or more genes of related organisms.
  - Two consensus sequences in bacterial promoters:
    - **TATAAT**: 10 nucleotides upstream (Pribnow box).
    - **TTGACA**: 35 nucleotides upstream.
    - In some promoters those sequences are more or less different (mutations). This means that the $`\sigma`$ subunit will less likely attach to them thus those genes will be less expressed. This indicates that they are not too important.
- **Operon**: several genes can be found one after another with one promoter at the start of the chain. Ribosomes can translate them one after another (start and stop codon indicates the start and end of each one of the genes).

![promoter](./img/12_Genetic_Code/promoter.png)

- A **termination sequence** indicates the end of the gene (around 40 base pairs).
  - The termination sequence is also **transcribed into RNA**.
  - The transcribed termination sequence folds on itself forming a **hairpin secondary structure** (held by hydrogen bonds).
- Some terminations are **rho independent** and some are **rho dependent**.
- **Rho terminator** is a protein that attaches to the RNA and help cut the new RNA when it is done coding.
  - The rho terminator follows the RNA polymerase. 
  - When the termination sequence is transcribed and folded it slows down the RNA polymerase allowing for the rho terminator to catch up and causes the RNA polymerase to dissociate thus cutting free the mRNA.
  
### Eukaryotes

- The transcription into mRNA happens in the nucleus.
- 3 different forms of RNA polymerase.
  - RNA polymerase I makes rRNA.
  - RNA polymerase II can make mRNA (and snRNA).
  - RNA polymerase II makes tRNA (and 5S rRNA).
- No $`\sigma`$ but other similar complexes.
- In general the RNA polymerases of eukaryotes are more complex than those of the prokaryotes.
- Ribosomes cannot attach right away to the mRNA since it has to get out of the nucleus.
- **Chromatin remodeling**: chromatin needs to be uncoiled (it is coiled around histones) and other changes so the RNA polymerase can attach.
- First pre-mRNA is created that has to be spliced and capped to become mRNA.
- Like with prokaryotes some parts of the promoters are similar in every genes. 
  - One of core promoters is the **TATA box** (looks like TATAAAA...) located around 25/30 nucleotides upstream.

![bacteria vs eukaryotes](./img/12_Genetic_Code/bacteria_eukaryote.png)

- In eukaryotes the **core promoter** is similar to the promoter in prokaryotes.
- **Enhancers** increase the level of transcription (cis element).
- **Silencers** decrease the level of transcription (cis element).
- They can be anywhere from just before the promoter to just after the gene.
- They respond to the cell's requirement for gene product, time in development or place in the organism.
- RNAP II (RNA polymerase II) cannot bind directly to the core promoter and needs other proteins called **transcription factors** in order to bind.
- **Transcription Factors** are proteins required for the transcription. Proteins starting with TFII are transcription factors required for RNAP II.
  - **GTF - General transcription factors** are required for all RNAP II (for binding to the promoter).
  - **Transcriptional activators and repressors** influence the efficiency or rate of the RNAP II transcription initiation.
- In humans, GTFs called TFIIA, TFIIB and so on, help with binding.
  - TFIID binds to the TATA box via TBP (TATA-binding protein). Following it, the rest of the GTFs can bind as well.
- Activators and repressors bind to enhancer and silencer elements. 
- During **initiation and termination** the complex formed by the TFs is **unstable**. During **elongation** it is **stable**.
- Unlike prokaryotes there are no rho proteins, just the folding of the RNA that ends the transcription.

#### From pre-mRNA to mRNA (eukaryotes)

- 3 stages of the process that pre-mRNA undergo to become mRNA:
  - **Capping**: addition of the 5' cap.
  - **Polyadenylation**: poly A tail (from AAUAAA).
  - **Splicing**: parts not needed cut out.

##### Capping

- At the 5' end of the pre-mRNA is added a **7-methylguanine (7-mG) cap** (G linked in reverse).
  - The bond is reversed: 5'-5'.
  - It is added before transcription is finished.
  - It protects the molecule from nuclease attack.
  
  ![capping](./img/12_Genetic_Code/capping.png)
  

##### Polyadenylation

- At the 3' end, a small part is cut from the pre-mRNA (the folded part) and are added around 20-50 bases (A). This called the **poly-A sequence** or poly-A tail.
- The sequence **AAUAAA** indicates where to cut and then add the poly-A tail.

##### Splicing

- **Intervening sequences** are segments that are removed from the pre-mRNA. They are also called **introns** ("int" for intervening).
- **Exons** ("ex" for expressed) are the segments that are retained from pre-mRNA to mRNA.
- Splicing removes the introns and joins the exons together.
- **Heteroduplexes**: hybridization of mRNA:DNA complex (after the introns were removed). The matching parts bond causing the DNA parts that match the introns to form loops.

![dna rna](./img/12_Genetic_Code/dna_rna.png)

- Splicing mechanism: the introns are removed with small RNA molecules attaching to the intron and slowly create a loop that joins the two extremities of the exons (see picture).
- **Spliceosome**: complex of proteins and snRNA (small RNA molecules) that performs the splicing (removing the introns).

![spliceosomes](./img/12_Genetic_Code/spliceosomes.png)

- Primitive splicing mechanisms: **ribozymes**: RNA that can cut itself.

## Translation - RNA to Protein

- Protein synthesis from mRNA via ribosomes and tRNA.
- tRNA: 
  - Contain **anticodons** complementary to codons.
  - Convalently bonded to the codon's amino acid.
- mRNA and tRNA bond with an hydrogen bond: this allow to form the peptide bond between the polypeptide chain and the next amino acid.

### Ribosomes

- 40nm in bacteria.
- 2 subunits: one large and one small.
- The subunits are made of rRNA and ribosomal proteins.
- The 2 subunits together are called a **monosome**.
- Ribosomes are dissociated into 2 subunits when they are not in the process of translating.

![ribosome](./img/12_Genetic_Code/ribosome.png)
 
 > **Svedberg coefficient**: rate of migration. It reflects a molecule density, mass and shape.  
 > Abbreviated **S**.  
 
- In prokaryotes, the monosome is around 70S. In eukaryotes the monosome is around 80S.
- In prokaryotes 3 continous genes that code for rRNA. 
- In E. coli there are 8 genes that code for rRNA sequences.
- In eukaryotes, 18S, 5.8S and 28S are continous but 5S is not.
- They are several genes coding for those parts (120 in Drosophila).

### tRNA

- 75-90 nucleotides.
- Nearly identical in bacteria and prokaryotes.
- **Posttranscriptional modification**: modified nucleotides that are only found in tRNA and rRNA.
  - The modification happens after the transcription of the tRNA.
  - Those modifications are believed to enhance the hydrogen bonding during translation.
  
  ![trna modifications](./img/12_Genetic_Code/trna_modifications.png)
  
- The secondary structure of tRNA is called **cloverleaf**. 
- The loops contains modified bases that don't form base pairs.

  > GCU, GCC and GCA code for alanine.  
  > tRNA$`^{Ala}`$ has a loop with the anticodon **CGI** where I can form hydrogen bonds with U, C or A (wobble hypothesis).  
  
- The tRNA 3' end binds to the amino acid and has the same sequence: pCpCpA (the amino acid is convalently bonded to the the [A]denosine).
- At the tRNA 5' end there's always a pG.

![trna](./img/12_Genetic_Code/trna.png)

- **Charging (טעינה)** of tRNA refers to the process of attaching the amino acid to it.
  - It is done by an enzyme called **aminoacyl tRNA synthetase**.
  - It uses ATP on the amino acid which creates an **aminocyladenylic acid**.
  - Then a covalent bond is formed between the phosphate of the 5' end of the ATP and the carboxyl end of amino acid.
  - The amino acid is then tranferred to the correct tRNA 3' end (adenine).
- Since there are 20 different amino acids, there must be at least 20 different tRNA molecules as well as enzymes.
- There are 61 triplets that encode amino acids but due to the "wobble" there are less tRNA.

![charging trna](./img/12_Genetic_Code/trna_charging.png)

- **In tRNA there are 4 bonding sites**:

```math
\boxed{
\begin{aligned}
& \text{ - To the codon. }\\
& \text{ - To the amino acid. }\\
& \text{ - To the ribosome. }\\
& \text{ - To the Aminoacyl tRNA Synthetase enzyme. }\\
\end{aligned}
}
```

### Translation

- 3 general steps to the translation:
  - Initiation
  - Elongation
  - Termination

#### Initiation

- In the intiation are involved: small subunit, an mRNA molecule, an initiator tRNA, GTP, $`Mg^{2+}`$ and 3 initiation factors (IFs).
- In prokaryotes the initiation codon calls for the modified amino acid **f-met**.
- The 3 initiation factors IFs bind to the small subunit and then the mRNA bind to this complex.
- IF1 blocks the A site from binding to a tRNA.
- IF3 blocks the small subunit to bind to the large subunit.
- IF2 (it is a GTPase which is bound to GTP) stabilizes the mRNA and tRNA and thus sets the reading frame.
- IF3 is released to allow the large subunit to attach.
- The GTP bound IF2 is hydrolized provding the required energy for the large subunit to attach.
- IF1 and IF2 are released.

![initiation](./img/12_Genetic_Code/initiation.png)

#### Elongation

- In the A site, the next codon is ready to bind with the tRNA.
- The charged tRNAs are tranported into the complex by elongation factors EF-Tu (it is a GTPase thus bound to a GTP, the hydrolisys of which provide the energy for the process).
- The terminal amino acid in the P site is linked to the amino acid in the A site by a peptide bond catalyzed by **peptidyl transferase**. This is called **elongation**.
- The tRNA in the P site moves on to the E site and the one in the A site moves on to the P site (with the polypeptide chain attached) and the next 3 nucleotides are placed in the A site. This is called **translocation**.
- The large subunit contain a tunnel through which the polypeptide chain emerges (when it's long enough).

![elongation](./img/12_Genetic_Code/elongation.png)

#### Termination

- 3 codons code for termination (stop codons): UAG, UAA and UGA.
- Those codons do not specify an amino acid thus no tRNA comes into the A site to attach.
- Several stop codons can be found in one mRNA.
- The stop codon moves on to the P site leaving the A site empty.
- It signals the action of a GTP-dependent release factor which will release the polypeptide from the ribosome.
- In prokaryotes there are 2 release factors: RF1 and RF2.
- The ribosome subunits then separate.

![termination](./img/12_Genetic_Code/termination.png)

#### Polyribosomes

- Also called polysomes.
- Multiple ribosomes can translate one mRNA.

![polyribosomes](./img/12_Genetic_Code/polyribosomes.png)

#### Translation in Prokaryotes

- **Shine-Dalgarno Sequence**: AGGAGG. 
- In e. coli: AGGAGGU.
- This sequence precedes the start codon AUG of mRNA.

#### Translation in Eukaryotes

- Many mRNA contain a purine (A or G) three bases upstream from the start codon AUG which is itself followed by a G (A/GNNAUGG). This is called the **Kozak sequence**.
- It is believed to increase the efficiency of translation by interacting with  initiator tRNA​.

## Proteins

- A protein is a folded polypeptide chain that can function.
- Amino acid:
  - Carboxyl group
  - Amino group
  - Radical group [R]
  - Central carbon atom [C]
  
  ![amino acid](./img/12_Genetic_Code/amino_acid.png)
  
- The R group can be divided into 4 main classes:
  - Nonpolar (hydrophobic)
  - Polar (hydrophilic)
  - Positively charged
  - Negatively charged
- Amino acid are bonded by dehydration (condensation) between the amino group of the first and the carboxyl group of the second. This is called a **peptide bond**.
- The end with the amino group is called the **N-terminus**, the one with the carboxyl group is called the **C-terminus**.

![peptide bond](./img/12_Genetic_Code/peptide_bond.png)

- 4 levels of protein structure:
  - Primary structure: polypeptide chain (sequence of amino acids).
  - Secondary structure: $`\alpha`$-helix and $`\beta`$-sheets (see [here](../Biochemistry/02_Proteins.md#secondary-structure)).
  - Tertiary structure: 3D formation:
    - Covalent disulfide bonds between close cysteine residues forming cystine.
    - Polar (hydrophilic) R groups are set on the outside while the hydrophobic (non polar) are found in the inside of the structure.
  - Quaternary structure: multiple polypeptide chains.

### Alternative splicing
- _To know for the exam!_
- Sex determination in Drosophila:
  - 3 genes: 
    - sxl (sex-lethal)
    - tra (transformer)
    - dsx (double-sex)
- After the transcription $`\rightarrow`$ splicing.
- In sxl gene, in females there's a factor (sxl protein) that causes one exon to not be considered as an exon thus exon2 will join exon4. In males exon2,3,4 are joined.
- Stop codon is found on exon3 thus in males stops the formation of the protein but not in females which don't use this exon.
- In tra pre-mRNA similar scenario (see picture) which causes tra protein to be synthesized only in females.
- In the end two different proteins are synthesized in males and females (dsx protein).
- In short alternative splicing is what allow the sex determination in drosophila.

![alter splicing](./img/12_Genetic_Code/alter_splicing.png)
