# Quantitative Genetics and Multifactorial Traits

- **Quantitative inheritance**: continuous variations across a range of phenotypes. Also called **polygenic** traits.
- Genotype generated at fertilization establishes a quantitative range where the individual can fall but the final phenotype is also influenced by the environment.
- **Multifactorial** traits are traits whose phenotypes are a results of both genes and environment influence.
- Cumulative effect of alleles at multiple loci produce a range of phenotypes seen in quantitative traits.
  - Red grain wheat mixed with white grain wheat (2 alleles).
  - In $`F_2`$ generation there are 1/16 white and 15/16 plants with different shades of red in a 1:4:6:4:1 ratio.

  ![wheat grain](./img/15_Quantitative_Genetics/wheat.png)
  
- The different genes influence the phenotype in an **additive way**.
  - Each allele can either be **additive**, contributing an amount to the phenotype (like the red in the wheat grain), or **nonadditive** which doesn't contribute quantitatively to the phenotype.
- Although not always the case, we assume that each allele contributes approximately the same amount.
  - The more genes contribute to a phenotype the possibilities there are (see table below).

![ratios](./img/15_Quantitative_Genetics/ratios.png)

- Assuming that $`F_2`$ generation isn't affected by environmental factors and that alleles contribute equally to a phenotype, we can find the number of genes participating in the phenotype by looking at the ratio for the $`P_1`$ phenotypes in the $`F_2`$ generation (with wheat $`P_1`$ phenotypes were red and white grain, which, in the $`F_2`$ generation, had a 1/16 ratio)
  - Taking the wheat example, the red and white phenotypes having a 1/16 ratio we can calculate the number of genes participating in the phenotype as follow:
  
```math
\frac{1}{4^n} = \frac{1}{16} \Rightarrow n = 2
```
  - Calculating the number of different phenotypes from the number of genes involved is calculated as follow:
  
  ```math
  \begin{aligned}
  & 2n + 1 = \text{phenotypic categories } \\
  & \text{Example with 2 genes:} \\
  & 2 \times 2 + 1 = 5 \\
  & \text{Each phenotype is the result of either 4,3,2,1 or 0 additive alleles} \\
  & \text{Example with 3 genes:} \\
  & 2 \times 3 + 1 = 7 \\
  & \text{Each phenotype is the result of either 6,5,4,3,2,1 or 0 additive alleles} \\
  \end{aligned}
  ```
  
 ![calculation](./img/15_Quantitative_Genetics/calculation.png) 
 
- Multifactorial phenotypic traits are influenced (the result of):
  - Genotypic information
  - Environmental conditions
  - Interactions between the environment and the genotype

> Example:  
> Someone with the genes to be tall but with a nutrition that isn't enough to use this potential will be shorter.  
  
- Some traits (usually diseases) are set by a threshold: 

![diabetes](./img/15_Quantitative_Genetics/diabetes.png)
 
- Studying identical twins (monozygote - identical genotype) and dizygote twins reduces the influence of environment and allows to study the influence of the genome of phenotypic traits (although there are still environmental influences).
  - Blood type is known to be entirely based on the genome thus MZ twins have 100% same blood type while DZ twins are closer to 50% (66%).
- **Quantitative trait locus - QTL** is a region in a chromosome identified as containing one or more genes contributing to a quantitative trait.
  - Changes in the genome at this locus will greatly affect the specific trait since there are several alleles (affecting a lot more than one gene affecting this trait at another locus).
- **QTL mapping** involves looking for associations between DNA markers and phenotype.
  - Picture (a): before looking a the genes, taking the individuals with most of the wanted traits and crossing between them was the way. After a few generations the individuals were mostly homozygous for the wanted traits.
  - Picture (b): in order to understand which alleles influences certain traits, crossing between individuals and looking at the phenotype as well as genotype.
  - Picture (c): looking at where the alleles influencing are located in the genome to find the QTLs.
  
  ![qtl mapping](./img/15_Quantitative_Genetics/qtl.png)

- QTL mapping is used a lot in agriculture (corn, rice, wheat, tomatoes, calttle, pigs, sheeps and chicken).
- A lot of QTL have been mapped in the tomato.
