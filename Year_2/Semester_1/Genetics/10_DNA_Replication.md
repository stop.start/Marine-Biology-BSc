# DNA Replication and Recombination

## Replication mode

- Replication: duplication of DNA before cell division.
- 3 theoritecal modes of replication:
  - **Semiconservative**: the accepted one. The name comes from the fact that during replication and each strand serves as a template for a new strand thus making pairs of "old" and "new" strands.
  - A **conservative replication** would have the two new strands created come together to form a new helix. This way the original helix is "conserved".
  - **Dispersive replication** is a random dispersion into two new double helices (each strand has both old and new DNA).
- **Sedimentation equilibrium centrifugation**: samples are separated by density gradient by centrifugation. 
  - Cesium chloride [CsCl] is a salt used to build the gradient.
- **The Meselson-Stahl experiment**:
  - 1958
  - Provides evidence of semiconservative replication in bacterial cell (E. coli).
  - They used $`^{15}N`$ isotope as a source for nitrogen and after a few generations got cells that were all with that isotope.
  - Those cells were transfered to an environment with only $`^{14}N`$ isotope as a nitrogen source.
  - The E. coli replicated over serveral generations with cell samples taken after each replication cycle. DNA was isolated and subjected to sedimentation equilibrium centrifugation.
  - After one generation, the sedimentation equilibrium centrifugation resulted in one band. This means that all the DNA had the same density, i.e both isotopes. This rules out conservative replication which would have resulted in two bands: one for the old DNA with the $`^{15}N`$ isotope and one for the new DNA with the $`^{14}N`$ isotope.
  - After each generation, the band corresponding with having the $`^{15}N`$ isotope got thinner and thinner which still matches a semiconservative replication mode (more and more DNA with only $`^{14}N`$).
  - Dispersive replication was ruled out form the fact that after the first generation, some DNA was denatured and the two strands were either $`^{15}N`$ profile or $`^{14}N`$ profile but not a mix. More over, after the first generation all DNA would have the same density.
  
  ![meselson-stahl](./img/10_DNA_Replication/meselson_stahl.png)
  
- Semiconservative replication in eukaryotes has also be observed in 1957 (one year before the Meselson-Stahl experiment).
  - DNA was labeled with radioactive $`^3H`$-thymidine.
  - Autoradiography is a method that allows to pinpoint the location of a radioisotope in a cell.
  - Cells were grown in with the radioisotope then placed in a unlabeled environment.
  - They observed results matching a semiconservative replication.
  - The only exception was because of crossing over.

## Origin of Replication and Forks

![dna replication](./img/10_DNA_Replication/dna_replication.png)

- **Replication fork**: point on the chromosome where the replication is occuring. The fork moves along the DNA as replication happens.
- **Bidirectional replication** means two forks moving away from each other (from the origin).
- A **replicon** is a unit of length of DNA that replicates from the origin.
- Bacteria have a single circular DNA.
  - They have only one ORI called **oriC**.
  - Because of the single point of origin of replication bacteria have **only one replicon** consisting of the whole genome (4.6 million base pairs).
  - **Ter** is the region where replication forks merge when the replication of the chromosome is completed.
  
![bacteria replication](./img/10_DNA_Replication/bacteria_replication.png)

### Ori

- The origin of replication - Ori - are in specific locations and there can be more than one.
- Certain protein complexes know how to attached to those Ori sites.

![ori](./img/10_DNA_Replication/ori.png)

- **DnaA box** is how OriC is identified. Those are composed of 9-mers and 13-mers repetitions (9 and 13 base pairs) and those are mainly composed of AT base pairs (which are less stable because less hydrogen bonds).
- **DnaA** are the proteins which attach to the DnaA box in a region with 9-mers and this complex associate to 13-mers. They form a structure that causes the melting of the DNA, meaning the 2 strands at this point separate.

![dna a](./img/10_DNA_Replication/dnaa.png)

- **Helicase** is an enzyme made up of DnaB, that attaches onto the Ori site just after it opened. Its role is to open more the DNA (separate the two strands) and requires ATP to do it.
- **SSB** are single-strand binding proteins whose role is to protect the newly single strands of DNA segments.
  - Prevent the DNA strands to reattach.
  - Protect from enzymes.
  - Attach replicating proteins (DNA polymerase).
  - They move on along with the fork.
- **Primase (DnaG)** synthesizes the **RNA primer**.
  - Type of **RNA polymerase**.
  - Brought by the helicase enzyme.
  - Brings 3'-OH that DNA Pol III requires.

![ori proteins](./img/10_DNA_Replication/ori_proteins.png)

![ori2](./img/10_DNA_Replication/ori2.png)

- With circular DNA, uncoiling the DNA can be problematic since it causes all the DNA to turn on itself. Special enzymes (**topoisomerase**) will release the pressure from the uncoiling ahead of the fork to prevent issues. It does so by cutting, twists the strands to remove tension and reattach.

## Bacteria Polymerases

- In bacteria, DNA synthesis involved **5 Polymerases** (DNA Pol I-V) and other enzymes.

### DNA Polymerase I

- The first to be discovered.
- Consists of 928 nucleotides.
- **Directs the synthesis of DNA**.
- In vitro requirements: all dNTPs need to be present (deoxiribonucleoside triphosphates) and a template DNA.
- If one of the dNTPs isn't present, not much synthesis happens.
- If nucleotides (or nucleosides) disphosphate are present the replication doesn't occur.
- Without a DNA template synthesis happens but reduced.
- Attaching a new nucleotide:
  - The dNTP contains **3 phosphate groups attached to the 5'-carbon** of deoxyribose.
  - 2 phosphate are removed while attaching the third is attached **covalently to the 3'-OH group of the deoxyribose** of the chain.
  - The chain elongation happens **from 5' to 3' direction**.

![3 to 5](./img/10_DNA_Replication/3_to_5.png)


### DNA Polymerase II-V

- A mutant strain of E. coli with deficience in polymerase I activity still duplicated its DNA and reproduced.
  - The cells were deficient in repairing DNA. Where nonmutant bacteria can repair damage from UV light this strain couldn't.
- DNA polymerase I,II and III: 
  - Are not able to initiate the synthesis of DNA. 
  - Can go backward (from 3' to 5') to fix an error by removing nucleotides.
  - Polymerase I can also remove nucleotides from 5' to 3'. 
  - Polymerase I is found in greater amount and is more stable.
  - Polymerase III is responsible for the synthesis in vivo with proofreading and fixing abilities.
  - Polymerase I is responsible for the removal of **primers** and the synthesis of nucleotides in their place.
- DNA Pol II, IV, V are involved in repairing DNA that has been damaged by external forces like UV light.

![dna pol](./img/10_DNA_Replication/dna_pol.png)


### DNA Polymerase III

- Its active form is called **holoenzyme**.
- Made of 10 polypeptide subunits.
 - Core enzyme formed by 3 subunits.
- Very fast.

![dna pol 3](./img/10_DNA_Replication/dna_pol_3.png)

## Synthesis

- The two strands are antiparallel:
  - **Leading strand** - 3'-5' which allows to write new DNA in 5' to 3' direction.
  - **Lagging strand** - 5'-3' which causes to write new DNA in 3' to 5' direction.
- On the lagging strand the synthesis is discontinuous, the fragments are called **Okazaki fragments**.
- Each Okazaki fragment starts with an RNA primer.
  - **DNA Pol I** is responsible for removing the primers.
  - Something with RNase H.
  - **DNA ligase** is responsible for joining the fragments together.
- Both DNA Pol III synthesize in the the same physical direction thus the lagging strand has to be flipped (see picture and video).
  - Between each okazaki fragment the loop is released, a new primer is added and a new loop is made.

[video](https://www.youtube.com/watch?v=D91QfkZEN_M)

![okazaki](./img/10_DNA_Replication/okazaki.png)

## Eukaryotes

| Similitudes with prokaryotes                                        | Differences with prokaryotes       |
|---------------------------------------------------------------------|------------------------------------|
| DNA unwound at Ori                                                  | More than one Ori                  |
| Replication forks                                                   | Linear and bigger DNA              |
| Bidirectional synthesis with leading and lagging strand             | Nucleosomes (presence of histones) |
| DNA polymerase has the same requirements: 4 dNTPs, template, primer |                                    |

- In yeast there are between 250-400 origins. In mammals, 25000.
- In yeast (שמרים), origins are called **ARSs** (autonomously replicating sequences). They consist of 120 base pairs (including 11 base pairs that are almost always the same).
- In mammals, origins don't have a specific sequence pattern and might be defined by **chromatine structure**.
- In eukaryotes, origins act as:
  - **Sites of replication initiation**.
  - **Control the timing of DNA replication**.
- The **prereplication complex (pre-RC)** assembles at replication origins and consist of more than 20 proteins. 
  - It is responsible for start the replication process.
  - In early G1 phase, replication origins are recognized by a protein complex called ORC (origin recognision complex) which tags the origin site.
  - Other proteins associate with ORC to form pre-RC.
  - Disassemble after DNA polymerase comes in to start synthesis.
- Humans have 14 different types of DNA polymerase.
  - 3 are involved in the DNA replication.
    - Polα synthesizes RNA primer.
    - **Polymerase switching**: Polα dissociates from the template and is replaced by Polδ and Polε.
    - Polε synthesizes DNA on the **leading** strand.
    - Polδ synthesizes the **lagging** strand.
  - One synthesizes mitochondria DNA and others do repairs.

### Replication

- **Chromatin**: DNA with its proteins attached.
- **Nucleosomes**: 200 base pairs wrapped around 8 histones (proteins).
- Chromatin is made up of nucleosomes.
- In order for polymerases to work on DNA the proteins needs to be removed or modified.
- Nucleosomes reform rapidly after the new DNA has been synthesized.
- During the S phase, histone proteins synthesis is thightly coupled to DNA synthesis (new histones are needed for new DNA).

#### End of Replication


- At the end of replication, 2 problems:
  - End of the linear strands: it can be mistaken for broken DNA and thus the ends can fuse. If they do not fuse they are vulnerable to degradation by nuclease.
  - DNA polymerase cannot synthesize new DNA at the tips of 5' ends (lagging strand).
- **Telomeres** are sequences at the ends of the chromosomes and prevent the above issues.
- **G-rich** end (TTGGGG multiple times) on the 3' strand (5' is C-rich).
- The G-rich strand **lacks a complement**. G-G pairing is possible when there is a lot of G nucleotide present like in a G-rich strand.
  - The G-rich tails loop back on themselves to form a G-quartet. The loops are called **t-loops**.
  - It is believed that those structures protect from nuclease digestion and DNA fusion.
- After RNA primers are removed, when filling the gaps between the Okazaki fragments, DNA polymerase needs the 3'-OH group of one fragment in order to synthesize nucleotides to fill the gap up to the 5' end of the Okazaki fragments.
  - The issue is with the last one, there is no other fragment providing the required 3'-OH group for DNA polymerase to anchor itself and make the synthesis.
- **Telomerase** is an enzyme that contains a short strand of **RNA** which allows the telomerase to attach the 3' end of the template strand and synthesize DNA by **reverse transcription**.
  - Part of the telomerase RNA is the complement of the telomeric DNA.
  - Telomerase attaches to a small part of the template's 3' end with its RNA.
  - It then starts to synthesize DNA (by reverse transcription), meaning it extends the template.
  - When telomerase finishes, **primase** comes in and write a primer on the end of this extended template.
  - DNA polymerase can now synthesize the missing part of the new DNA.
  - Primer is removed leaving a gap. This gap though is located well after the original DNA ends.
  - What's not needed is then cut out.
  - [explaination](https://youtu.be/wf6QiIilGxSg?t=193)

![drawing](./img/10_DNA_Replication/drawing.png)

- In most eukaryotic cells, **telomerase is not active** thus the chromosome shortens.
- After many division the cell lose the ability to divide more.
- Most stem cells and malignant cells keep the telomerase active which contribute to their immortality.
- Telomerase activity has been linked to aging, cancer and more.
- It is possible to extend the telomeres and thus life. The issue being some cancer types that starts with renewed telomerase activity.

## DNA Recombination

- General, or homologous recombination is the exchange of genetic information at equivalent position in two chromosomes.
- There are several models that try to explain the process but we're studying one.
  - Each chromosomes of a homologous pair is cut at the same location by **endonuclease**.
  - The single strands produced by these cuts pair with the ones on the opposite homolog (**strand displacement**).
  - **Ligase** seals the loose ends creating a **heteroduplex DNA molecule** that is held by a cross-bridge structure.
  - The cross-bridge moves down the chromosome by a process called **branch migration** (breaking and reforming of hydrogen bonds).
  - Bending and rotating gives a planar structure called **Holliday structure ($`\chi`$ - chi)**.
  - Endonuclease strikes again and cuts again.
  - Ligase seals.
  - We get recombinant chromosomes.
  
  ![recombinant](./img/10_DNA_Replication/recombinant.png)
  
- Enzymes involved in recombination:
  - **Rec A** in E. coli and **Rec51** in eukaryotic cells are the most important. Associate with the single stranded DNA after endonuclease nicked the DNA.
    - They search for homologous sequence and are the cause of the strand displacement.
    - In E. coli, **RecB,C,D** help with the release of the Holliday structure.
    - In eukaryotes, protein BRCA2 interacts with Rec51. BRCA2 is linked to cancer (breast, ovarian and others).
