# Gene Mutation and DNA Repair

- Any change in any base-pair anywhere in the DNA is considered a mutation.
  - It can be by **substitution, insertion or deletion**.
  - It can alos be a **major alteration in the structure of the chromosome**.
- Mutations in germ cells are heritable. Mutations in somatic cells are not.

## Organization of Mutations

### Molecular Change

- **Point mutation** or **base substitution** is a change of a base pair for another.
- **Missense mutation** occurs when a change in a nucleotide within a codon results in a codon coding for another amino acid.
- **Nonsense mutation** occurs when a change in a nucleotide within a codon results in a **stop codon**.
- **Silent mutation**: the change in the codon still codes for the same amino acid.
- **Transition**: pyrimidine replaces a pyrimidine or a purine replaces a purine.
- **Transvertion**: pyrimidine replaces a purine and vice versa.
- **Frameshift mutation**: addition or deletion of a non multiple of three number of nucleotides resulting in the shift of the codon frames (ABC DEF $`\xrightarrow{\text{adding X at}}`$ ABX CDE F).

### Phenotypic Effects

- _There are more than described but the classification doesn't seem to be for the exam except the ones below the **for the exam** line_.

- **Loss-of-function mutation**: reduces or eliminates the function of the gene product.
- **Null mutation**: complete loss of function.
- Most loss-of-function are **recessive**.
- Dominant mutations can have 2 effects:
  - **Haploinsufficiency**: a single wild-type copy of the gene does not produce enough of the gene product to get the wild-type phenotype.
  - **Dominant gain-of-function mutation**: either enhanced, negative or new functions.
    - This can either be because of a change in an amino acid or in the regulatory region causing changes in activity.
- **Visible mutations** alter visible wild-type phenotypes.
- _The following are for the exam:_
- **Lethal mutation**: mutation affecting a gene product essential for the survival of the organism.
  - Tay-Sachs disease.
  - Hungtington disease.
- **Conditional mutation**: the effects of the mutation depend on the environment the organism is in.
  - **Temperature-sensitive mutations**: permissive temperature is a temperature where the gene product can function normally; restrictive temperature $`\rightarrow`$ gene product doesn't function normally.

### Location of Mutations

- _Also not for the exam_
- Mutations classified based on the cell type or chromosomal location in which they occur.
- Somatic mutations, autosomal mutations, X-linked, Y-linked.

## Spontaneous and Random Mutations

- **Spontaneous mutations**: occurs naturally. They often occur during DNA replication. 
- **Induced mutations** are caused by an external factor (natural or artificial agent).
- **Mutation rate**: the likelyhood that a gene will undergo a mutation.
- The rate of spontanious mutation is low for all organisms and varies between different organisms. Even in the same species it varies from gene to gene.
- **Mutation hot spots**: DNA sequences more susceptible to mutations.
- Different mutation rates in different organisms may reflect difference in efficiency of DNA proofreading and repair systems.

> _Interesting note (not for the exam)_:  
> From a study on humans, the number of new mutations in babys increases with the age of the father (but not the mother).  
> A 20 years old father contributes around 25 new mutations to his child.  
> A 40 years old father contributes around 65 new mutations.  
> Mothers contributes around 15 new mutations no matter their age.  
> This can be explained by the fact that male germ cells go through more cell divisions during a lifetime than female germ cells.  

### Luria-Delbruk fluctuation test

- **Luria-Delbruk fluctuation test**: mutations are spontanious and are not the consequence of selective pressure.
  - This means that they do not occur as an adaptive mechanism and **are random**.
- They used E. coli and T1 phages (in the ppt it's written T2 phages).
- T1 bacteriophage infects and lyses the bacteria.
- Some mutations make the E. coli cells resistant to T1.
- The experiment:
  - Grow several generations of E. coli without T1 in a large flask as well as in several small tubes.
  - The cells in the small tubes were spread on a media with T1 bacteriophage.
  - Same with several portions of the large flask.
  - After incubation, the resistant E. coli cells formed colonies and were counted.
- 2 hypotheses were made:
  - **Adaptive Mutation**: the mutation occurs as a result of the presence of the bacteriophage. 
    - In this case all petri dishes that had approx. the same number of cells should have approx. the same number of resistant cells.
  - **Random Mutation**: random mutation happen also before the introduction of the bacteriophage. The earlier the mutation occured the more cells have the mutations (as a result of reproduction).
    - This means that the number of resistant cells should differ in each small tube culture.
    - This also means that the number of resistant cells should be roughly the same in the cultures taken from the flask.
- The results of the experiment were in favor of the **random mutation hypothesis**.

![fluctuation test](./img/14_Gene_Mutation_DNA_Repair/fluctuation_test.png)


## Mutation Processes

- Mutations happen from replication errors and base modification.
- Mutations that occur spontaneously occur at a higher rate when induced.
- Although DNA polymerase can correct its errors some slips through. Those errors can lead to mutations.
- Insertion or deletion can occur when the DNA template strand make loops or when DNA polymerase slips during replication.
  - A loop in the template strand may cause the DNA polymerase to not go over and replicate the looped part resulting in deletion.
  - When DNA polymerase inserts nucleotides that are not supposed to be there, it creates an unpaired loop on the new strand.
- **Replication slippage** can occur anywhere but sequences that are repeated are hot spots for mutations.
- Another cause of insertion and deletion: **unequal crossing over** (in chromosome crossing over).

### Tautomeric Shifts

- **Tautomers**: multiple forms of the bases that differ from a single proton shift.
  - The important tautomers are called **keto-enol** forms of thymine and guanine and **amino-imino** forms of cytosine and adenine.

![tautomers](./img/14_Gene_Mutation_DNA_Repair/tautomers.png)

- Tautomers allows the bonding of noncomplementary bases. This is called **tautomeric shift**.

![tautomeric pair](./img/14_Gene_Mutation_DNA_Repair/tautomeric_pair.png)

- In a **tautomeric shift**:
  - A tautomer is formed in the template strand and pairs with a noncomplementary base.
  - In the next replication the tautomer can shift back to normal form a replicate normally but the other strand will have mutated.

![tautomeric shift](./img/14_Gene_Mutation_DNA_Repair/tautomeric_shift.png)

### Depurination and Deamination

- Depurination and deamination are two common causes of spontaneous mutations.
- **Depurination** is the loss of one of the nitrogenous bases gunanine or adenine.
  - If not repaired then there's a hole and DNA polymerase can put a random nucleotide instead.
- **Deamination**: cytosise or adenine is converted to a keto group $`\rightarrow`$ cytosine to uracil and adenine to hypoxanthine.
  - This affects the base pairing.
  
  ![deamination](./img/14_Gene_Mutation_DNA_Repair/deamination.png)
  
### Oxidative Damage

- DNA can suffer from by-products of normal cellular processes like:
  - Superoxides $`O_2^-`$
  - Hydroxyl radicals OH
  - Hydrogen peroxide $`H_2O_2`$
- They can also be produced due to exposure to radiations.
- They can produce more than 100 different types of chemical modifications in DNA like modifications to bases, loss of bases, single-stranded breaks.

## Induced Mutations

- **Mutagens** are agents (like UV light, radiations and chemicals) that can cause induced mutations.
- **Base analogs** are compounds that can be used instead of the regular bases.
  - **5-bromouracil** (5-BU) is a **thymine** analog. It is less stable and more prone to tautomeric shift.
  
  ![bromouracil](./img/14_Gene_Mutation_DNA_Repair/bromouracil.png)
  
### Chemical Agents

- **Alkylating agents** are **chemical mutagens**.
  - They donate an **alkyl group** ($`CH_3`$ or $`CH_2CH_3`$) to amino or keto group.
  - They contain sulfur.
  - They are found in human made mustard gases, EMS, EES.
  - EMS alkylates a keto group in guanine and in thymine. This alters the base-pairing affinities (guanine $`\xrightarrow{EMS}`$ 6-ethylgunanine $`\rightarrow`$ pairs with thymine instead of cytosine).
  
  ![alkylating agents](./img/14_Gene_Mutation_DNA_Repair/alkylating_agents.png)
  
- **Intercaling agents** are chemicals of shape and size that allow them to get in between the base pairs of DNA.
  - They cause the base pairs to distort which in turn cause the DNA strands to unwind.
  - This affects transcription, replication, repair. Insertions and deletions happen which leads to frameshifts during replication.
- Intercaling agents are also used as **DNA stains**:
  - **Ethidium bromide** (fluorescent).
  - **Chemotherapeutic agents** $`\rightarrow`$ cancer cells undergo DNA replication a lot thus are sensitive to mutations which can disrupt the replication process enough to stop it.
- **Adduct-forming agents**: substance that covalently binds to DNA. This alters the DNA conformation and interfere with replication and repair.
  - **Acetaldehyde** (found in cigarettes smoke)
  - **HCAs** (created during the cooking of meat, chicken and fish).

### UV Light and Radiations

- **UV radiations** are electromagnetic radiations of shorts wavelength.
- Purines and pyrimidines absorbs UV radiations at a wavelength of 260nm.
- UV radiations can cause **pyrimidine dimers** in the DNA (mainly with thymine). Those are two pyrimidines next to each other that bond (see picture).
  - This causes the DNA to be distorted interfering with the replication.
  
  ![pyrimidine dimer](./img/14_Gene_Mutation_DNA_Repair/pyrimidine_dimer.png)
  
- **Ionizing radiations** ionize the molecules they encounter.
  - Those radiations penetrates deeply into tissues.
  - Stable molecules are transformed into **free radicals** (molecules that have at least one unpaired electron).
  - Free radicals can directly or inderectly affect the genetic material:
    - Altering DNA bases.
    - Breaking phosphodiester bond (bond of nucleic acids).
    - Deletions, translocations in the DNA.
- They are caused by shorter wavelengths than UV light: **X ray, gamma rays, cosmic rays**.

## Mutations in Humans

- **Polygenic disease** is a genetic disease caused by mutations in several genes.
- **Monogenic disease** is a genes disease caused by a mutation in a single gene.
- The blood system ABO: from A to B 5 mutations and from A to O 1 mutation.
- Distrophine (BMD, DMD) disease.
- **β-thalassemia** is an inherited disease that comes from a different possible mutations on a the gene β-globin (HBB gene).
  - Inherited autosomal recessive blood disorder.
  - Most common single-gene disease (worldwide).
  - Causes different symptomes and different degrees of anemia.
  - 250 different mutations on this gene have been discovered to cause the disease (20 of those mutations are common).
  - Most mutations change a single nucleotide or from a small insertion or deletion.
  
  ![hbb gene](./img/14_Gene_Mutation_DNA_Repair/hbb.png)
  
- **Trinucleotide repeat sequences** are short DNA sequences repeated many times.
- Usually there are not a lot of those sequences but people with a lot of human disorder have a lot of those sequences.
  - **Fragile-X syndrome**: 
    - Dominant
    - Repeated sequences of CGG because the first codon of the FMR-1 gene.
    - 6-54 repeats: healthy
    - 55-230 repeats: carrier
    - >230 repeats: sick
    
### Counteracting Mutations

- Some systems have evolved to repair spontaneous as well as induced mutations.

#### Proofreading

- Most common types of mutations occur **during** DNA replication.
- DNA PolIII makes error once every 100000 insertions but catches 99% of those errors.
  - It behaves as a 3' to 5' exonuclease: cuts out the wrong nucleotide and replaces it.
- **Mismatch repair** is a mechanism that can repair the errors that DNA PolIII did not catch.
  - Recognizing the template strand (to not remove the wrong nucleotide) is based on **DNA methylation**:
    - A methyl group is added to the adenine residues (by the enzyme adenine methylase) but not right away which allows for another enzyme to recognize the new strand (not yet methylated) from the new strand (already methylated).
  - An endonuclease enzyme creates a nick in the strand.
  - An exonuclease enzyme unwinds the nicked DNA strand.
  - DNA polymerase fills the gap with the correct nucleotide.
- Mutations in the genes coding for DNA mismatch repair proteins are linked to colon cancer.

#### Postreplication Repair

- This repair system responds **after** DNA replication.
- This is needed when parts of the DNA were not replicated due to lesions (like from thymine dimers).
- This repair system finds the gaps allowing DNA polymerase to fill them and DNA ligase to attach everything together.
- It is a form of **homologous recombination repair**.
- In bacteria, **SOS repair** is activated when there's a lot of unrepaired DNA mismatches and gaps.
  - It is a last resort repair and is error-prone (thus prone to mutations) but can allow the cell to survive.
  - It is error-prone because it inserts random nucleotides in places where replication would have stalled.

#### Photoreactivation Repair

- Repaired UV damage (like pyrimidine dimers).
- DNA can be partially repaired, if following irradiation, the cells are exposed briefly to light in the blue range of the visible spectrum.
- The process is dependent on the activity protein called **photoreactivation enzyme (PRE)** which remove the bond between thymine dimers.
  - In order to do that it needs a photon (it can attach to the dimer in the dark but needs the photon to break the bond).
- The PRE enzyme is present is many organisms including some vertebrates but not in humans.

#### Base Excision Repaired

- Similar to the mismatch mechanism but works on both strands.
  - Recognition of a distortion on the DNA helix.
  - A nick is made by and endonuclease.
  - DNA polymerase fills the gap.
  - DNA ligase seals everything.
- Two types of excision repair:
  - **BER - base excision repair**: repairs a damaged DNA base.
    - **DNA glycolases** are enzymes that each recognizes specific base (like uracil DNA glycolase recognizes uracil).
  - **NER - nucleotide excision repair** repair bulky lesions in the DNA (like pyrimidine dimers and DNA adducts).
- In eukaryotes the NER mechanism is more complex with a lot of proteins involved encoded in around 30 genes.
- **Xeroderma pigmentosum (XP)** is a recessive genetic disorder that predisposes individuals to severe skin conditions (those individuals are very sensitive to UV light).
  - It is thought that it comes from mutations in genes of the NER pathway.

#### Double-Strand Break Repaired (in eukaryotes)

- **DSB repair (double-strand break repair)** are responsible for reattaching two broken DNA strands.
- Two pathways:
  - **Homologous recombination repair**: see page 418 (on the right, third paragraph).
    - It uses is similar section of a sister chromatide as a template (the damaged strand of a double helix attaches to a complementary strand of another double helix)
  - **Nonhomologous end joining**: see page 418 (on the right, fifth paragraph).
    - Doesn't use a template.
    - A part of the damaged DNA is removed and both ends around this part are joined together.
    - Not a good system.

## The Ames Test

- Used to assess the mutagenicity of a compound.
- Mutagenicity can be tested in various ways but the most common tests use **bacteria**.
- The Ames test uses different strain of the bacteria **Salmonella typhimurium**.
  - They can reveal specific types of mutations like base-pair substitution or frameshifts.
  - Each strain contain a mutation in one of the genes of the **histidine operon**.
  - Because a lot a substances are activated in the liver, the substance being tested is incubated in vitro in the presence of liver extract.
  - The test checks the **frequency of reverse mutation** into wild-type bacteria.
- This test is useful as a preliminary test.
  
  ![ames test](./img/14_Gene_Mutation_DNA_Repair/ames_test.png)
  
## Transposable Elements

- **Transposons** are "jumping genes" that can move within and between chromosomes.
- Half of the human genome is derived from transposable elements.
- Consequences: disrupting genes, mutations, double-strand breaks.
- In bacteria, 2 types of transposons:
  - **Insertion sequences** (IS elements) can move from one location to another.
    - They can cause mutations if move into a gene or its regulatory region.
    - IS elements contain **inverted terminal repeats** (ITRs) on its ends that are short segments of DNA with the same nucleotides but in inverted order (e.g AGTC CTGA). Those sequences are used by the enzyme that bind IS elements.
    - IS elements code for the enzyme that allow the transposition of themselves and Tn elements (see below). This enzyme is called **transposase**.
  - Tn elements are bacterial transposons that contain protein coding genes.
    - They also contain the ITRs.
  
  ![itr](./img/14_Gene_Mutation_DNA_Repair/itr.png)
  
- Tn elements might introduce drug resistance onto bacterial plasmids (R factors).

### Transposons in Maize (corn)

- **Barbara McClintock** discovered mobile generic elements in **corn plants** before transposons were discovered.
- She analyzed two pigmentation mutations in corn kernels:
  - **Dissociation (Ds)**
  - **Activator (Ac)**
- Ds is able to move locations but only if Ac is present.
- Ac is able to move autonomously.
- Ac codes for the transposase enzyme.
- Ds is derived from Ac by internal deletions.
- When Ds moves location it can inserts itself into pigmentation genes disrupting their expression. It can move out of the gene location reestablishing the gene as it should be and allowing its expression.

![ac ds](./img/14_Gene_Mutation_DNA_Repair/ac_ds.png)

- In Drosophila there are 30 families of transposons. 
  - Not all are active but still can cause issues.
- In human transposons:
  - LINE and others
  
  ![transposons humans](./img/14_Gene_Mutation_DNA_Repair/transposons_humans.png)
