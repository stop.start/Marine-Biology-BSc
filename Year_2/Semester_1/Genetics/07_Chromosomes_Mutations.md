# Chromosome Mutations

![chromosomes structure](./img/07_Chromosomes_Mutations/chromosomes_structure.png)

- Today we use fluorescence to color chromosomes and track them.
- Subtantial changes called **chromosome mutations** or **chromosome aberrations** can have drastic effects on the phenotype:
  - Too many or not enough chromosomes
  - Deletion or duplication of genes or segments of chromosomes
  - Rearrangement of the genetic material.
- **Aneuploidy** is the loss or gain of chromosomes but not a complete set. This is caused by **nondisjunction** where homologous chromosomes or sister chromatids are not separated during meiosis.
- The loss of a single chromosome is called **monosomy** while the gain of a single chromosome is called **trisomy**.
- **Euploidy** is the loss or gain of a complete haploid set of chromosomes.
- Contrary of the previous chapter, here the chromosomes added or lost are autosomal.

## Aneuploidy

### Monosomy

- Having a single copy of a chromosome can be lethal because:
  - Recessive genes are expressed.
  - Recessive genes may not be sufficient to provide adequate function. This is called **haploinsufficiency**.
- Plants can survive aneuploidy but are less viable.

### Trisomy

- Similar but more viable than monosomy as long as the added chromosome is small. Big chromosomes can be lethal.
- In plant, trisomic individual are viable with altered phenotype.

#### Trisomy 21 - Down Syndrome

- The only human autosomal trisomy that a lot of individuals survive.
- Discovered in 1866 by Langdon Down.
- Trisomy of the chromosome 21.
- Down syndrome is characterized by 12 to 14 phenotypic characteristics. Individuals usually express **6 to 8**.
- 95% of the times it comes from the mother. From data collected, it seems that the risk of having children with Down syndrome increases with the age of the mother.
- **DSCR - the Down Syndrome Critical Region** is a critical region in chromosome 21 thought to be the source of the condition.
  - It contains genes which are **dosage sensitive** and can be responsible for many phenotypes of the syndrome.
- The 3 copies of the genes in the DSCR are necessary but not sufficient fo the cognitive deficiencies.
- Individuals with the syndrome are **less at risk for certain cancers**. This is thought to be because of the extra copy of DSCR1 which codes for a **protein that supresses VEGF (vascular endothelial growth factor)**. This suppression blocks the process of **angiogenesis**.
- Although Down syndrome isn't expected to be inherited, it can run in families. In this case it usually involves **translocation of chromosome 21**.

#### Other trisomies

- **Patau** (trisomy 13) and **Edwards** (trisomy 18) syndromes are the only two trisomies that can survive to term (and only aneuploidy since no monosomy survive). They usually are lethal though.

## Polyploidy

- More than two haploid sets of chromosomes are present.
- If n is the number of chromosomes in a haploid set then: triploid is 3n, tetraploid is 4n, etc.
- This is found in lizards, amphibians, fish but it is mainly prevalent in **plants**.
- Because such a condition do not produce genetically balanced gametes, it will be found mainly on plants that do not rely only on sexual reproduction.
- **Autopolyploidy**: the extra set/s is identical to the normal haploid (seen in aggricultute - man made).
- **Allopolyploidy**: extra set/s from different species.

![allopolyploidy](./img/07_Chromosomes_Mutations/polyploidy.png)


## Composition and Arrangement of chromosomes

- Changes that delete, add, rearrange substantial portions of one of more chromosomes: inversion of chromosome segment, exchanged with a non homologous chromosome or transferred to another chromosome.
- **Translocations** are exchanges and transfers to another chromosome.
- **Inversion** won't have a lot of effects on an individual but during gametes formation it can cause duplication or loss of chromosomes often leading to steril offsprings.
- Usually those changes are due to breakage in the chromosome which then have "sticky" ends that can join other broken ends.

![rearrangements](./img/07_Chromosomes_Mutations/rearrangements.png)

### Deletions

- **Deletions** happen when a chromosome breaks in one or more places and a part of it is lost.
- A **terminal deletion** happens near the end of the chromosome, while an **intercalary deletion** happens in the middle of the chromosome.
- The part without the centromere is completely lost.
- With small deleted portion the individual might survive.
- During meiosis, the normal homolog must form a **compensation loop**.

![deletion](./img/07_Chromosomes_Mutations/deletion.png)

#### Cri du Chat Syndrome

- Loss of portion on p arm (short arm) of chromosome 5.
- The clinical symptoms of cri du chat syndrome usually include a high-pitched cat-like cry, mental retardation, delayed development, distinctive facial features, small head size (microcephaly), widely-spaced eyes (hypertelorism), low birth weight and weak muscle tone (hypotonia) in infancy.​
- In 2004, it was reported that part of the missing portion contains the TERT gene which encodes for an enzyme essential during DNA replication.

### Duplication

- Repeated segment of a chromosome.
- It may come from **unequal crossovers** which will cause deletions as well or through **replication error** before meiosis.
- Duplication may result in gene redundancy and phenotypic variation.
- One theory is that duplication has been an important source of genetic variability during evolution.
- **Ribosomal RNA (rRNA)** comes from a gene that has been duplicated several times. An hypothesis is that one copy does not provide enough rRNA for cells with high metabolism.
- DNA coding for rRNA is called rDNA.
- Evolution causes the duplicated genes to produce different proteins.
- **Gene families**: group of contiguous genes whose products perform the same or similar functios. The genes share a common origin and were duplications of it.
- The number of copy of a gene varies in individuals from the same species which shows that duplication occurs on a regular basis.

#### The Bar mutation in Drosophila

- Instead of the normal oval-eye shape, Bar-eyed flies have narrow eyes.
- Inherited the same way as a dominant X-linked mutation since the duplication (and some are triplicated) is on the X chromosome.

### Inversions

- Rearrange the linear gene sequence.
- Most of the time, the breakage doesn't happen in the middle of a gene.
- Rearrangements occur by forming a loop before breakage then the wrong ends join together.

![inversion](./img/07_Chromosomes_Mutations/inversion.png)

- It can be short or long, if it includes the centromere it called a **pericentric inversion** and if it doesn't it is called a **paracentric inversion**.
- Inversions don't have a lot of impact on the bearer but heterozygotes from inversions can produce aberrant gametes.
- If one chromosome has an inversion but its homolog hasn't then linear synapsis during meiosis isn't possible. They are called **inversion heterozygotes**. During meiosis they can be paired if they form an **inversion loop**.
- When crossing over happen between inversion heterozygotes:
  - In paracentric inversion: 
    - One recombinant **dicentric chromatid** (two centromeres) and one **acentric chromatid** (no centromere).
    - Both contain duplication and deletion.
    - During anaphase, an acentric chromatid moves at random, either to one random pole or it can get lost. A dicentric chromatid is pulled in two directions and makes **dicentric bridges** which break at some point. This produce gametes with missing genetic info.
    
![inversion](./img/07_Chromosomes_Mutations/inversion2.png)


### Translocations

- Translocation is the movement of a chromosomal segment to a new location in the genome.
- **Reciprocal translocation**: exchange of segments between two nonhomologous chromosomes.
- Genetic consequences similar to inversion.
- Homologs that are heterozygous for a reciprocal translocation undergo unorthodox synapsis during meiosis: they pair looking like a cruciform.

![translocation](./img/07_Chromosomes_Mutations/translocation.png)

#### Robertsonian translocation

- When the end of the short arm on nonhomologous chromosomes break and get lost, the larger segments fuse at their centrometric region which creates a new chromosome. This is called a **Robertsonian translocation**.
- This causes inherited Down syndrome.

![robertsonian translocation](./img/07_Chromosomes_Mutations/robertsonian.png)
