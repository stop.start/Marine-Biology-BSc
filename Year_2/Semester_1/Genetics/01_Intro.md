# Genetics

- **Translational medicine** is the process of using genetic finding and translate them into medical solutions (methods of diagnosis, treatments).

  > Example: Treating high cholesterol to prevent heart diseases  
  > LDL is the protein of cholesterol which in high levels in the blood can cause heart diseases.  
  > There are treatments but for some people those aren't effective or even a possibility.  
  > PCSK9 is a gene that participate in controlling the LDL levels. Its discovery has helped understanding more how levels of cholesterol work.  
  > LDL bind to LDL-receptor on liver cells and brought within the cell then the receptor goes back "recepting".  
  > PCSK9 binds the the LDL-receptor then, when LDL binds to the receptor as well and brought into the cell, they all are broken down (including the LDL-receptor) in a lysosome. This causes the LDL levels to rise in the blood since less receptors means less LDL brought into the cells and "taken care of".  
  > Those findings helped develop new treatment that inhibits PCSK9 (bind to them so they won't bind to the LDL-receptors).  

## How it began

- Heredity has been discussed since around 500-300 B.C. (Hippocratic treatise) , cells since 1830 (Schleiden, Schwann), but **genetics** started to develop rapidely from the work of **Darwin and Mendel** in the mid-1800s.
- **Darwin** was convinced that species underwent modification with time (i.e. current **species developped from previous ones**).
- Darwin formulated the theory of **natural selection** stating that there are too many offsprings compared to the carrying capacity thus creating a struggle for survival. Individuals with more adaptive traits survive.
- Darwin published **The Origin of Species** in 1859.
- **Mendel** work on peas showed that traits were passed form generation to generation (heredity) in a predictable manner.
- Mendel understood that traits are the results of a _pair of factors_ which later will be called genes which are separated during gamete formation.
- Mendel published his paper on peas in 1866 and is the foundation of genetics.

![Mendel's peas](./img/01_Intro/peas.png)

- From his work on peas, Mendel concluded that some pairs will form healthy offsprings and some won't: _Aa_ or _AA_ means healthy and _aa_ means not healthy.

![Mendel heredity](./img/01_Intro/heredity_peas.png)

## 20th Century

- Chromosomes were observed in the last few years of the 19th century.
- Sutton and Boveri found a parallel between the separation of the pairs of chromosomes and the separation of genes described by Mendel thus concluded that chromosomes carry genes.
- They formulated the **theory of inheritance** which states that inherited traits are controlled by genes residing on chromosomes faithfully through gametes, maintaining genetic continuity from generation to generation.

### Fruit flies

- Work on fruit flies helped understand more how genes and genetic varition work.
- **Mutation** is a modification in the DNA that can be inherited and is the source of genetic varition.

  > During the work on fruit flies, a white-eyed fly was found among red-eyed flies.  
  > This was the result of a mutation.  

- Mutant genes are used as markers which helps map the location of genes on chromosomes.
- **Alleles** are alternative forms of a gene (like the gene for eye color in fruit flies can have an allele for white and one for red).
- **Phenotype** is the observable features of an organism produced from different alleles.
- **Genotype** is the set of alleles carried by a specific organism.

### DNA

- In the early 20th century, chromosomes were known to be composed of proteins and DNA, but because of their number, the proteins were thought to be the carrier of genetic information.
- In 1944, it was showed that DNA carries the genetic information but it took some years to be received as truth.
- In 1953, Watson and Crick discovered the **structure of DNA**: double helix composed of nucleotides.
- The nucleotides composing DNA each contain one of 4 nitrogenous bases: [A]denine, [G]uanine, [C]ytosine, [T]hymine.

![DNA structure](./img/01_Intro/dna_structure.png)

#### Gene expression

1. **Transcription**: RNA is created from a complementary DNA strand.
2. **mRNA** goes out of the nucleus to the cytoplasm.
3. **mRNA** binds to a ribosome.
4. **Translation**: translating the genetic information in the mRNA into a protein $`\rightarrow`$ each **codon** (3 consecutive nucleotides) codes for one **amino acid**.

- There are 20 common amino acids.
- A mutation in DNA (i.e a different nucleotide than expected) will result in a different amino acid added to the polypeptid chain which can be the source of a different phenotype.

  > Sickle-Cell anemia:  
  > Hemoglobin is the protein responsible for the transportation of oxygen from the lungs to the cells.  
  > It is composed of α-globin and β-globin.  
  > A mutation in β-globin causes a change in 1 of the 146 amino acids composing it.  
  > Sickel-cell anemia is developped when someone has two genes with this mutation. This causes red blood cells to lose their shape when oxygen is low. Those red blood cells will break easily causing a decrease in the number of working red blood cells.  
  > This causes a lot of health issues.  
  
![hemoglobin mutation](./img/01_Intro/hemoglobin_mutation.png)

## Epigenetics and Biotechnology

- **Epigenetics** is the field of changing the genome (?)

### Cloning

- **Restriction enzymes** are used by bacteria to cut the DNA of invading viruses.
- Researchers used this in order to form **recombinant DNA** which are DNA molecules formed by laboratory methods of genetic recombination.
- **Vectors** are DNA molecules carrying DNA fragments produced by restriction enzymes. They form recombinant DNA.
- The recombinant DNA can be transferred in bacterial cells in order to produce copies (**clones**).
- **Genomic libraries** are collections of clones that represent organisms genome (complete haploid DNA content of specific organisms).
- One of the uses of genomic libraries is to test persons at risk of genetic anomaly when having a child.

![cloning](./img/01_Intro/cloning.png)

- Recombinant DNA is widely used in agriculture to modify crops plants.

![agriculture](./img/01_Intro/agriculture.png)

### Understanding genes functions

- **Reverse genetics**: using a known DNA sequence and discover its function.
- **Gene knockout**: disabling a gene and see what consequences it has at a cellular and phenotype level.
- **Transgenetic organisms**: organisms (like mice or fruit flies) with transferred genes from one species to another.
- Species have a lot of shared genes which helps researching solutions for one species (human) from other species (researching for colon cancer cure by using e.coli which has the same relevant genes).

## Ethics 

- With all the advancement in genetics, questions regarding ethics arise:
  - Privacy of genetic information.
  - Non moral use of genetic information.
  - Damage to the health and environment of plants, animals and transgenics.
  - Stem cells.
  - Human cloning.
