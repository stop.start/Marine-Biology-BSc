# Extranuclear Inheritance

- Extranuclear inheritance is usually transmitted from **one parent only** through **cytoplasm**.
- 3 types of extranuclear inheritance:
  - **Organelle heredity**: DNA in mitochondria or chloroplasts determines certain phenotypic characteristics of the offspring. Usually from the female parent.
  - **Infectious heredity**: From symbiotic or parasitic association with a microorganism in the cytoplasm.
  - **Maternal effect**: Nuclear gene products that are stored in the egg and then transmitted to the offspring's cells and influence its phenotype.
- **Moth butterfly** (פרפר עש) - eye pigment is given by the mother:
  - White female and dark male will give 50% white/dark offsprings.
  - Dark female and white male will result in 100% dark offsprings even those with _aa_ genes. Those offsprings will **become white** with time. This happens because the mother transmits pigments with the gamete but it is a limited resource which disappears over time.
  
![moth](./img/08_Extranuclear_Inheritance/moth.png)

- **Limnaea peregra** is a snail whose shell coiling is either left handed (dextral) or right handed (sinistral).
  - They are hermaphrodites and can reproduce by either cross fertilization or self fertilization.
  - The offsprings' phenotype depends on the **mother's genotype**.
  - The default is left handed since _D-_ codes for a protein that allows for dextral coiling but without it (_dd_) there is no protein and the default sinistral coil is applied.
  - The protein works in the egg thus the genotype of the mother defines the phenotype of the offsprings.

![snail](./img/08_Extranuclear_Inheritance/snail.png)

- All of the mitochondria and chloroplasts (in plants) is passed from the mother to the offsprings (or almost all of it).
- **Mirabilis jalapa** is a plant with several colors.
  - The phenotype (color) is the same as the female one (white, green or mixed).
  - The eggs contain chloroplasts and their DNA is what determine the color thus the DNA from the male doesn't matter.
  - When taking sex cells from a mixed colored area, there a chance taking more chloroplasts or less chloroplasts.
  - Statistics play a big role here since the chloroplasts are randomly dispersed during meiosis thus two "brothers" can look completely different.

![chloroplasts](./img/08_Extranuclear_Inheritance/chloroplasts.png)

![chloroplasts in cell division](./img/08_Extranuclear_Inheritance/chloroplasts_cells.png)


- Chloroplasts (and mitochondria) come from an **endosymbiotic event** (they were engulfed by an eukaryote).
- Despite having their own DNA, chloroplasts and mitochondria cannot survive alone. After the endosymbiotic event, a lot of DNA functions were duplicated which was unecessary, so some of the functions were lost in the chloroplasts and mitochondria (for example DNA polymerase doesn't exist and the host is responsible for DNA duplication).
- We estimate that the DNA that is found in chloroplasts today is around 10% of what it was once.
- Mitochondria receives DNA polymerase from outside.
- Mitochondria has genes for tRNA, respiration and ribosomes.
- Mitochondria is different from species to species but similar.
- In humans, 37 genes in the mitochondria including 13 genes coding for proteins and 22 genes coding for tRNA. 
- There is one small region in the mitochondria circular DNA that doesn't code for anything (d-loop). This region is important because it is where the DNA start replication (opens).

![dna mitochondria](./img/08_Extranuclear_Inheritance/mitochondria_dna.png)

- **Cytochrome oxidase** is an example of genes that comes from the mitochondria into the cytoplasm.
  - 4 of the required proteins that composed this enzyme are produced in the mitochondria and 3 of them are produced in the cytoplasm (7 total).
  - Part of the ribosomes are in the mitochondria.

![cytochrome oxidase](./img/08_Extranuclear_Inheritance/cytochrome_oxidase.png)

- Humans can get diseases from mitochondria: less **repair systems** thus mutations are more suceptible to not get repaired and make damages.
- The diseases can touch pretty all organs.
  - NARP ​
  - MELAS ​
  - MERRF ​
  - Cardiopathy ​
  - Myopathy ​
  - Leber's Hereditary Optic Neuropathy ​
  - Diabetes ​
  - Deafness ​
  - Leigh's Syndrome​
- **Heteroplasmic**: mitochondria DNA (mtDNA) are different within one organism. It happens since the mutations are not fixed and replicate.
- **Three parent embryo**: someone who has mitochondria mutations, taking the mtDNA from a third parent (second mother).

### Using mtDNA

- Mitochondrial DNA (mtDNA) is more or less the same within the individuals of a same species.
- **Barcode**: part of the mitochondria genome called **Cytochrome c oxidase subunit I** to compare between two individuals to know how close their species are.
- Mutation rate: 1 per 20000years
- Following the mutations follow a geographic path.
- 7 mothers
- The oldest "father" found is 40000 years old.
- Turtles:
  - 70 species
  - In the meditarreanan sea: seemed almost all the same species but are not in the DNA.
- d-loop - control region
