# Population and Evolutionary Genetics

- A **population** in a group of individuals belonging to the same species that live in a defined geographic area and reproduce.
- The **gene pool** of a population is the genetic information carried by the members of this population.
- Studying genetic variations of populations: 
   - Alleles frequencies, genotype frequencies.
   - How the frequencies change from one generation to the next.
   - How genotypes are distributed in time and space.
   - What processes affect a population's gene pool.
   
## The Hardy-Weinberg Law

- The **Hardy-Weinberg law** defines the the relationship between the relative proportion of alleles in the gene pool and the frequencies of different genotypes in a population.
  - It assumes that the population is large, randomly mating, not subject to evolutionary forces (mutation, migration, selection).
  - Two predictions: 
    - The frequencies of alleles do not change over time.
    - If, at a locus, there are two alleles - _A_ and _a_ - then after one generation of random mating the frequencies of the genotypes (_AA_:_Aa_:_aa_) in the population can be calculated with $`p^2 + 2pq + q^2 = 1`$
      - p = frequency of allele _A_
      - q = frequency of allele _a_
    - p + q = 1
- A population meeting those 2 predictions is said to be in Hardy-Weinberg equilibrium.
- It is rare for a real population to conform to this model and for the genotype frequencies to remain unchanged for generation after generation.

![probabilities](./img/16_Evolutionary_Genetics/probabilities.png)

- Looking at the above example:
  - Frequency of _A_ is 0.7.
  - Frequency of _a_ is 0.3.
  - 0.7 + 0.3 = 1.0
  - In the new generation, 49% of the gene pool is _AA_, 21%+21%=42% is _Aa_ and 9% is _aa_.
  - The frequency of alleles stays the same (0.7 _A_ and 0.3 _a_):
    - _Aa_ individuals produces half (50%) of gametes with _A_ and the other half with _a_ thus 21% of _A_ alleles and same for _a_.
    - Frequency of _A_ = 0.49 + 0.21 = 0.7
    - Frequency of _a_ = 0.09 + 0.21 = 0.3
- General form:
  - p = frequency of allele _A_
  - q = frequency of allele _a_
  
  ```math
  \begin{aligned}
  &\text{Alleles frequencies:}\\
  & \boxed{p + q = 1} \\
  &\text{Phenotypes frequencies:}\\
  & \boxed{p^2 + 2pq + q^2 = 1} 
  \end{aligned}
  ```

- **Neutral genes**  are not influenced by forces of evolution.
- The Hardy-Weinberg model has 3 consequences:
  - Dominant traits do not increase from one generation to the next.
  - Genetic variability can be maintained in a population (allele frequencies stay unchanged). This means that with time the inviduals in a population are not getting more and more homozygous.
  - Knowing the frequency of one genotype at a locus allows to know the frequencies of all other genotypes at that same locus.
- In order to test wether a population is in Hardy-Weinberg equilibrium, the population's genotype frequencies and compare it to the parental generation.
  - If from the parental generation we can predict (with the above formula) the offspring generation genotype frequencies then it is in equilibrium.
- The ABO blood system is one gene with 3 alleles: $`I^A,\;I^B,\;i`$.
  - It yields 6 genotypic combinations: $`I^AI^A,\;I^BI^B,\;ii,\;I^AI^B,\;I^Ai,\;I^Bi`$
  - $`I^A`$ and $`I^B`$ are codominant to $`i`$.
  - p, q and r represent the frequencies of alleles $`I^A`$, $`I^B`$ and $`i`$ respectively.
  - p + q + r = 1
  
  ```math
  \begin{aligned}
  &\text{Alleles frequencies:}\\
  & \boxed{p + q + r = 1} \\
  &\text{Phenotypes frequencies:}\\
  & \boxed{p^2 + q^2 + r^2 + 2pq + 2pr + 2qr  = 1} 
  \end{aligned}
  ```
  
 ![blood frequencies](./img/16_Evolutionary_Genetics/blood_freq.jpg)
 
- X-linked genes will yield frequencies of the alleles 0.66/0.33.
  - Males will always have the same genotype frequency as their mothers since they give them the X chromosome.
- The frequency of the X-linked allele in the gene pool and the frequency of male expressed X-linked trait is the same.
  - This means that the percentage of males affected by a recessive X-linked trait is equal to the frequency of that recessive allele.
- In females the frequency is $`q^2`$ (q being the frequency of the allele).

> Examples:  
> 8% of the males have a X-linked recessive trait (like color blindness).  
> This means that the allele frequency is 0.08 and also that 92% of X chromosome carry the dominant allele.  
> Let's define p=0.92 and q=0.08.  
> The frequency of females with two recessive alleles is $`q^2=0.08^2=0.0064`$.  
> The frequency of carrier females is $`2pq=2\times0.08\times0.92`$.  

- The frequency of homozygous individuals with a recessive trait can be determined just by counting the individuals with that trait.
- The frequency of heterozygotes in a population can be determined by using the number of homozygous individuals for a recessive trait.

> Cystic fibrosis is a recessive trait with a frequency of 0.0004.  
> Knowing that we can use Hardy-Weinberg law:  
> p is the frequency of the dominant allele and q of the recessive one (the one that causes cystic fibrosis).  
> $`q^2`$ is the frequency of the recessive phenotype (having two recessive allele).  
> $`q = \sqrt{q^2} = \sqrt{0.0004} = 0.02`$  
> p + q = 1 thus the frequency p of the dominant allele is:  
> p = 1 - q = 1 - 0.02 = 0.98  
> The frequency of heterozygotes can be determined with 2pq:  
> $`2pq = 2 \times 0.98 \times 0.02 = 0.04`$  

## Allele Frequency Changes

- **Speciation** is the formation of new species through evolution.
- Concept of **natural selection**:
  - Individuals of a species show variations in phenotype.
  - Many of those variations are heritable.
  - Organisms tend to produce more offsprings than can survivee. This causes the members of a species to compete for survival.
  - Individuals with certain phenotype will be more sucessful in the competition for survival (thus they will be able to pass on their genes).
- Hardy-Weinberg law is based on certain assumptions (like random mating and absence of selection) but certain genotype are more favorable than others to survival.
- If two alleles _A_ and _a_ have both a frequency of 0.5. The result of random mating gives the 0.25:0.5:0.25 ratio for genotypes _AA:Aa:aa_.
  - If the A phenotype is more favorable to survival, for example if the survival rates of a population of 100 inviduals are:
    - All 25 inviduals with _AA_ genotype.
    - 45 of individuals with _Aa_ genotype (90% of the 50 individuals with _Aa_).
    - 20 of inviduals with _aa_ genotype (80% of the 25 inviduals with _aa_).
    - The new gene pool has $`2 \times 25 + 2\times 45 + 2\times 20 = 180`$ gametes (each individuals with 2 gametes).
    - The frequency of alleles based on the new population is:
      - Frequency of _A_: $`2\times 25 = 50`$ from the _AA_ individuals + 45 from the _Aa_ individuals. Thus the frequency for _A_ allele is: $`(50 + 45)/180 = 0.53`$.
      - This means that the frequency of the _a_ allele is 0.47.
    - Those frequencies changed from the 0.5/0.5 that were one generation before thus don't correspond the Hardy-Weinberg law. This is because of natural selection which is the principal cause for change in allele frequencies.
- Mutations give variations to the genes but they **do not** change the frequency of alleles, their influence on the frequency is not relevant.
- **Migration** is a factor in change of allele frequencies.
  - The B allele of the ABO system comes from the east (central Asia - Mongolia -).
- **Fitness** is the genetic contribution of an individual to future generation.
  - It is represented by the letter $`w`$.
  - Genotypes with high rates of reproductive success are said to have high fitness.
- $`w_{AA}`$ represents the relative fitness of the genotype _AA_.
  - $`w_{AA} = 0.9`$ means that 90% of individuals with this genotype survive.
  - $`w_{aa} = 0`$ indicates a homozygous lethal recessive allele.
- Homozygous recessive individuals (for a lethal allele) die without leaving offspring the frequency of allele a drops:
  - With a lethal recessive allele, the frequency drops fast at first (all the homozygous individuals that die without offspring), then the decline is slower since most individuals are heterozygotes.
  - The decline in the frequency is defined by:
    - $`q_g`$: frequency of allele _a_ in generation _g_.
    - $`q_0`$: starting frequency of _a_.
    - _g_: the number of generations that have passed.
  
```math
q_g = \frac{q_0}{1 + gq_0}
```

![lethal](./img/16_Evolutionary_Genetics/lethal.png)

- Non lethal recessive alleles have various degrees of decline rates depending on selection.
- Selection can be classified (see page 692/693 for examples):
  - **Directional selection**
    - Phenotypes at the end of the spectrum are either selected for or against depending on the changes in the environment.
  - **Stabilizing selection**
    - Favors the phenotypes in the middle and the extreme ones tend to be selected against (e.g human birth weight (too small and too big children tend to not survive)).
  - **Disruptive selection**
    - Favors the extremes and not the intermediates. This can cause for 2 different populations to be formed.
- **Genetic drift**: in small population, significant **random** fluctuations in allele frequencies are possible by chance alone.
  - In addition to small populations, this effect exist also in population that are derived from a small population since the genes carried come from a small gene pool. This is called the **Founder effect**.
  - It can also happen via **genetic bottleneck** which means a drastic but temporary reduction in numbers. The population will recover but the genetic pool has been reduced.
- **Non random mating** is also a cause of changing the allele frequency.
  - **Positive assortive mating**: similar genotypes are more likely to mate.
  - **Negative assortive mating**: dissimilar genotypes are more likely to mate.
- The most common form of non random mating that affect genotype frequencies is **inbreeding**.
  - Inbreeding: mating individuals are more closely related than any two individuals from the population.
  - Inbreeding increases the proportion of a given allele.
  - **Coefficient of inbreeding - F**: probability that the two alleles of a given gene in an individual are identical because they are descended from the same ancestor.
  - F=1 means that all individuals of a population are homozygous and both alleles in everybody is derived from the same ancestor.
  - F=0: no individual has two alleles derived from a common ancestor.
  
> In the picture below, the pink female is the daughter (fourth generation of the family) of first cousins. What is the probability she will be homozygous for a recessive lethal allele?  
> If the grand-mother was a carrier then:  
> The grand-mother would have had to pass the allele to both her daughter (prob=1/2) and son (prob=1/2) then:  
> Both the daughter and son would have had to pass the allele to their daughter (prob=1/2) and son (prob=1/2) - the 2 parents of the pink generation.  
> In turn the daughter and son have to pass the allele to their daughter (prob=1/2 each).  
> In all the probability is $`\right(\frac{1}{2}\left)^6=\frac{1}{64}`$
> F is the probability of being homozygous. The probability calculated above is only for one allele. The pink daughter could have been homozygous for any of the 4 alleles of that genes (2 from her grand-mother and 2 from her grand-father) thus:  
> $`\bold{F = 4 \times \frac{1}{64} = \frac{1}{16}}`$  

![f coefficient](./img/16_Evolutionary_Genetics/f_coeff.png)

### Speciation

- A **species** is a group of actually or potentially interbreeding organisms that is reproductively isolated in nature from other similar groups.
- Two populations are considered different species if they are reproductively isolated from one another.
- Speciation may cause changes in morphology, physiology as well as adaptation to ecological niches.
- Migration counteracts the tendency of populations to diverge since it homogenizes allele frequencies.
- A reduction in **gene flow** between populations can cause the populations to diverge.
- Biological barriers preventing or reducing interbreeding between populations are called **reproductive isolating mechanisms**. Those can be:
  - Ecological
  - Behavioral
  - Seasonal
  - Mechanical
  - Physiological
- **Prezygotic isolating mechanisms** prevent individuals from mating.
  - Individuals from different populations may not find each other, are unable to mate, or are do not sea each other as potential mating partners.
- **Postzygotic isolating mechanisms**: isolation even when members of a species are able and will to mate with each other.
  - Genetic divergence might be too great for the viability of hybrids.
  - Hybrids might survive but sterile.
  - Even if hybrids are fertile their offsprings might have lowered viability (F2 is weak).
- Phylogenic tree show the divergence of species up to the species alive today.
  - The lines may reflect a time scale if denoted.
  - Groups that consist of an ancestral species and all its descendants are called **monophyletic groups**
  - The root of the tree is the oldest common ancestor of the species shown is that tree.

### Amino Acid Sequences

- The amino acid sequence of **cytochrome c** which is a eukaryotic **mitochondrial protein** is used to compair species and how close they are.
- This protein changed slowly over time which is why it is used that way.
- The difference between chimpanzees and humans have identical sequences.
- Human and rhesus monkey have only one amino acid difference.
- **Minimal mutational distance** is the minimum changes in nucleotide required for the difference in amino acid between two species.

![cytochrome c](./img/16_Evolutionary_Genetics/cytochrome_c.png)

### The Origin of our Species

- Studies of mtDNA have revealed the highest levels of diversity in African populations.
- Non-African populations have a lot less diversity and all originated around 50,000 ago.
- Homo sapiens originated in Africa and then migrated replacing other preexisting human species including Homo neanderthalensis (Nehanderthals).
- Neanderthals:
  - Lived in Europe and western Asia about 300,000 years ago.
  - Disappeared about 30,000 years ago.
  - Coexisted with Homo sapiens.
- Neanderthals genome is 99.7% identical to ours and both (ours and Neanderthals') are 99.8% identical to the genome of chimpanzees.
- Present day humans in Europe and Asis - but not Africa - have Neanderthal sequences.
- Interbreeding might have started in the Middle East.
- Neanderthals are not our ancestors but have contributed to our genome.
- Other human species might have contributed to our genome:
  - H. floresiensis (human fossils found on the island of Flores, Indonesia) were 1meter tall. They existed about 38,000 years ago. No DNA has been recovered due to the tropical conditions so we don't know if they contributed to our genome.
  - Denisova, Siberia: human fossils were discovered there that were a separate group from us and Neanderthals. They are closer to Neanderthals but sequencing showed that they contributed to the genome of some members of our species..
  
  ![trees](./img/16_Evolutionary_Genetics/trees.png)
  
## Heterosis

- In hebrew: און כלאיים
- Being heterozygote is better: two specific different alleles together give better offspring (A1B2 > A1B1).
  - Two homozygous parents for those two alleles will give the best offsprings.
