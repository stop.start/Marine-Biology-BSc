# Mitosis & Meiosis

**What is life?**:

- Order: cells as unit of life.
- Homeostasis: state of steady internal physical and chemical conditions maintained by living systems.
- Metabolism
- Growing, adapting and reacting to the surrounding.
- Reproduction: hereditary material
- There are two types of cells: eukaryotes and prokaryotes. The main difference being a defined membrane bound nucleus in prokaryotes.
- All cells have the following properties:
  - Cell membrane
  - Hereditary material - DNA
  - Ribozomes

## Intro

- **Mitosis** is the duplication of a cell into an identical one, also called **asexual reproduction**.
- **Meiosis** reduces the genetic material by half to produce sex cells - **gametes** or **spores**. It is also called **sexual reproduction**.
- Chromosomes are only visible during those two stages. Outside of those two stages, the chromosomes unfold within the nucleus.
- The unfolded chromosomes and the dispersed proteins fibers inside the nucleus are called **chromatin**.
- During mitosis and meiosis the unfolded chromosomes and the proteins fibers condense into chromosomes.
- The **nucleolus** is an area within the nucleus where ribosomal RNA - rRNA - is synthesized.
- In **prokaryotes** the **circular DNA** enclosed in a specific area: the **nucleolid**.
- During mitosis the prokaryotes DNA are not as compact and visible as in eukaryotes.
- In **diploid** organisms, chromosomes exist in pairs. Chromosomes of a same pair are called **homologous chromosomes**.
- When condensed, the chromosomes' **centromere** is visible. It is the region where a chromosome is more or less bent.
- Each chromosome of a same pair is identical in length and centromere location and genetic content, except for the sex-determining chromosomes.
- Sex-determining  chromosomes - X & Y - although not identical they do behave as homologous chromosomes during the meiosis passing to the gamete either one of them (i.e for males either X or Y will be transfered to the gamete).
- When DNA is duplicated, the pair of chromosomes are identical (not homologous but identical). The chromosomes in those pairs are called **sister chromatids**.
- The centromere is also the region where sister chromatids connect.
- The **haploid number** of chromosomes contain the genome of the species.
- During meiosis, the gametes/spores will be created with an haploid number of chromosomes.
- When two gametes come together it creates a **diploid zygote**.
- A specific **gene** can exist in different forms which are called **alleles**.

## Mitosis

- Mitosis allow the organism to grow from a **zygote** (fertilized egg) as well as heal and replace tissues.
- Mitosis replicates a single cell and results into two daughter cells which requires for the DNA to be first duplicated.

### Cell cycle

![cell cycle](./img/02_Mitosis_Meiosis/mitosis_cycle.png)

- **Interphase**: chromatin - no visible chromosomes
  - **S phase**: replication of DNA.
  - **G1 (gap I)** and **G2 (gap II)**: before and after DNA synthesis. At the end of G2 the cell has doubled in size.
  - **G0**: after mitosis, the cell can go back to G1 for a new reproduction cycle or enter G0 which is in between reproduction cycles.
- **Mitosis**:
  - **Prophase**
  - **Prometaphase**
  - **Metaphase**
  - **Anaphase**
  - **Telophase**

> _Note:_  
> Tumor are cells that don't stop dividing and reproducing.  
> They don't enter the G0 phase, instead they continue always the G1 phase in order to continue the cycle again and again.  

#### Prophase

- Longest phase of the mitosis.
- The **centrosome** is composed of two pairs of **centrioles**. Each pair move to one side of the cells.

![centrosome](./img/02_Mitosis_Meiosis/centrosome.png)

- The nucleus membrane disappears.
- The chromosomes condense.
- Sister chromatids join at their centromere.
- **Cohesin** holds the sister chromatids together from on extremity to the other.

![cohesin](./img/02_Mitosis_Meiosis/cohesin.png)

#### Prometaphase and Metaphase

- **Prometaphase** is the stage where the chromosomes move to the middle in a line perpendicular to the spindle fibers axis (from the centrioles).
- **Metaphase** refers to the stage where the chromosomes are placed in line.
- The movement of the chromosomes is done by the spindle fibers binding to the **kinetochore** of the chromosome.
- The kinetochore are plates in the centromere region.
- The **spindle fibers** are made of microtubules. Those microtubules are made of **tubulin subunits**.

![kinetochore](./img/02_Mitosis_Meiosis/kinetochore.png)

- The cohesin is dissolved by the enzyme **separase** but in the centromere region which is protected by a protein called **shugoshin**.
- By the end of this stage all the chromosomes are in line in the middle of the cell and all sister chromatids are joined only at their centromere region.

![shugoshin](./img/02_Mitosis_Meiosis/shugoshin.png)

#### Anaphase

- Shortest phase of the mitosis.
- During the **disjunction**, the sister chromatids are pulled from one another, each to one side of the cell (to the centrioles).
- In order to achieve the disjunction the shugoshin protein need to be degraded.
- At this point the chromatids are called **daughter chromosomes**.
- **Motor proteins** cut the spindle fibers (tubulin subunits) shortening them thus causing the chromatids to move closer to the side of the cell.

![motor protein cut spindle fibers](./img/02_Mitosis_Meiosis/cut_tubulin.png)

#### Telophase

- During telophase, **cytokynesis** takes place.
- During cytokynesis, the cytoplasm is divided in two to complete the two daughter cells. This includes moving part of the organnelles to one daughter cell and the rest to the other.
- The organnelles don't have to be divided equally between the daughter cell which has (or should have if the anaphase went smoothly) all the genetic information in order to produce new organnelles.
- Mitochondria and chloroplast have their own DNA and can produce more of themselves if not enough has passed in the daughter cell.
- In **plant cells**, a **cell plate** in synthesized.
- The nuclear membrane reform while the chromoses uncoil and return to **chromatin** form.
- At the end of this stage the cell enters **interphase**.

### Checkpoints

- During the cell cycle, mutations can occur. Those mutations can be detrimental the good functioning of the cell.
- Some molecules checks the division of the cell at **checkpoints**. There are **3 known checkpoints**.
- During those checkpoints, it is checked that all vital conditions for the good functioning of the daughter cells are met.
- Those checkpoints help prevent cells with bad DNA to reproduce.

![checkpoints](./img/02_Mitosis_Meiosis/checkpoints.png)

#### Checkpoint G1/S

- The cell checks its size since the previous mitosis and DNA conditions.
- Only if the cell has reached a certain size and if the DNA isn't damaged, the cell is allowed to proceed to the S phase.

#### Checkpoint G2/M

- Checks DNA before the next mitosis.
- The cycle won't proceed if: the DNA is damages or the replication is incomplete.
- Damaged DNA can be repaired.

#### Checkpoint M

- Attachement of the spindle fibers to the kinetochore in the centromere region is monitored.
- If something is wrong the mitosis is stopped.


## Meiosis

- Produces **haploid** cells.
- For an organism with n pairs of chromosomes, the number of possible haploid cells with different combinations of "dad/mom" chromosomes is $`\bold{2^n}`$.
- Sexual reproduction ensures genetic variety mainly due **crossing over**.
- In **mitosis**, the DNA is duplicated then 2 cells can be produced, whereas in **meiosis** the DNA is duplicated, 2 cells are produced (crossing over happen at that stage) then 2 cells from each cell (4 total) are produced.
- Meiosis has two stages: Meiosis I and Meiosis II. Both have PMAT cycle.

![meiosis](./img/02_Mitosis_Meiosis/meiosis.png) 

### Meiosis I

#### Prophase I

- Entering this stage the DNA is already duplicated.
- **Leptonema**: the DNA start condensing and start searching for their homologous pair.
- **Zygonema**: the chromosomes condense more although the sister chromatids are not yet visible as two different structures. In the end the homologous chromosomes are next to each other (**synepsis**) and each pair is called a **bivalent**.
- **Pachynema**: chromosomes become visible as a double structure and each pair of homologous chromosomes is called a **tetrad**.
- **Diplonema**: chromosomes are fully condensed. Chromatids of homologous pairs join in areas called **chiasma**. This process is called **crossing over (שחלוף​)** and this is what allow for a lot of genetic variety: the homologous chromosomes exchange genetic material.
- **Diakinesis**: pulling the chromosomes on each side, homologous chromosomes are each pulled to a different side but they both are still with their sister chromatids.

![prophase I](./img/02_Mitosis_Meiosis/meiotic_prophase1.png) 

#### Metaphase I, Anaphase I and Telophase I

- Similar to the same phases in mitosis.
- **At the end of cytokynesis, the cells are already haploids** but the chromosomes still need to be separated from their sister chromatids.

### Meiosis II

- Works like in mitosis (PMAT).
- In the end 4 haploid cells are created.

## Spermatogenesis & Oogenesis

- Production of the gametes.
- In **oogenesis** the cytoplasm isn't divided equally between the haploid cells. Only the cell receiving the most cytoplasm gets the be the **ovum** (egg).
