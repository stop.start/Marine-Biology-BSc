# Genetics
_Based on: **Concepts of Genetics (10th edition) - PEARSON**_

1. [Intro](./01_Intro.md)
2. [Mitosis & Meiosis](./02_Mitosis_Meiosis.md)
3. [Mendelian Genetics](./03_Mendelian_Genetics.md)
4. [Extensions of Mendelian Genetics](./04_Extensions_Mendelian_Genetics.md)
5. [Chromosomes Mapping in Eukaryotes](./05_Chromosomes_Mapping.md)
6. [Sex Determination](./06_Sex_Determination.md)
7. [Chromosome Mutations](./07_Chromosomes_Mutations.md)
8. [Extranuclear Inheritance](./08_Extranuclear_Inheritance.md)
9. [DNA](./09_DNA.md)
10. [DNA Replication and Recombination](./10_DNA_Replication.md) 
11. [DNA Organization in Chromosomes](./11_DNA_Organization.md)
12. [The Genetic Code, Transcription and Translation](./12_Genetic_Code.md)
13. [Regulation of Gene Expression in Prokaryotes](./13_Gene_Expression_Regulation.md)
14. [Gene Mutation and DNA Repair](./14_Gene_Mutation_DNA_Repair.md)
15. [Quantitative Genetics and Multifactorial Traits](./15_Quantitative_Genetics.md)
16. [Population and Evolutionary Genetics](./16_Evolutionary_Genetics.md)
17. [Developmental Genetics](./17_Developmental_Genetics.md)
18. [Recombinant DNA Technology](./18_Recombinant_DNA_Technology.md)

