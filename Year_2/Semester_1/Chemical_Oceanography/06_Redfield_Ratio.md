# Plankton Stoichiometry and Redfield Ratio

![biogeochemical loop](./img/06_Redfield_Ratio/biogeochemical_loop.png)

- A few reminders:
  - Dissolved matter can only return to the surface in upwelling areas.
  - Redfield ratio: C:N:P $`\rightarrow`$ 106:16:1
- Redfield wrote the hypothesis that changes in inorganic matter concentrations depend only on the organic characteristics that break down.
  - Maybe the chemical composition of the living organisms is more or less the same.
  - He looked at elements (C,N,P) that have a smaller concentration at the surface and a higher concentration in deep water (used in biological processes).
- When looking at $`NO_3:PO_4`$ ratios samples, there's clear linear relation between them.
  - In all oceans the ratio is the same.

![np ratio samples](./img/06_Redfield_Ratio/np_ratio_samples.png)

- This means that, knowing the changes in one element makes it easy to calculate the changes in another element.
  - _Examples_: 

```math
\begin{aligned}
& \Delta N = 16 \cdot \Delta P \\
& \Delta C = 106 \cdot \Delta P \\
& \Delta P = \Delta N \div 16 \\
& \Delta C = (\Delta N \div 16) \cdot 106 
\end{aligned}
```

- The energy from the sunlight is converted to sugar by plants and bacteria, eaten by heterotrophs (carnivorous heterotrophs eat other heterotrophs). 
  - The sugar is used by all organisms and the energy from the sunlight is released at heat from those organisms.
  - From start to finsh of this process there's a loss of energy (from the sunlight's energy caught by the plants to the release of heat from all organisms that use it).

![energy pathway](./img/06_Redfield_Ratio/energy.png)

- Redfield followed ratios of Carbon, Nitrogen and Phosphorus. 
  - All living organisms are carbon based thus is important to look at.
  - Nitrogen is found (not only) in Proteins (enzymes).
  - Phosphorus is mainly found (not only) in nucleic acids, ribosomal RNA, membranes (used from reproduction).
- The issue with Redfield ratio: **organisms that try to survive will produce more enzymes (= more nitrogen particles) while organisms that live in good conditions will reproduce more (= more phosphorus particles). This means that the ratio N:P should vary depending on the conditions the organisms live in**.
  - Redfield explained that it's possible that the sea behaves around the average quantities.
- Redfield tested the PNC ratio in plankton and found the 1:16:106 ratio. In the sea he found the ratio 1:15:105.
- The conclusion is that the concentrations in the sea are based on the living organisms that live in it.
- The difference in nitrogen 16 in organisms and 15 in the sea is from organisms that know how to fix nitrogen.
- Carbon and Phosphorus: a lot in the sediments.
- $`CO_2`$: a lot in the sea, less in the atmosphere.
- $`N_2`$: a lot in the atmosphere but from the quantity that enters the sea, only a small part in transformed into amonia $`NH_3`$ which can partake in the biological cycle.
- **If the N/P ratio > 16 $`\rightarrow`$ phosphorus is the limiting element**.
- **If the N/P ratio < 16 $`\rightarrow`$ nitrogen is the limiting element**.
- The above is because the usage ratio in biological processes is constant (N:P = 16:1) thus if the ratio N:P in the sea is below 16 then there's not enough nitrogen for all the phosphorus and vice versa.
- Since some organisms know how to fix nitrogen, it is not the most limiting element (even if its entry is slow). The real limiting element is phosphorus.

![biological cycle](./img/06_Redfield_Ratio/biological_cycle.png)

- $`C_{106}H_{175}O_{42}N_{16}P_{1}`$ is not a real molecule. It simulates the ratio of atoms present in plankton.
- When creating one of these above "sea molecules" $`150 O_2`$ are created.
- **AOU (Apparent Oxygen Utilization)**: $`O_2\;saturation - O_2\;measured`$
  - The surface water are always saturated in $`O_2`$ due to its proximity to the atmosphere.
  - The water below the photic layer uses $`O_2`$ for respiration only.
  - In the north Atlantic, the water sinks full of oxygen thus the AOU is low. It rises the farther it goes from there.
  - Some maths:
    - [P] preformed: existing nutrients at the surface before sinking (the water is low on them but they exist).
    - [P] regenerated: regenerated dissolved phosphorus at the bottom from the particles.
  
```math
\begin{aligned}
& [P] \text{observed} = [P] \text{preformed} + [P] \text{regenerated} \\
& [P] \text{regenerated} = [P]_{measured} - [P]_{preformed} = \Delta P = AOU \div 150 \\
& AOU = O_{2\text{saturation}} - O_{2\text{measured}}\\
& [N] \text{regenerated} = \Delta N = 16 \Delta P = 16 \times (AOU \div 150)
\end{aligned}
  ```
  
- In the graph below we can see that:
  - The data at the surface is 0 for N but not for P (there's always a bit of phosphorus at the surfece).
  - The water in the Pacific are old water that lacks oxygen (they start in the Atlantic and by the time they get to the Pacific they are low on oxygen).
    - $`NO_3`$ can be broken down to get and use this oxygen for respiration thus less (???).
  - The data from the Atlantic is mostly above the 1:16 ratio line which means that it is limited in Phosphorus.
  - The data from the Pacific is mostly under the 1:16 ratio line which means that it is limited in Nitrogen.
  
![ratio oceans](./img/06_Redfield_Ratio/ratio_oceans.png)


- Phosphate is the limited factor in the sea. Nitrogen can always be taken from the atmosphere.
  - Nitrogen and phosphorus both comes from the rivers, sink in sediment and are used in the biological cycle in the same N:P ratio. 
  - Only nitrogen can also come from the atmosphere thus in the long term phosphorus is the real limiting element.
- In places (like Alexander river) where nitrogen and phosphorus are in big quantities, the limiting element is the sunlight.
- The oxygen concentration depends on:
  - Solubility of the oxygen (oxygen from the atmosphere).
  - Biological activity: photosynthesis, respiration.
- The concentration of oxygen at the surface of the sea is a bit oversaturated (>100%) from:
  - Photosynthesis.
  - Oxygen taken from the atmosphere by waves and such.
- Because it is hotter at the surface the concentration of $`O_2`$ is lower (hot solution can contain less dissolved matter).
- Then the concentration increases because the water is colder + photosynthesis.
- At around 1km the concentration is at its minimum (it is below the photic layer):
  - It takes long for particles to sink, this means that organic matter is mainly broken down in this layer and it requires $`O_2`$ to do that.
  - The water below has a higher concentration of oxygen.
