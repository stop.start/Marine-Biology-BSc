# Sediments

## Deposition of Sediment

- Sediment is what makes up the floor wether seafloor or landfloor.
- The seafloor structure:
  - Continental shelf (מדף היבשת)
  - Slope (שולי היבשת)
  - Abysal planes (המישורים האביסליים)
- The slope $`\rightarrow`$ 4% slope.
- The continental shelf slope $`\rightarrow`$ 1% slope.

![structure](./img/08_Sediments/structure.png)

- The sendiments are composed from particles from the land (terrigenous), some of which are blown farther by the wind, atmosphere and from salt because of the evaporation (messianic event in the Mediterranean).
- **Authigenic** sediment: sediment formed where it is found (sediment in the seefloor that comes from the land is NOT authigenic but salt from the sea is).
- **Biogenic** sediment: sediments from animals.
- Sediments are a collection of particles from different origins. 
- The layers of sediments are an indication of history.
- The **porosity** of sediment: sediments can be more or less compact, meaning that there are holes between the particles where liquid accumulates.
  - To test the porosity, the sediments are weighted then dried out (at $`105\degree C`$) to remove all liquid and then weighted again.
- Most sediments are made up of water.
- Usually the **porosity** decreases the deeper the sediments are because of the pressure from the sediments above.

```math
\begin{aligned}
& \text{Porosity} \; \Phi \\
& \Phi = \frac{V_{liquid}}{V_{liquid}+V_{solids}}
\end{aligned}
```

![porosity](./img/08_Sediments/porosity.png)

- To know the **percentage of organic** matter in the sediments:
  - $`DW_{105}`$ = matter that was dried at $`105\degree C`$ in order to not burn anything, just remove water.
  - $`DW_{450}`$ = matter that was heated at $`450\degree C`$ in order to burn the organic matter.
  - The difference between the two is the organic matter mass.
  
```math
LOI_{450} = \frac{DW_{105}-DW_{450}}{DW_{450}} \times 100
```

- Usually the concentration of organic matter is under 3% but in areas with high porosity it can reach 10%.
- When the organic matter leaves the sediments, it does so in its inorganic form thus becoming nutrients.
- **Types of sediments**:

| Clastic (land)               | Biogenic     |
|------------------------------|--------------|
| Rivers                       | Soft tissues |
| Eolian (wind)                | Shell        |
| Ice rafted debris (ice caps) |              |
| Turbidites (land slides)     |              |
| Meteorites                   |              |
| Direct from the seafloor     |              |

- Categorizing sediments can be done also by size (see picture below). In hebrew silt and clay are both חרסיות. 
  - Mud usually means either silt or clay.
  - Under $`1\mu m`$ is considered dissolved. $`0.7\mu m`$ is so dissolved that it doesn't sink.

![sand](./img/08_Sediments/sand.png)


- The farther from the shore, the less sand there is (more mud, meaning more silt and clay).
- Near the shore a lot of sand (the bigger particles) and not so much silt and clay.
- The rate of sedimentation is slow (less than 2cm/1000 years)
- The sediments in the upwelling areas
  - Is more porous.
  - Is biogenic (silica) because upwelling allows for nutrients to go up thus lot of primary production.
- At the equator the sediments are mostly biogenic (silica) because of the upwelling.
- In the Atlantic there are more sediments because of the Sahara.
- The type of sediment particles and their origin:
  - Glacial sediments: from ice caps origin (like fjords).
  - Land particles that were dragged by rivers.

![sediments map](./img/08_Sediments/sediments_map.png)

- Israel is build mainly from sea organisms (sea sediments that go above water).
  - **Calcium carbonate** skeleton - **גיר**:
    - Coccolithophorid: phytoplankton
    - Zookplankton like foraminifera and pteropods.
  - **Opal** (silica):
    - Diatoms (צורניות). In areas with a lot of nutrients.
    - Radiolaria
- $`CaCo_3`$ minerals:
  - **Calcite**: from coccolithophorids and foraminifera. More resistant to dissolution.
  - **Argonit**: from pteropods. Less resistant to dissolution.
- **Carbonate Compensation Depth (CCD)**: 
  - Deep water dissolves more $`CaCO_3`$ because it is **colder and more acidic**.
  - At the surface there's the primary production, deeper there's breaking up of organic matter, somewhere between those layers there an area where the rate of breaking up equals the rate of primary production $`\rightarrow`$ this is called the compensation depth.
  - Volcanoes are higher on the sides and lower in the middle which matches the above description. Because oceanic ridges are basically long volcanoes (mainly from north to south) the $`CaCO_3`$ will be found vertically along them.

|                                    |                                      |
|------------------------------------|--------------------------------------|
| ![ccd](./img/08_Sediments/ccd.png) | ![ccd](./img/08_Sediments/caco3.png) |

- As a result of mid ocean ridges constantly opening:
  - There are more sediments far from a mid ocean rigde than close to it (from sediment showering above): bigger layer far than close.
  - At some point the layer of $`CaCO_3`$ stops getting bigger and even becomes thinner from the acidic depth dissolving it.
  - Areas where calcium carbonate doesn't sink to the bottom (i.e is dissolved before reaching the ground) it can be found in the sediments under layers that deposit above it.
- Far from the shores there's no much that sinks onto the bottom so there is mainly clay.


## Redox of Sediment

- There is less and less oxygen in the sea because:
  - The sea temperature increases thus is able to contain less gas.
  - Humans drop more and more fertilizers that are oxidized by oxygen.
- Marine snow: anaerobic organic matter poop form zooplakton.
- Anaerobic activity is mostly found where there's a big influx of carbon thus where there's a lot of nutrients and primary production thus biogenic sediment. For example upwelling areas.
- Breaking down organic matter uses oxygen. When there's no more oxygen, the rate of disassembly of organic matter slows.
  - The less organic there is in the sediment the deeper there will oxygen in the sediment.
- With decrease in oxygen there's an increase in nitrate (Redfield ratio) from the the breaking down of organic matter.
  - At some point where there's not enough oxygen, the oxygen in the nitrate is used to break down organic matter decreasing the level of nitrate.
    - This increases nitrogen gas $`N_2`$ levels.
    - When 
  - Once there's no more nitrate, metals are freed. Oxygen is taken from rust freeing iron ions ($`Fe^{2+}`$).
  - Once there's no more metals, sulfate is used for its oxygen converting it to sulfide.
  
|                                                    |                                              |
|----------------------------------------------------|----------------------------------------------|
| ![redox chain](./img/08_Sediments/redox_chain.png) | ![nitrogen](./img/08_Sediments/nitrogen.png) |
