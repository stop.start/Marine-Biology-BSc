# Oceanography of the Eastern Mediterranean

- The eastern Mediterranean is limited in **phosphate**.
- In oceans the limiting nutrient is nitrogen.
- Soap contains phosphate thus it is important to wash the measurement tools with acid and not soap.
- The N:P ratio is well over the 16:1 ratio that exist in oceans.
  - At the Gibraltar strait the ratio is 16:1, before the Sicilli strait 23:1 and in the Levant basin 28:1.
- In water with a ratio higher than 16:1, with primary production the ratio will go up and vice versa.
- Phosphate tends to be adsorb to particles.
  - **One theory**: with all the dust from the Sahara it can adsorb the phosphate and drown it.
  - Testing this theory with several type of dust showed that with the dust that is present in this area it was the **opposite result: [phosphate] increased**.
- **A second theory** was biological fixing of nitrogen thus increasing the ratio:
  - Fixing nitrogen requires a lot of energy thus not clear why this would happen.
  - With depth, nitrogen is transformed into amonia and then nitrate which is then used to break down organic matter which should return the ratio to 16:1.
  - In the eastern Mediterranean, there's almost no organic matter that sinks and can be broken down. This can explain why the ratio stays over 16:1.
- **A third theory** is a mixed between:
  - All the input of water into the Mediterranean is well over the 16:1 ratio (although this true for all oceans).
  - No use of the nitrogen to break down organic matter.
- From measurement, the fixation of nitrogen is low in the eastern Mediterranean thus the third theory seems the most probable.

