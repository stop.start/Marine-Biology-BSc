# Chemical Oceanography

1. [Intro](./01_Intro.md)
2. [Properties of Water](./02_Water_Oceans.md)
3. [Salinity and the Composition of Sea Water](./03_Salinity_Composition.md)
4. [Trace and Minor Elements](./04_Trace_Minor_Elements.md)
5. [Dissolved Gases](./05_Dissolved_Gases.md)
6. [Redfield Ratio](./06_Redfield_Ratio.md)
7. [Estuaries](./07_Estuaries.md)
8. [Sediments and Redox](./08_Sediments.md)
9. [Fluxes](./09_Fluxes.md)
10. [Box Model](./10_Box_Model.md)
11. [Inorganic Carbon & pH](./11_Inorganic_Carbon.md)
12. [Oceanography of the Eastern Mediterranean](./12_Oceanography_East_Mediterranean.md)
