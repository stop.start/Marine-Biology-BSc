# Isotopes

**Not for the exam**  

- The more protons there is in an element the less stable the element is and disassemble (=radioactive).

# Inorganic Carbon

## pH

- The lowest the pH the more acidic the water and vice versa.
- Surface water pH: **8.1**
- Once it was 8.2 but it decreased because of global warming.
- Average pH: **7.7**
- $`CO_2`$ increases the acidity of the sea:
- $`HCO_{3(aq)}`$: carbonic acid (dissolved $`CO_2`$)
- $`HCO^-_{3(aq)}`$: bicarbonate
- $`CO^{-2}_{3(aq)}`$: carbonate

```math
H_2O + CO_{2(gas)} \Harr \underbrace{H_2CO_{3(aq)}}_{Carbonic \; acid} \Harr \underbrace{H^+ + HCO^-_{3(aq)}}_{Bicarbonate} \Harr \underbrace{H^+ + CO^{-2}_{3(aq)}}_{Carbonate}
```

- **DIC** (dissolved inorganic carbon) = $`\sum CO_2`$
- 46% of the $`CO_2`$ stays in the atmosphere.
- The acidity affects organisms like shellfish with calcareous shells (made of limestone (french: calcaire, hebrew גיר)).
- The acidity reacts with limestone 
- Areas where the water gets warmer it releases $`CO_2`$ and areas where the water gets colder it absorbs more $`CO_2`$.

![comparasion](./img/11_Inorganic_Carbon/comparasion.png)

- $`O_2`$ and pH are not linked but $`O_2`$ is transformed into $`CO_2`$ which transforms into bi/carbonate and releases hydronium ions.
- $`CO_2`$ has a higher concentration in deep water **because of respiration** and **breaking down of calcareous shells**.
- Creation and breaking down of organic matter:

```math
O_{2(g)} + (H_2C)_n \Harr H_2O + CO_{2(g)}
```

- Creation and breaking down of calcareous shells:
- $`CaCO_{3}`$ is used at the surface to make the shells and is dissolved in deep water.

```math
CaCO_{3(solid)} \Harr Ca_{(aq)} + CO_{3(aq)}
```

- pH calculated from the reaction of carbon dioxide $`\Rightarrow`$ bicarbonate:

```math
\boxed{
pH = -\log\left(\frac{K_1[HCO_3^-]}{[H_2CO_3]}\right)
}
```

- pH calculated from the reaction of carbonic acid $`\Rightarrow`$ carbonate:

```math
\boxed{
pH = -\log\left(\frac{K_2[CO^{-2}_3]}{[HCO_3^-]}\right)
}
```

![pH diagram](./img/11_Inorganic_Carbon/pH_diagram.png)

- Photosynthesis increases the pH (uses $`CO_2`$).
- Respiration descreases the pH (releases $`CO_2`$).
- Shells formation decreases the pH (uses $`CaCO_3`$ increasing bicarbonate concentration).
- Shells disassembly increases the pH (releases $`CaCO_3`$ decreasing bicarbonate concentration).

![summary](./img/11_Inorganic_Carbon/summary.png**

- _Phosphate is also an acid: $`H_3PO_4`$_

## Carbonate System and Alkalinity

- **Alkalinty**: one definition is the difference in charge of strong anions and strong cations.

```math
\begin{aligned}
& \text{Charge of the cations:} \\
& A = \left{[Na^+] + [K^+] + 2[Mg^{2+} + 2[Ca^{2+}]} \longrightarrow \text{0.606 mol equivalent} \\
& \text{Charge of the anions:} \\
& - \left{[Cl^-] + 2[SO^{2-}_4 + [Br^-]} \longrightarrow \text{-0.604 mol equivalent}
\end{aligned}
```

- The overall charge is not 0. What balances the charge are the bi/carbonate ions.
= Calcium is the least conservative element. If there are organisms building calceous shells then the alkalinity will drecrease and if those shells are being broken down the alkalinity increases.
- A lot of carbon $`\implies H_2CO_3`$. 
- Lack of carbon $`\implies CO_3^{2-}`$.

- **Alkalinity**: another definition is a measure of acid neutralizing capability.
- This means that the more acidic the water the more $`H_2CO_3`$ there is (and less bi/carbonate ions).
- From this we can calculate the alkalinity by:

```math
A = [HCO_3^-] + 2[CO_3^{2-}]
```

- Alkalinity is calculated by titration of acid:
- Adding acids to the water to be tested while constantly checking the pH.
- The pH will increases at a non constant rate although the acid is added at a constant rate.
- The difference between the pH and the amount of acid added is the alkalinity.

```math
\boxed{
\boxed{
\begin{aligned}
& \text{Amount of carbon:}\\
& \sum{CO_2} = [HCO_3^-] + [CO_3^{2-}] \\
& \text{Alkalinity:} \\
& A = [HCO_3^-] + 2[CO_3^{2-}] \\
\end{aligned}
}
}
```

- From the above, the alkalinity is always higher than the amount of carbon.
- **Calcification enhanced photosynthesis and vice versa**.
  - Photosynthesis works when there's $`CO_2`$.
  - A lot of carbonate means that shells can be build (calcification).
