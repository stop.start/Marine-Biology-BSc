# Estuaries

[National Geographics](https://www.nationalgeographic.org/encyclopedia/estuary/)

- An estuary is a body of water partially closed, where a freshwater river or stream meets the ocean.
- Its level is below sea level (<0).
- In estuaries, the salty ocean mixes with a freshwater river, resulting in **brackish water** which is less salty than the ocean.
- Other names for estuary: bay, lagoon, sound, or slough.
- Water continually circulates into and out of an estuary. 
  - Tides (oceans) create the largest flow of saltwater.
  - River mouths create the largest flow of freshwater.
- In the ocean, the temperature determines the layers of water, in an estuary the salinity determines the layers.
- Estuaries can be big like the Black Sea or the Baltic Sea.
- Fjords are also estuaries: the ice melt (freshwater) from the mountains flows into the water and then into the salty sea
- **Most estuaries were formed from the rising of the sea level after the ice age (14000 years ago)**.
- Below types of estuaries (there are estuaries that combines different types):
  - Fjordic estuary: ice moved in between the mountains dragging dirt and when it got to the sea the ice melted which left all the dirt at the estuary entrance (see picture).
  - Tectonic estuary
  - Dam-build estuary (dam=barrage=סכר): from perpendicular currents that are strong close to the beach and become weaker the farther they are thus the dirt collected in the water falls, building a dam and the water mixed in the middle (see picture).
  - Deltas estuary
  
![types of estuaries](./img/07_Estuaries/estuary_types.png)

- ICOLLS - Intermittently Closed and Open Coastal System​ (Alexander river is an example).
  - In the Alexander river, when it rains and the river flows more strongly the sand spit break down.
  - The sand spit is built because the river flow is usually weak than of the ocean. All the sand that the river drags sink when the the river meets the ocean and its flow gets weaker (not strong enough to continue to drag the sand).
  - In the end we can control the flow of the Alexander river and thus control the close/open state of it.
  - In the Alexander river the tides have almost no effect.
- The estuary water becomes saltier the closer to the ocean it gets. Also the water at the bottom is saltier because that's where they enter from the ocean. The layers depends on a mixed the saltwater and freshwater are:
  - **Salt wedge** estuary: 
    - Very clear boundary between the saltwater layer and the freshwater layer (see pictures below). Strong halocline.
    - Higher flow of freshwater into the sea/ocean than the opposite.
  - **Well mixed** estuary: the tides play a big role in the mixing of the sea water and river water by going up and down the river.
  - **Partially mixed** estuary: between salt wedge and well mixed. Halocline not well defined.
  
![layers types](./img/07_Estuaries/layers_types.png)

![all types](./img/07_Estuaries/all_types.png)

- A difference of 10 in salinity is $`\pm`$ equivalent to $`50\degree C`$ difference in temperature.
- In the summer a river will be hotter than the sea and in winter it will be colder.
- After the ice age, the sea level lowered thus a lot of estuaries became waterfalls. With global warming the rising of the sea level, we expect to see more estuaries because of those waterfalls that can become estuaries again.
- Areas of an estuary:

![estuary areas](./img/07_Estuaries/estuary_areas.png)

- **Flow rate of the river**:
  - The water form the river gets saltier the more it advances into the sea (top layer because of low salinity). It gets saltier because the saltwater from the sea has to go up from being stuck by the land (see picture).
  - **R** represent the flow rate of the freshwater from the river.
  - The more the water flows into the ocean the more water there is (because of the saltwater that goes up??)
  - The difference in saltiness can help calculate the flow rate of freshwater:
    - If the seawater has a salinity of 33 and at a certain point the salinity is 16.5 it means that half of the water is freshwater and half is saltwater thus the rate is 2R (R being the flow rate in the estuary before the mixing).
    - If the water at a certain point has a salinity of 26.4 then the saltwater is 80% of the water (26.4/33=80%) thus the freshwater is 20% or 1/5 of the water which is 5R.

![flow rate](./img/07_Estuaries/flow_rate.png)

- In estuarine circulation there is more condensation (water comes from rain and run-offs) than evaporation.
- Lagoon circulation (like the Mediterranean sea) has more evaporation than condensation.
- What the estuary ecologic system gives us:
  - Habitat for young animals to grow (they then go live in the sea).
  - Nutrients and contaminants trap:
    - Organic matter that grows in the estuary gets into the sea to increase the quantity of nutrients.
    - Contaminants like metals and organic waste sink into the estuary.
  - Lessen the strength of floods.
- Antropogenic affect: slide ppt7/slide 30
  - Ports and cities around.
  - Less freshwater flow from using the water upward.
  - The freshwater is replaced with (more or less) treated water.
  - Nutrients from farms.
  - Contaminants from fumigations.
  - Other industrial waste (heavy metals and such).
