# Box Model

- Looking at the surface and deep water, the water that goes from the surface to the deep water (downwelling) is equal to the quantity of water that goes up from upwelling.
- Assumptions:
  - No particles go to sediments thus it goes back up as dissolved matter in upwelling.
  - No input from rivers (it is insignificant compared to the fluxes between surface and deep water).

![fluxes](./img/10_Box_Model/fluxes.png)

- Looking at phosphorus since it is the real limiting factor in the ocean and unlike nitrate it cannot be fixed from the atmosphere - adding an input origin which complicates the model.
- Phosphorus goes down from the surface to the deep water (circles in yellow in the above graph): $`\nu \cdot C_{surf}`$
- It also goes down as particles $`\Phi`$ cicled in green.
- The **overall downwards flux**: $`\nu \cdot C_{surf} + \Phi [mol/yr]`$
- The **overall upwards flux**: $`\nu \cdot C_{deep}`$
- The rate of change in quatity in the deep water: 

```math
V_{deep} \cdot \frac{\delta C_{deep}}{dt} = \overbrace{\nu \cdot C_{surface} + \Phi}^{influx} - \underbrace{\nu \cdot C_{deep}}_{outflux}
```

- The rate of change in quatity at the surface: 

```math
V_{surface} \cdot \frac{\delta C_{surface}}{dt} = \overbrace{\nu \cdot C_{deep}}^{influx} - (\underbrace{\nu \cdot C_{surface} + \Phi}_{outflux})
```

- At the surface the concentration of phosphorus is 0 thus we can get to the conclusion that the flux of particles into the deep water is equal to the flux of dissolved P in upwelling:

```math
\begin{aligned}
& V_{surf}\cdot \frac{dP_{surf}}{dt} = \nu \cdot P_{deep} - (\nu \cdot P_surface + \Phi_p) = 0 \\
& \Phi_p = \nu \cdot (P_{deep} - P_{surf}) \\
& P_{surf} \approx 0 $`\Rightarrow`$ \boxed{\Phi_p = \nu \cdot P_{deep}}
\end{aligned}
```

> There's more phosphorus going down into the deep water as particles than as dissolved matter since the amount of dissolved phosphorus at the surface is 0.

- We know that at the surface the concentration of phosphorus is 0 and in deep water around 2.1 $`\mu mol\;L^{-1}`$.
- We also know the flux of water in down/upwelling.
- We now can know how much phosphorus goes down as particles: $`\Phi_p = \underbrace{\nu}_{1.2\times10^{15}m^3\;yr^{-1}} \cdot \underbrace{P_{deep}}_{{2.1 mmol\;m^{-3}}`$
- From this we can know how much carbon is passed on from the surface to the deep water (Redfield). This helps understand how the ocean helps cope with global warming.
- When taking those results and using them for oxygen concentration, the model doesn't work. That's because the concentration of phosphorus at the surface is not really 0.
  - A more complex model is needed (3 boxes model)
