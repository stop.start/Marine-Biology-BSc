# Calculations

```math
1m = 100cm \Rightarrow 1m^3 = (100cm)^3 = (10^2cm)^3 = 10^6cm^3 \\
\\
1g = 1cm^3 \Rightarrow 1kg = 1000cm^3 \\
\\
\boxed{1m^3 = 1000L} \\
\\
1 Mol = 6\times 10^{23} \\
1 \mu Mol = 6\times 10^{17}\\
\\

```
