# Dissolved Gases

- Gas is one of the 4 states of matter. A gas can be from a pure element like $`O_2`$ or more complex molecules like $`CO_2`$.
- In the sea there can be gas everywhere.
- 50% of the atmospheric oxygen $`O_2`$ comes from the sea (photosynthesis).
- In the sea, organisms also need oxygen, but in certain areas like basin-margin (אגן שוליים) there is not enough oxygen.
- 30% of the atmospheric $`CO_2`$ is absorbed by the sea.
- DMS (dimethyl sulfide) produced by organisms and function as an aerosol in the atmosphere (also gives that "sea smell").
- Gases like CFC that are man made and not used anymore (caused damage to the ozone layer) can be used as **tracers**: if they are found at a certain depth this means this body of water is at least 30/40 years old.
- Exchange of gases between the atmosphere and the sea:
  - Diffusion
  - Mixing (from winds and currents)
  - Internal processes (photosynthesis, respiration, breaking down of matter)
  - External processes (underwater volcanoes, hydrothermal)
  
![gas exchange](./img/05_Dissolved_Gases/gas_exchange.png)

- The atmosphere (mainly talking about the troposphere) is homogenic (a lot of mixing).
  - Only the water quantity changes depending on time and place.

![atmospheric gases](./img/05_Dissolved_Gases/atmospheric_gases.png)

- Gases in the sea are measured in $`\frac{ml_{gas}}{Liter_{seawater}}`$ where the gas volume is measured in STP (standard conditions).
- For ideal gases, all gases have a volume **V=22.414L** (per mol) in STP.
- The total pressure of a mixture is equal to the sum of the pressure of each gases that compose it (**Dalton's law**).

```math
\begin{aligned}
& P_t = \Sigma P_i \\
& P_t = P_{N_2} + P_{O_2} + P_{H_2O} + P_{Ar} + ...
\end{aligned}
```

- The atmospheric pressure at the surface is 1atm although it is not precise since there are some fluctuations in atmospheric pressure: anticyclones and depressions, changes in air temperatures, tides...
- The higher the temperature the more water a block of air can retain.
- Humidity is measured based on the maximum humidity possible. _For example, if the maximum is 20mbar and it is measured 10mbar then the humidity is 50%_.
- Because the water pressure in the atmosphere changes all the time, it is more comfortable to use **partial pressure** for a specific gas for **dry air**:
  - $`f_i`$: fraction of the gas in the dry air
  - $`P_i`$: partial pressure of the gas
  - $`P_T`$: total atmospheric pressure
  - $`P_{H_2O}`$: partial water pressure
  
```math
\boxed{P_i = f_i \cdot (P_t - P_i{H_2O})}
```

- The water pressure is calculated from the percentage of humidity multplied by the maximum humidity.
- The warmer the water the more salt it can dissolve but the less gas it can dissolve:

![dissolve](./img/05_Dissolved_Gases/water_dissolve.png)

- Cold water has a higher **saturation concentration**.
- The water at the poles can contain more gas than the water in very hot areas. It can contain 1.5 times more gas than in our area.
- Division constant (equilibrium constant?) is different from gas to gas: the bigger the molecule the bigger the constant (gas will dissolve more the bigger the molecules), the colder the bigger the constant.

![constant](./img/05_Dissolved_Gases/constant.png)

- **Henry's law** states that the amount of dissolved gas in a liquid is proportional to its partial pressure above the liquid. It means how much gas dissolves in water.

```math
\boxed{K_h \; \left(\frac{mol}{Kg \cdot atm}\right) = \frac{[A_{(aq)}]\left(\frac{mol}{Kg}\right)}{P_A \; (atm)}}
```
- The **Bunsen coefficient** $`\alpha`$ is defined as "the volume of saturating gas, V1, reduced to T° = 273.15 K, p° = 1 bar, which is absorbed by unit volume V2* of pure solvent at the temperature of measurement and partial pressure of 1 bar."

```math
\boxed{\alpha \; \left(\frac{mL_{(g)}}{L \cdot atm}\right) = \frac{[A_{(aq)}]\left(\frac{mL}{L}\right)}{P_A \; (atm)}}
```

- Showing the concentration as a percentage of saturation helps seeing the biological processes. For example Nitrogen isn't used much thus its concentration will be 100%. Elements that are used in processes will show different percentages of saturation.
- In the poles in the summer, the water is hotter thus can contain less gas, in addition there's a lot of photosynthesis which release oxygen making the water over 100% saturation, but in winter it is a lot under saturation since no photosynthesis + the water can contain more gas.
- Upwelling areas can be more or less saturated depending on where they are in the currents circulation path. Closer to the start: still is oxygen in the water, the farther the less oxygen there is.

![saturation](./img/05_Dissolved_Gases/saturation.png)

- The map below shows the saturation at 1000m. We can see the old water which in unsaturated, in contrast to the younger water which still has oxygen in it.

![saturation](./img/05_Dissolved_Gases/saturation_1000m.png)

### $`CO_2`$

- Difference in partial pressure of $`CO_2`$ between the ocean and the atmosphere determines how much of, and in which direction, $`CO_2`$ will go.
- $`\Delta pCO_2`$ is determined by differences in the water properties (T,S,A,DIC).

```math
\boxed{\Delta pCO_2 = pCO_2^{ocean} - pCO_2^{atmosphere}}
```

- The $`CO_2`$ shows that there is too much of in the equator area and not enough in the poles. 
- The upwelling areas are low in oxygen but rich in $`CO_2`$ thus those areas release more of it.
- It changes with the season.
- The exchange (between sea and atmosphere) is slow compared to that of the oxygen.

![average $`\Delta CO_2`$](./img/05_Dissolved_Gases/average_dco2.png)

- **Eutrophication** (התרה) is the process that leads to lack of oxygen:
  - Fertilizers (from human) get into the sea from  being dragged by the rain into the land.
  - Algae grow at the bottom and at the surface.
  - The algae at the surface come to grow too much thus blocking sunlight from the algae at the bottom.
  - The algae at the bottom die from lack of sunlight causing a lot of nutrients to be release (organic matter).
  - Less oxygen in the sea until it bocomes **hypoxic** (30% oxygen) and even **anoxic** (0% oxygen).
  - Fish die.
- In areas with a lot of nitrate and low on oxygen, the water can become anoxic (very rare though).
  - In the below map (dead zones) we can see that hypoxia is present mainly where humans are present (in India it was not measured).
  
![eutrophication](./img/05_Dissolved_Gases/eutrophication.png)

- How much of a gas pass is exchanged between sea and atmosphere in a $`m^2`$ surface? It is called **flow** (שטף).
- A few properties:
  - Speed of the wind at the surface is considered 0 (because of friction).
  - Same thing with the currents of the sea at the surface.
  - Still the water and atmosphere at the surface are both homogenic and there's an exchange of gases between them: **diffusion**
- In the below equation:
  - Right side: flow (שטף) with its units
  - Left side: difference in concentration between sea and atmosphere per surface unit (in equilibrium the flow is 0, e.g nitrogen)
  - $`\epsilon`$ is the gas diffusion rate constant (per gas), measured in laboratory.

![equation](./img/05_Dissolved_Gases/equation.png)

- The diffusion is faster in the atmosphere than in the sea (molecules in gas move faster). Because of that we can ignore its effect of the diffusion rate and thus we look only at the concentration of a gas in the sea.
- With the wind the diffusion is faster through the sea surface because the sea increases its surface by making waves.
- After normalizing the gases to the salinity, at the surface there is still less gas, that's because at the surface the water is hotter.
