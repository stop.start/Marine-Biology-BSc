# Intro

## James Livelock - Gaia hypothesis

- James Livelock worked at NASA on how to look for possibility of life on other planets and developed the Gaia hypothesis.
- Life detection:
  - Living organisms keep their **entropy low** (when we die we dissolve into smaller and smaller pieces increasing the entropy - living is the opposite).
  - A lot of **free energy** (like methane + oxygen that burn = free energy) points to the possibility of life.
  - Life is what builds the chemical properties of the planet.
- Other names for the Gaia hypothesis: **geophysiology**, **earth-system science**.
- The Gaia hypothesis suggests that organisms co-evolve with their environment: that is, they "influence their abiotic environment, and that environment in turn influences the biota. In other words the environment conditions (on land, water and atmosphere) match confortable living conditions and stay the same (more or less).
- Evidence of the above:
  - The Earth's temperature that hasn't changed in 3.5 Ma years depiste a 25% increase in radiations from the Sun.
  - The chemical composition of the oceans and atmosphere is stable for the last 500M years.
- Before life, $`CO_2`$ constituted 25% of the atmosphere. After life started, it decreased due to photosynthesis.
  
### Daisies (חינניות)

|             | Black Daisies                                       | White Daisies|
|-------------|-----------------------------------------------------|--------------------------------------------------------------------|
| Hot weather | Die                                                 | Are able to return radiations increasing the albedo |
| Cold weater | Are able to absorb and release radiations, warming up themselves and the environment | Die                |

- In all both types of daisies help regulate the globe's climate.

## Humans control the Earth

- Animals once controlled the environment but humans have caused a lot of extinctions in the animal kingdom. 
  > _Note:_  
  > _In Africa man and animal have developed together thus the animals have learned to be aware of men._
  > _That's why there are still big animals in Africa whereas in other part of the world they were killed._
- In Autralia, Aborigenes were hunting with fire which burned the original pines (אורנים) making room for eucaliptus that develop faster.
- Ice ages stopped. One hypothesis is that the big animals got extinct thus there was more plants making the climate more stable
- **Holocene** current epoch, being replaced by **anthropocene** due to the human impact.

## Climate change

- The world gets warmer wether we look from year to year or average per decade.
- The temperature has **increased** $`\bold{1\degree C}`$ from global warming.
- The sea temperature has **increased** $`\bold{0.1\degree C}`$ from global warming.
- The $`CO_2`$ concentration is increasing 
- **Keeling curve** is the measurement of the $`CO_2`$ concentration in **Hawaii** over the years.
- The movement that we see on the keeling curve is because of the seasons, in summer there are more plants thus more absorption of $`CO_2`$ (on the graph we can see ups and downs while overall the curve goes up).
- Air can be trapped in ice (as bubbles), that's how we can analyse and date old air. Usually the ice is taken from **Antartica**.
- Comparing the $`CO_2`$ concentrations from Antartica and Hawaii shows that the climate change is in fact _global_ warming.

| Keeling curve (Hawaii)                       | Measurement from ice (Antartica)            |
|----------------------------------------------|---------------------------------------------|
| ![keeling curve](./img/01_Intro/keeling.png) | ![co2 from ice](./img/01_Intro/co2_ice.png) |

- **Greenhouse gases** by importance:
  - Water is the main greenhouse gas in the atmosphere. As humans, we do not change directly its concentration. Its effect on the temperature is unclear since it participates in both positive and negative feedback.
  - Carbon dioxide $`CO_2`$
  - Methane $`CH_4`$
  - Nitrus oxide $`N_2O`$
  - Ozone $`O_3`$
- **Positive feedback**: cycle which causes a phenomenon to get stronger, in this case the increasing surface temperature:
  - Temperature increases $`\longrightarrow`$ Increase evaporation from the oceans $`\longrightarrow`$ Increase concentration of water in the atmosphere $`\longrightarrow`$ Enhanced greenhouse effect $`\longrightarrow`$ Temperature increases
- **Negative feedback**: opposite of positive feedback, in this case the decreasing surface temperature:
  - Temperature increases slightly $`\longrightarrow`$ Increase evaporation from the oceans $`\longrightarrow`$ Increase concentration of water in the atmosphere $`\longrightarrow`$ More clouds in the atmosphere $`\longrightarrow`$ Higher albedo $`\longrightarrow`$ Temperature decreases

- **Predicting the future**:
  - In order to create accurate (in theory) models for the future, data from the past is used to see if the predictions are accurate (does the model created programmatically can predict what happened in the past based on old data?)
 - In the below examples, the left model is without the effect of human beings vs real data. We can see that it doesn't match. On the right, the effect of humans has been added (like $`CO_2`$ emission) and it matches the real data for the same period.

 ![models](./img/01_Intro/models.png) 

### IPCC

- IPCC (intergovernemental panel for climate change) is and international organization working on the climate change.
- According to the IPCC, more than **90% of the radiations** coming to earth is **absorbed by the sea**.
- There are changes in the amount of greenhouse gases and aerosols in the atmosphere and changes on lands which disturb the balance of the climate.
- The **concentration** of $`\bold{CO_2}`$, $`\bold{N_2O}`$ and $`\bold{CH_4}`$ increased a lot since the start of the **industrial revolution** (1750).
- The increase in $`CO_2`$ comes mainly from burning fossil fuel and the increase in $`N_2O`$ and $`CH_4`$ comes mainly from agriculture.

#### Consequences of global warming

- Increase in temperature.
- Heat waves are becoming more common.
- The ice is melting.
- There are more and more floods.
- More and more affected areas.
- More tropical storms that are stronger.
- Sea level is rising.

### Sea level

- Higher temperature means more evaporation. The reason the sea level is rising and not decreasing from the increase in temperature is because warming a body will expand it.
- From an increase of $`0.1\degree C`$, the sea level can rise around 17cm from its expansion.
- Another cause of the rise of the sea level is the melting of the terrestrial ice.
- Because of the Archimede law, the iceberg in the sea don't affect the sea level since 90% of their volume is already in the sea. 
