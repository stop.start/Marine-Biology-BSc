# Salinity and the Composition of Seawater

- A solution is always **homogeneous**.
- If there's solid (even in small particles) in a solution it isn't part of the solution. Molecules from this solid will dissolve in the solution increasing their concentration.
- Salinity is affected only by evaporation and condensation thus it changes mainly at the surface.
- The definition of a molecule vs solid particle is $`7\mu m`$ (below it's a molecule and above it's a solid).
- In the sea:
  - The salinity is rather stable.
  - The salinity is higher at the surface.
  - The difference in salinity depends on the latitude.
  - In basin-margin (אגני שוליים) the difference is higher.
- From the map below, we can see that:
  - The salinity is between around 30 to around 40.
  - In the poles, the salinity is low.
  - In the equator, the salinity is low. This is caused by the **upwelling** from the Coriolis effect that "sends" water to the side causing deeper water to go up. In addition there is a lot of rain.
  - Between the equator and the poles the salinity is higher. This is because of the רצועת המדבריות where evaporation is higher than the condensation (rain and such).
  - In the **north Atlantic** there's an polar area with a higher salinity than expected. This is caused by the salty water that comes from the Mediterranean sea. Having hihgly salty water that is also cold causes high density thus this water sink to the bottom.

![salinity map](./img/03_Salinity_Composition/salinity_map.png)

- Looking at the same map but at 2000m deep, we can that the salinity is more homogeneous. 
- The difference between the equator, poles and in between isn't there anymore.
- Still we see the effect of the Mediterranean on the Atlantic.

![salinity map 2](./img/03_Salinity_Composition/salinity_map2.png)

- **Marcet law** states that the **proportion of the ions in the sea is always the same***. Enough to know the concentration of one ion to be able to calculate the concentration of the rest.
- This is called a **conservative behavior** (התנהגות משמרת).
- Certain ions have a specific error margin to say that the water has a conservative behavior:
  - $`Sr^{2+}`$: <2% 
  - $`Ca^{2+}`$: <1%
  - $`HCO_3^-`$: <20%. Bicarbonate is the only componant with a non stable concentration.
- **Major ions** are the ions that have a concentration of at least **1mm/kg of water**.

![ions concentration](./img/03_Salinity_Composition/ion_concentration.png)

- **Recycled behavior** are said when there is less solute (e.g phosphate) at the surface because it is in the organims and deeper the concentration of the solute increases since there are less living organims. The dead organims dissolve which shows on the graph less carbon with depth.

![recycled](./img/03_Salinity_Composition/recycled.png)

- In order to get the componants out of the water to weigh them or use them, increasing the temperature **is not** a good option since in addition to the water other particles will evaporate.
- Finding the concentration of Chlore is the best way to find out the concentration of the other componants (since the ratio stays the same).
- **Chlorinity (%C)** is a mix between **Sorenson titration of chloride** and the **Marcet law of proportions**.
- Today (since the mid 20th century) chlorinity isn't used anymore instead **electric conductivity** is used to measure salinity.
- The **electric conductivity** measured is compared to a standard **KCl** and their ratio is converted into salinity. The precision is 0.001.

![composition of seawater](./img/03_Salinity_Composition/seawater_composition.png)

- In order to decide if an element has a conservative behavior, the best way is to draw a graph with the element's concentration vs the salinity and look at the measurements vs the trendline (קו מגמה). Trying to compare graphs that shows salinity vs depth isn't effective. The trendline allows to see better how close are the measurements to be conservative.
- The blue measurements seem conservative in the first graph but with a scatter graph it's more obvious that it's not totally conservative.

|                                                                  |                                                                  |
|------------------------------------------------------------------|------------------------------------------------------------------|
| ![measurements](./img/03_Salinity_Composition/measurements1.png) | ![measurements](./img/03_Salinity_Composition/measurements2.png) |

- All solutes are affected by evaporation and condensation.
- **Normalization to salinity** take the measurements for any salinity and cancel out the effect of evaporation and condensation. This is done by converting them to a predefined salinity (for example 35).
- This allows to compare the water from different places in the world without the effect of evaporation and condensation which is different depending on the location, thus with this normalization we can see if something else affect the water.

```math
\boxed{\frac{C_{Norm}}{C_O} = \frac{Sal_{Norm}}{Sal_0}}
```

### Concentrations types

- **Conservative**: the concentration doesn't change with depth, on a graph it shows a straight veritcal line.
- **Biolimiting and Recycled**: elements that are used in biological processes at the surface thus their concentration is 0 at the surface and increases slowly with depth up to 1500m where it stabilizes.
  - Recycled elements dissolve completly in deep water. 
- **Biointermediate**: elements used in biological processes at the surface but they're not limiting thus their concentration is not 0 at the surface and still increases a bit with depth up to 1500m where it stabilizes.
- **Scavenged**: other types of elements.

### Periodic table

![periodic table](./img/03_Salinity_Composition/periodic_table.png)
