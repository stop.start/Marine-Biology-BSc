# Trace and Minor Elements

- **Minor elemtents** have a concentration of <1ppm ($`<mg \; kg^{-1}`$).
- **Trace elemtents** have a concentration of <1ppb ($`<\mu g \; kg^{-1}`$).
- **Biolimiting elements**: N,P,Fe,Si
- **Biointermediate elements**: C,Sr,Ba,Ca
- Fertilizers (nutrients) on land are dragged through the soil and into the sea by rain.
- Nutrients are dissolved in the sea through bacteria.
- Their size affect how fast they reach the ground.
- From the below graphs comparing Carbon, Nitrogen and Phosphate in dissolved state (top line) vs particle state (bottom line), we can see that particles are found at the surface while dissolved nutrients are found in deeper water.

![particles vs dissolved](./img/04_Trace_Minor_Elements/particles_dissolved.png)

- When comparing nutrient concentration in oceans, the concentrations are not the same. That's because of the conveyor belt:
  - Cold salty water around Greenland sinks to the ground with almost no dissolved nutrients because of it comes from the surface area with organisms that use the nutrients.
  - Along with the cold water at the bottom, there more and more dissolved nutrients that fall down.
  - Particles can sink from everywhere but can resurface only from **upwelling** areas thus deep water gets richer and richer in nutrients until it resurfaces and then gets poorer in nutrients from use in biological processes.
- Because of the **conveyor belt**, the concentration of nutrients in deep water increases from: North Atlantic < South Atlantic < South Pacific < North Pacific.
  - That's because the surface water arrives at the North Atlantic (near the pole) empty from nutrients (they have been used) and sinks to the bottom, and the cycles continues: particles sink to the deep water from rain and soil which are dragged by the current.
- The most dissolved nutrients are not found deep, nor at the surface but in between. It is **old water** that doesn't move.
- The difference between the amount of Silica/Phosphate and Carbon is that the bacteria will "prefer" to break down dead organisms first before their silica shell.
  - Phosphate has a steeper profil due to it being used in building soft tissues.
- At the surface, everything is needed thus used.

  |                                                               |                                                                   |
  |---------------------------------------------------------------|-------------------------------------------------------------------|
  | ![oceans](./img/04_Trace_Minor_Elements/oceans_nutrients.png) | ![conveyor belt](./img/04_Trace_Minor_Elements/conveyor_belt.png) |


- Some of the recycled nutrient are biolimiting and some are not.
- On the surface:
  - At the poles there is a high concentration of dissolved nutrients since there not a lot of organisms that use them.
  - At the equator there is also a higher concentration of dissolved nutrients because of the upwelling. 
  - On the western side of the continents, there's also some upwelling. It is mainly on the western side due to the Coriolis effect.
  - On the regions without upwelling and with a lot of sunlight the nutrient concentration is low.
  - The main source of solutes (phosphate, nitrogen) in the ocean near the continents is from dust, water streams.
- At 3000m it is the opposite of what's going on at the surface.
  
![phosphorus concentration](./img/04_Trace_Minor_Elements/phosphorus.png)

- Part of the recycled matter sinks to the bottom (sedimentation). 
- Carbon that doesn't sink to the bottom (sedimentation) will go through upwelling and go back to the surface after around 1000 years.
- Carbon that does sink is stuck forever in the sediments.
- Phosphate is also a biolimiting element on the continents. It goes through land before it gets into the sea through rivers and dust.
- Phosphorus will go back to land from sediments via **geologic elevation** (convergent tectonic plates). Also **guano** is the accumulated excrement of seabirds (can be penguins) and bats. It is a highly effective fertilizer due to its exceptionally high content of nitrogen, phosphate and potassium.

![nutrient cycle](./img/04_Trace_Minor_Elements/nutrient_cycle.png)

- Most of the inorganic phosphorus is found as **phosphoric acid**.
- In the ocean it has several forms that differ from their charge: $`PO_4^{2-}`$, $`PO_4^{3-}`$, $`H_2PO_4^{-}`$
- In the ocean: **SRP** (soluble reactive phosphorus) = phosphate. There is also **DOP** (dissolved organic phosphorus): like from DNA - it takes long for the bacteria to break it down.
- Phosphorus also goes through **scavenging**: phosphorus molecules stick to iron and sediments molecules.
- In Israel there are areas with more phosphate in the soil. It is used as fertilizer and sold. There is less and less.
- **Nitrate**, like phosphate is more present in the upwelling areas and near the continents, and around the pole.
- Nitrogen can be found in multiple forms. The human body uses mainly the nitrogen in its amonia form $`NH_4`$.
- **Nitrogen can't be fixed from the atmosphere**, only cyanobacteria know how to fix it.
- In the sea nitrate $`NO_3`$ is mainly found.
- **DON** (dissolved organic nitrogen) like DOP is hard to dissolve.
- $`N_2`$ is fixed by bacteria. This transform nitrogen into a particle in the sea. In organisms the nitrogen is in amonia form $`NH_4^+`$. Part of the amonia is "peed" out as a solute which is oxidized then nitrogen is found as $`NO_2^-`$.
- Today there are machines than can produce amonia by putting iron, hydrogen and atmospheric nitrogen and amonia comes out.

![nitrogen](./img/04_Trace_Minor_Elements/nitrogen.png)

![elements](./img/04_Trace_Minor_Elements/elements.png)

- Metals tends to get oxidized thus is hard to measure, especially with boats made of metal.
- **Metals have a biolimiting profile**. They are measured in ppb (part per billion).
- Scavenged particles come from dust. High concentration on the surface and low deeper. That's because at first it dissolved but then it meets organic matter that stick on it and make it solid.
- Aluminium is also a scavenged element. At some point it reappears (redissolves).
- Iron (Fe) is a recycled element. It controls the climate.

