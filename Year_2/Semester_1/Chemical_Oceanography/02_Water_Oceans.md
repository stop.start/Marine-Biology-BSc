# Properties of Water, Saltiness and Descriptive Oceanography

- Oceanography studies the **geographical distribution of chemical elements** in the sea and why they are scatterd that way.
- Chemical measurements need to be precise, reproducible by other scientist (like all experiments) and need to have a statistical meaning.
- The distribution chemical elements can be horizontal (longitude), vertical (latitude), and/or with time (like ice periods that come and goes).
- The **scientific method**: 
  - Observe
  - Make a theory
  - Use the theory to predict an outcome
  - Make an experiment
  - Observe again, has the prediction based on the theory actually happened in the experiment?
  - Accept or reject the theory (will usually need more than one experiment)

![scientific method](./img/02_Water_Oceans/scientific_method.png)

- **Models** are hypothesis with a mathematical definition. 
  - The models are based on samples taken, they are then used to predict an outcome and compared to samples taken (old and new).
- Usually the measurements are **concentrations**.

## Properties of water

- Where does the water come from? 
  - One theory is that when the Earth was formed it was so hot that gases escaped and created the atmosphere. The Earth then got colder and the gases lost energy thus hydrogen and oxygen came together to form water.
  - Another theory based on meteorites that fell on Earth, is that it came from a lot of meteorites that fall on Earth after its creation.
- The water molecule isn't symetric and the charge isn't equally distributed (polar molecule), it has a **dipole**. This is what allows the molecules to bond (hydrogen bonds).
- The hydrogen bonds consistently form and break.
- Water looks partly like (ice) crytals (גבישים).
- Ice formation: the colder the more crystals there are until all molecules form a crystal.

### Water anomaly

- The **water anomaly**: when the temperature decreases, the density of water increases until it reaches $`4 \degree C`$ then it decreases (in icebergs, the density decreases around 10%).
- On average, around the freezing point, liquid water molecules have 4.4 "neighbors" while ice molecules have exactly 4.

![water molecules density](./img/02_Water_Oceans/water_molecules.png)

- This anomaly is what make ice floats and that's what allow life to continue in frozen lake in winter:
  - The water at the surface gets colder. 
  - Cold water is denser thus sinks beneath the surface make a new layer of water at the surface which gets colder and sinks and so on and so on until all of the water reaches $`4 \degree C`$ trapping the top layer of water at the surface since it won't become denser than the layers below.
  - At $`4 \degree C`$, the water at the surface starts to become less dense than the water below will stay at $`4 \degree C`$.
  - The ice at the surface isolates the water beneath since it stays at the surface and is the only layer that is affected by the colder temperatures.

![water anomaly](./img/02_Water_Oceans/water_anomaly.png)

### Heat capacity

- **Heat capacity is how much energy is need to increase the temperature of 1gr of water $`\bold{1 \degree C}`$**.
- Water has the **highest heat capacity** (קיבול חום) after $`NO_3`$. This means that it takes the water a lot of time to warm/get hot.
- This allows the water to regulate the temperature coming from the Sun. This is important because the Sun delivers a lot of radiations around the equator and water is what allows it to stay at a viable temperature:
  - Because it takes time for water to get hot, it decreases the temperature of the atmosphere.
  - Cold water from beneath that gets warmer will go upward and to the sides (upwelling). This is what regulate the temperature at the poles and allows them not to be too cold.

### Melting water

- Water has the highest **latent heat of fusion** after $`NH_3`$ (energy require to change state from solid to liquid).
- Ice needs a lot of energy in order to break bonds and become liquid.
- **Melting ice causes a big change in temperature** (less albedo).

### Water evaporation

- It also needs a lot of energy in order to break bonds of liquid water.
- Water has the highest **latent heat of vaporation**.

![water properties](./img/02_Water_Oceans/water_properties.png)

## Sea properties

### Density and Freezing point

- As mentioned above the **temperature of maximum density is $`\bold{4 \degree C}`$** but **with salt the temperature of maximum density lowers**.
- **At 25 g/kg saltiness, both the maximum density and the freezing poing are at $`\bold{-1.9 \degree C}`$**.
- Salt water push out the salt when freezing.
- At a saltiness above 25, the maximum density is **below** the freezing point, meaning that salted water keeps getting heavier and isn't "trapped" above all other layers in order to turn into ice.
- Theoretically, if all the water (all the layers) gets to the freezing temperature it will start to freeze.
- In reality, what happens is that some areas become saltier and some areas become less salty (see [below](./02_Water_Oceans.md#salt)). The saltier water is denser thus sinks at the bottom leaving less dense water at the top. This layer can freeze. In order for ice to actually form, the weather needs to be calm (at night there's a higher chance).
- Sea water can get at a point of saltiness that it won't freeze.

![sea properties](./img/02_Water_Oceans/sea_properties.png)

### Viscozity (צמיגות)

- Viscozity is the resistance to flow.
- In order to test the viscozity of a substance, some is put on a surface then put a glass plate on it and test how easy or hard it is to slide the glass plate. _For example, honey won't let the glass plate move a lot but water will let it slide quite easily_.
- Water has a very low viscozity.
- This means that the Sun doesn't have to put a lot of energy in order to create currents.

### Water as a solvent

- Water can dissolve the most substances. This is important in biology in order to dissolve and pass around all the nutrients and substances need by the organism.
- Water moleluces will "attack" for example NaCl molecules. The positive end of the water molecules will attack the $`Cl^-`$.
- **The increase in density when salt is added to water isn't because of the salt itself, but also because the water molecules are attracted to the ions and come together to attack them making the water molecules more "packed"**.
- _Note: outside of the water, substances like NaCl will pull in water from the atmosphere and that's why in labs it is important to change and weigh substances because a mole of substance change weight with the water it pulled in_.
- **Cl is the most common solute in the sea**.

#### Composition of sea water

- The following constitute 99% of the dissolved salts in sea water:
  - Cl and Na constitute 86% of dissolved salt sea water.
  - Sulfate $`SO^{-2}`$ 
  - Magnesium $`Mg^{2+}`$
  - Calcium $`Ca^{2+}`$
  - Potassium $`K^+`$
- In 1L or 1Kg of sea water there is 965gr of water and 35gr of salts.

![salt in water](./img/02_Water_Oceans/salt_water.png)

## Salt

- Salt affects all the properties of the water.

![salt](./img/02_Water_Oceans/salt.png)

- As mentioned with salted water and low temperatures, some areas become saltier and some less.
- The less salted areas can freeze leaving some "holes" in the ice at the surface. Those are called **polynya**.
- The **polynyas** are saltier areas thus more dense and those are the areas where the **water sinks**.
- With enough cold those polynyas can also freeze.
- The saltier water that sunk, flows at the bottom of the oceans from the poles to all oceans.
- Although the salt sinks the always gets more salt from water that freezes and extracts the salt out..
- Because those areas gets saltier the freezing point decreases.

