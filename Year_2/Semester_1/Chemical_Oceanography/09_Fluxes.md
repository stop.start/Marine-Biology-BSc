# Fluxes

## Ocean box model

### Accumulation hypothesis

- No lost of matter.
- Source of dissolved matter only from rivers.
- Volume of the ocean is stable.
- C = concentration of dissolved matter.

```math
\begin{aligned}
& \text{Quantity of dissolved matter from rivers into the ocean every year:} \\
& V_{river} \times C_{river} \\
&\\
& \text{Since the volume of the ocean is stable:}\\
& V_{ocean} \qquad \cdot \underbrace{\frac{\delta C_{ocean}}{\delta t}}_{\text{ocean concentration change}} = V_{river} \cdot C_{river} \\
& \\
& \text{Concentration change accross time:} \\
& C_{ocean(t)} - C_{ocean(t=0)} = \frac{V_{river} \cdot C_{river} \cdot \Delta t}{V_{ocean}}
& \\\\
& \text{At the begining the ocean was composed of fresh water (=no dissolved matter) thus:} \\
& C_{ocean(t=0)} = 0 \\
& C_{ocean(t)} = \frac{V_{river} \cdot C_{river} \cdot \Delta t}{V_{ocean}}
\end{aligned}
```

- A conclusion of this model is that the concentration of dissolved matter in the ocean is proportionate to the concentration in the rivers ($`C_{ocean} \propto C_{rivers}`$).


![box model 1](./img/09_Fluxes/box_model_1.png) 

- Age of the accumulation $`\tau`$
  - $`tau_{a}`$ is (equal to) the time $`\Delta t`$ that took an element A to accumulate in the ocean according sea and rivers.
  - It is more or less the same for all the elements.
  
```math
C_{ocean(t)} = \frac{V_{river} \cdot C_{river} \cdot \Delta t}{V_{ocean}} \Rightarrow \boxed{\Delta t = \tau_a = 
\frac{
  \overbrace{
     V_{ocean}\cdot C_{ocean}
   }^{\text{reservoir size}}}
{
   \underbrace{
     V_{river} \cdot C_{river}
}_{\text{flux (rate)}}
}}
```

- Looking at the numbers with this theory... it doesn't work... The theory is not correct.
  - In order to have a stable state we need to look at what goes in as well as what goes out.
- We need to look at a more dynamic model.

### Kinetics hypothesis

- Dynamic balance: 
  - Inflow Q (mol/year)
  - Outflow R 
  - Constant k ([1/year] - per year)

```math
\begin{aligned}
& \text{Change of moles per time:}\\
& \frac{\delta M}{\delta t} = Q - R
\end{aligned}
```

- In a **stable state**: $`Q = R`$
  - $`R = kM`$
- Mass of dissolved matter A in the ocean: $`M^A_{Ocean} = C_{ocean} \cdot V_{Ocean}`$
- Rate of accumulation of A in the ocean:
  - **Inputs** from rivers [mmol/year]: $`V_{rivers} \cdot C_{rivers}`$
  - **Removal** mostly into sediments [mol/year].

```math
\begin{aligned}
&\boxed{
\frac {\delta M^A_{Oc}}{\delta t} = \frac{\delta (C_{Oc}\cdot V_{Oc})}{\delta t} = Inputs - Removal
} \\
& V_{Oc} \cdot \frac{\delta C_{Oc}}{\delta t} = Inputs - Removal = V_{rivers} \cdot C_{rivers} - R \\
& \text{Ocean is in stable state thus no change in concentration which means what comes in goes out:}\\
& \frac{\delta C_{Oc}}{\delta t} = 0 \Rightarrow R = V_{rivers}\cdot \C_{rivers}
\end{aligned}
```

![box model 2](./img/09_Fluxes/box_model_2.png) 

- Looking at the different elements concentration per year:

```math
R[mol \cdot yr^{-1}] = k[yr^{-1}] \cdot C_{Oc}[mol\cdot m^{-3}] \cdot V_{Oc}[m^3]
```

- After some math in ppt9/23:
  - $`\frac{1}{k} = \tau_a = \tau_r`$ is the residence time per year

```math
\begin{aligned}
&\frac{1}{k} = \frac{V_{Oc}\cdot C_{Oc}}{V_{rivers}\cdot C_{rivers}}\\
\\
&\boxed{
\boxed{
C_{Oc} = \tau_r \cdot \frac{V_{river}\cdot C_{river}}{V_{Oc}}
}}
\end{aligned}
```

- **Removal processes**:

| Biological                                 | Non biological       |
|--------------------------------------------|----------------------|
| Skeleton building                          | Scavenging           |
| Soft tissues                               | Redox                |
| Complexes formation with organic molecules | Hydrothermal removal |


- In reality, the removal is less effective the more of an element there is or the longer it stays in the ocean.
  - Michaelis-Menten graph
  
  ![michealis menten](./img/09_Fluxes/michealis_menten.png)
