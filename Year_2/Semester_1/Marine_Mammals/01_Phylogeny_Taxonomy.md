# Phylogeny & Taxonomy

![taxonomy tree](./img/01_Phylogeny_Taxonomy/taxonomy.png)

## Definition of Mammal

- **Phylum**: Chordata
- **Subphylum**: Vertebrata
- Mammary glands to feed foetuses* (בלוטות חלב להזנת וולדות*​)
- Placenta (שליה*​)
- Birth live foetuses
- Monotremata (יונקי ביב) lay egg so no mammary glands.
- **Homoeothermnic**: "warm blood", keep a constant body temperature.
- They have hair (dolphins have some mustache when they are born).
- Lower jaw - mandibula - of one bone.
- Diaphragm in the chest
- Heart with 4 parts
- Neocortex
- **3 hearing bones**

## 3 orders - 5 groups

- They are all mammals thaat came back to the sea, either as **amphibians** or **obligatory marine**.
### Cetacea

- Mysticete: baleen plates (נזיפות)
- Odontocete לוויתני שיניים

![mysticete](./img/01_Phylogeny_Taxonomy/mysticete.png)
![odontocete](./img/01_Phylogeny_Taxonomy/ondontocete.png)

### Sirenia - תחשאים

- Dugong: תחש המשכםן
- Manatee: תחשהנהרות

![sirenia](./img/01_Phylogeny_Taxonomy/sirenia.png)

### Carnivora

#### Pinnipeds: טורפי הים

- Sea lions
- Fur seals
- True seals
- Walrus

![pinnipeds](./img/01_Phylogeny_Taxonomy/pinnipeds.png)

#### Otters

- Sea otter & Marine otter: לוטרות ים

![otters](./img/01_Phylogeny_Taxonomy/otters.png)

#### Polar bears

- Polar bear (ursus maritimus): דוב קוטב
  
![polar bears](./img/01_Phylogeny_Taxonomy/polar_bears.png)


## The common and the different

- All are mammals that came back to the sea.
- Some are amphibians: pinnipeds, polar bears, marine otters (sea otters are only in the sea).

### History

- 200 Ma: first mammals
- 65 Ma: extinction of dinausors
- 50Ma: first marine mammals

### Adjustments to the seals

- Insulation: water transport heat a lot easier than air thus they need a better insulation to keep warm.
- Changes in the blood flow.
- Activity without oxygen (less available oxygen in water): some animal can last hours without oxygen.
- Adaptation to the salinity and lack of fresh water to drink.
- Changes in location and structure of the organs:
  - For some the tail is now needed to move and not only for balance.
  - For some legs are needed in the water as well as on land.
- Changes in senses:
  - Smell is not needed
  - Sonar
  - Weight balance can hinder in water


## Taxonomy

- **Order** (סדרה): suffixe A (e.g Carnivora, Cetacea)
- **Family** (משפחה): suffixe IDAE (e.g Canidae, Phocidae)
- **Species**: the only classification under is male/female.

### Hints of common origin

> **Analog feature**: feature found in different but developed separately (both species needed it thus developed it: e.g dorsal fin in dolphins and sharks).  
> **Homolog feature**: features that don't look too much alike on the outside but come from the same origin (human arms & from limd in whales).  

- Anatomy (homolog features and not analog)
  - E.g: 3 hearing bones in the ear in ALL mammals (including the marine mammals that don't use it).
- Behavior
  - Movement in water
- Genetics
  - Immune system, mitochondria
  - Hard to use on ancient animals without flesh.

### Linnaean taxonomy

- Before Darwin
- Based on morphology.
- Morphology can be misleading:

![morphology](./img/01_Phylogeny_Taxonomy/morphology.png)

### Cladistics

- Change in feature: evidence of **cladogram** (development tree)
- Clade = branch

![cladogram](./img/01_Phylogeny_Taxonomy/cladogram.png)

- In the cladogram below:
  - Apomorphy (= one feature): E & F have the same feature as the common ancestor but D has developed it.
  - Synapomorphies (= several features): B & C made several changes.

![cladogram explained](./img/01_Phylogeny_Taxonomy/cladogram_explained.png)

- **Character**: inherited feature common to the subtree.
  - DNA
  - Physiologic feature
  - Behavioral
- **Character state**: several states of the same character (at least 2).
  - Swimming:
    - With all four limbs
    - Only with the two back limbs
    - Horizontal tail movements (ondulation)
    - Vertical tail movements (oscilation)
    
![swimming](./img/01_Phylogeny_Taxonomy/swimming.png)

- Old state is called **plesimorphic** (primitive).
- New state is called **apomorphic** (developed).
 
> _Note:_  
> _Primitive is said only of characteristics that didn't change for a long time and not of species_.  

- **Apomorphy**: change in state from old to new.
- **Synapomorphy**: change in several features states from old to new.
- It is not always easy to know which state is plesimorphic and which is apomorphic.
- In-group (קבוצה פנימית): group that is checked for evolution development.
- Out-group (קבוצה חיצונית): group from which the in-group is thought to have developed from.
- Sister group: group closest to the in-group (can be extand - living - or extinct).

> Comparing cetacea to the out-group.  
> Sister group: **Mesonychids**.  
> The way of swimming:  
> Plesimorphic (primitive): with 4 limbs since mesonychids came before the modern cetacea.  

- Sometimes, some ancestor species are missing and that can mean that some branching are missing.
- **Monophyletic**: one branch from one common ancestor.
  - Example: whales.
    - Definition: all of the descendants of the Pakicetus.
    - Investigation: all of the animals with the following characteristics:
      - הבולה באוזן גדולה ועבה
      - צורת הרכסים על השיניים האחוריות
      - עצם טימפנית
- **Polyphyletic**: the group is only a part of several branches that developped from a common ancestor.
  - Some species that are separated by more than 2 common ancestors can be merged together.
  - Example: River dolphin.
  - The River dolphins were all grouped together from the same branch because there were similar until an DNA test was performed and showed that they were actually 2 groups (two different branches) from a common ancestor.

![polyphyletic](./img/01_Phylogeny_Taxonomy/polyphyletic.png)

> **Exercise: build the cladogram of the pinnipeds (PINNIPEDIA)**  
> Phocidae (seals - כלבי ים), Otariidae (sea lions & fur seals - כלבי הים בעלי אוזניים), Odobenidae (walruses - ניבתנים)  
>  
> **Steps to build a cladogram:**  
> **1. Choose a group**: PINNIPEDIA
> **2. Choose the characteristics and their state in each group.**  
> **3. Arrange the characteristics in a table according to their state.**  
> **4. For each characteristics choose which is the primitive one (plesimorphic) and which is the developped (apomorphic) one with the help of outgroup**: Bears (because it is the sister group of Pinnipeds).  
> **5. Build all the possible cladograms.**  
> **6. Choose the one that requires the least 'changes in parellel'.**  
>  
> Steps 2,3,4:  
> ![char_table](./img/01_Phylogeny_Taxonomy/char_table.png)  
>  
> Steps 5,6:  
> ![all_cladograms](./img/01_Phylogeny_Taxonomy/all_cladograms.png)  
>  
> Looking at all the cladograms, the one with the least evolutionary changes is (b) which has 5 (1 and then 4).  
> ![cladograms_compare](./img/01_Phylogeny_Taxonomy/cladograms_compare.png)  
>  
> Using the cladogram to understand development processes, for example: increase in body volume. Is it a primitive (plesimorphic) or developped (apomorphic) characteristics?  
>  
> The assumption is that the increase in body volume is a way to keep heat.  
> Some species spend more time in the water which would require a way a to keep heat.  
> Seals actually are also developped into two groups, one having a smaller body.  
>  
> From the chosen cladogram, seals were the last branch suggesting that a high body volume is the plesimorphic (primitive) characteristic while a smaller one i the apomorphic characteristic.  
>  
> ![big volume](./img/01_Phylogeny_Taxonomy/big_volume.png)
