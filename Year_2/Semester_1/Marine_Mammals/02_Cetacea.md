# Cetacea

- From greek word **ketos** which means "sea monster".
- They all live fully in the water (ימים אובליגטורי).
- **External characteristics**:
  - Loss of body hair and replaced by layer of fat as insulation.
  - Loss of back legs and replaced by a tail for movement.
  - Elongation of the skull - גולגולת(telescopic) + displacement of the breathing orifice.
- The group has the most species in all marine mammals.
  - 80 living species.
  - 400 extinct species.
- **Taxonomy**:
  - Order from the class Mammalia (order = סידרה, class = מחלקה).
  - Around 87-89 species in 2 sub-orders:
    - **Mysticete** (Baleen Whales - לוויתני מזיפים) - 13 species
    - **Odontocete** (Toothed Whales - לוויתני שיניים) - majority of species
- Out group/sister group: mesonychids (Pakicetus).
- State of characteristics specific to Cetacea (and Mesonychidae):
  - Mastoid & Petrosal: lacking or reduced
  - Part of the bone מהעצם הספוגית in the ear was changed to עצם מלאה.
    - Tampon bone instead of עור תוף)
  - קפל בצורת S באחת מעצמות האוזן התיכונה* (הטימפנית), (שנוי דומה גם בתחשאים) ​
  - מבנה העצמות המקיפות את האוזן התיכונה – ייחודי
  - מבנה שונה ברכסים שונים בשיניים

## Mysticete

- Baleen - מזיפים
- The baleen is attached to the upper part of the jaw.
- The baleen is used to filter water: the water is filtered out leaving in the mouth the food.

![baleen](./img/02_Cetacea/baleen.png)

- Monophyletic suborder (common ancestor)
- 4 families:
  - **Balaenidae**: 2 types, 3 species
  - **Neobalaenidae**: 1 species
  - **Balaenopteridae**: 2 types, 6-9 species
    - Long fins (like wings)
  - **Eschrichtiidae**: 1 species

### Balaenidae

- Species:
  - Bowhead
  - Right Whale (Northen and Southern)
- Up to 18 meters
- 100 tons
- No dorsal fins
- Head is a third of the body: this for long baleen to filter the water.
- No חריצי לסת תחתונה
- The Bowhead's baleen can reach 4.5 meters (150kg).
  - It is the mammal with the longest known lifespan (200 years)

> A female Bowhead was found to be 211 years old in Alaska.  

![balaenidae](./img/02_Cetacea/balaenidae.png)

- **Skim feeding**: they feed by swimming at the surface with the mouth open letting in the plankton and filtering out the water.
  - The tongue will scrap the plankton from the baleen then the whale can swallow.

### Neobalaenidae

- Pygmy Right Whale
- Mostly unknown because very rare.
- Around 6 meters
- Its feeding habits are close to the Balaenidae.

### Balaenopteridae

- Also known as **Rorquals**.
- They have long fins and חריצי לסת תחתונה.
- 2 types:
  - **Balaenoptera**: Minke (Common & Antarctic), Bryde's (+Eden’s?),  sei,  Fin,  Blue and Omura’s (new 2003 – No common name)
    - There's an argument over certain species (do they belong?).
  - **Megaptera**: Humpback (only one that is not solitary)
- Blue Whale:
  - Can take in up to 70 tons of water in its mouth to filter the food.
  - Record: 33 meters (99 tons)
  - Record: makes the highes noise: 188Db
- Perspective (Blue whale):

![perspective](./img/02_Cetacea/perspective.png)

- Humpback whale hunt in groups by one of the pack creating a circle of bubbles around a school of fish which acts as a barrier that the fish are afraid to cross.
  - Then it will call the rest of the pack to come and eat.
  
### Eschrichtiidae

- One species left: **Grey whale** also called Eschrichtius robustus.
- There were 2 species once Pacific and Atlantic but the latter went extinct 300 years ago.
- Short and thin baleen
- 20 tons
- 14 meters
- Eat from the sea bottom. This is called **benthic feeding**.
  - Sometimes it will skim feed like Balaenidae when there's no food on the bottom.
  
### Comparasion and records

![baleen_comp](./img/02_Cetacea/baleen_comp.png)


### Differences with baleen whales

- Unlike baleen whales they hunt one individual at a time thus their body has adapted including the use of teeth instead of baleen.
- Their ribs have two parts: **vertebral & sternal** (back and chest).
  - Baleen whales do not have sternal ribs (chest ribs).
- Sternum (עצם החזה) is composed of 3 bones while in baleen whales only one bone.
  
![odontocete body type](./img/02_Cetacea/odontocete_body_type.png)

- They have asymmetric skull

| Dolphin Skull                                        | Pygmy Skull                                      | Sperm whale head                                           |
|------------------------------------------------------|--------------------------------------------------|------------------------------------------------------------|
| ![dolphin_skull](./img/02_Cetacea/dolphin_skull.png) | ![pygmy_skull](./img/02_Cetacea/pygmy_skull.png) | ![sperm_whale head](./img/02_Cetacea/sperm_whale_head.png) |

- The **blow hole** merge the two nostril together.
  - The sperm whale has the most distinct one because it has been displaced on one side of the head (left side) and cannot be seen from the right side.
  - Baleen whales (Mysticete) have two blow holes while Odontocete have one (merging the two nostril from the skull).
  
![blowhole](./img/02_Cetacea/blowhole.png)

- Baleen whales don't have a sonar.
  - The melon is the organ responsible for the sonar.
  
#### Summary

| Baleen Whales                    | Tooth Whales                          |
|----------------------------------|---------------------------------------|
| Baleen attached to the upper jaw | Teeth not baleen                      |
| 2 blow holes                     | One blow holes                        |
| Symmetric skull                  | Asymmetric skull                      |
| No chest rib                     | Sternal ribs (chest)                  |
| Sternum composed of one bone     | Sternum composed of 3 bones           |
| No sonar                         | Sonar at the front of the head: melon |


## Odontocete

- 6 families, 69 species:
  - **Delphinidae**: dolphins, orcas, pilot whales, etc.
  - **Iniidae**: river dolphins.
  - **Monodontidae**: beluga and narwhal.
  - **Phocoenidae**: porpoises.
  - **Physeteridae**: sperm whales.
  - **Ziphiidae**: beaked whales.
    - **Platanistidae**: ganges and indus river dolphins
- Records:


### Delphinidae

- The biggest family (30-35 species).
- Whales are dolphins over 4-5 meters.

![delphinidae](./img/02_Cetacea/delphinidae.png)

- The beaked whale holds the record for diving: 2992 meters, 137.5 minutes.

### Iniidae

- Platanistidae are NOT in this group but in Ziphiidae.
- Yangtze river dolphin went extinct a few years ago.

### Monodontidae

- Beluga
- Narwhal
  - Has a protruding long and sensitive tooth.
  
### Phocoenidae

- The smallest of the dolphins.
- Porpoises (the name come from the jump they make).

### Physeteridae

- Sperm whale an related (pygmy sperm whale & dwarf sperm whale).
  - Has the longest nose in the world.
- The biggest of the odontocete (up to 18m).

### Ziphiidae

- "Original whales".
- Beaked whales.


## History

- The group has the most species in all marine mammals.
  - 80 living species.
  - 400 extinct species.
- There were 3 periods of **radiations** (= in short period a lot of new species).
  - 50 Ma: Eocene period, the entry to the water thus a lot of new species.
  - 30 Ma: Oligocene period, modern whales (Neocete which split into Mistycete and Odontocete).
  - 12-15 Ma: Myocene period, 7 new species, might be from entry to the water or change in land and ocean structure.
- 50Ma:
  - There was a gap between the 2 Americas.
  - The Tethys sea was open.
  - The Pakicetus developped between India and Asia (where now there the Himalayas).
- 35Ma:
  - Tethys became shallow
  - The changes separated species between the Pacific and Atlantic.
- 12-15Ma
  - Tethys closed
  - The pathway between the Americas closed
- Taxonomy:
  - Linneus: knew they were mammals but grouped them with the fish.
  - Flower [1883]: grouped them with odd-toed ungulate (hoofed animals) [מפריסי פרסה].
  - 1960: understood that they descend from the Mesonychids which are carnivorous hoofed animals. Called the ancestor Mesonics (?)
- Pakicetus: 
  - Ears not attached to the skull
  - Nose moved a bit to the back.
- Pakicetus (50Ma) > Ambulocetus (49Ma) > Rodhocetus (46.5Ma) > Kutchicetus (43-46Ma)
- Basilosaurus & Durodon (37Ma)
- Durodon: ancestor to all today's whales.
- Aetiocetus (24-26Ma): ancestor to all today's baleen whales.
  - The last one with teeth.
  - Llanocetus and Mamalodon were its ancestors: their teeth were adapted to filter water out.
- Llanocetus still had teeth but was really big. This suggests that first came the size then the way the whales feed (baleen whales filtering water allowing them to eat a lot a once).
- Some dolphins in Japan have back limbs which proves that in most whales the gene for back limbs was not used anymore.
- Telecopisation: displacement of the nose backward.
- They cannot breath through the mouth due to the need of protect the airways when eating.
  - They still have the 2 pathways that were separating air/food in the oesophagus but both are used to eat (?)
- 3 stomachs ()
